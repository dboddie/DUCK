Disallow subclassing of final classes
=====================================

Add checks to the compiler to disallow subclassing of final classes. We can
define them but the virtual machine does not handle them.
