
Template return values with T notation
======================================

Some Android API methods return types that incorporate T, such as this one in
Bundle:

  public T getParcelable (String key)

Since it looks like this gets encoded as

  android.os.Parcelable android.os.Bundle.getParcelable(java.lang.String)

in the DEX file, it looks like the compiler has to infer the type from the
object its return value is assigned to and take care of casting it correctly,
even though the object at run-time contains information that identifies its
type correctly. (We don't have this information at compile time.)

This is also the case with the cast method in Class:

  T Class.cast(Object)

This requires the return value to be assigned to an object of the appropriate
type. If such an object does not exist then a method could potentially be
written to wrap the call in order to provide the type information that the call
requires, assigning the result of the cast call to a return value with a
predefined type:

    @args(SensorManager, [Object])
    def sensorManager(self, obj):
    
        return obj.getClass().cast(obj)

However, this still requires resolving the problem that the cast method will
return a generic Object.

Current solution
----------------

The current solution is to modify variables with types that are "placeholder"
types that are typically obtained from methods that return the "T" parameter
type. Whenever the compatibility of a placeholder type is checked against
others, we check whether the other type can be used instead of the placeholder
and, if so, propagate the type to the variable.
