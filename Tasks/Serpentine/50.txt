Stricter compatibility checking for classes
===========================================

When returning a LinkedList(String) from a method that expects an instance of
a custom template class that defines its item type as String (an equivalent
string list), the virtual machine raises an exception at run-time.

This means that the compiler treats the two template classes as equivalent
and compatible when they should be treated as incompatible.
