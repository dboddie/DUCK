Do not allow local variables defined in one suite to be referenced in another
=============================================================================

It is possible to define local variables in an if-suite and refer to them in
an elif-suite. The compiler should report an error when this occurs.
