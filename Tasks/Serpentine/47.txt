Fix generation of shr_long and shl_long instructions
====================================================

Add code to truncate the operands containing the number of places to shift.
