DUCK - Dalvik Unpythonic Compiler Kit
=====================================

The latest version of DUCK can be obtained from
[this repository](https://gitlab.com/dboddie/DUCK).

Introduction
------------

This package contains Python packages, modules and tools for reading and
writing files related to Android and the Dalvik virtual machine, particularly
DEX files and the bytecode they contain.

The motivation for this package is to understand the contents of Android's APK
package files, to help with assembling new packages without the need for the
standard Android SDK, and to experiment with compiling applications written
in programming languages other than Java.

In addition, tools are also provided to perform tasks related to publication,
such as package signing, without the need for the equivalent Java tools. See
the [Keys and Certificates](Documents/Keys_and_Certificates.md) document for
information about creating keys and certificates, and the contents of the Tools
directory for tools that can be used to create manifests and sign packages.

The [Getting Started](Documents/Getting_Started.md) document covers basic
installation of the package and its dependencies.

Compilers
---------

Since provision of compilers is one of the motivations behind this package, it
is intended that at least one will be provided. These can be found in
subdirectories of the Compilers directory. Examples for each language can be
found in corresponding subdirectories of the Examples directory.

Currently, the only compiler included is for a
[Python-like language](DUCK/Serpentine/README.md) with features that map
closely to those exposed by the DEX file format.

Tasks
-----

Instead of maintaining a separate bugtracker, tasks related to work on each
component of this package are stored in subdirectories of the Tasks directory.
This is done in the spirit of having a TODO file which would list things that
needed to be worked on. The Tools/tasks.py utility can be used to print a list
of tasks in order to get an overview of the current state of the project.

Examples and Demonstrations
---------------------------

A collection of example applications can be found in the
[Examples/Serpentine](Examples/Serpentine/README.md) directory.

Some demonstration applications are included in the
[Demos/Serpentine](Demos/Serpentine/README.md) directory.

Authors and License
-------------------

David Boddie <david@boddie.org.uk>

Unless indicated otherwise, the contents of this package are licensed under the
GNU General Public License version 3 (or later).

```"
Copyright (C) 2016 David Boddie <david@boddie.org.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
```

The authors may change the license of examples and header files to suit license
compatibility requirements in the future.
