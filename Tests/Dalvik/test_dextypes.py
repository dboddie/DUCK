#!/usr/bin/env python
# -*- coding: UTF-8 -*-

from Dalvik import dextypes

# See https://source.android.com/devices/tech/dalvik/dex-format.html for the
# table containing these example number encodings and the DWARF 3.0
# specification (http://dwarfstd.org/doc/Dwarf3.pdf) for additional tests.

def test_leb128():

    assert dextypes.read_sleb128("\x00", 0) == (0, 1)
    assert dextypes.read_sleb128("\x01", 0) == (1, 1)
    assert dextypes.read_sleb128("\x02", 0) == (2, 1)
    assert dextypes.read_sleb128("\xff\x00", 0) == (127, 2)
    assert dextypes.read_sleb128("\x80\x01", 0) == (128, 2)
    assert dextypes.read_sleb128("\x81\x01", 0) == (129, 2)
    assert dextypes.read_sleb128("\xff\xff\xff\xff\x07", 0) == ((1 << 31) - 1, 5)
    assert dextypes.read_sleb128("\x7f", 0) == (-1, 1)
    assert dextypes.read_sleb128("\x7e", 0) == (-2, 1)
    assert dextypes.read_sleb128("\x81\x7f", 0) == (-127, 2)
    assert dextypes.read_sleb128("\x80\x7f", 0) == (-128, 2)
    assert dextypes.read_sleb128("\xff\x7e", 0) == (-129, 2)
    assert dextypes.read_sleb128("\x80\x80\x80\x80\x78", 0) == (-(1 << 31), 5)
    
    assert dextypes.write_sleb128(0) == "\x00"
    assert dextypes.write_sleb128(1) == "\x01"
    assert dextypes.write_sleb128(2) == "\x02"
    assert dextypes.write_sleb128(127) == "\xff\x00"
    assert dextypes.write_sleb128(128) == "\x80\x01"
    assert dextypes.write_sleb128(129) == "\x81\x01"
    assert dextypes.write_sleb128((1 << 31) - 1) == "\xff\xff\xff\xff\x07"
    assert dextypes.write_sleb128(-1) == "\x7f"
    assert dextypes.write_sleb128(-2) == "\x7e"
    assert dextypes.write_sleb128(-127) == "\x81\x7f"
    assert dextypes.write_sleb128(-128) == "\x80\x7f"
    assert dextypes.write_sleb128(-129) == "\xff\x7e"
    assert dextypes.write_sleb128(-(1 << 31)) == "\x80\x80\x80\x80\x78"

def test_uleb128():

    assert dextypes.read_uleb128("\x00", 0) == (0, 1)
    assert dextypes.read_uleb128("\x01", 0) == (1, 1)
    assert dextypes.read_uleb128("\x02", 0) == (2, 1)
    assert dextypes.read_uleb128("\x7f", 0) == (127, 1)
    assert dextypes.read_uleb128("\x80\x01", 0) == (128, 2)
    assert dextypes.read_uleb128("\x81\x01", 0) == (129, 2)
    assert dextypes.read_uleb128("\x82\x01", 0) == (130, 2)
    assert dextypes.read_uleb128("\xb9\x64", 0) == (12857, 2)
    assert dextypes.read_uleb128("\x80\x7f", 0) == (16256, 2)
    
    assert dextypes.write_uleb128(0) == "\x00"
    assert dextypes.write_uleb128(1) == "\x01"
    assert dextypes.write_uleb128(2) == "\x02"
    assert dextypes.write_uleb128(127) == "\x7f"
    assert dextypes.write_uleb128(128) == "\x80\x01"
    assert dextypes.write_uleb128(129) == "\x81\x01"
    assert dextypes.write_uleb128(130) == "\x82\x01"
    assert dextypes.write_uleb128(12857) == "\xb9\x64"
    assert dextypes.write_uleb128(16256) == "\x80\x7f"

def test_mutf8():

    assert dextypes.read_mutf8("\xc0\x80", 0, 1) == u"\x00"
    assert dextypes.read_mutf8("\x01", 0, 1) == u"\x01"
    assert dextypes.read_mutf8("\x48\x65\x6c\x6c\x6f", 0, 5) == u"Hello"
    assert dextypes.read_mutf8("\x7f", 0, 1) == u"\x7f"
    assert dextypes.read_mutf8("\xc2\x80", 0, 1) == u"\x80"
    assert dextypes.read_mutf8("\xdf\xbf", 0, 1) == u"\u07ff"
    assert dextypes.read_mutf8("\xe0\xa0\x80", 0, 1) == u"\u0800"
    assert dextypes.read_mutf8("\xe8\x80\x80", 0, 1) == u"\u8000"
    assert dextypes.read_mutf8("\xef\xbf\xbf", 0, 1) == u"\uffff"
    
    try:
        dextypes.read_mutf8("\xf0", 0, 1)
    except ValueError:
        pass
    else:
        raise AssertionError("Call should fail.")
    
    try:
        dextypes.read_mutf8("\xc0\xc0", 0, 1)
    except ValueError:
        pass
    else:
        raise AssertionError("Call should fail.")
    
    try:
        dextypes.read_mutf8("\xe0\xc0", 0, 1)
    except ValueError:
        pass
    else:
        raise AssertionError("Call should fail.")
    
    try:
        dextypes.read_mutf8("\xe0\x80\xc0", 0, 1)
    except ValueError:
        pass
    else:
        raise AssertionError("Call should fail.")
    
    assert dextypes.write_mutf8(u"\x00") == "\xc0\x80"
    assert dextypes.write_mutf8(u"\x01") == "\x01"
    assert dextypes.write_mutf8(u"Hello") == "\x48\x65\x6c\x6c\x6f"
    assert dextypes.write_mutf8(u"\x7f") == "\x7f"
    assert dextypes.write_mutf8(u"\x80") == "\xc2\x80"
    assert dextypes.write_mutf8(u"\u07ff") == "\xdf\xbf"
    assert dextypes.write_mutf8(u"\u0800") == "\xe0\xa0\x80"
    assert dextypes.write_mutf8(u"\u8000") == "\xe8\x80\x80"
    assert dextypes.write_mutf8(u"\uffff") == "\xef\xbf\xbf"


if __name__ == "__main__":

    test_leb128()
    test_uleb128()
    test_mutf8()
