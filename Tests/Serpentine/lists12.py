__package__ = "com.example.lists12"

from java.lang import Object
from java.util import List
from Common.helper import TestActivity

class ListsActivity(TestActivity):

    __fields__ = {"l": List(Object)}
    
    def __init__(self):
    
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        self.l = []
        
        # Store subclasses of Object in the list.
        s1 = Sub()
        s2 = Sub()
        s3 = Sub()
        self.l.add(s1)
        self.l.add(s2)
        self.l.add(s3)
        
        i1 = self.l[0]
        i2 = self.l[1]
        i3 = self.l[2]
        
        actual = str(s1 == i1) + " " + str(s2 == i2) + " " + str(s3 == i3)
        expected = "true true true"
        
        self.showResult(expected, actual)


class Sub(Object):

    def __init__(self):
        Object.__init__(self)
