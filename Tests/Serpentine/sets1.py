__package__ = "com.example.sets1"

from java.util import Collections
import android.os
from Common.helper import TestActivity

class SetsActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    @args(void, [android.os.Bundle])
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        a = {1, 2, 3}
        
        l = []
        for i in a:
            l.add(i)
        
        Collections.sort(l)
        
        s = ""
        for i in l:
            s += str(i) + " "
        
        expected = "1 2 3 "
        self.showResult(expected, s)
