__package__ = "com.example.assignments6"

import android.os
from Common.helper import TestActivity

class AssignmentsActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    @args(void, [android.os.Bundle])
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        s = ""
        
        a = 123
        b = float(4.0)
        c = long(4294967296)
        d = -1.1e64
        
        s = str(a) + " " + str(b) + " " + str(c) + " " + str(d)
        
        self.showResult("123 4.0 4294967296 -1.1E64", s)
