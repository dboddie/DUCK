__package__ = "com.example.lists13"

from java.lang import Object
from java.util import List
from Common.helper import TestActivity

class ListsActivity(TestActivity):

    __fields__ = {"l": List(Object)}
    
    def __init__(self):
    
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        l = [1, 2, 3, 4]
        
        a = 2 in l
        b = 5 in l
        
        actual = str(a) + " " + str(b)
        expected = "true false"
        
        self.showResult(expected, actual)


class Sub(Object):

    def __init__(self):
        Object.__init__(self)
