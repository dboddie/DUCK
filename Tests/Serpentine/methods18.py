__package__ = "com.example.methods18"

from java.lang import Object, String
from android.os import Bundle

from Common.helper import TestActivity

class MethodsActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    @args(void, [Bundle])
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        s = self.fn()
        
        self.showResult("abc", s)
    
    # Declare that the method returns a String but return a CharSequence.
    @args(String, [])
    def fn(self):
    
        return String("abc").subSequence(0, 3).toString()
