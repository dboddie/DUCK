#!/usr/bin/env python

"""
Copyright (C) 2016 David Boddie <david@boddie.org.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import glob, os, shutil, subprocess, sys, tempfile

from DUCK.Tools import buildhelper

svg_icon_file = "icon.svg"
include_path = "Include"
res_files = {"drawable": {"ic_launcher": "icon.svg"}}
layout = None
features = []
permissions = []    

ignore_files = ("build.py", "buildall.py", "install.py", "testall.py",
                "uninstall.py")

def find_class_name(file_name):

    for line in open(file_name).readlines():
        c = line.find("class ")
        if c != -1 and "Activity)" in line:
            a = line.find("(", c + 6)
            class_name = line[c + 6:a].strip()
            return class_name
    
    return None

def make_temp_dir(temporary_directory):

    if temporary_directory is None:
        temp_dir = tempfile.mkdtemp()
    else:
        temp_dir = temporary_directory
        if not os.path.exists(temp_dir):
            os.mkdir(temp_dir)
    
    return temp_dir

def get_package_info(package_line, output_dir, app_name):

    package_name = package_line[package_line.find('"') + 1:package_line.rfind('"')]
    package_file_name = os.path.join(output_dir, app_name + ".apk")
    
    return package_name, package_file_name


if __name__ == "__main__":

    if not 4 <= len(sys.argv) <= 5:
        sys.stderr.write("Usage: %s <key file> <certificate file> "
            "<output directory> [<temporary directory>]\n" % sys.argv[0])
        sys.exit(1)
    
    key_file = sys.argv[1]
    cert_file = sys.argv[2]
    output_dir = sys.argv[3]
    screenshots_dir = os.path.join(output_dir, "screenshots")
    
    if len(sys.argv) == 5:
        temporary_directory = sys.argv[4]
    else:
        temporary_directory = None
    
    if not os.path.exists(output_dir):
        print "Creating", output_dir
        os.mkdir(output_dir)
        os.mkdir(screenshots_dir)
    
    this_dir = os.path.split(__file__)[0]
    names = os.listdir(this_dir)
    names.sort()
    
    common_build_py = os.path.join(this_dir, "build.py")
    
    to_install = []
    to_test = []
    to_uninstall = []
    
    for test_name in names:
    
        test_file = os.path.join(this_dir, test_name)
        
        if test_name in ignore_files:
            continue
        
        if os.path.isdir(test_file):
        
            subdir = test_file
            app_name = test_name
            
            build_py = os.path.join(subdir, "build.py")
            if not os.path.isfile(build_py):
                continue
            
            # Use simple checks to determine the name of the Activity class.
            for file_name in glob.glob(os.path.join(subdir, "*.py")):
            
                class_name = find_class_name(file_name)
                if class_name:
                    break
            else:
                sys.stderr.write("Failed to find the Activity class name for %s.\n" % test_name)
                sys.exit(1)
            
            package_line = filter(lambda x: x.startswith("package_name"),
                                            open(build_py).readlines())[0]
            
            temp_dir = make_temp_dir(temporary_directory)
            package_name, package_file_name = get_package_info(package_line, output_dir, app_name)
            build_args = [build_py, temp_dir, key_file, cert_file,
                          package_file_name]
        
        elif os.path.isfile(test_file):
        
            app_name, suffix = os.path.splitext(test_name)
            
            if suffix != ".py":
                continue
            
            class_name = find_class_name(test_file)
            if not class_name and not app_name.endswith("_fail"):
                sys.stderr.write("Failed to find the Activity class name for %s.\n" % test_name)
                sys.exit(1)
            
            package_line = filter(lambda line: "__package__" in line,
                                               open(test_file).readlines())[0]
            
            temp_dir = make_temp_dir(temporary_directory)
            package_name, package_file_name = get_package_info(package_line, output_dir, app_name)
            build_args = [common_build_py, test_file, temp_dir, key_file, cert_file,
                          package_file_name]
        else:
            continue
        
        if not app_name.endswith("_fail"):
            to_uninstall.append(package_name)
            to_install.append(app_name + ".apk")
        
        details = {}
        
        print "Building", test_name
        
        if subprocess.call(build_args) != 0:
            if app_name.endswith("_fail"):
                print "Failed as expected"
            else:
                sys.exit(1)
        elif app_name.endswith("_fail"):
            sys.exit(1)
        else:
            shutil.rmtree(temp_dir)
            if class_name is not None:
                to_test.append((package_name, class_name))
    
    install_file_name = os.path.join(output_dir, "install.py")
    testall_file_name = os.path.join(output_dir, "testall.py")
    uninstall_file_name = os.path.join(output_dir, "uninstall.py")
    
    f = open(install_file_name, "w")
    f.write(
        "import os\n"
        "import sys\n"
        "if '-r' in sys.argv[1:]:\n"
        "    reinstall = '-r '\n"
        "else:\n"
        "    reinstall = ''\n"
        "this_dir = os.path.abspath(os.curdir)\n"
        "os.chdir('" + output_dir + "')\n"
        "for name in " + repr(to_install) + ":\n"
        "    os.system('adb install ' + reinstall + name)\n"
        "\n"
        "os.chdir(this_dir)\n"
        )
    f.close()
    
    f = open(testall_file_name, "w")
    f.write(
        "import os, time\n"
        "output_dir = '" + screenshots_dir + "'\n"
        "if not os.path.exists(output_dir):\n"
        "    os.mkdir(output_dir)\n"
        "for package_name, class_name in " + repr(to_test) + ":\n"
        "    os.system('adb shell am start -n %s/.%s' % (package_name, class_name))\n"
        "    time.sleep(1)\n"
        "    image_file = os.path.join(output_dir, package_name)\n"
        "    os.system('adb shell screencap /sdcard/Download/%s.png' % package_name)\n"
        "    os.system('adb shell am force-stop %s' % package_name)\n"
        "    os.system('adb pull /sdcard/Download/%s.png %s' % (package_name, image_file))\n"
        "    os.system('adb shell rm /sdcard/Download/%s.png' % package_name)\n"
        "\n"
        )
    f.close()
    
    f = open(uninstall_file_name, "w")
    f.write(
        "import os\n"
        "for package_name in " + repr(to_uninstall) + ":\n"
        "    print 'Uninstalling', package_name\n"
        "    os.system('adb uninstall ' + package_name)\n"
        "\n"
        )
    f.close()
    
    print
    print "To install all tests, run 'python %s'" % install_file_name
    print "To run all tests, run 'python %s'" % testall_file_name
    print "To uninstall all tests, run 'python %s'" % uninstall_file_name
    
    sys.exit(0)
