__package__ = "com.example.imports6"

from Common.helper import TestActivity
from android.content import res

class ImportsActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        # If the example compiles then the test passed.
        self.showResult("", "")
