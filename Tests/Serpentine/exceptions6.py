__package__ = "com.example.exceptions6"

from java.lang import ArithmeticException, Exception
from android.app import Activity
import android.os
from android.widget import TextView

# Application classes

class ExceptionsActivity(Activity):

    def __init__(self):
    
        Activity.__init__(self)
    
    @args(void, [android.os.Bundle])
    def onCreate(self, bundle):
    
        Activity.onCreate(self, bundle)
        
        view = TextView(self)
        try:
            a = self.fn(0)
        except:
            view.setText("Caught exception")
        
        self.setContentView(view)
    
    @args(int, [int])
    def fn(self, value):
    
        if value == 0:
            raise ArithmeticException()
        
        return 1/value
