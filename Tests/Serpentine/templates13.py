__package__ = "com.example.templates13"

from java.util import List, Object, String
from android.os import Bundle
from Common.helper import TestActivity

class TemplatesActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    @args(void, [Bundle])
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        s = ""
        s += str(MyTemplate.plus(1, 2)) + " "
        s += str(MyTemplate.plus(1.0, 2)) + " "
        s += str(MyTemplate.plus(1, 2.0)) + " "
        s += str(MyTemplate.plus(1.0, 2.0))
        
        self.showResult("3 3.0 3 3.0", s)


# Define a template class that will be specialised depending on the types of
# the arguments passed to it.

class MyTemplate(Object):

    __replace__ = [Q]
    
    @static
    @args(Q, [Q, int])
    def plus(a, b):
    
        return a + b
