__package__ = "com.example.arrays6"

from android.app import Activity
from android.content import Context
from android.os import Bundle
from android.widget import TextView

# Application classes

class ArraysActivity(Activity):

    def __init__(self):
    
        Activity.__init__(self)
    
    @args(void, [Bundle])
    def onCreate(self, bundle):
    
        Activity.onCreate(self, bundle)
        
        # Create an array of integers.
        a = array([1,2,3])
        
        self.showOutput(a)
    
    @args(void, [[int]])
    def showOutput(self, a):
    
        view = TextView(self)
        l = len(a)
        text = str(l) + " items: "
        i = 0
        while i < l:
            text += str(a[i])
            i += 1
            if i != l:
                text += ","
        
        if text == "3 items: 1,2,3":
            text += "\nTest passed!"
        else:
            text += "\nTest failed"
        
        view.setText(text)
        self.setContentView(view)
