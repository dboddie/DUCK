__package__ = "com.example.if1"

from java.lang import String

from Common.helper import TestActivity

class IfActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        if True:
            s = "1"
        else:
            s = "2"
        
        self.showResult("1", s)
