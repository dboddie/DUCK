__package__ = "com.example.if8"

from java.lang import String

from Common.helper import TestActivity

class IfActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        a = 1
        if a == 1:
            s = 1
        else:
            s = byte(2)
        
        self.showResult("1", str(s))
