__package__ = "com.example.arrays7"

from java.lang import ArrayIndexOutOfBoundsException
from android.os import Bundle

from Common.helper import TestActivity

# Application classes

class ArraysActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    @args(void, [Bundle])
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        # Create an array of integers.
        a = array([1,2,3])
        
        try:
            s = str(a[3])
        except ArrayIndexOutOfBoundsException, e:
            s = str(e)
        
        self.showResult("java.lang.ArrayIndexOutOfBoundsException: length=3; index=3", s)
