__package__ = "com.example.operators3"

from java.lang import Integer, String
from android.app import Activity
from android.content import Context
from android.os import Bundle
from android.widget import TextView

# Application classes

class OperatorsActivity(Activity):

    def __init__(self):
    
        Activity.__init__(self)
    
    @args(void, [Bundle])
    def onCreate(self, bundle):
    
        Activity.onCreate(self, bundle)
        
        # Test that different primitive types can be combined using binary
        # operators.
        s = ""
        
        a = 640
        b = 480
        s += "a = 640\nb = 480\n"
        
        c = a/2 + b*0.5
        s += "a/2 + b*0.5 = "
        s += str(c)
        
        view = TextView(self)
        if str(c) == "560.0":
            view.setText("Test passed!")
        else:
            view.setText("Text failed:\n" + s)
        
        self.setContentView(view)
