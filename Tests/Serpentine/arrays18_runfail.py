__package__ = "com.example.arrays18"

from java.lang import Integer, String
from android.app import Activity
from android.content import Context
from android.os import Bundle
from android.util import Log
from android.widget import TextView

# Application classes

class ArraysActivity(Activity):

    def __init__(self):
    
        Activity.__init__(self)
    
    @args(void, [Bundle])
    def onCreate(self, bundle):
    
        Activity.onCreate(self, bundle)
        
        l = [1,2,3]         # a list of Integers (ints need to be wrapped)
        Log.i("******", str(len(l)))
        o = l.toArray()     # an array of Objects (actually Integers)
        Log.i("******", str(len(o)))
        i = 0
        while i < len(o):
            Log.i("******", str(o[i]))
            i += 1
        m = l.toArray(o)    # an array of ints (actually Integers)
        self.showOutput(o)
    
    @args(void, [[int]])
    def showOutput(self, a):
    
        view = TextView(self)
        text = str(len(a))
        self.setContentView(view)
