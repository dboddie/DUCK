__package__ = "com.example.templates4"

from java.util import Object
from android.os import Bundle
from Common.helper import TestActivity

class TemplatesActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    @args(void, [Bundle])
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        l = MyTemplate(123)
        m = MyTemplate("Hello world!")
        
        s = str(l.get()) + " "
        s += m.get()
        
        self.showResult("123 Hello world!", s)


# Define a template class. This will be used directly rather than via a
# specialised subclass that defines the item type used.

class MyTemplate(Object):

    __parameters__ = [E]
    
    @args(void, [E])
    def __init__(self, value):
    
        Object.__init__(self)
        self.value = value
    
    @args(E, [])
    def get(self):
    
        return self.value
