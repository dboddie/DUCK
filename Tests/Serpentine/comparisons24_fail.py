__package__ = "com.example.comparisons24"

from Common.helper import TestActivity

class ComparisonsActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        i = 2
        
        if i == 1:
            x = 123
        else:
            a = x
        
        s = str(x)
        
        self.showResult("124", s)
