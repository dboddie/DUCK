__package__ = "com.example.strings12"

from java.lang import String
from serpentine.strings import Strings

from Common.helper import TestActivity

class StringsActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        a = "Hello world! Hello Android!"
        l = a.split(" ")
        b = Strings.join(" ", l)
        
        self.showResult(a, b, True)
