__package__ = "com.example.methods13"

from java.lang import Object, String
from android.os import Bundle

from Common.helper import TestActivity

class MethodsActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    @args(void, [Bundle])
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        m = Middle()
        s = m.fn(123)
        s += m.fn(111, 333, 222)
        
        self.showResult("246222333444", s)


class Base(Object):

    def __init__(self):
        Object.__init__(self)
    
    @args(String, [int])
    def fn(self, i):
        return str(i)


class Middle(Base):

    def __init__(self):
        Base.__init__(self)
    
    @args(String, [int])
    def fn(self, i):
        return Base.fn(self, i * 2)
    
    @args(String, [int, int, int])
    def fn(self, i, j, k):
        return self.fn(i) + \
               Base.fn(self, j) + \
               Middle.fn(self, k)
