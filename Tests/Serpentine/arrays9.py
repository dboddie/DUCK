__package__ = "com.example.arrays9"

from android.os import Bundle

from Common.helper import TestActivity
from serpentine.arrays import Arrays

# Application classes

class ArraysActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    @args(void, [Bundle])
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        # Create an list of integers.
        l = [1, 3, 5, 7, 9]
        a = Arrays.create(l)
        
        s = ""
        for i in a:
            s += str(i) + " "
        
        self.showResult("1 3 5 7 9 ", s)
