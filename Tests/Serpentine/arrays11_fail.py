__package__ = "com.example.arrays11"

from Common.helper import TestActivity

class ArraysActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        # Create an array of integers.
        a = array([1, 3, 5])
        b = array([array([2, 4, 6]), a])
        
        s = str(0 in a) + " " + \
            str(1 in a) + " " + \
            str(2 in a) + " " + \
            str(a in a) + " " + \
            str(0 in b) + " " + \
            str(1 in b) + " " + \
            str(2 in b) + " " + \
            str(a in b) + " " + \
            str([2, 4, 6] in b) + " " + \
            str(array([2, 4, 6]) in b) + " " + \
            str(b in b)
        
        expected = "false true false false false false false true true false"
        
        self.showResult(expected, s)
