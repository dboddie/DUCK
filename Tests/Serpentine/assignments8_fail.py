__package__ = "com.example.assignments8"

import android.os
from Common.helper import TestActivity

class TuplesActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    @args(void, [android.os.Bundle])
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        # Slice assignment to a list.
        l = [1,2,3,4,5]
        l[1:4] = [-2,-3,-4]
        
        s = ""
        for i in l:
            s += str(i) + " "
        
        expected = "1 -2 -3 -4 5 "
        self.showResult(expected, s)
