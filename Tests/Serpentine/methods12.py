__package__ = "com.example.methods12"

from java.lang import Object, String
from android.os import Bundle

from Common.helper import TestActivity

class MethodsActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    @args(void, [Bundle])
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        m = Middle()
        s = m.fn(333)
        s += m.fn(555, 777)
        
        self.showResult("333555777", s)


class Base(Object):

    def __init__(self):
        Object.__init__(self)
    
    @args(String, [int])
    def fn(self, i):
        return str(i)


class Middle(Base):

    def __init__(self):
        Base.__init__(self)
    
    @args(String, [int])
    def fn(self, i):
        return Base.fn(self, i)
    
    @args(String, [int, int])
    def fn(self, i, j):
        return Base.fn(self, i) + Base.fn(self, j)
