__package__ = "com.example.expressions3"

from java.lang import String
from android.os import Bundle

from Common.helper import TestActivity

# Application classes

class MethodsActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    @args(void, [Bundle])
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        s = str((3 - (123 - 121)) + 1)
        
        self.showResult("2", s)
