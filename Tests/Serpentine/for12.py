__package__ = "com.example.for12"

from java.lang import Integer
import android.os
from Common.helper import TestActivity

class ForActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    @args(void, [android.os.Bundle])
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        self.a = 0
        s = ""
        
        for self.a in array([1, 2, 3, 4, 5]):
            s += str(self.a) + " "
        
        s += str(self.a)
        
        expected = "1 2 3 4 5 5"
        self.showResult(expected, s)
