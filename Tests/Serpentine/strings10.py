__package__ = "com.example.strings10"

from java.lang import String

from Common.helper import TestActivity

class StringsActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        a = "Hello world!"
        b = a[:15]
        
        expected = "Hello world!"
        
        self.showResult(expected, b)
