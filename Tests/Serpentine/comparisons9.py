__package__ = "com.example.comparisons9"

import android.os
from Common.helper import TestActivity

# Application classes

class ComparisonsActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        i = 1 == 1
        s = ""
        
        if i == True:
            s = "Hello world!"
        else:
            s = "This text should never appear!"
        
        self.showResult("Hello world!", s)
