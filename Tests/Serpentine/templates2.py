__package__ = "com.example.templates2"

from java.util import Object
from android.app import Activity
from android.content import Context
from android.os import Bundle
from android.widget import TextView

# Application classes

class TemplatesActivity(Activity):

    def __init__(self):
    
        Activity.__init__(self)
    
    @args(void, [Bundle])
    def onCreate(self, bundle):
    
        Activity.onCreate(self, bundle)
        
        l = MyTemplate("Test passed!")
        
        textView = TextView(self)
        textView.setText(l.get())
        
        self.setContentView(textView)


# Define a template class. This will be used directly rather than via a
# specialised subclass that defines the item type used.

class MyTemplate(Object):

    __parameters__ = [E]
    
    @args(void, [E])
    def __init__(self, value):
    
        Object.__init__(self)
        self.value = value
    
    @args(E, [])
    def get(self):
    
        return self.value
