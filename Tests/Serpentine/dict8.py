__package__ = "com.example.dict8"

from Common.helper import TestActivity

class DictActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        d = {0: "Alice", 1: "Bob", 3: "Carol"}
        
        s = ""
        i = 0
        while i < 4:
        
            try:
                s += d[i] + "\n"
            except KeyError:
                s += "???\n"
            
            i += 1
        
        expected = "Alice\nBob\n???\nCarol\n"
        self.showResult(expected, s, True)
