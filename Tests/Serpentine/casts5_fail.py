# Tests generation of bytecode for constants of specific types.
# Use the DUCK_LISTING environment variable when building to create a listing
# for inspection.

__package__ = "com.example.casts5"

from java.lang import Integer
from Common.helper import TestActivity

# Application classes

class CastsActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        i = Integer(123)
        j = int(i)
        
        s = str(i) + " " + str(j)
        
        expected = "123 123"
        
        self.showResult(expected, s)
