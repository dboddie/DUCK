__package__ = "com.example.for9"

from java.lang import Integer
import android.os
from Common.helper import TestActivity

class ForActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    @args(void, [android.os.Bundle])
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        a = [[1, 1], [2, 4], [3, 9], [4, 16], [5, 25]]
        s = ""
        
        for i, j in a:
            s += str(i) + " " + str(j) + "\n"
        
        expected = "1 1\n2 4\n3 9\n4 16\n5 25\n"
        self.showResult(expected, s, True)
