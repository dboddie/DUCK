__package__ = "com.example.assignments7"

import android.os
from Common.helper import TestActivity

class AssignmentsActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    @args(void, [android.os.Bundle])
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        s = ""
        
        a = 123
        b = float(4.0)
        c = long(4294967296)
        d = -1.1e64
        
        # Some of these assignments reduce the precision of the values.
        x = a   # integer
        y = b   # float
        
        a = d   # double to integer
        b = c   # long to float
        c = y   # float to long
        d = x   # integer to double
        
        s = str(a) + " " + str(b) + " " + str(c) + " " + str(d)
        
        self.showResult("-2147483648 4.2949673E9 4 123.0", s)
