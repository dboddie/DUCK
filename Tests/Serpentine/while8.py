__package__ = "com.example.while8"

from Common.helper import TestActivity

class WhileActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        i = 0
        while i < 10:
            x = i
            i = i + 1
        else:
            x = 10
        
        s = str(x)
        
        expected = "10"
        
        self.showResult(expected, s)
