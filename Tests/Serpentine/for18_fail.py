__package__ = "com.example.for18"

from Common.helper import TestActivity

class ForActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        s = ""
        l = [0, 1, 2, 3, 4, 5]
        
        for i in l:
            s += str(i) + " "
        
        # The item variable is not defined outside the loop.
        s += str(i)
        
        expected = "0 1 2 3 4 5 5"
        self.showResult(expected, s)
