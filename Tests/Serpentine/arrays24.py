__package__ = "com.example.arrays24"

from java.util import String
from serpentine.arrays import Arrays
from Common.helper import TestActivity

class ArraysActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        b = array(int, 3, 3)
        
        for i in range(3):
            for j in range(3):
                b[i][j] = i + j
        
        c = b[1]
        
        expected = "1 2 3 "
        s = ""
        
        for i in range(3):
            s += str(c[i]) + " "
        
        self.showResult(expected, s, True)
