__package__ = "com.example.dict3"

from Common.helper import TestActivity

class DictActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        keys = ["Aa", "Bb", "Cc"]
        values = ["Alice", "Bob", "Carol"]
        d = dict(keys, values)
        
        s = ""
        for key in keys:
            s += key + ": " + d[key] + "\n"
        
        expected = "Aa: Alice\nBb: Bob\nCc: Carol\n"
        self.showResult(expected, s, True)
