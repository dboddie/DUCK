__package__ = "com.example.min1"

from java.lang import Math
from Common.helper import TestActivity

class MinActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        i = Math.min(1, 2)
        j = Math.min(3.0, 2)
        k = Math.min(long(100), 101)
        l = Math.min(1000, double(999))
        s = str(i) + " " + str(j) + " " + str(k) + " " + str(l)
        
        expected = "1 2 100 999"
        self.showResult(expected, s, True)
