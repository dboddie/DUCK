__package__ = "com.example.expressions6"

from java.lang import String
from android.os import Bundle

from Common.helper import TestActivity

# Application classes

class MethodsActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    @args(void, [Bundle])
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        s = str(self.fn(self.fn(255, self.fn(265, 133)), 121))
        
        self.showResult("2", s)
    
    @args(int, [int, int])
    def fn(self, x, y):
    
        return x - y
