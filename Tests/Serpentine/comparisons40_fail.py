__package__ = "com.example.comparisons40"

from Common.helper import TestActivity

class ComparisonsActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        i = 2
        l = [123, 124]
        
        if i == 1:
            x = 123
        else:
            if x == i:
                a = 1
        
        self.showResult("123", "123")
