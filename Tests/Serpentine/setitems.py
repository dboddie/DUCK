__package__ = "com.example.setitems"

from java.lang import Integer, String
from java.util import ArrayList
from android.app import Activity
import android.os
from android.widget import TextView

# Application classes

class IntegerList(ArrayList):

    __item_types__ = [Integer]
    
    def __init__(self):
        ArrayList.__init__(self)


class ArraysActivity(Activity):

    __interfaces__ = []
    
    def __init__(self):
    
        Activity.__init__(self)
    
    @args(void, [android.os.Bundle])
    def onCreate(self, bundle):
    
        Activity.onCreate(self, bundle)
        
        numbers = IntegerList()
        numbers.add(Integer(68))
        numbers.add(Integer(85))
        numbers.add(Integer(67))
        numbers.add(Integer(75))
        
        numbers.set(0, Integer(123))
        numbers[1] = Integer(456)
        
        a = numbers.toArray()
        a[2] = Integer(789)
        
        view = TextView(self)
        
        s = ""
        i = 0
        while i < len(a):
            s = s.concat(numbers[i].toString())
            s = s.concat(" ")
            i += 1
        
        s = s.concat("\n")
        
        i = 0
        while i < len(a):
            s = s.concat(CAST(a[i], Integer).toString())
            s = s.concat(" ")
            i += 1
        
        if s.equals("123 456 67 75 \n123 456 789 75 "):
            s = s.concat("\nTest passed!")
        else:
            s = s.concat("\nTest failed!")
        
        view.setText(s)
        self.setContentView(view)
