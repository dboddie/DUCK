__package__ = "com.example.comparisons41"

from Common.helper import TestActivity

class ComparisonsActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        self.x = 0
        
        # If short-circuiting works, only the first method call should occur.
        i = self.fn() and self.fn()
        
        self.showResult(str(self.x), "1")
    
    @args(bool, [])
    def fn(self):
    
        self.x += 1
        return False
