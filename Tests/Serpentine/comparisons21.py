__package__ = "com.example.comparisons21"

from java.lang import String
from Common.helper import TestActivity

class ComparisonsActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        i = 1
        
        if i == 1:
            a = 1
            s = String("Equal to 1\n")
        else:
            s = String("Not equal to 1\n")
            b = 2
        
        t = ""
        
        self.showResult("Equal to 1\n", s)
