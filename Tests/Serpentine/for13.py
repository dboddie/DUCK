__package__ = "com.example.for13"

from java.util import Collections
import android.os
from Common.helper import TestActivity

class ForActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    @args(void, [android.os.Bundle])
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        s = ""
        m = {"abc", "def", "ghi"}
        m.remove("abc")
        
        keys = []
        
        for key in m:
            keys.add(key)
        
        Collections.sort(keys)
        for key in keys:
            s += key
        
        expected = "defghi"
        self.showResult(expected, s)
