__package__ = "com.example.strings2"

from java.lang import Object
from android.app import Activity
import android.os
from android.widget import TextView

# Application classes

class StringsActivity(Activity):

    __interfaces__ = []
    
    def __init__(self):
    
        Activity.__init__(self)
    
    @args(void, [android.os.Bundle])
    def onCreate(self, bundle):
    
        Activity.onCreate(self, bundle)
        
        view = TextView(self)
        output = ""
        
        # Construct strings from existing strings rather than assign strings
        # directly. This results in different String objects rather than two
        # variables that refer to the same one.
        a = "Hello" + " world"
        b = "Hello" + " world"
        
        if a == b:
            output += a + " == " + b + "\n"
        else:
            output += a + " != " + b + "\n"
        
        a = "Hello"
        b = "world"
        
        if a == b:
            output += a + " == " + b + "\n"
        else:
            output += a + " != " + b + "\n"
        
        if a <= b:
            output += a + " <= " + b + "\n"
        if a < b:
            output += a + " < " + b + "\n"
        if a >= b:
            output += a + " >= " + b + "\n"
        if a > b:
            output += a + " < " + b + "\n"
        
        if b <= a:
            output += b + " <= " + a + "\n"
        if b < a:
            output += b + " < " + a + "\n"
        if b >= a:
            output += b + " >= " + a + "\n"
        if b > a:
            output += b + " < " + a + "\n"
        
        view.setText(output)
        self.setContentView(view)
