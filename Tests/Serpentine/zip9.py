__package__ = "com.example.zip9"

from java.lang import String
from java.util import ArrayList, List
from Common.helper import TestActivity

class Strings(ArrayList):

    __item_types__ = [String]
    
    def __init__(self):
        ArrayList.__init__(self)


class ZipActivity(TestActivity):

    __fields__ = {
        "numbers": List(int),
        "stringlists": List(Strings)
        }
    
    def __init__(self):
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        self.numbers = [1, 2, 3]
        strings = Strings()
        strings.add("Aa")
        strings.add("Bb")
        strings.add("Cc")
        
        self.stringlists = [strings, strings, strings]
        
        s = self.getString()
        
        expected = "1 Aa Bb Cc \n2 Aa Bb Cc \n3 Aa Bb Cc \n"
        self.showResult(expected, s, True)
    
    @args(String, [])
    def getString(self):
    
        z = zip(self.numbers, self.stringlists)
        s = ""
        
        for p in z:
            s += str(p.first()) + " "
            for i in p.second():
                s += i + " "
            s += "\n"
        
        return s
