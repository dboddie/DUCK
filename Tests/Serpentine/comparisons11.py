__package__ = "com.example.comparisons11"

import android.os
from Common.helper import TestActivity

class ComparisonsActivity(TestActivity):

    __interfaces__ = []
    
    def __init__(self):
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        t = [# Integer and double
             [(1 == 1.0), (1.0 == 1), (1 != 1.0), (1.0 != 1),
              (1 < 1.0), (1.0 < 1), (1 > 1.0), (1.0 > 1),
              (1 <= 1.0), (1.0 <= 1), (1 >= 1.0), (1.0 >= 1),
              (1 == 2.0), (2.0 == 1), (1 != 2.0), (2.0 != 1),
              (1 < 2.0), (2.0 < 1), (1 > 2.0), (2.0 > 1),
              (1 <= 2.0), (2.0 <= 1), (1 >= 2.0), (2.0 >= 1),
              (1.0 == 2), (2 == 1.0), (1.0 != 2), (2 != 1.0),
              (1.0 < 2), (2 < 1.0), (1.0 > 2), (2 > 1.0),
              (1.0 <= 2), (2 <= 1.0), (1.0 >= 2), (2 >= 1.0)],
             # Integer and short
             [(1 == short(1.0)), (short(1.0) == 1), (1 != short(1.0)), (short(1.0) != 1),
              (1 < short(1.0)), (short(1.0) < 1), (1 > short(1.0)), (short(1.0) > 1),
              (1 <= short(1.0)), (short(1.0) <= 1), (1 >= short(1.0)), (short(1.0) >= 1),
              (1 == short(2.0)), (short(2.0) == 1), (1 != short(2.0)), (short(2.0) != 1),
              (1 < short(2.0)), (short(2.0) < 1), (1 > short(2.0)), (short(2.0) > 1),
              (1 <= short(2.0)), (short(2.0) <= 1), (1 >= short(2.0)), (short(2.0) >= 1),
              (short(1.0) == 2), (2 == short(1.0)), (short(1.0) != 2), (2 != short(1.0)),
              (short(1.0) < 2), (2 < short(1.0)), (short(1.0) > 2), (2 > short(1.0)),
              (short(1.0) <= 2), (2 <= short(1.0)), (short(1.0) >= 2), (2 >= short(1.0))],
             # Integer and byte
             [(1 == byte(1.0)), (byte(1.0) == 1), (1 != byte(1.0)), (byte(1.0) != 1),
              (1 < byte(1.0)), (byte(1.0) < 1), (1 > byte(1.0)), (byte(1.0) > 1),
              (1 <= byte(1.0)), (byte(1.0) <= 1), (1 >= byte(1.0)), (byte(1.0) >= 1),
              (1 == byte(2.0)), (byte(2.0) == 1), (1 != byte(2.0)), (byte(2.0) != 1),
              (1 < byte(2.0)), (byte(2.0) < 1), (1 > byte(2.0)), (byte(2.0) > 1),
              (1 <= byte(2.0)), (byte(2.0) <= 1), (1 >= byte(2.0)), (byte(2.0) >= 1),
              (byte(1.0) == 2), (2 == byte(1.0)), (byte(1.0) != 2), (2 != byte(1.0)),
              (byte(1.0) < 2), (2 < byte(1.0)), (byte(1.0) > 2), (2 > byte(1.0)),
              (byte(1.0) <= 2), (2 <= byte(1.0)), (byte(1.0) >= 2), (2 >= byte(1.0))],
             # Integer and char
             [(1 == char(1.0)), (char(1.0) == 1), (1 != char(1.0)), (char(1.0) != 1),
              (1 < char(1.0)), (char(1.0) < 1), (1 > char(1.0)), (char(1.0) > 1),
              (1 <= char(1.0)), (char(1.0) <= 1), (1 >= char(1.0)), (char(1.0) >= 1),
              (1 == char(2.0)), (char(2.0) == 1), (1 != char(2.0)), (char(2.0) != 1),
              (1 < char(2.0)), (char(2.0) < 1), (1 > char(2.0)), (char(2.0) > 1),
              (1 <= char(2.0)), (char(2.0) <= 1), (1 >= char(2.0)), (char(2.0) >= 1),
              (char(1.0) == 2), (2 == char(1.0)), (char(1.0) != 2), (2 != char(1.0)),
              (char(1.0) < 2), (2 < char(1.0)), (char(1.0) > 2), (2 > char(1.0)),
              (char(1.0) <= 2), (2 <= char(1.0)), (char(1.0) >= 2), (2 >= char(1.0))],
             # Integer and float
             [(1 == float(1.0)), (float(1.0) == 1), (1 != float(1.0)), (float(1.0) != 1),
              (1 < float(1.0)), (float(1.0) < 1), (1 > float(1.0)), (float(1.0) > 1),
              (1 <= float(1.0)), (float(1.0) <= 1), (1 >= float(1.0)), (float(1.0) >= 1),
              (1 == float(2.0)), (float(2.0) == 1), (1 != float(2.0)), (float(2.0) != 1),
              (1 < float(2.0)), (float(2.0) < 1), (1 > float(2.0)), (float(2.0) > 1),
              (1 <= float(2.0)), (float(2.0) <= 1), (1 >= float(2.0)), (float(2.0) >= 1),
              (float(1.0) == 2), (2 == float(1.0)), (float(1.0) != 2), (2 != float(1.0)),
              (float(1.0) < 2), (2 < float(1.0)), (float(1.0) > 2), (2 > float(1.0)),
              (float(1.0) <= 2), (2 <= float(1.0)), (float(1.0) >= 2), (2 >= float(1.0))],
             # Integer and long
             [(1 == long(1)), (long(1) == 1), (1 != long(1)), (long(1) != 1),
              (1 < long(1)), (long(1) < 1), (1 > long(1)), (long(1) > 1),
              (1 <= long(1)), (long(1) <= 1), (1 >= long(1)), (long(1) >= 1),
              (1 == long(2)), (long(2) == 1), (1 != long(2)), (long(2) != 1),
              (1 < long(2)), (long(2) < 1), (1 > long(2)), (long(2) > 1),
              (1 <= long(2)), (long(2) <= 1), (1 >= long(2)), (long(2) >= 1),
              (long(1) == 2), (2 == long(1)), (long(1) != 2), (2 != long(1)),
              (long(1) < 2), (2 < long(1)), (long(1) > 2), (2 > long(1)),
              (long(1) <= 2), (2 <= long(1)), (long(1) >= 2), (2 >= long(1))]]
        
        results = (
            True, True, False, False,
            False, False, False, False,
            True, True, True, True,
            False, False, True, True,
            True, False, False, True,
            True, False, False, True,
            False, False, True, True,
            True, False, False, True,
            True, False, False, True,
            )
        
        s = ""
        for tests in t:
            i = 0
            for result in tests:
                if result != results[i]:
                    s += "Not OK\n"
                    break
                i += 1
            else:
                s += "OK\n"
        
        expected = "OK\nOK\nOK\nOK\nOK\nOK\n"
        
        self.showResult(expected, s)
