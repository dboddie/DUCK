__package__ = "com.example.strings11"

from java.lang import String, Object

from Common.helper import TestActivity

class StringsActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        s = str(Object()) + "\n"
        s += str(123) + "\n"
        s += str(123.456) + "\n"
        
        # Automatically pass but show the output.
        self.showResult(s, s, True)
