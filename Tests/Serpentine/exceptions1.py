__package__ = "com.example.exceptions1"

from java.lang import ArithmeticException, Exception
from android.app import Activity
import android.os
from android.widget import TextView

# Application classes

class ExceptionsActivity(Activity):

    def __init__(self):
    
        Activity.__init__(self)
    
    @args(void, [android.os.Bundle])
    def onCreate(self, bundle):
    
        Activity.onCreate(self, bundle)
        
        view = TextView(self)
        try:
            a = 1/0
        except (Exception, ArithmeticException), e:
            view.setText("Caught " + str(e))
        except:
            view.setText("Catch all")
        
        self.setContentView(view)
