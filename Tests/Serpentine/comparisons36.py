__package__ = "com.example.comparisons36"

from Common.helper import TestActivity

class ComparisonsActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        a = 0 == 0 == 1
        i = 1
        j = 2
        k = 3
        b = i < j < k
        
        s = str(a) + " " + str(b)
        expected = "false true"
        self.showResult(expected, s)
