__package__ = "com.example.arrays8"

from java.lang import ArrayIndexOutOfBoundsException, Integer
from android.os import Bundle

from Common.helper import TestActivity

# Application classes

class ArraysActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    @args(void, [Bundle])
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        # Create an array of integers.
        a = array([Integer(1), Integer(2), Integer(3)])
        
        try:
            s = str(a[1])
        except ArrayIndexOutOfBoundsException, e:
            s = str(e)
        
        self.showResult("2", s)
