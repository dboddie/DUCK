__package__ = "com.example.dict4"

from Common.helper import TestActivity

class DictActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        keys = array([1, 2, 3])
        values = array(["Aa", "Bb", "Cc"])
        d = dict(keys, values)
        
        s = ""
        for key in keys:
            s += str(key) + ": " + d[key] + "\n"
        
        expected = "1: Aa\n2: Bb\n3: Cc\n"
        self.showResult(expected, s, True)
