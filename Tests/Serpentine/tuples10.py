__package__ = "com.example.tuples10"

from java.lang import String
from Common.helper import TestActivity

class TuplesActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        s = self.getTuple(array([1, 2]))
        
        expected = "1 2"
        self.showResult(expected, s)
    
    @args(String, [[int]])
    def getTuple(self, t):
    
        a, b = t
        return str(a) + " " + str(b)
