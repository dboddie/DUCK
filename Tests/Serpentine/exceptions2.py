__package__ = "com.example.exceptions2"

from java.lang import ArithmeticException, Exception
from android.app import Activity
import android.os
from android.widget import TextView

# Application classes

class ExceptionsActivity(Activity):

    def __init__(self):
    
        Activity.__init__(self)
    
    @args(void, [android.os.Bundle])
    def onCreate(self, bundle):
    
        Activity.onCreate(self, bundle)
        
        view = TextView(self)
        text = ""
        
        i = -5
        while i <= 5:
        
            try:
                a = 100/i
                text += "1/" + str(i) + " = " + str(a) + "\n"
            
            except ArithmeticException, e:
                text += "1/" + str(i) + " -> " + str(e) + "\n"
                pass
            
            i += 1
        
        view.setText(text)
        self.setContentView(view)
