__package__ = "com.example.templates1"

from java.util import Object, String
from android.app import Activity
from android.content import Context
from android.os import Bundle
from android.widget import TextView

# Application classes

class TemplatesActivity(Activity):

    def __init__(self):
    
        Activity.__init__(self)
    
    @args(void, [Bundle])
    def onCreate(self, bundle):
    
        Activity.onCreate(self, bundle)
        
        l = Specialised("Test passed!")
        
        textView = TextView(self)
        textView.setText(l.get())
        
        self.setContentView(textView)


# Define a template class.

class MyTemplate(Object):

    __parameters__ = [E]
    
    @args(void, [E])
    def __init__(self, value):
    
        Object.__init__(self)
        self.value = value
    
    @args(E, [])
    def get(self):
    
        return self.value


# Define a specialised version of the template class that defines the item
# type used.

class Specialised(MyTemplate):

    __item_types__ = [String]
    
    @args(void, [E])
    def __init__(self, value):
    
        MyTemplate.__init__(self, value)
