__package__ = "com.example.for20"

from Common.helper import TestActivity

class ForActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        s = ""
        l = [0, 1, 2, 3, 4, 5]
        m = [0.0, 2.25, 5.5, 7.75, 10.0]
        
        i = 0
        
        for i in l:
            s += str(i) + " "
        
        # Because we reuse the i variable, the doubles will be cast to integers.
        for i in m:
            s += str(i) + " "
        
        expected = "0 1 2 3 4 5 0 2 5 7 10 "
        self.showResult(expected, s)
