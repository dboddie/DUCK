__package__ = "com.example.interfaces1"

from java.lang import Object
import android.os
from Common.helper import TestActivity

class ValueInterface:

    @args(int, [int])
    def getValue(self, x):
        pass


class Sequencer(Object):

    __fields__ = {"valueProvider": ValueInterface}
    
    def __init__(self):
        Object.__init__(self)
    
    @args(int, [])
    def getResult(self):
    
        total = 0
        i = 0
        while i < 10:
            total += self.valueProvider.getValue(i)
            i += 1
        
        return total
    
    @args(void, [ValueInterface])
    def setValueProvider(self, provider):
    
        self.valueProvider = provider


class InterfacesActivity(TestActivity):

    __interfaces__ = [ValueInterface]
    
    def __init__(self):
        TestActivity.__init__(self)
    
    @args(void, [android.os.Bundle])
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        sequencer = Sequencer()
        sequencer.setValueProvider(self)
        
        s = str(sequencer.getResult())
        
        expected = "90"
        self.showResult(expected, s)
    
    def getValue(self, x):
    
        return x * 2
