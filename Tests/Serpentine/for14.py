__package__ = "com.example.for14"

import android.os
from Common.helper import TestActivity

class ForActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    @args(void, [android.os.Bundle])
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        s = ""
        m = ("abc", "def", "ghi")
        m.remove("abc")
        
        for key in m:
            s += key
        
        expected = "defghi"
        self.showResult(expected, s)
