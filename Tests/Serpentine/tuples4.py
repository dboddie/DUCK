__package__ = "com.example.tuples4"

from Common.helper import TestActivity

class TuplesActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        a = [123]
        a[0], b = 1, 2
        s = str(a) + " " + str(b) + " "
        
        a[0], b = b, a[0]
        s += str(a) + " " + str(b)
        
        expected = "[1] 2 [2] 1"
        self.showResult(expected, s, True)
