__package__ = "com.example.imports8"

from Common.helper import TestActivity
from android.os import Bundle

# This test should fail because android.os should not be imported as a side
# effect of importing the SystemClock class.

class ImportsActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        # If the example compiles then the test passed.
        t = android.os.SystemClock.elapsedRealtime()
        self.showResult("", "")
