__package__ = "com.example.pairs2"

from serpentine.collections import Pair
from Common.helper import TestActivity

class PairsActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        p = Pair(1, "Hello")
        l = []
        l.add(p)
        
        q = l[0]
        s = str(q.first()) + " " + q.second()
        
        expected = "1 Hello"
        self.showResult(expected, s, True)
