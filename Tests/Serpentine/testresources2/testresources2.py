__package__ = "com.example.testresources2"

from java.lang import String
from android.app import Activity
import android.os
from android.widget import TextView

import app_resources

class TestResourcesActivity(Activity):

    def __init__(self):
    
        Activity.__init__(self)
    
    @args(void, [android.os.Bundle])
    def onCreate(self, bundle):
    
        Activity.onCreate(self, bundle)
        
        view = TextView(self)
        a = app_resources.R.string.test_string
        view.setText(self.getResources().getString(a))
        
        self.setContentView(view)
