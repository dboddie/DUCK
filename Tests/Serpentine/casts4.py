# Tests generation of bytecode for constants of specific types.
# Use the DUCK_LISTING environment variable when building to create a listing
# for inspection.

__package__ = "com.example.casts4"

from java.lang import Byte, Short
from Common.helper import TestActivity

# Application classes

class CastsActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        # Create a short from a constant and use it to create a Short object.
        # If the value used to initialise the register containing the short
        # primitive value is incorrect then the resulting register type will be
        # incorrect for the Short.__init__ call and the virtual machine will
        # raise an exception.
        sh1 = Short(short(-16384))
        sh2 = Short(short(16384))
        b1 = Byte(byte(-64))
        b2 = Byte(byte(64))
        
        s = str(sh1) + " " + str(sh2) + " " + str(b1) + " " + str(b2)
        
        expected = "-16384 16384 -64 64"
        
        self.showResult(expected, s)
