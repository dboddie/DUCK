__package__ = "com.example.maps8"

from android.app import Activity
from android.content import Context
from android.os import Bundle
from android.widget import TextView

# Application classes

class MapsActivity(Activity):

    def __init__(self):
    
        Activity.__init__(self)
    
    @args(void, [Bundle])
    def onCreate(self, bundle):
    
        Activity.onCreate(self, bundle)
        
        a = {"ABC": 01234}
        a["XYZ"] = 1234
        
        d = {}
        d["Hello"] = 123
        
        view = TextView(self)
        if d["Hello"] == 123:
            view.setText("Test passed!\n" + d.toString())
        else:
            view.setText("Test failed: " + str(d["Hello"]))
        
        self.setContentView(view)
