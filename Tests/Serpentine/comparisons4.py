__package__ = "com.example.comparisons4"

from android.app import Activity
import android.os
from android.widget import TextView

# Application classes

class ComparisonsActivity(Activity):

    __interfaces__ = []
    
    def __init__(self):
    
        Activity.__init__(self)
    
    @args(void, [android.os.Bundle])
    def onCreate(self, bundle):
    
        Activity.onCreate(self, bundle)
        
        view = TextView(self)
        
        i = 1 < 2 and -1 < 1
        
        if i == True:
            view.setText("Hello world!")
        else:
            view.setText("This text should never appear!")
        
        self.setContentView(view)
