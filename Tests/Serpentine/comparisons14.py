__package__ = "com.example.comparisons14"

import android.os

from Common.helper import TestActivity

class ComparisonsActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        s = ""
        
        if False and True:
            s = "False and True -> True"
        else:
            s = "False and True -> False"
        
        s += "\n"
        
        if False or True:
            s += "False or True -> True"
        else:
            s += "False or True -> False"
        
        expected = "False and True -> False\nFalse or True -> True"
        actual = s
        self.showResult(expected, actual)
