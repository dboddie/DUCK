__package__ = "com.example.for15"

import android.os
from Common.helper import TestActivity

class ForActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    @args(void, [android.os.Bundle])
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        s = ""
        m = ("abc", "def", "ghi")
        
        for key in m:
            if key == "def":
                break
            s += key
        
        expected = "abc"
        self.showResult(expected, s)
