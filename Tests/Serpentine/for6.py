__package__ = "com.example.for6"

from android.app import Activity
import android.os
from android.widget import TextView

# Application classes

class ForActivity(Activity):

    def __init__(self):
        Activity.__init__(self)
    
    @args(void, [android.os.Bundle])
    def onCreate(self, bundle):
    
        Activity.onCreate(self, bundle)
        
        view = TextView(self)
        s = ""
        l = [0, 1, 2, 3, 4, 5]
        for i in l:
            s += str(i) + " "
        else:
            s += str(i)
        
        expected = "0 1 2 3 4 5 5"
        if s == expected:
            view.setText("Test passed!")
        else:
            view.setText("Test failed:\nExpected:\n" + expected + "\nObtained:\n" + s)
        
        self.setContentView(view)
