__package__ = "com.example.methods5"

from java.lang import Object, String
from android.os import Bundle

from Common.helper import TestActivity

# Application classes

class MethodsActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    @args(void, [Bundle])
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        s = Base.fn(123)        # static method call
        o = Base()
        s += o.fn(456)          # calling a static method via an instance
        
        m = Middle()
        n = Middle(-1)
        s += m.fn(333)          # instance method calls
        s += n.fn(555)
        
        self.showResult("123456333555", s)


class Base(Object):

    def __init__(self):
        Object.__init__(self)
    
    @static
    @args(String, [int])
    def fn(i):
        return str(i)


class Middle(Base):

    def __init__(self):
        Base.__init__(self)
    
    @args(void, [int])
    def __init__(self, i):
        Base.__init__(self)
