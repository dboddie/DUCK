__package__ = "com.example.maps4"

from java.lang import Integer, String
from java.util import Collection, HashMap, HashSet
from android.app import Activity
from android.content import Context
from android.os import Bundle
from android.widget import TextView

# Application classes

class StringIntegerMap(HashMap):

    __item_types__ = [String, Integer]
    
    def __init__(self):
        HashMap.__init__(self)


class MapsActivity(Activity):

    def __init__(self):
    
        Activity.__init__(self)
    
    @args(void, [Bundle])
    def onCreate(self, bundle):
    
        Activity.onCreate(self, bundle)
        
        numbers = StringIntegerMap()
        numbers.put("Hello", Integer(123))
        numbers["Hello"] = Integer(456)
        
        entries = numbers.entrySet()
        it = entries.iterator()
        entry = it.next()
        t = Integer(123)
        t = entry.getValue()
        
        text = ""
        
        if numbers.get("Hello").intValue() != 456:
            text = "Failed (numbers)"
        else:
            text = "Test passed!"
        
        view = TextView(self)
        view.setText(text)
        
        self.setContentView(view)
