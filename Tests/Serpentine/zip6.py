__package__ = "com.example.zip6"

from Common.helper import TestActivity

class ZipActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        l1 = array([1, 2, 3])
        l2 = array(["Aa", "Bb", "Cc"])
        z = zip(l1, l2)
        s = ""
        
        for p in z:
            s += str(p.first()) + " " + p.second() + "\n"
        
        expected = "1 Aa\n2 Bb\n3 Cc\n"
        self.showResult(expected, s, True)
