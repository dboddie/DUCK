__package__ = "com.example.comparisons10"

import android.os
from Common.helper import TestActivity

# Application classes

class ComparisonsActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        s = ""
        
        i = 0
        while i < 3:
        
            if i < 1:
                s += "Less than 1\n"
            elif i == 1:
                s += "Equals 1\n"
            else:
                s += "Greater than 1\n"
            
            i += 1
        
        self.showResult("Less than 1\nEquals 1\nGreater than 1\n", s)
