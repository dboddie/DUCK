__package__ = "com.example.comparisons7"

from android.app import Activity
import android.os
from android.widget import TextView

# Application classes

class ComparisonsActivity(Activity):

    __interfaces__ = []
    
    def __init__(self):
    
        Activity.__init__(self)
    
    @args(void, [android.os.Bundle])
    def onCreate(self, bundle):
    
        Activity.onCreate(self, bundle)
        
        view = TextView(self)
        
        i = 5
        s = ""
        
        while i:
            s += str(i) + " "
            i -= 1
        
        if s == "5 4 3 2 1 ":
            s += "\nTest passed!"
            
        else:
            s += "\nTest failed!"
        
        view.setText(s)
        
        self.setContentView(view)
