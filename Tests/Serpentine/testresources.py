__package__ = "com.example.testresources"

from java.lang import String
from android.app import Activity
import android.os
from android.widget import TextView

import app_resources

# Application classes

class TestResourcesActivity(Activity):

    __interfaces__ = []
    abc = 123
    
    def __init__(self):
    
        Activity.__init__(self)
        a = TestResourcesActivity.abc
    
    @args(void, [android.os.Bundle])
    def onCreate(self, bundle):
    
        Activity.onCreate(self, bundle)
        
        view = TextView(self)
        a = app_resources.R.string.app_name
        view.setText(self.getResources().getString(a))
        
        self.setContentView(view)
