__package__ = "com.example.ranges2"

from Common.helper import TestActivity
from serpentine.collections import Collections

class RangesActivity(TestActivity):

    def __init__(self):
    
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        s = ""
        
        for i in range(-5, 5.0):
            s += str(i) + " "
        
        s += "\n"
        
        expected = (
            "0 1 2 3 4 \n"
            "0.0 1.0 2.0 3.0 4.0 \n"
            "0.0 1.0 2.0 3.0 4.0 \n"
            "0 1 2 3 4 \n"
            "-5 -4 -3 -2 -1 0 1 2 3 4 \n"
            "-5.0 -4.0 -3.0 -2.0 -1.0 0.0 1.0 2.0 3.0 4.0 \n"
            "-5 -4 -3 -2 -1 0 1 2 3 4 \n"
            "-5.0 -4.0 -3.0 -2.0 -1.0 0.0 1.0 2.0 3.0 4.0 \n"
            "-5 -3 -1 1 3 \n"
            "-5.0 -3.0 -1.0 1.0 3.0 \n"
            "-5 -3 -1 1 3 \n"
            "-5.0 -3.0 -1.0 1.0 3.0 "
            )
        
        self.showResult(expected, s, True)
