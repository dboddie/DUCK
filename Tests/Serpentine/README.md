Manual Regression Tests for Serpentine Programs
-----------------------------------------------

This directory contains regression tests written in the Serpentine language
that need to be run and verified on a device with the Dalvik virtual machine.
They also function as simple regression tests for any API or compiler breakage
since changes to either of these may cause compilation to fail completely.

To compile the tests, invoke the buildall.py script, making sure that the
Compiler package directory is in the PYTHONPATH. For example, from the root of
the distribution type the following, remembering to substitute your own key and
certificate files for the placeholders used:

```\
PYTHONPATH=. Tests/Serpentine/buildall.py <key.pem> <cert.pem> /tmp/packages
```

Tests can also be built individually. For example:

```\
PYTHONPATH=. Tests/Serpentine/build.py Tests/Serpentine/maps.py /tmp/output \
             <key.pem> <cert.pem> /tmp/Maps.apk
```

When run, some of these tests will indicate success or failure. Others need to
have their output checked against their source code.
