# -*- coding: utf8 -*-

__package__ = "com.example.unicode1"

from Common.helper import TestActivity

class UnicodeActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        text = u"ø"
        
        self.showResult(u"\xf8", text, True)
