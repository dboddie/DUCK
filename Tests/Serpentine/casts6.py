# Tests generation of bytecode for constants of specific types.
# Use the DUCK_LISTING environment variable when building to create a listing
# for inspection.

__package__ = "com.example.casts6"

from java.lang import Byte, Short
from Common.helper import TestActivity

# Application classes

class CastsActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        i = self.fn(2.0)
        
        s = str(i)
        
        expected = "2"
        
        self.showResult(expected, s)

    @args(int, [int])
    def fn(self, v):
        return v
