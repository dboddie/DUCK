__package__ = "com.example.methods4"

from java.lang import Object, String
from android.os import Bundle

from Common.helper import TestActivity

# Application classes

class MethodsActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    @args(void, [Bundle])
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        s = Other.fn(123)       # static method call
        o = Other()
        t = o.fn(456)           # calling a static method via an instance
        
        self.showResult("123456", s + t)


class Other(Object):

    def __init__(self):
        Object.__init__(self)
    
    @static
    @args(String, [int])
    def fn(i):
        return str(i)
