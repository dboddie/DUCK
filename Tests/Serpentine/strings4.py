__package__ = "com.example.strings4"

from java.lang import String

from Common.helper import TestActivity

class StringsActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        a = "Hello world!"
        b = a[0:5] + " " + a[6:11] + " " + a[:5] + " " + a[6:]
        
        expected = "Hello world Hello world!"
        
        self.showResult(expected, b)
