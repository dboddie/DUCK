__package__ = "com.example.views_and_clicks"

from java.lang import String
from android.app import Activity
import android.os
from android.view import View, ViewGroup
from android.widget import Button, LinearLayout, TextView, Toast

# Application classes

class HelloActivity(Activity):

    __interfaces__ = [View.OnClickListener]
    __fields__ = {"textView": TextView}
    
    abc = 0x7f030000 # R.layout.main
    
    def __init__(self):
    
        Activity.__init__(self)
        self.counter = 123
        # Note that we cannot instantiate a TextView here because it needs
        # onCreate to have been called first.
    
    @args(void, [android.os.Bundle])
    def onCreate(self, bundle):
    
        Activity.onCreate(self, bundle)
        layout = LinearLayout(self)
        layout.setOrientation(LinearLayout.VERTICAL)
        layout.setLayoutParams(ViewGroup.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT))
        self.button = Button(self)
        self.button.setText("Click me")
        self.textView = TextView(self)
        self.textView.setText("Hello World!")
        
        self.button.setOnClickListener(self)
        
        layout.addView(self.button)
        layout.addView(self.textView)
        self.setContentView(layout)
    
    @args(int, [])
    def value(self):
        return HelloActivity.abc
    
    @args(int, [int, int, int, int])
    def fn(self, a, b, c, d):
        e = a == b
        return a + b + c + d
    
    @args(void, [View])
    def onClick(self, view):
    
        Toast.makeText(view.getContext(), "Clicked!", 1).show()
        s = String.valueOf(self.counter)
        self.textView.setText(s)
        self.counter = self.counter + 1
