__package__ = "com.example.while6"

from android.os import Bundle

from Common.helper import TestActivity

class WhileActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    @args(void, [Bundle])
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        s = ""
        i = 0
        while i < 10:
            s += str(i) + " "
            if i == 5:
                break
            i = i + 1
        else:
            s += str(i)
        
        expected = "0 1 2 3 4 5 "
        
        self.showResult(expected, s)
