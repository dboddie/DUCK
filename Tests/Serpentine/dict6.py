__package__ = "com.example.dict6"

from serpentine.collections import Pair
from Common.helper import TestActivity

class DictActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        pairs = [Pair("Aa", "Alice"), Pair("Bb", "Bob"), Pair("Cc", "Carol")]
        d = dict(pairs)
        
        s = ""
        for pair in pairs:
            key = pair.first()
            s += key + ": " + d[key] + "\n"
        
        expected = "Aa: Alice\nBb: Bob\nCc: Carol\n"
        self.showResult(expected, s, True)
