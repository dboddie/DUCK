__package__ = "com.example.tuples5"

from Common.helper import TestActivity

class TuplesActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        l = a, b = 1, 2
        s = str(a) + " " + str(b) + " : " + str(l[0]) + " " + str(l[1]) + "\n"
        
        a, b = l = 3, 4
        s += str(a) + " " + str(b) + " : " + str(l)
        
        expected = "1 2 : 1 2\n3 4 : [3, 4]"
        self.showResult(expected, s, True)
