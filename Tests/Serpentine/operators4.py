__package__ = "com.example.operators4"

from android.os import Bundle
from Common.helper import TestActivity

class OperatorsActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    @args(void, [Bundle])
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        # Test that different primitive types can be combined using binary
        # operators.
        s = ""
        
        s += str(long(4294967296) + 1) + "\n"   # long/int
        s += str(1 + long(4294967296)) + "\n"   # long/int
        
        self.showResult("4294967297\n4294967297\n", s)
