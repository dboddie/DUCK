__package__ = "com.example.lists7"

from android.app import Activity
from android.content import Context
from android.os import Bundle
from android.widget import TextView

# Application classes

class ListsActivity(Activity):

    def __init__(self):
    
        Activity.__init__(self)
    
    @args(void, [Bundle])
    def onCreate(self, bundle):
    
        Activity.onCreate(self, bundle)
        
        b = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
        
        s = ""
        
        i = b.iterator()
        while i.hasNext():
        
            j = i.next().iterator()
            while j.hasNext():
            
                value = j.next()
                s += str(value) + " "
            
            s += "\n"
        
        view = TextView(self)
        view.setText(s)
        self.setContentView(view)
