__package__ = "com.example.integerlist2"

from java.lang import Integer
from java.util import ArrayList
from Common.helper import TestActivity

class IntegerListActivity(TestActivity):

    def __init__(self):
    
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        numbers = []
        numbers.add(Integer(68))
        numbers.add(Integer(85))
        numbers.add(Integer(67))
        numbers.add(Integer(75))
        
        s = ""
        for n in numbers:
            s += str(n.intValue()) + " "
        
        expected = "68 85 67 75 "
        self.showResult(expected, s)
