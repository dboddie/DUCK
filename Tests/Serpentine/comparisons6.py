__package__ = "com.example.comparisons6"

from android.app import Activity
import android.os
from android.widget import TextView

# Application classes

class ComparisonsActivity(Activity):

    __interfaces__ = []
    
    def __init__(self):
    
        Activity.__init__(self)
    
    @args(void, [android.os.Bundle])
    def onCreate(self, bundle):
    
        Activity.onCreate(self, bundle)
        
        view = TextView(self)
        
        i = True
        
        if i:
            view.setText("Test passed!")
        else:
            view.setText("Test failed!")
        
        self.setContentView(view)
