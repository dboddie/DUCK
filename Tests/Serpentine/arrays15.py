__package__ = "com.example.arrays15"

from java.lang import String

from Common.helper import TestActivity

class ArraysActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        s1 = "Hello World!"
        a = s1.getBytes("ASCII")[:6]
        b = s1.getBytes("ASCII")[6:]
        
        s = String(a, "ASCII") + String(b, "ASCII")
        expected = String("Hello World!".getBytes("ASCII"), "ASCII")
        
        self.showResult(expected, s)
