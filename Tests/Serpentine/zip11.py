__package__ = "com.example.zip11"

from Common.helper import TestActivity

class ZipActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        l1 = ["A", "D", "G"]
        l2 = ["B", "E", "H"]
        l3 = ["C", "F", "I", "J"]
        s = ""
        
        for i, j, k in zip([l1, l2, l3]):
            s += i + j + k
        
        expected = "ABCDEFGHI"
        self.showResult(expected, s, True)
