__package__ = "com.example.fields1"

from java.lang import Object

from Common.helper import TestActivity

class FieldsActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        # Test field access in a separate class.
        c = Container()
        s = c.attr
        
        self.showResult("Hello world!", s, True)


class Container(Object):

    __fields__ = {
        "attr": str
        }
    
    def __init__(self):
    
        Object.__init__(self)
        
        self.attr = "Hello world!"
