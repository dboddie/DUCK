__package__ = "com.example.iterators2"

import android.os
from Common.helper import TestActivity

class IteratorsActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        numbers = [68, 85, 67, 75]
        
        iterator = numbers.iterator()
        
        s = ""
        while iterator.hasNext():
            v = iterator.next()
            s += str(v) + " "
        
        self.showResult("68 85 67 75 ", s)
