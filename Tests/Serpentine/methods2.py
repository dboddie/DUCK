__package__ = "com.example.methods2"

from java.lang import Object, String
from android.os import Bundle

from Common.helper import TestActivity

# Application classes

class MethodsActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    @args(void, [Bundle])
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        other = Other()
        s = other.fn(123)
        
        self.showResult("123", s)


class Other(Object):

    def __init__(self):
        Object.__init__(self)
    
    @args(String, [int])
    def fn(self, i):
    
        return str(i)
