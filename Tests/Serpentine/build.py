#!/usr/bin/env python

"""
Copyright (C) 2016 David Boddie <david@boddie.org.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import os, shutil, sys, tempfile

from DUCK.Tools import buildhelper

svg_icon_file = "icon.svg"
include_paths = []
res_files = {"drawable": {"ic_launcher": "icon.svg"}}
layout = None
features = []
permissions = []    

if __name__ == "__main__":

    if not 5 <= len(sys.argv) <= 6:
        sys.stderr.write("Usage: %s <source file> [<output directory>] <key file> "
            "<certificate file> <package file>\n" % sys.argv[0])
        sys.exit(1)
    
    code_file = sys.argv[1]
    
    # Drop the source file from the arguments passed to the build helper.
    args = sys.argv[:1] + sys.argv[2:]
    
    if len(args) == 4:
        temp_dir = tempfile.mkdtemp()
        args.insert(1, temp_dir)
    else:
        temp_dir = None
    
    app_name = os.path.splitext(os.path.split(code_file)[1])[0]
    try:
        package_line = filter(lambda line: "__package__" in line,
                                           open(code_file).readlines())[0]
    except IndexError:
        sys.stderr.write("No __package__ line found in file '%s'.\n" % code_file)
        sys.exit(1)
    
    package_name = package_line[package_line.find('"') + 1:package_line.rfind('"')]
    
    code_file_name = os.path.split(code_file)[1]
    
    result = buildhelper.main(__file__, app_name, package_name, res_files,
        layout, code_file_name, include_paths, features, permissions, args)
    
    if result == 0 and temp_dir:
        shutil.rmtree(temp_dir)
    
    sys.exit(result)
