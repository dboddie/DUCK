__package__ = "com.example.if3"

from java.lang import String

from Common.helper import TestActivity

class IfActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        a = 1
        if a == 1:
            if a != 2:
                s = "1"
            else:
                s = "2"
        else:
            s = "3"
        
        self.showResult("1", s)
