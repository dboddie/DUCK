__package__ = "com.example.comparisons38"

from Common.helper import TestActivity

class ComparisonsActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        s = str(1 < 2 < 3) + " " + \
            str(1 < 3 < 2) + " " + \
            str(2 < 1 < 3) + " " + \
            str(2 < 3 < 1) + " " + \
            str(3 < 2 < 1) + " " + \
            str(3 < 1 < 2)
            
        expected = "true false false false false false"
        self.showResult(expected, s)
