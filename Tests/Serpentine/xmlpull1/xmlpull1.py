__package__ = "com.example.xmlpull1"

from org.xmlpull.v1 import XmlPullParser, XmlPullParserFactory

from Common.helper import TestActivity

from app_resources import R

class SAXActivity(TestActivity):

    expected = """Start document
Start tag: svg
Start tag: rect
End tag: rect
Start tag: circle
End tag: circle
End tag: svg
End document"""
    
    def __init__(self):
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        resources = self.getResources()
        stream = resources.openRawResource(R.raw.sample)
        
        factory = XmlPullParserFactory.newInstance()
        parser = factory.newPullParser()
        parser.setInput(stream, None)
        
        eventType = parser.getEventType()
        s = ""
        
        while eventType != XmlPullParser.END_DOCUMENT:
        
            if eventType == XmlPullParser.START_DOCUMENT:
                s += "Start document\n"
            elif eventType == XmlPullParser.START_TAG:
                s += "Start tag: " + parser.getName() + "\n"
            elif eventType == XmlPullParser.END_TAG:
                s += "End tag: " + parser.getName() + "\n"
            
            eventType = parser.next()
        
        s += "End document"
        
        self.showResult(self.expected, s, True)
