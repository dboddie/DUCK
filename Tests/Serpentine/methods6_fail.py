__package__ = "com.example.methods6"

from java.lang import Object, String
from android.os import Bundle

from Common.helper import TestActivity

# Application classes

class MethodsActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    @args(void, [Bundle])
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        # Middle.fn is not static and Base.fn should not be resolved.
        s = Middle.fn(123)
        
        self.showResult("", s)


class Base(Object):

    def __init__(self):
        Object.__init__(self)
    
    @static
    @args(String, [int])
    def fn(i):
        return str(i)


class Middle(Base):

    def __init__(self):
        Base.__init__(self)
    
    @args(String, [int])
    def fn(self, i):
        return str(i)
