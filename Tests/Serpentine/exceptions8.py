__package__ = "com.example.exceptions8"

from java.lang import ArithmeticException, Exception
from android.app import Activity
import android.os
from android.widget import TextView
from Common.helper import TestActivity

# Application classes

class ExceptionsActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        try:
            a = 4/2
        except:
            pass
        
        expected = "2"
        
        self.showResult(expected, str(a))
