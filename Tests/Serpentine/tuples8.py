__package__ = "com.example.tuples8"

from Common.helper import TestActivity

class TuplesActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        t = (1, 2)
        a, b = t
        s = str(a) + " " + str(b)
        
        expected = "1 2"
        self.showResult(expected, s)
