__package__ = "com.example.methods17"

from java.lang import Object, String
from android.os import Bundle

from Common.helper import TestActivity

class MethodsActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    @args(void, [Bundle])
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        m = Base(123)
        n = Base(456)
        s = Base.fn(m)
        s += Base.fn(n)
        s += m.fn()
        s += n.fn()
        s += Base.fn()
        
        self.showResult("123456123456xxx", s)


class Base(Object):

    @args(void, [int])
    def __init__(self, i):
    
        Object.__init__(self)
        self.i = i
    
    # The following two methods have the same signature. Java doesn't allow this.
    
    @args(String, [])
    def fn(self):
        return str(self.i)
    
    @static
    @args(String, [])
    def fn():
        return "xxx"
