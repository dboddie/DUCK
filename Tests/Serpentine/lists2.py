__package__ = "com.example.lists2"

from java.lang import Integer, String
from java.util import Collection, HashMap, HashSet
from android.app import Activity
from android.content import Context
from android.os import Bundle
from android.widget import TextView

# Application classes

class ListsActivity(Activity):

    def __init__(self):
    
        Activity.__init__(self)
    
    @args(void, [Bundle])
    def onCreate(self, bundle):
    
        Activity.onCreate(self, bundle)
        
        b = []
        b.add("Hello")
        
        view = TextView(self)
        if b[0] == "Hello":
            view.setText("Test passed!")
        else:
            view.setText("Test failed: " + str(b[0]))
        
        self.setContentView(view)
