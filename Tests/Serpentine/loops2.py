__package__ = "com.example.loops2"

from Common.helper import TestActivity

class LoopsActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        s = ""
        i = 1
        while i <= 5:
            s += str(i) + ": "
            j = 1
            while j <= 5:
                s += str(i * j) + " "
                j += 1
            
            s += "\n"
            i = i + 1
        
        expected = "1: 1 2 3 4 5 \n" \
                   "2: 2 4 6 8 10 \n" \
                   "3: 3 6 9 12 15 \n" \
                   "4: 4 8 12 16 20 \n" \
                   "5: 5 10 15 20 25 \n"
        
        self.showResult(expected, s)
