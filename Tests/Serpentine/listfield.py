__package__ = "com.example.listfield"

import java.util
from java.util import ArrayList
from android.app import Activity
from android.content import Context
import android.os
from android.widget import TextView

# Application classes

class TextViewList(ArrayList):

    __item_types__ = [TextView]
    
    def __init__(self):
        ArrayList.__init__(self)


class ListFieldActivity(Activity):

    __interfaces__ = []
    __fields__ = {"views": java.util.List(TextView)}
    #__fields__ = {"views": TextViewList}
    
    def __init__(self):
    
        Activity.__init__(self)
    
    @args(void, [android.os.Bundle])
    def onCreate(self, bundle):
    
        Activity.onCreate(self, bundle)
        
        self.views = TextViewList()
        view = TextView(self)
        view.setText("Test passed!")
        self.views.add(view)
        
        self.setContentView(view)
