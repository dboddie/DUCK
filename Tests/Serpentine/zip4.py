__package__ = "com.example.zip4"

from Common.helper import TestActivity

class ZipActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        l1 = array([1.0, 2.0, 2.5])
        l2 = array([1.0, 4.0, 6.25])
        z = zip(l1, l2)
        s = ""
        
        for i, j in z:
            s += str(i) + " " + str(j) + "\n"
        
        expected = "1.0 1.0\n2.0 4.0\n2.5 6.25\n"
        self.showResult(expected, s, True)
