__package__ = "com.example.for7"

import android.os
from Common.helper import TestActivity

class ForActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    @args(void, [android.os.Bundle])
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        s = ""
        m = {"abc": "def"}
        m.remove("abc")
        
        for key in m.keySet():
            s += key
        
        expected = ""
        self.showResult(expected, s)
