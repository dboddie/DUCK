__package__ = "com.example.lists16"

from java.lang import Object
from java.util import List
from Common.helper import TestActivity

class ListsActivity(TestActivity):

    __fields__ = {"l": List(Object)}
    
    def __init__(self):
    
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        l = [1, 2, 3]
        m = [l, [4, 5, 6]]
        
        a = 1 in l in m
        
        actual = str(a)
        
        expected = "true"
        
        self.showResult(expected, actual)


class Sub(Object):

    def __init__(self):
        Object.__init__(self)
