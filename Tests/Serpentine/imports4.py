__package__ = "com.example.imports4"

from Common.helper import TestActivity
from android import app

class ImportsActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        # If the example compiles then the test passed.
        self.showResult("", "")
