__package__ = "com.example.for1"

from android.app import Activity
import android.os
from android.widget import TextView

# Application classes

class ForActivity(Activity):

    def __init__(self):
        Activity.__init__(self)
    
    @args(void, [android.os.Bundle])
    def onCreate(self, bundle):
    
        Activity.onCreate(self, bundle)
        
        view = TextView(self)
        s = ""
        l = [0, 1, 2, 3, 4, 5]
        for i in l:
            s += str(i) + " "
        
        if s == "0 1 2 3 4 5 ":
            view.setText("Test passed!")
        else:
            view.setText("Test failed:\nExpected:\n0 1 2 3 4 5 \nObtained:\n" + s)
        self.setContentView(view)
