__package__ = "com.example.comparisons33"

from Common.helper import TestActivity

class ComparisonsActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        i = 1
        
        if i == 0:
            x = 123
        elif False:
            y = -1
        else:
            x = 124
        
        s = str(x)
        
        self.showResult("124", s)
