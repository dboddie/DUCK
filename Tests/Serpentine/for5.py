__package__ = "com.example.for5"

from Common.helper import TestActivity

class ForActivity(TestActivity):

    def __init__(self):
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        s = ""
        l = [0, 1, 2, 3, 4, 5]
        
        # Define the item variable outside the loop so that it can be accessed
        # after iteration.
        i = 0
        for i in l:
            s += str(i) + " "
        
        s += str(i)
        
        expected = "0 1 2 3 4 5 5"
        self.showResult(expected, s)
