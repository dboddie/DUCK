__package__ = "com.example.imports3"

from Common import helper

class ImportsActivity(helper.TestActivity):

    def __init__(self):
        helper.TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        helper.TestActivity.onCreate(self, bundle)
        
        # If the example compiles then the test passed.
        self.showResult("", "")
