#!/usr/bin/env python

"""
Copyright (C) 2017 David Boddie <david@boddie.org.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import os, sys

from DUCK.Tools import buildhelper
from DUCK.Tools.common import full_path

app_name = "interfaces2"
package_name = "com.example.interfaces2"
res_files = {
    "drawable": {"ic_launcher": "icon.svg"},
    "string": {"test_string": "Test passed!"},
    "raw": {"sample": "icon.svg"}
    }
code_file = "interfaces2.py"
include_paths = [full_path(__file__, os.pardir)]
layout = None
features = []
permissions = []

if __name__ == "__main__":

    args = sys.argv[:]
    
    result = buildhelper.main(__file__, app_name, package_name, res_files,
        layout, code_file, include_paths, features, permissions, args)
    
    sys.exit(result)
