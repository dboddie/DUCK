__package__ = "com.example.lists20"

from Common.helper import TestActivity
from serpentine.collections import Collections

class ListsActivity(TestActivity):

    def __init__(self):
    
        TestActivity.__init__(self)
    
    def onCreate(self, bundle):
    
        TestActivity.onCreate(self, bundle)
        
        s = ""
        for i in range(10):
            s += str(i) + " "
        
        expected = "0 1 2 3 4 5 6 7 8 9 "
        
        self.showResult(expected, s)
