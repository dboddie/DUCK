Pop-up Window Example
=====================

This is a simple example that responds to the user clicking a button by showing
a pop-up window.

![A screenshot of the application.](popupwindow.png)

We import the classes and modules that will be needed by the application. The
relevant one to this example is the `PopupWindow` class.

```python
from java.lang import Object, String
from android.app import Activity
from android.view import Gravity, View, ViewGroup
from android.widget import Button, LinearLayout, PopupWindow, TextView
```

We define a class based on the standard Activity class. This represents the
application, and will be used to present a graphical interface to the user.
We also declare that the class implements the `View.OnClickListener` interface
so that we can respond to clicks and define a field to hold a `PopupWindow`
object.

```python
class PopupWindowActivity(Activity):

    __interfaces__ = [View.OnClickListener]
    
    __fields__ = {"popup": PopupWindow}
```

The initialisation method calls the corresponding method in the base
class and initialise the `popup` field.

```python
    def __init__(self):
    
        Activity.__init__(self)
        
        self.popup = None
```

The onCreate method is called with a Bundle object that lets the
activity restore its state from the last time it was run. Here, we simply
pass the bundle to the onCreate method in the base class.

```python
    def onCreate(self, bundle):
    
        Activity.onCreate(self, bundle)
```

At this point it is possible to create a user interface using views
and widgets. We create a vertical layout and place a button in it.

```python
        self.layout = LinearLayout(self)
        self.layout.setOrientation(LinearLayout.VERTICAL)
        self.layout.setLayoutParams(ViewGroup.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT))
        
        self.button = Button(self)
        self.button.setText("Click me")
        self.layout.addView(self.button)
```

In order to respond to clicks on the button, we create a listener
object that we register with the button. This will cause the listener's
onClick method to be called when the button is clicked. The Listener
class is defined below.

```python
        self.button.setOnClickListener(self)
        
        self.setContentView(self.layout)
```

The layout is the main content in the activity.

Since the activity is a listener for the button's click events, when the
button is clicked, the activity's `onClick` method is called. In this
method we dismiss any existing pop-up window and show another just below
the button.

```python
    def onClick(self, view):
    
        if self.popup != None:
            self.popup.dismiss()
        
        self.popup = MyPopup(self)
        self.popup.showAsDropDown(self.button, 0, 0)
        self.popup.update(self.button.getWidth(), self.button.getHeight())
```

We create a pop-up window using a custom class and show it at an offset
of (0, 0) from the bottom-left corner of the button. We update its size to
ensure that something is shown, otherwise the window will have zero width
and height.

We define a subclass of `PopupWindow` to allow us to customise our pop-up
window and make it respond to user input by implementing an interface.

```python
class MyPopup(PopupWindow):

    __interfaces__ = [View.OnClickListener]
```

We only reimplement the form of the `__init__` method that accepts a
`Context` object. We call the corresponding method in the base class and
make the window respond to touch events.

```python
    def __init__(self, context):
    
        PopupWindow.__init__(self, context)
        self.setTouchable(True)
        
        label = TextView(context)
        label.setText("Pop up!")
        label.setOnClickListener(self)
        self.setContentView(label)
```

To make the pop-up useful, we create a label and make it the main
content view for the window. The pop-up object is registered as a click
listener for the label so that the following method is called when the user
touches or clicks it.

```python
    def onClick(self, view):
    
        self.dismiss()
```

We simply dismiss the pop-up window when a click occurs.

## Files

* [icon.svg](../icon.svg)
