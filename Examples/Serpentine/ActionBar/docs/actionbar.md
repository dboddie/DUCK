# Action Bar Example

This simple example shows how to access an activity's action bar.

![A screenshot of the application.](actionbar.png)

We import the classes and modules that will be needed by the application. The
Window class is particularly relevant since we use a constant it defines to
request access to the action bar feature.

```python
from android.app import Activity
import android.os
from android.view import Window
from android.widget import TextView
```

We define a class based on the standard Activity class. This represents the
application, and will be used to present a graphical interface to the user.

```python
class ActionBarActivity(Activity):

    def __init__(self):
    
        Activity.__init__(self)
```

The initialisation method simply calls the corresponding base class
method to properly initialise the activity.

Similarly, the onCreate method calls the onCreate method of the base class
to help set up the activity before creating a user interface.

```python
    @args(void, [android.os.Bundle])
    def onCreate(self, bundle):
    
        Activity.onCreate(self, bundle)
```

We obtain the window shown by the activity and use its
requestFeature method to request an action bar, and we create a
TextView that we use to report the presence or absence of the action
bar.

```python
        window = self.getWindow()
        window.requestFeature(Window.FEATURE_ACTION_BAR)
        
        view = TextView(self)
```

Having requested an action bar feature, we attempt to obtain its
corresponding object. If none exists, we set the TextView's text to
indicate this. Otherwise we report its presence.

```python
        bar = self.getActionBar()
        if bar == None:
            view.setText("No bar!")
        else:
            view.setText("Bar found!")
```

We make the TextView the main view of the activity, displaying its
contents to the user.

```python
        self.setContentView(view)
```



## Files

* [icon.svg](../icon.svg)
