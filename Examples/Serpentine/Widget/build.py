#!/usr/bin/env python

"""
Copyright (C) 2016 David Boddie <david@boddie.org.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import os, sys

from DUCK.Tools import buildhelper
from DUCK.Tools.makelayout import ImageView, LinearLayout, TextView

app_name = "Widget"
package_name = "com.example.widget"
res_files = {
    "drawable": {"preview": "../Bitmaps/duck.png"},
    "xml": {"widget": "widget.xml"}
    }
code_file = "widget.py"
include_paths = []
layout = [
    LinearLayout({"android:background": 0x3f0000ff},
        children=[
            TextView({"android:text": "Widget Example",
                      "android:textColor": 0xffffffff,
                      "android:id": "first_line",
                      "android:gravity": 0x01}),
            ImageView({"android:id": "image"}),
            TextView({"android:text": "using DUCK",
                      "android:textColor": 0x7f00ff00,
                      "android:id": "second_line",
                      "android:gravity": 0x01})
        ])
    ]
features = []
permissions = []
docs_dir = os.getenv("DOCS_DIR")

if __name__ == "__main__":

    args = sys.argv[:]
    
    result = buildhelper.main(__file__, app_name, package_name, res_files,
        layout, code_file, include_paths, features, permissions, args,
        app_template = "widget", description = {"resource": "widget"},
        docs_dir = docs_dir)
    
    sys.exit(result)
