# Bitmaps Example

This example shows how to display a bitmap stored as a resource in the
application package.

![A screenshot of the application.](bitmaps.png)

We begin by declaring a unique namespace for the package we are creating.
This is typically based on the reversed form of a domain name and should be
unique to this package.

```python
__package__ = "com.example.bitmaps"
```

We import the classes and modules needed by our application. The most
relevant to this example are the BitmapFactory, Canvas and Paint classes from
the android.graphics module which handle bitmap loading and painting.
The Resources class from the android.content.res module is used to access the
resources packaged with the application.

```python
import android.app
from android.content import Context
from android.content.res import Resources
from android.graphics import BitmapFactory, Canvas, Paint
import android.os
import android.view
```

We import a class from the special app_resources module that is created when
the application is built. This allows us to refer to the application resources.

```python
from app_resources import R
```

The BitmapsActivity is derived from the standard Activity class and
represents the application. Android will create an instance of this class when
the user runs it.

The initialisation method simply calls the corresponding method in the base
class.

```python
class BitmapsActivity(android.app.Activity):

    def __init__(self):
    
        android.app.Activity.__init__(self)
```

The onCreate method is called when the activity is run. We call the
corresponding method in the base class to help set up the activity before
creating a custom view that we set as the activity's content view.
The DrawView class is defined below.

```python
    def onCreate(self, bundle):
    
        android.app.Activity.onCreate(self, bundle)
        
        view = DrawView(self, self.getResources())
        self.setContentView(view)
```

The DrawView class is a custom view that is derived from the standard View
class that simply draws a bitmap on a single colour background and responds to
touches.

The initialisation method accepts the usual Context object as its first
parameter. The second parameter is a Resources object that refers to the
application's resources.

```python
class DrawView(android.view.View):

    @args(void, [Context, Resources])
    def __init__(self, context, resources):
```

We initialise the view by calling the corresponding method in the
base class, and we initialise some instance attributes to describe
how the view will appear.

```python
        android.view.View.__init__(self, context)
        self.paint = Paint()
        self.paint.setARGB(255, 40, 80, 160)
        self.x = 0
        self.y = 0
        
        self.bitmap = BitmapFactory.decodeResource(resources, R.raw.duck)
```

The bitmap is decoded from the data in the application's resources
using a static method of the BitmapFactory class. This is stored as an
instance attribute.

We reimplement the standard onDraw method to draw the custom content in
the view. As usual, we call the corresponding method in the base class
before painting the background and bitmap.

```python
    def onDraw(self, canvas):
    
        android.view.View.onDraw(self, canvas)
        canvas.drawPaint(self.paint)
        canvas.drawBitmap(self.bitmap, self.x, self.y, self.paint)
```

The onSizeChanged method is called when the size of the view has
changed. We only take note of the size the first time this method is
called - for this call the oldWidth and oldHeight values are zero.

```python
    def onSizeChanged(self, width, height, oldWidth, oldHeight):
    
        if oldWidth == 0:
            self.x = width/2 - self.bitmap.getWidth()/2
            self.y = height/2 - self.bitmap.getHeight()/2
```

The onTouchEvent method is called when the user touches the screen.
We respond to this by changing the position of the bitmap, redraw the view
by calling the standard invalidate method and return True to indicate that
we handled the event.

```python
    def onTouchEvent(self, event):
    
        self.x = int(event.getX()) - self.bitmap.getWidth()/2
        self.y = int(event.getY()) - self.bitmap.getHeight()/2
        self.invalidate()
        return True
```



## Files

* [duck.png](../duck.png)
