# Nominatim Example

This example shows how to perform a HTTP GET request to the Nominatim API.
See https://nominatim.org for information.

![A screenshot of the application.](nominatim.png)

We import the classes and modules that will be needed by the application. The
most relevant are the classes from the java.net module.

```python
from java.lang import String
from java.io import BufferedInputStream, ByteArrayOutputStream, \
                    FileNotFoundException, IOException
from java.net import HttpURLConnection, URL, URLEncoder
from android.app import Activity
from android.content import Context
from android.os import Build
from android.view import View, ViewGroup
from android.widget import Button, EditText, LinearLayout, TextView, \
                           ScrollView, Toast
from android.net import ConnectivityManager
```

We define a class based on the standard Activity class. This represents the
application, and will be used to present a graphical interface to the user.

```python
class HTTPGetActivity(Activity):

    __interfaces__ = [View.OnClickListener]
```

The activity implements an interface to handle clicks on views.

The initialisation method simply calls the base class implementation.

```python
    def __init__(self):
        Activity.__init__(self)
```

The onCreate method calls the base class implementation before obtaining
a reference to the connectivity service and setting up the user interface.

```python
    def onCreate(self, bundle):
    
        Activity.onCreate(self, bundle)
```

We obtain an object that gives us access to the connectivity service.
Since the method used to obtain this object does not specify the
specific type of the manager object, we need to cast it explicitly to
the correct type so that we can access its methods.

```python
        service = self.getSystemService(Context.CONNECTIVITY_SERVICE)
        self.manager = CAST(service, ConnectivityManager)
```

We set up the user interface: a text input field, a button that is
used to request data from the Nominatim API, and a view that is used to
display the response.

```python
        self.textInput = EditText(self)
        self.textInput.setText("Birmingham")
        
        self.button = Button(self)
        self.button.setText("Fetch Locations")
        self.button.setOnClickListener(self)
        
        self.resultView = TextView(self)
        scrollView = ScrollView(self)
        scrollView.addView(self.resultView)
```

The views are placed in a layout that is the main content shown by
the activity.

```python
        layout = LinearLayout(self)
        layout.setLayoutParams(ViewGroup.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT))
        layout.setOrientation(LinearLayout.VERTICAL)
        
        layout.addView(self.textInput)
        layout.addView(self.button)
        layout.addView(scrollView)
        
        self.setContentView(layout)
```

We implement the onClick method to provide the View.OnClickListener
interface that we declared earlier. This responds to clicks on the button
that we created in the previous method.

```python
    def onClick(self, view):
```

Currently, we have only implemented support for older versions of
the connectivity API, so we check for those before checking whether the
device has access to the Internet.

```python
        if Build.VERSION.SDK_INT < 23:
            info = self.manager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE)
            if info == None or not info.isConnected():
                Toast.makeText(self, "Not connected", Toast.LENGTH_SHORT).show()
                return
        else:
            Toast.makeText(self, "Not supported\nPlease update the example code.", Toast.LENGTH_SHORT).show()
            return
```

If the device is able to access online resources, we send a request
to the Nominatim service. We specify the URL of the resource and use
the URL object to open a connection to the server hosting it.

```python
        city = str(self.textInput.getText()).trim()
        url = URL("https://nominatim.openstreetmap.org/search?city=" + \
                  URLEncoder.encode(city, "UTF-8") + "&format=json")
        
        connection = CAST(url.openConnection(), HttpURLConnection)
        connection.setInstanceFollowRedirects(True)
        connection.setRequestProperty("User-Agent", "Nominatim test app")
        connection.setRequestProperty("Accept", "application/json")
```

We use a stream to read the content, storing it in an array of the
required length or showing an error message and returning if the stream
cannot be opened.

```python
        try:
            stream = BufferedInputStream(connection.getInputStream())
        except FileNotFoundException:
            Toast.makeText(self, "Resource not found", Toast.LENGTH_SHORT).show()
            return
```

We loop until we have read all the content, storing new data after
that we have already read, and requesting as much as possible. When all
the data has been read, we close the stream.

```python
        buf_out = ByteArrayOutputStream()
        
        while True:
            b = stream.read()
            if b == -1:
                break
            buf_out.write(b)
        
        json_text = buf_out.toString("UTF-8")
        
        stream.close()
```

Since the content should be encoded as UTF-8, 

```python
        self.resultView.setText(json_text)
```



## Files

* [icon.svg](../icon.svg)
