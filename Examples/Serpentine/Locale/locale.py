"""
# Locale Example

This example shows how to show appropriate text for the current locale.

![A screenshot of the application.](locale.png)

We begin by importing the classes that we will use.
"""

from android.app import Activity
from android.widget import TextView
from serpentine.widgets import VBox

"""We also import the `R` class from the application's resources, making it
possible to access data prepared by the build script."""

from app_resources import R

"""The application is a minimal implementation with the usual `__init__` and
`onCreate` methods."""

class LocaleActivity(Activity):

    def __init__(self):
    
        Activity.__init__(self)
    
    def onCreate(self, bundle):
    
        Activity.onCreate(self, bundle)
        
        """The user interface consists of three text views. The first shows the
        current locale."""
        
        view = TextView(self)
        locale = self.getResources().getConfiguration().locale
        view.setText(locale.getDisplayName() + " " + locale.getLanguage())
        
        """The other text views show text strings that are referred to by their
        resource identifiers, `R.string.hello` and `R.string.world`. These are
        defined in the `res_files` dictionary in the build script."""
        
        hello = TextView(self)
        hello.setText(R.string.hello)
        
        world = TextView(self)
        world.setText(R.string.world)
        
        layout = VBox(self)
        layout.addView(view)
        layout.addView(hello)
        layout.addView(world)
        
        self.setContentView(layout)

"""Each of the `R.string.hello` and `R.string.world` identifiers is mapped to
more than one string in the build script, stored in the `"string"`, `"string-en"`
and `"string-nb"` dictionaries. The `-en` and `-nb` suffixes indicate that
strings in those dictionaries should be associated with two locales.

As a result, the resource bundle provided with the application contains all
three sets of strings, and the application will automatically use the
appropriate string for the user's locale at run-time."""
