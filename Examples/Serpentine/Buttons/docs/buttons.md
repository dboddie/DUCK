Buttons Example
===============

This is a simple example that displays a few different types of button and
responds to the user clicking them.

![A screenshot of the application.](buttons.png)

We import the classes and modules that will be needed by the application. The
most relevant are from the android.widget package.

```python
from java.lang import Object
from android.app import Activity
import android.content
import android.os
from android.view import View, ViewGroup
from android.widget import Button, CheckBox, LinearLayout, RadioButton, \
                           RadioGroup, Switch, TextView, ToggleButton
```

We also import the R object from the special app_resources module that is
created by the build script. This provides constants that we use to refer to
resources bundled in the application's package.

```python
import android.R
```

We define a class based on the standard Activity class. This represents the
application, and will be used to present a graphical interface to the user.

```python
class ButtonsActivity(Activity):
```

We declare that the class implements the View.OnClickListener
interface by adding this interface to the list held by the
`__interfaces__` attribute. To implement this interface, we implement
the onClick method.

```python
    __interfaces__ = [RadioGroup.OnCheckedChangeListener, View.OnClickListener]
```

The initialisation method simply calls the corresponding method in the
base class. Note that we cannot instantiate views here because they need
the base class's onCreate method to have been called first.

```python
    def __init__(self):
    
        Activity.__init__(self)
```

The onCreate method is called with a Bundle object that lets the
activity restore its state from the last time it was run. Here, we simply
pass the bundle to the onCreate method in the base class.

```python
    def onCreate(self, bundle):
    
        Activity.onCreate(self, bundle)
```

The Switch widget requires that the API theme is set before it can
be shown.

```python
        self.setTheme(android.R.style.Theme_DeviceDefault)
```

At this point it is possible to create a user interface using views
and widgets.

The Button, CheckBox, RadioButton, ToggleButton and Switch widgets
can all be created and used individually.

```python
        button = Button(self)
        button.setText("Click me")
        
        checkBox = CheckBox(self)
        checkBox.setText("Check me")
        
        radioButton = RadioButton(self)
        radioButton.setText("Click me (single radio button)")
        
        toggleButton = ToggleButton(self)
        toggleButton.setText("Toggle me")
        
        self.switch = Switch(self)
        self.switch.setText("Switch me")
```

A collection of RadioButtons are held in a RadioGroup instance.
Here, we create three buttons and add them to a group, disabling them
initially. We will enable them later when the switch is toggled.

```python
        self.radioGroup = RadioGroup(self)
        for i in range(1, 4):
            rb = RadioButton(self)
            rb.setText("Option " + str(i))
            rb.setEnabled(False)
            self.radioGroup.addView(rb)
```

In order to respond to clicks on the buttons, we register the
activity with each of them. This will cause the onClick method to be
called when a button is clicked.

```python
        button.setOnClickListener(self)
        checkBox.setOnClickListener(self)
        radioButton.setOnClickListener(self)
        toggleButton.setOnClickListener(self)
        self.switch.setOnClickListener(self)
```

We also make the activity class the listener for changes to the
RadioGroup. The onCheckedChanged method will be called when any of the
RadioButtons in the group are modified.

```python
        self.radioGroup.setOnCheckedChangeListener(self)
```

We create a TextView to report interactions and a vertical layout to
hold all the views.

```python
        self.textView = TextView(self)
        
        layout = LinearLayout(self)
        layout.setOrientation(LinearLayout.VERTICAL)
        layout.setLayoutParams(ViewGroup.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT))
```

We place all the views in the layout and set the layout as the main
content in the activity.

```python
        layout.addView(button)
        layout.addView(checkBox)
        layout.addView(radioButton)
        layout.addView(toggleButton)
        layout.addView(self.switch)
        layout.addView(self.radioGroup)
        layout.addView(self.textView)
        
        self.setContentView(layout)
```

The onClick method is called when the user clicks any of the buttons
that uses this class as a listener. We prepend a string with its identity
to the text displayed by the text view.

```python
    def onClick(self, view):
    
        s = str(view) + "\n" + str(self.textView.getText())
        self.textView.setText(s)
```

If the Switch is checked, we enable the RadioButtons in the
RadioGroup, making them accessible. If it is unchecked, the
RadioButtons are disabled instead.

```python
        if view == self.switch:
            for i in range(self.radioGroup.getChildCount()):
                self.radioGroup.getChildAt(i).setEnabled(self.switch.isChecked())
```

The onCheckedChanged method is called when a RadioButton in the
RadioGroup is changed. Again, we prepend the identity of the button to the
text in the text view.

```python
    def onCheckedChanged(self, group, checkedId):
    
        view = self.findViewById(checkedId)
        s = str(view) + "\n" + str(self.textView.getText())
        self.textView.setText(s)
```



## Files

* [icon.svg](../icon.svg)
