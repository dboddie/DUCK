Example Serpentine Programs
===========================

This directory contains examples written in the Serpentine language that need
to be run on a device with the Dalvik or ART virtual machines. They are also
used as simple regression tests for any API or compiler breakage since changes
to either of these may cause compilation to fail completely.

To compile the examples, invoke the buildall.py script, making sure that the
Compiler package directory is in the PYTHONPATH. For example, from the root of
the distribution type the following, remembering to substitute your own [key
and certificate files](../../Documents/Keys_and_Certificates.md) for the
placeholders used:

```\
PYTHONPATH=. Examples/Serpentine/buildall.py <key.pem> <cert.pem> /tmp/packages
```

Examples can also be built individually. For example:

```\
PYTHONPATH=. Examples/Serpentine/Timers/build.py <key.pem> <cert.pem> \
             /tmp/Timers.apk
```

Some of these examples are simple tests of functionality; others were written
to show how to access the Java and Android APIs using the syntax of the
Serpentine language.

More complex examples may be moved to the [Demos](../../Demos/Serpentine/README.md)
directory in the future.

Examples
--------

It might be worth starting with the following examples that introduce aspects
of the language as well as describing how the Java and Android APIs are used:

 * [Minimal](Minimal/docs/minimal.md) is the minimal amount of code required
   for an activity.
 * [Hello](Hello/docs/hello.md) is a simple activity that displays the text,
   "Hello World!"
 * [Clicker](Clicker/docs/clicker.md) responds to clicks on a button by
   updating a text label, showing how to implement an interface.
 * [Buttons](Buttons/docs/buttons.md) showcases some of the buttons available
   in the Android GUI.
 * [Icons](Icons/docs/icons.md) demonstrates how to display images that are
   packaged with an application.
 * [Scroll Icons](ScrollIcons/docs/scrollicons.md) shows how to display a grid
   of icons in a scrolling view.
 * [List View](ListView/docs/listview.md) shows how to expose an array of
   strings to a ListView using a custom adapter class.
 * [String List View](StringListView/docs/stringlistview.md) shows how to
   expose an list of strings to a ListView using a custom adapter class.
 * [Spinner](Spinner/docs/spinner.md) displays a widget that allows the user to
   select an option in a drop-down list. As in the previous example, the list
   is exposed via an adapter.
 * [Views](Views/docs/views.md) explains how custom views are created.
 * [Bitmaps](Bitmaps/docs/bitmaps.md) shows how to display a bitmap from the
   application's packaged resources and respond to touch events.
 * [Mandelbrot](Mandelbrot/docs/mandelbrot.md) shows how to create a custom
   view class and draw on it.
 * [Timers](Timers/docs/timers.md) demonstrates the use of a timer to
   periodically update a user interface.
 * [XML Layout](XMLLayout/docs/xmllayout.md) shows how to refer to an
   XML-like description of an activity's layout.
 * [XML Layout References](XMLLayoutRefs/docs/xmllayoutrefs.md) shows how to
   update a user interface defined using an XML-like layout description.
 * [View Switcher](ViewSwitcher/docs/viewswitcher.md) demonstrates the use of
   a view switcher to display alternating views.

The following examples use more features of the Android API:

 * [Accelerometer](Accelerometer/docs/accelerometer.md) shows how to read
   values from an accelerometer if a device contains one. It also shows how to
   implement callback methods described by an interface.
 * [Action Bar](ActionBar/docs/actionbar.md) requests an action bar for the
   application.
 * [Action Bar List](ActionBarList/docs/actionbarlist.md) shows how to add a
   drop-down list to an action bar.
 * [Action Bar Tabs](ActionBarTabs/docs/actionbartabs.md) uses an alternative
   form of navigation within an activity based on tabs of content.
 * [Audio Record](AudioRecord/docs/audiorecord.md) records sounds using the
   Android media APIs and saves them to files in a device's external storage.
 * [Audio Sampler](AudioSampler/docs/audiosampler.md) records sounds in the
   same way as the Audio Record example, but allows the user to specify the
   audio parameters.
 * [Audio Track](AudioTrack/docs/audiotrack.md) plays a sound using the Android
   media APIs when the user clicks on a button.
 * [Async Task](AsyncTask/docs/asynctask.md) uses a custom class to perform
   processing in the background.
 * [Bluetooth Devices](BluetoothDevices/docs/bluetoothdevices.md) displays
   information about a device's Bluetooth adapter and can scan for other
   devices. It listens for broadcast intents to find out when Bluetooth is
   enabled and obtain information about other devices.
 * [Camera](Camera/docs/camera.md) shows how to display preview images from a
   camera if a device contains one. It also shows how to implement callback
   methods described by an interface.
 * [Camera Intent](CameraIntent/docs/cameraintent.md) requests a camera picture
   from another component using an intent.
 * [Camera Picture](CameraPicture/docs/camerapicture.md) shows how to take a
   picture using a device's camera and save it as a file.
 * [Camera Snap](CameraSnap/docs/camerasnap.md) is another example showing
   how to take a picture using a device's camera.
 * [Class Loader](ClassLoader/docs/classloader.md) uses a class loader to
   import a standard class and examine its methods.
 * [Class Loader Method Call](ClassLoaderMethodCall/docs/classloadermethodcall.md)
   shows how to use a class loader to call methods of a class.
 * [Clocks](Clocks/docs/clocks.md) showcases some of the widgets available
   for displaying the time.
 * [Compass](Compass/docs/compass.md) shows one way to obtain orientation
   information by combining inputs from two sensors.
 * [Environment](Environment/docs/environment.md) demonstrates how to access
   the application's environment in order to access the external public storage
   directory of a device.
 * [Export Calendar](ExportCalendar/docs/exportcalendar.md) shows how to
   access the user's calendar and export individual events to files.
 * [Export Call Log](ExportCallLog/docs/exportcalllog.md) shows how to
   access the user's call log and export individual entries to files.
 * [Export Contacts](ExportContacts/docs/exportcontacts.md) shows how to
   access the user's contacts and export individual entries to files.
 * [Export SMS](ExportSMS/docs/exportsms.md) shows how to access the user's
   SMS messages and export individual messages to files.
 * [Files](Files/docs/files.md) shows how to access external storage. It also
   uses an additional module that provides a custom adapter class.
 * [GPS](GPS/docs/gps.md) uses the Android location APIs to access a device's
   GPS receiver in order to report its current location.
 * [GPS File](GPSFile/docs/gpsfile.md) allows the user to write the current
   location to a file in the external storage area of a device.
 * [HTTP Get](HTTPGet/docs/httpget.md) shows how to use the connectivity
   service to download a file from the Internet.
 * [Light Meter](LightMeter/docs/lightmeter.md) uses a light sensor to measure
   the ambient light level.
 * [Network Info](NetworkInfo/docs/networkinfo.md) displays information about
   the available network interfaces on a device.
 * [Options Menu](OptionsMenu/docs/optionsmenu.md) shows how to attach a menu
   to a device's menu button, if it has one.
 * [Orientation Compass](OrientationCompass/docs/orientationcompass.md) shows
   one way to present information from an orientation sensor.
 * [OS Info](OSInfo/docs/osinfo.md) is a simple tool to display information
   about the operating system on a device.
 * [Pop-up Window](PopupWindow/docs/popupwindow.md) shows how to open, resize
   and dismiss pop-up windows.
 * [Process](Process/docs/process.md) runs a background process and collects
   the output for display in the GUI.
 * [Proximity](Proximity/docs/proximity.md) uses the Android Sensor APIs to
   measure the proximity of a device to its surroundings.
 * [Send SMS](SendSMS/docs/sendsms.md) can be used to send SMS messages on
   devices with access to a cellular network.
 * [Send SMS Notify](SendSMSNotify/docs/sendsmsnotify.md) shows how to send SMS
   messages with notifications.
 * [Sensor List](SensorList/docs/sensorlist.md) shows how to query the system
   services to obtain a sensor manager, using a type cast to access the methods
   of the SensorManager class and list the available sensors on a device.
 * [Telephony Info](TelephonyInfo/docs/telephonyinfo.md) displays information
   about the telephony features of a device, if present.
 * [Widget](Widget/docs/widget.md) is a simple widget that can be installed
   on the home screen of a device.
 * [Widget Service](WidgetService/docs/widget.md) is a simple widget that
   uses a service to update itself periodically.
 * [Widget Updates](WidgetUpdates/docs/widget.md) and
   [Widget Intent Updates](WidgetIntentUpdates/docs/widget.md) show two
   additional ways to update a widget periodically.

Documentation
-------------

Some of the examples are documented using inline docstrings. These are processed
when the examples are built if the `DOCS_DIR` environment variable is set to
the name of a directory. If an absolute path is not used, the name is
interpreted as a subdirectory of the relevant example directory, as in the
following example:

```\
DOCS_DIR=docs PYTHONPATH=. Examples/Serpentine/Compass/build.py \
                           <key.pem> <cert.pem> /tmp/Compass.apk
```

This requires the example's build script to have been written so that it reads
the `DOCS_DIR` environment variable and passes it to the `buildhelper.main`
function. The script uses the `gendocs` module to create an HTML file in the
documentation directory.
