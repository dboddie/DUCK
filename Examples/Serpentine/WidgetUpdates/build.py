#!/usr/bin/env python

"""
Copyright (C) 2016 David Boddie <david@boddie.org.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import os, sys

from DUCK.Tools import buildhelper
from DUCK.Tools.makelayout import ImageView, LinearLayout, TextView

app_name = "Widget Updates"
package_name = "com.example.widgetupdates"
res_files = {
    "drawable": {"preview": "icon.svg"},
    "xml": {"widget": "widget.xml"}
    }
code_file = "widget.py"
include_paths = []
layout = [
    LinearLayout({"android:background": 0x3f800000,
                  "android:gravity": 0x10},         # center vertically
        children=[
            TextView({"android:text": "Updating...",
                      "android:textColor": 0xffffffff,
                      "android:id": "widget_text",
                      "android:gravity": 0x01})     # center horizontally
        ])
    ]
features = []
permissions = []
docs_dir = os.getenv("DOCS_DIR")

if __name__ == "__main__":

    args = sys.argv[:]
    
    result = buildhelper.main(__file__, app_name, package_name, res_files,
        layout, code_file, include_paths, features, permissions, args,
        app_template = "widget", description = {"resource": "widget"},
        docs_dir = docs_dir)
    
    sys.exit(result)
