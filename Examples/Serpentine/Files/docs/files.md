Files Example
=============

This example shows how to access files on external storage media.

![A screenshot of the application.](files.png)

We begin by declaring a unique namespace for the package we are creating.
This is typically based on the reversed form of a domain name and should be
unique to this package.

```python
__package__ = "com.example.files"
```

We also import the classes that will be needed by the application. The most
relevant one is Environment, which gives us access to constants that describe
the locations of resources.

```python
from java.io import File
from android.app import Activity
from android.os import Bundle, Environment
from android.view import ViewGroup
from android.widget import LinearLayout, ListView, TextView
```

We also import an adapter class that will let us expose arrays of strings to
views.

```python
from serpentine.adapters import StringArrayAdapter
```

The FilesActivity class is derived from the standard Activity class and
represents the application.

```python
class FilesActivity(Activity):

    def __init__(self):
    
        Activity.__init__(self)
```

The initialisation method only needs to call the base class's
implementation.

The onCreate method sets up the user interface, creating and populating a
vertical layout and making it the main content view of the activity.

```python
    @args(void, [Bundle])
    def onCreate(self, bundle):
    
        Activity.onCreate(self, bundle)
        
        layout = LinearLayout(self)
        layout.setLayoutParams(ViewGroup.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT))
        layout.setOrientation(LinearLayout.VERTICAL)
```

We obtain a constant that describes the location of images.

```python
        t = Environment.DIRECTORY_DCIM
```

Using the constant, we obtain a File object that corresponds to a
directory on the filing system. We pass this to the addList method to
populate the layout with TextViews that show the names of files in the
directory.

```python
        externalDir = self.getExternalFilesDir(t)
        self.addList(externalDir, layout)
```

This time we obtain a File object for a different external storage
location that we pass to the same method as before, adding more
TextViews to the layout.

```python
        envDir = Environment.getExternalStoragePublicDirectory(t)
        self.addList(envDir, layout)
        
        self.setContentView(layout)
```

This custom method creates a ListView instance for each directory
supplied by our onCreate method that contains files, or a placeholder
TextView widget if no files are found. The view created is added to the
specified layout.

```python
    @args(void, [File, ViewGroup])
    def addList(self, directory, layout):
    
        textView = TextView(self)
        textView.setText(directory.getAbsolutePath())
        textView.setTextColor(0xffff0000)
        layout.addView(textView)
        
        files = directory.list()
        
        if len(files) == 0:
            textView = TextView(self)
            textView.setText("(No files)")
            layout.addView(textView)
        else:
            adapter = StringArrayAdapter(files)
            
            listView = ListView(self)
            listView.setAdapter(adapter)
            layout.addView(listView)
```



## Files

* [icon.svg](../icon.svg)
