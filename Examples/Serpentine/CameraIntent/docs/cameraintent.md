# Camera Intent Example

This example shows how to access the camera on a device and display its output
in a view.

![A screenshot of the application.](cameraintent.png)

Note that, in order to access the camera, the application needs to be built
with the android.hardware.camera feature and android.permission.CAMERA
permission. The build script takes care of this.

We import the classes and modules needed by our application. The most
relevant to this example is the Intent class which we use to communicate with
other activities and services.

```python
from android.app import Activity
from android.content import Context, Intent
from android.graphics import Bitmap
from android.os import Bundle
from android.provider import MediaStore
from android.view import View
from android.widget import Button, ImageView, LinearLayout
```

We define a class based on the standard Activity class. This represents the
application, and will be used to present a graphical interface to the user.

```python
class CameraIntentActivity(Activity):

    __interfaces__ = [View.OnClickListener]
```

We declare that the class implements the View.OnClickListener interface
by adding this interface to the list held by the `__interfaces__`
attribute. To implement this interface, we implement the onClick method.

The initialisation method simply calls the corresponding base class method
to properly initialise the activity.

```python
    def __init__(self):
    
        Activity.__init__(self)
```

Similarly, the onCreate method calls the onCreate method of the base
class to help set up the activity before creating a user interface.

```python
    def onCreate(self, bundle):
    
        Activity.onCreate(self, bundle)
```

We create a button with a "Take picture" label and register the
activity with it as a listener. Since the activity implements the
View.OnClickListener interface, the activity's onClick method will be
called when the user clicks the button.

```python
        button = Button(self)
        button.setText("Take picture")
        button.setOnClickListener(self)
```

We also create an image view for showing pictures obtained from the
camera and a layout to hold both the button and image view. We make the
layout the main view in the activity.

```python
        self.imageView = ImageView(self)
        
        layout = LinearLayout(self)
        layout.setOrientation(LinearLayout.VERTICAL)
        layout.addView(button)
        layout.addView(self.imageView)
        
        self.setContentView(layout)
```

The onClick method is called when the user clicks the button. We create
an intent with the MediaStore.ACTION_IMAGE_CAPTURE action that we pass as
the first argument to the activity's startActivityForResult method. The
second argument is the request code that should be reported if the attempt
to start an activity fails.

```python
    def onClick(self, view):
    
        intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        self.startActivityForResult(intent, 1)
```

The onActivityResult method is called when the activity started in the
previous method exits. The requestCode parameter contains the value
supplied as the second argument to the startActivityForResult method,
the resultCode parameter contains the result code of the helper activity,
and the data parameter contains an Intent from the helper that contains the
data we requested.

In this example we are only interested in a result code of RESULT_OK. In
this case, we obtain the extra data from the Intent passed in the data
parameter. This takes the form of a Bundle object whose "data" entry we
read to extract a Bitmap. We inform the compiler that we expected a Bitmap
by casting the value held by the `bitmap` variable before setting it as the
image to display in the image view created earlier.

```python
    def onActivityResult(self, requestCode, resultCode, data):
    
        if resultCode == self.RESULT_OK:
            extras = data.getExtras()
            bitmap = extras.get("data")
            self.imageView.setImageBitmap(CAST(bitmap, Bitmap))
```



## Files

* [icon.svg](../icon.svg)
