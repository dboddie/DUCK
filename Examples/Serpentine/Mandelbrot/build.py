#!/usr/bin/env python

"""
Copyright (C) 2016 David Boddie <david@boddie.org.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import os, sys

from DUCK.Tools import buildhelper

app_name = "Mandelbrot"
package_name = "com.example.mandelbrot"
res_files = {
    "drawable": {
        "ic_launcher": "icon.ppm",
        "zoom_in": "zoomin.svg",
        "zoom_out": "zoomout.svg"
        }
    }
code_file = "mandelbrot.py"
include_paths = []
layout = None
features = []
permissions = []
docs_dir = os.getenv("DOCS_DIR")

svg = """<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN"
  "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">

<svg version="1.1"
     xmlns="http://www.w3.org/2000/svg"
     xmlns:xlink="http://www.w3.org/1999/xlink"
     width="1.5cm" height="1.5cm"
     viewBox="0 0 100 100">

<rect x="0" y="0" width="100" height="100" stroke="none" fill="black" />
%s
</svg>
"""

def write_ppm(file_name):

    f = open(file_name, "w")

    lines = []
    rows = columns = 72
    f.write("P3 %i %i 255\n" % (rows, columns))
    
    for y in range(rows):
    
        i = 1.5 - (3 * (y + 0.5)/rows)
        for x in range(columns):
        
            r = -2.25 + (3 * (x + 0.5)/columns)
            c = r + i*1j
            count = 0
            
            while count < 16:
                c = c*c + (r + i*1j)
                if abs(c) > 4:
                    break
                count += 1
            
            if count <= 7:
                g = count * 36
            else:
                g = (15 - count) * 36
            
            if count < 16:
                colour = "%i %i %i " % (count * 17, g, 255 - (count * 17))
            else:
                colour = "0 0 0 "
            
            f.write(colour)
        
        f.write("\n")
    
    f.close()

if __name__ == "__main__":

    args = sys.argv[:]
    
    this_dir = os.path.split(__file__)[0]
    write_ppm(os.path.join(this_dir, "icon.ppm"))
    
    result = buildhelper.main(__file__, app_name, package_name, res_files,
        layout, code_file, include_paths, features, permissions, args,
        docs_dir = docs_dir)
    
    os.remove(os.path.join(this_dir, "icon.ppm"))
    sys.exit(result)
