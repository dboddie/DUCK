# Minimal Example

This example is a minimal set of code for an application.

![A screenshot of the application.](minimal.png)

We import the only class needed by our application.

```python
from android.app import Activity
```

The MinimalActivity class is derived from the standard Activity class and
represents the application. Android will create an instance of this class when
the user runs it.

```python
class MinimalActivity(Activity):

    def __init__(self):
    
        Activity.__init__(self)
```

The initialisation method simply calls the corresponding method in the base
class and does nothing else.

When run, the application displays an empty window. Exit it in the usual way.

## Files

* [icon.svg](../icon.svg)
