# Action Bar Example

This example shows how to access an activity's action bar and enable its list
navigation mode, placing a drop-down list in it for selecting different items
of content.

![A screenshot of the application.](actionbarlist.png)

We import the classes and modules that will be needed by the application. The
Window class is particularly relevant since we use a constant it defines to
request access to the action bar feature.

```python
from java.lang import String
from android.app import Activity, ActionBar, Context, Fragment
from android.os import Bundle
from android.view import LayoutInflater, View, ViewGroup, Window
from android.util import TypedValue
from android.widget import TextView
```

We also import the R object from the special app_resources module that is
created by the build script. This provides constants that we use to refer to
resources bundled in the application's package.

```python
import android.R
```

We also import an adapters module containing a class that we want to extend.

```python
import serpentine.adapters
```

We define a class based on the standard Activity class. This represents the
application, and will be used to present a graphical interface to the user.

```python
class ActionBarListActivity(Activity):

    __interfaces__ = [ActionBar.OnNavigationListener]
```

We declare that the class implements the ActionBar.OnNavigationListener
interface. Implementing this interface involves implementing the
onNavigationItemSelected method.

The initialisation method simply calls the corresponding base class
method to properly initialise the activity.

```python
    def __init__(self):
    
        Activity.__init__(self)
```

Similarly, the onCreate method calls the onCreate method of the base
class to help set up the activity before creating a user interface.

```python
    @args(void, [Bundle])
    def onCreate(self, bundle):
    
        Activity.onCreate(self, bundle)
        
        self.setTheme(android.R.style.Theme_DeviceDefault)
```

We obtain the window shown by the activity and use its
requestFeature method to request an action bar.

```python
        window = self.getWindow()
        window.requestFeature(Window.FEATURE_ACTION_BAR)
```

Having requested an action bar feature, we attempt to obtain its
corresponding object. If one exists, we set its title and navigation
mode.

```python
        bar = self.getActionBar()
        
        if bar != None:
            bar.setTitle("Action Bar List")
            bar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST)
            
            adapter = StringListAdapter(["Item 1", "Item 2", "Item 3"])
            bar.setListNavigationCallbacks(adapter, self)
```

We create a view that we use to display the main content of the
activity. This will be updated when the user selects a list item.

```python
        self.textView = TextView(self)
        self.setContentView(self.textView)
    
    @args(bool, [int, long])
    def onNavigationItemSelected(self, itemPosition, itemId):
    
        if itemPosition == 0:
            self.textView.setText("Hello world!")
        elif itemPosition == 1:
            self.textView.setText("Hello again!")
        elif itemPosition == 2:
            self.textView.setText("This is item 3's text.")
        else:
            self.textView.setText("Out of bounds!")
        
        return True
```

We extend an adapter class supplied with the compiler to customise how its
contents is presented when displayed in a drop-down menu.

```python
class StringListAdapter(serpentine.adapters.StringListAdapter):

    def __init__(self, strings):
        serpentine.adapters.StringListAdapter.__init__(self, strings)
```

We need to provide an initialisation method to ensure that instances of
this class can be created. Since we don't do anything extra to set up
instances of this class, we merely call the corresponding method in the
base class.

Since the adapter is used to return a view that is used in a drop-down
menu, we implement this method in addition to the getView method, enabling
us to return a view that is more appropriate for that use. If this method
is not implemented, views returned by the getView method will be used
instead.

```python
    @args(View, [int, View, ViewGroup])
    def getDropDownView(self, position, convertView, parent):
    
        view = TextView(parent.getContext())
        view.setText(self.items[position])
        view.setTextSize(TypedValue.COMPLEX_UNIT_PX, view.getTextSize() * 1.5)
        return view
```



## Files

* [icon.svg](../icon.svg)
