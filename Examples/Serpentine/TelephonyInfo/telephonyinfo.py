"""
# Telephony Info Example

This example shows how to access the telephony manager on a device and extract
information about the features available.

![A screenshot of the application.](telephonyinfo.png)

We begin by importing the classes and modules needed by our application. The
most relevant ones for this example are those from the android.telephony
module."""

from android.app import Activity
from android.content import Context
from android.os import Build
from android.telephony import TelephonyManager
from android.widget import ListView, TextView

"""We also import an adapter class that will let us expose lists of strings to
views."""

from serpentine.adapters import StringListAdapter

"""The TelephonyInfoActivity is derived from the standard Activity class and
represents the application. Android will create an instance of this class when
the user runs it."""

class TelephonyInfoActivity(Activity):

    def __init__(self):
    
        Activity.__init__(self)
    
    """The initialisation method only needs to call the corresponding method in
    the base class.
    
    The onCreate method is called when the activity is created. Our
    implementation calls the onCreate method of the base class, queries the
    available telephony features and displays them in a graphical layout."""
    
    def onCreate(self, bundle):
    
        Activity.onCreate(self, bundle)
        
        """Information about the available features is obtained from the
        device's telephony service. This is obtained using the getSystemService
        method. The instance we obtain from this method needs to be cast to a
        suitable type so that we can access its methods."""
        
        service = self.getSystemService(Context.TELEPHONY_SERVICE)
        if service == None:
            errorLabel = TextView(self)
            errorLabel.setText("No Telephony Manager found.")
            self.setContentView(errorLabel)
            return
        
        telephonyManager = CAST(service, TelephonyManager)
        
        """We create a list of strings for use in an adapter. Each item is a
        label concatenated to the corresponding piece of information from the
        TelephonyManager class."""
        
        stringlist = []
        
        stringlist.add("Device ID: " + str(telephonyManager.getDeviceId()))
        
        stringlist.add("Software Version: " + str(telephonyManager.getDeviceSoftwareVersion()))
        
        phoneType = ["None", "GSM", "CDMA", "SIP"][telephonyManager.getPhoneType()]
        stringlist.add("Phone Type: " + phoneType)
        stringlist.add("Phone Count: " + str(telephonyManager.getPhoneCount()))
        
        stringlist.add("Location: " + str(telephonyManager.getCellLocation()))
        
        stringlist.add("Network Country: " + str(telephonyManager.getNetworkCountryIso()))
        stringlist.add("Network Operator: " + str(telephonyManager.getNetworkOperator()))
        stringlist.add("Network Operator Name: " + str(telephonyManager.getNetworkOperatorName()))
        
        stringlist.add("SIM Country: " + str(telephonyManager.getSimCountryIso()))
        if telephonyManager.getSimState() == TelephonyManager.SIM_STATE_READY:
            stringlist.add("SIM Operator: " + telephonyManager.getSimOperator())
            stringlist.add("SIM Operator Name: " + telephonyManager.getSimOperatorName())
        
        stringlist.add("SIM Serial Number: " + str(telephonyManager.getSimSerialNumber()))
        
        if Build.VERSION.SDK_INT >= 22:
            stringlist.add("Carrier Privileges: " + str(telephonyManager.hasCarrierPrivileges()))
        
        if Build.VERSION.SDK_INT >= 18:
            groupId = telephonyManager.getGroupIdLevel1()
            if groupId != None:
                stringlist.add("Group Identifier Level 1: " + groupId)
        
        stringlist.add("Line 1 Number: " + str(telephonyManager.getLine1Number()))
        
        stringlist.add("Subscriber ID: " + str(telephonyManager.getSubscriberId()))
        
        if telephonyManager.hasIccCard():
            stringlist.add("Has ICC Card")
        
        """We create a custom adapter that exposes the list to other components
        and a ListView that we connect it to. This causes the contents of the
        list to be displayed in the view."""
        
        adapter = StringListAdapter(stringlist)
        view = ListView(self)
        view.setAdapter(adapter)
        
        """The view is used as the main user interface in the activity."""
        
        self.setContentView(view)
