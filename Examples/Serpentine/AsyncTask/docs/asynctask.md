# Async Task Example

This example shows one way to perform processing in a background thread.

![A screenshot of the application.](asynctask.png)

We import the classes and modules needed by our application. The most relevant
to this example is the AsyncTask class which enables background processing to
be initiated by the main thread.

```python
from java.lang import Integer, Object, String
from android.app import Activity
from android.os import AsyncTask, Bundle
from android.view import View
from android.widget import TextView
```

We define a class based on the standard Activity class. This represents the
application, and will be used to present a graphical interface to the user.

```python
class AsyncTaskActivity(Activity):

    __interfaces__ = [View.OnClickListener]
```

The class declares that it implements the View.OnClickListener interface
by including it the `__interfaces__` attribute. This requires that we
implement the onClick method.

The initialisation method simply calls the corresponding base class method
to properly initialise the activity.

```python
    def __init__(self):
    
        Activity.__init__(self)
```

Similarly, the onCreate method calls the onCreate method of the base
class to help set up the activity. We create a TextView and register the
activity as the listener for it so that we can handle clicks on it in the
onClick method.

```python
    @args(void, [Bundle])
    def onCreate(self, bundle):
    
        Activity.onCreate(self, bundle)
        
        self.view = TextView(self)
        self.view.setOnClickListener(self)
        self.view.setText("Click me!")
        
        self.setContentView(self.view)
```

The onClick method is called when the user clicks on the TextView.
We create an custom Task instance, passing the TextView instance to it for
later use, and we call its execute method, passing an array of Objects for
processing.

```python
    @args(void, [View])
    def onClick(self, view):
    
        l = array([1, 2, 3])
        self.task = Task(self.view)
        self.task.execute(l)
```

We define a Task class based on the standard AsyncTask class. This
encapsulates the processing that we want to perform in the background.
The equivalent Java class is a template class with the following parameters:
Params, Progress and Result.

```python
class Task(AsyncTask):

    __item_types__ = [int, int, String]
```

The `__item_types__` attribute defines the types of the three parameters
defining our specialisation of the AsyncTask class: Params, Progress and
Result.

The initialisation method accepts the TextView object that we want to
update, storing it for later reference, and calls the base class method.

```python
    @args(void, [TextView])
    def __init__(self, view):
    
        AsyncTask.__init__(self)
        self.view = view
```

We define the doInBackground method with the code we want to be
executed in the background. The method is called with an array of
parameters of the first type declared in the `__item_types__` attribute and
we return a value of the third type in the `__item_types__` attribute.
In this example, we simply concatenate string representations of the
integers in the list supplied and return the resulting string.

```python
    @args(Result, [[Params]])
    def doInBackground(self, params):
    
        s = ""
        i = 0
        while i < len(params):
            s += str(params[i]) + "\n"
            i += 1
        
        return s
```

The onPostExecute method is called with the value we returned from the
previous method. This lets us decide how the result of the processing is
used. Here, we display the string we created in the previous method in the
TextView instance that we passed to the Task instance on its creation.

```python
    @args(void, [Result])
    def onPostExecute(self, result):
    
        self.view.setText(result)
```



## Files

* [icon.svg](../icon.svg)
