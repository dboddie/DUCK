# HTTP Post Example

This example shows how to perform a HTTP POST request.

![A screenshot of the application.](httppost.png)

We import the classes and modules that will be needed by the application. The
most relevant are the classes from the java.net module.

```python
from java.lang import Exception, Math, String
from java.io import BufferedInputStream, FileNotFoundException, IOException
from java.net import HttpURLConnection, URL, URLEncoder
from android.app import Activity
from android.content import Context
from android.os import Build
from android.view import View, ViewGroup
from android.widget import Button, EditText, LinearLayout, ScrollView, TextView, Toast
from android.net import ConnectivityManager
```

We define a class based on the standard Activity class. This represents the
application, and will be used to present a graphical interface to the user.

```python
class HTTPPostActivity(Activity):

    __interfaces__ = [View.OnClickListener]
```

The activity implements an interface to handle clicks on views.

The initialisation method simply calls the base class implementation.

```python
    def __init__(self):
        Activity.__init__(self)
```

The onCreate method calls the base class implementation before obtaining
a reference to the connectivity service and setting up the user interface.

```python
    def onCreate(self, bundle):
    
        Activity.onCreate(self, bundle)
```

We obtain an object that gives us access to the connectivity service.
Since the method used to obtain this object does not specify the
specific type of the manager object, we need to cast it explicitly to
the correct type so that we can access its methods.

```python
        service = self.getSystemService(Context.CONNECTIVITY_SERVICE)
        self.manager = CAST(service, ConnectivityManager)
```

We set up the user interface: an editable field to contain some text,
a button that is used to send the text to a website, and a view that is
used to display the response.

```python
        self.textInput = EditText(self)
        
        self.button = Button(self)
        self.button.setText("Post (HTTP POST)")
        self.button.setOnClickListener(self)
        
        self.responseView = TextView(self)
        scrollView = ScrollView(self)
        scrollView.addView(self.responseView)
```

The views are placed in a layout that is the main content shown by
the activity.

```python
        layout = LinearLayout(self)
        layout.setLayoutParams(ViewGroup.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT))
        layout.setOrientation(LinearLayout.VERTICAL)
        
        layout.addView(self.textInput)
        layout.addView(self.button)
        layout.addView(scrollView)
        
        self.setContentView(layout)
```

We implement the onClick method to provide the View.OnClickListener
interface that we declared earlier. This responds to clicks on the button
that we created in the previous method.

```python
    def onClick(self, view):
```

Currently, we have only implemented support for older versions of
the connectivity API, so we check for those before checking whether the
device has access to the Internet.

```python
        if Build.VERSION.SDK_INT < 23:
            info = self.manager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE)
            if info == None or not info.isConnected():
                Toast.makeText(self, "Not connected", Toast.LENGTH_SHORT).show()
                return
        else:
            Toast.makeText(self, "Not supported\nPlease update the example code.", Toast.LENGTH_SHORT).show()
            return
```

If the device is able to access online resources, we use it to post
some data to a website. We specify the URL of the resource and use the
URL object to open a connection to the server hosting it. We cast the
connection object to a HttpURLConnection object so that we can enable
request redirections. Enabling output causes a POST request to get sent
instead of a GET request.

```python
        url = URL("https://httpbin.org/post")
        connection = CAST(url.openConnection(), HttpURLConnection)
        connection.setDoOutput(True)
        connection.setInstanceFollowRedirects(True)
```

We construct a query with some hard-coded values, but also encode
the text from the editable view in the comment field.

```python
        text = str(self.textInput.getText())
        query = String.format("custname=DUCK&size=medium&topping=cheese&comment=%s",
                              [URLEncoder.encode(text, "UTF-8")].toArray())
```

The encoded query is written to the connection's output stream,
causing it to be sent to the server.

```python
        try:
            out = connection.getOutputStream()
            out.write(query.getBytes("UTF-8"))
        except IOException:
            Toast.makeText(self, "Cannot send data", Toast.LENGTH_SHORT).show()
            return
```

We get the response from the server to check that it was received
and valid.

```python
        try:
            msg = connection.getResponseMessage()
        except IOException:
            msg = "Failed to get a response"
        
        if msg != "OK":
            Toast.makeText(self, "Failed to get a response", Toast.LENGTH_SHORT).show()
            return
```

We obtain the length of the content from the connection and check to
see if it is reasonable, showing a message and returning if not.

```python
        length = connection.getContentLength()
        
        if length <= 0:
            Toast.makeText(self, "Invalid content length", Toast.LENGTH_SHORT).show()
            return
```

We use a stream to read the content, storing it in an array of the
required length or showing an error message and returning if the stream
cannot be opened.

```python
        try:
            stream = BufferedInputStream(connection.getInputStream())
        except FileNotFoundException:
            Toast.makeText(self, "Resource not found", Toast.LENGTH_SHORT).show()
            return
        
        a = array(byte, length)
        total = 0
```

We loop until we have read all the content, storing new data after
that we have already read, and requesting as much as possible. When all
the data has been read, we close the stream.

```python
        while total < length:
            total += stream.read(a, total, length - total)
        
        stream.close()
```

Finally, we show the content returned, assuming that it is encoded
as UTF-8.

```python
        self.responseView.setText(String(a, "UTF-8"))
```



## Files

* [icon.svg](../icon.svg)
