# View Switcher Example

This example shows how to switch between two views.

![A screenshot of the application.](viewswitcher.png)

We import the classes and modules that will be needed by the application. The
most relevant is the ViewSwitcher classes from the android.widget module.

```python
from android.app import Activity
from android.view import View
from android.widget import ImageView, TextView, ViewSwitcher

from app_resources import R
```

The ViewActivity is derived from the standard Activity class and represents
the application. Android will create an instance of this class when the user
runs it, and the activity will present a graphical interface to the user.

```python
class ViewSwitcherActivity(Activity):
```

The class declares that it implements the View.OnClickListener interface
by including it the `__interfaces__` attribute. This requires that we
implement the onClick method.

```python
    __interfaces__ = [View.OnClickListener]
```

The initialisation method simply calls the corresponding method in the
base class. This must be done even if no other code is included in the
method.

```python
    def __init__(self):
    
        Activity.__init__(self)
```

The onCreate method is called when the activity is created by Android.

```python
    def onCreate(self, bundle):
```

As with the __init__ method, we must call the corresponding method
in the base class.

```python
        Activity.onCreate(self, bundle)
```

The user interface is an instance of the ViewSwitcher class. As with
other standard views, we pass the activity instance as an argument to
the class when we create it. We also create two views to display in the
switcher.

```python
        self.switcher = ViewSwitcher(self)
        self.switcher.setOnClickListener(self)
        
        textView = TextView(self)
        textView.setText("Hello world!\nClick me to show the other view.")
        self.switcher.addView(textView)
        
        imageView = ImageView(self)
        imageView.setImageResource(R.drawable.ic_launcher)
        imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE)
        self.switcher.addView(imageView)
```

We set the switcher as the main view in the activity.

```python
        self.setContentView(self.switcher)
```

The onClick method is called when the user clicks on the ViewSwitcher.
It simply calls the switcher's `showNext` method to switch between the
two views.

```python
    def onClick(self, view):
    
        self.switcher.showNext()
```



## Files

* [icon.svg](../icon.svg)
