"""
# GPS Example

This example shows how to access information from a device's GPS receiver in
order to report its status and location.

![A screenshot of the application.](gps.png)

We import the classes and modules that will be needed by the application. The
most relevant are the classes from the android.location module."""

from java.lang import String
from android.app import Activity
from android.content import Context
from android.location import GpsStatus, Location, LocationListener, \
                             LocationManager
from android.os import Bundle
from android.view import View, ViewGroup
from android.widget import ImageView, LinearLayout, ScrollView, TextView

"""We define a class based on the standard Activity class. This represents the
application, and will be used to present a graphical interface to the user."""

class GPSActivity(Activity):

    __interfaces__ = [GpsStatus.Listener, LocationListener]
    
    """The activity implements two interfaces: one which handles changes to the
    GPS status and another which handles general notifications about location.
    
    The initialisation method simply calls the corresponding method in the
    base class and initialises two variables that are used to keep a textual
    log of events and record the old connection status."""
    
    def __init__(self):
    
        Activity.__init__(self)
        self.log = ""
        self.oldStatus = 0
    
    """The onCreate method calls the corresponding base class method before
    obtaining an instance of the location service manager and setting up the
    user interface."""
    
    def onCreate(self, bundle):
    
        Activity.onCreate(self, bundle)
        
        """We obtain a location service manager object and cast it to the
        correct type so that we can call its addGpsStatusListener method to
        register the activity as a listener. This requires us to implement the
        GpsStatus.Listener interface that we declared earlier."""
        
        self.manager = CAST(self.getSystemService(Context.LOCATION_SERVICE), LocationManager)
        self.manager.addGpsStatusListener(self)
        
        """We create a user interface that contains TextViews showing the
        currently known location, the number of satellites visible to the
        device and a log of events."""
        
        self.locationView = TextView(self)
        self.locationView.setBackgroundColor(0xff404040)
        self.locationView.setTextColor(0xffffffff)
        self.locationView.setText("No location")
        
        self.satellites = TextView(self)
        self.satellites.setText("Satellites: 0")
        
        self.statusView = TextView(self)
        scrollView = ScrollView(self)
        scrollView.addView(self.statusView)
        
        layout = LinearLayout(self)
        layout.setLayoutParams(ViewGroup.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT))
        layout.setOrientation(LinearLayout.VERTICAL)
        
        layout.addView(self.locationView)
        layout.addView(self.satellites)
        layout.addView(scrollView)
        
        self.setContentView(layout)
    
    """The onResume method is called when the activity is first started, or
    when the user navigates to it. We call the corresponding base class method
    and request notification of location updates every 500 milliseconds and if
    the device moves more than 1 metre."""
    
    def onResume(self):
    
        Activity.onResume(self)
        
        self.manager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
            long(500), float(1), self)
    
    """The onPause method is called when the user navigates away from the
    activity. We call the corresponding base class method and remove our
    request for location updates."""
    
    def onPause(self):
    
        Activity.onPause(self)
        
        self.manager.removeUpdates(self)
    
    """The following four methods are part of the LocationListener interface.
    
    We implement the onLocationChanged method to handle location changes, using
    the Location object supplied to update the location text displayed in the
    user interface."""
    
    def onLocationChanged(self, location):
    
        self.locationView.setText(
            "Latitude: " + location.convert(location.getLatitude(), Location.FORMAT_SECONDS) + \
            " Longitude: " + \
            location.convert(location.getLongitude(), Location.FORMAT_SECONDS)
            )
    
    """In the onProviderDisabled method, we simply report that a location
    provider has been disabled, updating the status TextView."""
    
    def onProviderDisabled(self, provider):
    
        self.addText(provider + " disabled")
    
    """Similarly, in the onProviderEnabled method, we report that a location
    provider has been enabled, updating the status TextView, and request
    updates of half a second in frequency and for changes of 1 metre or more."""
    
    def onProviderEnabled(self, provider):
    
        self.addText(provider + " enabled")
        self.manager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
            long(500), float(1), self)
    
    """The onStatusChanged method is called to inform the activity about
    changes to a location provider. In this example, we only report that
    something about the provider changed."""
    
    def onStatusChanged(self, provider, status, extras):
    
        self.addText(provider + " changed")
    
    """The onGpsStatusChanged method is required by an implementation of the
    GpsStatus.Listener interface, declared earlier.
    
    We only care about changes to the status, only reporting any changes if the
    newly delivered event differs from the old one."""
    
    def onGpsStatusChanged(self, event):
    
        if event != self.oldStatus:
        
            self.oldStatus = event
            self.addText("GPS Status: " + str(event))
            
            """Additionally, if the event indicated a change in the number of
            visible satellites, we obtain an iterable containing information
            about each of then in order to count them, reporting their number
            in the relevant TextView."""
            
            if event == GpsStatus.GPS_EVENT_SATELLITE_STATUS:
            
                status = self.manager.getGpsStatus(None)
                sats = status.getSatellites()
                
                i = 0
                for sat in sats:
                    i += 1
                
                self.satellites.setText("Satellites: " + str(i))
    
    """The addText method is a custom method that we use to append lines of
    text to the event log in the activity's user interface. The method accepts
    a string that it appends to the existing log text before updating the
    TextView that displays the log."""
    
    @args(void, [String])
    def addText(self, text):
    
        self.log += text + "\n"
        self.statusView.setText(self.log)
