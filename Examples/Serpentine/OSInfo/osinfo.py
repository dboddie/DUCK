"""
# OS Info Example

This example displays information about a device's operating system in a
scrolling list.

![A screenshot of the application.](osinfo.png)

We import the classes and modules needed by our application. The most
relevant class to this example is the Build class which exposes information
about the configuration used when the operating system was built."""

from java.lang import Object, String
from java.util import ArrayList, List
from android.app import Activity
from android.os import Build, Bundle
from android.view import View, ViewGroup
from android.widget import Adapter, BaseAdapter, ListView, TextView

"""We also import an adapter class that will let us expose lists of strings to
views."""

from serpentine.adapters import StringListAdapter

"""The OSInfoActivity class is derived from the standard Activity class and
represents the application. Android will create an instance of this class when
the user runs it."""

class OSInfoActivity(Activity):

    def __init__(self):
    
        Activity.__init__(self)
    
    """The initialisation method simply calls the corresponding method in the
    base class.

    The onCreate method is called when the activity is created. We call the
    corresponding method of the base class to set up the application before
    creating the user interface."""
    
    @args(void, [Bundle])
    def onCreate(self, bundle):
    
        Activity.onCreate(self, bundle)
        
        """We create a list of strings for use in an adapter. Each item is a
        label concatenated to the corresponding piece of information from the
        Build class."""
        
        stringlist = [
            "Board: " + Build.BOARD,
            "BOOTLOADER: " + Build.BOOTLOADER,
            "BRAND: " + Build.BRAND,
            "CPU_ABI: " + Build.CPU_ABI,
            "CPU_ABI2: " + Build.CPU_ABI2,
            "DEVICE: " + Build.DEVICE,
            "DISPLAY: " + Build.DISPLAY,
            "FINGERPRINT: " + Build.FINGERPRINT,
            "HARDWARE: " + Build.HARDWARE,
            "HOST: " + Build.HOST,
            "ID: " + Build.ID,
            "MANUFACTURER: " + Build.MANUFACTURER,
            "MODEL: " + Build.MODEL,
            "PRODUCT: " + Build.PRODUCT,
            "RADIO: " + Build.RADIO,
            "SERIAL: " + Build.SERIAL,
            "TAGS: " + Build.TAGS,
            "TIME: " + str(Build.TIME),
            "TYPE: " + Build.TYPE,
            "USER: " + Build.USER,
            "",
            "CODENAME: " + Build.VERSION.CODENAME,
            "INCREMENTAL: " + Build.VERSION.INCREMENTAL,
            "RELEASE: " + Build.VERSION.RELEASE,
            "SDK: " + Build.VERSION.SDK,
            "SDK_INT: " + str(Build.VERSION.SDK_INT),
            "",
            "RadioVersion: " + Build.getRadioVersion()
            ]
        
        """We create a custom adapter that exposes the list to other components
        and a ListView that we connect it to. This causes the contents of the
        list to be displayed in the view."""
        
        adapter = StringListAdapter(stringlist)
        view = ListView(self)
        view.setAdapter(adapter)
        
        self.setContentView(view)

"""The view is used as the main user interface in the activity."""
