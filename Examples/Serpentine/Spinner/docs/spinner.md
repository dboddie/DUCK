# Spinner Example

This example shows how to use a spinner to display a list of strings that the
user can choose from.

![A screenshot of the application.](spinner.png)

We import the classes and modules that will be needed by the application. The
most relevant are the classes from the android.widget module.

```python
from java.lang import String, Object
from java.util import List
from android.app import Activity
from android.content import Context
from android.os import Bundle
from android.view import View, ViewGroup
from android.widget import AdapterView, LinearLayout, Spinner, TextView
```

We also import the R object from the special app_resources module that is
created by the build script. This provides constants that we use to refer to
resources bundled in the application's package.

```python
import android.R
```

We also import an adapter class that will let us expose lists of strings to
views.

```python
from serpentine.adapters import StringListAdapter
```

The SpinnerActivity class is derived from the standard Activity class and
represents the application. Android will create an instance of this class when
the user runs it.

```python
class SpinnerActivity(Activity):

    __interfaces__ = [AdapterView.OnItemSelectedListener]
```

The class implements the AdapterView.OnItemSelectedListener interface in
order to respond to user interaction with the spinner.

The initialisation method simply calls the corresponding method in the
base class.

```python
    def __init__(self):
    
        Activity.__init__(self)
```

The onCreate method is called when the activity is created. We call the
onCreate method of the base class with the Bundle object passed to this
method to set up the application. To ensure that the user interface looks
correct, we set the theme used to the device's default.

```python
    @args(void, [Bundle])
    def onCreate(self, bundle):
    
        Activity.onCreate(self, bundle)
        
        self.setTheme(android.R.style.Theme_DeviceDefault)
```

We create a Spinner view, passing the activity as its context, and
create an adapter for use with it. The adapter is initialised with a
list of example strings. We pass it to the spinner in order to display
the strings.

```python
        spinner = Spinner(self)
        self.adapter = StringListAdapter(["Item 1", "Item 2", "Item 3"])
        spinner.setAdapter(self.adapter)
```

We tell the spinner that the Activity instance will handle item
selection. This causes the methods defined by the OnItemSelectedListener
interface to be called when the user selects items shown by the spinner.
We also create a TextView to show messages in the main part of the
activity's window.

```python
        spinner.setOnItemSelectedListener(self)
        
        self.textView = TextView(self)
```

Finally, we create a layout in which to contain the spinner and text
view, which we make the main view in the activity.

```python
        layout = LinearLayout(self)
        layout.setOrientation(layout.VERTICAL)
        layout.addView(spinner)
        layout.addView(self.textView)
        
        self.setContentView(layout)
```

The next two methods are the implementation of the
AdapterView.OnItemSelectedListener interface.

When an item in one of the spinners is selected, this method is called with
the parent view (the spinner in this case), the view displaying the item
data, the position of the item in the spinner and the item's ID. We use
the position to extract data from the adapter and show it in the text view.

```python
    def onItemSelected(self, parent, view, position, id):
    
        self.textView.setText(self.adapter.items[position] + " was selected.")
```

If no item is selected then we do nothing.

```python
    def onNothingSelected(self, parent):
        pass
```



## Files

* [icon.svg](../icon.svg)
