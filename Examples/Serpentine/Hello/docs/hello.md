Hello Example
=============

This is a simple "Hello world!" example, written in Serpentine.

![A screenshot of the application.](hello.png)

We begin by declaring a unique namespace for the package we are creating.
This is typically based on the reversed form of a domain name and should be
unique to this package.

```python
__package__ = "com.example.hello"
```

We also import the classes that will be needed by the application. Since
this is a simple example, we only need three of them.

```python
from android.app import Activity
from android.os import Bundle
from android.widget import TextView
```

We define a class based on the standard Activity class. This represents the
application, and will be used to present a graphical interface to the user.

The initialisation method simply calls the corresponding method in the
base class. This must be done even if no other code is included in the
method.

```python
class HelloActivity(Activity):

    def __init__(self):
    
        Activity.__init__(self)
```

The onCreate method is called when the activity is created by Android.
The return type and parameter types expected by the method are declared
using the following decorator. Since the parameter types for the onCreate
method are defined in the Activity base class, this decorator is not
strictly necessary.

```python
    @args(void, [Bundle])
    def onCreate(self, bundle):
    
        Activity.onCreate(self, bundle)
        
        view = TextView(self)
        view.setText("Hello world!")
        self.setContentView(view)
```

As with the `__init__` method, we must call the corresponding method in the
base class.

The user interface for the application is simply a TextView widget
that displays a string of text. The final call ensures that the widget
is used as the main view for the application.

## Files

* [icon.svg](../icon.svg)
