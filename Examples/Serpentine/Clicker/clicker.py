"""
Clicker Example
===============

This is a simple "Hello world!" example that responds to the user clicking a
button.

![A screenshot of the application.](clicker.png)

We import the classes and modules that will be needed by the application. The
most relevant are the Button and View classes."""

from java.lang import Object, String
from android.app import Activity
import android.content
import android.os
from android.view import View, ViewGroup
from android.widget import Button, LinearLayout, TextView

"""We define a class based on the standard Activity class. This represents the
application, and will be used to present a graphical interface to the user.

The initialisation method simply calls the corresponding method in the base
class. Note that we cannot instantiate views here because they need the base
class's onCreate method to have been called first."""

class ClickerActivity(Activity):

    def __init__(self):
    
        Activity.__init__(self)
    
    """The onCreate method is called with a Bundle object that lets the
    activity restore its state from the last time it was run. Here, we simply
    pass the bundle to the onCreate method in the base class."""
    
    @args(void, [android.os.Bundle])
    def onCreate(self, bundle):
    
        Activity.onCreate(self, bundle)
        
        """At this point it is possible to create a user interface using views
        and widgets. We create a vertical layout and place a button and a text
        view in it."""
        
        layout = LinearLayout(self)
        layout.setOrientation(LinearLayout.VERTICAL)
        layout.setLayoutParams(ViewGroup.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT))
        
        button = Button(self)
        button.setText("Click me")
        
        self.textView = TextView(self)
        self.textView.setText("Hello World!")
        
        layout.addView(button)
        layout.addView(self.textView)
        
        """In order to respond to clicks on the button, we create a listener
        object that we register with the button. This will cause the listener's
        onClick method to be called when the button is clicked. The Listener
        class is defined below."""
        
        listener = ClickerActivity.Listener(self, self.textView)
        button.setOnClickListener(listener)
        
        self.setContentView(layout)
    
    """The layout is the main content in the activity.
    
    The Listener class is a custom inner class derived from the Java Object
    class that we use to respond to clicks. We are notified about clicks when
    the onClick method is called."""
    
    """We declare that the class implements the View.OnClickListener interface
    by adding this interface to the list held by the `__interfaces__` attribute.
    To implement this interface, we implement the onClick method."""
    
    class Listener(Object):
    
        __interfaces__ = [View.OnClickListener]
        
        """The initialisation method accepts two arguments: a Context and a
        TextView. The first is used to initialise all views - we pass it to the
        corresponding base class method. The second is the view that we will
        modify when the onClick callback method is called.
        
        We store the TextView for later use and initialise a counter."""
        
        @args(void, [android.content.Context, TextView])
        def __init__(self, context, textView):
        
            Object.__init__(self)
            
            self.textView = textView
            self.counter = 0
        
        """The onClick method is called when the user clicks the button defined
        above. We simply increment the counter and append a string with its
        value to the text displayed by the text view."""
        
        @args(void, [View])
        def onClick(self, view):
        
            self.counter += 1
            s = str(self.textView.getText()) + " " + str(self.counter)
            self.textView.setText(s)

"""This example uses a separate class to handle clicks, but it is also possible
to make the activity itself handle them. This is the way clicks are handled in
the [Audio Track](../../AudioTrack/audiotrack.py) example."""
