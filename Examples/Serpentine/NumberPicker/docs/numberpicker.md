# Number Picker Example

This example shows how to use a spinner to display a list of strings that the
user can choose from.

![A screenshot of the application.](numberpicker.png)

We import the classes and modules that will be needed by the application. The
most relevant are the classes from the android.widget module.

```python
from java.lang import String, Object
from java.util import List
from android.app import Activity
from android.widget import LinearLayout, NumberPicker, TextView
```

We also import the R object from the special app_resources module that is
created by the build script. This provides constants that we use to refer to
resources bundled in the application's package.

```python
import android.R
```

The NumberPickerActivity class is derived from the standard Activity class and
represents the application. Android will create an instance of this class when
the user runs it.

```python
class NumberPickerActivity(Activity):

    __interfaces__ = [NumberPicker.OnValueChangeListener]
```

The class implements the NumberPicker.OnValueChangeListener interface
in order to respond to user interaction with the number picker.

The initialisation method simply calls the corresponding method in the
base class.

```python
    def __init__(self):
    
        Activity.__init__(self)
```

The onCreate method is called when the activity is created. We call the
onCreate method of the base class with the Bundle object passed to this
method to set up the application. To ensure that the user interface looks
correct, we set the theme used to the device's default.

```python
    def onCreate(self, bundle):
    
        Activity.onCreate(self, bundle)
        
        self.setTheme(android.R.style.Theme_DeviceDefault)
```

We create a NumberPicker view, passing the activity as its context, and
create an adapter for use with it. The adapter is initialised with a
list of example strings. We pass it to the number picker in order to display
the strings.

```python
        picker = NumberPicker(self)
        picker.setMinValue(0)
        picker.setMaxValue(100)
```

We tell the number picker that the Activity instance will handle
notifications about changes to the value shown. This causes the methods
defined by the OnValueChangeListener interface to be called when the
user selects items shown by the number picker. Note that the name is
slightly different from the name of the interface.

```python
        picker.setOnValueChangedListener(self)
```

We also create a TextView to show messages in the main part of the
activity's window.

```python
        self.textView = TextView(self)
```

Finally, we create a layout in which to contain the spinner and text
view, which we make the main view in the activity.

```python
        layout = LinearLayout(self)
        layout.setOrientation(layout.VERTICAL)
        layout.addView(picker)
        layout.addView(self.textView)
        
        self.setContentView(layout)
```

The next two methods are the implementation of the
NumberPicker.OnValueChangeListener interface.

When an item in the number picker is selected, this method is called with
the parent view (the number picker in this case), the old value and the new
value.

```python
    def onValueChange(self, parent, oldValue, newValue):
    
        self.textView.setText("Old value: " + str(oldValue) + "\n"
                              "New value: " + str(newValue))
```



## Files

* [icon.svg](../icon.svg)
