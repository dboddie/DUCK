# GPS File Example

This example shows how to access information from a device's GPS receiver and
write it to a file in a device's external storage area.

![A screenshot of the application.](gpsfile.png)

We import the classes and modules that will be needed by the application. The
most relevant are the classes from the android.location module.

```python
from java.lang import String
from android.app import Activity
from android.content import Context
from android.location import GpsStatus, Location, LocationListener, \
                             LocationManager
from android.os import Bundle
from android.view import View, ViewGroup
from android.widget import ImageView, LinearLayout, Space, TextView
```

We import the the following module so that we can specify the theme of the
application.

```python
import android.R
```

We also import a custom class from the [entrywidget module](entrywidget.html)
that we use to accept user input.

```python
from entrywidget import EntryWidget
```

We define a class based on the standard Activity class. This represents the
application, and will be used to present a graphical interface to the user.

```python
class GPSFileActivity(Activity):

    __interfaces__ = [GpsStatus.Listener, LocationListener]
```

The activity implements two interfaces: one which handles changes to the
GPS status and another which handles general notifications about location.

The initialisation method simply calls the corresponding method in the
base class and initialises two variables that are used to keep a textual
log of events and record the old connection status.

```python
    def __init__(self):
    
        Activity.__init__(self)
        self.oldStatus = 0
```

The onCreate method calls the corresponding base class method before
obtaining an instance of the location service manager and setting up the
user interface.

```python
    def onCreate(self, bundle):
    
        Activity.onCreate(self, bundle)
```

We set the theme of the application to the device's default. This
tends to make it fit in better with the rest of the user interface.

```python
        self.setTheme(android.R.style.Theme_DeviceDefault)
```

We obtain a location service manager object and cast it to the
correct type so that we can call its addGpsStatusListener method to
register the activity as a listener. This requires us to implement the
GpsStatus.Listener interface that we declared earlier.

```python
        self.manager = CAST(self.getSystemService(Context.LOCATION_SERVICE), LocationManager)
        self.manager.addGpsStatusListener(self)
```

We create a user interface that contains TextViews showing the
currently known location, the number of satellites visible to the
device and a log of events.

```python
        self.locationView = TextView(self)
        self.locationView.setBackgroundColor(0xff404040)
        self.locationView.setTextColor(0xffffffff)
        self.locationView.setText("No location")
        
        self.entryWidget = EntryWidget(self)
        self.entryWidget.setEnabled(False)
        
        layout = LinearLayout(self)
        layout.setLayoutParams(ViewGroup.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT))
        layout.setOrientation(LinearLayout.VERTICAL)
        
        layout.addView(self.locationView)
        layout.addView(Space(self))
        layout.addView(self.entryWidget)
        
        self.setContentView(layout)
```

The onResume method is called when the activity is first started, or
when the user navigates to it. We call the corresponding base class method
and request notification of location updates every 500 milliseconds and if
the device moves more than 1 metre.

```python
    def onResume(self):
    
        Activity.onResume(self)
        
        self.manager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
            long(500), float(1), self)
```

The onPause method is called when the user navigates away from the
activity. We call the corresponding base class method and remove our
request for location updates.

```python
    def onPause(self):
    
        Activity.onPause(self)
        
        self.manager.removeUpdates(self)
```

The following four methods are part of the LocationListener interface.

We implement the onLocationChanged method to handle location changes, using
the Location object supplied to update the location text displayed in the
user interface.

```python
    def onLocationChanged(self, location):
    
        self.locationView.setText(
            "Latitude: " + location.convert(location.getLatitude(), Location.FORMAT_SECONDS) + \
            "\nLongitude: " + \
            location.convert(location.getLongitude(), Location.FORMAT_SECONDS)
            )
```

We also inform the entry widget of the new location.

```python
        self.entryWidget.setLocation(location)
```

In the onProviderDisabled method, we simply report that a location
provider has been disabled, updating the status TextView.

```python
    def onProviderDisabled(self, provider):
    
        self.entryWidget.setEnabled(False)
```

Similarly, in the onProviderEnabled method, we report that a location
provider has been enabled, updating the status TextView, and request
updates of half a second in frequency and for changes of 1 metre or more.

```python
    def onProviderEnabled(self, provider):
    
        self.manager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
            long(500), float(1), self)
```

The onStatusChanged method is required by an implementation of the
LocationListener interface, declared earlier.

```python
    def onStatusChanged(self, provider, status, extras):
    
        pass
```

The onGpsStatusChanged method is required by an implementation of the
GpsStatus.Listener interface, declared earlier.

```python
    def onGpsStatusChanged(self, event):
    
        pass
```



## Files

* [entrywidget.py](../entrywidget.py)
* [icon.svg](../icon.svg)
