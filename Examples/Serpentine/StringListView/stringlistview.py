"""
# String List View Example

This example shows how to use an adapter to display a list of strings in a
ListView. An alternative form of this example that uses arrays instead of lists
can be found in the [Examples/ListView](../../ListView/listview.py)
directory.

![A screenshot of the application.](stringlistview.png)

We import the classes and modules referred to by our classes."""

from java.lang import Object, String
from android.app import Activity
import android.os
from android.view import View, ViewGroup
from android.widget import Adapter, BaseAdapter, ListView, TextView

"""We also import an adapter class that will let us expose lists of strings to
views."""

from serpentine.adapters import StringListAdapter

"""The StringListViewActivity is derived from the standard Activity class and
represents the application. Android will create an instance of this class when
the user runs it."""

class StringListViewActivity(Activity):

    s = ("This example shows how to expose an array of strings to a list view "
         "via an adapter, creating text views for each of the strings.")
    
    """We define a constant string that an instance of this class can access at
    run-time. The contents of the string will be used to populate an array.
    
    The initialisation method simply calls the corresponding method in the
    base class."""
    
    def __init__(self):
    
        Activity.__init__(self)
    
    """The onCreate method is where we set up the user interface after calling
    the base class's onCreate method."""
    
    @args(void, [android.os.Bundle])
    def onCreate(self, bundle):
    
        Activity.onCreate(self, bundle)
        
        """We split the string defined above to obtain an array of strings
        which we add to a list of strings for use in an adapter."""
        
        stringlist = []
        for s in self.s.split(" "):
            stringlist.add(s)
        
        """Finally, we create an adapter to wrap the string list and a list
        view to use with it. We make the list view the main view in the
        activity."""
        
        adapter = StringListAdapter(stringlist)
        view = ListView(self)
        view.setAdapter(adapter)
        
        self.setContentView(view)
