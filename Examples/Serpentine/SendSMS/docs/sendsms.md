# Send SMS Example

This example shows how to send an SMS on a suitably equipped device.

![A screenshot of the application.](sendsms.png)

The example only performs minimal checking to ensure that some sort of phone
number is entered by the user and that the message is not empty. It does not
check whether the message was sent successfully, nor does it receive a
delivery notification.

We begin by importing the classes and modules needed by our application. The
most relevant ones for this example are those from the android.telephony
module.

```python
from android.content import Context
from android.os import Build
from android.telephony import SmsManager
from android.text import InputType, TextWatcher
from android.view import View, ViewGroup
from android.widget import Button, EditText, LinearLayout, TextView
```

We also use a custom activity class and a convenience widget from the
serpentine package.

```python
from serpentine.activities import Activity
from serpentine.widgets import VBox
```

We define a class based on the standard Activity class. This represents the
application, and will be used to present a graphical interface to the user.

```python
class SendSMSActivity(Activity):

    __interfaces__ = [TextWatcher, View.OnClickListener]
```

The initialisation method only needs to call the corresponding method in
the base class.

```python
    def __init__(self):
    
        Activity.__init__(self)
```

The onCreate method is called when the activity is created. Our
implementation calls the onCreate method of the base class, queries the
available telephony features and displays them in a graphical layout.

```python
    def onCreate(self, bundle):
    
        Activity.onCreate(self, bundle)
```

The user interface is a simple collection of text labels and editors
that allow the user to enter a phone number and a message. The message
is sent when the user clicks the "Send message" button.

```python
        numberLabel = TextView(self)
        numberLabel.setText("Phone number:")
        self.numberEdit = EditText(self)
        self.numberEdit.setInputType(InputType.TYPE_CLASS_PHONE)
        self.numberEdit.addTextChangedListener(self)
        
        textLabel = TextView(self)
        textLabel.setText("Message:")
        self.textEdit = EditText(self)
        self.textEdit.addTextChangedListener(self)
        
        self.sendButton = Button(self)
        self.sendButton.setText("Send message")
        self.sendButton.setEnabled(False)
        self.sendButton.setOnClickListener(self)
```

The widgets are added to a convenience widget from the serpentine
package.

```python
        vbox = VBox(self)
        vbox.addView(numberLabel)
        vbox.addView(self.numberEdit)
        vbox.addView(textLabel)
        vbox.addView(self.textEdit)
        vbox.addView(self.sendButton)
```

The VBox widget is used as the main user interface in the activity.

```python
        self.setContentView(vbox)
```

We implement the following three methods to implement the TextWatcher
interface.

```python
    def afterTextChanged(self, s):
        pass
    
    def beforeTextChanged(self, s, start, count, after):
        pass
```

The following method is the only one from the TextWatcher interface that
we implement in full, checking the contents of the two editors in the user
interface and enabling the send button if both of them contain text.

```python
    def onTextChanged(self, s, start, before, count):
    
        if str(CAST(self.numberEdit, TextView).getText()) != "" and \
           str(CAST(self.textEdit, TextView).getText()) != "":
        
            self.sendButton.setEnabled(True)
        else:
            self.sendButton.setEnabled(False)
```

We implement the following method to implement the View.OnClickListener
interface. The method is called when the user clicks the send button.

```python
    def onClick(self, view):
```

We obtain the number and text that the user entered.

```python
        number = str(CAST(self.numberEdit, TextView).getText())
        text = str(CAST(self.textEdit, TextView).getText())
```

Since this method can only be called when the number and text
editors contain values, we can use those values with the default
SmsManager to send a text message. Note that we do not check the
validity of the number but we split the message into pieces that are
short enough to send.

```python
        smsManager = SmsManager.getDefault()
        
        for message in smsManager.divideMessage(text):
            smsManager.sendTextMessage(number, None, message, None, None)
```

Finally, we clear the editors so that the send button is disabled
again.

```python
        self.numberEdit.setText("")
        self.textEdit.setText("")
```



## Files

* [icon.svg](../icon.svg)
