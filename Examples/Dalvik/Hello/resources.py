"""
Copyright (C) 2016 David Boddie <david@boddie.org.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import os

from Dalvik.binxml import *

def create_layout_main():

    # Create and write the res/layout/main.xml file.
    string_pool = StringPool([
        u"orientation",
        u"layout_width",
        u"layout_height",
        u"text",
        u"android",
        u"http://schemas.android.com/apk/res/android",
        u"",
        u"LinearLayout",
        u"TextView",
        u"Hello World, HelloActivity"
        ])
    
    contents = [
        XMLResourceMap([
            0x10100c4, # orientation
            0x10100f4, # layout_width
            0x10100f5, # layout_height
            0x101014f  # text
            ]),
        XMLStartNamespace(prefix="android",
                          uri="http://schemas.android.com/apk/res/android",
                          line_number=2),
        XMLStartElement(name="LinearLayout", line_number=2,
            attributes=[
                XMLAttribute(namespace="android", name="orientation",
                             data_type=TYPE_INT_DEC, value=1), # vertical
                XMLAttribute(namespace="android", name="layout_width",
                             data_type=TYPE_INT_DEC, value=0xffffffff), # fill_parent
                XMLAttribute(namespace="android", name="layout_height",
                             data_type=TYPE_INT_DEC, value=0xffffffff) # fill_parent
                ]),
        XMLStartElement(name="TextView", line_number=7,
            attributes=[
                XMLAttribute(namespace="android", name="layout_width",
                             data_type=TYPE_INT_DEC, value=0xffffffff), # fill_parent
                XMLAttribute(namespace="android", name="layout_height",
                             data_type=TYPE_INT_DEC, value=0xfffffffe), # wrap_content
                XMLAttribute(namespace="android", name="text",
                             raw_value="Hello World, HelloActivity",
                             data_type=TYPE_STRING,
                             value=string_pool.index("Hello World, HelloActivity"))
                ]),
        XMLEndElement(name="TextView", line_number=11),
        XMLEndElement(name="LinearLayout", line_number=12),
        XMLEndNamespace(prefix="android",
                        uri="http://schemas.android.com/apk/res/android",
                        line_number=12)
        ]
    
    xml = XML(string_pool, contents)
    layout_main = File(root=xml)
    return layout_main


def create_resources():

    # Create and write the resources.arsc file.
    string_pool = StringPool([
        u"res/layout/main.xml",
        u"res/drawable-ldpi/ic_launcher.png",
        u"res/drawable-mdpi/ic_launcher.png",
        u"res/drawable-hdpi/ic_launcher.png",
        u"HelloActivity"
        ])
    
    packages = {
        0x7f: Package(
            id=0x7f, package_name=u"com.example.hello",
            # Types are 1-indexed.
            type_strings=StringPool([u"attr", u"drawable", u"layout", u"string"]),
            last_public_type=4,
            key_strings=StringPool([u"ic_launcher", u"main", u"app_name"]),
            last_public_key=3,
            type_specs={
                1: TypeSpec(1),             # attr, unused
                2: TypeSpec(2, [0x100]),    # drawable, CONFIG_DENSITY
                3: TypeSpec(3, [0]),        # layout
                4: TypeSpec(4, [0])},       # string
            types={
                2: [Type(2, config=Config(density=120, sdk_version=4), # type=drawable
                         entries=[Resource(key=0,       # key=ic_launcher
                            values=[(TYPE_STRING, 1)])  # string=res/drawable-ldpi/ic_launcher.png
                            ]
                         ),
                    Type(2, config=Config(density=160, sdk_version=4), # type=drawable
                         entries=[Resource(key=0,       # key=ic_launcher
                            values=[(TYPE_STRING, 2)])  # string=res/drawable-mdpi/ic_launcher.png
                            ]
                         ),
                    Type(2, config=Config(density=240, sdk_version=4), # type=drawable
                         entries=[Resource(key=0,       # key=ic_launcher
                            values=[(TYPE_STRING, 3)])  # string=res/drawable-hdpi/ic_launcher.png
                            ]
                         )
                    ],
                3: [Type(3, config=Config(),            # type=layout
                         entries=[Resource(key=1,       # key=main
                            values=[(TYPE_STRING, 0)])  # string=res/layout/main.xml
                            ]
                         )
                    ],
                4: [Type(4, config=Config(),            # type=string
                         entries=[Resource(key=2,       # key=app_name
                            values=[(TYPE_STRING, 4)])
                            ]
                        )
                    ]
                }
            )
        }
    
    table = Table(string_pool, packages)
    resources = File(root=table)
    return resources


def create_manifest():

    # Create and write the AndroidManifest.xml file.
    string_pool = StringPool([
        u"label",
        u"icon",
        u"name",
        u"versionCode",
        u"versionName",
        u"android",
        u"http://schemas.android.com/apk/res/android",
        u"",
        u"package",
        u"manifest",
        u"com.example.hello",
        u"1.0",
        u"application",
        u"activity",
        u"HelloActivity",
        u"intent-filter",
        u"action",
        u"android.intent.action.MAIN",
        u"category",
        u"android.intent.category.LAUNCHER"
        ])
    
    contents = [
        XMLResourceMap([
            0x1010001, # label
            0x1010002, # icon
            0x1010003, # name
            0x101021b, # versionCode
            0x101021c  # versionName
            ]),
        XMLStartNamespace(prefix="android",
                          uri="http://schemas.android.com/apk/res/android",
                          line_number=2),
        XMLStartElement(name="manifest", line_number=2,
            attributes=[
                XMLAttribute(namespace="android", name="versionCode",
                             data_type=TYPE_INT_DEC, value=1),
                XMLAttribute(namespace="android", name="versionName",
                             raw_value="1.0",
                             data_type=TYPE_STRING, value=string_pool.index("1.0")),
                XMLAttribute(name="package", data_type=TYPE_STRING,
                             raw_value="com.example.hello",
                             value=string_pool.index("com.example.hello"))
                ]),
        XMLStartElement(name="application", line_number=6,
            attributes=[
                XMLAttribute(namespace="android", name="label",
                             data_type=TYPE_REFERENCE, value=0x7f040000),
                             # package=0x7f, type=4, resource=0, value=0 (@string/app_name)
                XMLAttribute(namespace="android", name="icon",
                             data_type=TYPE_REFERENCE, value=0x7f020000),
                             # package=0x7f, type=2, resource=0, value=0 (@drawable/ic_launcher)
                ]),
        XMLStartElement(name="activity", line_number=7,
            attributes=[
                XMLAttribute(namespace="android", name="label",
                             data_type=TYPE_REFERENCE, value=0x7f040000),
                             # package=0x7f, type=4, resource=0, value=0 (@string/app_name)
                XMLAttribute(namespace="android", name="name",
                             data_type=TYPE_STRING,
                             raw_value="HelloActivity",
                             value=string_pool.index("HelloActivity"))
                ]),
        XMLStartElement(name="intent-filter", line_number=9),
        XMLStartElement(name="action", line_number=10,
            attributes=[
                XMLAttribute(namespace="android", name="name",
                             data_type=TYPE_STRING,
                             raw_value="android.intent.action.MAIN",
                             value=string_pool.index("android.intent.action.MAIN")),
                ]),
        XMLEndElement(name="action", line_number=10),
        XMLStartElement(name="category", line_number=11,
            attributes=[
                XMLAttribute(namespace="android", name="name",
                             data_type=TYPE_STRING,
                             raw_value="android.intent.category.LAUNCHER",
                             value=string_pool.index("android.intent.category.LAUNCHER")),
                ]),
        XMLEndElement(name="category", line_number=11),
        XMLEndElement(name="intent-filter", line_number=12),
        XMLEndElement(name="activity", line_number=13),
        XMLEndElement(name="application", line_number=14),
        XMLEndElement(name="manifest", line_number=15),
        XMLEndNamespace(prefix="android",
                        uri="http://schemas.android.com/apk/res/android",
                        line_number=15)
        ]
    
    xml = XML(string_pool, contents)
    manifest = File(root=xml)
    return manifest


def build(output_dir):

    res_dir = os.path.join(output_dir, "res")
    if not os.path.exists(res_dir):
        os.mkdir(res_dir)
    
    layout_dir = os.path.join(res_dir, "layout")
    if not os.path.exists(layout_dir):
        os.mkdir(layout_dir)
    
    layout_main = create_layout_main()
    layout_main.save(os.path.join(layout_dir, "main.xml"))
    
    resources = create_resources()
    resources.save(os.path.join(output_dir, "resources.arsc"))
    
    manifest = create_manifest()
    manifest.save(os.path.join(output_dir, "AndroidManifest.xml"))
