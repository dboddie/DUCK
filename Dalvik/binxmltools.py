"""
Copyright (C) 2016 David Boddie <david@boddie.org.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from binxml import *
from string import digits
from xml.dom import minidom

# Define a dictionary containing identifiers for XMLResourceMap elements.
# These identifiers appear to relate to resources in a standard resource file
# with id=1 and type=attr (1).

# They are defined in android-sdk-linux/docs/reference/android/R.attr.html
### We could read in the definitions from the Include/android/R.py file.

resource_map = {
    "authorities":          0x1010018,
    "background":           0x10100d4,
    "configChanges":        0x101001f,
    "debuggable":           0x101000f,
    "exported":             0x1010010,
    "gravity":              0x10100af,
    "host":                 0x1010028,
    "icon":                 0x1010002,
    "id":                   0x10100d0,
    "initialLayout":        0x1010251,
    "label":                0x1010001,
    "layout_width":         0x10100f4,
    "layout_height":        0x10100f5,
    "maxSdkVersion":        0x1010271,
    "mimeType":             0x1010026,
    "minSdkVersion":        0x101020c,
    "minHeight":            0x1010140,
    "minResizeHeight":      0x1010396,
    "minResizeWidth":       0x1010395,
    "minWidth":             0x101013f,
    "name":                 0x1010003,
    "orientation":          0x10100c4,
    "path":                 0x101002a,
    "pathPattern":          0x101002c,
    "pathPrefix":           0x101002b,
    "permission":           0x1010006,
    "previewImage":         0x10102da,
    "priority":             0x101001c,
    "port":                 0x1010029,
    "resizeMode":           0x1010363,
    "resource":             0x1010025,
    "scheme":               0x1010027,
    "shadowColor":          0x1010161,
    "showAsAction":         0x10102d9,
    "src":                  0x1010119,
    "targetSdkVersion":     0x1010270,
    "text":                 0x101014f,
    "textColor":            0x1010098,
    "title":                0x10101e1,
    "updatePeriodMillis":   0x1010250,
    "versionCode":          0x101021b,
    "versionName":          0x101021c
    }

# Note that these types are too restrictive for attributes that can be
# specified using alternative types; for example, where a reference to a value
# can be used instead of the value itself.
type_map = {
    "authorities":          TYPE_STRING,
    "background":           TYPE_INT_COLOR_ARGB8,
    "configChanges":        TYPE_INT_DEC,
    "debuggable":           TYPE_BOOLEAN,
    "exported":             TYPE_BOOLEAN,
    "gravity":              TYPE_INT_HEX,
    "host":                 TYPE_STRING,
    "icon":                 TYPE_REFERENCE,
    "id":                   TYPE_REFERENCE,
    "initialLayout":        TYPE_REFERENCE,
    "label":                TYPE_REFERENCE,
    "layout_height":        TYPE_INT_DEC,
    "layout_width":         TYPE_INT_DEC,
    "maxSdkVersion":        TYPE_INT_DEC,
    "mimeType":             TYPE_STRING,
    "minSdkVersion":        TYPE_INT_DEC,
    "minHeight":            TYPE_DIMENSION,
    "minResizeHeight":      TYPE_DIMENSION,
    "minResizeWidth":       TYPE_DIMENSION,
    "minWidth":             TYPE_DIMENSION,
    "name":                 TYPE_STRING,
    "orientation":          TYPE_INT_DEC,
    "path":                 TYPE_STRING,
    "pathPattern":          TYPE_STRING,
    "pathPrefix":           TYPE_STRING,
    "package":              TYPE_STRING,
    "permission":           TYPE_STRING,
    "port":                 TYPE_STRING,
    "previewImage":         TYPE_REFERENCE,
    "priority":             TYPE_INT_DEC,
    "resizeMode":           TYPE_INT_DEC,
    "resource":             TYPE_REFERENCE,
    "scheme":               TYPE_STRING,
    "shadowColor":          TYPE_INT_COLOR_ARGB8,
    "showAsAction":         TYPE_INT_DEC,
    "src":                  TYPE_REFERENCE,
    "targetSdkVersion":     TYPE_INT_DEC,
    "text":                 TYPE_STRING,
    "textColor":            TYPE_INT_COLOR_ARGB8,
    "title":                TYPE_REFERENCE,
    "updatePeriodMillis":   TYPE_INT_DEC,
    "versionCode":          TYPE_INT_DEC,
    "versionName":          TYPE_STRING
    }

class Element:

    args = {}
    
    def __init__(self, args = None, children = None):
    
        if not args: args = {}
        # Even though we reference it as an instance attribute, self.args is a
        # class attribute which we need to copy in order to avoid mutating the
        # original.
        self.args = self.args.copy()
        self.args.update(args)
        
        if not children: children = []
        self.children = children

def find_xml_resources(elements, resources, attributes):

    for element in elements:
    
        for arg in element.args.keys():
        
            if arg.startswith("android:"):
                try:
                    resource_id = resource_map[arg[8:]]
                except KeyError:
                    raise KeyError("Element %s not defined in DUCK/Dalvik/binxmltools.py." % repr(arg[8:]))
                resources.add((resource_id, arg[8:]))
            else:
                attributes.append(arg)
        
        # Examine this element's children, if any.
        find_xml_resources(element.children, resources, attributes)

def create_contents(elements, strings, resources = None, ids = None):

    contents = []
    
    for element in elements:
    
        # Ensure the element name is in the list of strings.
        element_name = element.name
        if element_name not in strings:
            strings.append(element_name)
        
        # Create the attributes for the element.
        attributes = []
        
        # Sort the attributes by resource_id, putting those without after the
        # others.
        keys = []
        for arg in element.args.keys():
            if arg.startswith("android:"):
                keys.append((resource_map.get(arg[8:]), arg))
            else:
                keys.append((0xffffffff, arg))
        
        keys.sort()
        
        for resource_id, full_name in keys:
        
            if ":" in full_name:
                namespace, name = full_name.split(":")
            else:
                namespace = ""
                name = full_name
            
            attr = XMLAttribute(namespace=namespace, name=name,
                data_type=type_map[name])
            
            value = element.args[full_name]
            
            if type_map[name] == TYPE_STRING:
            
                # String attributes also have a raw value and a value that must
                # be an index into the list of strings.
                try:
                    index = strings.index(value)
                except ValueError:
                    index = len(strings)
                    strings.append(value)
                
                attr.value = index
                attr.raw_value = value
            
            elif type_map[name] == TYPE_REFERENCE:
            
                if resources == None:
                    # When defining references, store the index of the ID in
                    # the list of IDs in the attribute and record the attribute
                    # in the ids dictionary for a later update since it will
                    # need to be turned into a resource ID.
                    attr.value = len(ids)
                    ids[value] = attr
                else:
                    # Resolve the reference using the supplied resources.
                    attr.value = make_reference(resources, value)
            
            else:
                attr.value = value
            
            attributes.append(attr)
        
        contents.append(XMLStartElement(name=element_name, attributes=attributes))
        
        # Create content elements for this element's children, if any.
        contents += create_contents(element.children, strings, resources, ids)
        
        # End this element.
        contents.append(XMLEndElement(name=element_name))
    
    return contents

# According to https://justanapplication.wordpress.com/tag/androidresourceids/
# resource IDs are 0xPPTTEEEE where PP=package, TT=type and EEEE=entry

def make_reference(resources, value):

    # Refer to the contents of the resources file.
    table = resources.root
    
    # The value should be of the form, "@<type>/<key>".
    type_string, key_string = value[1:].split("/")
    
    keys = table.packages.keys()
    keys.sort()
    
    for key in keys:
    
        package = table.packages[key]
        
        try:
            type_index = package.type_strings.index(type_string, initial=1)
            key_index = package.key_strings.index(key_string)
            
            for type_ in package.types[type_index]:
                for i, res in enumerate(type_.entries):
                    if res and (res.key == key_index):
                        return (package.id << 24) | (type_index << 16) | i
        
        except KeyError:
            pass # Failed to find anything in this package.
    
    raise KeyError("Failed to make a reference from '%s'." % value)

def from_xml(file_name):

    d = minidom.parse(file_name)
    
    return [from_xml_element(d.documentElement)]

def to_binxml(elements, res):
    
    # Compile collections of resources and attributes mentioned in the document.
    resources = set()
    attributes = []
    
    # Find arguments corresponding to resource attributes.
    find_xml_resources(elements, resources, attributes)
    
    # The index of each resource_ids needs to match the indexes of the
    # corresponding string in the string pool that we will create later.
    # They also need to be sorted in ascending numerical order.
    resources = list(resources)
    resources.sort()
    
    resource_ids = []
    strings = []
    
    for resource_id, string in resources:
        resource_ids.append(resource_id)
        strings.append(string)
    
    for attribute in attributes:
        if attribute not in strings:
            strings.append(attribute)
    
    # First, start the contents with some boilerplate elements and add the
    # strings they use to the string pool.
    contents = [XMLResourceMap(resource_ids),
                XMLStartNamespace(prefix="android",
                uri="http://schemas.android.com/apk/res/android")]
    
    strings += [u"android", u"http://schemas.android.com/apk/res/android"]
    
    # Construct the list of contents from the document structure itself and
    # complete the string pool.
    contents += create_contents(elements, strings, res)
    
    # Append trailing boilerplate.
    contents.append(XMLEndNamespace(prefix="android",
                    uri="http://schemas.android.com/apk/res/android"))
    
    string_pool = StringPool(strings)
    
    xml = XML(string_pool, contents)
    xml_file = File(root=xml)
    return xml_file

def dimension(value):

    v = ""
    i = 0
    while i < len(value):
        if value[i] in digits:
            v += value[i]
            i += 1
        else:
            break
    
    v = int(v)
    
    if value[i:] == "dp":
        return (v << 8) | 1
    else:
        raise ValueError("No dimension encoding information for %s in DUCK/Dalvik/binxmltools.py." % repr(value[i:]))

type_convertors = {
    TYPE_BOOLEAN: bool,
    TYPE_DIMENSION: dimension,
    TYPE_INT_DEC: int,
    TYPE_STRING: str,
    }

def from_xml_element(element):

    ele = Element()
    ele.name = element.nodeName
    
    for key, value in element.attributes.items():
    
        if key == "xmlns:android":
            continue
        elif key.startswith("android:"):
            try:
                type_ = type_map[key[8:]]
            except KeyError:
                raise KeyError("No type information for element %s in DUCK/Dalvik/binxmltools.py." % repr(key[8:]))
            
            if type_ in type_convertors:
                ele.args[key] = type_convertors[type_](value)
            elif type_ == TYPE_REFERENCE:
                ele.args[key] = value
            else:
                raise KeyError("No type convertor for element %s in DUCK/Dalvik/binxmltools.py." % repr(key[8:]))
        else:
            ele.args[key] = value
    
    for child in element.childNodes:
    
        if isinstance(child, minidom.Element):
            ele.children.append(from_xml_element(child))
    
    return ele
