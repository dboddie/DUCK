"""
Copyright (C) 2016 David Boddie <david@boddie.org.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

# See https://source.android.com/devices/tech/dalvik/dex-format.html for
# information about DEX format files.

from struct import calcsize, pack, unpack
import Crypto.Hash.SHA
import zlib

from dextypes import *

class DexStructure:

    """Provides common methods for handling DEX data structures that are
    inherited by classes that represent the different sections in a DEX file."""
    
    value_types = {VALUE_SHORT: Short, VALUE_CHAR:  Char,  VALUE_INT:    Int,
                   VALUE_LONG:  Long,  VALUE_FLOAT: Float, VALUE_DOUBLE: Double}
    
    def __init__(self, dex):
    
        self.dex = dex
        self.elements = []
    
    def read(self, f, size):
    
        pass
    
    def read_descriptor(self, s, descriptors, string_index = None):
    
        try:
            desc = descriptors[s[0]]
        except KeyError:
            raise DecodeError, "Unknown type descriptor '%s' (string id %s)" % (
                s, string_index)
        
        if desc == Name:
        
            # Create an instance of the Name class used to represent the type.
            desc = desc(s)
        
        elif desc == Array:
        
            i = 1
            while i < len(s) and s[i] == "[":
                i += 1
            
            if i == len(s):
                raise DecodeError, "Invalid array descriptor '%s' in type_id " \
                    "%i (string id %i)" % (s, index, string_index)
            
            # Create an instance of the Array class used to represent the type.
            desc = Array(s)
            desc.dimensions = i
            desc.array_type = self.read_descriptor(s[i:], descriptors)
        
        elif desc == MemberName:
            desc = desc(s)
        
        # Return the instance created or originally obtained.
        return desc
    
    def read_type_list(self, offset):
    
        if offset == 0:
            return []
        
        offset -= self.dex.header.data_offset
        data = self.dex.data
        
        size = unpack("<I", data[offset:offset + 4])[0]
        offset += 4
        types = []
        
        while size > 0:
            index = unpack("<H", data[offset:offset + 2])[0]
            types.append(self.dex.type_ids.item(index))
            offset += 2
            size -= 1
        
        return types
    
    def read_encoded_value(self, offset):
    
        # This method is only provided for convenience since only the
        # _read_encoded_value method is needed to perform decoding for other
        # decoding methods.
        
        if offset == 0:
            return []
        
        offset -= self.dex.header.data_offset
        data = self.dex.data
        value, offset = self._read_encoded_value(data, offset)
        
        return value
    
    def read_encoded_array(self, offset):
    
        if offset == 0:
            return []
        
        offset -= self.dex.header.data_offset
        data = self.dex.data
        values, offset = self._read_encoded_array(data, offset)
        
        return values
    
    def read_encoded_annotation(self, offset):
    
        if offset == 0:
            return []
        
        offset -= self.dex.header.data_offset
        data = self.dex.data
        value, offset = self._read_encoded_annotation(data, offset)
        
        return value
    
    def _read_encoded_value(self, data, offset):
        """Read the encoded value at the given offset within the data section,
        converting it to the corresponding Python type or a wrapper that
        preserves information about the original value.
        """
        
        byte = unpack("<B", data[offset])[0]
        value_arg = byte >> 5
        value_type = byte & 0x1f
        offset += 1
        
        if value_type == VALUE_BYTE:
            value = unpack("<b", data[offset])[0]
            return Byte(value), offset + 1
        
        elif VALUE_SHORT <= value_type <= VALUE_DOUBLE:
        
            size = value_arg + 1
            value = read_signed(data, offset, size, little_endian = True)
            return self.value_types[value_type](value), offset + size
        
        elif VALUE_STRING <= value_type <= VALUE_ENUM:
        
            # Pad the string to fill the required number of characters.
            # This will need to be done differently for big-endian numbers.
            
            size = value_arg + 1
            value = read_unsigned(data, offset, size, little_endian = True)
            
            if value_type == VALUE_STRING:
                # The value is an index into the string_ids section.
                return self.dex.string_ids.item(value), offset + size
            
            elif value_type == VALUE_TYPE:
                # The value is an index into the type_ids section.
                return Type(self.dex.type_ids.item(value)), offset + size
            
            elif value_type == VALUE_FIELD:
                # The value is an index into the field_ids section.
                return self.dex.field_ids.item(value), offset + size
            
            elif value_type == VALUE_METHOD:
                # The value is an index into the method_ids section.
                return self.dex.method_ids.item(value), offset + size
            
            elif value_type == VALUE_ENUM:
                # The value is an index into the field_ids section.
                return self.dex.field_ids.item(value), offset + size
            
            else:
                raise DecodeError, "Unknown value type 0x%x at 0x%x." % (
                    value_type, offset + self.dex.header.data_offset - 1)
        
        elif value_type == VALUE_ARRAY:
            return self._read_encoded_array(data, offset)
        
        elif value_type == VALUE_ANNOTATION:
            return self._read_encoded_annotation(data, offset)
        
        elif value_type == VALUE_NULL:
            return None, offset
        
        elif value_type == VALUE_BOOLEAN:
            return value_arg == 1, offset
        
        else:
            raise DecodeError, "Unknown value type 0x%x at 0x%x." % (
                value_type, offset + self.dex.header.data_offset - 1)
    
    def _read_encoded_array(self, data, offset):
    
        # The offset is within the data section.
        
        size, offset = read_uleb128(data, offset)
        values = []
        while size > 0:
            value, offset = self._read_encoded_value(data, offset)
            values.append(value)
            size -= 1
        
        return values, offset
    
    def _read_encoded_annotation(self, data, offset):
    
        # The offset is within the data section.
        
        type_index, offset = read_uleb128(data, offset)
        type_ = self.dex.type_ids.item(type_index)
        size, offset = read_uleb128(data, offset)
        values = []
        while size > 0:
            name_index, offset = read_uleb128(data, offset)
            name = self.dex.string_ids.item(name_index)
            value, offset = self._read_encoded_value(data, offset)
            values.append((name, value))
            size -= 1
        
        return Annotation(type_, values), offset
    
    def write_encoded_value(self, dex, value):
    
        if value is None:
            return pack("<B", VALUE_NULL)
        
        elif type(value) == list or type(value) == tuple:
            # VALUE_ARRAY:
            return pack("<B", VALUE_ARRAY) + self.write_encoded_array(dex, value)
        
        elif type(value) == bool:
            # VALUE_BOOLEAN
            value_arg = {False: 0, True: 0x20}[value]
            return pack("<B", VALUE_BOOLEAN | value_arg)
        
        elif type(value) == str or type(value) == unicode:
            # VALUE_STRING:
            # The value is an index into the string_ids section.
            typecode = "I"
            size = calcsize(typecode)
            value_arg = (size - 1) << 5
            
            return pack("<B"+typecode, VALUE_STRING | value_arg,
                dex.string_ids.index[value])
        
        elif value.type == VALUE_BYTE:
            return pack("<B"+value.typecode, VALUE_BYTE, value)
        
        elif VALUE_SHORT <= value.type <= VALUE_DOUBLE:
        
            size = calcsize(value.typecode)
            value_arg = (size - 1) << 5
            return pack("<B"+value.typecode, value.type | value_arg, value.value)
        
        elif VALUE_STRING <= value.type <= VALUE_ENUM:
        
            # Pad the string to fill the required number of characters.
            # This will need to be done differently for big-endian numbers.
            
            size = calcsize(value.typecode)
            value_arg = (size - 1) << 5
            
            if value.type == VALUE_TYPE:
                # The value is an index into the type_ids section.
                return pack("<B"+value.typecode, value.type | value_arg,
                    dex.type_ids.index[value.value.desc])
            
            elif value.type == VALUE_FIELD:
                # The value is an index into the field_ids section.
                return pack("<B"+value.typecode, value.type | value_arg,
                    dex.field_ids.index[value])
            
            elif value.type == VALUE_METHOD:
                # The value is an index into the method_ids section.
                return pack("<B"+value.typecode, value.type | value_arg,
                    dex.method_ids.index[value])
            
            elif value.type == VALUE_ENUM:
                # The value is an index into the field_ids section.
                return pack("<B"+value.typecode, value.type | value_arg,
                    dex.field_ids.index[value])
            
            else:
                raise EncodeError, "Unknown value type 0x%x for %s." % (
                    value.type, repr(value))
        
        elif value.type == VALUE_ANNOTATION:
            return self.write_encoded_annotation(dex, value)
        
        else:
            raise EncodeError, "Unknown value type 0x%x for %s." % (
                value.type, repr(value))
    
    def write_encoded_array(self, dex, values):
    
        data = write_uleb128(len(values))
        
        for value in values:
            data += self.write_encoded_value(dex, value)
        
        return data
    
    def write_encoded_annotation(self, dex, annotation):
    
        type_index = dex.type_ids.index[annotation.type.desc]
        data = write_uleb128(type_index)
        data += write_uleb128(len(annotation.values))
        
        for name, value in annotation.values:
            name_index = dex.string_ids.index[name]
            data += write_uleb128(name_index)
            data += self.write_encoded_value(dex, value)
        
        return data
    
    def arrange(self, start):
    
        # Really, we should check that these structures are word-aligned but
        # they should be anyway.
        self.start = start


class Annotation:

    def __init__(self, type_, values):
    
        self.type = type_
        self.values = values
    
    def __repr__(self):
    
        return "Annotation(%s, %s)" % (repr(self.type), repr(self.values))

class Visibility:

    def __init__(self, value):
    
        if not 0 <= value <= 2:
            raise DecodeError, "Invalid visibility value, %i." % value
        
        self.value = value
    
    def __repr__(self):
    
        if self.value == VISIBILITY_BUILD:
            return "Visibility(VISIBILITY_BUILD)"
        elif self.value == VISIBILITY_RUNTIME:
            return "Visibility(VISIBILITY_RUNTIME)"
        elif self.value == VISIBILITY_SYSTEM:
            return "Visibility(VISIBILITY_SYSTEM)"
        else:
            raise ValueError("Invalid visibility value: %x" % self.value)


class Header(DexStructure):

    Magic = "dex\n035\x00"
    
    _format = [("<I", "checksum"),
               ("20s", "signature"),
               ("<I", "file_size"),
               ("<I", "header_size"),
               ("<I", "endian_tag"),
               ("<I", "link_size"),
               ("<I", "link_offset"),
               ("<I", "map_offset"),
               ("<I", "string_ids_size"),
               ("<I", "string_ids_offset"),
               ("<I", "type_ids_size"),
               ("<I", "type_ids_offset"),
               ("<I", "proto_ids_size"),
               ("<I", "proto_ids_offset"),
               ("<I", "field_ids_size"),
               ("<I", "field_ids_offset"),
               ("<I", "method_ids_size"),
               ("<I", "method_ids_offset"),
               ("<I", "class_defs_size"),
               ("<I", "class_defs_offset"),
               ("<I", "data_size"),
               ("<I", "data_offset")]
    
    def read(self, f, size = 0):
    
        d = f.read(8)
        if d != self.Magic:
            raise DecodeError, "Not a dex file."
        
        for type_, name in self._format:
            size = calcsize(type_)
            setattr(self, name, unpack(type_, f.read(size))[0])
    
    def write(self, f):
    
        f.write(self.Magic)
        written = ""
        
        for type_, name in self._format:
            data = pack(type_, getattr(self, name))
            f.write(data)
            if name != "checksum" and name != "signature":
                written += data
        
        return written
    
    def write_checksum_signature(self, f, written):
    
        offset = f.tell()
        
        f.seek(12)
        # Write the SHA1 digest of the written data.
        s = Crypto.Hash.SHA.new(written).digest()
        f.write(s)
        
        f.seek(8)
        # Write the unsigned representation of the adler32 checksum.
        checksum = zlib.adler32(s + written)
        f.write(pack("<I", checksum & 0xffffffff))
        f.seek(offset)
    
    def size(self):
    
        format = "".join(map(lambda (s, n): s.strip("<"), self._format))
        return len(self.Magic) + calcsize(format)
    
    def __len__(self):
        return 1

class IdContainer(DexStructure):

    ItemFormat = "<I"
    
    def read(self, f, size):
    
        self.elements = []
        i = 0
        
        item_size = calcsize(self.ItemFormat)
        
        while i < size:
            values = unpack(self.ItemFormat, f.read(item_size))
            if len(values) == 1:
                values = values[0]
            
            self.elements.append(values)
            i += 1
    
    def write(self, f):
    
        if self.ItemFormat == "<I":
            pack_tuples = False
        else:
            pack_tuples = True
        
        written = ""
        
        for element in self.elements:
        
            if pack_tuples:
                data = pack(self.ItemFormat, *element)
            else:
                data = pack(self.ItemFormat, element)
            
            f.write(data)
            written += data
        
        return written
    
    def __getitem__(self, index):
        return self.elements[index]
    
    def __len__(self):
        return len(self.elements)
    
    def __iter__(self):
        return IdIterator(self)
    
    def append(self, element):
        self.elements.append(element)
    
    def size(self):
        return len(self) * calcsize(self.ItemFormat)

class CachedIdContainer(IdContainer):

    def __init__(self, dex):
        IdContainer.__init__(self, dex)
        self.cache = {}

class IdIterator:

    def __init__(self, container):
    
        self.container = container
        self.index = 0
    
    def next(self):
    
        if 0 <= self.index < len(self.container):
            value = self.container.item(self.index)
            self.index += 1
            return value
        else:
            raise StopIteration

class StringIds(IdContainer):

    ItemFormat = "<I"
    
    def item(self, index):
    
        offset = self.elements[index]
        data_offset = offset - self.dex.header.data_offset
        
        utf16_size, data_offset = read_uleb128(self.dex.data, data_offset)
        data = read_mutf8(self.dex.data, data_offset, utf16_size)
        
        return data
    
    def resolve(self):
        i = 0
        while i < len(self.elements):
            element = self.elements[i]
            if isinstance(element, Reference):
                self.elements[i] = element.address()
            i += 1

class TypeIds(IdContainer):

    ItemFormat = "<I"
    
    def item(self, index):
    
        # The index is an index into the string_ids section.
        string_index = self.elements[index]
        # Dereference the offset obtained from the string_ids section.
        s = self.dex.string_ids.item(string_index)
        
        return self.read_descriptor(s, TypeDescriptors, string_index)

class ProtoIds(CachedIdContainer):

    ItemFormat = "<III"
    
    def item(self, index):
    
        if index not in self.cache:
        
            shorty_index, return_type_index, parameters_offset = self.elements[index]
            shorty_string = self.dex.string_ids.item(shorty_index)
            return_type = self.dex.type_ids.item(return_type_index)
        
            # Dereference and parse the offsets obtained from the string_ids
            # section.
            shorty = map(lambda c:
                self.read_descriptor(c, ShortDescriptors, shorty_index),
                shorty_string)
            parameters = self.read_type_list(parameters_offset)
            
            self.cache[index] = Prototype(shorty, return_type, parameters)
        
        return self.cache[index]
    
    def resolve(self):
        i = 0
        while i < len(self.elements):
            s, rt, p = self.elements[i]
            if isinstance(p, Reference):
                self.elements[i] = (s, rt, p.address())
            i += 1

class FieldIds(CachedIdContainer):

    ItemFormat = "<HHI"
    
    def item(self, index):
    
        if index not in self.cache:
        
            class_index, type_index, name_index = self.elements[index]
            
            # The first two are indices into the type_ids lists.
            class_type = self.dex.type_ids.item(class_index)
            type_ = self.dex.type_ids.item(type_index)
            name_string = self.dex.string_ids.item(name_index)
            
            name = MemberName(name_string)
            
            self.cache[index] = Field(class_type, type_, name)
        
        return self.cache[index]

class MethodIds(CachedIdContainer):

    ItemFormat = "<HHI"
    
    def item(self, index):
    
        if index not in self.cache:
        
            class_index, proto_index, name_index = self.elements[index]
            
            # The first two are indices into the type_ids lists.
            class_type = self.dex.type_ids.item(class_index)
            proto = self.dex.proto_ids.item(proto_index)
            name_string = self.dex.string_ids.item(name_index)
            
            name = MemberName(name_string)
            
            self.cache[index] = Method(class_type, proto, name)
        
        return self.cache[index]

class ClassDefs(CachedIdContainer):

    ItemFormat = "<IIIIIIII"
    
    def item(self, index):
    
        if index not in self.cache:
        
            class_index, access_flags, superclass_index, interfaces_offset, \
            source_file_index, annotations_offset, class_data_offset, \
            static_values_offset = self.elements[index]
            
            class_type = self.dex.type_ids.item(class_index)
            access_flags = AccessFlags(access_flags)
            if superclass_index == NO_INDEX:
                superclass_type = None
            else:
                superclass_type = self.dex.type_ids.item(superclass_index)
            
            interfaces = self.read_type_list(interfaces_offset)
            
            if source_file_index == NO_INDEX:
                source_file = None
            else:
                source_file = self.dex.string_ids.item(source_file_index)
            
            annotations = self.read_annotations_directory(annotations_offset)
            
            class_data = self.read_class_data(class_data_offset)
            static_values = self.read_encoded_array(static_values_offset)
            
            self.cache[index] = ClassDef(class_type, access_flags, superclass_type,
                interfaces, source_file, annotations, class_data, static_values)
        
        return self.cache[index]
    
    def read_annotations_directory(self, offset):
    
        if offset == 0:
            return []
        
        offset -= self.dex.header.data_offset
        data = self.dex.data
        
        class_annotations_offset, fields_size, annotated_methods_size, \
        annotated_parameters_size = unpack("<IIII", data[offset:offset + 16])
        offset += 16
        
        class_annotations = self.read_annotation_set_item(class_annotations_offset)
        
        fields = []
        while fields_size > 0:
            field_index, annotations_offset = unpack("<II", data[offset:offset + 8])
            field = self.dex.field_ids.item(field_index)
            annotations = self.read_annotation_set_item(annotations_offset)
            fields.append((field, annotations))
            fields_size -= 1
            offset += 8
        
        methods = []
        while annotated_methods_size > 0:
            method_index, annotations_offset = unpack("<II", data[offset:offset + 8])
            method = self.dex.method_ids.item(method_index)
            annotations = self.read_annotation_set_item(annotations_offset)
            methods.append((method, annotations))
            annotated_methods_size -= 1
            offset += 8
        
        parameters = []
        while annotated_parameters_size > 0:
            parameter_index, annotations_offset = unpack("<II", data[offset:offset + 8])
            parameter = self.dex.method_ids.item(parameter_index)
            annotations = self.read_annotation_set_ref_list(annotations_offset)
            parameters.append((parameter, annotations))
            annotated_parameters_size -= 1
            offset += 8
        
        return Annotations(class_annotations, fields, methods, parameters)
    
    def read_annotation_set_item(self, offset):
    
        if offset == 0:
            return []
        
        offset -= self.dex.header.data_offset
        data = self.dex.data
        value, offset = self._read_annotation_set_item(data, offset)
        
        return value
    
    def _read_annotation_set_item(self, data, offset):
    
        size = unpack("<I", data[offset:offset + 4])[0]
        offset += 4
        annotations = []
        
        while size > 0:
            # Read the annotation_off_item structure.
            annotation_offset = unpack("<I", data[offset:offset + 4])[0]
            annotation_offset -= self.dex.header.data_offset
            
            # Read the annotation_item structure.
            visibility = Visibility(unpack("<B", data[annotation_offset])[0])
            annotation = self.read_encoded_annotation(
                self.dex.header.data_offset + annotation_offset + 1)
            annotations.append((visibility, annotation))
            size -= 1
            offset += 4
        
        return annotations, offset
    
    def read_annotation_set_ref_list(self, offset):
    
        if offset == 0:
            return []
        
        offset -= self.dex.header.data_offset
        data = self.dex.data
        
        size = unpack("<I", data[offset:offset + 4])[0]
        offset += 4
        annotations = []
        
        while size > 0:
            # Read the annotation_set_ref_item structure.
            annotations_offset = unpack("<I", data[offset:offset + 4])[0]
            annotations_offset -= self.dex.header.data_offset
            
            # Read the annotation_set_item structure.
            values = self._read_annotation_set_item(data, annotations_offset)
            
            annotations.append(values)
            size -= 1
            offset += 4
        
        return annotations
    
    def read_class_data(self, offset):
    
        if offset == 0:
            return []
        
        offset -= self.dex.header.data_offset
        data = self.dex.data
        
        static_fields_size, offset = read_uleb128(data, offset)
        instance_fields_size, offset = read_uleb128(data, offset)
        direct_methods_size, offset = read_uleb128(data, offset)
        virtual_methods_size, offset = read_uleb128(data, offset)
        
        static_fields, offset = self.read_encoded_fields(data, offset, static_fields_size)
        instance_fields, offset = self.read_encoded_fields(data, offset, instance_fields_size)
        direct_methods, offset = self.read_encoded_methods(data, offset, direct_methods_size)
        virtual_methods, offset = self.read_encoded_methods(data, offset, virtual_methods_size)
        
        return ClassData(static_fields, instance_fields, direct_methods, virtual_methods)
    
    def read_encoded_fields(self, data, offset, size):
    
        fields = []
        field_index = 0
        while size > 0:
            field_index_diff, offset = read_uleb128(data, offset)
            field_index += field_index_diff
            field = self.dex.field_ids.item(field_index)
            access_flags, offset = read_uleb128(data, offset)
            fields.append((field, AccessFlags(access_flags)))
            size -= 1
        
        return fields, offset
    
    def read_encoded_methods(self, data, offset, size):
    
        methods = []
        method_index = 0
        while size > 0:
            method_index_diff, offset = read_uleb128(data, offset)
            method_index += method_index_diff
            method = self.dex.method_ids.item(method_index)
            access_flags, offset = read_uleb128(data, offset)
            code_offset, offset = read_uleb128(data, offset)
            code = self.read_code(code_offset)
            if code:
                code.offset = code_offset
            
            method.access_flags = AccessFlags(access_flags)
            method.code = code
            methods.append(method)
            size -= 1
        
        return methods, offset
    
    def read_code(self, offset):
    
        if offset == 0:
            return None
        
        offset -= self.dex.header.data_offset
        data = self.dex.data
        
        registers_size, ins_size, outs_size, tries_size, debug_info_offset, \
        instructions_size = unpack("<HHHHII", data[offset:offset + 16])
        offset += 16
        
        instructions = []
        i = 0
        while i < instructions_size:
            instructions.append(unpack("<H", data[offset:offset + 2])[0])
            i += 1
            offset += 2
        
        # Padding between the code and tries.
        if tries_size != 0 and instructions_size % 2 == 1:
            offset += 2
        
        tries = []
        i = 0
        while i < tries_size:
            start_address, instruction_count, handler_offset = \
                unpack("<IHH", data[offset:offset + 8])
            
            # Note that the instruction count is actually the number of 16-bit
            # code units, not actual instructions.
            tries.append((start_address, instruction_count, handler_offset))
            i += 1
            offset += 8
        
        handlers = {}
        
        if tries_size > 0:
        
            handler_start_offset = offset
            handlers_size, offset = read_uleb128(data, offset)
            
            i = 0
            while i < handlers_size:
            
                # Record the offset from the start of the list of handlers.
                this_handler_offset = offset - handler_start_offset
                
                catch_types_size, offset = read_sleb128(data, offset)
                catch_type_handlers = []
                j = 0
                while j < abs(catch_types_size):
                    type_index, offset = read_uleb128(data, offset)
                    bytecode_address, offset = read_uleb128(data, offset)
                    catch_type_handlers.append(
                        (self.dex.type_ids.item(type_index), bytecode_address))
                    j += 1
                
                # Explicitly typed catches are followed by a catch-all handler.
                # A catch type size of zero means that a catch-all is also used.
                if catch_types_size <= 0:
                    catch_all_bytecode_address, offset = read_uleb128(data, offset)
                    catch_handler = (catch_type_handlers, catch_all_bytecode_address)
                else:
                    catch_handler = (catch_type_handlers, None)
                
                handlers[this_handler_offset] = catch_handler
                i += 1
        
        # Insert the handlers into the tries list in order to create a single
        # structure that describes them both.
        tries_and_handlers = []
        for start_address, instruction_count, offset in tries:
            tries_and_handlers.append((start_address, instruction_count, handlers[offset]))
        
        return Code(registers_size, ins_size, outs_size, tries_and_handlers, debug_info_offset, instructions)
    
    def resolve(self):
        i = 0
        while i < len(self.elements):
            values = list(self.elements[i])
            j = 0
            while j < len(values):
                value = values[j]
                if isinstance(value, Reference):
                    values[j] = value.address()
                j += 1
            
            self.elements[i] = tuple(values)
            i += 1

class ClassDef:

    def __init__(self, class_type, access_flags, superclass_type, interfaces,
                       source_file, annotations, class_data, static_values):
    
        self.class_type = class_type
        self.access_flags = access_flags
        self.superclass_type = superclass_type
        self.interfaces = interfaces
        self.source_file = source_file
        self.annotations = annotations
        self.class_data = class_data
        self.static_values = static_values
    
    def __repr__(self):
    
        return "ClassDef(%s, %s, %s, %s, %s, %s, %s, %s)" % (
            repr(self.class_type), repr(self.access_flags),
            repr(self.superclass_type), repr(self.interfaces),
            repr(self.source_file), repr(self.annotations),
            repr(self.class_data), repr(self.static_values))

class Annotations(DexStructure):

    ItemFormat = "<IIII"
    field_annotation_format = "<II"
    method_annotation_format = "<II"
    parameter_annotation_format = "<II"
    
    def __init__(self, class_annotations, fields, methods, parameters):
    
        self.class_annotations = class_annotations
        self.fields = fields
        self.methods = methods
        self.parameters = parameters
    
    def __repr__(self):
    
        return "Annotations(%s, %s, %s, %s)" % (repr(self.class_annotations),
            repr(self.fields), repr(self.methods), repr(self.parameters))
    
    def write(self, dex, annotations_directory_items, annotation_set_ref_lists,
                         annotation_set_items, annotation_items):
    
        if self.class_annotations:
            class_annotation_offset = self.write_annotation_set_item(
                dex, self.class_annotations, annotation_set_items,
                annotation_items)
        else:
            class_annotation_offset = pack("<I", 0)
        
        data = [class_annotation_offset]
        
        # Write the lengths of the field, method and parameter annotations.
        data.append(pack("<III", len(self.fields), len(self.methods), len(self.parameters)))
        
        # Write the lists of field, method and parameter annotation references
        # (field_annotation_format, method_annotation_format,
        # parameter_annotation_format).
        
        for field, annotations in self.fields:
            index = dex.field_ids.index[field]
            data.append(pack("<I", index))
            
            ref = self.write_annotation_set_item(dex, annotations,
                annotation_set_items, annotation_items)
            
            data.append(ref)
        
        for method, annotations in self.methods:
            index = dex.method_ids.index[method]
            data.append(pack("<I", index))
            
            ref = self.write_annotation_set_item(dex, annotations,
                annotation_set_items, annotation_items)
            
            data.append(ref)
        
        for parameter, annotations in self.parameters:
            index = dex.method_ids.index[parameter]
            data.append(pack("<I", index))
            
            ref = self.write_annotation_set_ref_list(dex, annotations,
                annotation_set_ref_lists, annotation_set_items,
                annotation_items)
            
            data.append(ref)
        
        return annotations_directory_items.add_unresolved(data)
    
    def write_annotation_set_item(self, dex, annotations, annotation_set_items,
                                        annotation_items):
    
        # Write an annotation_set_item which refers to one or more
        # annotation_item structures.
        data = [pack("<I", len(annotations))]
        
        for visibility, annotation in annotations:
        
            # Write the annotation_item data for later.
            annotation_item = pack("<B", visibility.value) + \
                self.write_encoded_annotation(dex, annotation)
            
            ref = annotation_items.add(annotation_item)
            
            # Write the annotation_off_item structure, referring to the item.
            data.append(ref)
        
        return annotation_set_items.add_unresolved(data)
    
    def write_annotation_set_ref_list(self, dex, list_of_annotations,
                                      annotation_set_ref_lists,
                                      annotation_set_items, annotation_items):
    
        data = [pack("<I", len(list_of_annotations))]
        
        for annotations in list_of_annotations:
        
            # Write the annotation_set_item data for later.
            ref = self.write_annotation_set_item(dex, annotations,
                annotation_set_items, annotation_items)
            
            # Write the annotation_set_ref_item structure.
            data.append(ref)
        
        return annotation_set_ref_lists.add_unresolved(data)
    
    def size(self):
        return calcsize(self.ItemFormat) + (
            len(self.fields) * calcsize(self.field_annotation_format)) + (
            len(self.methods) * calcsize(self.method_annotation_format)) + (
            len(self.parameters) * calcsize(self.parameter_annotation_format))
    
    def copy(self, dex):
        return Annotations(self.class_annotations, self.fields,
                           self.methods, self.parameters)

class ClassData:

    def __init__(self, static_fields, instance_fields, direct_methods, virtual_methods):
    
        self.static_fields = static_fields
        self.instance_fields = instance_fields
        self.direct_methods = direct_methods
        self.virtual_methods = virtual_methods
    
    def __repr__(self):
    
        return "ClassData(%s, %s, %s, %s)" % (repr(self.static_fields),
            repr(self.instance_fields), repr(self.direct_methods),
            repr(self.virtual_methods))
    
    def write(self, dex, class_data_items, code_items, debug_info_items):
    
        # Sort the fields and methods according to their indices in the main
        # collection.
        self.sort_fields(dex)
        self.sort_methods(dex)
        
        # Write the code first so that the method structures have something to
        # refer to.
        direct_offsets, virtual_offsets = self.write_code(dex, code_items, debug_info_items)
        
        # Serialise the class data, referring to the code.
        
        # Write the numbers of fields and methods.
        data = [write_uleb128(len(self.static_fields)),
                write_uleb128(len(self.instance_fields)),
                write_uleb128(len(self.direct_methods)),
                write_uleb128(len(self.virtual_methods))]
        
        # Write the encoded fields.
        data += [self.write_encoded_fields(dex, self.static_fields),
                 self.write_encoded_fields(dex, self.instance_fields)]
        
        # Write the encoded methods.
        data += self.write_encoded_methods(dex, self.direct_methods, direct_offsets)
        data += self.write_encoded_methods(dex, self.virtual_methods, virtual_offsets)
        
        ref = class_data_items.add_unresolved(data)
        return ref
    
    def write_code(self, dex, code_items, debug_info_items):
    
        direct_offsets = []
        virtual_offsets = []
        
        for method in self.direct_methods:
            if method.code:
                code, debug_item = method.code.write(dex)
                debug_ref = debug_info_items.add(debug_item)
                ref = code_items.add(code, debug_ref)
                direct_offsets.append(ref)
            else:
                direct_offsets.append(write_uleb128(0))
        
        for method in self.virtual_methods:
            if method.code:
                code, debug_item = method.code.write(dex)
                debug_ref = debug_info_items.add(debug_item)
                ref = code_items.add(code, debug_ref)
                virtual_offsets.append(ref)
            else:
                virtual_offsets.append(write_uleb128(0))
        
        return direct_offsets, virtual_offsets
    
    def write_encoded_fields(self, dex, fields):
    
        data = ""
        field_index = 0
        
        for field, access_flags in fields:
            field_index_diff = dex.field_ids.index[field] - field_index
            data += write_uleb128(field_index_diff)
            field_index += field_index_diff
            data += write_uleb128(access_flags.value)
        
        return data
    
    def write_encoded_methods(self, dex, methods, code_offsets):
    
        data = []
        method_index = 0
        
        for method in methods:
            method_index_diff = dex.method_ids.index[method] - method_index
            data += [write_uleb128(method_index_diff),
                     write_uleb128(method.access_flags.value),
                     code_offsets.pop(0)]
            method_index += method_index_diff
        
        return data
    
    def sort_fields(self, dex):
    
        indexed = map(lambda (field, access):
                      (dex.field_ids.index[field], field, access),
                      self.static_fields)
        indexed.sort()
        self.static_fields = map(lambda (index, field, access): (field, access),
                                 indexed)
        
        indexed = map(lambda (field, access):
                      (dex.field_ids.index[field], field, access),
                      self.instance_fields)
        indexed.sort()
        self.instance_fields = map(lambda (index, field, access): (field, access),
                                   indexed)
    
    def sort_methods(self, dex):
    
        indexed = map(lambda method: (dex.method_ids.index[method], method),
                      self.direct_methods)
        indexed.sort()
        self.direct_methods = map(lambda (index, method): method, indexed)
        
        indexed = map(lambda method: (dex.method_ids.index[method], method),
                      self.virtual_methods)
        indexed.sort()
        self.virtual_methods = map(lambda (index, method): method, indexed)


class Code(DexStructure):

    _format = [("<H", "registers_size"),
               ("<H", "ins_size"),
               ("<H", "outs_size")]
    
    def __init__(self, registers_size, ins_size, outs_size, tries_and_handlers,
                       debug_info_offset, instructions):
    
        self.registers_size = registers_size
        self.ins_size = ins_size
        self.outs_size = outs_size
        self.tries_and_handlers = tries_and_handlers
        self.debug_info_offset = debug_info_offset
        self.instructions = instructions
    
    def __repr__(self):
    
        return "Code(%s, %s, %s, %s, %s, %s)" % (repr(self.registers_size),
            repr(self.ins_size), repr(self.outs_size),
            repr(self.tries_and_handlers), repr(self.debug_info_offset),
            repr(self.instructions))
    
    def write(self, dex):
    
        data = ""
        
        # Write the basic information.
        for type_, name in self._format:
            data += pack(type_, getattr(self, name))
        
        data += pack("<H", len(self.tries_and_handlers))
        # Leave the debug_info_offset field unset. It will be filled in when
        # the assocated CodeItems structure is written.
        data += pack("<I", 0)
        
        data += pack("<I", len(self.instructions))
        for instruction in self.instructions:
            data += pack("<H", instruction & 0xffff)
        
        # Padding between the code and tries.
        if len(self.tries_and_handlers) != 0 and len(self.instructions) % 2 == 1:
            data += "\x00\x00"
        
        if self.tries_and_handlers:
        
            # Write the handler list separately in order to calculate offsets
            # into it for the tries list to use.
            tries = []
            handler_data = write_uleb128(len(self.tries_and_handlers))
            
            for start, count, catch_handler in self.tries_and_handlers:
            
                # Record the offset to this catch handler
                tries.append((start, count, len(handler_data)))
                
                catch_type_handlers, catch_all_bytecode_address = catch_handler
                
                catch_types_size = len(catch_type_handlers)
                if catch_all_bytecode_address != None:
                    catch_types_size = -catch_types_size
                
                # Write the number of catch types for each handler.
                handler_data += write_sleb128(catch_types_size)
                
                for type_, bytecode_address in catch_type_handlers:
                    type_index = dex.type_ids.index[type_.desc]
                    handler_data += write_uleb128(type_index)
                    handler_data += write_uleb128(bytecode_address)
                
                if catch_types_size <= 0:
                    handler_data += write_uleb128(catch_all_bytecode_address)
            
            # Write the tries and the header data.
            for start_address, instruction_count, offset in tries:
                data += pack("<IHH", start_address, instruction_count, offset)
            
            data += handler_data
        
        # Include a debug_info_item with placeholder information.
        # (Line number = 0, number of parameters = 0, debug bytecodes = 7,14,0.)
        debug_item = "\x00\x00\x07\x0e\x00"
        
        return CodeItem(data), debug_item

class AccessFlags:

    def __init__(self, value):
    
        self.value = value
        
        self.public =       (value & ACC_PUBLIC) != 0
        self.private =      (value & ACC_PRIVATE) != 0
        self.protected =    (value & ACC_PROTECTED) != 0
        self.static =       (value & ACC_STATIC) != 0
        self.final =        (value & ACC_FINAL) != 0
        self.synchronized = (value & ACC_SYNCHRONIZED) != 0
        self.volatile =     (value & ACC_VOLATILE) != 0
        self.bridge =       (value & ACC_BRIDGE) != 0
        self.transient =    (value & ACC_TRANSIENT) != 0
        self.varargs =      (value & ACC_VARARGS) != 0
        self.native =       (value & ACC_NATIVE) != 0
        self.interface =    (value & ACC_INTERFACE) != 0
        self.abstract =     (value & ACC_ABSTRACT) != 0
        self.strict =       (value & ACC_STRICT) != 0
        self.synthetic =    (value & ACC_SYNTHETIC) != 0
        self.annotation =   (value & ACC_ANNOTATION) != 0
        self.enum =         (value & ACC_ENUM) != 0
        self.constructor =  (value & ACC_CONSTRUCTOR) != 0
        self.declared_synchronized = (value & ACC_DECLARED_SYNCHRONIZED) != 0
    
    def __repr__(self):
    
        access = self.access()
        return "AccessFlags(%s)" % hex(self.value)
    
    def __hash__(self):
        return self.value
    
    def __cmp__(self, other):
        return cmp(self.value, other.value)
    
    def __str__(self):
    
        l = [self.access()]
        if self.static:
            l.append("static")
        if self.final:
            l.append("final")
        if self.synchronized:
            l.append("synchronized")
        if self.volatile:
            l.append("volatile")
        return " ".join(l)
    
    def access(self):
    
        if self.public:
            access = "public"
        elif self.private:
            access = "private"
        elif self.protected:
            access = "protected"
        else:
            access = "?"
        
        return access


class Data(IdContainer):

    def __init__(self, dex):
        self.dex = dex
        self.elements = ""
    
    def read(self, f, size):
        self.elements = f.read(size)
    
    def write(self, f):
    
        data = self.elements
        f.write(data)
        return data

class LinkData(Data):

    def read(self, f, size):
        self.elements = f.read(size)

class Map:

    def __init__(self):
    
        self.structures = {
            TYPE_HEADER_ITEM: None,
            TYPE_STRING_ID_ITEM: None,
            TYPE_TYPE_ID_ITEM: None,
            TYPE_PROTO_ID_ITEM: None,
            TYPE_FIELD_ID_ITEM: None,
            TYPE_METHOD_ID_ITEM: None,
            TYPE_CLASS_DEF_ITEM: None,
            TYPE_MAP_LIST: None,
            TYPE_TYPE_LIST: None,
            TYPE_ANNOTATION_SET_REF_LIST: None,
            TYPE_ANNOTATION_SET_ITEM: None,
            TYPE_CLASS_DATA_ITEM: None,
            TYPE_CODE_ITEM: None,
            TYPE_STRING_DATA_ITEM: None,
            TYPE_DEBUG_INFO_ITEM: None,
            TYPE_ANNOTATION_ITEM: None,
            TYPE_ENCODED_ARRAY_ITEM: None,
            TYPE_ANNOTATIONS_DIRECTORY_ITEM: None
            }
        
        self.start = None
    
    def __len__(self):
        return 1
    
    def __getitem__(self, key):
        return self.structures[key]
    
    def __setitem__(self, key, value):
        self.structures[key] = value
    
    def size(self):
        keys = self.structures.keys()
        keys = filter(lambda key: self.structures[key] != None and \
                                  len(self.structures[key]) > 0, keys)
        
        return calcsize("<I") + (len(keys) * calcsize("<HHII"))
    
    def arrange(self, start = None):
    
        if start != None:
            # Assume that the address is word-aligned.
            self.start = start
            return
        
        keys = self.structures.keys()
        keys.sort()
        offset = 0
        
        for key in keys:
            collection = self.structures[key]
            if collection != None:
                # Update the collection's start address and give it a chance
                # to align itself.
                collection.arrange(offset)
                offset += collection.size()
    
    def resolve(self):
    
        keys = self.structures.keys()
        keys.sort()
        
        for key in keys:
            structure = self.structures[key]
            if isinstance(structure, DataItem) or \
                isinstance(structure, StringIds) or \
                isinstance(structure, ProtoIds) or \
                isinstance(structure, ClassDefs):
            
                structure.resolve()
    
    def write(self):
    
        keys = self.structures.keys()
        keys = filter(lambda key: self.structures[key] != None and \
                                  len(self.structures[key]) > 0, keys)
        keys.sort()
        
        data = pack("<I", len(keys))
        
        for key in keys:
            collection = self.structures[key]
            if collection != None:
                number = len(collection)
                data += pack("<HHII", key, 0, number, collection.start)
            else:
                data += pack("<HHII", key, 0, 0, NO_INDEX)
        
        return data
    
    def write_data(self):
    
        data = ""
        
        keys = self.structures.keys()
        keys.sort()
        
        for key in keys:
            if key >= TYPE_MAP_LIST:
                collection = self.structures[key]
                if collection != None:
                    data += collection.write()
        
        return data


# Define classes to represent structures in the data section and references
# between items inside them.

class Reference:

    _length = calcsize("I")
    
    def __init__(self, structure, index):
    
        self.structure = structure
        self.index = index
    
    def address(self):
        return self.structure.address(self.index)
    
    def __len__(self):
        return self._length

class uleb128Reference(Reference):

    def address(self):
    
        data = write_uleb128(self.structure.address(self.index))
        
        # Pad the number to fill four bytes so that it is aligned correctly in
        # the containing data section. This makes it easier to rearrange
        # offsets to items since they all have the same size. The number must
        # be encoded as a valid four byte uleb128 value even if the offset
        # would not normally require so many bytes.
        
        if len(data) < 4:
            rem = 4 - len(data)
            data = data[:-1] + chr(ord(data[-1]) | 0x80)
            data += (rem - 1) * "\x80"
            data += "\x00"
        
        return data

class DataItem:

    """Represents a collection of objects in the data section of the file."""
    
    def __init__(self, alignment = 1):
    
        self.elements = []
        self.offsets = {}
        self.start = 0
        self.alignment = alignment
        self._size = 0
        self._number = 0
    
    def align(self, n):
    
        # Align with an n byte boundary.
        if n != 1:
            rem = self._size % n
            if rem != 0:
                self.elements.append("\x00" * (n - rem))
                self._size += (n - rem)
        
        return self._size
    
    def add(self, data):
    
        if data not in self.offsets:
            self.offsets[data] = self.align(self.alignment)
            self.elements.append(data)
            self._size += len(data)
            self._number += 1
        
        return Reference(self, data)
    
    def add_unresolved(self, elements):
    
        data = tuple(elements)
        if data not in self.offsets:
            self.offsets[data] = self.align(self.alignment)
            self.elements += elements
            self._size += sum(map(len, elements))
            self._number += 1
        
        return Reference(self, data)
    
    def arrange(self, start):
    
        padding = 0
        
        rem = start % 4
        if rem != 0:
            # Insert padding before the real start of the structure and
            # increase the size to indicate the full amount of data plus
            # padding contained.
            padding = 4 - rem
            self.elements.insert(0, "\x00" * padding)
            self._size += padding
        
        self.start = start + padding
    
    def resolve(self):
        i = 0
        while i < len(self.elements):
            element = self.elements[i]
            if isinstance(element, uleb128Reference):
                self.elements[i] = element.address()
            elif isinstance(element, Reference):
                self.elements[i] = pack("<I", element.address())
            i += 1
    
    def address(self, value):
        return self.start + self.offsets[value]
    
    def size(self, elements = None):
        return self._size
    
    def __len__(self):
        return self._number
    
    def write(self):
    
        data = ""
        for element in self.elements:
            data += element
        
        return data

class StringData(DataItem):

    def add(self, string):
    
        if string not in self.offsets:
            self.offsets[string] = self._size
            element = write_uleb128(len(string)) + write_mutf8(string) + "\x00"
            self.elements.append(element)
            self._size += len(element)
            self._number += 1
        
        return Reference(self, string)

class TypeLists(DataItem):

    def add(self, type_list):
    
        data = pack("<I", len(type_list))
        for index in type_list:
            data += pack("<H", index)
        
        if data not in self.offsets:
            self.offsets[data] = self.align(4)
            self.elements.append(data)
            self._size += len(data)
            self._number += 1
        
        return Reference(self, data)

class CodeItems(DataItem):

    def __init__(self, alignment = 1):
    
        DataItem.__init__(self, alignment)
        self.debug_info = {}
    
    def add(self, data, debug_ref):
    
        self.offsets[data] = self.align(self.alignment)
        self.debug_info[data] = debug_ref
        self.elements.append(data)
        self._size += len(data)
        self._number += 1
        
        # Unlike other items that are referenced in sections of a Dex file,
        # code_items are referred to using offsets encoded as uleb128 values.
        return uleb128Reference(self, data)
    
    def write(self):
    
        data = ""
        before = calcsize("<HHHH")
        after = calcsize("<HHHHI")
        
        for element in self.elements:
            # Resolve the associated debug information and insert it into the
            # code item data.
            if isinstance(element, CodeItem):
                debug_ref = self.debug_info[element]
                debug_addr = debug_ref.address()
                edata = str(element)
                data += edata[:before] + pack("<I", debug_addr) + edata[after:]
            else:
                # Padding
                data += element
        
        return data

class CodeItem:

    def __init__(self, data):
        self.data = data
    
    def __len__(self):
        return len(self.data)
    
    def __str__(self):
        return self.data


class Dex:

    """Represents a Dalvik executable (DEX) file, containing definitions for
    strings, types, prototypes, fields, methods and classes used in an Android
    application.
    
    A DEX file is read by either specifying its file name when creating an
    instance of this class, or by passing a file object to the read method
    after a default instance has been created.
    
    The contents of an instance can be written to a file using either the save
    method or by passing a suitable file object to the write method.
    
    New DEX files can be created by importing suitable structures into a
    default instance via its create method, then calling the save or write
    methods as described above.
    """
    
    _format = [(Header, "header"),
               (StringIds, "string_ids"),
               (TypeIds, "type_ids"),
               (ProtoIds, "proto_ids"),
               (FieldIds, "field_ids"),
               (MethodIds, "method_ids"),
               (ClassDefs, "class_defs"),
               (Data, "data"),
               (LinkData, "link")]
    
    def __init__(self, file_name = None, dex_obj = None):
    
        if file_name:
            self.read(open(file_name))
        elif dex_obj:
            self.create(*dex_obj.export())
    
    def read(self, f):
    
        """Reads the contents of the DEX file specified by the file object, f,
        and fills in the internal structures corresponding to those in the file.
        """
        # Read the header first, then use its contents to read the rest of the
        # file.
        
        self.header = Header(self)
        self.header.read(f)
        
        if self.header.endian_tag != 0x12345678:
            raise DecodeError, "Cannot parse big-endian files."
        
        p = f.tell()
        f.seek(12)
        
        contents = f.read()
        checksum = zlib.adler32(contents) & 0xffffffff
        if checksum != self.header.checksum:
            raise DecodeError, "Checksum %s does not match file content." % hex(self.header.checksum)
        
        signature = Crypto.Hash.SHA.new(contents[20:]).digest()
        if signature != self.header.signature:
            raise DecodeError, "Signature %s does not match file content." % repr(self.header.signature)
        
        f.seek(p)
        
        for Structure, name in self._format[1:]:
        
            offset_attr = name + "_offset"
            size_attr = name + "_size"
            
            offset = getattr(self.header, offset_attr)
            f.seek(offset)
            size = getattr(self.header, size_attr)
            
            element = Structure(self)
            element.read(f, size)
            setattr(self, name, element)
        
        self.map = self.read_map(f)
    
    def read_map(self, f):
    
        m = []
        
        if self.header.map_offset != 0:
        
            f.seek(self.header.map_offset)
            list_size = unpack("<I", f.read(4))[0]
            while list_size > 0:
                type_, unused, size, offset = unpack("<HHII", f.read(12))
                m.append((type_, unused, size, offset))
                list_size -= 1
        
        return m
    
    def export(self):
    
        # Dereference all the objects in the file.
        strings = list(self.string_ids)
        types = list(self.type_ids)
        prototypes = list(self.proto_ids)
        fields = list(self.field_ids)
        methods = list(self.method_ids)
        classes = list(self.class_defs)
        
        return strings, types, prototypes, fields, methods, classes
    
    def create(self, strings, types, prototypes, fields, methods, classes):
    
        # Create a file map.
        self.map = Map()
        self.map[TYPE_MAP_LIST] = self.map
        
        # Define structures to represent collections of items in the data
        # section.
        self.map[TYPE_TYPE_LIST] = type_lists = TypeLists()
        self.map[TYPE_ANNOTATION_SET_REF_LIST] = annotation_set_ref_lists = \
                                                 DataItem(alignment = 4)
        self.map[TYPE_ANNOTATION_SET_ITEM] = annotation_set_items = \
                                             DataItem(alignment = 4)
        self.map[TYPE_CLASS_DATA_ITEM] = class_data_items = DataItem()
        self.map[TYPE_CODE_ITEM] = code_items = CodeItems(alignment = 4)
        self.map[TYPE_STRING_DATA_ITEM] = string_data_items = StringData()
        self.map[TYPE_DEBUG_INFO_ITEM] = debug_info_items = DataItem()
        self.map[TYPE_ANNOTATION_ITEM] = annotation_items = DataItem()
        self.map[TYPE_ENCODED_ARRAY_ITEM] = encoded_array_items = DataItem()
        self.map[TYPE_ANNOTATIONS_DIRECTORY_ITEM] = annotations_directory_items = \
                                                    DataItem(alignment = 4)
        
        self.map[TYPE_HEADER_ITEM] = self.header = Header(self)
        
        # Calculate the sizes in bytes of all the sections up to the data
        # section.
        data_start = self.header.size() + \
                     len(strings)    * calcsize(StringIds.ItemFormat) + \
                     len(types)      * calcsize(TypeIds.ItemFormat) + \
                     len(prototypes) * calcsize(ProtoIds.ItemFormat) + \
                     len(fields)     * calcsize(FieldIds.ItemFormat) + \
                     len(methods)    * calcsize(MethodIds.ItemFormat) + \
                     len(classes)    * calcsize(ClassDefs.ItemFormat)
        
        # Map strings back to indices, compile a collection of strings for
        # later storage, and create references to the strings for later
        # resolution.
        
        self.map[TYPE_STRING_ID_ITEM] = self.string_ids = StringIds(self)
        self.string_ids.index = {}
        
        for i, string in enumerate(strings):
            self.string_ids.index[string] = i
            ref = string_data_items.add(string)
            self.string_ids.append(ref)
        
        # Map types back to indices and compile a collection of string indices
        # for the types.
        
        self.map[TYPE_TYPE_ID_ITEM] = self.type_ids = TypeIds(self)
        self.type_ids.index = {}
        
        for i, type_ in enumerate(types):
            s = type_.desc
            self.type_ids.index[s] = i
            index = self.string_ids.index[s]
            self.type_ids.append(index)
        
        # Map prototypes to indices and add parameter types to a collection,
        # creating references to them for later resolution.
        
        self.map[TYPE_PROTO_ID_ITEM] = self.proto_ids = ProtoIds(self)
        self.proto_ids.index = {}
        
        for i, prototype in enumerate(prototypes):
        
            self.proto_ids.index[prototype] = i
            shorty_string = "".join(map(lambda arg: arg.desc, prototype.shorty))
            shorty_index = self.string_ids.index[shorty_string]
            return_type_index = self.type_ids.index[prototype.return_type.desc]
            
            if prototype.parameters:
                # Find the types of the parameters.
                type_list = []
                for p in prototype.parameters:
                    type_list.append(self.type_ids.index[p.desc])
                
                # Add the type list to the collection of type lists.
                ref = type_lists.add(type_list)
                parameters_offset = ref
            else:
                parameters_offset = 0
            
            self.proto_ids.append((shorty_index, return_type_index, parameters_offset))
        
        # Map fields back to indices and record the indices of types and
        # strings that make up each field.
        
        self.map[TYPE_FIELD_ID_ITEM] = self.field_ids = FieldIds(self)
        self.field_ids.index = {}
        
        for i, field in enumerate(fields):
        
            self.field_ids.index[field] = i
            class_type_index = self.type_ids.index[field.class_type.desc]
            type_idx = self.type_ids.index[field.type_.desc]
            name_index = self.string_ids.index[field.name.desc]
            self.field_ids.append((class_type_index, type_idx, name_index))
        
        # Map methods back to indices and record the indices of types, strings
        # and prototypes that make up each method.
        
        self.map[TYPE_METHOD_ID_ITEM] = self.method_ids = MethodIds(self)
        self.method_ids.index = {}
        
        for i, method in enumerate(methods):
        
            self.method_ids.index[method] = i
            class_type_index = self.type_ids.index[method.class_type.desc]
            proto_idx = self.proto_ids.index[method.proto]
            name_index = self.string_ids.index[method.name.desc]
            self.method_ids.append((class_type_index, proto_idx, name_index))
        
        # Write class definitions, add interface types to a collection, and
        # compile collections of class data, code, annotations and other items.
        
        self.map[TYPE_CLASS_DEF_ITEM] = self.class_defs = ClassDefs(self)
        
        for i, class_def in enumerate(classes):
        
            class_type_index = self.type_ids.index[class_def.class_type.desc]
            access_flags = class_def.access_flags.value
            
            if class_def.superclass_type:
                superclass_type_index = self.type_ids.index[class_def.superclass_type.desc]
            else:
                superclass_type_index = NO_INDEX
            
            if class_def.interfaces:
                # Find the types of the interfaces.
                type_list = []
                for class_type in class_def.interfaces:
                    type_list.append(self.type_ids.index[class_type.desc])
                
                # Add the type list to the collection of type lists.
                ref = type_lists.add(type_list)
                interfaces_offset = ref
            else:
                interfaces_offset = 0
            
            if class_def.source_file:
                source_file_index = self.string_ids.index[class_def.source_file]
            else:
                source_file_index = NO_INDEX
            
            if class_def.annotations:
                annotations = class_def.annotations.copy(self)
                # Serialise the annotations directory, adding it to the
                # appropriate collection, and adding other annotation items to
                # the relevant collections.
                annotations_offset = annotations.write(self,
                    annotations_directory_items, annotation_set_ref_lists,
                    annotation_set_items, annotation_items)
            else:
                annotations_offset = 0
            
            if class_def.class_data:
                # Serialise the class data, using the code_items collection to
                # make references to the code.
                class_data_offset = class_def.class_data.write(self,
                    class_data_items, code_items, debug_info_items)
            else:
                class_data_offset = 0
            
            if class_def.static_values:
                # Serialise the encoded array and store it in a collection for
                # later resolution.
                data = self.class_defs.write_encoded_array(
                    self, class_def.static_values)
                static_values_offset = encoded_array_items.add(data)
            else:
                static_values_offset = 0
                
            self.class_defs.append((class_type_index, access_flags,
                superclass_type_index, interfaces_offset, source_file_index,
                annotations_offset, class_data_offset, static_values_offset))
        
        # Create a new data section to hold these objects and new indexes.
        self.data = Data(self)
        
        self.link = LinkData(self)
        
        # Update the header with the sizes (counts of elements) of all the
        # sections.
        self.header.string_ids_size = len(self.string_ids)
        self.header.type_ids_size = len(self.type_ids)
        self.header.proto_ids_size = len(self.proto_ids)
        self.header.field_ids_size = len(self.field_ids)
        self.header.method_ids_size = len(self.method_ids)
        self.header.class_defs_size = len(self.class_defs)
        
        # Place the different sections in order in the file.
        self.header.header_size = self.header.size()
        self.header.string_ids_offset = self.header.header_size
        self.header.type_ids_offset = self.header.string_ids_offset + self.string_ids.size()
        self.header.proto_ids_offset = self.header.type_ids_offset + self.type_ids.size()
        self.header.field_ids_offset = self.header.proto_ids_offset + self.proto_ids.size()
        self.header.method_ids_offset = self.header.field_ids_offset + self.field_ids.size()
        self.header.class_defs_offset = self.header.method_ids_offset + self.method_ids.size()
        self.header.data_offset = self.header.class_defs_offset + self.class_defs.size()
        
        # Arrange the locations of the entries in the map and resolve addresses.
        self.map.arrange()
        self.map.resolve()
        self.header.map_offset = self.map.start
        
        self.data.elements = self.map.write_data()
        
        # The number of elements in the data and link sections is also the number of
        # bytes in each section.
        self.header.data_size = len(self.data)
        self.header.link_size = len(self.link)
        
        # Write the link data details and calculate the file size.
        if self.header.link_size > 0:
            self.header.link_offset = self.header.data_offset + self.header.data_size
            self.header.file_size = self.header.link_offset + self.header.link_size
        else:
            self.header.link_offset = 0
            self.header.file_size = self.header.data_offset + self.header.data_size
    
    def write(self, f):
    
        """Writes the contents of the instance to the DEX file specified by the
        file object, f."""
        
        self.header.checksum = 0
        self.header.signature = "\x00"*20
        self.header.endian_tag = 0x12345678
        
        # Write the basic header structure and each of the sections to the
        # file, but revisit the header later to fill in the checksum and
        # signature.
        written = ""
        
        for section_class, name in self._format:
        
            section = getattr(self, name)
            written += section.write(f)
        
        # Write the checksum and signature in the header.
        self.header.write_checksum_signature(f, written)
    
    def save(self, file_name):
    
        """Saves the contents of the instance to a DEX file with the specified
        file_name."""
        
        f = open(file_name, "wb")
        self.write(f)
        f.close()
