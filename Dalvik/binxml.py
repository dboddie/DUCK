"""
Copyright (C) 2016 David Boddie <david@boddie.org.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

# See https://justanapplication.wordpress.com/android-internals/ for
# information about the binary XML format used in Android packaging.

import codecs, sys
from struct import calcsize, pack, unpack

NO_ENTRY = 0xffffffff

FLAG_COMPLEX = 0x0001
FLAG_PUBLIC  = 0x0002

# Attribute types - see the android.util.TypedValue class for a more complete
# list.
TYPE_NULL            = 0x00
TYPE_REFERENCE       = 0x01
TYPE_ATTRIBUTE       = 0x02
TYPE_STRING          = 0x03
TYPE_FLOAT           = 0x04
TYPE_DIMENSION       = 0x05
TYPE_FRACTION        = 0x06
TYPE_INT_DEC         = 0x10
TYPE_INT_HEX         = 0x11
TYPE_BOOLEAN         = 0x12
TYPE_INT_COLOR_ARGB8 = 0x1c
TYPE_INT_COLOR_RGB8  = 0x1d
TYPE_INT_COLOR_ARGB4 = 0x1e
TYPE_INT_COLOR_RGB4  = 0x1f

class BinaryXML:

    def read_chunk(self, f):
    
        chunk_type, header_size = unpack("<HH", f.read(4))
        chunk_class = self.chunk_classes.get(chunk_type)
        if chunk_class:
            return chunk_class().read(f, header_size)
        else:
            sys.stderr.write("%s: Unknown chunk of type %s\n" % (hex(f.tell() - 4), hex(chunk_type)))
            chunk = Chunk().read(f, header_size, type = chunk_type)
            f.seek(chunk.address + chunk.length)
            raise ValueError
            return chunk

class Chunk(BinaryXML):

    def read(self, f, header_size, type = None):
    
        self.address = f.tell() - 4
        self.header_size = header_size
        self.length = unpack("<I", f.read(4))[0]
        
        if type != None:
            self.type = type
        
        return self
    
    def read_word(self, f):
    
        return unpack("<I", f.read(4))[0]
    
    def read_string(self, f, utf8 = False):
    
        if utf8:
            ### How are UTF-8 strings with more than 127 characters encoded?
            string_length, byte_length = unpack("<BB", f.read(2))
            return codecs.utf_8_decode(f.read(byte_length))[0]
        else:
            string_length = unpack("<H", f.read(2))[0]
            if string_length & 0x8000:
                string_length = unpack("<H", f.read(2))[0] | (string_length & 0x7fff)
            return codecs.utf_16_le_decode(f.read(string_length * 2))[0]
    
    def read_fixed_length_string(self, f, length):
    
        start = f.tell()
        s = ""
        i = 0
        while i < length:
            c = f.read(2)
            if c == "\x00\x00":
                break
            else:
                s += c
                i += 1
        
        f.seek(start + (length * 2))
        return codecs.utf_16_le_decode(s)[0]
    
    def write(self, f):
    
        self.address = f.tell()
        
        # Write the type and header size.
        f.write(pack("<HH", self.type, self.header_size))
        
        # Recalculate the length and write it.
        f.write(pack("<I", self.size()))
    
    def write_word(self, f, value):
    
        f.write(pack("<I", value))
    
    def write_string(self, f, s, utf8 = False):
    
        if utf8:
            data, l = codecs.utf_8_encode(s)
            f.write(pack("<BB"), len(s), l)
            f.write(data + "\x00")
        else:
            data, l = codecs.utf_16_le_encode(s)
            f.write(pack("<H", len(s)))
            f.write(data + "\x00\x00")
    
    def write_fixed_length_string(self, f, s, length):
    
        data, l = codecs.utf_16_le_encode(s[:length])
        if l < length:
            data += (length - l) * "\x00\x00"
        
        f.write(data)

class StringPool(Chunk):

    type = 1
    
    def __init__(self, strings = None, styles = None):
    
        if strings == None: strings = []
        if styles == None: styles = []
        
        self.header_size = 28
        self.strings = strings
        self.styles = styles
    
    def read(self, f, header_size):
    
        Chunk.read(self, f, header_size)
        
        string_count, style_count, flags = unpack("<III", f.read(12))
        sorted = (flags & 0x1) != 0
        utf8 = (flags & 0x100) != 0
        strings_start = self.address + self.read_word(f)
        styles_start = self.address + self.read_word(f)
        
        # Read the offsets into the string data.
        string_entries = []
        i = 0
        while i < string_count:
            string_entries.append(self.read_word(f))
            i += 1
        
        # Read the offsets into the style data.
        style_entries = []
        i = 0
        while i < style_count:
            style_entries.append(self.read_word(f))
            i += 1
        
        # Read the strings.
        strings = []
        i = 0
        while i < string_count:
            f.seek(strings_start + string_entries[i])
            strings.append(self.read_string(f, utf8))
            i += 1
        
        # Read the styles.
        styles = []
        i = 0
        while i < style_count:
            f.seek(styles_start + style_entries[i])
            name = self.read_word(f)
            styles.append([])
            
            while name != 0xffffffff:
                first_char, last_char = unpack("<II", f.read(8))
                styles[-1].append((name, first_char, last_char))
                name = self.read_word(f)
            
            i += 1
        
        self.strings = strings
        self.styles = styles
        
        f.seek(self.address + self.length)
        return self
    
    def __getitem__(self, index):
    
        return self.strings[index]
    
    def __setitem__(self, index, value):
    
        self.strings[index] = value
    
    def find(self, string):
    
        if string in self.strings:
            return self.strings.index(string)
        else:
            return NO_ENTRY
    
    def index(self, string, initial = 0):
    
        if string in self.strings:
            return self.strings.index(string) + initial
        else:
            raise ValueError, "String '%s' not found in %s." % (string, self)
    
    def toxml(self, indent, f=sys.stdout):
    
        print >>f, (u" " * indent) + u"<StringPool>"
        
        for string in self.strings:
            print >>f, (u" " * (indent + 2)) + u"<string>%s</string>" % string
        
        for style in self.styles:
            print >>f, (u" " * (indent + 2)) + u"<style>"
            for name, first_char, last_char in style:
                print >>f, (u" " * (indent + 2)) + \
                    u'<span name="%s" firstChar="%i" lastChar="%i">' % (
                        self.strings[name], first_char, last_char)
            print >>f, (u" " * (indent + 2)) + u"</style>"
        
        print >>f, (u" " * indent) + u"</StringPool>"
    
    def _string_data_size(self):
        # Include the count bytes and terminating two null bytes.
        return sum(map(lambda s: (len(s) * 2) + 4, self.strings))
    
    def _style_data_size(self):
        # Include the terminating 0xffffffff for each list of spans.
        return sum(map(lambda spans: (len(spans) * 12) + 4, self.styles))
    
    def size(self):
    
        size = self.header_size + (len(self.strings) * 4) + \
            (len(self.styles) * 4) + self._string_data_size() + \
            self._style_data_size()
        
        rem = size % 4
        if rem != 0:
            size += (4 - rem)
        
        return size
    
    def write(self, f):
    
        Chunk.write(self, f)
        
        # Write the basic information about the strings and styles.
        flags = 0   # unsorted, UTF-16
        f.write(pack("<III", len(self.strings), len(self.styles), flags))
        
        # Calculate and write the offsets of the string and style data.
        strings_start = self.header_size + (len(self.strings) * 4) + \
                                           (len(self.styles) * 4)
        if self.styles:
            styles_start = strings_start + self._string_data_size()
        else:
            styles_start = 0
        
        f.write(pack("<II", strings_start, styles_start))
        
        # Write the string offsets.
        offset = 0
        for string in self.strings:
            f.write(pack("<I", offset))
            offset += (len(string) * 2) + 4
        
        # Write the style offsets.
        offset = 0
        for style in self.styles:
            f.write(pack("<I", offset))
            offset += (len(style) * 12) + 4
        
        for string in self.strings:
            self.write_string(f, string)
        
        for style in self.styles:
            for name, first_char, last_char in style:
                f.write(pack("<III", self.strings.index(name), first_char, last_char))
            f.write(pack("<I", 0xffffffff))
        
        # Add padding so that the next chunk is word-aligned.
        end = self.address + self.size()
        while f.tell() < end:
            f.write("\x00")

class Table(Chunk):

    type = 2
    
    def __init__(self, string_pool = None, packages = None):
    
        self.header_size = 12
        self.string_pool = string_pool
        self.packages = packages
    
    def read(self, f, header_size):
    
        Chunk.read(self, f, header_size)
        number_of_packages = self.read_word(f)
        
        self.string_pool = self.read_chunk(f)
        self.packages = {}
        
        i = 0
        while i < number_of_packages:
            package = self.read_chunk(f)
            self.packages[package.id] = package
            i += 1
        
        return self
    
    def toxml(self, indent = 0, f=sys.stdout):
    
        print >>f, u"<Table>"
        
        self.string_pool.toxml(indent + 2, f)
        
        items = self.packages.items()
        items.sort()
        
        for id, package in items:
            package.toxml(indent + 2, self.string_pool, f)
        
        print >>f, u"</Table>"
    
    def size(self):
    
        return self.header_size + self.string_pool.size() + \
            sum(map(lambda p: p.size(), self.packages.values()))
    
    def write(self, f):
    
        Chunk.write(self, f)
        
        self.write_word(f, len(self.packages))
        self.string_pool.write(f)
        
        items = self.packages.items()
        items.sort()
        
        for id, package in items:
            package.write(f)

class XML(Chunk):

    type = 3
    
    def __init__(self, string_pool = None, contents = None):
    
        self.header_size = 8
        self.string_pool = string_pool
        self.contents = contents
    
    def read(self, f, header_size):
    
        Chunk.read(self, f, header_size)
        
        self.string_pool = self.read_chunk(f)
        
        end = self.address + self.length
        self.contents = []
        
        while f.tell() < end:
            self.contents.append(self.read_chunk(f))
        
        return self
    
    def toxml(self, table, indent = 0, f=sys.stdout):
    
        namespaces = {}
        to_apply = None
        
        for chunk in self.contents:
        
            if isinstance(chunk, XMLResourceMap):
                chunk.toxml(indent, self.string_pool, f)
            
            if isinstance(chunk, XMLStartNamespace):
                # Record the namespace definition and queue it for application
                # to the next element.
                namespaces[chunk.uri] = chunk.prefix
                to_apply = (self.string_pool[chunk.prefix], self.string_pool[chunk.uri])
            
            elif isinstance(chunk, XMLEndNamespace):
                to_apply = None
            
            elif isinstance(chunk, XMLStartElement):
                print >>f, (indent * u" ") + \
                    chunk.toxml(self.string_pool, namespaces, to_apply, table, f)
                to_apply = None
                indent += 2
            
            elif isinstance(chunk, XMLEndElement):
                indent -= 2
                print >>f, (indent * u" ") + \
                    chunk.toxml(self.string_pool, namespaces, f)
    
    def size(self):
    
        return self.header_size + self.string_pool.size() + \
            sum(map(lambda e: e.size(), self.contents))
    
    def write(self, f):
    
        Chunk.write(self, f)
        
        self.string_pool.write(f)
        
        namespaces = {}
        
        for chunk in self.contents:
            chunk.write(f, self.string_pool, namespaces)

class XMLStartNamespace(Chunk):

    type = 0x100
    
    def __init__(self, line_number = 0, comment = None, prefix = u"", uri = u""):
    
        self.header_size = 16
        
        self.line_number = line_number
        self.comment = comment
        self.prefix = prefix
        self.uri = uri
    
    def read(self, f, header_size):
    
        Chunk.read(self, f, header_size)
        
        self.line_number = self.read_word(f)
        self.comment = self.read_word(f)
        self.prefix = self.read_word(f)
        self.uri = self.read_word(f)
        
        return self
    
    def size(self):
    
        # The header includes the line number and comment.
        return self.header_size + 8
    
    def write(self, f, string_pool, namespaces):
    
        Chunk.write(self, f)
        
        f.write(pack("<IIII", self.line_number,
            string_pool.find(self.comment),
            string_pool.index(self.prefix),
            string_pool.index(self.uri)))
        
        # Translate the prefix to the corresponding URI.
        namespaces[self.prefix] = self.uri

class XMLEndNamespace(XMLStartNamespace):

    type = 0x101
    
    def __init__(self, line_number = 0, comment = None, prefix = u"", uri = u""):
    
        self.header_size = 16
        
        self.line_number = line_number
        self.comment = comment
        self.prefix = prefix
        self.uri = uri
    
    def read(self, f, header_size):
    
        Chunk.read(self, f, header_size)
        
        self.line_number = self.read_word(f)
        self.comment = self.read_word(f)
        self.prefix = self.read_word(f)
        self.uri = self.read_word(f)
        
        return self

class XMLStartElement(Chunk):

    type = 0x102
    
    def __init__(self, line_number = 0, comment = None, namespace = None,
                       name = u"", attributes = None):
    
        self.header_size = 16
        
        self.line_number = line_number
        self.comment = comment
        self.namespace = namespace
        self.name = name
        
        if attributes == None: attributes = []
        self.attributes = attributes
    
    def read(self, f, header_size):
    
        Chunk.read(self, f, header_size)
        
        self.line_number = self.read_word(f)
        self.comment = self.read_word(f)
        
        start = f.tell()
        
        self.namespace = self.read_word(f)
        self.name = self.read_word(f)
        
        attribute_start, attribute_size = unpack("<HH", f.read(4))
        attribute_count, id_index = unpack("<HH", f.read(4))
        class_index, style_index = unpack("<HH", f.read(4))
        
        f.seek(start + attribute_start)
        
        self.attributes = []
        i = 0
        while i < attribute_count:
            attr_start = f.tell()
            self.attributes.append(XMLAttribute().read(f))
            i += 1
        
        return self
    
    def toxml(self, string_pool, namespaces, to_apply, table, f=sys.stdout):
    
        s = u"<"
        if self.namespace != NO_ENTRY:
            uri = self.namespace
            prefix = namespaces[uri]
            s += string_pool[prefix] + u":"
        
        s += string_pool[self.name]
        
        if to_apply != None:
            s += u' xmlns:%s="%s"' % to_apply
        
        if self.attributes:
            for attr in self.attributes:
                s += u" " + attr.toxml(string_pool, namespaces, table, f)
        
        s += u">"
        return s
    
    def size(self):
    
        # The header includes the line number and comment.
        return self.header_size + 20 + sum(map(lambda a: a.size(), self.attributes))
    
    def write(self, f, string_pool, namespaces):
    
        Chunk.write(self, f)
        
        # Translate the namespace prefix into a URI.
        uri = namespaces.get(self.namespace)
        
        f.write(pack("<IIII", self.line_number,
            string_pool.find(self.comment),
            string_pool.find(uri),
            string_pool.index(self.name)))
        
        ### These values are currently fixed.
        id_index = class_index = style_index = 0
        
        # The attributes start 20 bytes afer the header.
        attribute_start = 20
        
        f.write(pack("<HH", attribute_start, 20))
        f.write(pack("<HH", len(self.attributes), id_index))
        f.write(pack("<HH", class_index, style_index))
        
        for attribute in self.attributes:
            attribute.write(f, string_pool, namespaces)

class XMLAttribute:

    def __init__(self, namespace = None, name = u"", raw_value = None,
                       data_type = TYPE_STRING, value = 0):
    
        self.namespace = namespace
        self.name = name
        self.raw_value = raw_value
        self.data_type = data_type
        self.value = value
    
    def read(self, f):
    
        self.namespace, self.name, self.raw_value = unpack("<III", f.read(12))
        size, res0, self.data_type = unpack("<HBB", f.read(4))
        self.value = unpack("<I", f.read(4))[0]
        return self
    
    def toxml(self, string_pool, namespaces, table, f=sys.stdout):
    
        s = u""
        if self.namespace != NO_ENTRY:
            uri = self.namespace
            prefix = namespaces[uri]
            s += string_pool[prefix] + u":"
        
        s += string_pool[self.name] + u"="
        if self.data_type == TYPE_REFERENCE:
            # The reference contains the package number and an index into the
            # types.
            package = table.packages[self.value >> 24]
            type_number = (self.value >> 16) & 0xff
            type_name = package.type_strings[type_number - 1]
            type_ = package.types[type_number][(self.value >> 8) & 0xff]
            resource = type_.entries[self.value & 0xff]
            
            s += u'"@%s/%s"' % (type_name, package.key_strings[resource.key])
        elif self.data_type == TYPE_STRING:
            s += u'"' + string_pool[self.value] + u'"'
        elif self.data_type == TYPE_FLOAT:
            s += u'"%f"' % self.value
        elif self.data_type == TYPE_INT_DEC:
            s += u'"%i"' % self.value
        elif self.data_type == TYPE_INT_HEX:
            s += u'"0x%x"' % self.value
        elif self.data_type == TYPE_BOOLEAN:
            s += {0: u'"false"', 1: u'"true"', 0xffffffff: u'"true"'}[self.value]
        elif self.data_type == TYPE_INT_COLOR_ARGB8:
            s += u'#%08x' % self.value
        elif self.data_type == TYPE_INT_COLOR_RGB8:
            s += u'#%06x' % self.value
        elif self.data_type == TYPE_INT_COLOR_ARGB4:
            s += u'#%04x' % self.value
        elif self.data_type == TYPE_INT_COLOR_RGB4:
            s += u'#%03x' % self.value
        elif self.data_type == TYPE_DIMENSION:
            s += u'"%i%s"' % (self.value >> 8, {1: u'dp'}[self.value & 0xff])
        else:
            raise ValueError, "Unhandled data type 0x%x for %s" % (
                self.data_type, repr(self))
        
        return s
    
    def size(self):
    
        return 20
    
    def write(self, f, string_pool, namespaces):
    
        # Translate the namespace prefix into a URI.
        uri = namespaces.get(self.namespace)
        
        f.write(pack("<III",
            string_pool.find(uri),
            string_pool.index(self.name),
            string_pool.find(self.raw_value)))
        
        f.write(pack("<HBB", 8, 0, self.data_type))
        
        ### For now, the creator needs to set the appropriate value for the
        ### data type.
        f.write(pack("<I", self.value))

class XMLEndElement(Chunk):

    type = 0x103
    
    def __init__(self, line_number = 0, comment = None, namespace = None,
                       name = u""):
    
        self.header_size = 16
        
        self.line_number = line_number
        self.comment = comment
        self.namespace = namespace
        self.name = name
    
    def read(self, f, header_size):
    
        Chunk.read(self, f, header_size)
        
        self.line_number = self.read_word(f)
        self.comment = self.read_word(f)
        self.namespace = self.read_word(f)
        self.name = self.read_word(f)
        
        return self
    
    def toxml(self, string_pool, namespaces, f=sys.stdout):
    
        s = u"</"
        if self.namespace != NO_ENTRY:
            uri = self.namespace
            prefix = namespaces[uri]
            s += string_pool[prefix] + u":"
        
        s += string_pool[self.name] + u">"
        return s
    
    def size(self):
    
        # The header includes the line number and comment.
        return self.header_size + 8
    
    def write(self, f, string_pool, namespaces):
    
        Chunk.write(self, f)
        
        self.write_word(f, self.line_number)
        self.write_word(f, string_pool.find(self.comment))
        self.write_word(f, string_pool.find(self.namespace))
        self.write_word(f, string_pool.index(self.name))

class XMLCDATA(Chunk):

    type = 0x104
    
    def read(self, f, header_size):
    
        Chunk.read(self, f, header_size)
        
        self.line_number = self.read_word(f)
        self.comment = self.read_word(f)
        
        self.namespace = self.read_word(f)
        self.name = self.read_word(f)
        
        f.seek(self.address + self.length)
        return self

class XMLResourceMap(Chunk):

    type = 0x180
    
    def __init__(self, ids = None):
    
        self.header_size = 8
        
        if ids != None:
            self.ids = ids
        else:
            self.ids = []
    
    def read(self, f, header_size):
    
        Chunk.read(self, f, header_size)
        
        end = self.address + self.length
        self.ids = []
        
        # These are defined in android-sdk-linux/docs/reference/android/R.attr.html
        while f.tell() < end:
            self.ids.append(self.read_word(f))
        
        return self
    
    def toxml(self, indent, string_pool, f=sys.stdout):
    
        i = indent * u" "
        print >>f, i + u"<!-- ResourceMap"
        
        for j, id in enumerate(self.ids):
            print >>f, i + hex(id), string_pool[j]
        
        print >>f, i + u"-->"
    
    def size(self):
    
        # The header includes the line number and comment.
        return self.header_size + (len(self.ids) * 4)
    
    def write(self, f, string_pool, namespaces):
    
        Chunk.write(self, f)
        
        for id in self.ids:
            self.write_word(f, id)

class Package(Chunk):

    type = 0x200
    
    def __init__(self, id = 0, package_name = u"", type_strings = None,
                       last_public_type = 0, key_strings = None,
                       last_public_key = 0, type_specs = None, types = None):
    
        self.header_size = 0x11c
        self.id = id
        self.package_name = package_name
        
        if type_strings == None: type_strings = []
        self.type_strings = type_strings
        self.last_public_type = last_public_type
        
        if key_strings == None: key_strings = []
        self.key_strings = key_strings
        self.last_public_key = last_public_key
        
        if type_specs == None: type_specs = {}
        self.type_specs = type_specs
        
        if types == None: types = {}
        self.types = types
    
    def read(self, f, header_size):
    
        Chunk.read(self, f, header_size)
        
        self.id = self.read_word(f)
        self.package_name = self.read_fixed_length_string(f, 128)
        
        type_strings_offset = self.read_word(f)
        self.last_public_type = self.read_word(f)
        key_strings_offset = self.read_word(f)
        self.last_public_key = self.read_word(f)
        
        if type_strings_offset != 0:
            f.seek(self.address + type_strings_offset)
            self.type_strings = self.read_chunk(f)
        else:
            self.type_strings = None
        
        if key_strings_offset != 0:
            f.seek(self.address + key_strings_offset)
            self.key_strings = self.read_chunk(f)
        else:
            self.key_strings = None
        
        end = self.address + self.length
        
        self.type_specs = {}
        self.types = {}
        
        while f.tell() < end:
            value = self.read_chunk(f)
            if isinstance(value, TypeSpec):
                self.type_specs[value.id] = value
            elif isinstance(value, Type):
                self.types.setdefault(value.id, []).append(value)
        
        f.seek(end)
        return self
    
    def toxml(self, indent, string_pool, f=sys.stdout):
    
        print >>f, (indent * u" ") + \
            u'<Package id="%s" name="%s">' % (self.id, self.package_name)
        
        if self.type_strings != None:
            print >>f, ((indent + 2) * u" ") + u"<typeStrings>"
            self.type_strings.toxml(indent + 4, f)
            print >>f, ((indent + 2) * u" ") + u"</typeStrings>"
        
        if self.key_strings != None:
            print >>f, ((indent + 2) * u" ") + u"<keyStrings>"
            self.key_strings.toxml(indent + 4, f)
            print >>f, ((indent + 2) * u" ") + u"</keyStrings>"
        
        print >>f, ((indent + 2) * u" ") + u"<typeSpecs>"
        
        items = self.type_specs.items()
        items.sort()
        
        for id, type_spec in items:
            type_spec.toxml(indent + 4, f)
        
        print >>f, ((indent + 2) * u" ") + u"</typeSpecs>"
        
        print >>f, ((indent + 2) * u" ") + u"<types>"
        
        items = self.types.items()
        items.sort()
        
        for id, types in items:
            for type_ in types:
                type_.toxml(indent + 4, f)
        
        print >>f, ((indent + 2) * u" ") + u"</types>"
        
        print >>f, (indent * u" ") + u"</Package>"
    
    def size(self):
    
        size = self.header_size
        
        if self.type_strings != None:
            size += self.type_strings.size()
        if self.key_strings != None:
            size += self.key_strings.size()
        
        return size + sum(map(lambda t: t.size(), self.type_specs.values())) + \
            sum(map(lambda t: sum(map(lambda r: r.size(), t)), self.types.values()))
    
    def write(self, f):
    
        Chunk.write(self, f)
        
        self.write_word(f, self.id)
        self.write_fixed_length_string(f, self.package_name, 128)
        
        if self.type_strings != None:
            type_strings_offset = self.header_size
        else:
            type_strings_offset = 0
        
        if self.key_strings != None:
            if type_strings_offset != 0:
                key_strings_offset = type_strings_offset + self.type_strings.size()
            else:
                key_strings_offset = self.header_size
        else:
            key_strings_offset = 0
        
        f.write(pack("<IIII", type_strings_offset, self.last_public_type,
                              key_strings_offset, self.last_public_key))
        
        if self.type_strings != None:
            self.type_strings.write(f)
        
        if self.key_strings != None:
            self.key_strings.write(f)
        
        keys = self.type_specs.keys()
        keys.sort()
        for key in keys:
            self.type_specs[key].write(f)
            for type_ in self.types.get(key, []):
                type_.write(f)

class Type(Chunk):

    # https://justanapplication.wordpress.com/2011/09/18/android-internals-resources-part-seven-the-type-chunk/
    # See the comments for an update about the header size.
    
    type = 0x201
    
    def __init__(self, id = 0, config = None, entries = None):
    
        self.header_size = 0x38
        
        self.id = id
        if config == None: config = Config()
        self.config = config
        
        if entries == None: entries = []
        self.entries = entries
    
    def read(self, f, header_size):
    
        Chunk.read(self, f, header_size)
        
        self.id = self.read_word(f) & 0xff
        
        entry_count = self.read_word(f)
        entries_start = self.address + self.read_word(f)
        
        self.config = Config().read(f)
        
        offsets = []
        i = 0
        while i < entry_count:
            offsets.append(self.read_word(f))
            i += 1
        
        self.entries = []
        
        i = 0
        while i < entry_count:
            f.seek(entries_start + offsets[i])
            if offsets[i] != NO_ENTRY:
                self.entries.append(Resource().read(f))
            else:
                self.entries.append(None)
            i += 1
        
        f.seek(self.address + self.length)
        return self
    
    def toxml(self, indent, f=sys.stdout):
    
        print >>f, (indent * u" ") + u'<Type id="%i">' % self.id
        
        self.config.toxml(indent + 2, f)
        
        print >>f, ((indent + 2) * u" ") + u"<Entries>"
        
        for entry in self.entries:
            if entry != None:
                entry.toxml(indent + 4, f)
            else:
                print >>f, ((indent + 4) * u" ") + u"<NO_ENTRY/>"
        
        print >>f, ((indent + 2) * u" ") + u"</Entries>"
        print >>f, (indent * u" ") + u"</Type>"
    
    def size(self):
    
        size = self.header_size + (len(self.entries) * 4)
        
        for entry in self.entries:
            if entry != None:
                size += entry.size()
        
        return size
    
    def write(self, f):
    
        Chunk.write(self, f)
        
        self.write_word(f, self.id)
        self.write_word(f, len(self.entries))
        
        entries_start = self.header_size + (len(self.entries) * 4)
        self.write_word(f, entries_start)
        
        self.config.write(f)
        
        offset = 0
        for entry in self.entries:
            if entry != None:
                self.write_word(f, offset)
                offset += entry.size()
            else:
                self.write_word(f, NO_ENTRY)
        
        for entry in self.entries:
            if entry != None:
                entry.write(f)

class Config:

    def __init__(self, mcc = 0, mnc = 0, language = None,
        country = None, orientation = 0, touchscreen = 0, density = 0,
        keyboard = 0, navigation = 0, input_flags = 0, input_pad0 = 0,
        screen_width = 0, screen_height = 0, sdk_version = 0,
        minor_version = 0, screen_layout = 0, ui_mode = 0,
        screen_config_pad1 = 0, screen_config_pad2 = 0):
    
        self.mcc = mcc
        self.mnc = mnc
        self.language = language or "\x00\x00"
        self.country = country or "\x00\x00"
        self.orientation = orientation
        self.touchscreen = touchscreen
        self.density = density
        self.keyboard = keyboard
        self.navigation = navigation
        self.input_flags = input_flags
        self.input_pad0 = input_pad0
        self.screen_width = screen_width
        self.screen_height = screen_height
        self.sdk_version = sdk_version
        self.minor_version = minor_version
        self.screen_layout = screen_layout
        self.ui_mode = ui_mode
        self.screen_config_pad1 = screen_config_pad1
        self.screen_config_pad2 = screen_config_pad2
    
    def read(self, f):
    
        start = f.tell()
        config_size = unpack("<I", f.read(4))[0]
        
        # IMSI
        self.mcc, self.mnc = unpack("<HH", f.read(4))
        
        # Locale
        self.language = f.read(2)
        self.country = f.read(2)
        
        # Screen type
        self.orientation, self.touchscreen, self.density = \
            unpack("<BBH", f.read(4))
        
        # Input
        self.keyboard, self.navigation, self.input_flags, self.input_pad0 = \
            unpack("<BBBB", f.read(4))
        
        # Screen size
        self.screen_width, self.screen_height = unpack("<HH", f.read(4))
        
        # Version
        self.sdk_version, self.minor_version = unpack("<HH", f.read(4))
        
        # Screen configuration
        self.screen_layout, self.ui_mode, self.screen_config_pad1, \
            self.screen_config_pad2 = unpack("<BBBB", f.read(4))
        
        f.seek(start + config_size)
        return self
    
    def toxml(self, indent, f=sys.stdout):
    
        i = indent * u" "
        print >>f, i + u'<Config mcc="%i" mnc="%i" language="%s" country="%s"' % (
            self.mcc, self.mnc, self.language, self.country),
        print >>f, u'orientation="%i" touchscreen="%i" density="%i"' % (
            self.orientation, self.touchscreen, self.density),
        print >>f, u'keyboard="%i" navigation="%i" inputFlags="%i" inputPad0="%i"' % (
            self.keyboard, self.navigation, self.input_flags, self.input_pad0),
        print >>f, u'screenWidth="%i" screenHeight="%i"' % (
            self.screen_width, self.screen_height),
        print >>f, u'sdkVersion="%i" minorVersion="%i"' % (
            self.sdk_version, self.minor_version),
        print >>f, u'screenLayout="%i" uiMode="%i" screenConfigPad1="%i"' % (
            self.screen_layout, self.ui_mode, self.screen_config_pad1),
        print >>f, u'screenConfigPad2="%i" />' % self.screen_config_pad2
    
    def size(self):
    
        return 36
    
    def write(self, f):
    
        f.write(pack("<I", self.size()))
        f.write(pack("<HH", self.mcc, self.mnc))
        f.write(self.language)
        f.write(self.country)
        
        f.write(pack("<BBH", self.orientation, self.touchscreen, self.density))
        f.write(pack("<BBBB", self.keyboard, self.navigation, self.input_flags,
            self.input_pad0))
        f.write(pack("<HH", self.screen_width, self.screen_height))
        f.write(pack("<HH", self.sdk_version, self.minor_version))
        f.write(pack("<BBBB", self.screen_layout, self.ui_mode,
            self.screen_config_pad1, self.screen_config_pad2))
        f.write(pack("<I", 0))

class Resource:

    # https://justanapplication.wordpress.com/2011/09/20/android-internals-resources-part-nine-simple-resource-entry-examples/
    # https://justanapplication.wordpress.com/2011/09/22/android-internals-resources-part-ten-complex-resource-entry-examples/
    
    def __init__(self, key = 0, flags = 0, parent = 0, values = None):
    
        self.key = key
        self.flags = flags
        self.parent = parent
        self.names = []
        
        if values == None: values = []
        self.values = values
    
    def read(self, f):
    
        resource_size, self.flags = unpack("<HH", f.read(4))
        
        self.key = unpack("<I", f.read(4))[0]
        self.values = []
        
        if self.flags & FLAG_COMPLEX != 0:
        
            self.parent = unpack("<I", f.read(4))[0]
            count = unpack("<I", f.read(4))[0]
            
            while count > 0:
                name, size, data_type, data = unpack("<IHxBI", f.read(12))
                self.names.append(name)
                self.values.append((data_type, data))
                count -= 1
        else:
            size, res0, data_type = unpack("<HBB", f.read(4))
            self.values.append((data_type, unpack("<I", f.read(4))[0]))
        
        return self
    
    def toxml(self, indent, f=sys.stdout):
    
        print >>f, (indent * u" ") + \
            u'<Resource key="%i" flags="%i" parent="%i">' % (self.key,
                self.flags, self.parent)
        
        for data_type, value in self.values:
            print >>f, ((indent + 2) * u" ") + \
                u'<Value dataType="%i" data="%i" />' % (data_type, value)
        
        print >>f, (indent * u" ") + u'</Resource>'
    
    def size(self):
    
        size = 8
        
        if self.flags & FLAG_COMPLEX != 0:
            return size + 8 + (len(self.values) * 12)
        else:
            return size + (len(self.values) * 8)
    
    def write(self, f):
    
        if self.flags & FLAG_COMPLEX != 0:
            # Write a different resource size (16) to that used for simple types (8).
            f.write(pack("<HHI", 16, self.flags, self.key))
            f.write(pack("<I", self.parent))
            f.write(pack("<I", len(self.values)))
            
            # Each item has a reference to a name. The references start at
            # 0x02000000. Android's toolchain doesn't appear to encode names
            # for items.
            for i, (data_type, value) in enumerate(self.values):
                f.write(pack("<IHBBI", 0x02000000 + i, 8, 0, data_type, value))
        else:
            f.write(pack("<HHI", 8, self.flags, self.key))
            for data_type, value in self.values:
                f.write(pack("<HBBI", 8, 0, data_type, value))

class TypeSpec(Chunk):

    type = 0x202
    
    def __init__(self, id = 0, entries = None):
    
        self.header_size = 16
        self.id = id
        
        if entries == None: entries = []
        self.entries = entries
    
    def read(self, f, header_size):
    
        Chunk.read(self, f, header_size)
        
        self.id = self.read_word(f) & 0xff
        
        entry_count = self.read_word(f)
        
        self.entries = []
        i = 0
        while i < entry_count:
            self.entries.append(self.read_word(f))
            i += 1
        
        return self
    
    def toxml(self, indent, f=sys.stdout):
    
        print >>f, (indent * u" ") + u'<TypeSpec id="%i">' % self.id
        
        for entry in self.entries:
            print >>f, ((indent + 2) * u" ") + u'<Entry flags="%i" />' % entry
        
        print >>f, (indent * u" ") + u'</TypeSpec>'
    
    def size(self):
    
        return self.header_size + (len(self.entries) * 4)
    
    def write(self, f):
    
        Chunk.write(self, f)
        
        f.write(pack("<BBH", self.id, 0, 0))
        self.write_word(f, len(self.entries))
        
        for entry in self.entries:
            self.write_word(f, entry)


BinaryXML.chunk_classes = {
    0x001: StringPool,
    0x002: Table,
    0x003: XML,
    0x100: XMLStartNamespace,
    0x101: XMLEndNamespace,
    0x102: XMLStartElement,
    0x103: XMLEndElement,
    0x104: XMLCDATA,
    0x180: XMLResourceMap,
    0x200: Package,
    0x201: Type,
    0x202: TypeSpec,
    }


class File(BinaryXML):

    def __init__(self, file_name = None, root = None):
    
        if file_name:
            self.root = self.read(open(file_name))
        else:
            self.root = root
    
    def read(self, f):
    
        return self.read_chunk(f)
    
    def save(self, file_name):
    
        f = open(file_name, "wb")
        self.root.write(f)
        f.close()
