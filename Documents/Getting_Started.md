Getting Started
===============

This document covers the steps needed to install DUCK and start using the
compiler for the Serpentine language. If you are reading this document online,
you will need to download the respository containing it to your workstation.
This is covered in the Installation section below.

Prerequisites
-------------

Since DUCK is a set of modules and tools written in the Python language, you
will need to install a Python interpreter. Specifically, you will need to
install the latest release of Python 2.7 or Python 3. You will also need to
install the corresponding [PyCrypto](https://www.pycrypto.org/) package for the
version of Python you have installed and the [Python Imaging
Library](http://effbot.org/zone/pil-index.htm) (or the Pillow fork of PIL).

If you have not yet downloaded the DUCK repository, you will need to install
[Git](https://git-scm.com/) since that is what we will use to
obtain the repository.

You need to install [ImageMagick](https://www.imagemagick.org/) and librsvg2 -
these are used to generate icons for applications from SVG files. If
ImageMagick is provided as a set of packages for your system you will also need
the extras package that includes the SVG plugin.

The documentation generator relies on [Markdown](https://daringfireball.net/projects/markdown/)
and [Pygments](http://pygments.org).

You will also need to install [OpenSSL](https://www.openssl.org/) because the
`openssl` tool is used to create and manage keys and certificates used for
signing packages.

Additionally, if you want to be able to install packages via a USB cable to an
attached device, you need to install the `adb` tool.

If you are using Debian GNU/Linux you can install these dependencies with one
the following commands depending on which version of Python you are using.

For Python 2:

```\
sudo apt-get install git python python-crypto imagemagick \
                     libmagickcore-extra openssl android-tools-adb \
                     python-markdown python-pygments
```

For Python 3:

```\
sudo apt-get install git python3 python3-crypto imagemagick \
                     libmagickcore-extra openssl android-tools-adb \
                     python3-markdown python3-pygments
```

Other GNU/Linux distributions and Unix-like systems should have packages that
correspond to these.

Installation
------------

If you haven't already done so, you need to download the repository containing
this document. First, open a console and change into a directory where the
repository will be placed, then use Git to clone the repository using the
following command if you are using Python 2:

```\
git clone https://gitlab.com/dboddie/DUCK
```

Use this command if you are using Python 3:
```\
git clone -b python3 https://gitlab.com/dboddie/DUCK
```

There should now be a directory called `DUCK` in the current directory.

Currently, there is no way of installing the modules and tools to a system-wide
location. To allow Python scripts to access the modules, and to run the
compiler for Serpentine, you need to add the directory to the `PYTHONPATH`
environment variable. On GNU/Linux and other Unix-like systems, you can do this
by typing the following in the console (running a bash shell):

```\
export PYTHONPATH=$PWD/DUCK:$PYTHONPATH
```

To test the installation, run Python from the console in the following way:

```\
python -c 'import Dalvik'
```

This should run without error messages.

Creating a Key and Certificate
------------------------------

The following information is given in the [Creating Keys and Certificates](Keys_and_Certificates.md)
document. You can create a key to use for signing packages and a corresponding
certificate using OpenSSL with the following commands:

```\
openssl genpkey -algorithm RSA -out key.pem -pkeyopt rsa_keygen_bits:2048 -pkeyopt rsa_keygen_pubexp:3
openssl req -new -x509 -days 365 -sha1 -key key.pem -out cert.pem
```

The `key.pem` and `cert.pem` files are used to sign packages. The Android
documentation contains the [Implementing Security](https://source.android.com/security/overview/implement)
document which gives advice about handling signing keys.

Building Packages
-----------------

The DUCK repository contains some simple application examples and some
regression tests. These are described in the [Examples README.html](../Examples/Serpentine/README.md)
and [Tests README.html](../Tests/Serpentine/README.md) files respectively.

To quickly check that package building works, type the following command in the
console, remembering to set the `PYTHONPATH` as described above:

```\
DUCK/Examples/Serpentine/Hello/build.py key.pem cert.pem Hello.apk
```

This should run without errors. If the `Hello.apk` package is created in the
current directory, you can install it with the following command:

```\
adb install Hello.apk
```

If successful, some information about the installation process and the word
`Success` should be printed to the console. Otherwise, you may find it useful
to inspect the debug log for the device by running the following command:

```\
adb logcat
```

You will need to terminate this process with Ctrl-C when you have finished
examining the debugging output.

Writing Applications
--------------------

Applications are written in the Serpentine language, which has a syntax based
on that of the Python language. Although source code written in this language
is compiled, there isn't a front-end for the compiler. Instead, it is invoked
using a build script written in Python. The examples in the
[Examples/Serpentine](../Examples/Serpentine/README.md) directory are stored in
self-contained directories, each with their own `build.py` script.

The [Introduction to Serpentine](Introduction_to_Serpentine.md) document gives
an overview of the language, providing code examples, and discusses aspects of
it where its semantics differ from those of the Python language.
