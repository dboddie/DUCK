Creating Keys and Certificates
==============================

You can create a key to use for signing packages and a corresponding
certificate using OpenSSL with the following commands:

```\
openssl genpkey -algorithm RSA -out key.pem -pkeyopt rsa_keygen_bits:2048 -pkeyopt rsa_keygen_pubexp:3
openssl req -new -x509 -days 365 -sha1 -key key.pem -out cert.pem
```

Alternatively, you can create both the key and certificate at the same time
without any interactive prompts by using the following command, substituting
the appropriate value for country code, state, locality, organisation and
common (domain) name:

```\
openssl req -new -newkey rsa:2048 -days 365 -nodes -x509 \
            -subj "/C=XX/ST=State/L=Locality/O=Org/CN=example.com" \
            -keyout key.pem -out cert.pem
```

Note that the certificate is created with a SHA1 digest. This must be the same
as the digest specified for signing. If omitted, the default digests used to
create the certificate and sign a package may differ and this will lead to adb
reporting a INSTALL_PARSE_FAILED_NO_CERTIFICATES error.

Using Build Scripts to Sign Packages
------------------------------------

The build scripts in the Examples and Tests directories accept arguments that
specify the key and certificate files. Passing these to a build script will
cause signing to occur automatically using the a command similar to that shown
in the following section.

Signing Packages Manually
-------------------------

If you have an unsigned package in a directory called `temp`, the following
command should use the key to sign the manifest and identify the signer using
the certificate:

```\
openssl smime -sign -noverify -noattr -binary -inkey key.pem -signer cert.pem \
    -md sha1 -in temp/META-INF/CERT.SF -outform der -out temp/META-INF/CERT.RSA
```

The digest must match the one used when creating the certificate - see above.

Verifying a Signed Package
--------------------------

If you have an signed package in a directory called `temp`, the following
command should extract the X.509 certificate from the package's `CERT.RSA` file
and verify that it was issued using the certificate in the file `cert.pem`:

```\
openssl pkcs7 -in temp/META-INF/CERT.RSA -inform DER -print_certs | \
    openssl verify -verbose -CAfile cert.pem
```

If you do not have the original signing certificate but instead have a SHA256
hash of it then you can obtain the SHA256 hash of the certificate in the
package for verification:

```\
openssl pkcs7 -in temp/META-INF/CERT.RSA -inform DER -print_certs | \
    openssl x509 -fingerprint -sha256 -noout
```
