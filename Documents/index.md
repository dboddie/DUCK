Documentation Index
===================

DUCK
----

 * [DUCK - Dalvik Unpythonic Compiler Kit](../README.md) - the main `README`
   file for this package
 * [Getting Started](Getting_Started.md)
 * [Creating Keys and Certificates](Keys_and_Certificates.md)

Serpentine
----------

 * [Introduction to Serpentine](Introduction_to_Serpentine.md)
 * [A Serpentine Compiler](../DUCK/Serpentine/README.md) - notes about
   the compiler for the Serpentine language
 * [Examples](../Examples/Serpentine/README.md) - how to build the examples
 * [Demonstrations](../Demos/Serpentine/README.md) - how to build the
   demonstration applications
 * [Tests](../Tests/Serpentine/README.md) - how to build and run the tests
