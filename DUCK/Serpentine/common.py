"""
Copyright (C) 2017 David Boddie <david@boddie.org.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

"""~
# Common Resources

This module provides fundamental types and common resources that are required
throughout the compiler. Unlike the methods defined in the [arrays](arrays.md),
[methods](methods.md) and [builtins](builtins.md) modules, many useful routines
that do not require access to the compiler object are defined as functions
instead of methods.
"""

import ast
from types import NoneType

from Dalvik.dex import AccessFlags
from Dalvik.dextypes import *

import visitor

class NameLookupError(Exception):
    pass

# Functions for finding objects by name from different starting points.

def find_object(name, scope, context, dex_objects, for_assignment = False):

    """Finds and returns the object with the given name that is accessible from
    the specified scope."""
    
    if name in scope.class_.replacement_types:
        return scope.class_.replacement_types[name]
    
    elif context.find_local(name, for_assignment = for_assignment):
        # The object is a variable that refers to an object.
        return context[name]
    
    # Look for the object at the module scope.
    obj = scope.parent.module.find_object(name)
    
    if obj is None:
        # Look for a built-in object.
        obj = dex_objects.builtins.find_object(name)
    
    return obj


def find_attribute(container_obj, name, node, scope):

    """Finds the object in the container_obj that is referred to by the name
    attribute. The associated node is used to provide line number information
    in the event of an error."""
    
    try:
        return container_obj.find_object(name, in_superclass = True,
                                               in_parent = False)
    
    except AttributeError:
        raise TypeError("Unsupported use of getattr with type '%s' at line %i "
            "of file %s." % (repr(container_obj.type_), node.lineno, scope.file_name()))


# Convenience functions

def class_from_container(obj):

    if is_class(obj):
        return obj
    else:
        return obj.type_

def is_class(type_):

    return isinstance(type_, visitor.ClassVisitor) or \
           isinstance(type_, TemplateClass)

def is_placeholder(type_):

    return isinstance(type_, Placeholder)

def is_container(obj):

    """Returns True if the object is either a template class or a normal class
    with predefined item types."""
    
    if isinstance(obj, TemplateClass):
        return True
    elif isinstance(obj, visitor.ClassVisitor):
        return obj.has_item_types() or obj.has_parameters()
    else:
        return False

def is_array(type_):

    return isinstance(type_, ArrayContainer)

def is_primitive(type_):

    return isinstance(type_, TypeDescriptor)

def has_primitive(type_):

    if is_array(type_):
        return has_primitive(type_.item_type)
    else:
        return is_primitive(type_)

def is_parameter(node):

    return isinstance(node, visitor.Parameter)

def has_parameter(node):

    if type(node) == list:  # array
        return has_parameter(node[0])
    else:
        return is_parameter(node)

def is_specialised(class_):

    return isinstance(class_, visitor.ClassVisitor) and class_.replace_

def is_temporary_variable(obj):

    """Checks whether an object is a temporary variable."""
    return isinstance(obj, Variable) and obj.temporary()

def is_sequence(obj):

    """Checks whether an object is a sequence."""
    return isinstance(obj, ast.Tuple) or isinstance(obj, ast.List) 


# Functions for creating type information

def create_dex_type(type_):

    if isinstance(type_, Variable):
        type_ = type_.type_
    
    if isinstance(type_, TypeDescriptor):
        return type_
    elif isinstance(type_, ArrayContainer):
        return type_.dex_type
    else:
        return create_class_name(type_)

def create_class_name(class_):

    if isinstance(class_, TemplateClass):
        return create_class_name(class_.container_class)
    else:
        return Name(u"L%s;" % class_.full_name())

def convert_field(visitor_field):

    type_ = create_dex_type(visitor_field.type_)
    
    # Associate the field name with this class type.
    class_name = create_dex_type(visitor_field.class_)
    return Field(class_name, type_, MemberName(visitor_field.name))

def create_access_flags(constructor, external, reference_only, decorators,
                        interface, static):

    if "protected" in decorators:
        access = ACC_PROTECTED
    else:
        access = ACC_PUBLIC
    
    if constructor:
        access = access | ACC_CONSTRUCTOR
    
    if interface:
        access = access | ACC_ABSTRACT
        
    if static:
        access = access | ACC_STATIC
        
    if external:
        if reference_only:
            access = None
        else:
            access = AccessFlags(access | ACC_ABSTRACT)
    else:
        access = AccessFlags(access)
    
    return access

def get_common_type(type1, type2):

    """Returns the most specialised common type of the two given types. This
    finds the closest ancestor class for two classes, or the most capable
    (largest) type for two non-class types."""
    
    ### This doesn't look at interfaces.
    
    if is_class(type1):
    
        if is_class(type2):
        
            # Obtain ancestors for each type.
            ancestors1 = type1.ancestors()
            ancestors2 = type2.ancestors()
            
            # Find the closest common ancestor.
            found = None
            
            while ancestors1 and ancestors2:
            
                a1 = ancestors1.pop()
                a2 = ancestors2.pop()
                
                if a1 == a2:
                    found = a1
                else:
                    break
            
            return found
    
    elif not is_class(type2):
    
        # Neither type is a class. Find the common type we can cast to.
        return get_type_to_cast_to(type1, type2)
    
    # There was no common type. Note that this needs to be updated if we decide
    # to support casting between primitive types and instances such as Number
    # subclasses, for example.
    return None


def update_item_types(callee, container_obj, item_types):

    """Update the item types of the container from the dictionary of item types."""
    
    if not item_types:
        return
    
    class_ = class_from_container(container_obj)
    
    # We can only update the item types of specialised classes, not the
    # original templates.
    if not isinstance(class_, TemplateClass):
    
        # Classes that both declare parameters and define item types do not
        # need to be updated. User-defined containers do this.
        if class_.has_item_types():
            return
        
        # Fill in empty values for the item types and use the template class
        # from now on.
        class_ = TemplateClass(class_, [None] * len(class_.parameters()))
        callee.container_obj = class_
    
    # If the parameter types were not defined, fill them in from the
    # argument types used.
    
    # Ensure that the class contains the required number of item types,
    # creating undefined items if necessary.
    required = len(item_types) > len(class_.item_types_)
    if required > 0:
        class_.item_types_ += [None] * required
    
    for par_name, item_type in item_types.items():
        par_index = class_.container_class.parameters().index(par_name)
        
        # Only fill in undefined item types. Others should have been
        # checked for compatibility earlier.
        if class_.item_types_[par_index] == None:
            class_.item_types_[par_index] = item_type


# Define an additional type to represent a null reference.

class Null:

    def size(self):
        return 1

dNull = Null()


type_name_to_type_map = {
    "void":  dVoid,    "bool":  dBoolean, "boolean": dBoolean,
    "byte":  dByte,    "short": dShort,   "char":    dChar,
    "int":   dInt,     "long":  dLong,    "float":   dFloat,
    "double": dDouble
    }

value_name_to_type_map = {
    "False": dBoolean, "True": dBoolean, "None": dNull
    }

value_name_to_value_map = {
    "False": False, "True": True, "None": None
    }

value_to_type_map = {
    bool: dBoolean, int: dInt, float: dDouble, long: dLong, NoneType: dNull
    }

type_class_to_type_name_map = {
    Boolean: "bool", Byte: "byte", Char: "char", Double: "double",
    Float: "float", Int:  "int",  Long: "long", Short: "short", Void: "void"
    }


def value_to_type(value):

    # Get an initial type.
    type_ = value_to_type_map[type(value)]
    
    # If the value was held in a Python long object then try to fit it into
    # an int object instead. This aims to prevent situations on 32-bit Python
    # where the parser presents a 32-bit value as a positive long number when
    # it was supposed to represent a signed 32-bit integer.
    if type_ == dLong and 0 <= value < (1 << 32):
        type_ = dInt
        if value >= (1 << 31):
            value = int(value - (1 << 32))
    
    return type_, value


# Representations of complex and specialised types in the compiler

class MethodCall:

    def __init__(self, container_obj, name, arguments = None):
    
        self.container_obj = container_obj
        self.name = name
        
        if arguments is None:
            arguments = []
        
        self.arguments = arguments
        self.updated_types = []
    
    def __repr__(self):
    
        return "MethodCall(%s, %s, %s)" % (
            repr(self.container_obj), repr(self.name), repr(self.arguments))
    
    def __str__(self):
    
        return "%s.%s(%s)" % (self.container_obj, self.name,
            ", ".join(map(str, self.arguments)))


class TemplateClass:

    def __init__(self, container_class, item_types):
    
        self.container_class = container_class
        self.item_types_ = item_types
    
    def __repr__(self):
    
        return "TemplateClass(%s, %s)" % (repr(self.container_class),
            repr(self.item_types_))
    
    def __str__(self):
    
        return "%s<%s>" % (self.container_class.name(),
            ",".join(map(str, self.item_types_)))
    
    def __cmp__(self, other):
    
        if not isinstance(other, TemplateClass):
            return 1
        
        c = cmp(self.container_class, other.container_class)
        if c != 0:
            return c
        
        for a, b in zip(self.item_types_, other.item_types_):
            c = cmp(a, b)
            if c != 0:
                return c
        
        return 0
    
    def full_name(self):
    
        item_names = []
        for t in self.item_types_:
            if isinstance(t, TypeDescriptor):
                item_names.append(str(t))
            else:
                item_names.append(t.full_name())
        
        return self.container_class.full_name() + "<" + ",".join(item_names) + ">"
    
    def unique_name(self):
    
        return "%s<%s>" % (self.container_class.unique_name(),
            ",".join(map(str, self.item_types_)))
    
    def size(self):
        return 1
    
    def is_interface(self):
        return self.container_class.is_interface()
    
    def find_object(self, name, in_superclass = False, in_parent = True):
        return self.container_class.find_object(name, in_superclass, in_parent)
    
    def has_parameters(self):
        return True
    
    def parameters(self):
        return self.container_class.parameters()
    
    def has_item_types(self):
        return True
    
    def item_types(self):
        return self.item_types_


class ArrayContainer:

    def __init__(self, dimensions, item_type, dex_type, wrapped = False):
    
        self.dimensions = dimensions
        self.item_type = item_type
        self.dex_type = dex_type
        self.wrapped_items = wrapped
    
    def __repr__(self):
    
        return "ArrayContainer(%i, %s, %s, %s)" % (self.dimensions,
            repr(self.item_type), repr(self.dex_type), self.wrapped_items)
    
    def __str__(self):
    
        return "%s%s" % (self.item_type, "[]" * self.dimensions)
    
    def __cmp__(self, other):
    
        if not isinstance(other, ArrayContainer):
            return 1
        
        if self.dimensions != other.dimensions or \
            self.item_type != other.item_type or \
            self.dex_type != other.dex_type or \
            self.wrapped_items != other.wrapped_items:
            return 1
        else:
            return 0
        
    def unique_name(self):
    
        return str(self)
    
    def size(self):
        return 1


class Placeholder:

    def __init__(self, class_):
        self.class_ = class_
    
    def __repr__(self):
        return "Placeholder(%s)" % repr(self.class_)
    
    def name(self):
        return self.class_.name()
    
    def full_name(self):
        return self.class_.full_name()
    
    def unique_name(self):
        return self.class_.unique_name()
    
    def size(self):
        return self.class_.size()
    
    def is_interface(self):
        return self.class_.is_interface()
    
    def find_object(self, name, in_superclass = False, in_parent = True):
        return self.class_.find_object(name, in_superclass, in_parent)


# Define abstractions to cover register usage, manage language level
# information such as scopes, and store generated code.

class Variable:

    def __init__(self, name, n, type_, src = None):
    
        self.name = name
        self.n = n
        self.set_type(type_)
        self.src = src
    
    def __repr__(self):
        return "Variable(%s, %i, %s, %i)" % (repr(self.name), self.n, repr(self.type_), self.size())
    
    def __str__(self):
        return "%s (type %s)" % (self.name, self.type_)
    
    def temporary(self):
        return self.name == None
    
    def is_alias(self, other):
        return self.n == other.n
    
    def size(self):
        return self.size_
    
    def set_type(self, type_):
        self.type_ = type_
        self.size_ = type_.size()
    
    def find_object(self, name, in_superclass = False, in_parent = True):
    
        # Search for the name in the type associated with the variable.
        
        # First look for the object in the instance only.
        name_obj = self.type_.find_object(name,
            in_superclass = False, in_parent = False)
        
        if name_obj is None:
            # Otherwise, look for the object in superclasses and interfaces.
            name_obj = self.type_.find_object(name, in_superclass = True,
                                                    in_parent = False)
        
        # If the resolved object is a collection of methods then fill in the
        # container object.
        if isinstance(name_obj, visitor.Methods):
            name_obj.container_obj = self
        
        return name_obj


class Context:

    """Allocates placeholder registers for method arguments and variables that
    are resolved to actual registers after the method's code has been created."""
    
    def __init__(self, arguments, var_base, keep_source = False):
    
        self.arguments = arguments
        
        # Create a dictionary to allow the arguments to be referred to.
        self.vars = {}
        
        for arg in arguments:
            self.vars[arg.name] = arg
        
        # Variables are allocated on a stack with numbers referring to the
        # number of words from the base of the stack.
        self.allocated = []
        self.free_number = var_base
        self.bookmarks = []
        
        # Variables are also maintained in scopes within the larger context
        # of the method.
        self.scopes = []
        # Local variables assigned within control flow structures are recorded
        # in this stack of lists. Each list contains a dictionary recording the
        # local variables assigned to within each branch of the structure.
        self.created_vars = [[{}]]
        
        # The maximum number of registers used for outgoing calls is updated
        # by code that generates pseudo-instructions for calls.
        self.max_outgoing = 0
        
        self.tries = []
        self.instructions = []
        self.keep_source = keep_source
    
    def __getitem__(self, key):
    
        # Look for the variable in the innermost scope before trying outer ones.
        if key in self.vars:
            return self.vars[key]
        
        i = len(self.scopes) - 1
        while i >= 0:
            if key in self.scopes[i]:
                return self.scopes[i][key]
            i -= 1
        
        raise KeyError
    
    def __setitem__(self, key, value):
    
        # Define an entry in the innermost scope.
        self.vars[key] = value
    
    def find_local(self, name, for_assignment = False):
    
        # Check whether the name is immediately visible.
        if name in self.vars:
        
            if name in self.created_vars[-1][0]:
                if for_assignment:
                    # The variable was defined in the first branch (suite) of
                    # the current control flow structure. This means we can use
                    # the reference in the latest branch.
                    return True
                else:
                    # For references to locals, check that it was defined in
                    # the latest branch before allowing it to be used.
                    return name in self.created_vars[-1][-1]
            else:
                # The variable was defined in the current scope but was not
                # created in a conditional suite.
                return True
        
        # Look for variables in enclosing scopes.
        i = len(self.scopes) - 1
        
        while i >= 0:
        
            # Check whether the variable is defined in the scope.
            if name in self.scopes[i]:
            
                if len(self.created_vars[i]) == 1:
                    # If there is only one suite in the corresponding scope
                    # then there is no need to check the validity of the
                    # variable found.
                    return True
                else:
                    # The variable was defined in the latest of a set of scopes
                    # for an enclosing structure.
                    return name in self.created_vars[i][-1]
            i -= 1
        
        return False
    
    def allocate(self, name = None, type_ = None):
    
        if name != None and name in self.vars:
            raise NameLookupError("Variable '%s' already allocated." % name)
        
        n = self.free_number
        self.free_number += type_.size()
        
        var = Variable(name, n, type_)
        self.vars[name] = var
        self.allocated.append(var)
        
        # When creating a named variable, record it so that it can be tracked.
        if name != None:
            self.current_suite()[name] = var
        
        return var
    
    def alias(self, variable, new_type):
    
        # Return a new variable with the same position as the one specified but
        # with a different type. This is usually used for temporary variables.
        var = Variable(variable.name, variable.n, new_type)
        self.allocated[self.allocated.index(variable)] = var
        return var
    
    def free(self, warn = True):
        """Free the last allocated variable."""
        
        # Decrease the free number by the sizes of the variables in the frame.
        if self.allocated == []:
            raise ValueError("Allocation stack empty.")
        
        # Pop the last variable, handling it appropriately if it is named.
        variable = self.allocated.pop()
        
        if not variable.temporary():
            if warn:
                raise ValueError("Internal error: tried to free '%s'" % variable.name)
            
            # Remove the named variable from the local dictionary.
            del self.vars[variable.name]
        
        # The number of the first variable is the new free number.
        self.free_number = variable.n
    
    def swap(self):
    
        # Swap the top two entries on the allocation stack.
        top = self.allocated.pop()
        next = self.allocated.pop()
        top.n = next.n
        next.n = top.n + top.size()
        self.allocated.append(top)
        self.allocated.append(next)
    
    def allocate_group(self):
        self.bookmarks.append(len(self.allocated))
    
    def free_group(self):
    
        index = self.bookmarks.pop()
        while len(self.allocated) > index:
            self.free(warn = False)
    
    def free_including(self, variable):
    
        while self.allocated:
            top = self.allocated[-1]
            self.free(warn = False)
            if top == variable:
                break
    
    def append(self, instruction):
    
        # Keep all instructions if making a listing. Only keep normal
        # instructions if not.
        
        if self.keep_source:
            self.instructions.append(instruction)
        
        elif not isinstance(instruction, Source):
            self.instructions.append(instruction)
    
    def insert(self, index, instruction, block = False):
    
        if block:
            self.instructions = self.instructions[:index] + instruction + \
                                self.instructions[index:]
        else:
            self.instructions.insert(index, instruction)
    
    def enter_scope(self):
    
        self.scopes.append(self.vars)
        self.created_vars.append([{}])
        self.vars = {}
    
    def leave_scope(self):
    
        self.vars = self.scopes.pop()
        self.created_vars.pop()
    
    def add_suite(self):
    
        self.created_vars[-1].append({})
    
    def drop_suite(self):
    
        self.created_vars[-1].pop()
    
    def current_suite(self):
    
        return self.created_vars[-1][-1]
    
    def all_suites(self):
    
        return self.created_vars[-1]
    
    def defined_in_scope(self):
    
        d = {}
        
        for name, var in self.vars.items():
            if name != None:
                d[name] = var
        
        return d
    
    def used_vars(self, start, finish = None):
    
        if finish == None:
            finish = len(self.instructions)
        
        used = {}
        
        for ins in self.instructions[start:finish]:
            used.update(ins.used_vars())
        
        return used


class Source:

    """Describes a reference to a node in the abstract syntax tree from within
    the instructions generated by the compiler."""
    
    size = 0
    
    def __init__(self, node):
        self.node = node
    
    def expand(self, dex_objects):
        return []
    
    def used_vars(self):
        return []
