__package__ = "serpentine.types"

from java.lang import Boolean, Byte, Double, Float, Integer, Long, Object, \
                      Short, String, Void

class Types(Object):

    @static
    @args(int, [byte])
    def cast_to_int(value):
        v = Byte(value).intValue()
        if v < 0: v += 256
        return v
    
    @static
    @args(int, [short])
    def cast_to_int(value):
        return Short(value).intValue()
    
    @static
    @args(int, [String])
    def cast_to_int(value):
        return Integer.parseInt(value)
