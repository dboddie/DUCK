__package__ = "serpentine.arrays"

from java.lang import Boolean, Byte, Double, Float, Integer, Long, Object, \
                      Short, Void
import java.util
from serpentine.exceptions import ValueError

class Arrays(Object):

    __replace__ = [P, Q]
    
    @static
    @args([Q], [P(Q)])
    def create(container):
    
        length = len(container)
        a = array(Q, length)
        
        i = 0
        while i < length:
            a[i] = container[i]
            i += 1
        
        return a
    
    @static
    @args([Boolean], [[bool]])
    def wrap(a):
    
        length = len(a)
        b = array(Boolean, length)
        
        i = 0
        while i < length:
            b[i] = Boolean(a[i])
            i += 1
        
        return b
    
    @static
    @args([bool], [[Boolean]])
    def unwrap(a):
    
        length = len(a)
        b = array(bool, length)
        
        i = 0
        while i < length:
            b[i] = a[i].boolValue()
            i += 1
        
        return b
    
    @static
    @args([Byte], [[byte]])
    def wrap(a):
    
        length = len(a)
        b = array(Byte, length)
        
        i = 0
        while i < length:
            b[i] = Byte(a[i])
            i += 1
        
        return b
    
    @static
    @args([byte], [[Byte]])
    def unwrap(a):
    
        length = len(a)
        b = array(byte, length)
        
        i = 0
        while i < length:
            b[i] = a[i].byteValue()
            i += 1
        
        return b
    
    @static
    @args([Double], [[double]])
    def wrap(a):
    
        length = len(a)
        b = array(Double, length)
        
        i = 0
        while i < length:
            b[i] = Double(a[i])
            i += 1
        
        return b
    
    @static
    @args([double], [[Double]])
    def unwrap(a):
    
        length = len(a)
        b = array(double, length)
        
        i = 0
        while i < length:
            b[i] = a[i].doubleValue()
            i += 1
        
        return b
    
    @static
    @args([Float], [[float]])
    def wrap(a):
    
        length = len(a)
        b = array(Float, length)
        
        i = 0
        while i < length:
            b[i] = Float(a[i])
            i += 1
        
        return b
    
    @static
    @args([float], [[Float]])
    def unwrap(a):
    
        length = len(a)
        b = array(float, length)
        
        i = 0
        while i < length:
            b[i] = a[i].floatValue()
            i += 1
        
        return b
    
    @static
    @args([Integer], [[int]])
    def wrap(a):
    
        length = len(a)
        b = array(Integer, length)
        
        i = 0
        while i < length:
            b[i] = Integer(a[i])
            i += 1
        
        return b
    
    @static
    @args([int], [[Integer]])
    def unwrap(a):
    
        length = len(a)
        b = array(int, length)
        
        i = 0
        while i < length:
            b[i] = a[i].intValue()
            i += 1
        
        return b
    
    @static
    @args([Long], [[long]])
    def wrap(a):
    
        length = len(a)
        b = array(Long, length)
        
        i = 0
        while i < length:
            b[i] = Long(a[i])
            i += 1
        
        return b
    
    @static
    @args([long], [[Long]])
    def unwrap(a):
    
        length = len(a)
        b = array(long, length)
        
        i = 0
        while i < length:
            b[i] = a[i].longValue()
            i += 1
        
        return b
    
    @static
    @args([Short], [[short]])
    def wrap(a):
    
        length = len(a)
        b = array(Short, length)
        
        i = 0
        while i < length:
            b[i] = Short(a[i])
            i += 1
        
        return b
    
    @static
    @args([short], [[Short]])
    def unwrap(a):
    
        length = len(a)
        b = array(short, length)
        
        i = 0
        while i < length:
            b[i] = a[i].shortValue()
            i += 1
        
        return b
    
    @static
    @args(int, [[Q], Q])
    def indexOf(a, v):
    
        i = 0
        while i < len(a):
            if v == a[i]:
                return i
            i += 1
        
        return -1
    
    @static
    @args(bool, [[Q], Q])
    def contains(a, v):
    
        for i in a:
            if v == i:
                return True
        
        return False
    
    @static
    @args([Q], [[Q], int, int])
    def slice(a, begin, end):
    
        return java.util.Arrays.copyOfRange(a, begin, end)
