__package__ = "serpentine.collections"

from java.lang import Math, IndexOutOfBoundsException
from java.util import Arrays, HashMap, List, Object, String
from serpentine.exceptions import IndexError, TypeError, ValueError

class Collections(Object):

    __replace__ = [P, Q, R, S]
    
    @static
    @args(String, [String, int])
    def subscript_get(s, index):
    
        if index < 0:
            l = len(s)
            if index < -l:
                raise IndexError()
            index = l + index
        
        try:
            sub = s.substring(index, index + 1)
        except IndexOutOfBoundsException:
            raise IndexError()
        
        return sub
    
    ### TODO: Handle slices with negative indices and steps.
    ### TODO: Need to cast the result of copyOfRange to the appropriate type
    ### when arrays of Objects are used.
    
    @static
    @args([Q], [[Q], int, int])
    def slice(a, begin, end):
    
        return Arrays.copyOfRange(a, begin, end)
    
    @static
    @args(List(Q), [List(Q), int, int])
    def slice(l, begin, end):
    
        return l.subList(begin, end)
    
    @static
    @args(String, [String, int, int])
    def slice(s, begin, end):
    
        # Ensure that the end index doesn't exceed the length of the string
        # because the String.substring method does not allow that.
        if end > len(s):
            end = len(s)
        
        try:
            sub = s.substring(begin, end)
        except IndexOutOfBoundsException:
            raise IndexError()
        
        return sub
    
    ### TODO: Consider using a Range class with its own iterator.
    
    @static
    @args(List(Q), [Q, Q, Q])
    def range(begin, end, step):
    
        # Check for a zero step, raising an exception if one is used.
        if step == 0:
            raise ValueError()
        
        l = []
        
        length = end - begin
        
        if length == 0:
            return l
        elif length > 0 and step < 0:
            return l
        elif length < 0 and step > 0:
            return l
        
        if step > 0:
            i = begin
            while i < end:
                l.add(i)
                i += step
        else:
            i = begin
            while i > end:
                l.add(i)
                i += step
        
        return l
    
    @static
    @args(List(Q), [Q, Q])
    def range(begin, end):
    
        return Collections.range(begin, end, Q(1))
    
    @static
    @args(List(Q), [Q])
    def range(end):
    
        return Collections.range(Q(0), end, Q(1))
    
    @static
    @args(List(List(Q)), [[Q], [Q]])
    def zip(sequence1, sequence2):
    
        length = Math.min(len(sequence1), len(sequence2))
        l = []
        
        i = 0
        while i < length:
            l.add([sequence1[i], sequence2[i]])
            i += 1
        
        return l
    
    @static
    @args(List(List(Q)), [P(Q), P(Q)])
    def zip(sequence1, sequence2):
    
        length = Math.min(len(sequence1), len(sequence2))
        l = []
        
        i = 0
        while i < length:
            l.add([sequence1[i], sequence2[i]])
            i += 1
        
        return l
    
    @static
    @args(List(List(Q)), [List(P(Q))])
    def zip(list_of_sequences):
    
        l = []
        
        if len(list_of_sequences) == 0:
            return l
        
        length = len(list_of_sequences[0])
        
        i = 1
        while i < len(list_of_sequences):
            length = Math.min(len(list_of_sequences[i]), length)
            i += 1
        
        i = 0
        while i < length:
            m = []
            for sequence in list_of_sequences:
                m.add(sequence[i])
            l.add(m)
            i += 1
        
        return l
    
    @static
    @args(List(Pair(Q, S)), [P(Q), R(S)])
    def zip(sequence1, sequence2):
    
        length = Math.min(len(sequence1), len(sequence2))
        l = []
        
        i = 0
        while i < length:
            l.add(Pair(sequence1[i], sequence2[i]))
            i += 1
        
        return l
    
    @static
    @args(List(Pair(Q, S)), [[Q], [S]])
    def zip(array1, array2):
    
        length = Math.min(len(array1), len(array2))
        l = []
        
        i = 0
        while i < length:
            l.add(Pair(array1[i], array2[i]))
            i += 1
        
        return l
    
    @static
    @args(HashMap(Q, Q), [P(Q), P(Q)])
    def dict(keys, values):
    
        d = {}
        for key, value in zip(keys, values):
            d[key] = value
        
        return d
    
    @static
    @args(HashMap(Q, S), [P(Q), R(S)])
    def dict(keys, values):
    
        d = {}
        for pair in zip(keys, values):
            d[pair.first()] = pair.second()
        
        return d
    
    @static
    @args(HashMap(Q, Q), [[Q], [Q]])
    def dict(keys, values):
    
        d = {}
        for key, value in zip(keys, values):
            d[key] = value
        
        return d
    
    @static
    @args(HashMap(Q, S), [[Q], [S]])
    def dict(keys, values):
    
        d = {}
        for pair in zip(keys, values):
            d[pair.first()] = pair.second()
        
        return d
    
    @static
    @args(HashMap(Q, Q), [P(List(Q))])
    def dict(lists):
    
        d = {}
        for l in lists:
            if len(l) < 2:
                raise TypeError()
            d[l[0]] = l[1]
        
        return d
    
    @static
    @args(HashMap(Q, S), [P(Pair(Q, S))])
    def dict(pairs):
    
        d = {}
        for pair in pairs:
            d[pair.first()] = pair.second()
        
        return d


class Pair(Object):

    __replace__ = [Q, S]
    
    @args(void, [Q, S])
    def __init__(self, value1, value2):
    
        Object.__init__(self)
        self.value1 = value1
        self.value2 = value2
    
    @args(Q, [])
    def first(self):
        return self.value1
    
    @args(S, [])
    def second(self):
        return self.value2
