__package__ = "serpentine.strings"

from java.lang import CharSequence
from java.util import List, Object, String

class Strings(Object):

    __replace__ = [Q]
    
    @static
    @args(String, [Object])
    def repr(value):
    
        if value == None:
            return "None"
        
        return value.toString()
    
    @static
    @args(String, [Object])
    def str(value):
    
        if value == None:
            return ""
        
        return value.toString()
    
    @static
    @args(String, [CharSequence])
    def str(value):
    
        return value.toString()
    
    @static
    @args(String, [Q])
    def str(value):
    
        return String.valueOf(value)
    
    @static
    @args(String, [String, [Q]])
    def join(delimiter, items):
    
        s = ""
        i = 0
        l = len(items)
        last = l - 1
        while i < l:
            s += str(items[i])
            if i < last: s += delimiter
            i += 1
        
        return s
    
    @static
    @args(String, [String, List(Q)])
    def join(delimiter, items):
    
        s = ""
        i = 0
        l = len(items)
        last = l - 1
        while i < l:
            s += str(items[i])
            if i < last: s += delimiter
            i += 1
        
        return s
