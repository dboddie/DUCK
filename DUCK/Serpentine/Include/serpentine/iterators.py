__package__ = "serpentine.iterators"

from java.lang import Object
from java.util import Iterator

class Iterators(Object):

    __replace__ = [P, Q]
    
    @static
    @args(Iterator(Q), [P(Q)])
    def iter(collection):
        return collection.iterator()
