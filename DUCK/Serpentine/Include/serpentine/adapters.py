"""
Adapters
========

This module provides adapters that expose various kinds of objects so that an
attached ListView can present them as scrolling lists of labels."""

__package__ = "serpentine.adapters"

from java.io import File
from java.lang import String, Object
from java.util import List
from android.content import Context
from android.view import View, ViewGroup
from android.widget import Adapter, ArrayAdapter, BaseAdapter, Filterable, \
    TextView

"""The FileListAdapter class provides an adapter to expose the contents of a
directory, supplied as a File object. It is implemented in a similar way to the
StringListAdapter class below."""

class FileListAdapter(BaseAdapter):

    __fields__ = {"items": List(File),
                  "suffixes": List(String)}
    
    @args(void, [File, List(String)])
    def __init__(self, directory, suffixes):
    
        BaseAdapter.__init__(self)
        
        self.directory = directory
        self.suffixes = suffixes
        
        self.items = []
        for suffix in self.suffixes:
            self.addFiles(suffix)
    
    @args(void, [File, String])
    def __init__(self, directory, suffix):
    
        BaseAdapter.__init__(self)
        
        self.directory = directory
        self.suffixes = [suffix]
        
        self.items = []
        self.addFiles(suffix)
    
    @args(void, [String])
    def addFiles(self, suffix):
    
        files = self.directory.listFiles()
        
        i = 0
        while i < len(files):
        
            f = files[i]
            if f.isFile() and f.getName().endsWith(suffix):
                self.items.add(f)
            
            i += 1
    
    @args(int, [])
    def getCount(self):
        return len(self.items)
    
    @args(Object, [int])
    def getItem(self, position):
        return self.items[position]
    
    @args(long, [int])
    def getItemId(self, position):
        return long(0)
    
    @args(View, [int, View, ViewGroup])
    def getView(self, position, convertView, parent):
    
        fileName = self.items[position].getName()
        
        view = TextView(parent.getContext())
        view.setText(fileName)
        return view


"""The StringArrayAdapter class is derived from the standard BaseAdapter class,
implementing the necessary methods in order to return information about the
array of Strings that it provides an interface for."""

class StringArrayAdapter(BaseAdapter):

    """We expose the items attribute as a field for the benefit of subclasses."""
    
    __fields__ = {"items": [String]}
    
    """The class is initialised with an array of Strings. These are stored in
    an instance attribute. The corresponding method in the base class also
    needs to be called."""
    
    @args(void, [[String]])
    def __init__(self, strings):
        BaseAdapter.__init__(self)
        self.items = strings
    
    """The getCount and getItem methods are simple wrappers around the String
    array, returning the length of the array and providing access to items
    respectively."""
    
    @args(int, [])
    def getCount(self):
        return len(self.items)
    
    @args(Object, [int])
    def getItem(self, position):
        return self.items[position]
    
    """The isEmpty method simply tests for an array with no items and returns
    the result."""
    
    @args(bool, [])
    def isEmpty(self):
        return self.getCount() == 0
    
    """The getItemId method simply returns the position of an item for the
    item's ID since we do not intend to allow items to be repositioned in the
    array."""
    
    @args(long, [int])
    def getItemId(self, position):
        return position
    
    """Although we could indicate the type of view that the adapter will
    supply, we instead returns a constant indicating that views will not be
    converted between different types. In any case, the getViewTypeCount method
    returns 1 to indicate that we only return one type of view."""
    
    @args(int, [int])
    def getItemViewType(self, position):
        return Adapter.IGNORE_ITEM_VIEW_TYPE
    
    @args(int, [])
    def getViewTypeCount(self):
        return 1
    
    """The getView method returns a view for the given position and parent.
    This avoids having to create a new view every time an item needs to be
    displayed."""
    
    @args(View, [int, View, ViewGroup])
    def getView(self, position, convertView, parent):
    
        view = TextView(parent.getContext())
        view.setText(self.items[position])
        return view
    
    """Since we will not support modification of the array contents, we return
    True to reflect the stable nature of the order of items."""
    
    @args(bool, [])
    def hasStableIds(self):
        return True


"""The FilterStringArrayAdapter class is a specialised adapter for use with the
AutoCompleteTextView, implementing the necessary methods in order to return information about the
array of Strings that it provides an interface for."""

class FilterStringArrayAdapter(ArrayAdapter):

    __parameters__ = [T]
    __item_types__ = [String]
    
    @args(void, [Context, int, [T]])
    def __init__(self, context, resourceId, objects):
    
        ArrayAdapter.__init__(self, context, resourceId, objects)


"""The StringListAdapter class is derived from the standard BaseAdapter class,
implementing the necessary methods in order to return information about the
list of Strings that it provides an interface for."""

class StringListAdapter(BaseAdapter):

    """We expose the items attribute as a field for the benefit of subclasses."""
    
    __fields__ = {"items": List(String)}
    
    """The class is initialised with a list of Strings. These are stored in
    an instance attribute. The corresponding method in the base class also
    needs to be called."""
    
    @args(void, [List(String)])
    def __init__(self, strings):
    
        BaseAdapter.__init__(self)
        self.items = strings
    
    """The getCount and getItem methods are simple wrappers around the String
    list, returning the length of the list and providing access to items
    respectively."""
    
    @args(int, [])
    def getCount(self):
        return len(self.items)
    
    @args(Object, [int])
    def getItem(self, position):
        return self.items[position]
    
    """The isEmpty method simply tests for an list with no items and returns
    the result."""
    
    @args(bool, [])
    def isEmpty(self):
        return self.getCount() == 0
    
    """The getItemId method simply returns the position of an item for the
    item's ID since we do not intend to allow items to be repositioned in the
    list."""
    
    @args(long, [int])
    def getItemId(self, position):
        return position
    
    """Although we could indicate the type of view that the adapter will
    supply, we instead return a constant indicating that views will not be
    converted between different types."""
    
    @args(int, [int])
    def getItemViewType(self, position):
        return Adapter.IGNORE_ITEM_VIEW_TYPE
    
    """The getView method returns a view for the given position and parent.
    This avoids having to create a new view every time an item needs to be
    displayed."""
    
    @args(View, [int, View, ViewGroup])
    def getView(self, position, convertView, parent):
    
        view = TextView(parent.getContext())
        view.setText(self.items[position])
        return view
    
    """Since we will not support modification of the list contents, we return
    True to reflect the stable nature of the order of items."""
    
    @args(bool, [])
    def hasStableIds(self):
        return True
    
    """Since the adapter is used to return a view that is used in a drop-down
    menu, we implement this method in addition to the getView method, enabling
    us to return a view that is more appropriate for that use. If this method
    is not implemented, views returned by the getView method will be used
    instead."""
    
    @args(View, [int, View, ViewGroup])
    def getDropDownView(self, position, convertView, parent):
    
        view = TextView(parent.getContext())
        view.setText(self.items[position])
        return view


"""The ViewAdapter class extends the BaseAdapter class to provide an adapter
that exposes View instances to ListViews and other View collections. The
implementation of this view is very similar to those above."""

class ViewAdapter(BaseAdapter):

    __fields__ = {"items": List(View)}
    
    @args(void, [List(View)])
    def __init__(self, items):
    
        BaseAdapter.__init__(self)
        self.items = items
    
    @args(int, [])
    def getCount(self):
        return len(self.items)
    
    @args(Object, [int])
    def getItem(self, position):
        return self.items[position]
    
    @args(long, [int])
    def getItemId(self, position):
        return long(position)
    
    @args(View, [int, View, ViewGroup])
    def getView(self, position, convertView, parent):
        return self.items[position]
