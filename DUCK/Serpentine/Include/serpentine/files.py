__package__ = "serpentine.files"

from java.io import File, FileNotFoundException, RandomAccessFile
from java.lang import IllegalArgumentException, String, Object
from java.text import SimpleDateFormat
from java.util import Date
from android.os import Environment

class Files(Object):

    @static
    @args(RandomAccessFile, [String, String])
    def open(path, mode):
    
        try:
            f = RandomAccessFile(path, mode)
        except IllegalArgumentException:
            raise ValueError("mode string must be one of 'r', 'rw', 'rws', 'rwd', not " + str(mode))
        except FileNotFoundException:
            raise IOError("Cannot open file '" + str(path) + "' in mode '" + str(mode) + "'")
        
        return f
    
    @static
    @args(RandomAccessFile, [File, String])
    def open(file, mode):
    
        return Files.open(file.getAbsolutePath(), mode)
    
    @static
    @args(File, [String, String, String, String, String])
    def createExternalFile(directory, subdirName, prefix, dateFormat, suffix):
    
        # If no external storage is mounted then return None immediately.
        if Environment.getExternalStorageState() != Environment.MEDIA_MOUNTED:
            return None
        
        # Obtain the directory on the external storage device.
        storageDir = Environment.getExternalStoragePublicDirectory(directory)
        
        if subdirName != None:
            # If required, create a subdirectory, returning None to indicate
            # failure.
            subdir = File(storageDir, subdirName)
            if not subdir.exists():
                if not subdir.mkdirs():
                    return None
        else:
            # Place new files directly in the storage directory.
            subdir = storageDir
        
        # Create a file name based on the prefix, current date and suffix then
        # use this to create a File object to return.
        fileName = ""
        
        if prefix != None:
            fileName += prefix
        
        if dateFormat != None:
            fileName += SimpleDateFormat(dateFormat).format(Date())
        
        if suffix != None:
            fileName += suffix
        
        # Return instead of creating a file with an empty name.
        if fileName == "":
            return None
        
        return File(subdir, fileName)
