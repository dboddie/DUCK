"""
Widgets
=======

This module provides a collection of widgets that are slightly more convenient
to use than the ones provided by the android.widget library."""

__package__ = "serpentine.widgets"

from android.view import View, ViewGroup
from android.widget import LinearLayout

class HBox(LinearLayout):

    def __init__(self, context):
    
        LinearLayout.__init__(self, context)
        self.setOrientation(self.HORIZONTAL)
    
    @args(void, [View, float])
    def addWeightedView(self, view, weight):
    
        view.setLayoutParams(LinearLayout.LayoutParams(
            ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.MATCH_PARENT, weight))
        LinearLayout.addView(self, view)


class VBox(LinearLayout):

    def __init__(self, context):
    
        LinearLayout.__init__(self, context)
        self.setOrientation(self.VERTICAL)
    
    @args(void, [View, float])
    def addWeightedView(self, view, weight):
    
        view.setLayoutParams(LinearLayout.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT, weight))
        LinearLayout.addView(self, view)
