# Class declarations obtained from Android API documentation licensed under the
# Apache 2.0 license (http://www.apache.org/licenses/LICENSE-2.0) and rewritten
# in syntax that the Python compiler module can parse.

__package__ = "org.xml.sax"

from java.lang import String

class Attributes:

    @args(int, [String, String])
    def getIndex(self, uri, localName):
        pass
    
    @args(int, [String])
    def getIndex(self, qName):
        pass
    
    @args(int, [])
    def getLength(self):
        pass
    
    @args(String, [int])
    def getLocalName(self, index):
        pass
    
    @args(String, [int])
    def getQName(self, index):
        pass
    
    @args(String, [String])
    def getType(self, qName):
        pass
    
    @args(String, [String, String])
    def getType(self, uri, localName):
        pass
    
    @args(String, [int])
    def getType(self, index):
        pass
    
    @args(String, [int])
    def getURI(self, index):
        pass
    
    @args(String, [String])
    def getValue(self, qName):
        pass
    
    @args(String, [String, String])
    def getValue(self, uri, localName):
        pass
    
    @args(String, [int])
    def getValue(self, index):
        pass


class ContentHandler:

    @args(void, [[char], int, int])
    def characters(self, ch, start, length):
        pass
    
    def endDocument(self):
        pass
    
    @args(void, [String, String, String])
    def endElement(self, uri, localName, qName):
        pass
    
    @args(void, [String])
    def endPrefixMapping(self, prefix):
        pass
    
    @args(void, [[char], int, int])
    def ignorableWhitespace(self, ch, start, length):
        pass
    
    @args(void, [String, String])
    def processingInstruction(self, target, data):
        pass
    
    @args(void, [Locator])
    def setDocumentLocator(self, locator):
        pass
    
    @args(void, [String])
    def skippedEntity(self, name):
        pass
    
    @args(void, [])
    def startDocument(self):
        pass
    
    @args(void, [String, String, String, Attributes])
    def startElement(self, uri, localName, qName, atts):
        pass
    
    @args(void, [String, String])
    def startPrefixMapping(self, prefix, uri):
        pass


class Locator:

    @args(int, [])
    def getColumnNumber(self):
        pass
    
    @args(int, [])
    def getLineNumber(self):
        pass
    
    @args(String, [])
    def getPublicId(self):
        pass
    
    @args(String, [])
    def getSystemId(self):
        pass
