# Class declarations obtained from Android API documentation licensed under the
# Apache 2.0 license (http://www.apache.org/licenses/LICENSE-2.0) and rewritten
# in syntax that the Python compiler module can parse.

__package__ = "org.xmlpull.v1"

from java.io import InputStream, OutputStream, Reader
from java.lang import Boolean, Class, Exception, Object, String, Throwable
from java.util import ArrayList, HashMap

class XmlPullParser:

    CDSECT = 5
    COMMENT = 9
    DOCDECL = 10
    END_DOCUMENT = 1
    END_TAG = 3
    ENTITY_REF = 6
    FEATURE_PROCESS_DOCDECL =  "http://xmlpull.org/v1/doc/features.html#process-docdecl"
    FEATURE_PROCESS_NAMESPACES = "http://xmlpull.org/v1/doc/features.html#process-namespaces"
    FEATURE_REPORT_NAMESPACE_ATTRIBUTES = "http://xmlpull.org/v1/doc/features.html#report-namespace-prefixes"
    FEATURE_VALIDATION = "http://xmlpull.org/v1/doc/features.html#validation"
    IGNORABLE_WHITESPACE = 7
    NO_NAMESPACE = ""
    PROCESSING_INSTRUCTION = 8
    START_DOCUMENT = 0
    START_TAG = 2
    TEXT = 4
    
    __static_fields__ = {"TYPES": [String]}
    
    @args(void, [String, String])
    def defineEntityReplacementText(self, entityName, replacementText):
        pass
    
    @args(int, [])
    def getAttributeCount(self):
        pass
    
    @args(String, [int])
    def getAttributeName(self, index):
        pass
    
    @args(String, [int])
    def getAttributeNamespace(self, index):
        pass
    
    @args(String, [int])
    def getAttributePrefix(self, index):
        pass
    
    @args(String, [int])
    def getAttributeType(self, index):
        pass
    
    @args(String, [int])
    def getAttributeValue(self, index):
        pass
    
    @args(String, [String, String])
    def getAttributeValue(self, namespace, name):
        pass
    
    @args(int, [])
    def getColumnNumber(self):
        pass
    
    @args(int, [])
    def getDepth(self):
        pass
    
    @args(int, [])
    def getEventType(self):
        pass
    
    @args(bool, [String])
    def getFeature(self, name):
        pass
    
    @args(String, [])
    def getInputEncoding(self):
        pass
    
    @args(int, [])
    def getLineNumber(self):
        pass
    
    @args(String, [])
    def getName(self):
        pass
    
    @args(String, [])
    def getNamespace(self):
        pass
    
    @args(String, [String])
    def getNamespace(self, prefix):
        pass
    
    @args(int, [int])
    def getNamespaceCount(self, depth):
        pass
    
    @args(String, [int])
    def getNamespacePrefix(self, pos):
        pass
    
    @args(String, [int])
    def getNamespaceUri(self, pos):
        pass
    
    @args(String, [])
    def getPositionDescription(self):
        pass
    
    @args(String, [])
    def getPrefix(self):
        pass
    
    @args(Object, [String])
    def getProperty(self, name):
        pass
    
    @args(String, [])
    def getText(self):
        pass
    
    @args([char], [[int]])
    def getTextCharacters(self, holderForStartAndLength):
        pass
    
    @args(bool, [int])
    def isAttributeDefault(self, index):
        pass
    
    @args(bool, [])
    def isEmptyElementTag(self):
        pass
    
    @args(bool, [])
    def isWhitespace(self):
        pass
    
    @args(int, [])
    def next(self):
        pass
    
    @args(int, [])
    def nextTag(self):
        pass
    
    @args(String, [])
    def nextText(self):
        pass
    
    @args(int, [])
    def nextToken(self):
        pass
    
    @args(void, [int, String, String])
    def require(self, type, namespace, name):
        pass
    
    @args(void, [String, bool])
    def setFeature(self, name, state):
        pass
    
    @args(void, [Reader])
    def setInput(self, input):
        pass
    
    @args(void, [InputStream, String])
    def setInput(self, inputStream, inputEncoding):
        pass
    
    @args(void, [String, Object])
    def setProperty(self, name, value):
        pass


class XmlPullParserException(Exception):

    __fields__ = {
        "column": int,
        "detail": Throwable,
        "row": int
        }
    
    @args(void, [String])
    def __init__(self, s):
        pass
    
    @args(void, [String, XmlPullParser, Throwable])
    def __init__(self, msg, parser, chain):
        pass
    
    @args(int, [])
    def getColumnNumber(self):
        pass
    
    @args(Throwable, [])
    def getDetail(self):
        pass
    
    @args(int, [])
    def getLineNumber(self):
        pass
    
    def printStackTrace(self):
        pass


class XmlPullParserFactory(Object):

    PROPERTY_NAME = "org.xmlpull.v1.XmlPullParserFactory"
    
    __fields__ = {
        "classNamesLocation": String,
        "features": HashMap(String, Boolean),
        "parserClasses": ArrayList,
        "serializerClasses": ArrayList
        }
    
    @protected
    def __init__(self):
        pass
    
    @args(boolean, [String])
    def getFeature(self, name):
        pass
    
    @args(boolean, [])
    def isNamespaceAware(self):
        pass
    
    @args(boolean, [])
    def isValidating(self):
        pass
    
    @static
    @args(XmlPullParserFactory, [])
    def newInstance():
        pass
    
    @static
    @args(XmlPullParserFactory, [String, Class])
    def newInstance(unused, unused2):
        pass
    
    @args(XmlPullParser, [])
    def newPullParser(self):
        pass
    
    @args(XmlSerializer, [])
    def newSerializer(self):
        pass
    
    @args(void, [String, boolean])
    def setFeature(self, name, state):
        pass
    
    @args(void, [boolean])
    def setNamespaceAware(self, awareness):
        pass
    
    @args(void, [boolean])
    def setValidating(self, validating):
        pass


class XmlSerializer:

    @args(XmlSerializer, [String, String, String])
    def attribute(self, namespace, name, value):
        pass
    
    @args(void, [String])
    def cdsect(self, text):
        pass
    
    @args(void, [String])
    def comment(self, text):
        pass
    
    @args(void, [String])
    def docdecl(self, text):
        pass
    
    def endDocument(self):
        pass
    
    @args(XmlSerializer, [String, String])
    def endTag(self, namespace, name):
        pass
    
    @args(void, [String])
    def entityRef(self, text):
        pass
    
    def flush(self):
        pass
    
    @args(int, [])
    def getDepth(self):
        pass
    
    @args(bool, [String])
    def getFeature(self, name):
        pass
    
    @args(String, [])
    def getName(self):
        pass
    
    @args(String, [])
    def getNamespace(self):
        pass
    
    @args(String, [String, bool])
    def getPrefix(self, namespace, generatePrefix):
        pass
    
    @args(Object, [String])
    def getProperty(self, name):
        pass
    
    @args(void, [String])
    def ignorableWhitespace(self, text):
        pass
    
    @args(void, [String])
    def processingInstruction(self, text):
        pass
    
    @args(void, [String, bool])
    def setFeature(self, name, state):
        pass
    
    @args(void, [Writer])
    def setOutput(self, writer):
        pass
    
    @args(void, [OutputStream, String])
    def setOutput(self, os, encoding):
        pass
    
    @args(void, [String, String])
    def setPrefix(self, prefix, namespace):
        pass
    
    @args(void, [String, Object])
    def setProperty(self, name, value):
        pass
    
    @args(void, [String, bool])
    def startDocument(self, encoding, standalone):
        pass
    
    @args(XmlSerializer, [String, String])
    def startTag(self, namespace, name):
        pass
    
    @args(XmlSerializer, [String])
    def text(self, text):
        pass
    
    @args(XmlSerializer, [[char], int, int])
    def text(self, buf, start, len):
        pass
