# Class declarations obtained from Android API documentation licensed under the
# Apache 2.0 license (http://www.apache.org/licenses/LICENSE-2.0) and rewritten
# in syntax that the Python compiler module can parse.

__package__ = "java.net"

from java.io import Closeable, IOException, InputStream, OutputStream, \
                    Serializable
from java.lang import Object, String
from java.security import Permission
from java.util import Enumeration, List, Map

class BindException(SocketException):

    def __init__(self):
        pass
    
    @args(void, [String])
    def __init__(self, detailMessage):
        pass


class ContentHandler(Object):

    def __init__(self):
        pass
    
    ### ...
    
    @args(Object, [URLConnection])
    def getContent(self, urlConnection):
        pass


class ContentHandlerFactory:

    @args(ContentHandler, [String])
    def createContentHandler(self, contentType):
        pass


class FileNameMap:

    @args(String, [String])
    def getContentTypeFor(self, filename):
        pass


class HttpURLConnection(URLConnection):

    HTTP_ACCEPTED           = 202
    HTTP_BAD_GATEWAY        = 502
    HTTP_BAD_METHOD         = 405
    HTTP_BAD_REQUEST        = 400
    HTTP_CLIENT_TIMEOUT     = 408
    HTTP_CONFLICT           = 409
    HTTP_CREATED            = 201
    HTTP_ENTITY_TOO_LARGE   = 413
    HTTP_FORBIDDEN          = 403
    HTTP_GATEWAY_TIMEOUT    = 504
    HTTP_GONE               = 410
    HTTP_INTERNAL_ERROR     = 500
    HTTP_LENGTH_REQUIRED    = 411
    HTTP_MOVED_PERM         = 301
    HTTP_MOVED_TEMP         = 302
    HTTP_MULT_CHOICE        = 300
    HTTP_NOT_ACCEPTABLE     = 406
    HTTP_NOT_AUTHORITATIVE  = 203
    HTTP_NOT_FOUND          = 404
    HTTP_NOT_IMPLEMENTED    = 501
    HTTP_NOT_MODIFIED       = 304
    HTTP_NO_CONTENT         = 204
    HTTP_OK                 = 200
    HTTP_PARTIAL            = 206
    HTTP_PAYMENT_REQUIRED   = 402
    HTTP_PRECON_FAILED      = 412
    HTTP_PROXY_AUTH         = 407
    HTTP_REQ_TOO_LONG       = 414
    HTTP_RESET              = 205
    HTTP_SEE_OTHER          = 303
    HTTP_SERVER_ERROR       = 500
    HTTP_UNAUTHORIZED       = 401
    HTTP_UNAVAILABLE        = 503
    HTTP_UNSUPPORTED_TYPE   = 415
    HTTP_USE_PROXY          = 305
    HTTP_VERSION            = 505
    
    __fields__ = {
        "chunkLength": int,
        "fixedContentLength": int,
        "fixedContentLengthLong": long,
        "instanceFollowRedirects": bool,
        "method": String,
        "responseCode": int,
        "responseMessage": String
        }
    
    @args(void, [URL])
    def __init__(self, url):
        pass
    
    @abstract
    def disconnect(self):
        pass
    
    @args(String, [])
    def getContentEncoding(self):
        pass
    
    @args(InputStream, [])
    def getErrorStream(self):
        pass
    
    @static
    @args(bool, [])
    def getFollowRedirects():
        pass
    
    @args(long, [String, long])
    def getHeaderFieldDate(self, field, defaultValue):
        pass
    
    @args(bool, [])
    def getInstanceFollowRedirects(self):
        pass
    
    @args(Permission, [])
    def getPermission(self):
        pass
    
    @args(String, [])
    def getRequestMethod(self):
        pass
    
    @args(int, [])
    def getResponseCode(self):
        pass
    
    @args(String, [])
    def getResponseMessage(self):
        pass
    
    @args(void, [int])
    def setChunkedStreamingMode(self, chunkLength):
        pass
    
    @args(void, [int])
    def setFixedLengthStreamingMode(self, contentLength):
        pass
    
    @args(void, [long])
    def setFixedLengthStreamingMode(self, contentLength):
        pass
    
    @static
    @args(void, [bool])
    def setFollowRedirects(auto):
        pass
    
    @args(void, [bool])
    def setInstanceFollowRedirects(self, followRedirects):
        pass
    
    @args(void, [String])
    def setRequestMethod(self, method):
        pass
    
    @abstract
    @args(bool, [])
    def usingProxy(self):
        pass


class InetAddress(Object):

    __interfaces__ = [Serializable]
    
    @args([byte], [])
    def getAddress(self):
        pass
    
    @static
    @args([InetAddress], [String])
    def getAllByName(host):
        pass
    
    @static
    @args(InetAddress, [String, [byte]])
    def getByAddress(host, ipAddress):
        pass
    
    @static
    @args(InetAddress, [[byte]])
    def getByAddress(ipAddress):
        pass
    
    @static
    @args(InetAddress, [String])
    def getByName(host):
        pass
    
    @args(String, [])
    def getCanonicalHostName(self):
        pass
    
    @args(String, [])
    def getHostAddress(self):
        pass
    
    @args(String, [])
    def getHostName(self):
        pass
    
    @static
    @args(InetAddress, [])
    def getLocalHost():
        pass
    
    @static
    @args(InetAddress, [])
    def getLoopbackAddress():
        pass
    
    @args(bool, [])
    def isAnyLocalAddress(self):
        pass
    
    @args(bool, [])
    def isLinkLocalAddress(self):
        pass
    
    @args(bool, [])
    def isLoopbackAddress(self):
        pass
    
    @args(bool, [])
    def isMCGlobal(self):
        pass
    
    @args(bool, [])
    def isMCLinkLocal(self):
        pass
    
    @args(bool, [])
    def isMCNodeLocal(self):
        pass
    
    @args(bool, [])
    def isMCOrgLocal(self):
        pass
    
    @args(bool, [])
    def isMCSiteLocal(self):
        pass
    
    @args(bool, [])
    def isMulticastAddress(self):
        pass
    
    @args(bool, [NetworkInterface, int, int])
    def isReachable(self, networkInterface, timeToLive, timeout):
        pass
    
    @args(bool, [int])
    def isReachable(self, timeout):
        pass
    
    @args(bool, [])
    def isSiteLocalAddress(self):
        pass


class InterfaceAddress(Object):

    @args(InetAddress, [])
    def getAddress(self):
        pass
    
    @args(InetAddress, [])
    def getBroadcast(self):
        pass
    
    @args(short, [])
    def getNetworkPrefixLength(self):
        pass


class NetworkInterface(Object):

    @static
    @args(NetworkInterface, [int])
    def getByIndex(index):
        pass
    
    @static
    @args(NetworkInterface, [InetAddress])
    def getByInetAddress(address):
        pass
    
    @static
    @args(NetworkInterface, [String])
    def getByName(interfaceName):
        pass
    
    @args(String, [])
    def getDisplayName(self):
        pass
    
    @args([byte], [])
    def getHardwareAddress(self):
        pass
    
    @args(int, [])
    def getIndex(self):
        pass
    
    @args(Enumeration(InetAddress), [])
    def getInetAddresses(self):
        pass
    
    @args(List(InterfaceAddress), [])
    def getInterfaceAddresses(self):
        pass
    
    @args(int, [])
    def getMTU(self):
        pass
    
    @args(String, [])
    def getName(self):
        pass
    
    @static
    @args(Enumeration(NetworkInterface), [])
    def getNetworkInterfaces():
        pass
    
    @args(NetworkInterface, [])
    def getParent(self):
        pass
    
    @args(Enumeration(NetworkInterface), [])
    def getSubInterfaces(self):
        pass
    
    @args(bool, [])
    def isLoopback(self):
        pass
    
    @args(bool, [])
    def isPointToPoint(self):
        pass
    
    @args(bool, [])
    def isUp(self):
        pass
    
    @args(bool, [])
    def isVirtual(self):
        pass
    
    @args(bool, [])
    def supportsMulticast(self):
        pass


class Proxy(Object):

    __static_fields__ = {"NO_PROXY": Proxy}
    
    class Type(Enum):
    
        __static_fields__ = {
            "DIRECT": Proxy.Type,
            "HTTP": Proxy.Type,
            "SOCKS": Proxy.Type
            }
        
        @static
        @args(Proxy.Type, [String])
        def valueOf(name): pass
        
        @final
        @static
        @args([Proxy.Type], [])
        def values(): pass
    
    @args(void, [Proxy.Type, SocketAddress])
    def __init__(self, type, address):
        pass
    
    @args(SocketAddress, [])
    def address(self):
        pass
    
    @args(Proxy.Type, [])
    def type(self):
        pass


class ServerSocket(Object):

    __interfaces__ = [Closeable]
    
    def __init__(self):
        pass
    
    @args(void, [int])
    def __init__(self, port):
        pass
    
    @args(void, [int, int])
    def __init__(self, port, backlog):
        pass
    
    @args(void, [int, int, InetAddress])
    def __init__(self, port, backlog, localAddress):
        pass
    
    @args(Socket, [])
    def accept(self):
        pass
    
    @args(void, [SocketAddress])
    def bind(self, localAddr):
        pass
    
    @args(void, [SocketAddress, int])
    def bind(self, localAddr, backlog):
        pass
    
    def close(self):
        pass
    
    @args(ServerSocketChannel, [])
    def getChannel(self):
        pass
    
    @args(InetAddress, [])
    def getInetAddress(self):
        pass
    
    @args(int, [])
    def getLocalPort(self):
        pass
    
    @args(SocketAddress, [])
    def getLocalSocketAddress(self):
        pass
    
    @args(int, [])
    def getReceiveBufferSize(self):
        pass
    
    @args(bool, [])
    def getReuseAddress(self):
        pass
    
    @synchronized
    @args(int, [])
    def getSoTimeout(self):
        pass
    
    @args(bool, [])
    def isBound(self):
        pass
    
    @args(bool, [])
    def isClosed(self):
        pass
    
    @args(void, [int, int, int])
    def setPerformancePreferences(self, connectionTime, latency, bandwidth):
        pass
    
    @args(void, [int])
    def setReceiveBufferSize(self, size):
        pass
    
    @args(void, [bool])
    def setReuseAddress(self, reuse):
        pass
    
    @synchronized
    @args(void, [int])
    def setSoTimeout(self, timeout):
        pass
    
    @synchronized
    @static
    @args(void, [SocketImplFactory])
    def setSocketFactory(self, aFactory):
        pass
    
    @args(String, [])
    def toString(self):
        pass
    
    @protected
    @final
    @args(void, [Socket])
    def implAccept(self, aSocket):
        pass


class Socket(Object):

    __interfaces__ = [Closeable]
    
    def __init__(self):
        pass
    
    @args(void, [Proxy])
    def __init__(self, proxy):
        pass
    
    @args(void, [String, int])
    def __init__(self, remoteName, remotePort):
        pass
    
    @args(void, [String, int, InetAddress, int])
    def __init__(self, remoteName, remotePort, localAddress, localPort):
        pass
    
    @args(void, [InetAddress, int])
    def __init__(self, remoteAddress, remotePort):
        pass
    
    @args(void, [InetAddress, int, InetAddress, int])
    def __init__(self, remoteAddress, remotePort, localAddress, localPort):
        pass
    
    @args(void, [SocketAddress])
    def bind(self, localAddress):
        pass
    
    @args(void, [SocketAddress, int])
    def connect(self, remoteAddress, timeout):
        pass
    
    @args(void, [SocketAddress])
    def connect(self, remoteAddress):
        pass
    
    @args(SocketChannel, [])
    def getChannel(self):
        pass
    
    @args(InetAddress, [])
    def getInetAddress(self):
        pass
    
    @args(InputStream, [])
    def getInputStream(self):
        pass
    
    @args(bool, [])
    def getKeepAlive(self):
        pass
    
    @args(InetAddress, [])
    def getLocalAddress(self):
        pass
    
    @args(int, [])
    def getLocalPort(self):
        pass
    
    @args(SocketAddress, [])
    def getLocalSocketAddress(self):
        pass
    
    @args(bool, [])
    def getOOBInline(self):
        pass
    
    @args(OutputStream, [])
    def getOutputStream(self):
        pass
    
    @args(int, [])
    def getPort(self):
        pass
    
    @synchronized
    @args(int, [])
    def getReceiveBufferSize(self):
        pass
    
    @args(SocketAddress, [])
    def getRemoteSocketAddress(self):
        pass
    
    @args(bool, [])
    def getReuseAddress(self):
        pass
    
    @synchronized
    @args(int, [])
    def getSendBufferSize(self):
        pass
    
    @args(int, [])
    def getSoLinger(self):
        pass
    
    @synchronized
    @arg(int, [])
    def getSoTimeout(self):
        pass
    
    @args(bool, [])
    def getTcpNoDelay(self):
        pass
    
    @args(int, [])
    def getTrafficClass(self):
        pass
    
    @args(bool, [])
    def isBound(self):
        pass
    
    @args(bool, [])
    def isClosed(self):
        pass
    
    @args(bool, [])
    def isConnected(self):
        pass
    
    @args(bool, [])
    def isInputShutdown(self):
        pass
    
    @args(bool, [])
    def isOutputShutdown(self):
        pass
    
    @args(void, [int])
    def sendUrgentData(self, value):
        pass
    
    @args(void, [bool])
    def setKeepAlive(self, keepAlive):
        pass
    
    @args(void, [bool])
    def setOOBInline(self, oobinline):
        pass
    
    @args(void, [int, int, int])
    def setPerformancePreferences(self, connectionTime, latency, bandwidth):
        pass
    
    @synchronized
    @args(void, [int])
    def setReceiveBufferSize(self, size):
        pass
    
    @args(void, [bool])
    def setReuseAddress(self, reuse):
        pass
    
    @synchronized
    @args(void, [int])
    def setSendBufferSize(self, size):
        pass
    
    @args(void, [bool, int])
    def setSoLinger(self, on, timeout):
        pass
    
    @synchronized
    @args(void, [int])
    def setSoTimeout(self, timeout):
        pass
    
    ### ...
    
    @arg(void, [bool])
    def setTcpNoDelay(self, on):
        pass
    
    @arg(void, [int])
    def setTrafficClass(self, value):
        pass
    
    def shutdownInput(self):
        pass
    
    def shutdownOutput(self):
        pass


class SocketAddress(Object):

    __interfaces__ = [Serializable]
    
    def __init__(self):
        pass


class SocketException(IOException):

    def __init__(self):
        pass
    
    @args(void, [String])
    def __init__(self, detailMessage):
        pass


class SocketImpl(Object):

    __interfaces__ = [SocketOptions]
    
    __fields__ = {
        "address": InetAddress,
        "fd": FileDescriptor,
        "localport": int,
        "port": int
        }
    
    def __init__(self):
        pass
    
    @args(String, [])
    def toString(self):
        pass
    
    @abstract
    @args(void, [SocketImpl])
    def accept(self, newSocket):
        pass
    
    @abstract
    @args(int, [])
    def available(self):
        pass
    
    @abstract
    @args(void, [InetAddress, int])
    def bind(self, address, port):
        pass
    
    @abstract
    def close(self):
        pass
    
    @abstract
    @args(void, [InetAddress, int])
    def connect(self, address, port):
        pass
    
    @abstract
    @args(void, [SocketAddress, int])
    def connect(self, remoteAddr, timeout):
        pass
    
    @abstract
    @args(void, [String, int])
    def connect(self, host, port):
        pass
    
    @abstract
    @args(void, [bool])
    def create(self, isStreaming):
        pass
    
    @args(FileDescriptor, [])
    def getFileDescriptor(self):
        pass
    
    @args(InetAddress, [])
    def getInetAddress(self):
        pass
    
    @abstract
    @args(InputStream, [])
    def getInputStream(self):
        pass
    
    @args(int, [])
    def getLocalPort(self):
        pass
    
    @abstract
    @args(OutputStream, [])
    def getOutputStream(self):
        pass
    
    @args(int, [])
    def getPort(self):
        pass
    
    @abstract
    @args(void, [int])
    def listen(self, backlog):
        pass
    
    @abstract
    @args(void, [int])
    def sendUrgentData(self, value):
        pass
    
    @args(void, [int, int, int])
    def setPerformancePreferences(self, connectionTime, latency, bandwidth):
        pass
    
    def shutdownInput(self):
        pass
    
    def shutdownOutput(self):
        pass
    
    @args(bool, [])
    def supportsUrgentData(self):
        pass


class SocketImplFactory:

    @args(SocketImpl, [])
    def createSocketImpl(self):
        pass


class SocketOptions:

    IP_MULTICAST_IF
    IP_MULTICAST_IF2
    IP_MULTICAST_LOOP
    IP_TOS
    SO_BINDADDR
    SO_BROADCAST
    SO_KEEPALIVE
    SO_LINGER
    SO_OOBINLINE
    SO_RCVBUF
    SO_REUSEADDR
    SO_SNDBUF
    SO_TIMEOUT
    TCP_NODELAY
    
    @args(Object, [int])
    def getOption(self, optID):
        pass
    
    @args(void, [int, Object])
    def setOption(self, optID, val):
        pass


class URI(Object):

    @args(void, [String])
    def __init__(self, spec):
        pass
    
    @args(void, [String, String, String])
    def __init__(self, scheme, schemeSpecificPart, fragment):
        pass
    
    @args(void, [String, String, String, int, String, String, String])
    def __init__(self, scheme, userInfo, host, port, path, query, fragment):
        pass
    
    @args(void, [String, String, String, String])
    def __init__(self, scheme, host, path, fragment):
        pass
    
    @args(void, [String, String, String, String, String])
    def __init__(self, scheme, authority, path, query, fragment):
        pass


class URL(Object):

    __interfaces__ = [Serializable]
    
    @args(void, [String])
    def __init__(self, spec):
        pass
    
    @args(void, [URL, String])
    def __init__(self, context, spec):
        pass
    
    @args(void, [String, String, String])
    def __init__(self, protocol, host, file):
        pass
    
    @args(void, [String, String, int, String])
    def __init__(self, protocol, host, port, file):
        pass
    
    @args(String, [])
    def getAuthority(self):
        pass
    
    @final
    @args(Object, [])
    def getContent(self):
        pass
    
    @args(int, [])
    def getDefaultPort(self):
        pass
    
    @args(String, [])
    def getFile(self):
        pass
    
    @args(String, [])
    def getHost(self):
        pass
    
    @args(String, [])
    def getPath(self):
        pass
    
    @args(int, [])
    def getPort(self):
        pass
    
    @args(String, [])
    def getProtocol(self):
        pass
    
    @args(String, [])
    def getQuery(self):
        pass
    
    @args(String, [])
    def getRef(self):
        pass
    
    @args(String, [])
    def getUserInfo(self):
        pass
    
    @args(URLConnection, [Proxy])
    def openConnection(self, proxy):
        pass
    
    @args(URLConnection, [])
    def openConnection(self):
        pass
    
    @final
    @args(InputStream, [])
    def openStream(self):
        pass
    
    @args(bool, [URL])
    def sameFile(self, other):
        pass
    
    ### ...
    
    @args(String, [])
    def toExternalForm(self):
        pass
    
    @args(URI, [])
    def toURI(self):
        pass


class URLConnection(Object):

    __fields__ = {
        "allowUserInteraction": bool,
        "connected": bool,
        "doInput": bool,
        "doOutput": bool,
        "ifModifiedSince": long,
        "url": URL,
        "useCaches": bool
        }
    
    @args(void, [URL])
    def __init__(self, url):
        pass
    
    @args(void, [String, String])
    def addRequestProperty(self, field, newValue):
        pass
    
    @abstract
    def connect(self):
        pass
    
    @args(bool, [])
    def getAllowUserInteraction(self):
        pass
    
    @args(int, [])
    def getConnectTimeout(self):
        pass
    
    ### ...
    
    @args(Object, [])
    def getContent(self):
        pass
    
    @args(String, [])
    def getContentEncoding(self):
        pass
    
    @args(int, [])
    def getContentLength(self):
        pass
    
    @args(String, [])
    def getContentType(self):
        pass
    
    @args(long, [])
    def getDate(self):
        pass
    
    @static
    @args(bool, [])
    def getDefaultAllowUserInteraction():
        pass
    
    @args(bool, [])
    def getDefaultUseCaches(self):
        pass
    
    @args(bool, [])
    def getDoInput(self):
        pass
    
    @args(bool, [])
    def getDoOutput(self):
        pass
    
    @args(long, [])
    def getExpiration(self):
        pass
    
    @static
    @args(FileNameMap, [])
    def getFileNameMap():
        pass
    
    @args(String, [String])
    def getHeaderField(self, key):
        pass
    
    @args(String, [int])
    def getHeaderField(self, position):
        pass
    
    @args(long, [String, long])
    def getHeaderFieldDate(self, key, defaultValue):
        pass
    
    @args(int, [String, int])
    def getHeaderFieldInt(self, key, defaultValue):
        pass
    
    @args(String, [int])
    def getHeaderFieldKey(self, position):
        pass
    
    @args(Map(String, List(String)), [])
    def getHeaderFields(self):
        pass
    
    @args(long, [])
    def getIfModifiedSince(self):
        pass
    
    @args(InputStream, [])
    def getInputStream(self):
        pass
    
    @args(long, [])
    def getLastModified(self):
        pass
    
    @args(OutputStream, [])
    def getOutputStream(self):
        pass
    
    @args(Permission, [])
    def getPermission(self):
        pass
    
    @args(int, [])
    def getReadTimeout(self):
        pass
    
    @args(Map(String, List(String)), [])
    def getRequestProperties(self):
        pass
    
    @args(String, [String])
    def getRequestProperty(self, field):
        pass
    
    @args(URL, [])
    def getURL(self):
        pass
    
    @args(bool, [])
    def getUseCaches(self):
        pass
    
    @static
    @args(String, [String])
    def guessContentTypeFromName(url):
        pass
    
    @static
    @args(String, [InputStream])
    def guessContentTypeFromStream(stream):
        pass
    
    @args(void, [bool])
    def setAllowUserInteraction(self, newValue):
        pass
    
    @args(void, [int])
    def setConnectTimeout(self, milliseconds):
        pass
    
    @synchronized
    @static
    @args(void, [ContentHandlerFactory])
    def setContentHandlerFactory(contentFactory):
        pass
    
    @static
    @args(void, [bool])
    def setDefaultAllowUserInteraction(allows):
        pass
    
    @args(void, [bool])
    def setDefaultUseCaches(self, newValue):
        pass
    
    @args(void, [bool])
    def setDoInput(self, newValue):
        pass
    
    @args(void, [bool])
    def setDoOutput(self, newValue):
        pass
    
    @static
    @args(void, [FileNameMap])
    def setFileNameMap(newMap):
        pass
    
    @args(void, [long])
    def setIfModifiedSince(self, newValue):
        pass
    
    @args(void, [int])
    def setReadTimeout(self, milliseconds):
        pass
    
    @args(void, [String, String])
    def setRequestProperty(self, field, value):
        pass
    
    @args(void, [bool])
    def setUseCaches(self, newValue):
        pass


class URLEncoder(Object):

    @static
    @args(String, [String, String])
    def encode(s, charsetName):
        pass
    
    @api(1, 1)
    @static
    @args(String, [String])
    def encode(s):
        pass
