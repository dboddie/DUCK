# Class declarations obtained from Android API documentation licensed under the
# Apache 2.0 license (http://www.apache.org/licenses/LICENSE-2.0) and rewritten
# in syntax that the Python compiler module can parse.

__package__ = "java.io"

from java.lang import Appendable, Comparable, Exception, Object, String
from java.net import URI, URL
from java.nio.channels import FileChannel
from java.util import Locale

class AutoCloseable:

    def close(self):
        pass


class BufferedInputStream(FilterInputStream):

    __fields__ = {
        "buf": [byte],
        "count": int,
        "marklimit": int,
        "markpos": int,
        "pos": int
        }
    
    @args(void, [InputStream])
    def __init__(self, input):
        pass
    
    @args(void, [InputStream, int])
    def __init__(self, input, size):
        pass


class BufferedOutputStream(FilterOutputStream):

    __fields__ = {
        "buf": [byte],
        "count": int
        }
    
    @args(void, [OutputStream])
    def __init__(self, input):
        pass
    
    @args(void, [OutputStream, int])
    def __init__(self, input, size):
        pass


class BufferedReader(Reader):

    @args(void, [Reader])
    def __init__(self, reader):
        pass
    
    @args(void, [Reader, int])
    def __init__(self, reader, size):
        pass
    
    def close(self):
        pass
    
    @args(void, [int])
    def mark(self, markLimit):
        pass
    
    @args(bool, [])
    def markSupported(self):
        pass
    
    @args(int, [])
    def read(self):
        pass
    
    @args(int, [[char], int, int])
    def read(self, buffer, offset, length):
        pass
    
    @args(String, [])
    def readLine(self):
        pass
    
    @args(bool, [])
    def ready(self):
        pass
    
    def reset(self):
        pass
    
    @args(long, [long])
    def skip(self, charCount):
        pass


class ByteArrayOutputStream(OutputStream):

    __fields__ = {
        "buf": [byte],
        "count": int
        }
    
    def __init__(self):
        pass
    
    @args(void, [int])
    def __init__(self, size):
        pass
    
    @args(void, [])
    def close(self):
        pass
    
    @synchronized
    @args(void, [])
    def reset(self):
        pass
    
    @args(int, [])
    def size(self):
        pass
    
    @synchronized
    @args([byte], [])
    def toByteArray(self):
        pass
    
    @args(String, [int])
    def toString(self, hibyte):
        pass
    
    @args(String, [String])
    def toString(self, charsetName):
        pass
    
    @args(String, [])
    def toString(self):
        pass
    
    @synchronized
    @args(void, [[byte], int, int])
    def write(self, buffer, offset, len):
        pass
    
    @synchronized
    @args(void, [int])
    def write(self, oneByte):
        pass
    
    @synchronized
    @args(void, [OutputStream])
    def writeTo(self, out):
        pass


class Closeable:

    __interfaces__ = [AutoCloseable]


class DataInput:

    @args(bool, [])
    def readBoolean(self):
        pass
    
    @args(byte, [])
    def readByte(self):
        pass
    
    @args(char, [])
    def readChar(self):
        pass
    
    @args(double, [])
    def readDouble(self):
        pass
    
    @args(float, [])
    def readFloat(self):
        pass
    
    @args(void, [[byte]])
    def readFully(self, dest):
        pass
    
    @args(void, [[byte], int, int])
    def readFully(self, dest, offset, byteCount):
        pass
    
    @args(int, [])
    def readInt(self):
        pass
    
    @args(String, [])
    def readLine(self):
        pass
    
    @args(long, [])
    def readLong(self):
        pass
    
    @args(short, [])
    def readShort(self):
        pass
    
    @args(String, [])
    def readUTF(self):
        pass
    
    @args(int, [])
    def readUnsignedByte(self):
        pass
    
    @args(int, [])
    def readUnsignedShort(self):
        pass
    
    @args(int, [int])
    def skipBytes(self, length):
        pass


class DataInputStream(FilterInputStream):

    __interfaces__ = [DataInput]
    
    @args(void, [InputStream])
    def __init__(self, input):
        pass
    
    @final
    @args(bool, [])
    def readBoolean(self):
        pass
    
    @final
    @args(byte, [])
    def readByte(self):
        pass
    
    @final
    @args(char, [])
    def readChar(self):
        pass
    
    @final
    @args(double, [])
    def readDouble(self):
        pass
    
    @final
    @args(float, [])
    def readFloat(self):
        pass
    
    @final
    @args(void, [[byte]])
    def readFully(self, dest):
        pass
    
    @final
    @args(void, [[byte], int, int])
    def readFully(self, dest, offset, byteCount):
        pass
    
    @final
    @args(int, [])
    def readInt(self):
        pass
    
    @final
    @args(long, [])
    def readLong(self):
        pass
    
    @final
    @args(short, [])
    def readShort(self):
        pass
    
    @final
    @args(String, [])
    def readUTF(self):
        pass
    
    @final
    @static
    @args(String, [DataInput])
    def readUTF(input):
        pass
    
    @final
    @args(int, [])
    def readUnsignedByte(self):
        pass
    
    @final
    @args(int, [])
    def readUnsignedShort(self):
        pass
    
    @final
    @args(int, [int])
    def skipBytes(self, length):
        pass


class DataOutput:

    @args(void, [int])
    def write(self, oneByte):
        pass
    
    @args(void, [[byte], int, int])
    def write(self, buffer, offset, count):
        pass
    
    @args(void, [[byte]])
    def write(self, buffer):
        pass
    
    @args(void, [bool])
    def writeBoolean(self, val):
        pass
    
    @args(void, [int])
    def writeByte(self, val):
        pass
    
    @args(void, [String])
    def writeBytes(self, str):
        pass
    
    @args(void, [int])
    def writeChar(self, val):
        pass
    
    @args(void, [String])
    def writeChars(self, str):
        pass
    
    @args(void, [double])
    def writeDouble(self, val):
        pass
    
    @args(void, [float])
    def writeFloat(self, val):
        pass
    
    @args(void, [int])
    def writeInt(self, val):
        pass
    
    @args(void, [long])
    def writeLong(self, val):
        pass
    
    @args(void, [int])
    def writeShort(self, val):
        pass
    
    @args(void, [String])
    def writeUTF(self, str):
        pass


class File(Object):

    __interfaces__ = [Serializable, Comparable]
    
    __static_fields__ = {
        "pathSeparator": String,
        "pathSeparatorChar": char,
        "separator": String,
        "separatorChar": char
        }
    
    @args(void, [File, String])
    def __init__(self, dir, name):
        pass
    
    @args(void, [String])
    def __init__(self, name):
        pass
    
    @args(void, [String, String])
    def __init__(self, dirPath, name):
        pass
    
    @args(void, [URI])
    def __init__(self, uri):
        pass
    
    @args(bool, [])
    def canExecute(self):
        pass
    
    @args(bool, [])
    def canRead(self):
        pass
    
    @args(bool, [])
    def canWrite(self):
        pass
    
    @args(int, [File])
    def compareTo(self, other):
        pass
    
    @args(bool, [])
    def createNewFile(self):
        pass
    
    @static
    @args(File, [String, String, File])
    def createTempFile(prefix, suffix, directory):
        pass
    
    @static
    @args(File, [String, String])
    def createTempFile(prefix, suffix):
        pass
    
    @args(bool, [])
    def delete(self):
        pass
    
    @args(bool, [])
    def deleteOnExit(self):
        pass
    
    @args(bool, [])
    def exists(self):
        pass
    
    @args(File, [])
    def getAbsoluteFile(self):
        pass
    
    @args(String, [])
    def getAbsolutePath(self):
        pass
    
    @args(File, [])
    def getCanonicalFile(self):
        pass
    
    @args(String, [])
    def getCanonicalPath(self):
        pass
    
    @args(long, [])
    def getFreeSpace(self):
        pass
    
    @args(String, [])
    def getName(self):
        pass
    
    @args(String, [])
    def getParent(self):
        pass
    
    @args(File, [])
    def getParentFile(self):
        pass
    
    @args(String, [])
    def getPath(self):
        pass
    
    @args(long, [])
    def getTotalSpace(self):
        pass
    
    @args(long, [])
    def getUsableSpace(self):
        pass
    
    @args(bool, [])
    def isAbsolute(self):
        pass
    
    @args(bool, [])
    def isDirectory(self):
        pass
    
    @args(bool, [])
    def isFile(self):
        pass
    
    @args(bool, [])
    def isHidden(self):
        pass
    
    @args(long, [])
    def lastModified(self):
        pass
    
    @args(long, [])
    def length(self):
        pass
    
    @args([String], [])
    def list(self):
        pass
    
    @args([String], [FilenameFilter])
    def list(self, filter):
        pass
    
    @args([File], [])
    def listFiles(self):
        pass
    
    @args([File], [FilenameFilter])
    def listFiles(self, filter):
        pass
    
    @static
    @args([File], [])
    def listRoots():
        pass
    
    @args(bool, [])
    def mkdir(self):
        pass
    
    @args(bool, [])
    def mkdirs(self):
        pass
    
    @args(bool, [File])
    def renameTo(self, newPath):
        pass
    
    @args(bool, [bool])
    def setExecutable(self, executable):
        pass
    
    @args(bool, [bool, bool])
    def setExecutable(self, executable, ownerOnly):
        pass
    
    @args(bool, [long])
    def setLastModified(self, time):
        pass
    
    @args(bool, [])
    def setReadOnly(self):
        pass
    
    @args(bool, [bool])
    def setReadable(self, readable):
        pass
    
    @args(bool, [bool, bool])
    def setReadable(self, readable, ownerOnly):
        pass
    
    @args(bool, [bool, bool])
    def setWritable(self, readable, ownerOnly):
        pass
    
    @args(bool, [bool])
    def setWritable(self, readable):
        pass
    
    @args(URI, [])
    def toURI(self):
        pass
    
    @args(URL, [])
    def toURL(self):
        pass


class FileDescriptor(Object):

    __fields__ = {
        "err": FileDescriptor,
        "in": FileDescriptor,
        "out": FileDescriptor
        }
    
    def __init__(self):
        pass
    
    def sync(self):
        pass
    
    @args(bool, [])
    def valid(self):
        pass


class FileFilter:

    @args(bool, [File])
    def accept(self, pathname):
        pass    


class FilenameFilter:

    @args(bool, [File, String])
    def accept(self, directory, filename):
        pass    


class FileInputStream(InputStream):

    @args(void, [File])
    def __init__(self, file):
        pass
    
    @args(void, [String])
    def __init__(self, path):
        pass
    
    @args(int, [])
    def available(self):
        pass


class FileNotFoundException(IOException):

    def __init__(self):
        pass
    
    @args(void, [String])
    def __init__(self, detailMessage):
        pass


class FileOutputStream(OutputStream):

    @args(void, [File])
    def __init__(self, file):
        pass
    
    @args(void, [File, bool])
    def __init__(self, file, append):
        pass
    
    @args(void, [String])
    def __init__(self, path):
        pass
    
    @args(void, [String, bool])
    def __init__(self, path, append):
        pass


class FileReader(InputStreamReader):

    @args(void, [File])
    def __init__(self, file):
        pass
    
    @args(void, [FileDescriptor])
    def __init__(self, fd):
        pass
    
    @args(void, [String])
    def __init__(self, filename):
        pass


class FileWriter(OutputStreamWriter):

    @args(void, [File])
    def __init__(self, file):
        pass
    
    @args(void, [File, bool])
    def __init__(self, file, append):
        pass
    
    @args(void, [FileDescriptor])
    def __init__(self, fd):
        pass
    
    @args(void, [String])
    def __init__(self, filename):
        pass
    
    @args(void, [String, bool])
    def __init__(self, filename, append):
        pass


class FilterInputStream(InputStream):

    __fields__ = {"in": InputStream}
    
    @args(void, [InputStream])
    def __init__(self, input):
        pass


class FilterOutputStream(OutputStream):

    __fields__ = {"out": OutputStream}
    
    @args(void, [OutputStream])
    def __init__(self, output):
        pass


class Flushable:

    def flush(self):
        pass


class InputStream(Object):

    __interfaces__ = [Closeable]
    
    def __init__(self):
        pass
    
    @args(int, [])
    def available(self):
        pass
    
    def close(self):
        pass
    
    @args(void, [int])
    def mark(self, readlimit):
        pass
    
    @args(bool, [])
    def markSupported(self):
        pass
    
    @args(int, [[byte]])
    def read(self, buffer):
        pass
    
    @abstract
    @args(int, [])
    def read(self):
        pass
    
    @args(int, [[byte], int, int])
    def read(self, buffer, byteOffset, byteCount):
        pass
    
    def reset(self):
        pass
    
    @args(long, [long])
    def skip(self, byteCount):
        pass


class InputStreamReader(Reader):

    @args(void, [InputStream])
    def __init__(self, stream):
        pass
    
    @args(void, [InputStream, String])
    def __init__(self, stream, charsetName):
        pass
    
    ### ...
    
    @args(void, [InputStream, Charset])
    def __init__(self, stream, charset):
        pass
    
    @args(void, [InputStream])
    def __init__(self, stream):
        pass
    
    @args(String, [])
    def getEncoding(self):
        pass


class IOException(Exception):

    def __init__(self):
        pass
    
    @args(void, [String])
    def __init__(self, detailMessage):
        pass
    
    @args(void, [String, Throwable])
    def __init__(self, detailMessage, throwable):
        pass
    
    @args(void, [Throwable])
    def __init__(self, throwable):
        pass


class ObjectInput:

    __interfaces__ = [DataInput, AutoCloseable]
    
    @args(int, [])
    def available(self):
        pass
    
    @args(void, [])
    def available(self):
        pass
    
    @args(int, [[byte]])
    def read(self, buffer):
        pass
    
    @args(int, [])
    def read(self):
        pass
    
    @args(int, [[byte], int, int])
    def read(self, buffer, byteOffset, byteCount):
        pass
    
    @args(Object, [])
    def readObject(self):
        pass
    
    @args(long, [long])
    def skip(self, byteCount):
        pass


class ObjectInputStream(InputStream):

    __interfaces__ = [ObjectInput]
    
    class GetField(Object):
    
        def __init__(self):
            pass
        
        @abstract
        @args(bool, [String])
        def defaulted(self, name):
            pass
        
        @abstract
        @args(byte, [String, byte])
        def get(self, name, defaultValue):
            pass
        
        @abstract
        @args(long, [String, long])
        def get(self, name, defaultValue):
            pass
        
        @abstract
        @args(Object, [String, Object])
        def get(self, name, defaultValue):
            pass
        
        @abstract
        @args(int, [String, int])
        def get(self, name, defaultValue):
            pass
        
        @abstract
        @args(short, [String, short])
        def get(self, name, defaultValue):
            pass
        
        @abstract
        @args(float, [String, float])
        def get(self, name, defaultValue):
            pass
        
        @abstract
        @args(double, [String, double])
        def get(self, name, defaultValue):
            pass
        
        @abstract
        @args(bool, [String, bool])
        def get(self, name, defaultValue):
            pass
        
        @abstract
        @args(char, [String, char])
        def get(self, name, defaultValue):
            pass
    
    @args(void, [InputStream])
    def __init__(self, input):
        pass
    
    def __init__(self):
        pass
    
    def defaultReadObject(self):
        pass
    
    @args(ObjectInputStream.GetField, [])
    def readFields(self):
        pass
    
    @args(Object, [])
    def readUnshared(self):
        pass


class OutputStream(Object):

    __interfaces__ = [Closeable, Flushable]
    
    def __init__(self):
        pass
    
    @args(void, [[byte], int, int])
    def write(self, buffer, offset, count):
        pass
    
    @args(void, [[byte]])
    def write(self, buffer):
        pass
    
    @abstract
    @args(void, [int])
    def write(self, oneByte):
        pass


class OutputStreamWriter(Writer):

    @args(void, [OutputStream])
    def __init__(self, out):
        pass
    
    @args(void, [OutputStream, String])
    def __init__(self, out, charsetName):
        pass
    
    @args(void, [OutputStream, Charset])
    def __init__(self, out, charset):
        pass
    
    @args(void, [OutputStream, CharsetEncoder])
    def __init__(self, out, charsetEncoder):
        pass
    
    def close(self):
        pass
    
    def flush(self):
        pass
    
    @args(String, [])
    def getEncoding(self):
        pass
    
    @args(void, [[char], int, int])
    def write(self, buffer, offset, count):
        pass
    
    @args(void, [String, int, int])
    def write(self, string, offset, count):
        pass
    
    @args(void, [int])
    def write(self, oneChar):
        pass


class PrintStream(FilterOutputStream):

    @args(void, [OutputStream])
    def __init__(self, out):
        pass
    
    @args(void, [OutputStream, bool])
    def __init__(self, out, autoFlush):
        pass
    
    @args(void, [OutputStream, bool, String])
    def __init__(self, out, autoFlush, charsetName):
        pass
    
    @args(void, [File])
    def __init__(self, file):
        pass
    
    @args(void, [File, String])
    def __init__(self, file, charsetName):
        pass
    
    @args(void, [String])
    def __init__(self, fileName):
        pass
    
    @args(void, [String, String])
    def __init__(self, fileName, charsetName):
        pass
    
    @args(PrintStream, [char])
    def append(self, c):
        pass
    
    @args(PrintStream, [Char, int, int])
    def append(self, charSequence, start, end):
        pass
    
    @args(PrintStream, [CharSequence])
    def append(self, charSequence):
        pass
    
    @args(bool, [])
    def checkError(self):
        pass
    
    @synchronized
    def close(self):
        pass
    
    @synchronized
    def flush(self):
        pass
    
    @args(PrintStream, [Locale, String, [Object]]) # ...
    def format(self, locale, format, args):
        pass
    
    @args(PrintStream, [String, [Object]]) # ...
    def format(self, format, args):
        pass
    
    #@args(void, [float])
    #def print(self, f):
    #    pass
    
    #@args(void, [double])
    #def print(self, d):
    #    pass
    
    #@synchronized
    #@args(void, [String])
    #def print(self, str):
    #    pass
    
    #@args(void, [Object])
    #def print(self, o):
    #    pass
    
    #@args(void, [char])
    #def print(self, c):
    #    pass
    
    #@args(void, [[char]])
    #def print(self, chars):
    #    pass
    
    #@args(void, [long])
    #def print(self, l):
    #    pass
    
    #@args(void, [int])
    #def print(self, i):
    #    pass
    
    #@args(void, [bool])
    #def print(self, b):
    #    pass
    
    @args(PrintStream, [Locale, String, [Object]]) # ...
    def printf(self, locale, format, args):
        pass
    
    @args(PrintStream, [String, [Object]]) # ...
    def printf(self, format, args):
        pass
    
    def println(self):
        pass
    
    @args(void, [float])
    def println(self, f):
        pass
    
    @args(void, [int])
    def println(self, i):
        pass
    
    @args(void, [Object])
    def println(self, o):
        pass
    
    @args(void, [[char]])
    def println(self, chars):
        pass
    
    @synchronized
    @args(void, [String])
    def println(self, str):
        pass
    
    @args(void, [char])
    def println(self, c):
        pass
    
    @args(void, [double])
    def println(self, d):
        pass
    
    @args(void, [bool])
    def println(self, b):
        pass
    
    @synchronized
    @args(void, [int])
    def write(self, oneByte):
        pass
    
    @args(void, [[byte], int, int])
    def write(self, buffer, offset, length):
        pass
    
    @protected
    def clearError(self):
        pass
    
    @protected
    def setError(self):
        pass


class RandomAccessFile(Object):

    __interfaces__ = [DataInput, DataOutput, Closeable]
    
    @args(void, [File, String])
    def __init__(self, file, mode):
        pass
    
    @args(void, [String, String])
    def __init__(self, fileName, mode):
        pass
    
    @args(void, [])
    def close(self):
        pass
    
    @synchronized
    @final
    @args(FileChannel, [])
    def getChannel(self):
        pass
    
    @final
    @args(FileDescriptor, [])
    def getFD(self):
        pass
    
    @args(long, [])
    def getFilePointer(self):
        pass
    
    @args(long, [])
    def length(self):
        pass
    
    @args(int, [[byte], int, int])
    def read(self, buffer, byteOffset, byteCount):
        pass
    
    @args(int, [[byte]])
    def read(self, buffer):
        pass
    
    @args(int, [])
    def read(self):
        pass
    
    @final
    @args(bool, [])
    def readBoolean(self):
        pass
    
    @final
    @args(byte, [])
    def readByte(self):
        pass
    
    @final
    @args(char, [])
    def readChar(self):
        pass
    
    @final
    @args(double, [])
    def readDouble(self):
        pass
    
    @final
    @args(float, [])
    def readFloat(self):
        pass
    
    @final
    @args(void, [[byte]])
    def readFully(self, dst):
        pass
    
    @final
    @args(void, [[byte], int, int])
    def readFully(self, dst, offset, byteCount):
        pass
    
    @final
    @args(int, [])
    def readInt(self):
        pass
    
    @final
    @args(String, [])
    def readLine(self):
        pass
    
    @final
    @args(long, [])
    def readLong(self):
        pass
    
    @final
    @args(short, [])
    def readShort(self):
        pass
    
    @final
    @args(String, [])
    def readUTF(self):
        pass
    
    @final
    @args(int, [])
    def readUnsignedByte(self):
        pass
    
    @final
    @args(int, [])
    def readUnsignedShort(self):
        pass
    
    @args(void, [long])
    def seek(self, offset):
        pass
    
    @args(void, [long])
    def setLength(self, newLength):
        pass
    
    @args(int, [int])
    def skipBytes(self, count):
        pass
    
    @args(void, [int])
    def write(self, oneByte):
        pass
    
    @args(void, [[byte], int, int])
    def write(self, buffer, byteOffset, byteCount):
        pass
    
    @args(void, [[byte]])
    def write(self, buffer):
        pass
    
    @final
    @args(void, [bool])
    def writeBoolean(self, val):
        pass
    
    @final
    @args(void, [int])
    def writeByte(self, val):
        pass
    
    @final
    @args(void, [String])
    def writeBytes(self, str):
        pass
    
    @final
    @args(void, [int])
    def writeChar(self, val):
        pass
    
    @final
    @args(void, [String])
    def writeChars(self, str):
        pass
    
    @final
    @args(void, [double])
    def writeDouble(self, val):
        pass
    
    @final
    @args(void, [float])
    def writeFloat(self, val):
        pass
    
    @final
    @args(void, [int])
    def writeInt(self, val):
        pass
    
    @final
    @args(void, [long])
    def writeLong(self, val):
        pass
    
    @final
    @args(void, [int])
    def writeShort(self, val):
        pass
    
    @final
    @args(void, [String])
    def writeUTF(self, str):
        pass


class Readable:

    @args(int, [CharBuffer])
    def read(self, charBuffer):
        pass


class Reader(Object):

    __interfaces__ = [Readable, Closeable]
    
    @args(void, [int])
    def mark(self, readLimit):
        pass
    
    @args(bool, [])
    def markSupported(self):
        pass
    
    @args(int, [])
    def read(self):
        pass
    
    @abstract
    @args(int, [[char], int, int])
    def read(self, buffer, offset, count):
        pass
    
    @args(int, [[char]])
    def read(self, buffer):
        pass
    
    @args(bool, [])
    def ready(self):
        pass
    
    def reset(self):
        pass
    
    @args(long, [long])
    def skip(self, charCount):
        pass


class Serializable:

    pass


class Writer(Object):

    __interfaces__ = [Appendable, Closeable, Flushable]
    
    __fields__ = {
        "lock": Object  # protected
        }
    
    def __init__(self):
        pass
    
    @args(void, [Object])
    def __init__(self, lock):
        pass
    
    @args(Writer, [CharSequence])
    def append(self, csq):
        pass
    
    @args(Writer, [CharSequence, int, int])
    def append(self, csq, start, end):
        pass
    
    @args(Writer, [char])
    def append(self, c):
        pass
    
    @abstract
    def close(self):
        pass
    
    @abstract
    def flush(self):
        pass
    
    @args(void, [[char]])
    def write(self, buf):
        pass
    
    @args(void, [String])
    def write(self, str):
        pass
    
    @abstract
    @args(void, [[char], int, int])
    def write(self, buf, offset, count):
        pass
    
    @args(void, [String, int, int ])
    def write(self, str, offset, count):
        pass
    
    @args(void, [int])
    def write(self, oneChar):
        pass
