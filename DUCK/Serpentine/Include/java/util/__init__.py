# Class declarations obtained from Android API documentation licensed under the
# Apache 2.0 license (http://www.apache.org/licenses/LICENSE-2.0) and rewritten
# in syntax that the Python compiler module can parse.

__package__ = "java.util"

from java.io import Serializable
from java.lang import Boolean, Class, Cloneable, Comparable, Iterable, \
                      Object, Runnable, RuntimeException, String

class AbstractCollection(Object):

    __interfaces__ = [Collection]
    
    def __init__(self):
        pass


class AbstractList(AbstractCollection):

    __interfaces__ = [List]
    __fields__ = {"modCount": int}
    
    def __init__(self):
        pass


class AbstractMap(Object):

    __interfaces__ = [Map]
    
    class SimpleEntry(Object):
    
        __interfaces__ = [Serializable, Map.Entry]
        
        @args(void, [K, V])
        def __init__(self, key, value):
            pass
        
        @args(void, [Map.Entry(K,V)])
        def __init__(self, copyFrom):
            pass
        
        @args(K, [])
        def getKey(self):
            pass
        
        @args(V, [])
        def getValue(self):
            pass
        
        @args(V, [V])
        def setValue(self, value):
            pass
    
    class SimpleImmutableEntry(Object):
    
        __interfaces__ = [Serializable, Map.Entry]
        
        @args(void, [K, V])
        def __init__(self, key, value):
            pass
        
        @args(void, [Map.Map.Entry(K,V)])
        def __init__(self, copyFrom):
            pass
        
        @args(K, [])
        def getKey(self):
            pass
        
        @args(V, [])
        def getValue(self):
            pass
        
        @args(V, [V])
        def setValue(self, value):
            pass
    
    def __init__(self):
        pass


class AbstractQueue(AbstractCollection):

    __interfaces__ = [Queue]
    __parameters__ = [E]
    
    def __init__(self):
        pass
    
    @args(bool, [E])
    def add(self, e):
        pass
    
    @args(bool, [Collection(E)])
    def addAll(self, c):
        pass
    
    def clear(self):
        pass
    
    @args(E, [])
    def element(self):
        pass
    
    @args(E, [])
    def remove(self):
        pass


class AbstractSequentialList(AbstractList):

    def __init__(self):
        pass


class AbstractSet(AbstractCollection):

    __interfaces__ = [Set]


class ArrayList(AbstractList):

    __interfaces__ = [Serializable, Cloneable, RandomAccess]
    
    @args(void, [int])
    def __init__(self, capacity):
        pass
    
    def __init__(self):
        pass
    
    def trimToSize(self):
        pass


class Arrays(Object):

    __parameters__ = [T]
    
    @static
    @args(List(T), [[T]])
    def asList(array):
        pass
    
    ### ...
    
    @static
    @args(int, [[byte], int, int, byte])
    def binarySearch(array, startIndex, endIndex, value):
        pass
    
    @static
    @args(int, [[byte], byte])
    def binarySearch(array, value):
        pass
    
    @static
    @args(int, [[char], int, int, char])
    def binarySearch(array, startIndex, endIndex, value):
        pass
    
    @static
    @args(int, [[char], char])
    def binarySearch(array, value):
        pass
    
    @static
    @args(int, [[double], int, int, double])
    def binarySearch(array, startIndex, endIndex, value):
        pass
    
    @static
    @args(int, [[double], double])
    def binarySearch(array, value):
        pass
    
    @static
    @args(int, [[float], int, int, float])
    def binarySearch(array, startIndex, endIndex, value):
        pass
    
    @static
    @args(int, [[float], float])
    def binarySearch(array, value):
        pass
    
    @static
    @args(int, [[int], int, int, int])
    def binarySearch(array, startIndex, endIndex, value):
        pass
    
    @static
    @args(int, [[int], int])
    def binarySearch(array, value):
        pass
    
    @static
    @args(int, [[long], int, int, long])
    def binarySearch(array, startIndex, endIndex, value):
        pass
    
    @static
    @args(int, [[long], long])
    def binarySearch(array, value):
        pass
    
    @static
    @args(int, [[Object], int, int, Object])
    def binarySearch(array, startIndex, endIndex, value):
        pass
    
    @static
    @args(int, [[Object], Object])
    def binarySearch(array, value):
        pass
    
    @static
    @args(int, [[short], int, int, short])
    def binarySearch(array, startIndex, endIndex, value):
        pass
    
    @static
    @args(int, [[short], short])
    def binarySearch(array, value):
        pass
    
    @static
    @args([bool], [[bool], int])
    def copyOf(array, newLength):
        pass
    
    @static
    @args([byte], [[byte], int])
    def copyOf(array, newLength):
        pass
    
    @static
    @args([char], [[char], int])
    def copyOf(array, newLength):
        pass
    
    @static
    @args([double], [[double], int])
    def copyOf(array, newLength):
        pass
    
    @static
    @args([float], [[float], int])
    def copyOf(array, newLength):
        pass
    
    @static
    @args([int], [[int], int])
    def copyOf(array, newLength):
        pass
    
    @static
    @args([long], [[long], int])
    def copyOf(array, newLength):
        pass
    
    @static
    @args([short], [[short], int])
    def copyOf(array, newLength):
        pass
    
    @static
    @args([T], [[T], int])
    def copyOf(array, newLength):
        pass
    
    @static
    @args([bool], [[bool], int, int])
    def copyOfRange(array, start, end):
        pass
    
    @static
    @args([byte], [[byte], int, int])
    def copyOfRange(array, start, end):
        pass
    
    @static
    @args([char], [[char], int, int])
    def copyOfRange(array, start, end):
        pass
    
    @static
    @args([double], [[double], int, int])
    def copyOfRange(array, start, end):
        pass
    
    @static
    @args([float], [[float], int, int])
    def copyOfRange(array, start, end):
        pass
    
    @static
    @args([int], [[int], int, int])
    def copyOfRange(array, start, end):
        pass
    
    @static
    @args([long], [[long], int, int])
    def copyOfRange(array, start, end):
        pass
    
    @static
    @args([short], [[short], int, int])
    def copyOfRange(array, start, end):
        pass
    
    @static
    @args([T], [[T], int, int])
    def copyOfRange(array, start, end):
        pass
    
    @static
    @args(bool, [[Object], [Object]])
    def deepEquals(array1, array2):
        pass
    
    @static
    @args(int, [[Object]])
    def deepHashCode(array):
        pass
    
    @static
    @args(String, [[Object]])
    def deepToString(array):
        pass
    
    @static
    @args(bool, [[bool], [bool]])
    def equals(array1, array2):
        pass
    
    @static
    @args(bool, [[byte], [byte]])
    def equals(array1, array2):
        pass
    
    @static
    @args(bool, [[char], [char]])
    def equals(array1, array2):
        pass
    
    @static
    @args(bool, [[double], [double]])
    def equals(array1, array2):
        pass
    
    @static
    @args(bool, [[float], [float]])
    def equals(array1, array2):
        pass
    
    @static
    @args(bool, [[int], [int]])
    def equals(array1, array2):
        pass
    
    @static
    @args(bool, [[long], [long]])
    def equals(array1, array2):
        pass
    
    @static
    @args(bool, [[Object], [Object]])
    def equals(array1, array2):
        pass
    
    @static
    @args(bool, [[short], [short]])
    def equals(array1, array2):
        pass
    
    @static
    @args(void, [[bool], bool])
    def fill(array, value):
        pass
    
    @static
    @args(void, [[bool], int, int, bool])
    def fill(array, start, end, value):
        pass
    
    @static
    @args(void, [[byte], byte])
    def fill(array, value):
        pass
    
    @static
    @args(void, [[byte], int, int, byte])
    def fill(array, start, end, value):
        pass
    
    @static
    @args(void, [[char], char])
    def fill(array, value):
        pass
    
    @static
    @args(void, [[char], int, int, char])
    def fill(array, start, end, value):
        pass
    
    @static
    @args(void, [[double], double])
    def fill(array, value):
        pass
    
    @static
    @args(void, [[double], int, int, double])
    def fill(array, start, end, value):
        pass
    
    @static
    @args(void, [[float], float])
    def fill(array, value):
        pass
    
    @static
    @args(void, [[float], int, int, float])
    def fill(array, start, end, value):
        pass
    
    @static
    @args(void, [[int], int])
    def fill(array, value):
        pass
    
    @static
    @args(void, [[int], int, int, int])
    def fill(array, start, end, value):
        pass
    
    @static
    @args(void, [[long], long])
    def fill(array, value):
        pass
    
    @static
    @args(void, [[long], int, int, long])
    def fill(array, start, end, value):
        pass
    
    @static
    @args(void, [[Object], Object])
    def fill(array, value):
        pass
    
    @static
    @args(void, [[Object], int, int, Object])
    def fill(array, start, end, value):
        pass
    
    @static
    @args(void, [[short], short])
    def fill(array, value):
        pass
    
    @static
    @args(void, [[short], int, int, short])
    def fill(array, start, end, value):
        pass
    
    @static
    @args(int, [[bool]])
    def hashCode(array):
        pass
    
    @static
    @args(int, [[byte]])
    def hashCode(array):
        pass
    
    @static
    @args(int, [[char]])
    def hashCode(array):
        pass
    
    @static
    @args(int, [[double]])
    def hashCode(array):
        pass
    
    @static
    @args(int, [[float]])
    def hashCode(array):
        pass
    
    @static
    @args(int, [[int]])
    def hashCode(array):
        pass
    
    @static
    @args(int, [[long]])
    def hashCode(array):
        pass
    
    @static
    @args(int, [[Object]])
    def hashCode(array):
        pass
    
    @static
    @args(int, [[short]])
    def hashCode(array):
        pass
    
    @static
    @args(void, [[byte]])
    def sort(array):
        pass
    
    @static
    @args(void, [[byte], int, int])
    def sort(array, start, end):
        pass
    
    @static
    @args(void, [[char]])
    def sort(array):
        pass
    
    @static
    @args(void, [[char], int, int])
    def sort(array, start, end):
        pass
    
    @static
    @args(void, [[double]])
    def sort(array):
        pass
    
    @static
    @args(void, [[double], int, int])
    def sort(array, start, end):
        pass
    
    @static
    @args(void, [[float]])
    def sort(array):
        pass
    
    @static
    @args(void, [[float], int, int])
    def sort(array, start, end):
        pass
    
    @static
    @args(void, [[int]])
    def sort(array):
        pass
    
    @static
    @args(void, [[int], int, int])
    def sort(array, start, end):
        pass
    
    @static
    @args(void, [[long]])
    def sort(array):
        pass
    
    @static
    @args(void, [[long], int, int])
    def sort(array, start, end):
        pass
    
    @static
    @args(void, [[Object]])
    def sort(array):
        pass
    
    @static
    @args(void, [[Object], int, int])
    def sort(array, start, end):
        pass
    
    @static
    @args(void, [[short]])
    def sort(array):
        pass
    
    @static
    @args(void, [[short], int, int])
    def sort(array, start, end):
        pass
    
    @static
    @args(String, [[bool]])
    def toString(array):
        pass
    
    @static
    @args(String, [[byte]])
    def toString(array):
        pass
    
    @static
    @args(String, [[char]])
    def toString(array):
        pass
    
    @static
    @args(String, [[double]])
    def toString(array):
        pass
    
    @static
    @args(String, [[float]])
    def toString(array):
        pass
    
    @static
    @args(String, [[int]])
    def toString(array):
        pass
    
    @static
    @args(String, [[long]])
    def toString(array):
        pass
    
    @static
    @args(String, [[Object]])
    def toString(array):
        pass
    
    @static
    @args(String, [[short]])
    def toString(array):
        pass
    

class Calendar(Object):

    __interfaces__ = [Serializable, Cloneable, Comparable] # Comparable(Calendar)
    
    __fields__ = {
        "areFieldsSet": bool,
        "fields": [int],
        "isSet": [bool],
        "isTimeSet": bool,
        "time": long
        }
    
    ALL_STYLES = 0
    AM = 0
    AM_PM = 9
    APRIL = 3
    AUGUST = 7
    DATE = 5
    DAY_OF_MONTH = 5
    DAY_OF_WEEK = 7
    DAY_OF_WEEK_IN_MONTH = 8
    DAY_OF_YEAR = 6
    DECEMBER = 11
    DST_OFFSET = 16
    ERA = 0
    FEBRUARY = 1
    FIELD_COUNT = 17
    FRIDAY = 6
    HOUR = 10
    HOUR_OF_DAY = 11
    JANUARY = 0
    JULY = 6
    JUNE = 5
    LONG = 2
    MARCH = 2
    MAY = 4
    MILLISECOND = 14
    MINUTE = 12
    MONDAY = 2
    MONTH = 2
    NOVEMBER = 10
    OCTOBER = 9
    PM = 1
    SATURDAY = 7
    SECOND = 13
    SEPTEMBER = 8
    SHORT = 1
    SUNDAY = 1
    THURSDAY = 5
    TUESDAY = 3
    UNDECIMBER = 12
    WEDNESDAY = 4
    WEEK_OF_MONTH = 4
    WEEK_OF_YEAR = 3
    YEAR = 1
    ZONE_OFFSET = 15
    
    @protected
    def __init__(self):
        pass
    
    @protected
    @args(void, [TimeZone, Locale])
    def __init__(self, timezone, locale):
        pass
    
    @abstract
    @args(void, [int, int])
    def add(self, field, value):
        pass
    
    @args(bool, [Object])
    def after(self, calendar):
        pass
    
    @args(bool, [Object])
    def before(self, calendar):
        pass
    
    @final
    @args(void, [int])
    def clear(self, field):
        pass
    
    @final
    def clear(self):
        pass
    
    @args(Object, [])
    def clone(self):
        pass
    
    @args(int, [Calendar])
    def compareTo(self, anotherCalendar):
        pass
    
    @args(bool, [Object])
    def equals(self, object):
        pass
    
    @args(int, [int])
    def get(self, field):
        pass
    
    @args(int, [int])
    def getActualMaximum(self, field):
        pass
    
    @args(int, [int])
    def getActualMinimum(self, field):
        pass
    
    @synchronized
    @static
    @args([Locale], [])
    def getAvailableLocales():
        pass
    
    @args(String, [int, int, Locale])
    def getDisplayName(self, field, style, locale):
        pass
    
    @args(Map(String, Integer), [int, int, Locale])
    def getDisplayNames(self, field, style, locale):
        pass
    
    @args(int, [])
    def getFirstDayOfWeek(self):
        pass
    
    @abstract
    @args(int, [int])
    def getGreatestMinimum(self, field):
        pass
    
    @synchronized
    @static
    @args(Calendar, [Locale])
    def getInstance(locale):
        pass
    
    @synchronized
    @static
    @args(Calendar, [TimeZone, Locale])
    def getInstance(timezone, locale):
        pass
    
    @synchronized
    @static
    @args(Calendar, [TimeZone])
    def getInstance(timezone):
        pass
    
    @synchronized
    @static
    @args(Calendar, [])
    def getInstance():
        pass
    
    @abstract
    @args(int, [int])
    def getLeastMaximum(self, field):
        pass
    
    @abstract
    @args(int, [int])
    def getMaximum(self, field):
        pass
    
    @args(int, [])
    def getMinimalDaysInFirstWeek(self):
        pass
    
    @abstract
    @args(int, [int])
    def getMinimum(self, field):
        pass
    
    @final
    @args(Date, [])
    def getTime(self):
        pass
    
    @args(long, [])
    def getTimeInMillis(self):
        pass
    
    @args(TimeZone, [])
    def getTimeZone(self):
        pass
    
    @args(int, [])
    def hashCode(self):
        pass
    
    @args(bool, [])
    def isLenient(self):
        pass
    
    @final
    @args(bool, [int])
    def isSet(self, field):
        pass
    
    @args(void, [int, int])
    def roll(self, field, value):
        pass
    
    @abstract
    @args(void, [int, bool])
    def roll(self, field, increment):
        pass
    
    @args(void, [int, int])
    def set(self, field, value):
        pass
    
    @final
    @args(void, [int, int, int, int, int, int])
    def set(self, year, month, day, hourOfDay, minute, second):
        pass
    
    @final
    @args(void, [int, int, int])
    def set(self, year, month, day):
        pass
    
    @final
    @args(void, [int, int, int, int, int])
    def set(self, year, month, day, hourOfDay, minute):
        pass
    
    @args(void, [int])
    def setFirstDayOfWeek(self, value):
        pass
    
    @args(void, [bool])
    def setLenient(self, value):
        pass
    
    @args(void, [int])
    def setMinimalDaysInFirstWeek(self, value):
        pass
    
    @final
    @args(void, [Date])
    def setTime(self, date):
        pass
    
    @args(void, [long])
    def setTimeInMillis(self, milliseconds):
        pass
    
    @args(void, [TimeZone])
    def setTimeZone(self, timezone):
        pass
    
    @args(String, [])
    def toString(self):
        pass


class Collection:

    __interfaces__ = [Iterable] # Iterable(T)
    __parameters__ = [E]
    
    @args(bool, [E])
    def add(self, obj):
        pass
    
    @args(bool, [Collection(E)])
    def addAll(self, collection):
        pass
    
    def clear(self):
        pass
    
    @args(bool, [Object])
    def contains(self, object):
        pass
    
    @args(bool, [Collection(E)])
    def containsAll(self, collection):
        pass
    
    @args(bool, [Object])
    def equals(self, object):
        pass
    
    @args(int, [])
    def hashCode(self):
        pass
    
    @args(bool, [])
    def isEmpty(self):
        pass
    
    @args(bool, [Object])
    def remove(self, object):
        pass
    
    @args(bool, [Collection(E)])
    def removeAll(self, collection):
        pass
    
    @args(bool, [Collection(E)])
    def retainAll(self, collection):
        pass
    
    @args(int, [])
    def size(self):
        pass
    
    @args([E], [[E]])
    def toArray(self, array):
        pass
    
    # Note that the standard API declares that this returns an Object, but this
    # throws away information about the type of the items, so we use the E
    # parameter to return type information. The method is still encoded with
    # the correct signature.
    @args([E], [])
    def toArray(self):
        pass


class Collections(Object):

    __fields__ = {
        "EMPTY_LIST": List,
        "EMPTY_MAP": Map,
        "EMPTY_SET": Set
        }
    __parameters__ = [T, E]
    
    # Requires support for multiple trailing arguments.
    #@static
    #@args(bool, [Collection(T), T])
    #def addAll(self, collection, elements):
    #    pass
    
    @static
    @args(Queue(T), [Deque(T)])
    def asLifoQueue(deque):
        pass
    
    @static
    @args(int, [List(T), T])
    def binarySearch(list, object):
        pass
    
    @static
    @args(int, [List(T), T, Comparator(T)])
    def binarySearch(list, object, comparator):
        pass
    
    @static
    @args(Collection(E), [Collection(E), Class(E)])
    def checkedCollection(collection, type):
        pass
    
    @static
    @args(List(E), [List(E), Class(E)])
    def checkedList(list, type):
        pass
    
    ### ...
    
    @static(void, [List(T), T])
    def fill(list, object):
        pass
    
    ### ...
    
    @static
    @args(Object, [Collection(T)])
    def max(collection):
        pass
    
    @static
    @args(Object, [Collection(T), Comparator(T)])
    def max(collection, comparator):
        pass
    
    @static
    @args(Object, [Collection(T)])
    def min(collection):
        pass
    
    @static
    @args(Object, [Collection(T), Comparator(T)])
    def min(collection, comparator):
        pass
    
    @static
    @args(List(T), [int, T])
    def nCopies(length, object):
        pass
    
    @static
    @args(Set(E), [Map(E, Boolean)])
    def newSetFromMap(map):
        pass
    
    @static
    @args(bool, [List(T), T, T])
    def replaceAll(list, object, replacement):
        pass
    
    @static
    @args(void, [List(T)])
    def reverse(list):
        pass
    
    ### ...
    
    @static
    @args(void, [List(T), Comparator(T)])
    def sort(list, comparator):
        pass
    
    @static
    @args(void, [List(T)])
    def sort(list):
        pass


class Comparator:

    __parameters__ = [T]
    
    @args(int, [T, T])
    def compare(self, lhs, rhs):
        pass
    
    @args(bool, [Object])
    def equals(self, object):
        pass


class Date(Object):

    __interfaces__ = [Serializable, Cloneable, Runnable]
    
    def __init__(self):
        pass
    
    @args(void, [long])
    def __init__(self, milliseconds):
        pass
    
    @args(bool, [Date])
    def after(self, date):
        pass
    
    @args(bool, [Date])
    def before(self, date):
        pass
    
    @args(Object, [])
    def clone(self):
        pass
    
    @args(int, [Date])
    def compareTo(self, date):
        pass
    
    @args(bool, [Object])
    def equals(self, object):
        pass
    
    @api(1, 1)
    @args(int, [])
    def getDate(self):
        pass
    
    @api(1, 1)
    @args(int, [])
    def getDay(self):
        pass
    
    @api(1, 1)
    @args(int, [])
    def getHours(self):
        pass
    
    @api(1, 1)
    @args(int, [])
    def getMinutes(self):
        pass
    
    @api(1, 1)
    @args(int, [])
    def getMonth(self):
        pass
    
    @api(1, 1)
    @args(int, [])
    def getSeconds(self):
        pass
    
    @args(long, []) 
    def getTime(self):
        pass
    
    @api(1, 1)
    @args(int, []) 
    def getTimezoneOffset(self):
        pass
    
    @api(1, 1)
    @args(int, []) 
    def getYear(self):
        pass
    
    @args(int, [])
    def hashCode(self):
        pass
    
    @static
    @args(long, [String])
    def parse(self, string):
        pass
    
    @api(1, 1)
    @args(void, [int])
    def setDate(self, day):
        pass
    
    @api(1, 1)
    @args(void, [int])
    def setHours(self, hour):
        pass
    
    @api(1, 1)
    @args(void, [int])
    def setMinutes(self, minute):
        pass
    
    @api(1, 1)
    @args(void, [int])
    def setMonth(self, month):
        pass
    
    @api(1, 1)
    @args(void, [int])
    def setSeconds(self, second):
        pass
    
    @args(void, [long])
    def setTime(self, milliseconds):
        pass
    
    @api(1, 1)
    @args(void, [int])
    def setYear(self, year):
        pass
    
    @api(1, 1)
    @args(String, [])
    def toGMTString(self):
        pass
    
    @api(1, 1)
    @args(String, [])
    def toLocaleString(self):
        pass
    
    @args(String, [])
    def toString(self):
        pass


class Deque:

    __interfaces__ = [Queue]
    __parameters__ = [E]
    
    @args(void, [E])
    def addFirst(self, element):
        pass
    
    @args(void, [E])
    def addLast(self, element):
        pass
    
    @args(Iterator(E), [])
    def descendingIterator(self):
        pass
    
    @args(E, [])
    def getFirst(self):
        pass
    
    @args(E, [])
    def getLast(self):
        pass
    
    @args(bool, [E])
    def offerFirst(self, element):
        pass
    
    @args(bool, [E])
    def offerLast(self, element):
        pass
    
    @args(E, [])
    def peekFirst(self):
        pass
    
    @args(E, [])
    def peekLast(self):
        pass
    
    @args(E, [])
    def pollFirst(self):
        pass
    
    @args(E, [])
    def pollLast(self):
        pass
    
    @args(E, [])
    def pop(self):
        pass
    
    @args(void, [E])
    def push(self, element):
        pass
    
    @args(E, [])
    def removeFirst(self):
        pass
    
    @args(bool, [Object])
    def removeFirstOccurrence(self, object):
        pass
    
    @args(E, [])
    def removeLast(self):
        pass
    
    @args(bool, [Object])
    def removeLastOccurrence(self, object):
        pass


class Enumeration:

    __parameters__ = [E]
    
    @args(bool, [])
    def hasMoreElements(self):
        pass
    
    @args(E, [])
    def nextElement(self):
        pass


class GregorianCalendar(Calendar):

    AD = 1
    BC = 0
    
    def __init__(self):
        pass
    
    @args(void, [int, int, int])
    def __init__(self, year, month, day):
        pass
    
    @args(void, [int, int, int, int, int])
    def __init__(self, year, month, day, hour, minute):
        pass
    
    @args(void, [int, int, int, int, int, int])
    def __init__(self, year, month, day, hour, minute, second):
        pass
    
    @args(void, [Locale])
    def __init__(self, locale):
        pass
    
    @args(void, [TimeZone])
    def __init__(self, timezone):
        pass
    
    @args(void, [TimeZone, Locale])
    def __init__(self, timezone, locale):
        pass
    
    @args(void, [int, int])
    def add(self, field, value):
        pass
    
    @args(bool, [Object])
    def equals(self, object):
        pass
    
    @args(int, [int])
    def getActualMaximum(self, field):
        pass
    
    @args(int, [int])
    def getActualMinimum(self, field):
        pass
    
    @args(int, [int])
    def getGreatestMinimum(self, field):
        pass
    
    @final
    @args(Date, [])
    def getGregorianChange(self):
        pass
    
    @args(int, [int])
    def getLeastMaximum(self, field):
        pass
    
    @args(int, [int])
    def getMaximum(self, field):
        pass
    
    @args(int, [int])
    def getMinimum(self, field):
        pass
    
    @args(int, [])
    def hashCode(self):
        pass
    
    @args(bool, [int])
    def isLeapYear(self, year):
        pass
    
    @args(void, [int, bool])
    def roll(self, field, increment):
        pass
    
    @args(void, [int, int])
    def roll(self, field, value):
        pass
    
    @args(void, [Date])
    def setGregorianChange(self, date):
        pass


class HashMap(AbstractMap):

    __interfaces__ = [Serializable, Cloneable]
    
    def __init__(self):
        pass
    
    @args(void, [int])
    def __init__(self, capacity):
        pass
    
    @args(void, [int, float])
    def __init__(self, capacity, loadFactor):
        pass
    
    @args(void, [Map(K,V)])
    def __init__(self, map):
        pass
    
    def clear(self):
        pass
    
    @args(Object, [])
    def clone(self):
        pass
    
    @args(bool, [Object])
    def containsKey(self, key):
        pass
    
    @args(bool, [Object])
    def containsValue(self, value):
        pass
    
    @args(Set(Map.Entry(K,V)), [])
    def entrySet(self):
        pass
    
    @args(V, [Object])
    def get(self, key):
        pass
    
    @args(bool, [])
    def isEmpty(self):
        pass
    
    @args(Set(K), [])
    def keySet(self):
        pass
    
    @args(V, [K, V])
    def put(self, key, value):
        pass
    
    @args(void, [Map(K,V)])
    def putAll(self, map):
        pass
    
    @args(V, [Object])
    def remove(self, key):
        pass
    
    @args(int, [])
    def size(self):
        pass
    
    @args(Collection(V), [])
    def values(self):
        pass


class HashSet(AbstractSet):

    __interfaces__ = [Serializable, Cloneable, Set]
    
    def __init__(self):
        pass
    
    @args(void, [int])
    def __init__(self, capacity):
        pass
    
    @args(void, [int, float])
    def __init__(self, capacity, loadFactor):
        pass
    
    @args(void, [Collection(E)])
    def __init__(self, collection):
        pass
    
    def clear(self):
        pass
    
    @args(Object, [])
    def clone(self):
        pass
    
    @args(bool, [Object])
    def contains(self, object):
        pass
    
    @args(bool, [])
    def isEmpty(self):
        pass
    
    @args(Iterator(E), [])
    def iterator(self):
        pass
    
    @args(bool, [Object])
    def remove(self, object):
        pass
    
    @args(int, [])
    def size(self):
        pass


class Iterator:

    __parameters__ = [E]
    
    @args(bool, [])
    def hasNext(self):
        pass
    
    @args(E, [])
    def next(self):
        pass
    
    def remove(self):
        pass


class List:

    __interfaces__ = [Collection] # Collection(E)
    
    @args(bool, [E])
    def add(self, object):
        pass
    
    @args(bool, [int, E])
    def add(self, location, obj):
        pass
    
    @args(bool, [Collection(E)])
    def addAll(self, collection):
        pass
    
    @args(bool, [int, Collection(E)])
    def addAll(self, location, collection):
        pass
    
    @args(void, [])
    def clear(self):
        pass
    
    @args(bool, [Object])
    def contains(self, object):
        pass
    
    @args(bool, [Collection(E)])
    def containsAll(self, collection):
        pass
    
    @args(bool, [Object])
    def equals(self, object):
        pass
    
    @args(E, [int])
    def get(self, location):
        pass
    
    @args(int, [])
    def hashCode(self):
        pass
    
    @args(int, [Object])
    def indexOf(self, object):
        pass
    
    @args(bool, [])
    def isEmpty(self):
        pass
    
    @args(Iterator(E), [])
    def iterator(self):
        pass
    
    @args(int, [Object])
    def lastIndexOf(self, object):
        pass
    
    @args(ListIterator(E), [int])
    def listIterator(self, location):
        pass
    
    @args(ListIterator(E), [])
    def listIterator(self):
        pass
    
    @args(E, [int])
    def remove(self, index):
        pass
    
    @args(bool, [Object])
    def remove(self, object):
        pass
    
    @args(bool, [Collection(E)])
    def removeAll(self, collection):
        pass
    
    @args(bool, [Collection(E)])
    def retainAll(self, collection):
        pass
    
    @args(E, [int, E])
    def set(self, index, object):
        pass
    
    @args(int, [])
    def size(self):
        pass
    
    @args(List(E), [int, int])
    def subList(self, start, end):
        pass
    
    @args([T], [[T]])
    def toArray(self, array):
        pass
    
    @args([Object], [])
    def toArray(self):
        pass


class LinkedList(AbstractSequentialList):

    __interfaces__ = [Serializable, Cloneable, Deque, List, Queue]
    __parameters__ = [E]
    
    def __init__(self):
        pass
    
    @args(void, [Collection(E)])
    def __init__(self, collection):
        pass
    
    @args(bool, [E])
    def add(self, object):
        pass
    
    @args(void, [int, E])
    def add(self, location, object):
        pass
    
    @args(bool, [Collection(E)])
    def addAll(self, collection):
        pass
    
    @args(bool, [int, Collection(E)])
    def addAll(self, location, collection):
        pass
    
    @args(void, [E])
    def addFirst(self, object):
        pass
    
    @args(void, [E])
    def addLast(self, object):
        pass
    
    @args(void, [])
    def clear(self):
        pass
    
    @args(Object, [])
    def clone(self):
        pass
    
    @args(bool, [Object])
    def contains(self, object):
        pass
    
    @args(Iterator(E), [])
    def descendingIterator(self):
        pass
    
    @args(E, [])
    def element(self):
        pass
    
    @args(E, [int])
    def get(self, location):
        pass
    
    @args(E, [])
    def getFirst(self):
        pass
    
    @args(E, [])
    def getLast(self):
        pass
    
    @args(int, [Object])
    def indexOf(self, object):
        pass
    
    @args(int, [Object])
    def lastIndexOf(self, object):
        pass
    
    @args(ListIterator(E), [int])
    def listIterator(self, location):
        pass
    
    @args(bool, [E])
    def offer(self, o):
        pass
    
    @args(bool, [E])
    def offerFirst(self, e):
        pass
    
    @args(bool, [E])
    def offerLast(self, e):
        pass
    
    @args(E, [])
    def peek(self):
        pass
    
    @args(E, [])
    def peekFirst(self):
        pass
    
    @args(E, [])
    def peekLast(self):
        pass
    
    @args(E, [])
    def poll(self):
        pass
    
    @args(E, [])
    def pollFirst(self):
        pass
    
    @args(E, [])
    def pollLast(self):
        pass
    
    @args(E, [])
    def pop(self):
        pass
    
    @args(void, [E])
    def push(self, e):
        pass
    
    @args(E, [])
    def remove(self):
        pass
    
    @args(E, [int])
    def remove(self, location):
        pass
    
    @args(bool, [Object])
    def remove(self, object):
        pass
    
    @args(E, [])
    def removeFirst(self):
        pass
    
    @args(bool, [Object])
    def removeFirstOccurrence(self, o):
        pass
    
    @args(E, [])
    def removeLast(self):
        pass
    
    @args(bool, [Object])
    def removeLastOccurrence(self, o):
        pass
    
    @args(E, [int, E])
    def set(self, location, object):
        pass
    
    @args(int, [])
    def size(self):
        pass
    
    @args([E], [[E]])
    def toArray(self, contents):
        pass
    
    # Note that the standard API declares that this returns an Object, but this
    # throws away information about the type of the items, so we use the E
    # parameter to return type information. The method is still encoded with
    # the correct signature.
    @args([E], [])
    def toArray(self):
        pass


class Locale(Object):

    __interfaces__ = [Serializable, Cloneable]
    
    __static_fields__ = {
        "CANADA": Locale,
        "CANADA_FRENCH": Locale,
        "CHINA": Locale,
        "CHINESE": Locale,
        "ENGLISH": Locale,
        "FRANCE": Locale,
        "FRENCH": Locale,
        "GERMAN": Locale,
        "GERMANY": Locale,
        "ITALIAN": Locale,
        "ITALY": Locale,
        "JAPAN": Locale,
        "JAPANESE": Locale,
        "KOREA": Locale,
        "KOREAN": Locale,
        "PRC": Locale,
        "ROOT": Locale,                 # API level 9
        "SIMPLIFIED_CHINESE": Locale,
        "TAIWAN": Locale,
        "TRADITIONAL_CHINESE": Locale,
        "UK": Locale,
        "US": Locale
        }
    
    # API level 21
    PRIVATE_USE_EXTENSION = 120
    UNICODE_LOCALE_EXTENSION = 117
    
    # API level 21
    class Builder(Object):
    
        def __init__(self):
            pass
        
        @api(21)
        @args(Locale.Builder, [String])
        def addUnicodeLocaleAttribute(self, attribute):
            pass
        
        @api(21)
        @args(Locale, [])
        def build(self):
            pass
        
        @api(21)
        @args(Locale.Builder, [])
        def clear(self):
            pass
        
        @api(21)
        @args(Locale.Builder, [])
        def clearExtensions(self):
            pass
        
        @api(21)
        @args(Locale.Builder, [String])
        def removeUnicodeLocaleAttribute(self, attribute):
            pass
        
        @api(21)
        @args(Locale.Builder, [char, String])
        def setExtension(self, key, value):
            pass
        
        @api(21)
        @args(Locale.Builder, [String])
        def setLanguage(self, language):
            pass
        
        @api(21)
        @args(Locale.Builder, [String])
        def setLanguageTag(self, languageTag):
            pass
        
        @api(21)
        @args(Locale.Builder, [Locale])
        def setLocale(self, locale):
            pass
        
        @api(21)
        @args(Locale.Builder, [String])
        def setRegion(self, region):
            pass
        
        @api(21)
        @args(Locale.Builder, [String])
        def setScript(self, script):
            pass
        
        @api(21)
        @args(Locale.Builder, [String, String])
        def setUnicodeLocaleKeyword(self, key, type):
            pass
        
        @api(21)
        @args(Locale.Builder, [String])
        def setVariant(self, variant):
            pass
    
    @args(void, [String])
    def __init__(self, language):
        pass
    
    @args(void, [String, String])
    def __init__(self, language, country):
        pass
    
    @args(void, [String, String, String])
    def __init__(self, language, country, variant):
        pass
    
    @args(Object, [])
    def clone(self):
        pass
    
    @args(bool, [Object])
    def equals(self, obj):
        pass
    
    @api(21)
    @static
    @args(Locale, [String])
    def forLanguageTag(languageTag):
        pass
    
    @static
    @args([Locale], [])
    def getAvailableLocales():
        pass
    
    @args(String, [])
    def getCountry(self):
        pass
    
    @static
    @args(Locale, [])
    def getDefault():
        pass
    
    @args(String, [Locale])
    def getDisplayCountry(self, locale):
        pass
    
    @final
    @args(String, [])
    def getDisplayCountry(self):
        pass
    
    @args(String, [Locale])
    def getDisplayLanguage(self, locale):
        pass
    
    @final
    @args(String, [])
    def getDisplayLanguage(self):
        pass
    
    @final
    @args(String, [])
    def getDisplayName(self):
        pass
    
    @args(String, [Locale])
    def getDisplayName(self, locale):
        pass
    
    @api(21)
    @args(String, [])
    def getDisplayScript(self):
        pass
    
    @api(21)
    @args(String, [Locale])
    def getDisplayScript(self, locale):
        pass
    
    @final
    @args(String, [])
    def getDisplayVariant(self):
        pass
    
    @args(String, [Locale])
    def getDisplayVariant(self, locale):
        pass
    
    @api(21)
    @args(String, [char])
    def getExtension(self, extensionKey):
        pass
    
    @api(21)
    @args(Set(Character), [])
    def getExtensionKeys(self):
        pass
    
    @args(String, [])
    def getISO3Country(self):
        pass
    
    @args(String, [])
    def getISO3Language(self):
        pass
    
    @static
    @args([String], [])
    def getISOCountries():
        pass
    
    @static
    @args([String], [])
    def getISOLanguages():
        pass
    
    @args(String, [])
    def getLanguage(self):
        pass
    
    @api(21)
    @args(String, [])
    def getScript(self):
        pass
    
    @api(21)
    @args(Set(String), [])
    def getUnicodeLocaleAttributes(self):
        pass
    
    @api(21)
    @args(Set(String), [])
    def getUnicodeLocaleKeys(self):
        pass
    
    @api(21)
    @args(String, [String])
    def getUnicodeLocaleType(self, keyWord):
        pass
    
    @args(String, [])
    def getVariant(self):
        pass
    
    @synchronized
    @args(int, [])
    def hashCode(self):
        pass
    
    @synchronized
    @static
    @args(void, [])
    def setDefault(locale):
        pass
    
    @api(21)
    @args(String, [])
    def toLanguageTag(self):
        pass
    
    @final
    @args(String, [])
    def toString(self):
        pass


class Map:

    __parameters__ = [K, V]
    
    class Entry:
    
        __parameters__ = [K, V]
        
        @args(bool, [])
        def equals(self):
            pass
        
        @args(K, [])
        def getKey(self):
            pass
        
        @args(V, [])
        def getValue(self):
            pass
        
        @args(int, [])
        def hashCode(self):
            pass
        
        @args(K, [V])
        def setValue(self, object):
            pass
    
    def clear(self):
        pass
    
    @args(bool, [Object])
    def containsKey(self, key):
        pass
    
    @args(bool, [Object])
    def containsValue(self, value):
        pass
    
    @args(Set(Map.Entry(K,V)), [])
    def entrySet(self):
        pass
    
    @args(bool, [Object])
    def equals(self, object):
        pass
    
    @args(V, [Object])
    def get(self, key):
        pass
    
    @args(int, [])
    def hashCode(self):
        pass
    
    @args(bool, [])
    def isEmpty(self):
        pass
    
    @args(Set(K), [])
    def keySet(self):
        pass
    
    @args(V, [K, V])
    def put(self, key, value):
        pass
    
    @args(void, [Map(K,V)])
    def putAll(self, map):
        pass
    
    @args(V, [Object])
    def remove(self, key):
        pass
    
    @args(int, [])
    def size(self):
        pass
    
    @args(Collection(V), [])
    def values(self):
        pass


class NoSuchElementException(RuntimeException):

    def __init__(self):
        pass
    
    @args(void, [String])
    def __init__(self, message):
        pass


class Queue:

    __interfaces__ = [Collection]
    __parameters__ = [E]
    
    @args(E, [])
    def element(self):
        pass
    
    @args(bool, [E])
    def offer(self, element):
        pass
    
    @args(E, [])
    def peek(self):
        pass
    
    @args(E, [])
    def poll(self):
        pass
    
    @args(E, [])
    def remove(self):
        pass


class Random(Object):

    __interfaces__ = [Serializable]
    
    def __init__(self):
        pass
    
    @args(void, [long])
    def __init__(self, seed):
        pass
    
    @args(bool, [])
    def nextBoolean(self):
        pass
    
    @args(void, [[byte]])
    def nextBytes(self):
        pass
    
    @args(double, [])
    def nextDouble(self):
        pass
    
    @args(float, [])
    def nextFloat(self):
        pass
    
    @args(double, [])
    def nextGaussian(self):
        pass
    
    @args(int, [int])
    def nextInt(self, n):
        pass
    
    @args(int, [])
    def nextInt(self):
        pass
    
    @args(long, [])
    def nextLong(self):
        pass
    
    @args(void, [long])
    def setSeed(self, seed):
        pass
    
    @args(int, [int])
    def next(self, bits):
        pass


class RandomAccess:

    pass


class Set:

    __interfaces__ = [Collection] # Collection(E)


class SortedMap:

    __interfaces__ = [Map]
    __parameters__ = [K, V]
    
    @args(K, [])
    def firstKey(self):
        pass
    
    @args(SortedMap(K, V), [K])
    def headMap(self, endKey):
        pass
    
    @args(K, [])
    def lastKey(self):
        pass
    
    @args(SortedMap(K, V), [K, K])
    def subMap(self, startKey, endKey):
        pass
    
    @args(SortedMap(K, V), [K])
    def tailMap(self, startKey):
        pass


class SortedSet:

    __interfaces__ = [Set]
    __parameters__ = [E]
    
    @args(Comparator(E), [])
    def comparator(self):
        pass
    
    @args(E, [])
    def first(self):
        pass
    
    @args(SortedSet(E), [E])
    def headSet(self, end):
        pass
    
    @args(E, [])
    def last(self):
        pass
    
    @args(SortedSet(E), [E, E])
    def subSet(self, start, end):
        pass
    
    @args(SortedSet(E), [E])
    def tailSet(self, start):
        pass


class Stack(Vector):

    __parameters__ = [E]
    
    def __init__(self):
        pass
    
    @args(bool, [])
    def empty(self):
        pass
    
    @synchronized
    @args(E, [])
    def peek(self):
        pass
    
    @synchronized
    @args(E, [])
    def pop(self):
        pass
    
    @args(E, [E])
    def push(self, object):
        pass
    
    @synchronized
    @args(int, [Object])
    def search(self, object):
        pass


class Timer(Object):

    @args(void, [String, bool])
    def __init__(self, name, isDaemon):
        pass
    
    @args(void, [String])
    def __init__(self, name):
        pass
    
    @args(void, [bool])
    def __init__(self, isDaemon):
        pass
    
    def __init__(self):
        pass
    
    def cancel(self):
        pass
    
    @args(int, [])
    def purge(self):
        pass
    
    @args(void, [TimerTask, Date, long])
    def schedule(self, task, when, period):
        pass
    
    @args(void, [TimerTask, long, long])
    def schedule(self, task, delay, period):
        pass
    
    @args(void, [TimerTask, Date])
    def schedule(self, task, when):
        pass
    
    @args(void, [TimerTask, long])
    def schedule(self, task, delay):
        pass
    
    @args(void, [TimerTask, long, long])
    def scheduleAtFixedRate(self, task, delay, period):
        pass
    
    @args(void, [TimerTask, Date, long])
    def scheduleAtFixedRate(self, task, when, period):
        pass


class TimerTask(Object):

    __interfaces__ = [Runnable]
    
    def __init__(self):
        pass
    
    @args(bool, [])
    def cancel(self):
        pass
    
    @abstract
    def run(self):
        pass
    
    @args(long, [])
    def scheduledExecutionTime(self):
        pass


class TimeZone:

    __interfaces__ = [Serializable, Cloneable]
    
    LONG = 1
    SHORT = 0
    
    def __init__(self):
        pass
    
    @args(Object, [])
    def clone(self):
        pass
    
    @synchronized
    @static
    @args([String], [])
    def getAvailableIDs():
        pass
    
    @synchronized
    @static
    @args([String], [int])
    def getAvailableIDs(offsetMillis):
        pass
    
    @args(int, [])
    def getDSTSavings(self):
        pass
    
    @synchronized
    @static
    @args(TimeZone, [])
    def getDefault():
        pass
    
    @final
    @args(String, [Locale])
    def getDisplayName(self, locale):
        pass
    
    @args(String, [bool, int, Locale])
    def getDisplayName(self, daylightTime, style, locale):
        pass
    
    @final
    @args(String, [])
    def getDisplayName(self):
        pass
    
    @final
    @args(String, [bool, int])
    def getDisplayName(self, daylightTime, style):
        pass
    
    @args(String, [])
    def getID(self):
        pass
    
    @abstract
    @args(int, [int, int, int, int, int, int])
    def getOffset(self, era, year, month, day, dayOfWeek, timeOfDayMillis):
        pass
    
    @args(int, [long])
    def getOffset(self, time):
        pass
    
    @abstract
    @args(int, [])
    def getRawOffset(self):
        pass
    
    @synchronized
    @static
    @args(TimeZone, [String])
    def getTimeZone(id):
        pass
    
    @args(bool, [TimeZone])
    def hasSameRules(self, timeZone):
        pass
    
    @abstract
    @args(bool, [Date])
    def inDaylightTime(self, time):
        pass
    
    @synchronized
    @static
    @args(void, [TimeZone])
    def setDefault(timeZone):
        pass
    
    @args(void, [String])
    def setID(self, id):
        pass
    
    @abstract
    @args(void, [int])
    def setRawOffset(self, offsetMillis):
        pass
    
    @abstract
    @args(bool, [])
    def useDaylightTime(self):
        pass


class UUID(Object):

    __interfaces__ = [Serializable, Comparable] # Comparable<UUID>
    
    @args(void, [long, long])
    def __init__(self, mostSigBits, leastSigBits):
        pass
    
    @args(int, [])
    def clockSequence(self):
        pass
    
    @args(int, [UUID])
    def compareTo(self, uuid):
        pass
    
    @args(bool, [Object])
    def equals(self, object):
        pass
    
    @static
    @args(UUID, [String])
    def fromString(uuid):
        pass
    
    @args(long, [])
    def getLeastSignificantBits(self):
        pass
    
    @args(long, [])
    def getMostSignificantBits(self):
        pass
    
    @args(int, [])
    def hashCode(self):
        pass
    
    @static
    @args(UUID, [[byte]])
    def nameUUIDFromBytes(name):
        pass
    
    @args(long, [])
    def node(self):
        pass
    
    @static
    @args(UUID, [])
    def randomUUID():
        pass
    
    @args(long, [])
    def timestamp(self):
        pass
    
    @args(String, [])
    def toString(self):
        pass
    
    @args(int, [])
    def variant(self):
        pass
    
    @args(int, [])
    def version(self):
        pass


class Vector(AbstractList):

    __interfaces__ = [List, RandomAccess, Cloneable, Serializable] # List(E)
    
    __fields__ = {
        "capacityIncrement": int,
        "elementCount": int,
        "elementData": [Object]
        }
    
    __parameters__ = [E]
    
    def __init__(self):
        pass
    
    @args(void, [int])
    def __init__(self, capacity):
        pass
    
    @args(void, [int, int])
    def __init__(self, capacity, capacityIncrement):
        pass
    
    @args(void, [Collection(E)])
    def __init__(self, collection):
        pass
    
    @synchronized
    @args(bool, [E])
    def add(self, object):
        pass
    
    @args(void, [int, E])
    def add(self, location, object):
        pass
    
    @synchronized
    @args(bool, [Collection(E)])
    def addAll(self, collection):
        pass
    
    @synchronized
    @args(bool, [int, Collection(E)])
    def addAll(self, location, collection):
        pass
    
    @synchronized
    @args(void, [E])
    def addElement(self, object):
        pass
    
    @synchronized
    @args(int, [])
    def capacity(self):
        pass
    
    def clear(self):
        pass
    
    @synchronized
    @args(Object, [])
    def clone(self):
        pass
    
    @args(bool, [Object])
    def contains(self, object):
        pass
    
    @synchronized
    @args(bool, [Collection(E)])
    def containsAll(self, collection):
        pass
    
    @synchronized
    @args(void, [[Object]])
    def copyInto(self, elements):
        pass
    
    @synchronized
    @args(E, [int])
    def elementAt(self, location):
        pass
    
    @args(Enumeration(E), [])
    def elements(self):
        pass
    
    @synchronized
    @args(void, [int])
    def ensureCapacity(self, minimumCapacity):
        pass
    
    @synchronized
    @args(bool, [Object])
    def equals(self, object):
        pass
    
    @synchronized
    @args(E, [])
    def firstElement(self):
        pass
    
    @args(E, [int])
    def get(self, location):
        pass
    
    @synchronized
    @args(int, [])
    def hashCode(self):
        pass
    
    @synchronized
    @args(int, [Object, int])
    def indexOf(self, object, location):
        pass
    
    @args(int, [Object])
    def indexOf(self, object):
        pass
    
    @synchronized
    @args(void, [E, int])
    def insertElementAt(self, object, location):
        pass
    
    @synchronized
    @args(bool, [])
    def isEmpty(self):
        pass
    
    @synchronized
    @args(E, [])
    def lastElement(self):
        pass
    
    @synchronized
    @args(int, [Object, int])
    def lastIndexOf(self, object, location):
        pass
    
    @synchronized
    @args(int, [Object])
    def lastIndexOf(self, object):
        pass
    
    @synchronized
    @args(E, [int])
    def remove(self, location):
        pass
    
    @args(bool, [Object])
    def remove(self, object):
        pass
    
    @synchronized
    @args(bool, [Collection(E)])
    def removeAll(self, collection):
        pass
    
    @synchronized
    def removeAllElements(self):
        pass
    
    @synchronized
    @args(bool, [Object])
    def removeElement(self, object):
        pass
    
    @synchronized
    @args(void, [int])
    def removeElementAt(self, location):
        pass
    
    @synchronized
    @args(bool, [Collection(E)])
    def retainAll(self, collection):
        pass
    
    @synchronized
    @args(E, [int, E])
    def set(self, location, object):
        pass
    
    @synchronized
    @args(void, [E, int])
    def setElementAt(self, object, location):
        pass
    
    @synchronized
    @args(void, [int])
    def setSize(self, length):
        pass
    
    @synchronized
    @args(int, [])
    def size(self):
        pass
    
    @synchronized
    @args(List(E), [int, int])
    def subList(self, start, end):
        pass
    
    @synchronized
    @args([E], [[E]])
    def toArray(self, contents):
        pass
    
    @synchronized
    @args([Object], [])
    def toArray(self):
        pass
    
    @synchronized
    @args(String, [])
    def toString(self):
        pass
    
    @synchronized
    def trimToSize(self):
        pass

    @protected
    @args(void, [int, int])
    def removeRange(self, start, end):
        pass
