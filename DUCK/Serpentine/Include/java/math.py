# Class declarations obtained from Android API documentation licensed under the
# Apache 2.0 license (http://www.apache.org/licenses/LICENSE-2.0) and rewritten
# in syntax that the Python compiler module can parse.

__package__ = "java.math"

from java.lang import Comparable, Number
from java.io import Serializable

class BigInteger(Number):

    __static_fields__ = {
        "ONE": BigInteger,
        "TEN": BigInteger,
        "ZERO": BigInteger
        }
    
    ### ...
