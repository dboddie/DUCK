# Class declarations obtained from Android API documentation licensed under the
# Apache 2.0 license (http://www.apache.org/licenses/LICENSE-2.0) and rewritten
# in syntax that the Python compiler module can parse.

__package__ = "java.nio"

from java.lang import Appendable, CharSequence, Comparable, Object, Readable, \
                      RuntimeException, String
from java.io import AutoCloseable, Closeable
from java.util import Locale, Set, SortedMap

class Buffer(Object):

    @abstract
    @args(Object, [])
    def array(self):
        pass
    
    @abstract
    @args(int, [])
    def arrayOffset(self):
        pass
    
    @args(int, [])
    def capacity(self):
        pass
    
    @args(Buffer, [])
    def clear(self):
        pass
    
    @args(Buffer, [])
    def flip(self):
        pass
    
    @abstract
    @args(bool, [])
    def hasArray(self):
        pass
    
    @args(bool, [])
    def hasRemaining(self):
        pass
    
    @abstract
    @args(bool, [])
    def isDirect(self):
        pass
    
    @abstract
    @args(bool, [])
    def isReadOnly(self):
        pass
    
    @args(int, [])
    def limit(self):
        pass
    
    @args(int, [int])
    def limit(self, newLimit):
        pass
    
    @args(Buffer, [])
    def mark(self):
        pass
    
    @args(Buffer, [int])
    def position(self, newPosition):
        pass
    
    @args(int, [])
    def position(self):
        pass
    
    @args(int, [])
    def remaining(self):
        pass
    
    @args(Buffer, [])
    def reset(self):
        pass
    
    @args(Buffer, [])
    def rewind(self):
        pass


class BufferUnderflowException(RuntimeException):

    def __init__(self):
        pass


class ByteBuffer(Buffer):

    __interfaces__ = [Comparable]
    
    @static
    @args(ByteBuffer, [int])
    def allocate(capacity):
        pass
    
    @static
    @args(ByteBuffer, [int])
    def allocateDirect(capacity):
        pass
    
    @args([byte], [])
    def array(self):
        pass
    
    @abstract
    @args(CharBuffer, [])
    def asCharBuffer(self):
        pass
    
    @abstract
    @args(DoubleBuffer, [])
    def asDoubleBuffer(self):
        pass
    
    @abstract
    @args(FloatBuffer, [])
    def asFloatBuffer(self):
        pass
    
    @abstract
    @args(IntBuffer, [])
    def asIntBuffer(self):
        pass
    
    @abstract
    @args(LongBuffer, [])
    def asLongBuffer(self):
        pass
    
    @abstract
    @args(ByteBuffer, [])
    def asReadOnlyBuffer(self):
        pass
    
    @abstract
    @args(ShortBuffer, [])
    def asShortBuffer(self):
        pass
    
    @abstract
    @args(ByteBuffer, [])
    def compact(self):
        pass
    
    @args(int, [ByteBuffer])
    def compareTo(self, otherBuffer):
        pass
    
    @abstract
    @args(ByteBuffer, [])
    def duplicate(self):
        pass
    
    @abstract
    @args(byte, [])
    def get(self):
        pass
    
    @args(ByteBuffer, [[byte], int, int])
    def get(self, array, destOffset, byteCount):
        pass
    
    @args(ByteBuffer, [[byte]])
    def get(self, array):
        pass
    
    @abstract
    @args(byte, [int])
    def get(self, index):
        pass
    
    @abstract
    @args(char, [])
    def getChar(self):
        pass
    
    @abstract
    @args(char, [int])
    def getChar(self, index):
        pass
    
    @abstract
    @args(double, [])
    def getDouble(self):
        pass
    
    @abstract
    @args(double, [int])
    def getDouble(self, index):
        pass
    
    @abstract
    @args(float, [])
    def getFloat(self):
        pass
    
    @abstract
    @args(float, [int])
    def getFloat(self, index):
        pass
    
    @abstract
    @args(int, [])
    def getInt(self):
        pass
    
    @abstract
    @args(int, [int])
    def getInt(self, index):
        pass
    
    @abstract
    @args(long, [])
    def getLong(self):
        pass
    
    @abstract
    @args(long, [int])
    def getLong(self, index):
        pass
    
    @abstract
    @args(short, [])
    def getShort(self):
        pass
    
    @abstract
    @args(short, [int])
    def getShort(self, index):
        pass
    
    @args(bool, [])
    def hasArray(self):
        pass
    
    @abstract
    @args(bool, [])
    def isDirect(self):
        pass
    
    @args(ByteBuffer, [ByteOrder])
    def order(self, byteOrder):
        pass
    
    @args(ByteOrder, [])
    def order(self):
        pass
    
    @abstract
    @args(ByteBuffer, [byte])
    def put(self, b):
        pass
    
    @args(ByteBuffer, [[byte], int, int])
    def put(self, src, srcOffset, byteCount):
        pass
    
    @abstract
    @args(ByteBuffer, [int, byte])
    def put(self, index, b):
        pass
    
    @args(ByteBuffer, [[byte]])
    def put(self, src):
        pass
    
    @args(ByteBuffer, [ByteBuffer])
    def put(self, src):
        pass
    
    @abstract
    @args(ByteBuffer, [int, char])
    def putChar(self, index, value):
        pass
    
    @abstract
    @args(ByteBuffer, [char])
    def putChar(self, value):
        pass
    
    @abstract
    @args(ByteBuffer, [int, double])
    def putDouble(self, index, value):
        pass
    
    @abstract
    @args(ByteBuffer, [double])
    def putDouble(self, value):
        pass
    
    @abstract
    @args(ByteBuffer, [int, float])
    def putFloat(self, index, value):
        pass
    
    @abstract
    @args(ByteBuffer, [float])
    def putFloat(self, value):
        pass
    
    @abstract
    @args(ByteBuffer, [int, int])
    def putInt(self, index, value):
        pass
    
    @abstract
    @args(ByteBuffer, [int])
    def putInt(self, value):
        pass
    
    @abstract
    @args(ByteBuffer, [int, long])
    def putLong(self, index, value):
        pass
    
    @abstract
    @args(ByteBuffer, [long])
    def putLong(self, value):
        pass
    
    @abstract
    @args(ByteBuffer, [int, short])
    def putShort(self, index, value):
        pass
    
    @abstract
    @args(ByteBuffer, [short])
    def putShort(self, value):
        pass
    
    @abstract
    @args(ByteBuffer, [])
    def slice(self):
        pass
    
    @static
    @args(ByteBuffer, [[byte]])
    def wrap(array):
        pass
    
    @static
    @args(ByteBuffer, [[byte], int, int])
    def wrap(array, start, byteCount):
        pass


class ByteOrder(Object):

    __fields__ = {"BIG_ENDIAN": ByteOrder,
                  "LITTLE_ENDIAN": ByteOrder}
    
    @static
    @args(ByteOrder, [])
    def nativeOrder():
        pass


class CharBuffer(Buffer):

    __interfaces__ = [Comparable, CharSequence, Appendable, Readable]
    
    @static
    @args(CharBuffer, [int])
    def allocate(capacity):
        pass
    
    @final
    @args([char], [])
    def array(self):
        pass
    
    @final
    @args(int, [])
    def arrayOffset(self):
        pass
    
    @abstract
    @args(CharBuffer, [])
    def asReadOnlyBuffer(self):
        pass
    
    @final
    @args(char, [int])
    def charAt(self, index):
        pass
    
    @abstract
    @args(CharBuffer, [])
    def compact(self):
        pass
    
    @args(int, [CharBuffer])
    def compareTo(self, other):
        pass
    
    @abstract
    @args(CharBuffer, [])
    def duplicate(self):
        pass
    
    @args(CharBuffer, [[char], int, int])
    def get(self, dest, destOffset, byteCount):
        pass
    
    @abstract
    @args(char, [])
    def get(self):
        pass
    
    @abstract
    @args(char, [int])
    def get(self, index):
        pass
    
    @args(CharBuffer, [[char]])
    def get(self, dest):
        pass
    
    @final
    @args(CharBuffer, [String])
    def put(self, s):
        pass
    
    @abstract
    @args(CharBuffer, [char])
    def put(self, c):
        pass
    
    @args(CharBuffer, [String, int, int])
    def put(self, s, start, end):
        pass
    
    @abstract
    @args(CharBuffer, [int, char])
    def put(self, s, index, c):
        pass
    
    @args(CharBuffer, [CharBuffer])
    def put(self, source):
        pass
    
    @final
    @args(CharBuffer, [[char]])
    def put(self, source):
        pass
    
    @args(CharBuffer, [[char], int, int])
    def put(self, source, sourceOffset, charCount):
        pass
    
    @args(int, [CharBuffer])
    def read(self, target):
        pass
    
    @abstract
    @args(CharBuffer, [])
    def slice(self):
        pass
    
    @abstract
    @args(CharBuffer, [int, int])
    def subSequence(self, start, end):
        pass
    
    @static
    @args(CharBuffer, [CharSequence, int, int])
    def wrap(charSequence, start, end):
        pass
    
    @static
    @args(CharBuffer, [CharSequence])
    def wrap(charSequence):
        pass
    
    @static
    @args(CharBuffer, [[char], int, int])
    def wrap(charArray, start, charCount):
        pass
    
    @static
    @args(CharBuffer, [[char]])
    def wrap(charArray):
        pass


class FloatBuffer(Buffer):

    __interfaces__ = [Comparable]
    
    @static
    @args(FloatBuffer, [int])
    def allocate(capacity):
        pass
    
    @final
    @args([float], [])
    def array(self):
        pass
    
    @final
    @args(int, [])
    def arrayOffset(self):
        pass
    
    @abstract
    @args(FloatBuffer, [])
    def asReadOnlyBuffer(self):
        pass
    
    @abstract
    def compact(self):
        pass
    
    @abstract
    @args(FloatBuffer, [])
    def duplicate(self):
        pass
    
    @abstract
    @args(float, [int])
    def get(self, index):
        pass
    
    @args(FloatBuffer, [[float], int, int])
    def get(self, dest, destOffset, floatCount):
        pass
    
    @abstract
    @args(float, [])
    def get(self):
        pass
    
    @args(FloatBuffer, [[float]])
    def get(self, dest):
        pass
    
    @abstract
    @args(FloatBuffer, [int, float])
    def put(self, index, f):
        pass
    
    @args(FloatBuffer, [[float], int, int])
    def put(self, src, srcOffset, floatCount):
        pass
    
    @args(FloatBuffer, [FloatBuffer])
    def put(self, src):
        pass
    
    @final
    @args(FloatBuffer, [[float]])
    def put(self, src):
        pass
    
    @abstract
    @args(FloatBuffer, [float])
    def put(self, f):
        pass
    
    @abstract
    @args(FloatBuffer, [])
    def slice(self):
        pass
    
    @static
    @args(FloatBuffer, [[float]])
    def wrap(array):
        pass
    
    @static
    @args(FloatBuffer, [[float], int, int])
    def wrap(array, start, floatCount):
        pass


class MappedByteBuffer(ByteBuffer):

    @final
    @args(MappedByteBuffer, [])
    def force(self):
        pass
    
    @final
    @args(bool, [])
    def isLoaded(self):
        pass
    
    @final
    @args(MappedByteBuffer, [])
    def load(self):
        pass


class ShortBuffer(Buffer):

    __interfaces__ = [Comparable]
    
    @static
    @args(ShortBuffer, [int])
    def allocate(capacity):
        pass
    
    @final
    @args([short], [])
    def array(self):
        pass
    
    @final
    @args(int, [])
    def arrayOffset(self):
        pass
    
    @abstract
    @args(ShortBuffer, [])
    def asReadOnlyBuffer(self):
        pass
    
    @abstract
    def compact(self):
        pass
    
    @abstract
    @args(ShortBuffer, [])
    def duplicate(self):
        pass
    
    @abstract
    @args(short, [int])
    def get(self, index):
        pass
    
    @args(ShortBuffer, [[short], int, int])
    def get(self, dest, destOffset, shortCount):
        pass
    
    @args(ShortBuffer, [[short]])
    def get(self, dest):
        pass
    
    @abstract
    @args(short, [])
    def get(self):
        pass
    
    @args(ShortBuffer, [[short], int, int])
    def put(self, src, srcOffset, shortCount):
        pass
    
    @final
    @args(ShortBuffer, [[short]])
    def put(self, src):
        pass
    
    @abstract
    @args(ShortBuffer, [short])
    def put(self, f):
        pass
    
    @args(ShortBuffer, [ShortBuffer])
    def put(self, src):
        pass
    
    @abstract
    @args(ShortBuffer, [int, short])
    def put(self, index, f):
        pass
    
    @abstract
    @args(ShortBuffer, [])
    def slice(self):
        pass
    
    @static
    @args(ShortBuffer, [[short]])
    def wrap(array):
        pass
    
    @static
    @args(ShortBuffer, [[short], int, int])
    def wrap(array, start, shortCount):
        pass
