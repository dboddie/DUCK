# Class declarations obtained from Android API documentation licensed under the
# Apache 2.0 license (http://www.apache.org/licenses/LICENSE-2.0) and rewritten
# in syntax that the Python compiler module can parse.

__package__ = "java.nio.channels"

from java.io import Closeable, InputStream, OutputStream, Reader, Writer
from java.lang import Object, String
from java.nio import ByteBuffer
from java.nio.channels.spi import AbstractInterruptibleChannel, \
    AbstractSelectableChannel

class ByteChannel:

    __interfaces__ = [ReadableByteChannel, WritableByteChannel]


class Channel:

    __interfaces__ = [Closeable]
    
    def close(self):
        pass
    
    @args(bool, [])
    def isOpen(self):
        pass


class Channels(Object):

    @static
    @args(WritableByteChannel, [OutputStream])
    def newChannel(outputStream):
        pass
    
    @static
    @args(ReadableByteChannel, [InputStream])
    def newChannel(inputStream):
        pass
    
    @static
    @args(InputStream, [ReadableByteChannel])
    def newInputStream(channel):
        pass
    
    @static
    @args(OutputStream, [WritableByteChannel])
    def newOutputStream(channel):
        pass
    
    @static
    @args(Reader, [ReadableByteChannel, CharsetDecoder, int])
    def newReader(channel, decoder, minBufferCapacity):
        pass
    
    @static
    @args(Reader, [ReadableByteChannel, String])
    def newReader(channel, charsetName):
        pass
    
    @static
    @args(Writer, [WritableByteChannel, String])
    def newWriter(channel, charsetName):
        pass
    
    @static
    @args(Writer, [WritableByteChannel, CharsetEncoder, int])
    def newWriter(channel, encoder, minBufferCapacity):
        pass


class FileChannel(AbstractInterruptibleChannel):

    __interfaces__ = [ByteChannel, GatheringByteChannel, ScatteringByteChannel]
    
    class MapMode(Object):
    
        __static_fields__ = {
            "PRIVATE": FileChannel.MapMode,
            "READ_ONLY": FileChannel.MapMode,
            "READ_WRITE": FileChannel.MapMode
            }
        
        @args(String, [])
        def toString(self):
            pass
    
    @protected
    def __init__(self):
        pass
    
    @abstract
    @args(void, [bool])
    def force(self, metadata):
        pass
    
    @final
    @args(FileLock, [])
    def lock(self):
        pass
    
    @abstract
    @args(FileLock, [long, long, bool])
    def lock(self, position, size, shared):
        pass
    
    @abstract
    @args(MappedByteBuffer, [FileChannel.MapMode, long, long])
    def map(self, mode, position, size):
        pass
    
    @abstract
    @args(long, [])
    def position(self):
        pass
    
    @abstract
    @args(FileChannel, [long])
    def position(self, newPosition):
        pass
    
    @abstract
    @args(int, [ByteBuffer, long])
    def read(self, buffer, position):
        pass
    
    @abstract
    @args(int, [ByteBuffer])
    def read(self, buffer):
        pass
    
    @abstract
    @args(long, [[ByteBuffer], int, int])
    def read(self, buffers, start, number):
        pass
    
    @final
    @args(long, [[ByteBuffer]])
    def read(self, buffers):
        pass
    
    @abstract
    @args(long, [])
    def size(self):
        pass
    
    @abstract
    @args(long, [ReadableByteChannel, long, long])
    def transferFrom(self, src, position, count):
        pass
    
    @abstract
    @args(long, [long, long, WritableByteChannel])
    def transferTo(self, position, count, target):
        pass
    
    @abstract
    @args(FileChannel, [long])
    def truncate(self, size):
        pass
    
    @abstract
    @args(FileLock, [long, long, bool])
    def tryLock(self, position, size, shared):
        pass
    
    @final
    @args(FileLock, [])
    def tryLock(self):
        pass
    
    @final
    @args(long, [[ByteBuffer]])
    def write(self, buffers):
        pass
    
    @abstract
    @args(long, [[ByteBuffer], int, int])
    def write(self, buffers, offset, length):
        pass
    
    @abstract
    @args(int, [ByteBuffer])
    def write(self, src):
        pass
    
    @abstract
    @args(int, [ByteBuffer, long])
    def write(self, buffer, position):
        pass


class FileLock(Object):

    __interfaces__ = [AutoCloseable]
    
    @protected
    @args(void, [FileChannel, long, long, bool])
    def __init__(self, channel, position, size, shared):
        pass
    
    @final
    @args(FileChannel, [])
    def channel(self):
        pass
    
    @final
    def close(self):
        pass
    
    @final
    @args(bool, [])
    def isShared(self):
        pass
    
    @abstract
    @args(bool, [])
    def isValid(self):
        pass
    
    @final
    @args(bool, [long, long])
    def overlaps(self, start, length):
        pass
    
    @final
    @args(long, [])
    def position(self):
        pass
    
    @abstract
    def release(self):
        pass
    
    @final
    @args(long, [])
    def size(self):
        pass
    
    @final
    @args(String, [])
    def toString(self):
        pass


class GatheringByteChannel:

    __interfaces__ = [WriteableByteChannel]
    
    @args(long, [[ByteBuffer], int, int])
    def write(self, buffers, offset, length):
        pass
    
    @args(long, [[ByteBuffer]])
    def write(self, buffers):
        pass


class InterruptibleChannel:

    __interfaces__ = [Channel]
    
    def close(self):
        pass


class ReadableByteChannel:

    __interfaces__ = [Channel]
    
    @args(int, [ByteBuffer])
    def read(self, buffer):
        pass


class ScatteringByteChannel:

    __interfaces__ = [ReadableByteChannel]
    
    @args(long, [[ByteBuffer], int, int])
    def read(self, buffers, offset, length):
        pass
    
    @args(long, [[ByteBuffer]])
    def read(self, buffers):
        pass


class SelectableChannel(AbstractInterruptibleChannel):

    __interfaces__ = [Channel]
    
    @protected
    def __init__(self):
        pass
    
    @abstract
    @args(Object, [])
    def blockingLock(self):
        pass
    
    @abstract
    @args(SelectableChannel, [bool])
    def configureBlocking(self, block):
        pass
    
    @abstract
    @args(bool, [])
    def isBlocking(self):
        pass
    
    @abstract
    @args(bool, [])
    def isRegistered(self):
        pass
    
    @abstract
    @args(SelectionKey, [Selector])
    def keyFor(self, selector):
        pass
    
    @abstract
    @args(SelectorProvider, [])
    def provider(self):
        pass
    
    @final
    @args(SelectionKey, [Selector, int])
    def register(self, selector, operations):
        pass
    
    @abstract
    @args(SelectionKey, [Selector, int, Object])
    def register(self, selector, ops, att):
        pass
    
    @abstract
    @args(int, [])
    def validOps(self):
        pass


class SelectionKey(Object):

    OP_ACCEPT  = 0x10
    OP_CONNECT = 0x08
    OP_READ    = 0x01
    OP_WRITE   = 0x04
    
    @protected
    def __init__(self):
        pass
    
    @final
    @args(Object, [Object])
    def attach(self, anObject):
        pass
    
    @final
    @args(Object, [])
    def attachment(self):
        pass
    
    @abstract
    def cancel(self):
        pass
    
    @abstract
    @args(SelectableChannel, [])
    def channel(self):
        pass
    
    @abstract
    @args(int, [])
    def interestOps(self):
        pass
    
    @abstract
    @args(SelectionKey, [int])
    def interestOps(self, operations):
        pass
    
    @final
    @args(bool, [])
    def isAcceptable(self):
        pass
    
    @final
    @args(bool, [])
    def isConnectable(self):
        pass
    
    @final
    @args(bool, [])
    def isReadable(self):
        pass
    
    @abstract
    @args(bool, [])
    def isValid(self):
        pass
    
    @final
    @args(bool, [])
    def isWritable(self):
        pass
    
    @abstract
    @args(int, [])
    def readyOps(self):
        pass
    
    @abstract
    @args(Selector, [])
    def selector(self):
        pass


class ServerSocketChannel(AbstractSelectableChannel):

    @protected
    def __init__(self):
        pass
    
    @abstract
    @args(SocketChannel, [])
    def accept(self):
        pass
    
    @static
    @args(ServerSocketChannel, [])
    def open(self):
        pass
    
    @abstract
    @args(ServerSocket, [])
    def socket(self):
        pass
    
    @final
    @args(int, [])
    def validOps(self):
        pass


class WriteableByteChannel:

    __interfaces__ = [Channel]
    
    @args(int, [ByteBuffer])
    def write(self, buffer):
        pass
