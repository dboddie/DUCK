# Class declarations obtained from Android API documentation licensed under the
# Apache 2.0 license (http://www.apache.org/licenses/LICENSE-2.0) and rewritten
# in syntax that the Python compiler module can parse.

__package__ = "java.nio.charset"

from java.lang import Comparable, Object, String
from java.util import Locale, Set, SortedMap
from java.nio import ByteBuffer, CharBuffer

class Charset(Object):

    __interfaces__ = [Comparable]
    
    @args(void, [String, [String]])
    def __init__(self, canonicalName, aliases):
        pass
    
    @final
    @args(Set(String), [])
    def aliases(self):
        pass
    
    @static
    @args(SortedMap(String, Charset), [])
    def availableCharsets():
        pass
    
    @args(bool, [])
    def canEncode(self):
        pass
    
    @final
    @args(int, [Charset])
    def compareTo(self, other):
        pass
    
    @abstract
    @args(bool, [Charset])
    def contains(self, other):
        pass
    
    @final
    @args(CharBuffer, [ByteBuffer])
    def decode(self, buffer):
        pass
    
    @static
    @args(Charset, [])
    def defaultCharset():
        pass
    
    @args(String, [])
    def displayName(self):
        pass
    
    @args(String, [Locale])
    def displayName(self, locale):
        pass
    
    @final
    @args(ByteBuffer, [CharBuffer])
    def encode(self, buffer):
        pass
    
    @final
    @args(ByteBuffer, [String])
    def encode(self, s):
        pass
    
    @static
    @args(Charset, [String])
    def forName(charsetName):
        pass
    
    @final
    @args(bool, [])
    def isRegistered(self):
        pass
    
    @static
    @args(bool, [String])
    def isSupported(charsetName):
        pass
    
    @final
    @args(String, [])
    def name(self):
        pass
