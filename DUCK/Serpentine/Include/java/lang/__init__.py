# Class declarations obtained from Android API documentation licensed under the
# Apache 2.0 license (http://www.apache.org/licenses/LICENSE-2.0) and rewritten
# in syntax that the Python compiler module can parse.

__package__ = "java.lang"

from java.io import InputStream, OutputStream, PrintStream, Serializable
from java.lang.annotation import Annotation
from java.lang.reflect import AnnotatedElement, Constructor, Field, \
                              GenericDeclaration, Method, Type
from java.nio import CharBuffer
from java.nio.charset import Charset
from java.util import Iterator, List, Locale, Map

class Appendable:

    @args(Appendable, [CharSequence])
    def append(self, charSequence):
        pass
    
    @args(Appendable, [CharSequence, int, int])
    def append(self, charSequence, start, end):
        pass
    
    @args(Appendable, [char])
    def append(self, c):
        pass


class ArithmeticException(RuntimeException):

    def __init__(self):
        pass
    
    @args(void, [String])
    def __init__(self, detailMessage):
        pass


class ArrayIndexOutOfBoundsException(IndexOutOfBoundsException):

    def __init__(self):
        pass
    
    @args(void, [int])
    def __init__(self, index):
        pass
    
    @args(void, [String])
    def __init__(self, detailMessage):
        pass


class Boolean(Object):

    __interfaces__ = [Serializable, Comparable]
    __fields__ = {"FALSE": Boolean, "TRUE": Boolean}
    
    @args(void, [String])
    def __init__(self, string):
        pass
    
    @args(void, [bool])
    def __init__(self, value):
        pass
    
    @args(bool, [])
    def booleanValue(self):
        pass
    
    @static
    @args(int, [bool, bool])
    def compare(bool1, bool2):
        pass
    
    @args(int, [Boolean])
    def compareTo(self, that):
        pass
    
    @static
    @args(bool, [String])
    def getBoolean(string):
        pass
    
    @static
    @args(bool, [String])
    def parseBoolean(string):
        pass
    
    @static
    @args(String, [bool])
    def toString(value):
        pass
    
    @static
    @args(Boolean, [String])
    def valueOf(string):
        pass
    
    @static
    @args(Boolean, [bool])
    def valueOf(value):
        pass


class Byte(Number):

    __static_fields__ = {"TYPE": Class} # Class(Byte)
    
    MAX_VALUE = 127
    MIN_VALUE = -128
    SIZE = 8
    
    @args(void, [byte])
    def __init__(self, value):
        pass
    
    @args(void, [String])
    def __init__(self, string):
        pass
    
    @static
    @args(int, [byte, byte])
    def compare(value1, value2):
        pass
    
    @static
    @args(Byte, [String])
    def decode(string):
        pass
    
    @static
    @args(byte, [String, int])
    def parseByte(string, radix):
        pass
    
    @static
    @args(byte, [String])
    def parseByte(string):
        pass
    
    @static
    @args(String, [byte])
    def toString(value):
        pass
    
    @static
    @args(Byte, [String, int])
    def valueOf(string, radix):
        pass
    
    @static
    @args(Byte, [byte])
    def valueOf(value):
        pass
    
    @static
    @args(Byte, [String])
    def valueOf(string):
        pass


class CharSequence:

    @abstract
    @args(char, [int])
    def charAt(self, index):
        pass
    
    @abstract
    @args(int, [])
    def length(self):
        pass
    
    @abstract
    @args(CharSequence, [int, int])
    def subSequence(self, start, end):
        pass
    
    @abstract
    @args(String, [])
    def toString(self):
        pass


class Class(Object):

    __interfaces__ = [Serializable, AnnotatedElement, GenericDeclaration, Type]
    #__parameters__ = [A, T, U]
    
    @args(U, [Class(U)])
    def asSubclass(self, c):
        pass
    
    @args(T, [Object])
    def cast(self, obj):
        pass
    
    @args(bool, [])
    def desiredAssertionStatus(self):
        pass
    
    @static
    @args(Class, [String, bool, ClassLoader])
    def forName(className, shouldInitialize, classLoader):
        pass
    
    @static
    @args(Class, [String])
    def forName(className):
        pass
    
    @args(A, [Class(A)])
    def getAnnotation(self, annotationType):
        pass
    
    @args([Annotation], [])
    def getAnnotations(self):
        pass
    
    @args(String, [])
    def getCanonicalName(self):
        pass
    
    @args(ClassLoader, [])
    def getClassLoader(self):
        pass
    
    @args([Class], [])
    def getClasses(self):
        pass
    
    @args(Class, [])
    def getComponentType(self):
        pass
    
    @args(Constructor(T), [[Class]])
    def getConstructor(self, parameterTypes):
        pass
    
    @args([Constructor], [])
    def getConstructors(self):
        pass
    
    @args([Annotation], [])
    def getDeclaredAnnotations(self):
        pass
    
    @args([Class], [])
    def getDeclaredClasses(self):
        pass
    
    @args(Constructor(T), [[Class]])
    def getDeclaredConstructor(self, parameterTypes):
        pass
    
    @args([Constructor], [])
    def getDeclaredConstructors(self):
        pass
    
    @args(Field, [String])
    def getDeclaredField(self, name):
        pass
    
    @args([Field], [])
    def getDeclaredFields(self):
        pass
    
    @args(Method, [String, [Class]])
    def getDeclaredMethod(self, name, parameterTypes):
        pass
    
    @args([Method], [])
    def getDeclaredMethods(self):
        pass
    
    @args(Class, [])
    def getDeclaringClass(self):
        pass
    
    @args(Class, [])
    def getEnclosingClass(self):
        pass
    
    @args(Constructor, [])
    def getEnclosingConstructor(self):
        pass
    
    @args(Method, [])
    def getEnclosingMethod(self):
        pass
    
    @args([T], [])
    def getEnumConstants(self):
        pass
    
    @args(Field, [String])
    def getField(self, name):
        pass
    
    @args([Field], [])
    def getFields(self):
        pass
    
    @args([Type], [])
    def getGenericInterfaces(self):
        pass
    
    @args(Type, [])
    def getGenericSuperclass(self):
        pass
    
    @args([Class], [])
    def getInterfaces(self):
        pass
    
    @args(Method, [String, [Class]])
    def getMethod(self, name, parameterTypes):
        pass
    
    @args([Method], [])
    def getMethods(self):
        pass
    
    @args(int, [])
    def getModifiers(self):
        pass
    
    @args(ProtectionDomain, [])
    def getProtectionDomain(self):
        pass
    
    @args(URL, [String])
    def getResource(self, resourceName):
        pass
    
    @args(InputStream, [String])
    def getResourceAsStream(self, resourceName):
        pass
    
    @args([Object], [])
    def getSigners(self):
        pass
    
    @args(String, [])
    def getSimpleName(self):
        pass
    
    @args(Class, [])
    def getSuperclass(self):
        pass
    
    @synchronized
    @args([TypeVariable], [])
    def getTypeParameters(self):
        pass
    
    @args(bool, [])
    def isAnnotation(self):
        pass
    
    @args(bool, [Class])
    def isAnnotationPresent(self, annotationType):
        pass
    
    @args(bool, [])
    def isAnonymousClass(self):
        pass
    
    @args(String, [])
    def getName(self):
        pass
    
    @args(String, [])
    def getPackage(self):
        pass
    
    @args(bool, [])
    def isArray(self):
        pass
    
    @args(bool, [Class])
    def isAssignableFrom(self, c):
        pass
    
    @args(bool, [])
    def isEnum(self):
        pass
    
    @args(bool, [Object])
    def isInstance(self, object):
        pass
    
    @args(bool, [])
    def isInterface(self):
        pass
    
    @args(bool, [])
    def isLocalClass(self):
        pass
    
    @args(bool, [])
    def isMemberClass(self):
        pass
    
    @args(bool, [])
    def isPrimitive(self):
        pass
    
    @args(bool, [])
    def isSynthetic(self):
        pass
    
    @args(T, [])
    def newInstance(self):
        pass
    
    @args(String, [])
    def toString(self):
        pass


class ClassLoader(Object):

    def __init__(self):
        pass
    
    @args(void, [ClassLoader])
    def __init__(self, parentLoader):
        pass
    
    @args(void, [])
    def clearAssertionStatus(self):
        pass
    
    @final
    @args(ClassLoader, [])
    def getParent(self):
        pass
    
    @args(URL, [String])
    def getResource(self, resName):
        pass
    
    @args(InputStream, [String])
    def getResourceAsStream(self, resName):
        pass
    
    @args(Enumeration(URL), [String])
    def getResources(self, resName):
        pass
    
    @static
    @args(ClassLoader, [])
    def getSystemClassLoader():
        pass
    
    @static
    @args(URL, [String])
    def getSystemResource(resName):
        pass
    
    @static
    @args(InputStream, [String])
    def getSystemResourceAsStream(resName):
        pass
    
    @static
    @args(Enumeration(URL), [String])
    def getSystemResources(resName):
        pass
    
    @args(Class, [String])
    def loadClass(self, className):
        pass
    
    @args(void, [String, bool])
    def setClassAssertionStatus(self, cname, enable):
        pass
    
    @args(void, [bool])
    def setDefaultAssertionStatus(self, enable):
        pass
    
    @args(void, [String, bool])
    def setPackageAssertionStatus(self, pname, enable):
        pass


class ClassNotFoundException(Exception):

    def __init__(self):
        pass
    
    @args(void, [String])
    def __init__(self, detailMessage):
        pass
    
    @args(void, [String, Throwable])
    def __init__(self, detailMessage, throwable):
        pass
    
    @args(void, [Throwable])
    def __init__(self, throwable):
        pass
    
    @args(Throwable, [])
    def getCause(self):
        pass
    
    @args(Throwable, [])
    def getException(self):
        pass


class Cloneable:

    pass


class Comparable:

    __parameters__ = [T]
    
    @args(int, [T])
    def compareTo(self, another):
        pass


class Double(Number):

    __static_fields__ = {"TYPE": Class} # Class(Double)
    
    @args(void, [double])
    def __init__(self, value):
        pass
    
    @args(void, [String])
    def __init__(self, string):
        pass
    
    @static
    @args(double, [String])
    def parseDouble(string):
        pass
    
    @static
    @args(Double, [String])
    def valueOf(string):
        pass
    
    @static
    @args(Double, [double])
    def valueOf(d):
        pass


class Enum(Object):

    pass


class Exception(Throwable):

    def __init__(self):
        pass
    
    @args(void, [String])
    def __init__(self, detailMessage):
        pass
    
    @args(void, [String, Throwable])
    def __init__(self, detailMessage, throwable):
        pass
    
    @args(void, [Throwable])
    def __init__(self, throwable):
        pass


class Float(Number):

    __interfaces__ = [Comparable]
    __static_fields__ = {"TYPE": Class} # Class(Float)
    
    MAX_EXPONENT = 127
    MAX_VALUE = 3.4028235e38
    MIN_EXPONENT = -126
    MIN_NORMAL = 1.17549435e-38
    MIN_VALUE = 1.4e-45
    #NEGATIVE_INFINITY = 
    #NaN = 
    #POSITIVE_INFINITY = 
    SIZE = 32
    
    @args(void, [float])
    def __init__(self, value):
        pass
    
    @args(void, [double])
    def __init__(self, value):
        pass
    
    @args(void, [String])
    def __init__(self, value):
        pass
    
    @static
    @args(int, [float, float])
    def compare(float1, float2):
        pass
    
    @static
    @args(int, [float])
    def floatToIntBits(value):
        pass
    
    @static
    @args(int, [float])
    def floatToRawIntBits(value):
        pass
    
    @static
    @args(float, [int])
    def intBitsToFloat(bits):
        pass
    
    @static
    @args(bool, [float])
    def isInfinite(f):
        pass
    
    @args(bool, [])
    def isInfinite(self):
        pass
    
    @args(bool, [])
    def isNaN(self):
        pass
    
    @static
    @args(bool, [float])
    def isNaN(f):
        pass
    
    @static
    @args(float, [String])
    def parseFloat(string):
        pass
    
    @static
    @args(String, [float])
    def toHexString(f):
        pass
    
    @static
    @args(String, [float])
    def toString(f):
        pass
    
    @static
    @args(Float, [String])
    def valueOf(string):
        pass
    
    @static
    @args(Float, [float])
    def valueOf(f):
        pass


class IllegalArgumentException(Exception):

    def __init__(self):
        pass
    
    @args(void, [String])
    def __init__(self, detailMessage):
        pass
    
    @args(void, [String, Throwable])
    def __init__(self, detailMessage, throwable):
        pass
    
    @args(void, [Throwable])
    def __init__(self, throwable):
        pass


class IllegalStateException(Exception):

    def __init__(self):
        pass
    
    @args(void, [String])
    def __init__(self, detailMessage):
        pass
    
    @args(void, [String, Throwable])
    def __init__(self, detailMessage, throwable):
        pass
    
    @args(void, [Throwable])
    def __init__(self, throwable):
        pass


class IndexOutOfBoundsException(RuntimeException):

    def __init__(self):
        pass
    
    @args(void, [String])
    def __init__(self, detailMessage):
        pass


class Integer(Number):

    __static_fields__ = {"TYPE": Class} # Class(Integer)
    
    @args(void, [int])
    def __init__(self, value):
        pass
    
    @args(void, [String])
    def __init__(self, string):
        pass

    @static
    @args(int, [int])
    def bitCount(i):
        pass
    
    @args(byte, [])
    def byteValue(self):
        pass
    
    @static
    @args(int, [int, int])
    def compare(lhs, rhs):
        pass
    
    @args(int, [Integer])
    def compareTo(self, object):
        pass
    
    @static
    @args(Integer, [String])
    def decode(string):
        pass
    
    @args(double, [])
    def doubleValue(self):
        pass
    
    @args(boolean, [Object])
    def equals(self, o):
        pass
    
    @args(float, [])
    def floatValue(self):
        pass
    
    @static
    @args(Integer, [String, Integer])
    def getInteger(string, defaultValue):
        pass
    
    @static
    @args(Integer, [String])
    def getInteger(string):
        pass
    
    @static
    @args(Integer, [String, int])
    def getInteger(string, defaultValue):
        pass
    
    @args(int, [])
    def hashCode(self):
        pass
    
    @static
    @args(int, [int])
    def highestOneBit(i):
        pass
    
    @args(int, [])
    def intValue(self):
        pass
    
    @args(long, [])
    def longValue(self):
        pass
    
    @static
    @args(int, [int])
    def lowestOneBit(i):
        pass
    
    @static
    @args(int, [int])
    def numberOfLeadingZeros(i):
        pass
    
    @static
    @args(int, [int])
    def numberOfTrailingZeros(i):
        pass
    
    @static
    @args(int, [String])
    def parseInt(string):
        pass
    
    @static
    @args(int, [String, int])
    def parseInt(string, radix):
        pass
    
    @static
    @args(int, [int])
    def reverse(i):
        pass
    
    @static
    @args(int, [int])
    def reverseBytes(i):
        pass
    
    @static
    @args(int, [int, int])
    def rotateLeft(i, distance):
        pass
    
    @static
    @args(int, [int, int])
    def rotateRight(i, distance):
        pass
    
    @args(short, [])
    def shortValue(self):
        pass
    
    @static
    @args(int, [int])
    def signum(i):
        pass
    
    @static
    @args(String, [int])
    def toBinaryString(i):
        pass
    
    @static
    @args(String, [int])
    def toHexString(i):
        pass
    
    @static
    @args(String, [int])
    def toOctalString(i):
        pass
    
    @args(String, [])
    def toString(self):
        pass
    
    @static
    @args(String, [int])
    def toString(i):
        pass
    
    @static
    @args(String, [int, int])
    def toString(i, radix):
        pass
    
    @static
    @args(Integer, [String, int])
    def valueOf(string, radix):
        pass
    
    @static
    @args(Integer, [int])
    def valueOf(i):
        pass
    
    @static
    @args(Integer, [String])
    def valueOf(string):
        pass


class Iterable:

    __parameters__ = [T]
    
    @args(Iterator(T), [])
    def iterator(self):
        pass
    

class Long(Number):

    __static_fields__ = {"TYPE": Class} # Class(Long)
    
    MAX_VALUE = 0x7fffffffffffffff
    MIN_VALUE = 0x8000000000000000
    SIZE = 64
    
    @args(void, [long])
    def __init__(self, value):
        pass
    
    @args(void, [String])
    def __init__(self, string):
        pass
    
    @static
    @args(int, [long])
    def bitCount(value):
        pass
    
    @static
    @args(int, [long, long])
    def compare(value1, value2):
        pass
    
    @static
    @args(Long, [String])
    def decode(string):
        pass
    
    @static
    @args(Long, [String, Long])
    def getLong(string, defaultValue):
        pass
    
    @static
    @args(Long, [String, long])
    def getLong(string, defaultValue):
        pass
    
    @static
    @args(Long, [String])
    def getLong(string):
        pass
    
    @static
    @args(long, [long])
    def highestOneBit(value):
        pass
    
    @static
    @args(long, [long])
    def lowestOneBit(value):
        pass
    
    @static
    @args(int, [long])
    def numberOfLeadingZeros(value):
        pass
    
    @static
    @args(int, [long])
    def numberOfTrailingZeros(value):
        pass
    
    @static
    @args(long, [String, int])
    def parseLong(string, radix):
        pass
    
    @static
    @args(long, [String])
    def parseLong(string):
        pass
    
    @static
    @args(long, [long])
    def reverse(value):
        pass
    
    @static
    @args(long, [long])
    def reverseBytes(value):
        pass
    
    @static
    @args(long, [long, int])
    def rotateLeft(value, distance):
        pass
    
    @static
    @args(long, [long, int])
    def rotateRight(value, distance):
        pass
    
    @static
    @args(int, [long])
    def signum(value):
        pass
    
    @static
    @args(String, [long])
    def toBinaryString(value):
        pass
    
    @static
    @args(String, [long])
    def toHexString(value):
        pass
    
    @static
    @args(String, [long])
    def toOctalString(value):
        pass
    
    @static
    @args(String, [long])
    def toString(value):
        pass
    
    @static
    @args(String, [long, int])
    def toString(value, radix):
        pass
    
    @static
    @args(Long, [String, int])
    def valueOf(string, radix):
        pass
    
    @static
    @args(Long, [long])
    def valueOf(value):
        pass
    
    @static
    @args(Long, [String])
    def valueOf(string):
        pass


class Math(Object):

    E = 2.718281828459045
    PI = 3.141592653589793
    
    @static
    @args(double, [double, double])
    def IEEEremainder(x, y):
        pass
    
    @static
    @args(double, [double])
    def abs(d):
        pass
    
    @static
    @args(long, [long])
    def abs(l):
        pass
    
    @static
    @args(float, [float])
    def abs(f):
        pass
    
    @static
    @args(int, [int])
    def abs(i):
        pass
    
    @static
    @args(double, [double])
    def acos(d):
        pass
    
    @static
    @args(double, [double])
    def asin(d):
        pass
    
    @static
    @args(double, [double])
    def atan(d):
        pass
    
    @static
    @args(double, [double, double])
    def atan2(x, y):
        pass
    
    @static
    @args(double, [double])
    def cbrt(d):
        pass
    
    @static
    @args(double, [double])
    def ceil(d):
        pass
    
    @static
    @args(double, [double, double])
    def copySign(magnitude, sign):
        pass
    
    @static
    @args(float, [float, float])
    def copySign(magnitude, sign):
        pass
    
    @static
    @args(double, [double])
    def cos(d):
        pass
    
    @static
    @args(double, [double])
    def cosh(d):
        pass
    
    @static
    @args(double, [double])
    def exp(d):
        pass
    
    @static
    @args(double, [double])
    def expm1(d):
        pass
    
    @static
    @args(double, [double])
    def floor(d):
        pass
    
    @static
    @args(int, [float])
    def getExponent(f):
        pass
    
    @static
    @args(int, [double])
    def getExponent(d):
        pass
    
    @static
    @args(double, [double, double])
    def hypot(x, y):
        pass
    
    @static
    @args(double, [double])
    def log(d):
        pass
    
    @static
    @args(double, [double])
    def log10(d):
        pass
    
    @static
    @args(double, [double])
    def log1p(d):
        pass
    
    @static
    @args(long, [long, long])
    def max(l1, l2):
        pass
    
    @static
    @args(int, [int, int])
    def max(i1, i2):
        pass
    
    @static
    @args(double, [double, double])
    def max(d1, d2):
        pass
    
    @static
    @args(float, [float, float])
    def max(f1, f2):
        pass
    
    @static
    @args(long, [long, long])
    def min(l1, l2):
        pass
    
    @static
    @args(int, [int, int])
    def min(i1, i2):
        pass
    
    @static
    @args(double, [double, double])
    def min(d1, d2):
        pass
    
    @static
    @args(float, [float, float])
    def min(f1, f2):
        pass
    
    @static
    @args(float, [float, double])
    def nextAfter(start, direction):
        pass
    
    @static
    @args(double, [double, double])
    def nextAfter(start, direction):
        pass
    
    @static
    @args(double, [double])
    def nextUp(d):
        pass
    
    @static
    @args(float, [float])
    def nextUp(f):
        pass
    
    @static
    @args(double, [double, double])
    def pow(x, y):
        pass
    
    @static
    @args(double, [])
    def random():
        pass
    
    @static
    @args(double, [double])
    def rint(d):
        pass
    
    @static
    @args(long, [double])
    def round(d):
        pass
    
    @static
    @args(int, [float])
    def round(f):
        pass
    
    @static
    @args(double, [double, int])
    def scalb(d, scaleFactor):
        pass
    
    @static
    @args(double, [float, int])
    def scalb(f, scaleFactor):
        pass
    
    @static
    @args(double, [double])
    def signum(d):
        pass
    
    @static
    @args(float, [float])
    def signum(f):
        pass
    
    @static
    @args(double, [double])
    def sin(d):
        pass
    
    @static
    @args(double, [double])
    def sinh(d):
        pass
    
    @static
    @args(double, [double])
    def sqrt(d):
        pass
    
    @static
    @args(double, [double])
    def tan(d):
        pass
    
    @static
    @args(double, [double])
    def tanh(d):
        pass
    
    @static
    @args(double, [double])
    def toDegrees(angrad):
        pass
    
    @static
    @args(double, [double])
    def toRadians(angdeg):
        pass
    
    @static
    @args(double, [double])
    def ulp(d):
        pass
    
    @static
    @args(float, [float])
    def ulp(f):
        pass


class NoSuchMethodException(ReflectiveOperationException):

    def __init__(self):
        pass
    
    @args(void, [String])
    def __init__(self, detailMessage):
        pass


class Number(Object):

    @args(byte, [])
    def byteValue(self):
        pass
    
    @abstract
    @args(double, [])
    def doubleValue(self):
        pass
    
    @abstract
    @args(float, [])
    def floatValue(self):
        pass
    
    @abstract
    @args(int, [])
    def intValue(self):
        pass
    
    @abstract
    @args(long, [])
    def longValue(self):
        pass
    
    @args(short, [])
    def shortValue(self):
        pass


class NumberFormatException(IllegalArgumentException):

    def __init__(self):
        pass
    
    @args(void, [String])
    def __init__(self, detailMessage):
        pass


class Object:

    def __init__(self):
        pass
    
    @protected
    @args(Object, [])
    def clone(self):
        pass
    
    @args(bool, [Object])
    def equals(self, obj):
        pass
    
    @protected
    @args(void, [])
    def finalize(self):
        pass
    
    @final
    @args(Class, [])
    def getClass(self):
        pass
    
    @args(int, [])
    def hashCode(self):
        pass
    
    @args(void, [])
    def notify(self):
        pass
    
    @args(void, [])
    def notifyAll(self):
        pass
    
    @args(String, [])
    def toString(self):
        pass
    
    @args(void, [])
    def wait(self):
        pass
    
    @args(void, [long, long])
    def wait(self, millis, nanos):
        pass
    
    @args(void, [long])
    def wait(self, millis):
        pass


class Process(Object):

    def __init__(self):
        pass
    
    @abstract
    def destroy(self):
        pass
    
    @abstract
    @args(int, [])
    def exitValue(self):
        pass
    
    @abstract
    @args(InputStream, [])
    def getErrorStream(self):
        pass
    
    @abstract
    @args(InputStream, [])
    def getInputStream(self):
        pass
    
    @abstract
    @args(OutputStream, [])
    def getOutputStream(self):
        pass
    
    @abstract
    @args(int, [])
    def waitFor(self):
        pass


class ProcessBuilder(Object):

    @args(void, [[String]])
    def __init__(self, command):
        pass
    
    @args(void, [List(String)])
    def __init__(self, command):
        pass
    
    @args(ProcessBuilder, [List(String)])
    def command(self, command):
        pass
    
    @args(List(String), [])
    def command(self):
        pass
    
    @args(ProcessBuilder, [[String]])
    def command(self, command):
        pass
    
    @args(ProcessBuilder, [File])
    def directory(self, directory):
        pass
    
    @args(File, [])
    def directory(self):
        pass
    
    @args(Map(String, String), [])
    def environment(self):
        pass
    
    @args(ProcessBuilder, [bool])
    def redirectErrorStream(self, redirectErrorStream):
        pass
    
    @args(bool, [])
    def redirectErrorStream(self):
        pass
    
    @args(Process, [])
    def start(self):
        pass


class Readable:

    @args(int, [CharBuffer])
    def read(self, charBuffer):
        pass


class ReflectiveOperationException(Exception):

    def __init__(self):
        pass
    
    @args(void, [String])
    def __init__(self, detailMessage):
        pass
    
    @args(void, [String, Throwable])
    def __init__(self, detailMessage, throwable):
        pass
    
    @args(void, [Throwable])
    def __init__(self, throwable):
        pass


class Runnable:

    def run(self):
        pass


class Runtime(Object):

    @args(void, [Thread])
    def addShutdownHook(self, hook):
        pass
    
    @args(int, [])
    def availableProcessors(self):
        pass
    
    @args(Process, [[String], [String]])
    def exec_(self, progArray, envp):
        pass
    
    @args(Process, [String, [String], File])
    def exec_(self, prog, envp, directory):
        pass
    
    @args(Process, [[String], [String], File])
    def exec_(self, progArray, envp, directory):
        pass
    
    @args(Process, [String, [String]])
    def exec_(self, prog, envp):
        pass
    
    @args(Process, [String])
    def exec_(self, prog):
        pass
    
    @args(Process, [[String]])
    def exec_(self, progArray):
        pass
    
    @args(void, [int])
    def exit(self, code):
        pass
    
    @args(long, [])
    def freeMemory(self):
        pass
    
    def gc(self):
        pass
    
    @args(InputStream, [InputStream])
    def getLocalizedInputStream(self, stream):
        pass
    
    @args(void, [int])
    def halt(self, code):
        pass
    
    @args(void, [String])
    def load(self, absolutePath):
        pass
    
    @args(void, [String])
    def loadLibrary(self, nickname):
        pass
    
    @args(long, [])
    def maxMemory(self):
        pass
    
    @args(bool, [Thread])
    def removeShutdownHook(self, hook):
        pass
    
    def runFinalization(self):
        pass
    
    @static
    @args(void, [bool])
    def runFinalizersOnExit(self, run):
        pass
    
    @args(long, [])
    def totalMemory(self):
        pass
    
    @args(void, [bool])
    def traceInstructions(self, enable):
        pass
    
    @args(void, [bool])
    def traceMethodCalls(self, enable):
        pass


class RuntimeException(Exception):

    def __init__(self):
        pass
    
    @args(void, [String])
    def __init__(self, detailMessage):
        pass
    
    @args(void, [String, Throwable])
    def __init__(self, detailMessage, throwable):
        pass
    
    @args(void, [Throwable])
    def __init__(self, throwable):
        pass


class Short(Number):

    __static_fields__ = {"TYPE": Class} # Class(Short)
    
    MAX_VALUE = 0x7fff
    MIN_VALUE = -0x8000
    SIZE = 16
    
    @args(void, [short])
    def __init__(self, value):
        pass
    
    @args(void, [String])
    def __init__(self, string):
        pass
    
    @static
    @args(int, [short, short])
    def compare(value1, value2):
        pass
    
    @static
    @args(Short, [String])
    def decode(string):
        pass
    
    @static
    @args(short, [String, int])
    def parseShort(string, radix):
        pass
    
    @static
    @args(short, [String])
    def parseShort(string):
        pass
    
    @static
    @args(short, [short])
    def reverseBytes(value):
        pass
    
    @static
    @args(String, [short])
    def toString(value):
        pass
    
    @static
    @args(Short, [String, int])
    def valueOf(string, radix):
        pass
    
    @static
    @args(Short, [short])
    def valueOf(value):
        pass
    
    @static
    @args(Short, [String])
    def valueOf(string):
        pass


class StackTraceElement(Object):

    @args(void, [String, String, String, int])
    def __init__(self, cls, method, file, line):
        pass
    
    @args(String, [])
    def getClassName(self):
        pass
    
    @args(String, [])
    def getFileName(self):
        pass
    
    @args(int, [])
    def getLineNumber(self):
        pass
    
    @args(String, [])
    def getMethodName(self):
        pass
    
    @args(bool, [])
    def isNativeMethod(self):
        pass


class String(Object):

    __interfaces__ = [CharSequence, Comparable]
    
    def __init__(self):
        pass
    
    @args(void, [[byte]])
    def __init__(self, data):
        pass
    
    @args(void, [[byte], int, int])
    def __init__(self, data, offset, byteCount):
        pass
    
    @args(void, [[byte], int, int, String])
    def __init__(self, data, offset, byteCount, charsetName):
        pass
    
    @args(void, [[byte], String])
    def __init__(self, data, charsetName):
        pass
    
    @args(void, [[byte], int, int, Charset])
    def __init__(self, data, offset, byteCount, charset):
        pass
    
    @args(void, [[byte], Charset])
    def __init__(self, data, charset):
        pass
    
    @args(void, [[char]])
    def __init__(self, chars):
        pass
    
    @args(void, [[char], int, int])
    def __init__(self, chars, offset, charCount):
        pass
    
    @args(void, [String])
    def __init__(self, toCopy):
        pass
    
    @args(void, [[int], int, int])
    def __init__(self, codePoints, offset, count):
        pass
    
    @args(int, [int])
    def codePointAt(self, index):
        pass
    
    @args(int, [int])
    def codePointBefore(self, index):
        pass
    
    @args(int, [int])
    def codePointCount(self, start, end):
        pass
    
    @args(int, [String])
    def compareToIgnoreCase(self, string):
        pass
    
    @args(String, [String])
    def concat(self, string):
        pass
    
    @args(bool, [CharSequence])
    def contains(self, cs):
        pass
    
    @args(bool, [CharSequence])
    def contentEquals(self, cs):
        pass
    
    @args(bool, [String])
    def endsWith(self, suffix):
        pass
    
    @args(bool, [Object])
    def equals(self, other):
        pass
    
    @args(bool, [String])
    def equalsIgnoreCase(self, string):
        pass
    
    @static
    @args(String, [Locale, String, [Object]])
    def format(locale, formats, arguments):
        pass
    
    @static
    @args(String, [String, [Object]])
    def format(formats, arguments):
        pass
    
    @args(void, [int, int, [byte], int])
    def getBytes(self, start, end, data, index):
        pass
    
    @args([byte], [String])
    def getBytes(self, charsetName):
        pass
    
    @args([byte], [])
    def getBytes(self):
        pass
    
    @args(void, [int, int, [char], int])
    def getChars(self, start, end, buffer, index):
        pass
    
    @args(int, [int])
    def indexOf(self, c):
        pass
    
    @args(int, [int, int])
    def indexOf(self, c, start):
        pass
    
    @args(int, [String, int])
    def indexOf(self, subString, start):
        pass
    
    @args(int, [String])
    def indexOf(self, string):
        pass
    
    @args(String, [])
    def intern(self):
        pass
    
    @args(bool, [])
    def isEmpty(self):
        pass
    
    @args([String], [String])
    def split(self, regularExpression):
        pass
    
    @args(int, [int])
    def lastIndexOf(self, c):
        pass
    
    @args(int, [int, int])
    def lastIndexOf(self, c, start):
        pass
    
    @args(int, [String, int])
    def lastIndexOf(self, subString, start):
        pass
    
    @args(int, [String])
    def lastIndexOf(self, string):
        pass
    
    @args(bool, [String])
    def matches(self, regularExpression):
        pass
    
    @args(int, [int, int])
    def offsetByCodePoints(self, index, codePointOffset):
        pass
    
    @args(bool, [bool, int, String, int, int])
    def regionMatches(self, ignoreCase, thisStart, string, start, length):
        pass
    
    @args(bool, [int, String, int, int])
    def regionMatches(self, thisStart, string, start, length):
        pass
    
    @args(String, [CharSequence, CharSequence])
    def replace(self, target, replacement):
        pass
    
    @args(String, [char, char])
    def replace(self, oldChar, newChar):
        pass
    
    @args(String, [String, String])
    def replaceAll(self, regularExpression, replacement):
        pass
    
    @args(String, [String, String])
    def replaceFirst(self, regularExpression, replacement):
        pass
    
    @args([String], [String])
    def split(self, regularExpression):
        pass
    
    @args([String], [String, int])
    def split(self, regularExpression, limit):
        pass
    
    @args(bool, [String])
    def startsWith(self, prefix):
        pass
    
    @args(bool, [String, int])
    def startsWith(self, prefix, start):
        pass
    
    @args(String, [int])
    def substring(self, start):
        pass
    
    @args(String, [int, int])
    def substring(self, start, end):
        pass
    
    @args([char], [])
    def toCharArray(self):
        pass
    
    @args(String, [Locale])
    def toLowerCase(self, locale):
        pass
    
    @args(String, [])
    def toLowerCase(self):
        pass
    
    @args(String, [Locale])
    def toUpperCase(self, locale):
        pass
    
    @args(String, [])
    def trim(self):
        pass
    
    @static
    @args(String, [long])
    def valueOf(value):
        pass
    
    @static
    @args(String, [Object])
    def valueOf(value):
        pass
    
    @static
    @args(String, [[char]])
    def valueOf(data):
        pass
    
    @static
    @args(String, [double])
    def valueOf(value):
        pass
    
    @static
    @args(String, [int])
    def valueOf(value):
        pass
    
    @static
    @args(String, [float])
    def valueOf(value):
        pass
    
    @static
    @args(String, [[char], int, int])
    def valueOf(data, start, length):
        pass
    
    @static
    @args(String, [bool])
    def valueOf(value):
        pass
    
    @static
    @args(String, [char])
    def valueOf(value):
        pass


class System(Object):

    __static_fields__ = {
        "err": PrintStream,
        "in":  InputStream,
        "out": PrintStream
        }
    
    @static
    @args(void, [Object, int, Object, int, int])
    def arrayCopy(src, srcPos, dst, dstPos, length):
        pass
    
    @static
    @args(String, [String])
    def clearProperty(name):
        pass
    
    #@static
    #@args(Console, [])
    #def console():
    #    pass
    
    @static
    @args(long, [])
    def currentTimeMillis():
        pass
    
    @static
    @args(void, [int])
    def exit(code):
        pass
    
    @static
    def gc():
        pass
    
    #@static
    #@args(Properties, [])
    #def getProperties():
    #    pass
    
    @static
    @args(String, [String])
    def getProperty(propertyName):
        pass
    
    @static
    @args(String, [String, String])
    def getProperty(propertyName, defaultValue):
        pass
    
    #@static
    #@args(SecurityManager, [])
    #def getSecurityManager():
    #    pass
    
    @static
    @args(String, [String])
    def getenv(name):
        pass
    
    @static
    @args(Map(String, String), [])
    def getenv():
        pass
    
    @static
    @args(int, [Object])
    def identityHashCode(anObject):
        pass
    
    #@static
    #@args(Channel, [])
    #def inheritedChannel():
    #    pass
    
    @static
    @args(String, [])
    def lineSeparator():
        pass
    
    @static
    @args(void, [String])
    def load(pathName):
        pass
    
    @static
    @args(void, [String])
    def loadLibrary(libName):
        pass
    
    @static
    @args(String, [String])
    def mapLibraryName(nickname):
        pass
    
    @static
    @args(long, [])
    def nanoTime():
        pass
    
    @static
    def runFinalization():
        pass
    
    @static
    @args(void, [PrintStream])
    def setErr(newErr):
        pass
    
    @static
    @args(void, [InputStream])
    def setIn(newIn):
        pass
    
    @static
    @args(void, [PrintStream])
    def setOut(newOut):
        pass
    
    #@static
    #@args(void, [Properties])
    #def setProperties(p):
    #    pass
    
    @static
    @args(String, [String, String])
    def setProperty(name, value):
        pass
    
    #@static
    #@args(void, [SecurityManager])
    #def setSecurityManager(sm):
    #    pass


class Thread(Object):

    __interfaces__ = [Runnable]
    
    class State(Enum):
    
        __static_fields__ = {
            "BLOCKED": Thread.State,
            "NEW": Thread.State,
            "RUNNABLE": Thread.State,
            "TERMINATED": Thread.State,
            "TIMED_WAITING": Thread.State,
            "WAITING": Thread.State
            }
        
        @static
        @args(Thread.State, [String])
        def valueOf(name):
            pass
        
        @final
        @static
        @args([Thread.State], [])
        def values():
            pass
    
    class UncaughtExceptionHandler:
    
        @args(void, [Thread, Throwable])
        def uncaughtException(self, thread, ex):
            pass
    
    def __init__(self):
        pass
    
    @args(void, [Runnable])
    def __init__(self, runnable):
        pass
    
    @args(void, [Runnable, String])
    def __init__(self, runnable, threadName):
        pass
    
    @args(void, [String])
    def __init__(self, threadName):
        pass
    
    @args(void, [ThreadGroup, Runnable])
    def __init__(self, group, runnable):
        pass
    
    @args(void, [ThreadGroup, Runnable, String])
    def __init__(self, group, runnable, threadName):
        pass
    
    @args(void, [ThreadGroup, String])
    def __init__(self, group, threadName):
        pass
    
    @args(void, [ThreadGroup, Runnable, String, long])
    def __init__(self, group, runnable, threadName, stackSize):
        pass
    
    @static
    @args(int, [])
    def activeCount():
        pass
    
    @final
    @args(void, [])
    def checkAccess(self):
        pass
    
    @static
    @args(Thread, [])
    def currentThread():
        pass
    
    def destroy(self):
        pass
    
    @static
    def dumpStack():
        pass
    
    @static
    @args(int, [[Thread]])
    def enumerate(threads):
        pass
    
    @static
    @args(Thread.UncaughtExceptionHandler, [])
    def getDefaultUncaughtExceptionHandler():
        pass
    
    @args(long, [])
    def getId(self):
        pass
    
    @final
    @args(String, [])
    def getName(self):
        pass
    
    @final
    @args(int, [])
    def getPriority(self):
        pass
    
    @args(Thread.UncaughtExceptionHandler, [])
    def getUncaughtExceptionHandler(self):
        pass
    
    @args(Thread.State, [])
    def getState(self):
        pass
    
    @final
    @args(ThreadGroup, [])
    def getThreadGroup(self):
        pass
    
    ### ...
    
    @static
    @args(bool, [Object])
    def holdsLock(object):
        pass
    
    def interrupt(self):
        pass
    
    @static
    @args(bool, [])
    def interrupted():
        pass
    
    @final
    @args(bool, [])
    def isAlive(self):
        pass
    
    @final
    @args(bool, [])
    def isDaemon(self):
        pass
    
    @args(bool, [])
    def isInterrupted(self):
        pass
    
    @final
    def join(self):
        pass
    
    @final
    @args(void, [long, int])
    def join(self, millis, nanos):
        pass
    
    @final
    @args(void, [long])
    def join(self, millis):
        pass
    
    ### ...
    
    @final
    @args(void, [bool])
    def setDaemon(self, isDaemon):
        pass
    
    @static
    @args(void, [Thread.UncaughtExceptionHandler])
    def setDefaultUncaughtExceptionHandler(handler):
        pass
    
    @final
    @args(void, [String])
    def setName(self, threadName):
        pass
    
    @final
    @args(void, [int])
    def setPriority(self, priority):
        pass
    
    @args(void, [Thread.UncaughtExceptionHandler])
    def setUncaughtExceptionHandler(self, handler):
        pass
    
    @static
    @args(void, [long, int])
    def sleep(millis, nanos):
        pass
    
    @static
    @args(void, [long])
    def sleep(millis):
        pass
    
    @synchronized
    def start(self):
        pass
    
    @synchronized
    @final
    @args(void, [Throwable])
    def stop(self, throwable):
        pass
    
    @final
    def stop(self):
        pass
    
    @final
    def suspend(self):
        pass
    
    #@static
    #def yield():
    #    pass


class ThreadGroup(Object):

    __interfaces__ = [Thread.UncaughtExceptionHandler]
    
    @args(void, [String])
    def __init__(self, name):
        pass
    
    @args(void, [ThreadGroup, String])
    def __init__(self, parent, name):
        pass
    
    @args(int, [])
    def activeCount(self):
        pass
    
    @args(int, [])
    def activeGroupCount(self):
        pass
    
    @final
    def checkAccess(self):
        pass
    
    @final
    def destroy(self):
        pass
    
    @args(int, [[Thread]])
    def enumerate(self, threads):
        pass
    
    @args(int, [[ThreadGroup], bool])
    def enumerate(self, groups, recurse):
        pass
    
    @args(int, [[Thread], bool])
    def enumerate(self, threads, recurse):
        pass
    
    @args(int, [[ThreadGroup]])
    def enumerate(self, groups):
        pass
    
    @final
    @args(int, [])
    def getMaxPriority(self):
        pass
    
    @final
    @args(String, [])
    def getName(self):
        pass
    
    @final
    @args(ThreadGroup, [])
    def getParent(self):
        pass
    
    @final
    def interrupt(self):
        pass
    
    @final
    @args(bool, [])
    def isDaemon(self):
        pass
    
    @synchronized
    @args(bool, [])
    def isDestroyed(self):
        pass
    
    def list(self):
        pass
    
    @final
    @args(bool, [ThreadGroup])
    def parentOf(self, group):
        pass
    
    @final
    @args(void, [bool])
    def setDaemon(self, isDaemon):
        pass
    
    @final
    @args(void, [int])
    def setMaxPriority(self, newMax):
        pass
    
    @args(void, [Thread, Throwable])
    def uncaughtException(self, thread, throwable):
        pass


class Throwable(Object):

    def __init__(self):
        pass
    
    @args(void, [String])
    def __init__(self, detailMessage):
        pass
    
    @args(void, [String, Throwable])
    def __init__(self, detailMessage, cause):
        pass
    
    @args(void, [Throwable])
    def __init__(self, cause):
        pass
    
    @args(void, [String, Throwable, bool, bool])
    def __init__(self, detailMessage, cause, enableSuppression, writableStackTrace):
        pass
    
    @args(void, [Throwable])
    def addSuppressed(self, throwable):
        pass
    
    @args(Throwable, [])
    def fillInStackTrace(self):
        pass
    
    @args(Throwable, [])
    def getCause(self):
        pass
    
    @args(String, [])
    def getLocalizedMessage(self):
        pass
    
    @args(String, [])
    def getMessage(self):
        pass
    
    @args([StackTraceElement], [])
    def getStackTrace(self):
        pass
    
    @final
    @args([Throwable], [])
    def getSuppressed(self):
        pass
    
    @args(Throwable, [Throwable])
    def initCause(self, throwable):
        pass
    
    #@args(void, [PrintStream])
    #def printStackTrace(self, err):
    #    pass
    
    #@args(void, [PrintWriter])
    #def printStackTrace(self, err):
    #    pass
    
    @args(void, [])
    def printStackTrace(self):
        pass
    
    @args(void, [[StackTraceElement]])
    def setStackTrace(self, trace):
        pass


class Void(Object):

    __static_fields__ = {"TYPE": Class(Void)}
