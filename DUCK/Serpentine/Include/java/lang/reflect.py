# Class declarations obtained from Android API documentation licensed under the
# Apache 2.0 license (http://www.apache.org/licenses/LICENSE-2.0) and rewritten
# in syntax that the Python compiler module can parse.

__package__ = "java.lang.reflect"

from java.io import Serializable
from java.lang import Class, Object, String

class AccessibleObject(Object):

    __interfaces__ = [AnnotatedElement]
    __parameters__ = [T]
    
    def __init__(self):
        pass
    
    @args(T, [Class(T)])
    def getAnnotation(self, annotationType):
        pass
    
    @args([Annotation], [])
    def getAnnotations(self):
        pass
    
    @args([Annotation], [])
    def getDeclaredAnnotations(self):
        pass
    
    @args(bool, [])
    def isAccessible(self):
        pass
    
    @args(bool, [Class])
    def isAnnotationPresent(self, annotationType):
        pass
    
    @args(void, [bool])
    def setAccessible(self, flag):
        pass
    
    @static
    @args(void, [[AccessibleObject], bool])
    def setAccessible(self, objects, flag):
        pass


class AnnotatedElement:

    __parameters__ = [T]
    
    #<T extends Annotation>
    @args(T, [T])
    def getAnnotation(self, annotationType):
        pass
    
    @args([Annotation], [])
    def getAnnotations(self):
        pass
    
    @args([Annotation], [])
    def getDeclaredAnnotations(self):
        pass
    
    @args(bool, [Class])
    def isAnnotationPresent(self, annotationType):
        pass


class Constructor(AccessibleObject):

    __interfaces__ = [GenericDeclaration, Member]
    #__parameters__ = [T, A]
    
    __fields__ = {
        "accessFlags": int,
        "declaringClass": Class,
        "declaringClassOfOverriddenMethod": Class,
        "dexMethodIndex": int
        }
    
    @args(boolean, [Object])
    def equals(self, other):
        pass
    
    @args(T, [Class(T)])
    def getAnnotation(self, annotationClass):
        pass
    
    @args(A, [Class(A)])
    def getAnnotation(self, annotationType):
        pass
    
    @args([Annotation], [])
    def getAnnotations(self):
        pass
    
    @args([Annotation], [])
    def getDeclaredAnnotations(self):
        pass
    
    @args(Class, [])
    def getDeclaringClass(self):
        pass
    
    @args([Class], [])
    def getExceptionTypes(self):
        pass
    
    @args([Type], [])
    def getGenericExceptionTypes(self):
        pass
    
    @args([Type], [])
    def getGenericParameterTypes(self):
        pass
    
    @args(int, [])
    def getModifiers(self):
        pass
    
    @args(String, [])
    def getName(self):
        pass
    
    @args([[Annotation]], [])
    def getParameterAnnotations(self):
        pass
    
    @args([Class], [])
    def getParameterTypes(self):
        pass
    
    @args([TypeVariable(Method)], [])
    def getTypeParameters(self):
        pass
    
    @args(int, [])
    def hashCode(self):
        pass
    
    @args(bool, [Class])
    def isAnnotationPresent(self, annotationType):
        pass
    
    @args(bool, [])
    def isSynthetic(self):
        pass
    
    @args(bool, [])
    def isVarArgs(self):
        pass
    
    @args(T, [[Object]])
    def newInstance(self, args):
        pass
    
    @args(void, [bool])
    def setAccessible(self, flag):
        pass
    
    @args(String, [])
    def toGenericString(self):
        pass
    
    @args(String, [])
    def toString(self):
        pass


class Field(AccessibleObject):

    __interfaces__ = [Member]
    
    @args(boolean, [Object])
    def equals(self, other):
        pass
    
    @args(Object, [Object])
    def get(self, object):
        pass
    
    @args(A, [Class(A)])
    def getAnnotation(self, annotationType):
        pass
    
    @args(bool, [Object])
    def getBoolean(self, object):
        pass
    
    @args(byte, [Object])
    def getByte(self, object):
        pass
    
    @args(char, [Object])
    def getChar(self, object):
        pass
    
    @args([Annotation], [])
    def getDeclaredAnnotations(self):
        pass
    
    @args(Class, [])
    def getDeclaringClass(self):
        pass
    
    @args(double, [Object])
    def getDouble(self, object):
        pass
    
    @args(float, [Object])
    def getFloat(self, object):
        pass
    
    @args(Type, [])
    def getGenericType(self):
        pass
    
    @args(int, [Object])
    def getInt(self, object):
        pass
    
    @args(long, [Object])
    def getLong(self, object):
        pass
    
    @args(int, [])
    def getModifiers(self):
        pass
    
    @args(String, [])
    def getName(self):
        pass
    
    @args(short, [Object])
    def getShort(self, object):
        pass
    
    @args(Class, [])
    def getType(self):
        pass
    
    @args(int, [])
    def hashCode(self):
        pass
    
    @args(bool, [Class])
    def isAnnotationPresent(self, annotationType):
        pass
    
    @args(bool, [])
    def isEnumConstant(self):
        pass
    
    @args(bool, [])
    def isSynthetic(self):
        pass
    
    @args(void, [Object, Object])
    def set(self, object, value):
        pass
    
    @args(void, [Object, bool])
    def setBoolean(self, object, value):
        pass
    
    @args(void, [Object, byte])
    def setByte(self, object, value):
        pass
    
    @args(void, [Object, char])
    def setChar(self, object, value):
        pass
    
    @args(void, [Object, double])
    def setDouble(self, object, value):
        pass
    
    @args(void, [Object, float])
    def setFloat(self, object, value):
        pass
    
    @args(void, [Object, int])
    def setInt(self, object, value):
        pass
    
    @args(void, [Object, long])
    def setLong(self, object, value):
        pass
    
    @args(void, [Object, short])
    def setShort(self, object, value):
        pass
    
    @args(String, [])
    def toGenericString(self):
        pass
    
    @args(String, [])
    def toString(self):
        pass


class GenericDeclaration:

    @args([TypeVariable], [])
    def getTypeParameters(self):
        pass


class InvocationHandler:

    @args(Object, [Object, Method, [Object]])
    def invoke(self, proxy, method, args):
        pass


class Member:

    DECLARED = 1
    PUBLIC = 0
    
    @args(Class, [])
    def getDeclaringClass(self):
        pass
    
    @args(int, [])
    def getModifiers(self):
        pass
    
    @args(String, [])
    def getName(self):
        pass
    
    @args(bool, [])
    def isSynthetic(self):
        pass


class Method(AccessibleObject):

    __interfaces__ = [GenericDeclaration, Member]
    #__parameters__ = [T, A]
    
    __fields__ = {
        "accessFlags": int,
        "declaringClass": Class,
        "declaringClassOfOverriddenMethod": Class,
        "dexMethodIndex": int
        }
    
    @args(boolean, [Object])
    def equals(self, other):
        pass
    
    @args(T, [Class(T)])
    def getAnnotation(self, annotationClass):
        pass
    
    @args(A, [Class(A)])
    def getAnnotation(self, annotationType):
        pass
    
    @args([Annotation], [])
    def getAnnotations(self):
        pass
    
    @args([Annotation], [])
    def getDeclaredAnnotations(self):
        pass
    
    @args(Class, [])
    def getDeclaringClass(self):
        pass
    
    @args(Object, [])
    def getDefaultValue(self):
        pass
    
    @args([Class], [])
    def getExceptionTypes(self):
        pass
    
    @args([Type], [])
    def getGenericExceptionTypes(self):
        pass
    
    @args([Type], [])
    def getGenericParameterTypes(self):
        pass
    
    @args(Type, [])
    def getGenericReturnType(self):
        pass
    
    @args(int, [])
    def getModifiers(self):
        pass
    
    @args(String, [])
    def getName(self):
        pass
    
    @args([[Annotation]], [])
    def getParameterAnnotations(self):
        pass
    
    @args([Class], [])
    def getParameterTypes(self):
        pass
    
    @args(Class, [])
    def getReturnType(self):
        pass
    
    @args([TypeVariable(Method)], [])
    def getTypeParameters(self):
        pass
    
    @args(int, [])
    def hashCode(self):
        pass
    
    @args(Object, [Object, [Object]])
    def invoke(self, receiver, args):
        pass
    
    @args(bool, [Class])
    def isAnnotationPresent(self, annotationType):
        pass
    
    @args(bool, [])
    def isBridge(self):
        pass
    
    @args(bool, [])
    def isSynthetic(self):
        pass
    
    @args(bool, [])
    def isVarArgs(self):
        pass
    
    @args(String, [])
    def toGenericString(self):
        pass
    
    @args(String, [])
    def toString(self):
        pass


class Proxy(Object):

    __interfaces__ = [Serializable]
    
    __fields__ = {"h": InvocationHandler}
    
    @protected
    @args([], [InvocationHandler])
    def __init__(self, h):
        pass
    
    @static
    @args(InvocationHandler, [Object])
    def getInvocationHandler(proxy):
        pass
    
    @static
    @args(Class, [ClassLoader, [Class]])
    def getProxyClass(loader, interfaces):
        pass
    
    @static
    @args(bool, [Class])
    def isProxyClass(cl):
        pass
    
    @static
    @args(Object, [ClassLoader, [Class], InvocationHandler])
    def newProxyInstance(loader, interfaces, invocationHandler):
        pass


class Type:

    pass


class TypeVariable:

    __interfaces__ = [Type]
    __parameters__ = [T]
    
    @args([Type], [])
    def getBounds(self):
        pass
    
    @args(T, [])
    def getGenericDeclaration(self):
        pass
    
    @args(String, [])
    def getName(self):
        pass
