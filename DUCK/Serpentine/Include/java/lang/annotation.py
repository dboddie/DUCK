# Class declarations obtained from Android API documentation licensed under the
# Apache 2.0 license (http://www.apache.org/licenses/LICENSE-2.0) and rewritten
# in syntax that the Python compiler module can parse.

__package__ = "java.lang.annotation"

from java.lang import Class, Object

class Annotation:

    @args(Class, [])
    def annotationType(self):
        pass
    
    @args(bool, [Object])
    def equals(self, obj):
        pass
    
    @args(int, [])
    def hashCode(self):
        pass
    
    @args(String, [])
    def toString(self):
        pass
