# Class declarations obtained from Android API documentation licensed under the
# Apache 2.0 license (http://www.apache.org/licenses/LICENSE-2.0) and rewritten
# in syntax that the Python compiler module can parse.

from java.lang import Object, String
from java.net import InetAddress, Socket

class SocketFactory(Object):

    def __init__(self):
        pass
    
    @abstract
    @args(Socket, [String, int, InetAddress, int])
    def createSocket(self, host, port, localHost, localPort):
        pass
    
    @abstract
    @args(Socket, [InetAddress, int, InetAddress, int])
    def createSocket(self, address, port, localAddress, localPort):
        pass
    
    @abstract
    @args(Socket, [InetAddress, int])
    def createSocket(self, host, port):
        pass
    
    @abstract
    @args(Socket, [String, int])
    def createSocket(self, host, port):
        pass
    
    @args(Socket, [])
    def createSocket(self):
        pass
    
    @synchronized
    @static
    @args(SocketFactory, [])
    def getDefault():
        pass
