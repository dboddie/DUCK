# Class declarations obtained from Android API documentation licensed under the
# Apache 2.0 license (http://www.apache.org/licenses/LICENSE-2.0) and rewritten
# in syntax that the Python compiler module can parse.

__package__ = "javax.net.ssl"

from java.io import IOException
from java.lang import Object, String
from java.net import HttpURLConnection, Socket, URL
from java.security import Principal, SecureRandom
from java.util import Date, Enumeration
from javax.net import SocketFactory

class Certificate(Object):

    def __init__(self):
        pass
    
    @args(bool, [Object])
    def equals(self, object):
        pass
    
    @args([byte], [])
    def getEncoded(self):
        pass
    
    @args(PublicKey, [])
    def getPublicKey(self):
        pass
    
    @args(void, [PublicKey, String])
    def verify(self, key, sigProvider):
        pass
    
    @args(void, [PublicKey])
    def verify(self, key):
        pass


class HostnameVerifier:

    @args(bool, [String, SSLSession])
    def verify(self, hostname, session):
        pass


class HttpsURLConnection(HttpURLConnection):

    __fields__ = {
        "hostnameVerifier": HostnameVerifier
        }
    
    @args(void, [URL])
    def __init__(self, url):
        pass
    
    @args(String, [])
    def getCipherSuite(self):
        pass
    
    @static
    @args(HostnameVerifier, [])
    def getDefaultHostnameVerifier():
        pass
    
    @static
    @args(SSLSocketFactory, [])
    def getDefaultSSLSocketFactory():
        pass
    
    @args(HostnameVerifier, [])
    def getHostnameVerifier(self):
        pass
    
    @args([Certificate], [])
    def getLocalCertificates(self):
        pass
    
    @args(Principal, [])
    def getLocalPrincipal(self):
        pass
    
    @args(Principal, [])
    def getPeerPrincipal(self):
        pass
    
    @args(SSLSocketFactory, [])
    def getSSLSocketFactory(self):
        pass
    
    @args([Certificate], [])
    def getServerCertificates(self):
        pass
    
    @static
    @args(void, [HostnameVerifier])
    def setDefaultHostnameVerifier(verifier):
        pass
    
    @static
    @args(void, [SSLSocketFactory])
    def setDefaultSSLSocketFactory(socketFactory):
        pass
    
    @args(void, [HostnameVerifier])
    def setHostnameVerifier(self, verifier):
        pass
    
    @args(void, [SSLSocketFactory])
    def setSSLSocketFactory(self, socketFactory):
        pass


class KeyManager:
    pass


class SSLContext(Object):

    @protected
    @args(void, [SSLContextSpi, Provider, String])
    def __init__(self, contextSpi, provider, protocol):
        pass
    
    @final
    @args(SSLEngine, [String, int])
    def createSSLEngine(self, peerHost, peerPort):
        pass
    
    @final
    @args(SSLEngine, [])
    def createSSLEngine(self):
        pass
    
    @final
    @args(SSLSessionContext, [])
    def getClientSessionContext(self):
        pass
    
    @static
    @args(SSLContext, [])
    def getDefault():
        pass
    
    @final
    @args(SSLParameters, [])
    def getDefaultSSLParameters(self):
        pass
    
    @static
    @args(SSLContext, [String, String])
    def getInstance(protocol, provider):
        pass
    
    @static
    @args(SSLContext, [String, Provider])
    def getInstance(protocol, provider):
        pass
    
    @static
    @args(SSLContext, [String])
    def getInstance(protocol):
        pass
    
    @final
    @args(String, [])
    def getProtocol(self):
        pass
    
    @final
    @args(Provider, [])
    def getProvider(self):
        pass
    
    @final
    @args(SSLSessionContext, [])
    def getServerSessionContext(self):
        pass
    
    @final
    @args(SSLServerSocketFactory, [])
    def getServerSocketFactory(self):
        pass
    
    @final
    @args(SSLSocketFactory, [])
    def getSocketFactory(self):
        pass
    
    @final
    @args(SSLParameters, [])
    def getSupportedSSLParameters(self):
        pass
    
    @final
    @args(void, [[KeyManager], [TrustManager], SecureRandom])
    def init(self, km, tm, sr):
        pass
    
    @static
    @args(void, [SSLContext])
    def setDefault(sslContext):
        pass


class SSLException(IOException):

    @args(void, [String])
    def __init__(self, reason):
        pass
    
    @args(void, [String, Throwable])
    def __init__(self, message, cause):
        pass
    
    @args(void, [Throwable])
    def __init__(self, cause):
        pass


class SSLProtocolException(SSLException):

    @args(void, [String])
    def __init__(self, reason):
        pass


class SSLSession:

    @args(int, [])
    def getApplicationBufferSize(self):
        pass
    
    @args(String, [])
    def getCipherSuite(self):
        pass
    
    @args(long, [])
    def getCreationTime(self):
        pass
    
    @args([byte], [])
    def getId(self):
        pass
    
    @args(long, [])
    def getLastAccessedTime(self):
        pass
    
    @args([Certificate], [])
    def getLocalCertificates(self):
        pass
    
    @args(Principal, [])
    def getLocalPrincipal(self):
        pass
    
    @args(int, [])
    def getPacketBufferSize(self):
        pass
    
    @args([X509Certificate], [])
    def getPeerCertificateChain(self):
        pass
    
    @args([Certificate], [])
    def getPeerCertificates(self):
        pass
    
    @args(String, [])
    def getPeerHost(self):
        pass
    
    @args(int, [])
    def getPeerPort(self):
        pass
    
    @args(Principal, [])
    def getPeerPrincipal(self):
        pass
    
    @args(String, [])
    def getProtocol(self):
        pass
    
    @args(SSLSessionContext, [])
    def getSessionContext(self):
        pass
    
    @args(Object, [String])
    def getValue(self, name):
        pass
    
    @args([String], [])
    def getValueNames(self):
        pass
    
    def invalidate(self):
        pass
    
    def isValid(self):
        pass
    
    @args(void, [String, Object])
    def putValue(self, name, value):
        pass
    
    @args(void, [String])
    def removeValue(self, name):
        pass


class SSLSessionContext:

    @args(Enumeration([byte]), [])
    def getIds(self):
        pass
    
    @args(SSLSession, [[byte]])
    def getSession(self, sessionId):
        pass
    
    @args(int, [])
    def getSessionCacheSize(self):
        pass
    
    @args(int, [])
    def getSessionTimeout(self):
        pass
    
    @args(void, [int])
    def setSessionCacheSize(self, size):
        pass
    
    @args(void, [int])
    def setSessionTimeout(self, seconds):
        pass


class SSLSocket(Socket):

    def __init__(self):
        pass
    
    @args(void, [String, int])
    def __init__(self, host, port):
        pass
    
    @args(void, [InetAddress, int])
    def __init__(self, address, port):
        pass
    
    @args(void, [String, int, InetAddress, int])
    def __init__(self, host, port, clientAddress, clientPort):
        pass
    
    @args(void, [InetAddress, int, InetAddress, int])
    def __init__(self, address, port, clientAddress, clientPort):
        pass
    
    @abstract
    @args(void, [HandshakeCompletedListener])
    def addHandshakeCompletedListener(self, listener):
        pass
    
    @abstract
    @args(boolean, [])
    def getEnableSessionCreation(self):
        pass
    
    @abstract
    @args([String], [])
    def getEnabledCipherSuites(self):
        pass
    
    @abstract
    @args([String], [])
    def getEnabledProtocols(self):
        pass
    
    @abstract
    @args(boolean, [])
    def getNeedClientAuth(self):
        pass
    
    @args(SSLParameters, [])
    def getSSLParameters(self):
        pass
    
    @abstract
    @args(SSLSession, [])
    def getSession(self):
        pass
    
    @abstract
    @args([String], [])
    def getSupportedCipherSuites(self):
        pass
    
    @abstract
    @args([String], [])
    def getSupportedProtocols(self):
        pass
    
    @abstract
    @args(boolean, [])
    def getUseClientMode(self):
        pass
    
    @abstract
    @args(boolean, [])
    def getWantClientAuth(self):
        pass
    
    @abstract
    @args(void, [HandshakeCompletedListener])
    def removeHandshakeCompletedListener(self, listener):
        pass
    
    @abstract
    @args(void, [bool])
    def setEnableSessionCreation(self, flag):
        pass
    
    @abstract
    @args(void, [[String]])
    def setEnabledCipherSuites(self, suites):
        pass
    
    @abstract
    @args(void, [[String]])
    def setEnabledProtocols(self, protocols):
        pass
    
    @abstract
    @args(void, [bool])
    def setNeedClientAuth(self, need):
        pass
        
    @args(void, [SSLParameters])
    def setSSLParameters(self, p):
        pass
    
    @abstract
    @args(void, [bool])
    def setUseClientMode(self, mode):
        pass
    
    @abstract
    @args(void, [bool])
    def setWantClientAuth(self, want):
        pass
    
    def shutdownInput(self):
        pass
    
    def shutdownOutput(self):
        pass
    
    @abstract
    def startHandshake(self):
        pass
    


class SSLSocketFactory(SocketFactory):

    def __init__(self):
        pass
    
    @abstract
    @args(Socket, [Socket, String, int, bool])
    def createSocket(self, s, host, port, autoClose):
        pass
    
    @synchronized
    @static
    @args(SocketFactory, [])
    def getDefault():
        pass
    
    @abstract
    @args([String], [])
    def getDefaultCipherSuites(self):
        pass
    
    @abstract
    @args([String], [])
    def getSupportedCipherSuites(self):
        pass


class TrustManager:
    pass


class X509Certificate(Certificate):

    def __init__(self):
        pass
    
    @abstract
    def checkValidity(self):
        pass
    
    @abstract
    @args(void, [Date])
    def checkValidity(self, date):
        pass
    
    @final
    @static
    @args(X509Certificate, [InputStream])
    def getInstance(inputStream):
        pass
    
    @final
    @static
    @args(X509Certificate, [byte])
    def getInstance(certificateData):
        pass
    
    @abstract
    @args(Principal, [])
    def getIssuerDN(self):
        pass
    
    @abstract
    @args(Date, [])
    def getNotAfter(self):
        pass
    
    @abstract
    @args(Date, [])
    def getNotBefore(self):
        pass
    
    @abstract
    @args(BigInteger, [])
    def getSerialNumber(self):
        pass
    
    @abstract
    @args(String, [])
    def getSigAlgName(self):
        pass
    
    @abstract
    @args(String, [])
    def getSigAlgOID(self):
        pass
    
    @abstract
    @args([byte], [])
    def getSigAlgParams(self):
        pass
    
    @abstract
    @args(Principal, [])
    def getSubjectDN(self):
        pass
    
    @abstract
    @args(int, [])
    def getVersion(self):
        pass
    
