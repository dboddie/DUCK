# Class declarations obtained from Android API documentation licensed under the
# Apache 2.0 license (http://www.apache.org/licenses/LICENSE-2.0) and rewritten
# in syntax that the Python compiler module can parse.

__package__ = "android.telephony"

from java.lang import Enum, Exception, CharSequence, Object, String
from java.util import ArrayList, List
from android.app import PendingIntent
from android.net import Uri
from android.os import Bundle, Parcel, Parcelable

class CdmaCellLocation(CellLocation):

    def __init__(self):
        pass
    
    @args(void, [Bundle])
    def __init__(self, bundle):
        pass
    
    @static
    @args(double, [int])
    def convertQuartSecToDecDegrees(quartSec):
        pass
    
    @args(bool, [Object])
    def equals(self, o):
        pass
    
    @args(void, [Bundle])
    def fillInNotifierBundle(self, bundleToFill):
        pass
    
    @args(int, [])
    def getBaseStationId(self):
        pass
    
    @args(int, [])
    def getBaseStationLatitude(self):
        pass
    
    @args(int, [])
    def getBaseStationLongitude(self):
        pass
    
    @args(int, [])
    def getNetworkId(self):
        pass
    
    @args(int, [])
    def getSystemId(self):
        pass
    
    @args(int, [])
    def hashCode(self):
        pass
    
    @args(void, [int, int, int, int, int])
    def setCellLocationData(self, baseStationId, baseStationLatitude, baseStationLongitude, systemId, networkId):
        pass
    
    @args(void, [int, int, int])
    def setCellLocationData(self, baseStationId, baseStationLatitude, baseStationLongitude):
        pass
    
    def setStateInvalid(self):
        pass
    
    @args(String, [])
    def toString(self):
        pass


class CellInfo(Object):

    __interfaces__ = [Parcelable]
    
    __static_fields__ = {
        "CREATOR": Parcelable.Creator(CellInfo)
        }
    
    @args(int, [])
    def describeContents(self):
        pass
    
    @args(bool, [Object])
    def equals(self, other):
        pass
    
    @args(long, [])
    def getTimeStamp(self):
        pass
    
    @args(int, [])
    def hashCode(self):
        pass
    
    @args(bool, [])
    def isRegistered(self):
        pass
    
    @args(String, [])
    def toString(self):
        pass
    
    @abstract
    @args(void, [Parcel, int])
    def writeToParcel(self, dest, flags):
        pass


class CellLocation(Object):

    def __init__(self):
        pass
    
    @static
    @args(CellLocation, [])
    def getEmpty():
        pass
    
    @static
    def requestLocationUpdate():
        pass


class GsmCellLocation(CellLocation):

    def __init__(self):
        pass
    
    @args(void, [Bundle])
    def __init__(self, bundle):
        pass
    
    @args(bool, [Object])
    def equals(self, o):
        pass
    
    @args(void, [Bundle])
    def fillInNotifierBundle(self, bundleToFill):
        pass
    
    @args(int, [])
    def getCid(self):
        pass
    
    @args(int, [])
    def getLac(self):
        pass
    
    @args(int, [])
    def getPsc(self):
        pass
    
    @args(int, [])
    def hashCode(self):
        pass
    
    @args(void, [int, int])
    def setLacAndCid(self, lac, cid):
        pass
    
    def setStateInvalid(self):
        pass
    
    @args(String, [])
    def toString(self):
        pass


class IccOpenLogicalChannelResponse(Object):

    ### Added in API level 21
    
    INVALID_CHANNEL = -1
    STATUS_MISSING_RESOURCE = 2
    STATUS_NO_ERROR = 1
    STATUS_NO_SUCH_ELEMENT = 3
    STATUS_UNKNOWN_ERROR = 4
    
    __interfaces__ = [Parcelable]
    
    __static_fields__ = {
        "CREATOR": Parcelable.Creator(IccOpenLogicalChannelResponse)
        }
    
    @args(int, [])
    def describeContents(self):
        pass
    
    @args(int, [])
    def getChannel(self):
        pass
    
    @args([byte], [])
    def getSelectResponse(self):
        pass
    
    @args(int, [])
    def getStatus(self):
        pass
    
    @args(String, [])
    def toString(self):
        pass
    
    @abstract
    @args(void, [Parcel, int])
    def writeToParcel(self, dest, flags):
        pass


class NeighboringCellInfo(Object):

    ### Deprecated in API level 23
    
    __interfaces__ = [Parcelable]
    
    __static_fields__ = {
        "CREATOR": Parcelable.Creator(NeighboringCellInfo)
        }
    
    UNKNOWN_CID = -1
    UNKNOWN_RSSI = 99
    
    @args(void, [int, String, int])
    def __init__(self, rssi, location, radioType):
        pass
    
    @args(void, [Parcel])
    def __init__(self, parcel):
        pass
    
    @args(int, [])
    def describeContents(self):
        pass
    
    @args(int, [])
    def getCid(self):
        pass
    
    @args(int, [])
    def getLac(self):
        pass
    
    @args(int, [])
    def getNetworkType(self):
        pass
    
    @args(int, [])
    def getPsc(self):
        pass
    
    @args(int, [])
    def getRssi(self):
        pass
    
    @args(String, [])
    def toString(self):
        pass
    
    @abstract
    @args(void, [Parcel, int])
    def writeToParcel(self, dest, flags):
        pass


class PhoneStateListener(Object):

    LISTEN_CALL_FORWARDING_INDICATOR = 0x008
    LISTEN_CALL_STATE                = 0x020
    LISTEN_CELL_INFO                 = 0x400
    LISTEN_CELL_LOCATION             = 0x010
    LISTEN_DATA_ACTIVITY             = 0x080
    LISTEN_DATA_CONNECTION_STATE     = 0x040
    LISTEN_MESSAGE_WAITING_INDICATOR = 0x004
    LISTEN_NONE                      = 0x000
    LISTEN_SERVICE_STATE             = 0x001
    LISTEN_SIGNAL_STRENGTH           = 0x002
    LISTEN_SIGNAL_STRENGTHS          = 0x100
    
    def __init__(self):
        pass
    
    @args(void, [bool])
    def onCallForwardingIndicatorChanged(self, cfi):
        pass
    
    @args(void, [int, String])
    def onCallStateChanged(self, state, incomingNumber):
        pass
    
    @args(void, [List(CellInfo)])
    def onCellInfoChanged(self, cellInfo):
        pass
    
    @args(void, [CellLocation])
    def onCellLocationChanged(self, location):
        pass
    
    @args(void, [int])
    def onDataActivity(self, direction):
        pass
    
    @args(void, [int])
    def onDataConnectionStateChanged(self, state):
        pass
    
    @args(void, [int, int])
    def onDataConnectionStateChanged(self, state, networkType):
        pass
    
    @args(void, [bool])
    def onMessageWaitingIndicatorChanged(self, mwi):
        pass
    
    @args(void, [ServiceState])
    def onServiceStateChanged(self, serviceState):
        pass
    
    @args(void, [SignalStrength])
    def onSignalStrengthsChanged(self, signalStrength):
        pass


class SmsManager(Object):

    EXTRA_MMS_DATA = "android.telephony.extra.MMS_DATA"
    EXTRA_MMS_HTTP_STATUS = "android.telephony.extra.MMS_HTTP_STATUS"
    MMS_CONFIG_ALIAS_ENABLED = "aliasEnabled"
    MMS_CONFIG_ALIAS_MAX_CHARS = "aliasMaxChars"
    MMS_CONFIG_ALIAS_MIN_CHARS = "aliasMinChars"
    MMS_CONFIG_ALLOW_ATTACH_AUDIO = "allowAttachAudio"
    MMS_CONFIG_APPEND_TRANSACTION_ID = "enabledTransID"
    MMS_CONFIG_EMAIL_GATEWAY_NUMBER = "emailGatewayNumber"
    MMS_CONFIG_GROUP_MMS_ENABLED = "enableGroupMms"
    MMS_CONFIG_HTTP_PARAMS = "httpParams"
    MMS_CONFIG_HTTP_SOCKET_TIMEOUT = "httpSocketTimeout"
    MMS_CONFIG_MAX_IMAGE_HEIGHT = "maxImageHeight"
    MMS_CONFIG_MAX_IMAGE_WIDTH = "maxImageWidth"
    MMS_CONFIG_MAX_MESSAGE_SIZE = "maxMessageSize"
    MMS_CONFIG_MESSAGE_TEXT_MAX_SIZE = "maxMessageTextSize"
    MMS_CONFIG_MMS_DELIVERY_REPORT_ENABLED = "enableMMSDeliveryReports"
    MMS_CONFIG_MMS_ENABLED = "enabledMMS"
    MMS_CONFIG_MMS_READ_REPORT_ENABLED = "enableMMSReadReports"
    MMS_CONFIG_MULTIPART_SMS_ENABLED = "enableMultipartSMS"
    MMS_CONFIG_NAI_SUFFIX = "naiSuffix"
    MMS_CONFIG_NOTIFY_WAP_MMSC_ENABLED = "enabledNotifyWapMMSC"
    MMS_CONFIG_RECIPIENT_LIMIT = "recipientLimit"
    MMS_CONFIG_SEND_MULTIPART_SMS_AS_SEPARATE_MESSAGES = "sendMultipartSmsAsSeparateMessages"
    MMS_CONFIG_SHOW_CELL_BROADCAST_APP_LINKS = "config_cellBroadcastAppLinks"
    MMS_CONFIG_SMS_DELIVERY_REPORT_ENABLED = "enableSMSDeliveryReports"
    MMS_CONFIG_SMS_TO_MMS_TEXT_LENGTH_THRESHOLD = "smsToMmsTextLengthThreshold"
    MMS_CONFIG_SMS_TO_MMS_TEXT_THRESHOLD = "smsToMmsTextThreshold"
    MMS_CONFIG_SUBJECT_MAX_LENGTH = "maxSubjectLength"
    MMS_CONFIG_SUPPORT_HTTP_CHARSET_HEADER = "supportHttpCharsetHeader"
    MMS_CONFIG_SUPPORT_MMS_CONTENT_DISPOSITION = "supportMmsContentDisposition"
    MMS_CONFIG_UA_PROF_TAG_NAME = "uaProfTagName"
    MMS_CONFIG_UA_PROF_URL = "uaProfUrl"
    MMS_CONFIG_USER_AGENT = "userAgent"
    MMS_ERROR_CONFIGURATION_ERROR = 7
    MMS_ERROR_HTTP_FAILURE = 4
    MMS_ERROR_INVALID_APN = 2
    MMS_ERROR_IO_ERROR = 5
    MMS_ERROR_NO_DATA_NETWORK = 8
    MMS_ERROR_RETRY = 6
    MMS_ERROR_UNABLE_CONNECT_MMS = 3
    MMS_ERROR_UNSPECIFIED = 1
    RESULT_ERROR_GENERIC_FAILURE = 1
    RESULT_ERROR_NO_SERVICE = 4
    RESULT_ERROR_NULL_PDU = 3
    RESULT_ERROR_RADIO_OFF = 2
    STATUS_ON_ICC_FREE = 0
    STATUS_ON_ICC_READ = 1
    STATUS_ON_ICC_SENT = 5
    STATUS_ON_ICC_UNREAD = 3
    STATUS_ON_ICC_UNSENT = 7
    
    @args(ArrayList(String), [String])
    def divideMessage(self, text):
        pass
    
    @args(void, [Context, String, Uri, Bundle, PendingIntent])
    def downloadMultimediaMessage(self, context, locationUrl, contentUri, configOverrides, downloadedIntent):
        pass
    
    @args(Bundle, [])
    def getCarrierConfigValues(self):
        pass
    
    @static
    @args(SmsManager, [])
    def getDefault():
        pass
    
    @static
    @args(int, [])
    def getDefaultSmsSubscriptionId():
        pass
    
    @static
    @args(SmsManager, [int])
    def getSmsManagerForSubscriptionId(subId):
        pass
    
    @args(int, [])
    def getSubscriptionId(self):
        pass
    
    @args(void, [[byte], String, PendingIntent])
    def injectSmsPdu(self, pdu, format, receivedIntent):
        pass
    
    @permissions(android.permission.SEND_SMS)
    @args(void, [String, String, short, [byte], PendingIntent, PendingIntent])
    def sendDataMessage(self, destinationAddress, scAddress, destinationPort, data, sentIntent, deliveryIntent):
        pass
    
    @args(void, [Context, Uri, String, Bundle, PendingIntent])
    def sendMultimediaMessage(self, context, contentUri, locationUrl, configOverrides, sentIntent):
        pass
    
    @permissions(android.permission.SEND_SMS)
    @args(void, [String, String, ArrayList(String), ArrayList(PendingIntent), ArrayList(PendingIntent)])
    def sendMultipartTextMessage(self, destinationAddress, scAddress, parts, sentIntents, deliveryIntents):
        pass
    
    @permissions(android.permission.SEND_SMS)
    @args(void, [String, String, String, PendingIntent, PendingIntent])
    def sendTextMessage(self, destinationAddress, scAddress, text, sentIntent, deliveryIntent):
        pass


class SmsMessage(Object):

    ENCODING_16BIT = 3
    ENCODING_7BIT = 1
    ENCODING_8BIT = 2
    ENCODING_UNKNOWN = 0
    MAX_USER_DATA_BYTES = 140
    MAX_USER_DATA_BYTES_WITH_HEADER = 134
    MAX_USER_DATA_SEPTETS = 160
    MAX_USER_DATA_SEPTETS_WITH_HEADER = 153
    
    class MessageClass(Enum):
    
        __static_fields__ = {
            "CLASS_0": SmsMessage.MessageClass,
            "CLASS_1": SmsMessage.MessageClass,
            "CLASS_2": SmsMessage.MessageClass,
            "CLASS_3": SmsMessage.MessageClass,
            "UNKNOWN": SmsMessage.MessageClass
            }
    
        @static
        @args(SmsMessage.MessageClass, [String])
        def valueOf(name):
            pass
        
        @final
        @static
        @args([SmsMessage.MessageClass], [])
        def values():
            pass
    
    class SubmitPdu(Object):
    
        __fields__ = {
            "encodedMessage": [byte],
            "encodedScAddress": [byte]
            }
    
    @static
    @args([int], [CharSequence, bool])
    def calculateLength(self, msgBody, use7bitOnly):
        pass
    
    @static
    @args([int], [String, bool])
    def calculateLength(messageBody, use7bitOnly):
        pass
    
    @static
    @args(SmsMessage, [[byte], String])
    def createFromPdu(pdu, format):
        pass
    
    @static
    @args(SmsMessage, [[byte]])
    def createFromPdu(pdu):
        pass
    
    @args(String, [])
    def getDisplayMessageBody(self):
        pass
    
    @args(String, [])
    def getDisplayOriginatingAddress(self):
        pass
    
    @args(String, [])
    def getEmailBody(self):
        pass
    
    @args(String, [])
    def getEmailFrom(self):
        pass
    
    @args(int, [])
    def getIndexOnIcc(self):
        pass
    
    @args(int, [])
    def getIndexOnSim(self):
        pass
    
    @args(String, [])
    def getMessageBody(self):
        pass
    
    @args(SmsMessage.MessageClass, [])
    def getMessageClass(self):
        pass
    
    @args(String, [])
    def getOriginatingAddress(self):
        pass
    
    @args([byte], [])
    def getPdu(self):
        pass
    
    @args(int, [])
    def getProtocolIdentifier(self):
        pass
    
    @args(String, [])
    def getPseudoSubject(self):
        pass
    
    @args(String, [])
    def getServiceCenterAddress(self):
        pass
    
    @args(int, [])
    def getStatus(self):
        pass
    
    @args(int, [])
    def getStatusOnIcc(self):
        pass
    
    @args(int, [])
    def getStatusOnSim(self):
        pass
    
    @static
    @args(SmsMessage.SubmitPdu, [String, String, short, [byte], bool])
    def getSubmitPdu(scAddress, destinationAddress, destinationPort, data, statusReportRequested):
        pass
    
    @static
    @args(SmsMessage.SubmitPdu, [String, String, String, bool])
    def getSubmitPdu(scAddress, destinationAddress, message, statusReportRequested):
        pass
    
    @static
    @args(int, [String])
    def getTPLayerLengthForPDU(pdu):
        pass
    
    @args(long, [])
    def getTimestampMillis(self):
        pass
    
    @args([byte], [])
    def getUserData(self):
        pass
    
    @args(bool, [])
    def isCphsMwiMessage(self):
        pass
    
    @args(bool, [])
    def isEmail(self):
        pass
    
    @args(bool, [])
    def isMWIClearMessage(self):
        pass
    
    @args(bool, [])
    def isMWISetMessage(self):
        pass
    
    @args(bool, [])
    def isMwiDontStore(self):
        pass
    
    @args(bool, [])
    def isReplace(self):
        pass
    
    @args(bool, [])
    def isReplyPathPresent(self):
        pass
    
    @args(bool, [])
    def isStatusReportMessage(self):
        pass


class TelephonyManager(Object):

    ACTION_CONFIGURE_VOICEMAIL = "android.telephony.action.CONFIGURE_VOICEMAIL"
    ACTION_PHONE_STATE_CHANGED = "android.intent.action.PHONE_STATE"
    ACTION_RESPOND_VIA_MESSAGE = "android.intent.action.RESPOND_VIA_MESSAGE"
    CALL_STATE_IDLE = 0
    CALL_STATE_OFFHOOK = 2
    CALL_STATE_RINGING = 1
    DATA_ACTIVITY_DORMANT = 4
    DATA_ACTIVITY_IN = 1
    DATA_ACTIVITY_INOUT = 3
    DATA_ACTIVITY_NONE = 0
    DATA_ACTIVITY_OUT = 2
    DATA_CONNECTED = 2
    DATA_CONNECTING = 1
    DATA_DISCONNECTED = 0
    DATA_SUSPENDED = 3
    EXTRA_INCOMING_NUMBER = "incoming_number"
    EXTRA_STATE = "state"
    NETWORK_TYPE_1xRTT = 7
    NETWORK_TYPE_CDMA = 4
    NETWORK_TYPE_EDGE = 2
    NETWORK_TYPE_EHRPD = 14
    NETWORK_TYPE_EVDO_0 = 5
    NETWORK_TYPE_EVDO_A = 6
    NETWORK_TYPE_EVDO_B = 12
    NETWORK_TYPE_GPRS = 1
    NETWORK_TYPE_HSDPA = 8
    NETWORK_TYPE_HSPA = 10
    NETWORK_TYPE_HSPAP = 15
    NETWORK_TYPE_HSUPA = 9
    NETWORK_TYPE_IDEN = 11
    NETWORK_TYPE_LTE = 13
    NETWORK_TYPE_UMTS = 3
    NETWORK_TYPE_UNKNOWN = 0
    PHONE_TYPE_CDMA = 2
    PHONE_TYPE_GSM = 1
    PHONE_TYPE_NONE = 0
    PHONE_TYPE_SIP = 3
    SIM_STATE_ABSENT = 1
    SIM_STATE_NETWORK_LOCKED = 4
    SIM_STATE_PIN_REQUIRED = 2
    SIM_STATE_PUK_REQUIRED = 3
    SIM_STATE_READY = 5
    SIM_STATE_UNKNOWN = 0
    VVM_TYPE_CVVM = "vvm_type_cvvm"
    VVM_TYPE_OMTP = "vvm_type_omtp"
    
    __static_fields__ = {
        "EXTRA_STATE_IDLE": String,
        "EXTRA_STATE_OFFHOOK": String,
        "EXTRA_STATE_RINGING": String
        }

    @api(23)
    @args(bool, [])
    def canChangeDtmfToneLength(self):
        pass
    
    @api(17)
    @permissions(android.permission.ACCESS_COARSE_LOCATION)
    @args(List(CellInfo), [])
    def getAllCellInfo(self):
        pass
    
    @args(int, [])
    def getCallState(self):
        pass
    
    @permissions(android.permission.ACCESS_COARSE_LOCATION,
                 android.permission.ACCESS_FINE_LOCATION)
    @args(CellLocation, [])
    def getCellLocation(self):
        pass
    
    @args(int, [])
    def getDataActivity(self):
        pass
    
    @args(int, [])
    def getDataState(self):
        pass
    
    @permissions(android.permission.READ_PHONE_STATE)
    @args(String, [])
    def getDeviceId(self):
        pass
    
    @permissions(android.permission.READ_PHONE_STATE)
    @args(String, [int])
    def getDeviceId(self, slotId):
        pass
    
    @permissions(android.permission.READ_PHONE_STATE)
    @args(String, [])
    def getDeviceSoftwareVersion(self):
        pass
    
    @api(18)
    @permissions(android.permission.READ_PHONE_STATE)
    @args(String, [])
    def getGroupIdLevel1(self):
        pass
    
    @permissions(android.permission.READ_PHONE_STATE, android.permission.READ_SMS)
    @args(String, [])
    def getLine1Number(self):
        pass
    
    @api(19)
    @args(String, [])
    def getMmsUAProfUrl(self):
        pass
    
    @api(19)
    @args(String, [])
    def getMmsUserAgent(self):
        pass
    
    @api(3, 23)
    @permissions(android.permission.ACCESS_COARSE_LOCATION)
    @args(List(NeighboringCellInfo), [])
    def getNeighboringCellInfo(self):
        pass
    
    @args(String, [])
    def getNetworkCountryIso(self):
        pass
    
    @args(String, [])
    def getNetworkOperator(self):
        pass
    
    @args(String, [])
    def getNetworkOperatorName(self):
        pass
    
    @args(int, [])
    def getNetworkType(self):
        pass
    
    @args(int, [])
    def getPhoneCount(self):
        pass
    
    @args(int, [])
    def getPhoneType(self):
        pass
    
    @args(String, [])
    def getSimCountryIso(self):
        pass
    
    @args(String, [])
    def getSimOperator(self):
        pass
    
    @args(String, [])
    def getSimOperatorName(self):
        pass
    
    @permissions(android.permission.READ_PHONE_STATE)
    @args(String, [])
    def getSimSerialNumber(self):
        pass
    
    @args(int, [])
    def getSimState(self):
        pass
    
    @permissions(android.permission.READ_PHONE_STATE)
    @args(String, [])
    def getSubscriberId(self):
        pass
    
    @permissions(android.permission.READ_PHONE_STATE)
    @args(String, [])
    def getVoiceMailAlphaTag(self):
        pass
    
    @permissions(android.permission.READ_PHONE_STATE)
    @args(String, [])
    def getVoiceMailNumber(self):
        pass
    
    @api(22)
    @args(bool, [])
    def hasCarrierPrivileges(self):
        pass
    
    @api(5)
    @args(bool, [])
    def hasIccCard(self):
        pass
    
    @api(21)
    @permissions(android.permission.MODIFY_PHONE_STATE)
    @args(bool, [int])
    def iccCloseLogicalChannel(self, channel):
        pass
    
    @api(21)
    @permissions(android.permission.MODIFY_PHONE_STATE)
    @args([byte], [int, int, int, int, int, String])
    def iccExchangeSimIO(self, fileID, command, p1, p2, p3, filePath):
        pass
    
    @api(21)
    @permissions(android.permission.MODIFY_PHONE_STATE)
    @args(IccOpenLogicalChannelResponse, [String])
    def iccOpenLogicalChannel(self, AID):
        pass
    
    @api(21)
    @permissions(android.permission.MODIFY_PHONE_STATE)
    @args(String, [int, int, int, int, int, String])
    def iccTransmitApduBasicChannel(self, cla, instruction, p1, p2, p3, data):
        pass
    
    @api(21)
    @permissions(android.permission.MODIFY_PHONE_STATE)
    @args(String, [int, int, int, int, int, int, String])
    def iccTransmitApduLogicalChannel(self, channel, cla, instruction, p1, p2, p3, data):
        pass
    
    @api(23)
    @args(bool, [])
    def isHearingAidCompatibilitySupported(self):
        pass
    
    @args(bool, [])
    def isNetworkRoaming(self):
        pass
    
    @api(21)
    @args(bool, [])
    def isSmsCapable(self):
        pass
    
    @api(23)
    @args(bool, [])
    def isTtyModeSupported(self):
        pass
    
    @api(22)
    @args(bool, [])
    def isVoiceCapable(self):
        pass
    
    @api(23)
    @args(bool, [])
    def isWorldPhone(self):
        pass
    
    @args(void, [PhoneStateListener, int])
    def listen(self, listener, events):
        pass
    
    @api(21)
    @permissions(android.permission.MODIFY_PHONE_STATE)
    @args(String, [String])
    def sendEnvelopeWithStatus(self, content):
        pass
    
    @api(22)
    @args(bool, [String, String])
    def setLine1NumberForDisplay(self, alphaTag, number):
        pass
    
    @api(22)
    @args(bool, [String])
    def setOperatorBrandOverride(self, brand):
        pass
    
    @api(22)
    @args(bool, [])
    def setPreferredNetworkTypeToGlobal(self):
        pass
    
    @api(22)
    @args(bool, [String, String])
    def setVoiceMailNumber(self, alphaTag, number):
        pass
