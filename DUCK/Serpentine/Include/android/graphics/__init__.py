# Class declarations obtained from Android API documentation licensed under the
# Apache 2.0 license (http://www.apache.org/licenses/LICENSE-2.0) and rewritten
# in syntax that the Python compiler module can parse.

__package__ = "android.graphics"

from java.io import OutputStream
from java.lang import Enum, Locale, Object, String
from java.nio import Buffer
from android.content.res import Resources
from android.os import Handler, Parcel, Parcelable
from android.util import DisplayMetrics

class Bitmap(Object):

    __interfaces__ = [Parcelable]
    
    class CompressFormat(Enum):
    
        __static_fields__ = {
            "JPEG": Bitmap.CompressFormat,
            "PNG": Bitmap.CompressFormat,
            "WEBP": Bitmap.CompressFormat   # api(14)
            }
    
    class Config(Enum):
    
        __static_fields__ = {
            "ALPHA_8": Bitmap.Config,
            "ARGB_4444": Bitmap.Config,
            "ARGB_8888": Bitmap.Config,
            "RGB_565": Bitmap.Config
            }
    
    @args(bool, [Bitmap.CompressFormat, int, OutputStream])
    def compress(self, format, quality, stream):
        pass
    
    @args(Bitmap, [Bitmap.Config, bool])
    def copy(self, config, isMutable):
        pass
    
    @args(void, [Buffer])
    def copyPixelsFromBuffer(self, src):
        pass
    
    @args(void, [Buffer])
    def copyPixelsToBuffer(self, dst):
        pass
    
    @api(17)
    @static
    @args(Bitmap, [DisplayMetrics, [int], int, int, Bitmap.Config])
    def createBitmap(display, colors, width, height, config):
        pass
    
    @api(17)
    @static
    @args(Bitmap, [DisplayMetrics, [int], int, int, int, int, Bitmap.Config])
    def createBitmap(display, colors, offset, stride, width, height, config):
        pass
    
    @static
    @args(Bitmap, [Bitmap, int, int, int, int])
    def createBitmap(source, x, y, width, height):
        pass
    
    @static
    @args(Bitmap, [Bitmap])
    def createBitmap(src):
        pass
    
    @api(17)
    @static
    @args(Bitmap, [DisplayMetrics, int, int, Bitmap.Config])
    def createBitmap(display, width, height, config):
        pass
    
    @static
    @args(Bitmap, [Bitmap, int, int, int, int, Matrix, bool])
    def createBitmap(source, x, y, width, height, m, filter):
        pass
    
    @static
    @args(Bitmap, [int, int, Bitmap.Config])
    def createBitmap(width, height, config):
        pass
    
    @static
    @args(Bitmap, [[int], int, int, int, int, Bitmap.Config])
    def createBitmap(colors, offset, stride, width, height, config):
        pass
    
    @static
    @args(Bitmap, [[int], int, int, Bitmap.Config])
    def createBitmap(colors, width, height, config):
        pass
    
    @static
    @args(Bitmap, [Bitmap, int, int, bool])
    def createScaledBitmap(src, dstWidth, dstHeight, filter):
        pass
    
    @args(int, [])
    def describeContents(self):
        pass
    
    @args(void, [int])
    def eraseColor(self, c):
        pass
    
    @args(Bitmap, [])
    def extractAlpha(self):
        pass
    
    @args(Bitmap, [Paint, [int]])
    def extractAlpha(self, paint, offsetXY):
        pass
    
    @api(19)
    @final
    @args(int, [])
    def getAllocationByteCount(self):
        pass
    
    @api(12)
    @final
    @args(int, [])
    def getByteCount(self):
        pass
    
    @final
    @args(Bitmap.Config, [])
    def getConfig(self):
        pass
    
    @api(4)
    @args(int, [])
    def getDensity(self):
        pass
    
    @api(12)
    @args(int, [])
    def getGenerationId(self):
        pass
    
    @final
    @args(int, [])
    def getHeight(self):
        pass
    
    @args([byte], [])
    def getNinePatchChunk(self):
        pass
    
    @args(int, [int, int])
    def getPixel(self, x, y):
        pass
    
    @args(void, [[int], int, int, int, int, int, int])
    def getPixels(self, pixels, offset, stride, x, y, width, height):
        pass
    
    @final
    @args(int, [])
    def getRowBytes(self):
        pass
    
    @api(4)
    @args(int, [DisplayMetrics])
    def getScaledHeight(self, metrics):
        pass
    
    @api(4)
    @args(int, [int])
    def getScaledHeight(self, targetDensity):
        pass
    
    @api(4)
    @args(int, [Canvas])
    def getScaledHeight(self, canvas):
        pass
    
    @api(4)
    @args(int, [int])
    def getScaledWidth(self, targetDensity):
        pass
    
    @api(4)
    @args(int, [DisplayMetrics])
    def getScaledWidth(self, metrics):
        pass
    
    @api(4)
    @args(int, [Canvas])
    def getScaledWidth(self, canvas):
        pass
    
    @final
    @args(int, [])
    def getWidth(self):
        pass
    
    @final
    @args(bool, [])
    def hasAlpha(self):
        pass
    
    @api(17)
    @final
    @args(bool, [])
    def hasMipMap(self):
        pass
    
    @final
    @args(bool, [])
    def isMutable(self):
        pass
    
    @api(17)
    @final
    @args(bool, [])
    def isPremultiplied(self):
        pass
    
    @final
    @args(bool, [])
    def isRecycled(self):
        pass
    
    def prepareToDraw(self):
        pass
    
    @api(19)
    @args(void, [int, int, Bitmap.Config])
    def reconfigure(self, width, height, config):
        pass
    
    def recycle(self):
        pass
    
    @api(12)
    @args(bool, [Bitmap])
    def sameAs(self,  other):
        pass
    
    @api(19)
    @args(void, [Bitmap.Config])
    def setConfig(self, config):
        pass
    
    @api(4)
    @args(void, [int])
    def setDensity(self, density):
        pass
    
    @api(12)
    @args(void, [bool])
    def setHasAlpha(self, hasAlpha):
        pass
    
    @api(17)
    @final
    @args(void, [bool])
    def setHasMipMap(self, hasMipMap):
        pass
    
    @api(19)
    @args(void, [int])
    def setHeight(self, height):
        pass
    
    @args(void, [int, int, int])
    def setPixel(self, x, y, color):
        pass
    
    @args(void, [[int], int, int, int, int, int, int])
    def setPixels(self, pixels, offset, stride, x, y, width, height):
        pass
    
    @api(19)
    @final
    @args(void, [bool])
    def setPremultiplied(self, premultiplied):
        pass
    
    @api(19)
    @args(void, [int])
    def setWidth(self, width):
        pass
    
    @args(void, [Parcel, int])
    def writeToParcel(self, p, flags):
        pass


class BitmapFactory(Object):

    class Options(Object):
    
        __fields__ = {
            "inBitmap": Bitmap,
            "inDensity": int,
            "inDither": bool,
            "inInputShareable": bool,
            "inJustDecodeBounds": bool,
            "inMutable": bool,
            "inPreferQualityOverSpeed": bool,
            "inPreferredConfig": Bitmap.Config,
            "inPremultiplied": bool,
            "inPurgeable": bool,
            "inSampleSize": int,
            "inScaled": bool,
            "inScreenDensity": int,
            "inTargetDensity": int,
            "inTempStorage": [byte],
            "mCancel": bool,
            "outHeight": int,
            "outMimeType": String,
            "outWidth": int
            }
        
        def __init__(self):
            pass
        
        def requestCancelDecode(self):
            pass
    
    def __init__(self):
        pass
    
    @static
    @args(Bitmap, [[byte], int, int, BitmapFactory.Options])
    def decodeByteArray(data, offset, length, options):
        pass
    
    @static
    @args(Bitmap, [[byte], int, int])
    def decodeByteArray(data, offset, length):
        pass
    
    @static
    @args(Bitmap, [String])
    def decodeFile(pathName):
        pass
    
    @static
    @args(Bitmap, [String, BitmapFactory.Options])
    def decodeFile(pathName, opts):
        pass
    
    @static
    @args(Bitmap, [Resources, int, BitmapFactory.Options])
    def decodeResource(res, id, opts):
        pass
    
    @static
    @args(Bitmap, [Resources, int])
    def decodeResource(res, id):
        pass


class Canvas(Object):

    def __init__(self):
        pass
    
    @args(void, [Bitmap])
    def __init__(self, bitmap):
        pass
    
    @args(void, [Bitmap, Rect, RectF, Paint])
    def drawBitmap(self, bitmap, src, dest, paint):
        pass
    
    @args(void, [Bitmap, float, float, Paint])
    def drawBitmap(self, bitmap, left, top, paint):
        pass
    
    @args(void, [Bitmap, Rect, Rect, Paint])
    def drawBitmap(self, bitmap, src, dest, paint):
        pass
    
    @args(void, [float, float, float, Paint])
    def drawCircle(self, cx, cy, radius, paint):
        pass

    @args(void, [int])
    def drawColor(self, color):
        pass
    
    @args(void, [float, float, float, float, Paint])
    def drawLine(self, x1, y1, x2, y2, paint):
        pass
    
    @args(void, [Paint])
    def drawPaint(self, paint):
        pass
    
    @args(void, [Path, Paint])
    def drawPath(self, path, paint):
        pass
    
    @args(void, [float, float, Paint])
    def drawPoint(self, x, y, paint):
        pass
    
    @args(void, [[float], int, int, Paint])
    def drawPoints(self, points, offset, count, paint):
        pass
    
    @args(void, [[float], Paint])
    def drawPoints(self, points, paint):
        pass
    
    @args(void, [[char], int, int, [float], Paint])
    def drawPosText(self, text, index, count, positions, paint):
        pass
    
    @args(void, [[char], [float], Paint])
    def drawPosText(self, text, positions, paint):
        pass
    
    @args(void, [int, int, int])
    def drawRGB(self, red, green, blue):
        pass
    
    @args(void, [float, float, float, float, Paint])
    def drawRect(self, left, top, right, bottom, paint):
        pass
    
    @args(void, [RectF, Paint])
    def drawRect(self, rect, paint):
        pass
    
    @args(void, [Rect, Paint])
    def drawRect(self, rect, paint):
        pass
    
    @args(void, [float, float, float, float, float, float, Paint])
    def drawRoundRect(self, left, top, right, bottom, rx, ry, paint):
        pass
    
    @args(void, [RectF, float, float, Paint])
    def drawRoundRect(self, rect, rx, ry, paint):
        pass
    
    @args(void, [String, float, float, Paint])
    def drawText(self, text, x, y, paint):
        pass
    
    @args(void, [CharSequence, int, int, float, float, Paint])
    def drawText(self, text, start, end, x, y, paint):
        pass
    
    @args(void, [[char], int, int, float, float, Paint])
    def drawText(self, text, index, count, x, y, paint):
        pass
    
    @args(void, [String, int, int, float, float, Paint])
    def drawText(self, text, start, end, x, y, paint):
        pass
    
    ### ...
    
    @args(int, [])
    def getHeight(self):
        pass
    
    @args(int, [])
    def getWidth(self):
        pass
    
    ### ...
    
    @args(void, [float])
    def rotate(self, degrees):
        pass
    
    @args(void, [float, float, float])
    def rotate(self, degrees, px, py):
        pass
    
    @args(void, [float])
    def rotate(self, degrees):
        pass
    
    ### save ... saveLayerAlpha
    
    @args(void, [float, float])
    def scale(self, sx, sy):
        pass
    
    @args(void, [float, float, float, float])
    def scale(self, sx, sy, px, py):
        pass
    
    ### setBitmap ... skew
    
    @args(void, [float, float])
    def translate(self, dx, dy):
        pass


class Color(Object):

    BLACK       = 0xff000000
    BLUE        = 0xff0000ff
    CYAN        = 0xff00ffff
    DKGRAY      = 0xff444444
    GRAY        = 0xff888888
    GREEN       = 0xff00ff00
    LTGRAY      = 0xffcccccc
    MAGENTA     = 0xffff00ff
    RED         = 0xffff0000
    TRANSPARENT = 0x00000000
    WHITE       = 0xffffffff
    YELLOW      = 0xffffff00
    
    def __init__(self):
        pass
    
    @static
    @args(int, [[float]])
    def HSVToColor(hsv):
        pass
    
    @static
    @args(int, [int, [float]])
    def HSVToColor(alpha, hsv):
        pass
    
    @static
    @args(void, [int, int, int, [float]])
    def RGBToHSV(red, green, blue, hsv):
        pass
    
    @static
    @args(int, [int])
    def alpha(color):
        pass
    
    @static
    @args(int, [int, int, int, int])
    def argb(alpha, red, green, blue):
        pass
    
    @static
    @args(int, [int])
    def blue(color):
        pass
    
    @static
    @args(void, [int, [float]])
    def colorToHSV(color, hsv):
        pass
    
    @static
    @args(int, [int])
    def green(color):
        pass
    
    @static
    @args(int, [String])
    def parseColor(colorString):
        pass
    
    @static
    @args(int, [int])
    def red(color):
        pass
    
    @static
    @args(int, [int, int, int])
    def rgb(red, green, blue):
        pass
    
    
class Paint(Object):

    ANTI_ALIAS_FLAG = 1
    DITHER_FLAG = 4
    EMBEDDED_BITMAP_TEXT_FLAG = 1024
    FAKE_BOLD_TEXT_FLAG = 32
    FILTER_BITMAP_FLAG = 2
    HINTING_OFF = 0
    HINTING_ON = 1
    LINEAR_TEXT_FLAG = 64
    STRIKE_THRU_TEXT_FLAG = 16
    SUBPIXEL_TEXT_FLAG = 128
    UNDERLINE_TEXT_FLAG = 8
    
    class Align(Enum):
    
        __static_fields__ = {"CENTER": Paint.Align, "LEFT": Paint.Align,
                             "RIGHT": Paint.Align}
        
        @static
        @args(Paint.Align, [String])
        def valueOf(name):
            pass
        
        @static
        @args([Paint.Align], [])
        def values():
            pass
    
    class Cap(Enum):
    
        __static_fields__ = {"BUTT": Paint.Cap, "ROUND": Paint.Cap,
                             "SQUARE": Paint.Cap}
        
        @static
        @args(Paint.Cap, [String])
        def valueOf(name):
            pass
        
        @static
        @args([Paint.Cap], [])
        def values():
            pass
    
    class FontMetrics(Object):
    
        __fields__ = {"ascent": float, "bottom": float, "descent": float,
                      "leading": float, "top": float}
        
        def __init__(self):
            pass
    
    class FontMetricsInt(Object):
    
        __fields__ = {"ascent": int, "bottom": int, "descent": int,
                      "leading": int, "top": int}
        
        def __init__(self):
            pass
    
    class Join(Enum):
    
        __static_fields__ = {"BEVEL": Paint.Join, "MITER": Paint.Join,
                             "ROUND": Paint.Join}
        @static
        @args(Paint.Join, [String])
        def valueOf(name):
            pass
        
        @static
        @args([Paint.Join], [])
        def values():
            pass
    
    class Style(Enum):
    
        __static_fields__ = {"FILL": Paint.Style,
                             "FILL_AND_STROKE": Paint.Style,
                             "STROKE": Paint.Style}
        @static
        @args(Paint.Style, [String])
        def valueOf(name):
            pass
        
        @static
        @args([Paint.Style], [])
        def values():
            pass
    
    def __init__(self):
        pass
    
    @args(void, [int])
    def __init__(self, flags):
        pass
    
    @args(void, [Paint])
    def __init__(self, paint):
        pass
    
    @args(float, [])
    def ascent(self):
        pass
    
    @args(int, [[char], int, int, float, [float]])
    def breakText(self, text, index, count, maxWidth, measuredWidth):
        pass
    
    @args(int, [CharSequence, int, int, bool, float, [float]])
    def breakText(self, text, start, end, measureForwards, maxWidth, measuredWidth):
        pass
    
    @args(int, [String, bool, float, [float]])
    def breakText(self, text, measureForwards, maxWidth, measuredWidth):
        pass
    
    @args(void, [])
    def clearShadowLayer(self):
        pass
    
    @args(float, [])
    def descent(self):
        pass
    
    @args(int, [])
    def getAlpha(self):
        pass
    
    @args(int, [])
    def getColor(self):
        pass
    
    ### getColorFilter ... getFillPath
    
    @args(int, [])
    def getFlags(self):
        pass
    
    @args(String, [])
    def getFontFeatureSettings(self):
        pass
    
    @args(float, [Paint.FontMetrics])
    def getFontMetrics(self, metrics):
        pass
    
    @args(Paint.FontMetrics, [])
    def getFontMetrics(self):
        pass
    
    @args(Paint.FontMetricsInt, [])
    def getFontMetricsInt(self):
        pass
    
    @args(int, [Paint.FontMetricsInt])
    def getFontMetricsInt(self, metrics):
        pass
    
    @args(float, [])
    def getFontSpacing(self):
        pass
    
    @args(int, [])
    def getHinting(self):
        pass
    
    @args(float, [])
    def getLetterSpacing(self):
        pass
    
    ### getMaskFilter ... getShader
    
    @args(Paint.Cap, [])
    def getStrokeCap(self):
        pass
    
    @args(Paint.Join, [])
    def getStrokeJoin(self):
        pass
    
    @args(float, [])
    def getStrokeMiter(self):
        pass
    
    @args(float, [])
    def getStrokeWidth(self):
        pass
    
    @args(Paint.Style, [])
    def getStyle(self):
        pass
    
    @args(Paint.Align, [])
    def getTextAlign(self):
        pass
    
    ### getTextBounds
    
    @args(Locale, [])
    def getTextLocale(self):
        pass
    
    ### getTextPath ... getTypeface
    
    @args(Xfermode, [])
    def getXfermode(self):
        pass
    
    @args(bool, [String])
    def hasGlyph(self):
        pass
    
    @args(bool, [])
    def isAntiAlias(self):
        pass
    
    @args(bool, [])
    def isDither(self):
        pass
    
    ### isElegantTextHeight ... isUnderlineText
    
    @args(float, [String])
    def measureText(self, text):
        pass
    
    @args(float, [CharSequence, int, int])
    def measureText(self, text, start, end):
        pass
    
    @args(float, [String, int, int])
    def measureText(self, text, start, end):
        pass
    
    @args(float, [[char], int, int])
    def measureText(self, text, index, count):
        pass
    
    def reset(self):
        pass
    
    @args(void, [Paint])
    def set(self, source):
        pass
    
    @args(void, [int, int, int, int])
    def setARGB(self, alpha, red, green, blue):
        pass
    
    @args(void, [int])
    def setAlpha(self, alpha):
        pass
    
    @args(void, [bool])
    def setAntiAlias(self, enable):
        pass
    
    @args(void, [int])
    def setColor(self, color):
        pass
    
    ### setColorFilter ... setFakeBoldText
    
    @args(void, [bool])
    def setFilterBitmap(self, filter):
        pass
    
    @args(void, [int])
    def setFlags(self, flags):
        pass
    
    ### setFontFeatureSettings ... setStrikeThruText
    
    @args(void, [Paint.Cap])
    def setStrokeCap(self, cap):
        pass
    
    @args(void, [Paint.Join])
    def setStrokeJoin(self, join):
        pass
    
    @args(void, [float])
    def setStrokeMiter(self, miter):
        pass
    
    @args(void, [float])
    def setStrokeWidth(self, width):
        pass
    
    @args(void, [Paint.Style])
    def setStyle(self, style):
        pass
    
    @args(void, [bool])
    def setSubpixelText(self, subpixelText):
        pass
    
    @args(void, [Paint.Align])
    def setTextAlign(self, align):
        pass
    
    @args(void, [Locale])
    def setTextLocale(self, locale):
        pass
    
    @args(void, [float])
    def setTextScaleX(self, scaleX):
        pass
    
    @args(void, [float])
    def setTextSize(self, textSize):
        pass
    
    @args(void, [float])
    def setTextSkewX(self, skewX):
        pass
    
    @args(Typeface, [Typeface])
    def setTypeface(self, textSize):
        pass
    
    @args(void, [bool])
    def setUnderlineText(self, underlineText):
        pass
    
    @args(Xfermode, [Xfermode])
    def setXfermode(self, mode):
        pass


class Path(Object):

    class Direction(Enum):
    
        __static_fields__ = {"CCW": Path.Direction, "CW": Path.Direction}
        
        @static
        @args(Path.Direction, [String])
        def valueOf(name):
            pass
        
        @static
        @args([Path.Direction], [])
        def values():
            pass
    
    class FillType(Enum):
    
        __static_fields__ = {"EVEN_ODD": Path.FillType,
                             "INVERSE_EVEN_ODD": Path.FillType,
                             "INVERSE_WINDING": Path.FillType,
                             "WINDING": Path.FillType}
        @static
        @args(Path.FillType, [String])
        def valueOf(name):
            pass
        
        @static
        @args([Path.FillType], [])
        def values():
            pass
    
    class Op(Enum):
    
        __static_fields__ = {"DIFFERENCE": Path.Op,
                             "INTERSECT": Path.Op,
                             "REVERSE_DIFFERENCE": Path.Op,
                             "UNION": Path.Op,
                             "XOR": Path.Op}
        @static
        @args(Path.Op, [String])
        def valueOf(name):
            pass
        
        @static
        @args([Path.Op], [])
        def values():
            pass
    
    def __init__(self):
        pass
    
    @args(void, [Path])
    def __init__(self, source):
        pass
    
    @args(void, [RectF, float, float])
    def addArc(self, oval, startAngle, sweepAngle):
        pass
    
    @args(void, [float, float, float, float, float, float])
    def addArc(self, left, top, right, bottom, startAngle, sweepAngle):
        pass
    
    @args(void, [float, float, float, Path.Direction])
    def addCircle(self, x, y, radius, direction):
        pass
    
    @args(void, [float, float, float, Path.Direction])
    def addOval(self, x, y, radius, direction):
        pass
    
    @args(void, [RectF, Path.Direction])
    def addOval(self, oval, direction):
        pass
    
    @args(void, [Path, float, float])
    def addPath(self, path, dx, dy):
        pass
    
    @args(void, [Path])
    def addPath(self, path):
        pass
    
    #@args(void, [Path, Matrix])
    #def addPath(self, path, matrix):
    #    pass
    
    @args(void, [float, float, float, float, Path.Direction])
    def addRect(self, left, top, right, bottom, direction):
        pass
    
    @args(void, [RectF, Path.Direction])
    def addRect(self, rect, direction):
        pass
    
    @args(void, [float, float, float, float, float, float, Path.Direction])
    def addRoundRect(self, left, top, right, bottom, rx, ry, direction):
        pass
    
    @args(void, [float, float, float, float, [float], Path.Direction])
    def addRoundRect(self, left, top, right, bottom, radii, direction):
        pass
    
    @args(void, [RectF, [float], Path.Direction])
    def addRoundRect(self, rect, radii, direction):
        pass
    
    @args(void, [RectF, float, float, Path.Direction])
    def addRoundRect(self, rect, rx, ry, direction):
        pass
    
    ### API level 21
    @args(void, [float, float, float, float, float, float, bool])
    def arcTo(self, left, top, right, bottom, startAngle, sweepAngle, forceMoveTo):
        pass
    
    ### API level 21
    @args(void, [RectF, float, float])
    def arcTo(self, oval, startAngle, sweepAngle):
        pass
    
    ### API level 21
    @args(void, [RectF, float, float, bool])
    def arcTo(self, oval, startAngle, sweepAngle, forceMoveTo):
        pass
    
    def close(self):
        pass
    
    @args(RectF, [bool])
    def computeBounds(self, bounds, exact):
        pass
    
    @args(void, [float, float, float, float, float, float])
    def cubicTo(self, x1, y1, x2, y2, x3, y3):
        pass
    
    @args(Path.FillType, [])
    def getFillType(self):
        pass
    
    @args(void, [int])
    def incReserve(self, extraPtCount):
        pass
    
    @args(bool, [])
    def isConvex(self):
        pass
    
    @args(bool, [])
    def isEmpty(self):
        pass
    
    @args(bool, [])
    def isInverseFillType(self):
        pass
    
    @args(bool, [])
    def isRect(self):
        pass
    
    @args(void, [float, float])
    def lineTo(self, x, y):
        pass
    
    @args(void, [float, float])
    def moveTo(self, x, y):
        pass
    
    @args(void, [float, float, Path])
    def offset(self, dx, dy, destination):
        pass
    
    @args(void, [float, float])
    def offset(self, dx, dy):
        pass
    
    @args(bool, [Path, Path, Path.Op])
    def op(self, path1, path2, op):
        pass
    
    @args(bool, [Path, Path.Op])
    def op(self, path, op):
        pass
    
    @args(void, [float, float, float, float])
    def quadTo(self, x1, y1, x2, y2):
        pass
    
    @args(void, [float, float, float, float, float, float])
    def rCubicTo(self, x1, y1, x2, y2, x3, y3):
        pass
    
    @args(void, [float, float])
    def rLineTo(self, dx, dy):
        pass
    
    @args(void, [float, float])
    def rMoveTo(self, dx, dy):
        pass
    
    @args(void, [float, float, float, float])
    def rQuadTo(self, dx1, dy1, dx2, dy2):
        pass
    
    def reset(self):
        pass
    
    def rewind(self):
        pass
    
    @args(void, [Path])
    def set(self, source):
        pass
    
    @args(void, [Path.FillType])
    def setFillType(self, fillType):
        pass
    
    @args(void, [float, float])
    def setLastPoint(self, dx, dy):
        pass
    
    def toggleInverseFillType(self):
        pass
    
    #@args(void, [Matrix, Path])
    #def transform(self, matrix, destination):
    #    pass
    
    #@args(void, [Matrix])
    #def transform(self, matrix):
    #    pass


class Point(Object):

    ### Note: the parcelable attributes are not yet defined.
    
    __interfaces__ = [Parcelable]
    __fields__ = {"x": int, "y": int}
    
    def __init__(self):
        pass
    
    @args(void, [int, int])
    def __init__(self, x, y):
        pass
    
    @args(void, [Point])
    def __init__(self, src):
        pass
    
    @args(bool, [Object])
    def equals(self, object):
        pass
    
    @args(int, [])
    def hashCode(self):
        pass
    
    @args(void, [])
    def negate(self):
        pass
    
    @args(void, [int, int])
    def offset(self, dx, dy):
        pass
    
    @args(void, [int, int])
    def set(self, x, y):
        pass
    
    @args(String, [])
    def toString(self):
        pass


class PixelXorXfermode(Xfermode):

    @args(void, [int])
    def __init__(self, opColor):
        pass


class PorterDuff(Object):

    class Mode(Enum):
    
        __static_fields__ = {
            "ADD": PorterDuff.Mode,
            "CLEAR": PorterDuff.Mode,
            "DARKEN": PorterDuff.Mode,
            "DST": PorterDuff.Mode,
            "DST_ATOP": PorterDuff.Mode,
            "DST_IN": PorterDuff.Mode,
            "DST_OUT": PorterDuff.Mode,
            "DST_OVER": PorterDuff.Mode,
            "LIGHTEN": PorterDuff.Mode,
            "MULTIPLY": PorterDuff.Mode,
            "OVERLAY": PorterDuff.Mode,
            "SCREEN": PorterDuff.Mode,
            "SRC": PorterDuff.Mode,
            "SRC_ATOP": PorterDuff.Mode,
            "SRC_IN": PorterDuff.Mode,
            "SRC_OUT": PorterDuff.Mode,
            "SRC_OVER": PorterDuff.Mode,
            "XOR": PorterDuff.Mode,
            }
    
    def __init__(self):
        pass


class PorterDuffXfermode(Xfermode):

    @args(void, [PorterDuff.Mode])
    def __init__(self, mode):
        pass


class Rect(Object):

    __interfaces__ = [Parcelable]
    __fields__ = {
        "CREATOR": Parcelable.Creator(Rect),
        "bottom": int,
        "left": int,
        "right": int,
        "top": int
        }
    
    def __init__(self):
        pass
    
    @args(void, [int, int, int, int])
    def __init__(self, left, top, right, bottom):
        pass
    
    @args(void, [Rect])
    def __init__(self, rect):
        pass
    
    @args(int, [])
    def centerX(self):
        pass
    
    @args(int, [])
    def centerY(self):
        pass
    
    @args(bool, [int, int])
    def contains(self, x, y):
        pass
    
    @args(bool, [int, int, int, int])
    def contains(self, left, top, right, bottom):
        pass
    
    @args(bool, [Rect])
    def contains(self, rect):
        pass
    
    @args(float, [])
    def exactCenterX(self):
        pass
    
    @args(float, [])
    def exactCenterY(self):
        pass
    
    @args(String, [])
    def flattenToString(self):
        pass
    
    @args(int, [])
    def height(self):
        pass
    
    @args(void, [int, int])
    def inset(self, dx, dy):
        pass
    
    @args(bool, [Rect])
    def intersect(self, rect):
        pass
    
    @args(bool, [int, int, int, int])
    def intersect(self, left, top, right, bottom):
        pass
    
    @args(bool, [int, int, int, int])
    def intersects(self, left, top, right, bottom):
        pass
    
    @static
    @args(bool, [Rect, Rect])
    def intersects(a, b):
        pass
    
    @args(bool, [])
    def isEmpty(self):
        pass
    
    @args(void, [int, int])
    def offset(self, dx, dy):
        pass
    
    @args(void, [int, int])
    def offsetTo(self, x, y):
        pass
    
    @args(void, [Parcel])
    def readFromParcel(self, parcel):
        pass
    
    @args(void, [int, int, int, int])
    def set(self, left, top, right, bottom):
        pass
    
    @args(void, [Rect])
    def set(self, rect):
        pass
    
    @args(void, [])
    def setEmpty(self):
        pass
    
    @args(bool, [Rect, Rect])
    def setIntersect(self, a, b):
        pass
    
    @args(void, [])
    def sort(self):
        pass
    
    @args(String, [])
    def toShortString(self):
        pass
    
    @static
    @args(Rect, [String])
    def unflattenFromString(str):
        pass
    
    @args(void, [int, int, int, int])
    def union(self, left, top, right, bottom):
        pass
    
    @args(void, [Rect])
    def union(self, rect):
        pass
    
    @args(void, [int, int])
    def union(self, x, y):
        pass
    
    @args(int, [])
    def width(self):
        pass


class RectF(Object):

    __interfaces__ = [Parcelable]
    __fields__ = {
        "CREATOR": Parcelable.Creator(RectF),
        "bottom": float,
        "left": float,
        "right": float,
        "top": float
        }
    
    def __init__(self):
        pass
    
    @args(void, [float, float, float, float])
    def __init__(self, left, top, right, bottom):
        pass
    
    @args(void, [RectF])
    def __init__(self, rect):
        pass
    
    @args(void, [Rect])
    def __init__(self, rect):
        pass
    
    @args(float, [])
    def centerX(self):
        pass
    
    @args(float, [])
    def centerY(self):
        pass
    
    @args(bool, [float, float])
    def contains(self, x, y):
        pass
    
    @args(bool, [float, float, float, float])
    def contains(self, left, top, right, bottom):
        pass
    
    @args(bool, [RectF])
    def contains(self, rect):
        pass
    
    @args(float, [])
    def height(self):
        pass
    
    @args(void, [float, float])
    def inset(self, dx, dy):
        pass
    
    @args(bool, [RectF])
    def intersect(self, rect):
        pass
    
    @args(bool, [float, float, float, float])
    def intersect(self, left, top, right, bottom):
        pass
    
    @args(bool, [float, float, float, float])
    def intersects(self, left, top, right, bottom):
        pass
    
    @static
    @args(bool, [RectF, RectF])
    def intersects(a, b):
        pass
    
    @args(bool, [])
    def isEmpty(self):
        pass
    
    @args(void, [float, float])
    def offset(self, dx, dy):
        pass
    
    @args(void, [float, float])
    def offsetTo(self, x, y):
        pass
    
    @args(void, [Parcel])
    def readFromParcel(self, parcel):
        pass
    
    @args(void, [float, float, float, float])
    def set(self, left, top, right, bottom):
        pass
    
    @args(void, [RectF])
    def set(self, rect):
        pass
    
    @args(void, [])
    def setEmpty(self):
        pass
    
    @args(bool, [RectF, RectF])
    def setIntersect(self, a, b):
        pass
    
    @args(void, [])
    def sort(self):
        pass
    
    @args(String, [])
    def toShortString(self):
        pass
    
    @args(void, [float, float, float, float])
    def union(self, left, top, right, bottom):
        pass
    
    @args(void, [RectF])
    def union(self, rect):
        pass
    
    @args(void, [float, float])
    def union(self, x, y):
        pass
    
    @args(float, [])
    def width(self):
        pass


class SurfaceTexture(Object):

    class OnFrameAvailableListener:
    
        @args(void, [SurfaceTexture])
        def onFrameAvailable(self, surfaceTexture):
            pass
    
    @args(void, [int])
    def __init__(self, texName):
        pass
    
    @args(void, [int, bool])
    def __init__(self, texName, singleBufferMode):
        pass
    
    @args(void, [int])
    def attachToGLContext(self, texName):
        pass
    
    def detachFromGLContext(self):
        pass
    
    @args(long, [])
    def getTimestamp(self):
        pass
    
    @args(void, [[float]])
    def getTransformMatrix(self, matrix):
        pass
    
    def release(self):
        pass
    
    def releaseTexImage(self):
        pass
    
    @args(void, [int, int])
    def setDefaultBufferSize(self, width, height):
        pass
    
    @args(void, [SurfaceTexture.OnFrameAvailableListener, Handler])
    def setOnFrameAvailableListener(self, listener, handler):
        pass
    
    @args(void, [SurfaceTexture.OnFrameAvailableListener])
    def setOnFrameAvailableListener(self, listener):
        pass
    
    def updateTexImage(self):
        pass


class Typeface(Object):

    __static_fields__ = {
        "DEFAULT": Typeface,
        "DEFAULT_BOLD": Typeface,
        "MONOSPACE": Typeface,
        "SANS_SERIF": Typeface,
        "SERIF": Typeface
        }
    
    BOLD = 1
    BOLD_ITALIC = 3
    ITALIC = 2
    NORMAL = 0
    
    @static
    @args(Typeface, [String, int])
    def create(familyName, style):
        pass
    
    @static
    @args(Typeface, [Typeface, int])
    def create(family, style):
        pass
    
    @static
    @args(Typeface, [AssetManager, String])
    def createFromAsset(mgr, path):
        pass
    
    @static
    @args(Typeface, [String])
    def createFromFile(path):
        pass
    
    @static
    @args(Typeface, [File])
    def createFromFile(path):
        pass
    
    @static
    @args(Typeface, [int])
    def defaultFromStyle(style):
        pass
    
    @args(boolean, [Object])
    def equals(self, o):
        pass
    
    @args(int, [])
    def getStyle(self):
        pass
    
    @args(int, [])
    def hashCode(self):
        pass
    
    @final
    @args(boolean, [])
    def isBold(self):
        pass
    
    @final
    @args(boolean, [])
    def isItalic(self):
        pass


class Xfermode(Object):

    def __init__(self):
        pass
