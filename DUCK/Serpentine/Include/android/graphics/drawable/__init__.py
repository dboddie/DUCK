# Class declarations obtained from Android API documentation licensed under the
# Apache 2.0 license (http://www.apache.org/licenses/LICENSE-2.0) and rewritten
# in syntax that the Python compiler module can parse.

__package__ = "android.graphics.drawable"

from java.lang import Object
from android.content.res import Resources
from android.graphics import Bitmap, Canvas, Paint
from android.graphics.drawable.shapes import Shape

class BitmapDrawable(Drawable):

    def __init__(self):
        pass
    
    @args(void, [Resources])
    def __init__(self, resources):
        pass
    
    @args(void, [Bitmap])
    def __init__(self, bitmap):
        pass
    
    @args(void, [Resources, Bitmap])
    def __init__(self, resources, bitmap):
        pass
    
    @args(void, [String])
    def __init__(self, filePath):
        pass
    
    @args(void, [Resources, String])
    def __init__(self, resources, filePath):
        pass


class Drawable(Object):

    def __init__(self):
        pass
    
    @abstract
    @args(void, [Canvas])
    def draw(self, canvas):
        pass


class ShapeDrawable(Drawable):

    def __init__(self):
        pass
    
    @args(void, [Shape])
    def __init__(self):
        pass
    
    @args(void, [Canvas])
    def draw(self, canvas):
        pass
    
    @args(Paint, [])
    def getPaint(self):
        pass
    
    @args(Shape, [])
    def getShape(self):
        pass


class VectorDrawable(Drawable):

    def __init__(self):
        pass
    
    @args(void, [Resources.Theme])
    def applyTheme(self, t):
        pass
    
    @args(bool, [])
    def canApplyTheme(self):
        pass
    
    @args(void, [Canvas])
    def draw(self, canvas):
        pass
    
    @args(int, [])
    def getAlpha(self):
        pass
    
    @args(int, [])
    def getChangingConfigurations(self):
        pass
    
    @args(ColorFilter, [])
    def getColorFilter(self):
        pass
    
    @args(Drawable.ConstantState, [])
    def getConstantState(self):
        pass
    
    @args(int, [])
    def getIntrinsicHeight(self):
        pass
    
    @args(int, [])
    def getIntrinsicWidth(self):
        pass
    
    @args(int, [])
    def getOpacity(self):
        pass
    
    @args(void, [Resources, XmlPullParser, AttributeSet, Resources.Theme])
    def inflate(self, res, parser, attrs, theme):
        pass
    
    @args(bool, [])
    def isAutoMirrored(self):
        pass
    
    @args(bool, [])
    def isStateful(self):
        pass
    
    @args(Drawable, [])
    def mutate(self):
        pass
    
    @args(void, [int])
    def setAlpha(self, alpha):
        pass
    
    @args(void, [bool])
    def setAutoMirrored(self, mirrored):
        pass
    
    @args(void, [ColorFilter])
    def setColorFilter(self, colorFilter):
        pass
    
    @args(void, [ColorStateList])
    def setTintList(self, tint):
        pass
    
    @args(void, [PorterDuff.Mode])
    def setTintMode(self, tintMode):
        pass
    
    @protected
    @args(bool, [[int]])
    def onStateChange(self, stateSet):
        pass
