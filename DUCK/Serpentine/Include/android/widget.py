# Class declarations obtained from Android API documentation licensed under the
# Apache 2.0 license (http://www.apache.org/licenses/LICENSE-2.0) and rewritten
# in syntax that the Python compiler module can parse.

__package__ = "android.widget"

from java.lang import Enum, Exception, CharSequence, Object, String
from java.lang.annotation import Annotation
from android.app import ActionBar, Service
from android.content import Context, DialogInterface
from android.content.res import ColorStateList, Resources
from android.database import DataSetObserver
from android.graphics import Bitmap, Rect, Typeface
from android.graphics.drawable import Drawable
from android.os import Parcel, Parcelable
from android.text import Editable, TextWatcher
from android.util import AttributeSet
from android.view import KeyEvent, LayoutInflater, MotionEvent, View, ViewGroup

class AbsListView(AdapterView):

    class LayoutParams(ViewGroup.LayoutParams):
    
        @args(void, [int, int])
        def __init__(self, width, height):
            pass
        
        @args(void, [int, int, int])
        def __init__(self, width, height, viewType):
            pass
        
        @args(void, [ViewGroup.LayoutParams])
        def __init__(self, source):
            pass
    
    class MultiChoiceModeListener:
    
        __interfaces__ = [ActionMode.Callback]
        
        @args(void, [ActionMode, int, long, bool])
        def onItemCheckedStateChanged(self, mode, position, id, checked):
            pass
    
    class OnScrollListener:
    
        SCROLL_STATE_FLING = 2
        SCROLL_STATE_IDLE = 0
        SCROLL_STATE_TOUCH_SCROLL = 1
        
        @args(void, [AbsListView, int, int, int])
        def onScroll(self, view, firstVisibleItem, visibleItemCount, totalItemCount):
            pass
        
        @args(void, [AbsListView, int])
        def onScrollStateChanged(self, view, scrollState):
            pass
    
    class RecyclerListener:
    
        @args(void, [View])
        def onMovedToScrapHeap(self, view):
            pass
    
    class SelectionBoundsAdjuster:
    
        @args(void, [Rect])
        def adjustListItemSelectionBounds(self, bounds):
            pass
    
    @args(void, [Context])
    def __init__(self, context):
        pass
    
    ### ...


class AbsSeekBar(ProgressBar):

    @args(void, [Context])
    def __init__(self, context):
        pass
    
    @args(void, [Context, AttributeSet])
    def __init__(self, context, attrs):
        pass
    
    @args(void, [Context, AttributeSet, int])
    def __init__(self, context, attrs, defStyleAttr):
        pass
    
    @args(void, [Context, AttributeSet, int, int])
    def __init__(self, context, attrs, defStyleAttr, defStyleRes):
        pass
    
    ### ...


class AbsoluteLayout(ViewGroup):

    class LayoutParams(ViewGroup.LayoutParams):
    
        __fields__ = {"x": int, "y": int}
        
        @args(void, [int, int, int, int])
        def __init__(self, width, height, x, y):
            pass
        
        @args(void, [Context, AttributeSet])
        def __init__(self, context, attrs):
            pass
        
        @args(void, [ViewGroup.LayoutParams])
        def __init__(self, source):
            pass
        
        @args(String, [String])
        def debug(self, output):
            pass
    
    @args(void, [Context])
    def __init__(self, context):
        pass
    
    @args(void, [Context, AttributeSet])
    def __init__(self, context, attrs):
        pass
    
    @args(void, [Context, AttributeSet, int])
    def __init__(self, context, attrs, defStyleAttr):
        pass
    
    @args(void, [Context, AttributeSet, int, int])
    def __init__(self, context, attrs, defStyleAttr, defStyleRes):
        pass
    
    ### ...


class ActionMenuView(LinearLayout):

    class LayoutParams(LinearLayout.LayoutParams):
    
        @api(21)
        @args(void, [Context, AttributeSet])
        def __init__(self, context, attrs):
            pass
        
        @api(21)
        @args(void, [ViewGroup.LayoutParams])
        def __init__(self, source):
            pass
        
        @api(21)
        @args(void, [ActionMenuView.LayoutParams])
        def __init__(self, source):
            pass
        
        @api(21)
        @args(void, [int, int])
        def __init__(self, width, height):
            pass
    
    class OnMenuItemClickListener:
    
        @api(21)
        @args(void, [MenuItem])
        def onMenuItemClick(self, item):
            pass
    
    @api(21)
    @args(void, [Context])
    def __init__(self, context):
        pass
    
    @api(21)
    @args(void, [Context, AttributeSet])
    def __init__(self, context, attrs):
        pass
    
    @api(21)
    def dismissPopupMenus(self):
        pass
    
    @api(21)
    @args(ActionMenuView.LayoutParams, [AttributeSet])
    def generateLayoutParams(self, attrs):
        pass
    
    @api(21)
    @args(Menu, [])
    def getMenu(self):
        pass
    
    @api(23)
    @args(Drawable, [])
    def getOverflowIcon(self):
        pass
    
    @api(21)
    @args(int, [])
    def getPopupTheme(self):
        pass
    
    @api(21)
    @args(bool, [])
    def hideOverflowMenu(self):
        pass
    
    @api(21)
    @args(bool, [])
    def isOverflowMenuShowing(self):
        pass
    
    @api(21)
    @args(void, [Configuration])
    def onConfigurationChanged(self, newConfig):
        pass
    
    @api(21)
    def onDetachedFromWindow(self):
        pass
    
    @api(21)
    @args(void, [ActionMenuView.OnMenuItemClickListener])
    def setOnMenuItemClickListener(self, listener):
        pass
    
    @api(23)
    @args(void, [Drawable])
    def setOverflowIcon(self, icon):
        pass
    
    @api(21)
    @args(void, [int])
    def setPopupTheme(self, resId):
        pass
    
    @api(21)
    @args(bool, [])
    def showOverflowMenu(self):
        pass


class Adapter:

    IGNORE_ITEM_VIEW_TYPE = 0xffffffff
    NO_SELECTION = 0x8000000
    
    @args(int, [])
    def getCount(self):
        pass
    
    @args(Object, [int])
    def getItem(self, position):
        pass
    
    @args(long, [int])
    def getItemId(self, position):
        pass
    
    @args(int, [int])
    def getItemViewType(self, position):
        pass
    
    @args(View, [int, View, ViewGroup])
    def getView(self, position, convertView, parent):
        pass
    
    @args(int, [])
    def getViewTypeCount(self):
        pass
    
    @args(bool, [])
    def hasStableIds(self):
        pass
    
    @args(bool, [])
    def isEmpty(self):
        pass
    
    @args(void, [DataSetObserver])
    def registerDataSetObserver(self, observer):
        pass
    
    @args(void, [DataSetObserver])
    def unregisterDataSetObserver(self, observer):
        pass


class AdapterView(ViewGroup):

    class AdapterContextMenuInfo(Object):
    
        __interfaces = [ContextMenu.ContextMenuInfo]
        __fields__ = {"id": long, "position": int, "targetView": View}
        
        @args(void, [View, int, long])
        def __init__(self, targetView, position, id):
            pass
    
    class OnItemClickListener:
    
        @args(void, [AdapterView, View, int, long])
        def onItemClick(self, parent, view, position, id):
            pass
    
    class OnItemLongClickListener:
    
        @args(bool, [AdapterView, View, int, long])
        def onItemLongClick(self, parent, view, position, id):
            pass
    
    class OnItemSelectedListener:
    
        @args(void, [AdapterView, View, int, long])
        def onItemSelected(self, parent, view, position, id):
            pass
        
        @args(void, [AdapterView])
        def onNothingSelected(self, parent):
            pass
    
    @args(void, [Context])
    def __init__(self, context):
        pass
    
    @abstract
    @args(Adapter, [])
    def getAdapter(self):
        pass
    
    @args(int, [])
    def getCount(self):
        pass
    
    @args(View, [])
    def getEmptyView(self):
        pass
    
    @args(int, [])
    def getFirstVisiblePosition(self):
        pass
    
    @args(Object, [int])
    def getItemAtPosition(self, position):
        pass
    
    @args(long, [int])
    def getItemIdAtPosition(self, position):
        pass
    
    @args(int, [])
    def getLastVisiblePosition(self):
        pass
    
    @final
    @args(AdapterView.OnItemClickListener, [])
    def getOnItemClickListener(self):
        pass
    
    @final
    @args(AdapterView.OnItemLongClickListener, [])
    def getOnItemLongClickListener(self):
        pass
    
    @final
    @args(AdapterView.OnItemSelectedListener, [])
    def getOnItemSelectedListener(self):
        pass
    
    @args(int, [View])
    def getPositionForView(self, view):
        pass
    
    @args(Object, [])
    def getSelectedItem(self):
        pass
    
    @args(long, [])
    def getSelectedItemId(self):
        pass
    
    @args(int, [])
    def getSelectedItemPosition(self):
        pass
    
    @abstract
    @args(View, [])
    def getSelectedView(self):
        pass
    
    @args(bool, [View, int, long])
    def performItemClick(self, view, position, id):
        pass
    
    @abstract
    @args(void, [Adapter])
    def setAdapter(self, adapter):
        pass
    
    @args(void, [View])
    def setEmptyView(self, view):
        pass
    
    @args(void, [bool])
    def setFocusable(self, focusable):
        pass
    
    @args(void, [bool])
    def setFocusableInTouchMode(self, focusable):
        pass
    
    @args(void, [View.OnClickListener])
    def setOnClickListener(self, listener):
        pass
    
    @args(void, [AdapterView.OnItemClickListener])
    def setOnItemClickListener(self, listener):
        pass
    
    @args(void, [AdapterView.OnItemLongClickListener])
    def setOnItemLongClickListener(self, listener):
        pass
    
    @args(void, [AdapterView.OnItemSelectedListener])
    def setOnItemSelectedListener(self, listener):
        pass
    
    @abstract
    @args(void, [int])
    def setSelection(self, position):
        pass


class Advanceable:

    def advance(self):
        pass
    
    def fyiWillBeAdvancedByHostKThx(self):
        pass


class AnalogClock(View):

    ### Deprecated in API level 23
    
    @args(void, [Context])
    def __init__(self, context):
        pass
    
    @args(void, [Context, AttributeSet])
    def __init__(self, context, attrs):
        pass
    
    @args(void, [Context, AttributeSet, int])
    def __init__(self, context, attrs, defStyleAttr):
        pass
    
    @args(void, [Context, AttributeSet, int, int])
    def __init__(self, context, attrs, defStyleAttr, defStyleRes):
        pass
    
    ### ...


class ArrayAdapter(BaseAdapter):

    __interfaces__ = [Filterable, ThemedSpinnerAdapter]
    __parameters__ = [T]
    
    @args(void, [Context, int])
    def __init__(self, context, resource):
        pass
    
    @args(void, [Context, int, int])
    def __init__(self, context, resource, textViewResourceId):
        pass
    
    @args(void, [Context, int, [T]])
    def __init__(self, context, resource, objects):
        pass
    
    @args(void, [Context, int, int, [T]])
    def __init__(self, context, resource, textViewResourceId, objects):
        pass
    
    @args(void, [Context, int, List(T)])
    def __init__(self, context, resource, objects):
        pass
    
    @args(void, [Context, int, int, List(T)])
    def __init__(self, context, resource, textViewResourceId, objects):
        pass
    
    @args(void, [T])
    def add(self, object):
        pass
    
    @args(void, [Collection(T)])
    def addAll(self, collection):
        pass
    
    @args(void, [[T]])
    def addAll(self, items):
        pass
    
    def clear(self):
        pass
    
    @static
    @args(ArrayAdapter(CharSequence), [Context, int, int])
    def createFromResource(self, context, textArrayResId, textViewResId):
        pass
    
    @args(Context, [])
    def getContext(self):
        pass
    
    @args(int, [])
    def getCount(self):
        pass
    
    @args(View, [int, View, ViewGroup])
    def getDropDownView(self, position, convertView, parent):
        pass
    
    @args(Resources.Theme, [])
    def getDropDownViewTheme(self):
        pass
    
    @args(Filter, [])
    def getFilter(self):
        pass
    
    @args(T, [int])
    def getItem(self, position):
        pass
    
    @args(long, [int])
    def getItemId(self, position):
        pass
    
    @args(int, [T])
    def getPosition(self, item):
        pass
    
    @args(View, [int, View, ViewGroup])
    def getView(self, position, convertView, parent):
        pass
    
    @args(void, [T, int])
    def insert(self, object, index):
        pass
    
    @args(void, [])
    def notifyDataSetChanged(self):
        pass
    
    @args(void, [T])
    def remove(self, object):
        pass
    
    @args(void, [int])
    def setDropDownViewResource(self, resource):
        pass
    
    @args(void, [Resources.Theme])
    def setDropDownViewTheme(self, theme):
        pass
    
    @args(void, [bool])
    def setNotifyOnChange(self, notifyOnChange):
        pass
    
    @args(void, [Comparator(T)])
    def sort(self, comparator):
        pass


class AutoCompleteTextView(EditText):

    __interfaces__ = [Filter.FilterListener]
    
    class OnDismissListener:
    
        def onDismiss(self):
            pass
    
    class Validator:
    
        @args(CharSequence, [CharSequence])
        def fixText(self, invalidText):
            pass
        
        @args(bool, [CharSequence])
        def isValid(self, text):
            pass
    
    @args(void, [Context])
    def __init__(self, context):
        pass
    
    @args(void, [Context, AttributeSet])
    def __init__(self, context, attrs):
        pass
    
    @args(void, [Context, AttributeSet, int])
    def __init__(self, context, attrs, defStyleAttr):
        pass
    
    @api(21)
    @args(void, [Context, AttributeSet, int, int])
    def __init__(self, context, attrs, defStyleAttr, defStyleRes):
        pass
    
    @api(3)
    def clearListSelection(self):
        pass
    
    def dismissDropDown(self):
        pass
    
    @args(bool, [])
    def enoughToFilter(self):
        pass
    
    @args(ListAdapter, [])
    def getAdapter(self):
        pass
    
    @api(16)
    @args(CharSequence, [])
    def getCompletionHint(self):
        pass
    
    @api(3)
    @args(int, [])
    def getDropDownAnchor(self):
        pass
    
    @api(5)
    @args(Drawable, [])
    def getDropDownBackground(self):
        pass
    
    @api(4)
    @args(int, [])
    def getDropDownHeight(self):
        pass
    
    @api(5)
    @args(int, [])
    def getDropDownHorizontalOffset(self):
        pass
    
    @api(5)
    @args(int, [])
    def getDropDownVerticalOffset(self):
        pass
    
    @api(3)
    @args(int, [])
    def getDropDownWidth(self):
        pass
    
    @api(1, 3)
    @args(AdapterView.OnItemClickListener, [])
    def getItemClickListener(self):
        pass
    
    @api(1, 3)
    @args(AdapterView.OnItemSelectedListener, [])
    def getItemSelectedListener(self):
        pass
    
    @api(3)
    @args(int, [])
    def getListSelection(self):
        pass
    
    @api(3)
    @args(AdapterView.OnItemClickListener, [])
    def getOnItemClickListener(self):
        pass
    
    @api(3)
    @args(AdapterView.OnItemSelectedListener, [])
    def getOnItemSelectedListener(self):
        pass
    
    @args(int, [])
    def getThreshold(self):
        pass
    
    @args(AutoCompleteTextView.Validator, [])
    def getValidator(self):
        pass
    
    @api(3)
    @args(bool, [])
    def isPerformingCompletion(self):
        pass
    
    @args(bool, [])
    def isPopupShowing(self):
        pass
    
    @api(3)
    @args(void, [CompletionInfo])
    def onCommitCompletion(self, completion):
        pass
    
    @args(void, [int])
    def onFilterComplete(self, count):
        pass
    
    @args(bool, [int, KeyEvent])
    def onKeyDown(self, keyCode, event):
        pass
    
    @api(3)
    @args(bool, [int, KeyEvent])
    def onKeyPreIme(self, keyCode, event):
        pass
    
    @args(bool, [int, KeyEvent])
    def onKeyUp(self, keyCode, event):
        pass
    
    @args(void, [bool])
    def onWindowFocusChanged(self, hasWindowFocus):
        pass
    
    def performCompletion(self):
        pass
    
    def performValidation(self):
        pass
    
    # Object is actually a type that implements the ListAdapter and Filterable
    # interfaces:
    @args(void, [ListAdapter])
    def setAdapter(self, adapter):
        pass

    @args(void, [CharSequence])
    def setCompletionHint(self, hint):
        pass
    
    @api(3)
    @args(void, [int])
    def setDropDownAnchor(self, id):
        pass
    
    @api(5)
    @args(void, [Drawable])
    def setDropDownBackgroundDrawable(self, d):
        pass
    
    @api(5)
    @args(void, [int])
    def setDropDownBackgroundResource(self, id):
        pass
    
    @api(4)
    @args(void, [int])
    def setDropDownHeight(self, height):
        pass
    
    @api(5)
    @args(void, [int])
    def setDropDownHorizontalOffset(self, offset):
        pass
    
    @api(5)
    @args(void, [int])
    def setDropDownVerticalOffset(self, offset):
        pass
    
    @api(3)
    @args(void, [int])
    def setDropDownWidth(self, width):
        pass
    
    @api(3)
    @args(void, [int])
    def setListSelection(self, position):
        pass
    
    @args(void, [View.OnClickListener])
    def setOnClickListener(self, listener):
        pass
    
    @api(17)
    @args(void, [AutoCompleteTextView.OnDismissListener])
    def setOnDismissListener(self, dismissListener):
        pass
    
    @args(void, [AdapterView.OnItemClickListener])
    def setOnItemClickListener(self, l):
        pass
    
    @args(void, [AdapterView.OnItemSelectedListener])
    def setOnItemSelectedListener(self, l):
        pass
    
    @api(17)
    @args(void, [CharSequence, bool])
    def setText(self, text, filter):
        pass
    
    @args(void, [int])
    def setThreshold(self, threshold):
        pass
    
    @args(void, [AutoCompleteTextView.Validator])
    def setValidator(self, validator):
        pass
    
    def showDropDown(self):
        pass
    
    ### protected methods...


class BaseAdapter(Object):

    __interfaces__ = [ListAdapter, SpinnerAdapter]
    
    def __init__(self):
        pass


class Button(TextView):

    @args(void, [Context])
    def __init__(self, context):
        pass


class CalendarView(FrameLayout):

    class OnDateChangeListener:
    
        @api(11)
        @args(void, [CalendarView, int, int, int])
        def onSelectedDayChange(self, view, year, month, dayOfMonth):
            pass
    
    @api(11)
    @args(void, [Context])
    def __init__(self, context):
        pass
    
    @api(11)
    @args(void, [Context, AttributeSet])
    def __init__(self, context, attrs):
        pass
    
    @api(11)
    @args(void, [Context, AttributeSet, int])
    def __init__(self, context, attrs, defStyleAttr):
        pass
    
    @api(21)
    @args(void, [Context, AttributeSet, int, int])
    def __init__(self, context, attrs, defStyleAttr, defStyleRes):
        pass
    
    @api(23)
    @args(CharSequence, [])
    def getAccessibilityClassName(self):
        pass
    
    @api(11)
    @args(long, [])
    def getDate(self):
        pass
    
    @api(16)
    @args(int, [])
    def getDateTextAppearance(self):
        pass
    
    @api(11)
    @args(int, [])
    def getFirstDayOfWeek(self):
        pass
    
    @api(16, 23)
    @args(int, [])
    def getFocusedMonthDateColor(self):
        pass
    
    @api(11)
    @args(long, [])
    def getMaxDate(self):
        pass
    
    @api(11)
    @args(long, [])
    def getMinDate(self):
        pass
    
    @api(16, 23)
    @args(Drawable, [])
    def getSelectedDateVerticalBar(self):
        pass
    
    @api(16, 23)
    @args(int, [])
    def getSelectedWeekBackgroundColor(self):
        pass
    
    @api(11)
    @args(bool, [])
    def getShowWeekNumber(self):
        pass
    
    @api(16, 23)
    @args(int, [])
    def getShownWeekCount(self):
        pass
    
    @api(16, 23)
    @args(int, [])
    def getUnfocusedMonthDateColor(self):
        pass
    
    @api(16)
    @args(int, [])
    def getWeekDayTextAppearance(self):
        pass
    
    @api(16, 23)
    @args(int, [])
    def getWeekNumberColor(self):
        pass
    
    @api(16, 23)
    @args(int, [])
    def getWeekSeparatorLineColor(self):
        pass
    
    @api(11)
    @args(void, [long])
    def setDate(self, date):
        pass
    
    @api(11)
    @args(void, [long, bool, bool])
    def setDate(self, date, animate, center):
        pass
    
    @api(16)
    @args(void, [int])
    def setDateTextAppearance(self, resourceId):
        pass
    
    @api(11)
    @args(void, [int])
    def setFirstDayOfWeek(self, firstDayOfWeek):
        pass
    
    @api(16, 23)
    @args(void, [int])
    def setFocusedMonthDateColor(self, color):
        pass
    
    @api(11)
    @args(void, [long])
    def setMaxDate(self, maxDate):
        pass
    
    @api(11)
    @args(void, [long])
    def setMinDate(self, minDate):
        pass
    
    @api(11)
    @args(void, [CalendarView.OnDateChangeListener])
    def setOnDateChangeListener(self, listener):
        pass
    
    @api(16, 23)
    @args(void, [Drawable])
    def setSelectedDateVerticalBar(self, drawable):
        pass
    
    @api(16, 23)
    @args(void, [int])
    def setSelectedDateVerticalBar(self, resourceId):
        pass
    
    @api(16, 23)
    @args(void, [int])
    def setSelectedWeekBackgroundColor(self, color):
        pass
    
    @api(11)
    @args(void, [bool])
    def setShowWeekNumber(self, showWeekNumber):
        pass
    
    @api(16, 23)
    @args(void, [int])
    def setShownWeekCount(self, count):
        pass
    
    @api(16, 23)
    @args(void, [int])
    def setUnfocusedMonthDateColor(self, color):
        pass
    
    @api(16)
    @args(void, [int])
    def setWeekDayTextAppearance(self, resourceId):
        pass
    
    @api(16, 23)
    @args(void, [int])
    def setWeekNumberColor(self, color):
        pass
    
    @api(16, 23)
    @args(void, [int])
    def setWeekSeparatorLineColor(self, color):
        pass


class Checkable:

    @args(bool, [])
    def isChecked(self):
        pass
    
    @args(void, [bool])
    def setChecked(self, checked):
        pass
    
    def toggle(self):
        pass


class CheckBox(CompoundButton):

    @args(void, [Context])
    def __init__(self, context):
        pass
    
    @args(void, [Context, AttributeSet])
    def __init__(self, context, attrs):
        pass
    
    @args(void, [Context, AttributeSet, int])
    def __init__(self, context, attrs, defStyleAttr):
        pass
    
    @args(void, [Context, AttributeSet, int, int])
    def __init__(self, context, attrs, defStyleAttr, defStyleRes):
        pass
    
    @args(CharSequence, [])
    def getAccessibilityClassName(self):
        pass


class CheckedTextView(TextView):

    __interfaces__ = [Checkable]
    
    @args(void, [Context])
    def __init__(self, context):
        pass
    
    @args(void, [Context, AttributeSet])
    def __init__(self, context, attrs):
        pass
    
    @args(void, [Context, AttributeSet, int])
    def __init__(self, context, attrs, defStyleAttr):
        pass
    
    @args(void, [Context, AttributeSet, int, int])
    def __init__(self, context, attrs, defStyleAttr, defStyleRes):
        pass
    
    ### ...
    
    @args(bool, [])
    def isChecked(self):
        pass
    
    ### ...
    
    @args(void, [bool])
    def setChecked(self, checked):
        pass
    
    @args(void, [int])
    def setVisibility(self, v):
        pass
    
    def toggle(self):
        pass


class Chronometer(TextView):

    class OnChronometerTickListener:
    
        @args(void, [Chronometer])
        def onChronometerTick(self, chronometer):
            pass
    
    @args(void, [Context])
    def __init__(self, context):
        pass
    
    @args(void, [Context, AttributeSet])
    def __init__(self, context, attrs):
        pass
    
    @args(void, [Context, AttributeSet, int])
    def __init__(self, context, attrs, defStyleAttr):
        pass
    
    @args(void, [Context, AttributeSet, int, int])
    def __init__(self, context, attrs, defStyleAttr, defStyleRes):
        pass
    
    @args(CharSequence, [])
    def getAccessibilityClassName(self):
        pass
    
    @args(long, [])
    def getBase(self):
        pass
    
    @args(CharSequence, [])
    def getContentDescription(self):
        pass
    
    @args(String, [])
    def getFormat(self):
        pass
    
    @args(Chronometer.OnChronometerTickListener, [])
    def getOnChronometerTickListener(self):
        pass
    
    @args(void, [long])
    def setBase(self, base):
        pass
    
    @args(void, [String])
    def setFormat(self, format):
        pass
    
    @args(void, [Chronometer.OnChronometerTickListener])
    def setOnChronometerTickListener(self, listener):
        pass
    
    def start(self):
        pass
    
    def stop(self):
        pass


class CompoundButton(Button):

    __interfaces__ = [Checkable]
    
    @args(void, [Context])
    def __init__(self, context):
        pass
    
    @args(void, [Context, AttributeSet])
    def __init__(self, context, attrs):
        pass
    
    @args(void, [Context, AttributeSet, int])
    def __init__(self, context, attrs, defStyleAttr):
        pass
    
    @args(void, [Context, AttributeSet, int, int])
    def __init__(self, context, attrs, defStyleAttr, defStyleRes):
        pass
    
    ### ...
    
    @args(int, [])
    def getCompoundPaddingLeft(self):
        pass
    
    @args(int, [])
    def getCompoundPaddingRight(self):
        pass
    
    @args(bool, [])
    def isChecked(self):
        pass
    
    ### ...
    
    @args(void, [Parcelable])
    def onRestoreInstanceState(self, state):
        pass
    
    @args(Parcelable, [])
    def onSaveInstanceState(self):
        pass
    
    def performClick(self):
        pass
    
    ### ...
    
    @args(void, [bool])
    def setChecked(self, checked):
        pass
    
    @args(void, [CompoundButton.OnCheckedChangeListener])
    def setOnCheckedChangeListener(self, listener):
        pass
    
    def toggle(self):
        pass


class DatePicker(FrameLayout):

    class OnDateChangeListener:
    
        @args(void, [DatePicker, int, int, int])
        def onDateChanged(self, view, year, monthOfYear, dayOfMonth):
            pass
    
    @args(void, [Context])
    def __init__(self, context):
        pass
    
    @args(void, [Context, AttributeSet])
    def __init__(self, context, attrs):
        pass
    
    @args(void, [Context, AttributeSet, int])
    def __init__(self, context, attrs, defStyleAttr):
        pass
    
    @api(21)
    @args(void, [Context, AttributeSet, int, int])
    def __init__(self, context, attrs, defStyleAttr, defStyleRes):
        pass
    
    @api(23)
    @args(CharSequence, [])
    def getAccessibilityClassName(self):
        pass
    
    @api(12)
    @args(CalendarView, [])
    def getCalendarView(self):
        pass
    
    @api(11)
    @args(bool, [])
    def getCalendarViewShown(self):
        pass
    
    @args(int, [])
    def getDayOfMonth(self):
        pass
    
    @api(21)
    @args(int, [])
    def getFirstDayOfWeek(self):
        pass
    
    @api(11)
    @args(long, [])
    def getMaxDate(self):
        pass
    
    @api(11)
    @args(long, [])
    def getMinDate(self):
        pass
    
    @args(int, [])
    def getMonth(self):
        pass
    
    @api(11)
    @args(bool, [])
    def getSpinnersShown(self):
        pass
    
    @args(int, [])
    def getYear(self):
        pass
    
    @args(void, [int, int, int, DatePicker.OnDateChangedListener])
    def init(self, year, monthOfYear, dayOfMonth, onDateChangedListener):
        pass
    
    @args(bool, [])
    def isEnabled(self):
        pass
    
    @api(11)
    @args(void, [bool])
    def setCalendarViewShown(self, shown):
        pass
    
    @args(void, [bool])
    def setEnabled(self, enabled):
        pass
    
    @api(21)
    @args(void, [int])
    def setFirstDayOfWeek(self, firstDayOfWeek):
        pass
    
    @api(11)
    @args(void, [long])
    def setMaxDate(self, maxDate):
        pass
    
    @api(11)
    @args(void, [long])
    def setMinDate(self, minDate):
        pass
    
    @api(11)
    @args(void, [bool])
    def setSpinnersShown(self, shown):
        pass
    
    @args(void, [int, int, int])
    def updateDate(self, year, month, dayOfMonth):
        pass


class DigitalClock(TextView):

    ### Deprecated in API level 17
    
    @args(void, [Context])
    def __init__(self, context):
        pass
    
    @args(void, [Context, AttributeSet])
    def __init__(self, context, attrs):
        pass
    
    @args(CharSequence, [])
    def getAccessibilityClassName(self):
        pass
    
    ### ...


class EditText(TextView):

    @args(void, [Context])
    def __init__(self, context):
        pass
    
    @args(void, [int])
    def extendSelection(self, index):
        pass
    
    @args(CharSequence, [])
    def getAccessibilityClassName(self):
        pass
    
    @args(Editable, [])
    def getText(self):
        pass
    
    def selectAll(self):
        pass
    
    @args(void, [int])
    def setSelection(self, index):
        pass
    
    @args(void, [int, int])
    def setSelection(self, start, stop):
        pass


class Filter(Object):

    class FilterListener:
    
        @args(void, [int])
        def onFilterComplete(self, count):
            pass
    
    class FilterResults():
    
        __fields__ = {"count": int, "values": Object}
        
        def __init__(self):
            pass
    
    def __init__(self):
        pass
    
    @args(CharSequence, [Object])
    def convertResultToString(self, resultValue):
        pass
    
    @final
    @args(void, [CharSequence, Filter.FilterListener])
    def filter(self, constraint, listener):
        pass
    
    @final
    @args(void, [CharSequence])
    def filter(self, constraint):
        pass
    
    @protected
    @abstract
    @args(Filter.FilterResults, [CharSequence])
    def performFiltering(self, constraint):
        pass
    
    @protected
    @abstract
    @args(void, [CharSequence, Filter.FilterResults])
    def publishResults(self, constraint, results):
        pass


class Filterable(Object):

    @args(Filter, [])
    def getFilter(self):
        pass


class FrameLayout(ViewGroup):

    class LayoutParams(ViewGroup.MarginLayoutParams):
    
        __fields__ = {"gravity": int}
        
        @args(void, [int, int])
        def __init__(self, width, height):
            pass
        
        @args(void, [int, int, int])
        def __init__(self, width, height, gravity):
            pass
        
        @args(void, [ViewGroup.LayoutParams])
        def __init__(self, source):
            pass
        
        @args(void, [ViewGroup.MarginLayoutParams])
        def __init__(self, source):
            pass
        
        @args(void, [LinearLayout.LayoutParams])
        def __init__(self, source):
            pass
    
    @args(void, [Context])
    def __init__(self, context):
        pass
    
    @args(CharSequence, [])
    def getAccessibilityClassName(self):
        pass
    
    @args(bool, [])
    def getMeasureAllChildren(self):
        pass
    
    @args(void, [int])
    def setForegroundGravity(self, foregroundGravity):
        pass
    
    @args(void, [bool])
    def setMeasureAllChildren(self, measureAll):
        pass
    
    @args(bool, [])
    def shouldDelayChildPressedState(self):
        pass


class GridLayout(ViewGroup):

    ALIGN_BOUNDS = 0
    ALIGN_MARGINS = 1
    HORIZONTAL = 0
    UNDEFINED = 0x80000000
    VERTICAL = 1
    
    __fields__ = {
        "BASELINE": GridLayout.Alignment,
        "BOTTOM": GridLayout.Alignment,
        "CENTER": GridLayout.Alignment,
        "END": GridLayout.Alignment,
        "FILL": GridLayout.Alignment,
        "LEFT": GridLayout.Alignment,
        "RIGHT": GridLayout.Alignment,
        "START": GridLayout.Alignment,
        "TOP": GridLayout.Alignment
        }
    
    class Spec(Object):
    
        @args(bool, [Object])
        def equals(self, that):
            pass
        
        @args(int, [])
        def hashCode(self):
            pass
    
    class Alignment(Object):
    
        pass
    
    class LayoutParams(ViewGroup.MarginLayoutParams):
    
        __fields__ = {"columnSpec": GridLayout.Spec,
                      "rowSpec": GridLayout.Spec}
        
        def __init__(self):
            pass
        
        @args(void, [GridLayout.Spec, GridLayout.Spec])
        def __init__(self, rowSpec, columnSpec):
            pass
        
        @args(void, [ViewGroup.LayoutParams])
        def __init__(self, source):
            pass
        
        @args(void, [ViewGroup.MarginLayoutParams])
        def __init__(self, source):
            pass
        
        @args(void, [GridLayout.LayoutParams])
        def __init__(self, source):
            pass
        
        @args(void, [int])
        def setGravity(self, gravity):
            pass
    
    @args(void, [Context])
    def __init__(self, context):
        pass
    
    @args(GridLayout.LayoutParams, [AttributeSet])
    def generateLayoutParams(self, attrs):
        pass
    
    @args(CharSequence, [])
    def getAccessibilityClassName(self):
        pass
    
    @args(int, [])
    def getAlignmentMode(self):
        pass
    
    @args(int, [])
    def getColumnCount(self):
        pass
    
    @args(int, [])
    def getOrientation(self):
        pass
    
    @args(int, [])
    def getRowCount(self):
        pass
    
    @args(bool, [])
    def getUseDefaultMargins(self):
        pass
    
    @args(bool, [])
    def isColumnOrderPreserved(self):
        pass
    
    @args(bool, [])
    def isRowOrderPreserved(self):
        pass
    
    @args(void, [View])
    def onViewAdded(self, child):
        pass
    
    @args(void, [View])
    def onViewRemoved(self, child):
        pass
    
    @args(void, [])
    def requestLayout(self):
        pass
    
    @args(void, [int])
    def setAlignmentMode(self, alignmentMode):
        pass
    
    @args(void, [int])
    def setColumnCount(self, columnCount):
        pass
    
    @args(void, [bool])
    def setColumnOrderPreserved(self, columnOrderPreserved):
        pass
    
    @args(void, [int])
    def setOrientation(self, orientation):
        pass
    
    @args(void, [int])
    def setRowCount(self, rowCount):
        pass
    
    @args(void, [bool])
    def setRowOrderPreserved(self, RowOrderPreserved):
        pass
    
    @args(void, [bool])
    def setUseDefaultMargins(self, useDefaultMargins):
        pass
    
    @api(14)
    @static
    @args(GridLayout.Spec, [int])
    def spec(start):
        pass
    
    @api(21)
    @static
    @args(GridLayout.Spec, [int, float])
    def spec(start, weight):
        pass
    
    @api(14)
    @static
    @args(GridLayout.Spec, [int, GridLayout.Alignment])
    def spec(start, alignment):
        pass
    
    @api(14)
    @static
    @args(GridLayout.Spec, [int, int])
    def spec(start, size):
        pass
    
    @api(14)
    @static
    @args(GridLayout.Spec, [int, int, GridLayout.Alignment])
    def spec(start, size, alignment):
        pass
    
    @api(21)
    @static
    @args(GridLayout.Spec, [int, int, float])
    def spec(start, size, weight):
        pass
    
    @api(21)
    @static
    @args(GridLayout.Spec, [int, GridLayout.Alignment, float])
    def spec(start, alignment, weight):
        pass
    
    @api(21)
    @static
    @args(GridLayout.Spec, [int, int, GridLayout.Alignment, float])
    def spec(start, size, alignment, weight):
        pass
    


class GridView(AbsListView):

    AUTO_FIT = -1
    NO_STRETCH = 0
    STRETCH_COLUMN_WIDTH = 2
    STRETCH_SPACING = 1
    STRETCH_SPACING_UNIFORM = 3
    
    @args(void, [Context])
    def __init__(self, context):
        pass
    
    @args(int, [])
    def getColumnWidth(self):
        pass
    
    @args(int, [])
    def getGravity(self):
        pass
    
    @args(int, [])
    def getHorizontalSpacing(self):
        pass
    
    @args(int, [])
    def getNumColumns(self):
        pass
    
    @args(int, [])
    def getRequestedColumnWidth(self):
        pass
    
    @args(int, [])
    def getRequestedHorizontalSpacing(self):
        pass
    
    @args(int, [])
    def getStretchMode(self):
        pass
    
    @args(int, [])
    def getVerticalSpacing(self):
        pass
    
    @args(void, [int])
    def setColumnWidth(self, columnWidth):
        pass
    
    @args(void, [int])
    def setGravity(self, gravity):
        pass
    
    @args(void, [int])
    def setHorizontalSpacing(self, spacing):
        pass
    
    @args(void, [int])
    def setNumColumns(self, columns):
        pass
    
    @args(void, [Intent])
    def setRemoteViewsAdapter(self, intent):
        pass
    
    @args(void, [int])
    def setStretchMode(self, mode):
        pass
    
    @args(void, [int])
    def setVerticalSpacing(self, spacing):
        pass
    
    @args(void, [int])
    def smoothScrollByOffset(self, offset):
        pass
    
    @args(void, [int])
    def smoothScrollToPosition(self, position):
        pass


class HorizontalScrollView(FrameLayout):

    @args(void, [Context])
    def __init__(self, context):
        pass
    
    @args(void, [Context, AttributeSet])
    def __init__(self, context, attrs):
        pass
    
    @args(void, [Context, AttributeSet, int])
    def __init__(self, context, attrs, defStyleAttr):
        pass
    
    @args(void, [Context, AttributeSet, int, int])
    def __init__(self, context, attrs, defStyleAttr, defStyleRes):
        pass
    
    @args(bool, [int])
    def arrowScroll(self, direction):
        pass
    
    def computeScroll(self):
        pass
    
    @args(bool, [KeyEvent])
    def dispatchKeyEvent(self, event):
        pass
    
    @args(void, [Canvas])
    def draw(self, canvas):
        pass
    
    @args(bool, [KeyEvent])
    def executeKeyEvent(self, event):
        pass
    
    @args(void, [int])
    def fling(self, velocityY):
        pass
    
    @args(bool, [int])
    def fullScroll(self, direction):
        pass
    
    @args(CharSequence, [])
    def getAccessibilityClassName(self):
        pass
    
    @args(int, [])
    def getMaxScrollAmount(self):
        pass
    
    @args(bool, [])
    def isFillViewport(self):
        pass
    
    @args(bool, [])
    def isSmoothScrollingEnabled(self):
        pass
    
    @args(bool, [MotionEvent])
    def onGenericMotionEvent(self, event):
        pass
    
    @args(bool, [MotionEvent])
    def onInterceptTouchEvent(self, event):
        pass
    
    @args(bool, [MotionEvent])
    def onTouchEvent(self, event):
        pass
    
    @args(bool, [int])
    def pageScroll(self, direction):
        pass
    
    @args(void, [View, View])
    def requestChildFocus(self, child, focused):
        pass
    
    @args(bool, [View, Rect, bool])
    def requestChildRectangleOnScreen(self, child, rectangle, immediate):
        pass
    
    @args(void, [bool])
    def requestDisallowInterceptTouchEvent(self, disallowIntercept):
        pass
    
    def requestLayout(self):
        pass
    
    @args(void, [int, int])
    def scrollTo(self, x, y):
        pass
    
    @args(void, [bool])
    def setFillViewport(self, fillViewport):
        pass
    
    @args(void, [int])
    def setOverScrollMode(self, mode):
        pass
    
    @args(void, [bool])
    def setSmoothScrollingEnabled(self, smoothScrollingEnabled):
        pass
    
    @args(bool, [])
    def shouldDelayChildPressedState(self):
        pass
    
    @final
    @args(void, [int, int])
    def smoothScrollBy(self, dx, dy):
        pass
    
    @final
    @args(void, [int, int])
    def smoothScrollTo(self, x, y):
        pass


class ImageButton(ImageView):

    @args(void, [Context])
    def __init__(self, context):
        pass


class ImageView(View):

    class ScaleType(Enum):
    
        __static_fields__ = {
            "CENTER": ImageView.ScaleType,
            "CENTER_CROP": ImageView.ScaleType,
            "CENTER_INSIDE": ImageView.ScaleType,
            "FIT_CENTER": ImageView.ScaleType,
            "FIT_END": ImageView.ScaleType,
            "FIT_START": ImageView.ScaleType,
            "FIT_XY": ImageView.ScaleType,
            "MATRIX": ImageView.ScaleType
            }
    
        @static
        @args(ImageView.ScaleType, [String])
        def valueOf(name):
            pass
        
        @final
        @static
        @args([ImageView.ScaleType], [])
        def values():
            pass
    
    @args(void, [Context])
    def __init__(self, context):
        pass
    
    @args(void, [Context, AttributeSet])
    def __init__(self, context, attrs):
        pass
    
    @args(void, [Context, AttributeSet, int])
    def __init__(self, context, attrs, defStyleAttr):
        pass
    
    @args(void, [Context, AttributeSet, int, int])
    def __init__(self, context, attrs, defStyleAttr, defStyleRes):
        pass
    
    @final
    def clearColorFilter(self):
        pass
    
    @args(void, [float, float])
    def drawableHotspotChanged(self, x, y):
        pass
    
    @args(CharSequence, [])
    def getAccessibilityClassName(self):
        pass
    
    @args(bool, [])
    def getAdjustViewBounds(self):
        pass
    
    @args(int, [])
    def getBaseline(self):
        pass
    
    @args(bool, [])
    def getBaselineAlignBottom(self):
        pass
    
    @args(ColorFilter, [])
    def getColorFilter(self):
        pass
    
    @args(bool, [])
    def getCropToPadding(self):
        pass
    
    @args(Drawable, [])
    def getDrawable(self):
        pass
    
    @args(int, [])
    def getImageAlpha(self):
        pass
    
    @args(Matrix, [])
    def getImageMatrix(self):
        pass
    
    @args(ColorStateList, [])
    def getImageTintList(self):
        pass
    
    @args(PorterDuff.Mode, [])
    def getImageTintMode(self):
        pass
    
    @args(int, [])
    def getMaxHeight(self):
        pass
    
    @args(int, [])
    def getMaxWidth(self):
        pass
    
    @args(ImageView.ScaleType, [])
    def getScaleType(self):
        pass
    
    @args(bool, [])
    def hasOverlappingRendering(self):
        pass
    
    @args(void, [Drawable])
    def invalidateDrawable(self, dr):
        pass
    
    @args(bool, [])
    def isOpaque(self):
        pass
    
    def jumpDrawablesToCurrentState(self):
        pass
    
    @args([int], [int])
    def onCreateDrawableState(self, extraSpace):
        pass
    
    @args(void, [int])
    def onRtlPropertiesChanged(self, layoutDirection):
        pass
    
    @args(void, [bool])
    def setAdjustViewBounds(self, adjustViewBounds):
        pass
    
    @args(void, [int])
    def setAlpha(self, alpha):
        pass
    
    @args(void, [int])
    def setBaseline(self, baseline):
        pass
    
    @args(void, [bool])
    def setBaselineAlignBottom(self, aligned):
        pass
    
    @args(void, [ColorFilter])
    def setColorFilter(self, cf):
        pass
    
    @final
    @args(void, [int])
    def setColorFilter(self, color):
        pass
    
    @final
    @args(void, [int, PorterDuff.Mode])
    def setColorFilter(self, color, mode):
        pass
    
    @args(void, [bool])
    def setCropToPadding(self, cropToPadding):
        pass
    
    @args(void, [int])
    def setImageAlpha(self, alpha):
        pass
    
    @args(void, [Bitmap])
    def setImageBitmap(self, bm):
        pass
    
    @args(void, [Drawable])
    def setImageDrawable(self, drawable):
        pass
    
    @args(void, [Icon])
    def setImageIcon(self, icon):
        pass
    
    @args(void, [int])
    def setImageLevel(self, level):
        pass
    
    @args(void, [Matrix])
    def setImageMatrix(self, matrix):
        pass
    
    @args(void, [int])
    def setImageResource(self, resId):
        pass
    
    @args(void, [[int], bool])
    def setImageState(self, state, merge):
        pass
    
    @args(void, [ColorStateList])
    def setImageTintList(self, tint):
        pass
    
    @args(void, [PorterDuff.Mode])
    def setImageTintMode(self, tintMode):
        pass
    
    @args(void, [Uri])
    def setImageURI(self, uri):
        pass
    
    @args(void, [int])
    def setMaxHeight(self, maxHeight):
        pass
    
    @args(void, [int])
    def setMaxWidth(self, maxWidth):
        pass
    
    @args(void, [ImageView.ScaleType])
    def setScaleType(self, scaleType):
        pass
    
    @args(void, [bool])
    def setSelected(self, selected):
        pass
    
    @args(void, [int])
    def setVisibility(self, visibility):
        pass


class LinearLayout(ViewGroup):

    HORIZONTAL = 0
    SHOW_DIVIDER_BEGINNING = 1
    SHOW_DIVIDER_END = 4
    SHOW_DIVIDER_MIDDLE = 2
    SHOW_DIVIDER_NONE = 0
    VERTICAL = 1
    
    class LayoutParams(ViewGroup.MarginLayoutParams):
    
        __fields__ = {
            "gravity": int,
            "weight": float
            }
        
        @args(void, [int, int])
        def __init__(self, width, height):
            pass
        
        @args(void, [int, int, float])
        def __init__(self, width, height, weight):
            pass
        
        @args(void, [ViewGroup.LayoutParams])
        def __init__(self, source):
            pass
        
        @args(void, [ViewGroup.MarginLayoutParams])
        def __init__(self, source):
            pass
        
        @args(void, [LinearLayout.LayoutParams])
        def __init__(self, source):
            pass
        
        @args(String, [String])
        def debug(self, output):
            pass
    
    @args(void, [Context])
    def __init__(self, context):
        pass
    
    @args(CharSequence, [])
    def getAccessibilityClassName(self):
        pass
    
    @args(int, [])
    def getBaseline(self):
        pass
    
    @args(int, [])
    def getBaselineAlignedChildIndex(self):
        pass
    
    @args(Drawable, [])
    def getDividerDrawable(self):
        pass
    
    @args(int, [])
    def getDividerPadding(self):
        pass
    
    @args(int, [])
    def getOrientation(self):
        pass
    
    @args(int, [])
    def getShowDividers(self):
        pass
    
    @args(float, [])
    def getWeightSum(self):
        pass
    
    @args(bool, [])
    def isBaselineAligned(self):
        pass
    
    @args(bool, [])
    def isMeasureWithLargestChildEnabled(self):
        pass
    
    @args(void, [int])
    def onRtlPropertiesChanged(self, layoutDirection):
        pass
    
    @args(void, [bool])
    def setBaselineAligned(self, baselineAligned):
        pass
    
    @args(void, [int])
    def setBaselineAlignedChildIndex(self, index):
        pass
    
    @args(void, [Drawable])
    def setDividerDrawable(self, divider):
        pass
    
    @args(void, [int])
    def setDividerPadding(self, padding):
        pass
    
    @args(void, [int])
    def setGravity(self, gravity):
        pass
    
    @args(void, [int])
    def setHorizontalGravity(self, horizontalGravity):
        pass
    
    @args(void, [bool])
    def setMeasureWithLargestChildEnabled(self, enabled):
        pass
    
    @args(void, [int])
    def setOrientation(self, orientation):
        pass
    
    @args(void, [int])
    def setShowDividers(self, showDividers):
        pass
    
    @args(void, [int])
    def setVerticalGravity(self, verticalGravity):
        pass
    
    @args(void, [float])
    def setWeightSum(self, weightSum):
        pass
    
    @args(bool, [])
    def shouldDelayChildPressedState(self):
        pass


class ListAdapter:

    __interfaces__ = [Adapter]
    
    @args(bool, [])
    def areAllItemsEnabled(self):
        pass
    
    @args(bool, [int])
    def isEnabled(self, position):
        pass


class ListView(AbsListView): # via intermediate classes

    @args(void, [Context])
    def __init__(self, context):
        pass
    
    @args(void, [ListAdapter])
    def setAdapter(self, adapter):
        pass


class NumberPicker(LinearLayout):

    class Formatter:
    
        @args(void, [int])
        def format(self, value):
            pass
    
    class OnScrollListener:
    
        SCROLL_STATE_FLING = 2
        SCROLL_STATE_IDLE = 0
        SCROLL_STATE_TOUCH_SCROLL = 1
        
        @args(void, [NumberPicker, int])
        def onScrollStateChange(self, view, scrollState):
            pass
    
    class OnValueChangeListener:
    
        @args(void, [NumberPicker, int, int])
        def onValueChange(self, picker, oldValue, newValue):
            pass
    
    @api(11)
    @args(void, [Context])
    def __init__(self, context):
        pass

    @api(11)
    @args(void, [Context, AttributeSet])
    def __init__(self, context, attrs):
        pass
    
    @api(11)
    @args(void, [Context, AttributeSet, int])
    def __init__(self, context, attrs, defStyleAttr):
        pass
    
    @api(21)
    @args(void, [Context, AttributeSet, int, int])
    def __init__(self, context, attrs, defStyleAttr, defStyleRes):
        pass
    
    def computeScroll(self):
        pass

    @args(bool, [KeyEvent])
    def dispatchKeyEvent(self, event):
        pass

    @args(bool, [MotionEvent])
    def dispatchTouchEvent(self, event):
        pass

    @args(bool, [MotionEvent])
    def dispatchTrackballEvent(self, event):
        pass

    @api(16)
    @args(AccessibilityNodeProvider, [])
    def getAccessibilityNodeProvider(self):
        pass

    @args([String], [])
    def getDisplayedValues(self):
        pass

    @args(int, [])
    def getMaxValue(self):
        pass

    @args(int, [])
    def getMinValue(self):
        pass

    @args(int, [])
    def getSolidColor(self):
        pass

    @args(int, [])
    def getValue(self):
        pass

    @args(bool, [])
    def getWrapSelectorWheel(self):
        pass

    def jumpDrawablesToCurrentState(self):
        pass

    @args(bool, [MotionEvent])
    def onInterceptTouchEvent(self, event):
        pass

    @args(bool, [MotionEvent])
    def onTouchEvent(self, event):
        pass

    @args(bool, [])
    def performClick(self):
        pass

    @args(bool, [])
    def performLongClick(self):
        pass

    @args(void, [int, int])
    def scrollBy(self, x, y):
        pass

    @args(void, [[String]])
    def setDisplayedValues(self, displayedValues):
        pass

    @args(void, [bool])
    def setEnabled(self, enabled):
        pass

    @args(void, [NumberPicker.Formatter])
    def setFormatter(self, formatter):
        pass

    @args(void, [int])
    def setMaxValue(self, maxValue):
        pass

    @args(void, [int])
    def setMinValue(self, minValue):
        pass

    @args(void, [long])
    def setOnLongPressUpdateInterval(self, intervalMillis):
        pass

    @args(void, [NumberPicker.OnScrollListener])
    def setOnScrollListener(self, onScrollListener):
        pass

    @args(void, [NumberPicker.OnValueChangeListener])
    def setOnValueChangedListener(self, onValueChangedListener):
        pass

    @args(void, [int])
    def setValue(self, value):
        pass

    @args(void, [bool])
    def setWrapSelectorWheel(self, wrapSelectorWheel):
        pass

    ### Protected methods omitted


class PopupMenu(Object):

    __interfaces__ = [] # MenuBuilder.Callback, MenuPresenter.Callback
    
    class OnDismissListener:
    
        @args(void, [PopupMenu])
        def onDismiss(self, menu):
            pass
    
    class OnMenuItemClickListener:
    
        @args(bool, [MenuItem])
        def onMenuItemClick(self, item):
            pass
    
    @args(void, [Context, View])
    def __init__(self, context, anchor):
        pass
    
    @args(void, [Context, View, int])
    def __init__(self, context, anchor, gravity):
        pass
    
    @args(void, [Context, View, int, int, int])
    def __init__(self, context, anchor, gravity, popupStyleAttr, popupStyleRes):
        pass
    
    def dismiss(self):
        pass
    
    @args(View.OnTouchListener, [])
    def getDragToOpenListener(self):
        pass
    
    @args(int, [])
    def getGravity(self):
        pass
    
    @args(Menu, [])
    def getMenu(self):
        pass
    
    @args(MenuInflater, [])
    def getMenuInflater(self):
        pass
    
    @args(void, [int])
    def inflate(self, menuRes):
        pass
    
    @args(void, [int])
    def setGravity(self, gravity):
        pass
    
    @args(void, [PopupMenu.OnDismissListener])
    def setOnDismissListener(self, listener):
        pass
    
    @args(void, [PopupMenu.OnMenuItemClickListener])
    def setOnMenuItemClickListener(self, listener):
        pass
    
    def show(self):
        pass


class PopupWindow(Object):

    INPUT_METHOD_FROM_FOCUSABLE = 0
    INPUT_METHOD_NEEDED = 1
    INPUT_METHOD_NOT_NEEDED = 2
    
    class OnDismissListener:
    
        def onDismiss(self):
            pass
    
    @args(void, [Context])
    def __init__(self, context):
        pass
    
    @args(void, [Context, AttributeSet])
    def __init__(self, context, attrs):
        pass
    
    @args(void, [Context, AttributeSet, int])
    def __init__(self, context, attrs, defStyleAttr):
        pass
    
    @api(11)
    @args(void, [Context, AttributeSet, int, int])
    def __init__(self, context, attrs, defStyleAttr, defStyleRes):
        pass
    
    def __init__(self):
        pass
    
    @args(void, [View])
    def __init__(self, contentView):
        pass
    
    @args(void, [int, int])
    def __init__(self, width, height):
        pass
    
    @args(void, [View, int, int])
    def __init__(self, contentView, width, height):
        pass
    
    @args(void, [View, int, int, bool])
    def __init__(self, contentView, width, height, focusable):
        pass
    
    def dismiss(self):
        pass
    
    @args(int, [])
    def getAnimationStyle(self):
        pass
    
    @args(Drawable, [])
    def getBackground(self):
        pass
    
    @args(View, [])
    def getContentView(self):
        pass
    
    @api(21)
    @args(float, [])
    def getElevation(self):
        pass
    
    @args(int, [])
    def getHeight(self):
        pass
    
    @args(int, [])
    def getInputMethodMode(self):
        pass
    
    @args(int, [View])
    def getMaxAvailableHeight(self, anchor):
        pass
    
    @api(3)
    @args(int, [View, int])
    def getMaxAvailableHeight(self, anchor, yOffset):
        pass
    
    @api(23)
    @args(bool, [])
    def getOverlapAnchor(self):
        pass
    
    @api(4)
    @args(int, [])
    def getSoftInputMode(self):
        pass
    
    @args(int, [])
    def getWidth(self):
        pass
    
    @api(23)
    @args(int, [])
    def getWindowLayoutType(self):
        pass
    
    @api(3)
    @args(bool, [])
    def isAboveAnchor(self):
        pass
    
    @api(22)
    @args(bool, [])
    def isAttachedInDecor(self):
        pass
    
    @api(3)
    @args(bool, [])
    def isClippingEnabled(self):
        pass
    
    @args(bool, [])
    def isFocusable(self):
        pass
    
    @api(3)
    @args(bool, [])
    def isOutsideTouchable(self):
        pass
    
    @args(bool, [])
    def isShowing(self):
        pass
    
    @api(11)
    @args(bool, [])
    def isSplitTouchEnabled(self):
        pass
    
    @api(3)
    @args(bool, [])
    def isTouchable(self):
        pass
    
    @args(void, [int])
    def setAnimationStyle(self, animationStyle):
        pass
    
    @api(22)
    @args(void, [bool])
    def setAttachedInDecor(self, enabled):
        pass
    
    @args(void, [Drawable])
    def setBackgroundDrawable(self, background):
        pass
    
    @api(3)
    @args(void, [bool])
    def setClippingEnabled(self, enabled):
        pass
    
    @args(void, [View])
    def setContentView(self, contentView):
        pass
    
    @api(21)
    @args(void, [float])
    def setElevation(self, elevation):
        pass
    
    @api(23)
    @args(void, [Transition])
    def setEnterTransition(self, enterTransition):
        pass
    
    @api(23)
    @args(void, [Transition])
    def setExitTransition(self, exitTransition):
        pass
    
    @args(void, [bool])
    def setFocusable(self, focusable):
        pass
    
    @args(void, [int])
    def setHeight(self, height):
        pass
    
    def setIgnoreCheekPress(self):
        pass
    
    @api(3)
    @args(void, [int])
    def setInputMethodMode(self, mode):
        pass
    
    @args(void, [PopupWindow.OnDismissListener])
    def setOnDismissListener(self, onDismissListener):
        pass
    
    @api(3)
    @args(void, [bool])
    def setOutsideTouchable(self, touchable):
        pass
    
    @args(23)
    @args(void, [bool])
    def setOverlapAnchor(self, overlapAnchor):
        pass
    
    @api(4)
    @args(void, [int])
    def setSoftInputMode(self, mode):
        pass
    
    @api(11)
    @args(void, [bool])
    def setSplitTouchEnabled(self, enabled):
        pass
    
    @api(3)
    @args(void, [View.OnTouchListener])
    def setTouchInterceptor(self, l):
        pass
    
    @api(3)
    @args(void, [bool])
    def setTouchable(self, touchable):
        pass
    
    @args(void, [int])
    def setWidth(self, width):
        pass
    
    @api(3, 23)
    @args(void, [int, int])
    def setWindowLayoutMode(self, widthSpec, heightSpec):
        pass
    
    @api(23)
    @args(void, [int])
    def setWindowLayoutType(self, layoutType):
        pass
    
    @api(19)
    @args(void, [View, int, int, int])
    def showAsDropDown(self, anchor, xoff, yoff, gravity):
        pass
    
    @args(void, [View, int, int])
    def showAsDropDown(self, anchor, xoff, yoff):
        pass
    
    @args(void, [View])
    def showAsDropDown(self, anchor):
        pass
    
    @args(void, [View, int, int, int])
    def showAtLocation(self, parent, gravity, x, y):
        pass
    
    @args(void, [View, int, int])
    def update(self, anchor, width, height):
        pass
    
    @args(void, [int, int, int, int])
    def update(self, x, y, width, height):
        pass
    
    @api(3)
    @args(void, [View, int, int, int, int])
    def update(self, anchor, xoff, yoff, width, height):
        pass
    
    @api(3)
    def update(self):
        pass
    
    @api(3)
    @args(void, [int, int, int, int, bool])
    def update(self, x, y, width, height, force):
        pass
    
    @api(4)
    @args(void, [int, int])
    def update(self, width, height):
        pass


class ProgressBar(View):

    @args(void, [Context])
    def __init__(self, context):
        pass
    
    @args(void, [Context, AttributeSet])
    def __init__(self, context, attrs):
        pass
    
    @args(void, [Context, AttributeSet, int])
    def __init__(self, context, attrs, defStyleAttr):
        pass
    
    @args(void, [Context, AttributeSet, int, int])
    def __init__(self, context, attrs, defStyleAttr, defStyleRes):
        pass
    
    @args(void, [float, float])
    def drawableHotspotChanged(self, x, y):
        pass
    
    @args(CharSequence, [])
    def getAccessibilityClassName(self):
        pass
    
    @args(Drawable, [])
    def getIndeterminateDrawable(self):
        pass
    
    ### ...
    
    @synchronized
    @args(void, [int])
    def setMax(self, max):
        pass
    
    @synchronized
    @args(void, [int])
    def setProgress(self, progress):
        pass
    
    ### ...
    
    @args(void, [int])
    def setVisibility(self, v):
        pass
    
    ### ...


class RadioButton(CompoundButton):

    @args(void, [Context])
    def __init__(self, context):
        pass
    
    @args(void, [Context, AttributeSet])
    def __init__(self, context, attrs):
        pass
    
    @args(void, [Context, AttributeSet, int])
    def __init__(self, context, attrs, defStyleAttr):
        pass
    
    @args(void, [Context, AttributeSet, int, int])
    def __init__(self, context, attrs, defStyleAttr, defStyleRes):
        pass
    
    @args(CharSequence, [])
    def getAccessibilityClassName(self):
        pass
    
    def toggle(self):
        pass


class RadioGroup(LinearLayout):

    class LayoutParams(LinearLayout.LayoutParams):
    
        @args(void, [Context, AttributeSet])
        def __init__(self, context, attrs):
            pass
        
        @args(void, [int, int])
        def __init__(self, width, height):
            pass
        
        @args(void, [int, int, int])
        def __init__(self, width, height, initWeight):
            pass
        
        @args(void, [ViewGroup.LayoutParams])
        def __init__(self, source):
            pass
        
        @args(void, [ViewGroup.MarginLayoutParams])
        def __init__(self, source):
            pass
    
    class OnCheckedChangeListener:
    
        @args(void, [RadioGroup, int])
        def onCheckedChanged(self, group, checkedId):
            pass
    
    @args(void, [Context])
    def __init__(self, context):
        pass
    
    @args(void, [Context, AttributeSet])
    def __init__(self, context, attrs):
        pass
    
    @args(void, [View, int, ViewGroup.LayoutParams])
    def addView(self, child, index, params):
        pass
    
    @args(void, [int])
    def check(self, id):
        pass
    
    def clearCheck(self):
        pass
    
    @args(RadioGroup.LayoutParams, [AttributeSet])
    def generateLayoutParams(self, attrs):
        pass
    
    @args(CharSequence, [])
    def getAccessibilityClassName(self):
        pass
    
    @args(int, [])
    def getCheckedRadioButtonId(self):
        pass
    
    @args(void, [RadioGroup.OnCheckedChangeListener])
    def setOnCheckedChangeListener(self, listener):
        pass
    
    @args(void, [ViewGroup.OnHierarchyChangeListener])
    def setOnHierarchyChangeListener(self, listener):
        pass


class RatingBar(AbsSeekBar):

    class OnRatingBarChangeListener:
    
        @args(void, [RatingBar, float, bool])
        def onRatingChanged(self, ratingBar, rating, fromUser):
            pass
    
    @args(void, [Context])
    def __init__(self, context):
        pass
    
    @args(void, [Context, AttributeSet])
    def __init__(self, context, attrs):
        pass
    
    @args(void, [Context, AttributeSet, int])
    def __init__(self, context, attrs, defStyleAttr):
        pass
    
    @args(void, [Context, AttributeSet, int, int])
    def __init__(self, context, attrs, defStyleAttr, defStyleRes):
        pass
    
    @args(CharSequence, [])
    def getAccessibilityClassName(self):
        pass
    
    @args(int, [])
    def getNumStars(self):
        pass
    
    @args(RatingBar.OnRatingBarChangeListener, [])
    def getOnRatingBarChangeListener(self):
        pass
    
    @args(float, [])
    def getRating(self):
        pass
    
    @args(float, [])
    def getStepSize(self):
        pass
    
    @args(bool, [])
    def isIndicator(self):
        pass
    
    @args(void, [bool])
    def setIsIndicator(self, isIndicator):
        pass
    
    @synchronized
    @args(void, [int])
    def setMax(self, max):
        pass
    
    @args(void, [int])
    def setNumStars(self, numStars):
        pass
    
    @args(void, [RatingBar.OnRatingBarChangeListener])
    def setOnRatingBarChangeListener(self, listener):
        pass
    
    @args(void, [float])
    def setRating(self, rating):
        pass
    
    @args(void, [float])
    def setStepSize(self, stepSize):
        pass


class RelativeLayout(ViewGroup):

    ABOVE = 2
    ALIGN_BASELINE = 4
    ALIGN_BOTTOM = 8
    ALIGN_END = 19
    ALIGN_LEFT = 5
    ALIGN_PARENT_BOTTOM = 12
    ALIGN_PARENT_END = 21
    ALIGN_PARENT_LEFT = 9
    ALIGN_PARENT_RIGHT = 11
    ALIGN_PARENT_START = 20
    ALIGN_PARENT_TOP = 10
    ALIGN_RIGHT = 7
    ALIGN_START = 18
    ALIGN_TOP = 6
    BELOW = 3
    CENTER_HORIZONTAL = 14
    CENTER_IN_PARENT = 13
    CENTER_VERTICAL = 15
    END_OF = 17
    LEFT_OF = 0
    RIGHT_OF = 1
    START_OF = 16
    TRUE = -1
    
    class LayoutParams(ViewGroup.MarginLayoutParams):
    
        __fields__ = {
            "alignWithParent": bool
            }
        
        @args(void, [int, int])
        def __init__(self, width, height):
            pass
        
        @args(void, [ViewGroup.LayoutParams])
        def __init__(self, source):
            pass
        
        @args(void, [ViewGroup.MarginLayoutParams])
        def __init__(self, source):
            pass
        
        @args(void, [RelativeLayout.LayoutParams])
        def __init__(self, source):
            pass
        
        @args(void, [int])
        def addRule(self, verb):
            pass
        
        @args(void, [int, int])
        def addRule(self, verb, anchor):
            pass
        
        @args(String, [String])
        def debug(self, string):
            pass
        
        @args(int, [int])
        def getRule(self, verb):
            pass
        
        @args([int], [])
        def getRules(self):
            pass
        
        @args(void, [int])
        def removeRule(self, verb):
            pass
    
    @args(void, [Context])
    def __init__(self, context):
        pass
    
    @args(RelativeLayout.LayoutParams, [AttributeSet])
    def generateLayoutParams(self, attributes):
        pass
    
    @args(int, [])
    def getGravity(self):
        pass
    
    @args(void, [int])
    def setGravity(self, gravity):
        pass
    
    @args(void, [int])
    def setHorizontalGravity(self, gravity):
        pass
    
    @args(void, [int])
    def setIgnoreGravity(self, viewId):
        pass
    
    @args(void, [int])
    def setVerticalGravity(self, gravity):
        pass


class RemoteViews(Object):

    __interfaces__ = [Parcelable, LayoutInflater.Filter]
    __static_fields__ = {
        "CREATOR": Parcelable.Creator(RemoteViews)
        }
    
    class ActionException(Exception):
    
        @args(void, [Exception])
        def __init__(self, exception):
            pass
        
        @args(void, [String])
        def __init__(self, message):
            pass
    
    class RemoteView:
    
        __interfaces__ = [Annotation]
    
    @args(void, [String, int])
    def __init__(self, packageName, layoutId):
        pass
    
    @args(void, [RemoteViews, RemoteViews])
    def __init__(self, landscape, portrait):
        pass
    
    @args(void, [Parcel])
    def __init__(self, parcel):
        pass
    
    @args(void, [int, RemoteViews])
    def addView(self, viewId, nestedView):
        pass
    
    @args(View, [Context, ViewGroup])
    def apply(self, context, parent):
        pass
    
    @args(RemoteViews, [])
    def clone(self):
        pass
    
    @args(int, [])
    def getLayoutId(self):
        pass
    
    @args(String, [])
    def getPackage(self):
        pass
    
    @args(bool, [Class])
    def onLoadClass(self, class_):
        pass
    
    @args(void, [Context, View])
    def reapply(self, context, view):
        pass
    
    @args(void, [int])
    def removeAllViews(self, viewId):
        pass
    
    @args(void, [int, int])
    def setAccessibilityTraversalAfter(self, viewId, nextId):
        pass
    
    @args(void, [int, int])
    def setAccessibilityTraversalBefore(self, viewId, nextId):
        pass
    
    @args(void, [int, String, Bitmap])
    def setBitmap(self, viewId, methodName, value):
        pass
    
    @args(void, [int, String, bool])
    def setBoolean(self, viewId, methodName, value):
        pass
    
    @args(void, [int, String, Bundle])
    def setBundle(self, viewId, methodName, value):
        pass
    
    @args(void, [int, String, byte])
    def setByte(self, viewId, methodName, value):
        pass
    
    @args(void, [int, String, char])
    def setChar(self, viewId, methodName, value):
        pass
    
    @args(void, [int, String, CharSequence])
    def setCharSequence(self, viewId, methodName, value):
        pass
    
    @args(void, [int, long, String, bool])
    def setChronometer(self, viewId, base, format, started):
        pass
    
    @args(void, [int, CharSequence])
    def setContentDescription(self, viewId, contentDescription):
        pass
    
    @args(void, [int, int])
    def setDisplayedChild(self, viewId, childIndex):
        pass
    
    @args(void, [int, String, double])
    def setDouble(self, viewId, methodName, value):
        pass
    
    @args(void, [int, int])
    def setEmptyView(self, viewId, emptyViewId):
        pass
    
    @args(void, [int, String, float])
    def setFloat(self, viewId, methodName, value):
        pass
    
    #@args(void, [int, String, Icon])
    #def setIcon(self, viewId, methodName, value):
    #    pass
    
    @args(void, [int, Bitmap])
    def setImageViewBitmap(self, viewId, bitmap):
        pass
    
    #@args(void, [int, Icon])
    #def setImageViewIcon(self, viewId, icon):
    #    pass
    
    @args(void, [int, int])
    def setImageViewResource(self, viewId, srcId):
        pass
    
    @args(void, [int, Uri])
    def setImageViewUri(self, viewId, Uri):
        pass
    
    @args(void, [int, String, int])
    def setInt(self, viewId, methodName, value):
        pass
    
    @args(void, [int, String, Intent])
    def setIntent(self, viewId, methodName, value):
        pass
    
    @args(void, [int, int])
    def setLabelFor(self, viewId, labeledId):
        pass
    
    @args(void, [int, String, long])
    def setLong(self, viewId, methodName, value):
        pass
    
    @args(void, [int, Intent])
    def setOnClickFillInIntent(self, viewId, fillInIntent):
        pass
    
    @args(void, [int, PendingIntent])
    def setOnClickPendingIntent(self, viewId, pendingIntent):
        pass
    
    @args(void, [int, PendingIntent])
    def setPendingIntentTemplate(self, viewId, template):
        pass
    
    @args(void, [int, int, int, bool])
    def setProgressBar(self, viewId, max, progress, indeterminate):
        pass
    
    @args(void, [int, int])
    def setRelativeScrollPosition(self, viewId, offset):
        pass
    
    # Deprecated in API level 14
    @args(void, [int, int, Intent])
    def setRemoteAdapter(self, appWidgetId, viewId, intent):
        pass
    
    @args(void, [int, int])
    def setScrollPosition(self, viewId, position):
        pass
    
    @args(void, [int, String, short])
    def setShort(self, viewId, methodName, value):
        pass
    
    @args(void, [int, String, String])
    def setString(self, viewId, methodName, value):
        pass
    
    @args(void, [int, int])
    def setTextColor(self, viewId, color):
        pass
    
    @args(void, [int, int, int, int, int])
    def setTextViewCompoundDrawables(self, viewId, left, top, right, bottom):
        pass
    
    @args(void, [int, int, int, int, int])
    def setTextViewCompoundDrawablesRelative(self, viewId, start, top, end, bottom):
        pass
    
    @args(void, [int, CharSequence])
    def setTextViewText(self, viewId, text):
        pass
    
    @args(void, [int, String, Uri])
    def setUri(self, viewId, methodName, value):
        pass
    
    @args(void, [int, int, int, int, int])
    def setViewPadding(self, viewId, left, top, right, bottom):
        pass
    
    @args(void, [int, int])
    def setViewVisibility(self, viewId, visibility):
        pass
    
    @args(void, [int])
    def showNext(self, viewId):
        pass
    
    @args(void, [int])
    def showPrevious(self, viewId):
        pass


class RemoteViewsService(Service):

    class RemoteViewsFactory:
    
        @args(int, [])
        def getCount(self):
            pass
        
        @args(long, [int])
        def getItemId(self, position):
            pass
        
        @args(RemoteViews, [])
        def getLoadingView(self):
            pass
        
        @args(RemoteViews, [int])
        def getViewAt(self, position):
            pass
        
        @args(int, [])
        def getViewTypeCount(self):
            pass
        
        @args(bool, [])
        def hasStableIds(self):
            pass
        
        def onCreate(self):
            pass
        
        def onDataSetChanged(self):
            pass
        
        def onDestroy(self):
            pass
    
    def __init__(self):
        pass
    
    @args(IBinder, [Intent])
    def onBind(self, intent):
        pass
    
    @abstract
    @args(RemoteViewsService.RemoteViewsFactory, [Intent])
    def onGetViewFactory(self, intent):
        pass


class ScrollView(FrameLayout):

    @args(void, [Context])
    def __init__(self, context):
        pass
    
    @args(bool, [int])
    def arrowScroll(self, direction):
        pass
    
    def computeScroll(self):
        pass
    
    @args(void, [int])
    def fling(self, velocityY):
        pass
    
    @args(bool, [int])
    def fullScroll(self, direction):
        pass
    
    @args(CharSequence, [])
    def getAccessibilityClassName(self):
        pass
    
    @args(int, [])
    def getMaxScrollAmount(self):
        pass
    
    @args(bool, [])
    def isFillViewport(self):
        pass
    
    @args(bool, [])
    def isSmoothScrollingEnabled(self):
        pass
    
    @args(bool, [MotionEvent])
    def onGenericMotionEvent(self, event):
        pass
    
    @args(bool, [MotionEvent])
    def onInterceptTouchEvent(self, event):
        pass
    
    @args(bool, [View, float, float, bool])
    def onNestedFling(self, target, velocityX, velocityY, consumed):
        pass
    
    @args(void, [View, int, int, int, int])
    def onNestedScroll(self, target, dxConsumed, dyConsumed, dxUnconsumed, dyUnconsumed):
        pass
    
    @args(void, [View, View, int])
    def onNestedScrollAccepted(self, child, target, axes):
        pass
    
    @args(bool, [View, View, int])
    def onStartNestedScroll(self, child, target, nestedScrollAxes):
        pass
    
    @args(void, [View])
    def onStopNestedScroll(self, target):
        pass
    
    @args(bool, [MotionEvent])
    def onTouchEvent(self, event):
        pass
    
    @args(bool, [int])
    def pageScroll(self, direction):
        pass
    
    @args(void, [int, int])
    def scrollTo(self, x, y):
        pass
    
    @args(void, [bool])
    def setFillViewport(self, fillViewport):
        pass
    
    @args(void, [int])
    def setOverScrollMode(self, mode):
        pass
    
    @args(void, [bool])
    def setSmoothScrollingEnabled(self, smoothScrollingEnabled):
        pass
    
    @final
    @args(void, [int, int])
    def smoothScrollBy(self, dx, dy):
        pass
    
    @final
    @args(void, [int, int])
    def smoothScrollTo(self, x, y):
        pass


class Space(View):

    @args(void, [Context])
    def __init__(self, context):
        pass
    
    @args(void, [Context, AttributeSet])
    def __init__(self, context, attrs):
        pass
    
    @args(void, [Context, AttributeSet, int])
    def __init__(self, context, attrs, defStyleAttr):
        pass
    
    @args(void, [Context, AttributeSet, int, int])
    def __init__(self, context, attrs, defStyleAttr, defStyleRes):
        pass


# The base class of Spinner is actually an intermediate base class (AbsSpinner)
# that we don't represent.
class Spinner(AdapterView):

    __interfaces__ = [DialogInterface.OnClickListener]
    
    @args(void, [Context])
    def __init__(self, context):
        pass
    
    @args(void, [Context, int])
    def __init__(self, context, mode):
        pass
    
    @args(int, [])
    def getCount(self):
        pass
    
    @args(int, [])
    def getDropDownHorizontalOffset(self):
        pass
    
    @args(int, [])
    def getDropDownVerticalOffset(self):
        pass
    
    @args(int, [])
    def getDropDownWidth(self):
        pass
    
    @args(Drawable, [])
    def getPopupBackground(self):
        pass
    
    @args(Context, [])
    def getPopupContext(self):
        pass
    
    @args(CharSequence, [])
    def getPrompt(self):
        pass
    
    @args(void, [Parcelable])
    def onRestoreInstanceState(self, state):
        pass
    
    @args(void, [SpinnerAdapter])
    def setAdapter(self, adapter):
        pass
    
    @args(void, [int])
    def setDropDownHorizontalOffset(self, pixels):
        pass
    
    @args(void, [int])
    def setDropDownVerticalOffset(self, pixels):
        pass
    
    @args(void, [int])
    def setDropDownWidth(self, pixels):
        pass
    
    @args(void, [int])
    def setGravity(self, gravity):
        pass
    
    @args(void, [Drawable])
    def setPopupBackgroundDrawable(self, background):
        pass
    
    @args(void, [int])
    def setPopupBackgroundResource(self, resId):
        pass
    
    @args(void, [CharSequence])
    def setPrompt(self, prompt):
        pass
    
    @args(void, [int])
    def setPromptId(self, promptId):
        pass


class SpinnerAdapter:

    __interfaces__ = [Adapter]
    
    @args(View, [int, View, ViewGroup])
    def getDropDownView(self, position, convertView, parent):
        pass


class StackView(AdapterView): # AdapterViewAnimator

    @args(void, [Context])
    def __init__(self, context):
        pass
    
    def advance(self):
        pass
    
    @args(CharSequence, [])
    def getAccessibilityClassName(self):
        pass
    
    @args(bool, [MotionEvent])
    def onGenericMotionEvent(self, event):
        pass
    
    @args(bool, [MotionEvent])
    def onInterceptTouchEvent(self, event):
        pass
    
    @args(bool, [MotionEvent])
    def onTouchEvent(self, event):
        pass
    
    def showNext(self):
        pass
    
    def showPrevious(self):
        pass


class Switch(CompoundButton):

    @args(void, [Context])
    def __init__(self, context):
        pass
    
    @args(void, [Context, AttributeSet])
    def __init__(self, context, attrs):
        pass
    
    @args(void, [Context, AttributeSet, int])
    def __init__(self, context, attrs, defStyleAttr):
        pass
    
    @args(void, [Context, AttributeSet, int, int])
    def __init__(self, context, attrs, defStyleAttr, defStyleRes):
        pass
    
    @args(void, [Canvas])
    def draw(self, c):
        pass
    
    @args(void, [float, float])
    def drawableHotspotChanged(self, x, yself):
        pass
    
    @args(CharSequence, [])
    def getAccessibilityClassName(self):
        pass
    
    @args(int, [])
    def getCompoundPaddingLeft(self):
        pass
    
    @args(int, [])
    def getCompoundPaddingRight(self):
        pass
    
    @args(bool, [])
    def getShowText(self):
        pass
    
    @args(bool, [])
    def getSplitTrack(self):
        pass
    
    @args(int, [])
    def getSwitchMinWidth(self):
        pass
    
    @args(int, [])
    def getSwitchPadding(self):
        pass
    
    @args(CharSequence, [])
    def getTextOff(self):
        pass
    
    @args(CharSequence, [])
    def getTextOn(self):
        pass
    
    @args(Drawable, [])
    def getThumbDrawable(self):
        pass
    
    @args(int, [])
    def getThumbTextPadding(self):
        pass
    
    @args(ColorStateList, [])
    def getThumbTintList(self):
        pass
    
    @args(PorterDuff.Mode, [])
    def getThumbTintMode(self):
        pass
    
    @args(Drawable, [])
    def getTrackDrawable(self):
        pass
    
    @args(ColorStateList, [])
    def getTrackTintList(self):
        pass
    
    @args(PorterDuff.Mode, [])
    def getTrackTintMode(self):
        pass
    
    def jumpDrawablesToCurrentState(self):
        pass
    
    @args(void, [int, int])
    def onMeasure(self, widthMeasureSpec, heightMeasureSpec):
        pass
    
    @args(void, [ViewStructure])
    def onProvideStructure(self, structure):
        pass
    
    @args(bool, [MotionEvent])
    def onTouchEvent(self, ev):
        pass
    
    @args(void, [bool])
    def setChecked(self, checked):
        pass
    
    ### API level 21 and later
    @args(void, [bool])
    def setShowText(self, showText):
        pass
    
    @args(void, [bool])
    def setSplitTrack(self, splitTrack):
        pass
    
    @args(void, [int])
    def setSwitchMinWidth(self, pixels):
        pass
    
    @args(void, [int])
    def setSwitchPadding(self, pixels):
        pass
    
    @args(void, [Context, int])
    def setSwitchTextAppearance(self, context, resid):
        pass
    
    @args(void, [Typeface, int])
    def setSwitchTypeface(self, tf, style):
        pass
    
    @args(void, [Typeface])
    def setSwitchTypeface(self, tf):
        pass
    
    @args(void, [CharSequence])
    def setTextOff(self, textOff):
        pass
    
    @args(void, [CharSequence])
    def setTextOn(self, textOn):
        pass
    
    @args(void, [Drawable])
    def setThumbDrawable(self, thumb):
        pass
    
    @args(void, [int])
    def setThumbResource(self, resId):
        pass
    
    @args(void, [int])
    def setThumbTextPadding(self, pixels):
        pass
    
    @args(void, [ColorStateList])
    def setThumbTintList(self, tint):
        pass
    
    @args(void, [PorterDuff.Mode])
    def setThumbTintMode(self, tintMode):
        pass
    
    @args(void, [Drawable])
    def setTrackDrawable(self, track):
        pass
    
    @args(void, [int])
    def setTrackResource(self, resId):
        pass
    
    @args(void, [ColorStateList])
    def setTrackTintList(self, tint):
        pass
    
    @args(void, [PorterDuff.Mode])
    def setTrackTintMode(self, tintMode):
        pass
    
    def toggle(self):
        pass


class TabHost(FrameLayout):

    __interfaces__ = [View.TreeObserver.OnTouchModeChangeListener]
    
    class OnTabChangeListener:
    
        @args(void, [String])
        def onTabChanged(self, tabId):
            pass
    
    class TabContentFactory:
    
        @args(View, [String])
        def createTabContent(self, tag):
            pass
    
    class TabSpec(Object):
    
        @args(String, [])
        def getTag():
            pass
        
        @args(TabHost.TabSpec, [int])
        def setContent(self, viewId):
            pass
        
        @args(TabHost.TabSpec, [Intent])
        def setContent(self, intent):
            pass
        
        @args(TabHost.TabSpec, [TabHost.TabContentFactory])
        def setContent(self, contentFactory):
            pass
        
        @args(TabHost.TabSpec, [CharSequence])
        def setIndicator(self, label):
            pass
        
        @args(TabHost.TabSpec, [View])
        def setIndicator(self, view):
            pass
        
        @args(TabHost.TabSpec, [CharSequence, Drawable])
        def setIndicator(self, label, icon):
            pass
    
    @args(void, [Context])
    def __init__(self, context):
        pass
    
    @args(void, [Context, AttributeSet])
    def __init__(self, context, attrs):
        pass
    
    @args(void, [Context, AttributeSet, int])
    def __init__(self, context, attrs, defStyleAttr):
        pass
    
    @args(void, [Context, AttributeSet, int, int])
    def __init__(self, context, attrs, defStyleAttr, defStyleRes):
        pass
    
    @args(void, [TabHost.TabSpec])
    def addTab(self, tabSpec):
        pass
    
    def clearAllTabs(self):
        pass
    
    @args(bool, [KeyEvent])
    def dispatchKeyEvent(self, event):
        pass
    
    @args(void, [bool])
    def dispatchWindowFocusChanged(self, hasFocus):
        pass
    
    @args(CharSequence, [])
    def getAccessibilityClassName(self):
        pass
    
    @args(int, [])
    def getCurrentTab(self):
        pass
    
    @args(String, [])
    def getCurrentTabTag(self):
        pass
    
    @args(View, [])
    def getCurrentTabView(self):
        pass
    
    @args(View, [])
    def getCurrentView(self):
        pass
    
    @args(FrameLayout, [])
    def getTabContentView(self):
        pass
    
    @args(TabWidget, [])
    def getTabWidget(self):
        pass
    
    @args(TabHost.TabSpStringec, [])
    def newTabSpec(self, tag):
        pass
    
    @args(void, [bool])
    def onTouchModeChanged(self, isInTouchMode):
        pass
    
    @args(void, [int])
    def setCurrentTab(self, index):
        pass
    
    @args(void, [String])
    def setCurrentTabByTag(self, tag):
        pass
    
    @args(void, [TabHost.OnTabChangeListener])
    def setOnTabChangedListener(self, l):
        pass
    
    def setup(self):
        pass
    
    @args(void, [LocalActivityManager])
    def setup(self, activityGroup):
        pass


class TableLayout(LinearLayout):

    class LayoutParams(LinearLayout.LayoutParams):
    
        def __init__(self):
            pass
        
        @args(void, [int, int])
        def __init__(self, width, height):
            pass
        
        @args(void, [int, int, float])
        def __init__(self, width, height, initWeight):
            pass
        
        @args(void, [ViewGroup.LayoutParams])
        def __init__(self, source):
            pass
        
        @args(void, [ViewGroup.MarginLayoutParams])
        def __init__(self, source):
            pass
        
        #@args(void, [android.content.res.TypedArray, int, int])
        #def setBaseAttributes(self, a, widthAttr, heightAttr):
        #    pass
    
    @args(void, [Context])
    def __init__(self, context):
        pass
    
    @args(bool, [int])
    def isColumnCollapsed(self, columnIndex):
        pass
    
    @args(bool, [int])
    def isColumnShrinkable(self, columnIndex):
        pass
    
    @args(bool, [int])
    def isColumnStretchable(self, columnIndex):
        pass
    
    @args(bool, [])
    def isShrinkAllColumns(self):
        pass
    
    @args(bool, [])
    def isStretchAllColumns(self):
        pass
    
    @args(void, [int, bool])
    def setColumnCollapsed(self, columnIndex, isCollapsed):
        pass
    
    @args(void, [int, bool])
    def setColumnShrinkable(self, columnIndex, isShrinkable):
        pass
    
    @args(void, [int, bool])
    def setColumnStretchable(self, columnIndex, isStretchable):
        pass
    
    @args(void, [ViewGroup.OnHierarchyChangeListener])
    def setOnHierarchyChangeListener(self, listener):
        pass
    
    @args(void, [bool])
    def setShrinkAllColumns(self, shrinkAllColumns):
        pass
    
    @args(void, [bool])
    def setStretchAllColumns(self, stretchAllColumns):
        pass


class TableRow(LinearLayout):

    class LayoutParams(LinearLayout.LayoutParams):
    
        __fields__ = {
            "column": int,
            "span": int
            }
        
        def __init__(self):
            pass
        
        @args(void, [int])
        def __init__(self, column):
            pass
        
        @args(void, [int, int])
        def __init__(self, width, height):
            pass
        
        @args(void, [int, int, float])
        def __init__(self, width, height, initWeight):
            pass
        
        @args(void, [ViewGroup.LayoutParams])
        def __init__(self, source):
            pass
        
        @args(void, [ViewGroup.MarginLayoutParams])
        def __init__(self, source):
            pass
        
        #@args(void, [android.content.res.TypedArray, int, int])
        #def setBaseAttributes(self, a, widthAttr, heightAttr):
        #    pass
    
    @args(void, [Context])
    def __init__(self, context):
        pass
    
    @args(View, [int])
    def getVirtualChildAt(self, i):
        pass
    
    @args(int, [])
    def getVirtualChildCount(self):
        pass
    
    @args(void, [ViewGroup.OnHierarchyChangeListener])
    def setOnHierarchyChangeListener(self, listener):
        pass


class TabWidget(LinearLayout):

    __interfaces__ = [View.OnFocusChangeListener]
    
    @args(void, [Context])
    def __init__(self, context):
        pass
    
    @args(void, [Context, AttributeSet])
    def __init__(self, context, attrs):
        pass
    
    @args(void, [Context, AttributeSet, int])
    def __init__(self, context, attrs, defStyleAttr):
        pass
    
    @args(void, [Context, AttributeSet, int, int])
    def __init__(self, context, attrs, defStyleAttr, defStyleRes):
        pass
    
    @args(void, [View])
    def addView(self, child):
        pass
    
    @args(void, [View])
    def childDrawableStateChanged(self, child):
        pass
    
    @args(void, [Canvas])
    def dispatchDraw(self, canvas):
        pass
    
    @args(void, [int])
    def focusCurrentTab(self, index):
        pass
    
    @args(CharSequence, [])
    def getAccessibilityClassName(self):
        pass
    
    @args(View, [int])
    def getChildTabViewAt(self, index):
        pass
    
    @args(int, [])
    def getTabCount(self):
        pass
    
    @args(bool, [])
    def isStripEnabled(self):
        pass
    
    @args(void, [View, bool])
    def onFocusChange(self, v, hasFocus):
        pass
    
    def removeAllViews(self):
        pass
    
    @args(void, [int])
    def setCurrentTab(self, index):
        pass
    
    @args(void, [Drawable])
    def setDividerDrawable(self, drawable):
        pass
    
    @args(void, [int])
    def setDividerDrawable(self, resId):
        pass
    
    @args(void, [bool])
    def setEnabled(self, enabled):
        pass
    
    @args(void, [Drawable])
    def setLeftStripDrawable(self, drawable):
        pass
    
    @args(void, [int])
    def setLeftStripDrawable(self, resId):
        pass
    
    @args(void, [Drawable])
    def setRightStripDrawable(self, drawable):
        pass
    
    @args(void, [int])
    def setRightStripDrawable(self, resId):
        pass
    
    @args(void, [bool])
    def setStripEnabled(self, stripEnabled):
        pass


class TextClock(TextView):

    ### Added in API level 17
    
    @args(void, [Context])
    def __init__(self, context):
        pass
    
    @args(void, [Context, AttributeSet])
    def __init__(self, context, attrs):
        pass
    
    @args(void, [Context, AttributeSet, int])
    def __init__(self, context, attrs, defStyleAttr):
        pass
    
    @args(void, [Context, AttributeSet, int, int])
    def __init__(self, context, attrs, defStyleAttr, defStyleRes):
        pass
    
    @args(CharSequence, [])
    def getFormat12Hour(self):
        pass
    
    @args(CharSequence, [])
    def getFormat24Hour(self):
        pass
    
    @args(String, [])
    def getTimeZone(self):
        pass
    
    @args(bool, [])
    def is24HourModeEnabled(self):
        pass
    
    @args(void, [CharSequence])
    def setFormat12Hour(self, format):
        pass
    
    @args(void, [CharSequence])
    def setFormat24Hour(self, format):
        pass
    
    @args(void, [String])
    def setTimeZone(self, timeZone):
        pass
    
    ### ...


class TextView(View):

    class BufferType(Enum):
    
        __static_fields__ = {
            "EDITABLE": TextView.BufferType,
            "NORMAL": TextView.BufferType,
            "SPANNABLE": TextView.BufferType
            }
    
        @static
        @args(TextView.BufferType, [String])
        def valueOf(name):
            pass
        
        @final
        @static
        @args([TextView.BufferType], [])
        def values():
            pass
    
    class OnEditorActionListener:
    
        @args(bool, [TextView, int, KeyEvent])
        def onEditorAction(self, view, actionId, event):
            pass
    
    class SavedState(View.BaseSavedState):
    
        __static_fields__ = {"CREATOR": Parcelable.Creator(TextView.SavedState)}
        
        @args(String, [])
        def toString(self):
            pass
        
        @args(void, [Parcel, int])
        def writeToParcel(self, out, flags):
            pass
    
    @args(void, [Context])
    def __init__(self, context):
        pass
    
    @args(void, [Context, AttributeSet])
    def __init__(self, context, attrs):
        pass
    
    @args(void, [Context, AttributeSet, int])
    def __init__(self, context, attrs, defStyleAttr):
        pass
    
    @args(void, [Context, AttributeSet, int, int])
    def __init__(self, context, attrs, defStyleAttr, defStyleRes):
        pass
    
    @args(void, [TextWatcher])
    def addTextChangedListener(self, watcher):
        pass
    
    @final
    @args(void, [CharSequence])
    def append(self, text):
        pass
    
    @args(void, [CharSequence, int, int])
    def append(self, text, start, end):
        pass
    
    def beginBatchEdit(self):
        pass
    
    @args(bool, [int])
    def bringPointIntoView(self, offset):
        pass
    
    def cancelLongPress(self):
        pass
    
    def clearComposingText(self):
        pass
    
    def computeScroll(self):
        pass
    
    @args(void, [int])
    def debug(self, depth):
        pass
    
    @args(bool, [])
    def didTouchFocusSelect(self):
        pass
    
    @args(void, [float, float])
    def drawableHotspotChanged(self, x, y):
        pass
    
    def endBatchEdit(self):
        pass
    
    @args(bool, [ExtractedTextRequest, ExtractedText])
    def extractText(self, request, outText):
        pass
    
    @args(void, [ArrayList(View), CharSequence, int])
    def findViewsWithText(self, outViews, searched, flags):
        pass
    
    @args(CharSequence, [])
    def getAccessibilityClassName(self):
        pass
    
    @final
    @args(int, [])
    def getAutoLinkMask(self):
        pass
    
    @args(int, [])
    def getBaseline(self):
        pass
    
    @args(int, [])
    def getBreakStrategy(self):
        pass
    
    @args(int, [])
    def getCompoundDrawablePadding(self):
        pass
    
    @args(ColorStateList, [])
    def getCompoundDrawableTintList(self):
        pass
    
    @args(PorterDuff.Mode, [])
    def getCompoundDrawableTintMode(self):
        pass
    
    @args([Drawable], [])
    def getCompoundDrawables(self):
        pass
    
    @args([Drawable], [])
    def getCompoundDrawablesRelative(self):
        pass
    
    @args(int, [])
    def getCompoundPaddingBottom(self):
        pass
    
    @args(int, [])
    def getCompoundPaddingEnd(self):
        pass
    
    @args(int, [])
    def getCompoundPaddingLeft(self):
        pass
    
    @args(int, [])
    def getCompoundPaddingRight(self):
        pass
    
    @args(int, [])
    def getCompoundPaddingStart(self):
        pass
    
    @args(int, [])
    def getCompoundPaddingTop(self):
        pass
    
    @final
    @args(int, [])
    def getCurrentHintTextColor(self):
        pass
    
    @final
    @args(int, [])
    def getCurrentTextColor(self):
        pass
    
    @args(ActionMode.Callback, [])
    def getCustomInsertionActionModeCallback(self):
        pass
    
    @args(ActionMode.Callback, [])
    def getCustomSelectionActionModeCallback(self):
        pass
    
    @args(Editable, [])
    def getEditableText(self):
        pass
    
    @args(TextUtils.TruncateAt, [])
    def getEllipsize(self):
        pass
    
    @args(CharSequence, [])
    def getError(self):
        pass
    
    @args(int, [])
    def getExtendedPaddingBottom(self):
        pass
    
    @args(int, [])
    def getExtendedPaddingTop(self):
        pass
    
    @args([InputFilter], [])
    def getFilters(self):
        pass
    
    @args(void, [Rect])
    def getFocusedRect(self, r):
        pass
    
    @args(String, [])
    def getFontFeatureSettings(self):
        pass
    
    @args(bool, [])
    def getFreezesText(self):
        pass
    
    @args(int, [])
    def getGravity(self):
        pass
    
    @args(int, [])
    def getHighlightColor(self):
        pass
    
    @args(CharSequence, [])
    def getHint(self):
        pass
    
    @final
    @args(ColorStateList, [])
    def getHintTextColors(self):
        pass
    
    @args(int, [])
    def getHyphenationFrequency(self):
        pass
    
    @args(int, [])
    def getImeActionId(self):
        pass
    
    @args(CharSequence, [])
    def getImeActionLabel(self):
        pass
    
    @args(int, [])
    def getImeOptions(self):
        pass
    
    @args(bool, [])
    def getIncludeFontPadding(self):
        pass
    
    @args(Bundle, [bool])
    def getInputExtras(self, create):
        pass
    
    @args(int, [])
    def getInputType(self):
        pass
    
    @final
    @args(KeyListener, [])
    def getKeyListener(self):
        pass
    
    @final
    @args(Layout, [])
    def getLayout(self):
        pass
    
    @args(float, [])
    def getLetterSpacing(self):
        pass
    
    @args(int, [int, Rect])
    def getLineBounds(self, line, bounds):
        pass
    
    @args(int, [])
    def getLineCount(self):
        pass
    
    @args(int, [])
    def getLineHeight(self):
        pass
    
    @args(float, [])
    def getLineSpacingExtra(self):
        pass
    
    @args(float, [])
    def getLineSpacingMultiplier(self):
        pass
    
    @final
    @args(ColorStateList, [])
    def getLinkTextColors(self):
        pass
    
    @final
    @args(bool, [])
    def getLinksClickable(self):
        pass
    
    @args(int, [])
    def getMarqueeRepeatLimit(self):
        pass
    
    @args(int, [])
    def getMaxEms(self):
        pass
    
    @args(int, [])
    def getMaxHeight(self):
        pass
    
    @args(int, [])
    def getMaxLines(self):
        pass
    
    @args(int, [])
    def getMaxWidth(self):
        pass
    
    @args(int, [])
    def getMinEms(self):
        pass
    
    @args(int, [])
    def getMinHeight(self):
        pass
    
    @args(int, [])
    def getMinLines(self):
        pass
    
    @args(int, [])
    def getMinWidth(self):
        pass
    
    @final
    @args(MovementMethod, [])
    def getMovementMethod(self):
        pass
    
    @args(int, [float, float])
    def getOffsetForPosition(self, x, y):
        pass
    
    @args(TextPaint, [])
    def getPaint(self):
        pass
    
    @args(int, [])
    def getPaintFlags(self):
        pass
    
    @args(String, [])
    def getPrivateImeOptions(self):
        pass
    
    @args(int, [])
    def getSelectionEnd(self):
        pass
    
    @args(int, [])
    def getSelectionStart(self):
        pass
    
    @args(int, [])
    def getShadowColor(self):
        pass
    
    @args(float, [])
    def getShadowDx(self):
        pass
    
    @args(float, [])
    def getShadowDy(self):
        pass
    
    @args(float, [])
    def getShadowRadius(self):
        pass
    
    @final
    @args(bool, [])
    def getShowSoftInputOnFocus(self):
        pass
    
    @args(CharSequence, [])
    def getText(self):
        pass
    
    @args(CharSequence, [])
    def getText(self):
        pass
    
    @final
    @args(ColorStateList, [])
    def getTextColors(self):
        pass
    
    @args(Locale, [])
    def getTextLocale(self):
        pass
    
    @args(float, [])
    def getTextScaleX(self):
        pass
    
    @args(float, [])
    def getTextSize(self):
        pass
    
    @args(int, [])
    def getTotalPaddingBottom(self):
        pass
    
    @args(int, [])
    def getTotalPaddingEnd(self):
        pass
    
    @args(int, [])
    def getTotalPaddingLeft(self):
        pass
    
    @args(int, [])
    def getTotalPaddingRight(self):
        pass
    
    @args(int, [])
    def getTotalPaddingStart(self):
        pass
    
    @args(int, [])
    def getTotalPaddingTop(self):
        pass
    
    @final
    @args(TransformationMethod, [])
    def getTransformationMethod(self):
        pass
    
    @args(Typeface, [])
    def getTypeface(self):
        pass
    
    @args([URLSpan], [])
    def getUrls(self):
        pass
    
    @args(bool, [])
    def hasOverlappingRendering(self):
        pass
    
    @args(bool, [])
    def hasSelection(self):
        pass
    
    @args(void, [Drawable])
    def invalidateDrawable(self, drawable):
        pass
    
    @args(bool, [])
    def isCursorVisible(self):
        pass
    
    @args(bool, [])
    def isInputMethodTarget(self):
        pass
    
    @args(bool, [])
    def isSuggestionsEnabled(self):
        pass
    
    @args(bool, [])
    def isTextSelectable(self):
        pass
    
    def jumpDrawablesToCurrentState(self):
        pass
    
    @args(int, [])
    def length(self):
        pass
    
    @args(bool, [])
    def moveCursorToVisibleOffset(self):
        pass
    
    def onBeginBatchEdit(self):
        pass
    
    @args(bool, [])
    def onCheckIsTextEditor(self):
        pass
    
    @args(void, [CompletionInfo])
    def onCommitCompletion(self, text):
        pass
    
    @args(void, [CorrectionInfo])
    def onCommitCorrection(self, info):
        pass
    
    @args(InputConnection, [EditorInfo])
    def onCreateInputConnection(self, outAttrs):
        pass
    
    @args(bool, [DragEvent])
    def onDragEvent(self, event):
        pass
    
    @args(void, [int])
    def onEditorAction(self, actionCode):
        pass
    
    def onEndBatchEdit(self):
        pass
    
    def onFinishTemporaryDetach(self):
        pass
    
    @args(bool, [MotionEvent])
    def onGenericMotionEvent(self, event):
        pass
    
    @args(bool, [int, KeyEvent])
    def onKeyDown(self, keyCode, event):
        pass
    
    @args(bool, [int, int, KeyEvent])
    def onKeyMultiple(self, keyCode, repeatCount, event):
        pass
    
    @args(bool, [int, KeyEvent])
    def onKeyPreIme(self, keyCode, event):
        pass
    
    @args(bool, [int, KeyEvent])
    def onKeyShortcut(self, keyCode, event):
        pass
    
    @args(bool, [int, KeyEvent])
    def onKeyUp(self, keyCode, event):
        pass
    
    @args(bool, [])
    def onPreDraw(self):
        pass
    
    @args(bool, [String, Bundle])
    def onPrivateIMECommand(self, action, data):
        pass
    
    @args(void, [ViewStructure])
    def onProvideStructure(self, structure):
        pass
    
    @args(void, [Parcelable])
    def onRestoreInstanceState(self, state):
        pass
    
    @args(void, [int])
    def onRtlPropertiesChanged(self, layoutDirection):
        pass
    
    @args(Parcelable, [])
    def onSaveInstanceState(self):
        pass
    
    @args(void, [int])
    def onScreenStateChanged(self, screenState):
        pass
    
    def onStartTemporaryDetach(self):
        pass
    
    @args(bool, [int])
    def onTextContextMenuItem(self, id):
        pass
    
    @args(bool, [MotionEvent])
    def onTouchEvent(self, event):
        pass
    
    @args(bool, [MotionEvent])
    def onTrackballEvent(self, event):
        pass
    
    @args(void, [bool])
    def onWindowFocusChanged(self, hasWindowFocus):
        pass
    
    @args(bool, [])
    def performLongClick(self):
        pass
    
    @args(void, [TextWatcher])
    def removeTextChangedListener(self, watcher):
        pass
    
    @args(void, [bool])
    def setAllCaps(self, allCaps):
        pass
    
    @final
    @args(void, [int])
    def setAutoLinkMask(self, mask):
        pass
    
    @args(void, [int])
    def setBreakStrategy(self, breakStrategy):
        pass
    
    @args(void, [int])
    def setCompoundDrawablePadding(self, pad):
        pass
    
    @args(void, [ColorStateList])
    def setCompoundDrawableTintList(self, tint):
        pass
    
    @args(void, [PorterDuff.Mode])
    def setCompoundDrawableTintMode(self, tintMode):
        pass
    
    @args(void, [Drawable, Drawable, Drawable, Drawable])
    def setCompoundDrawables(self, left, top, right, bottom):
        pass
    
    @args(void, [Drawable, Drawable, Drawable, Drawable])
    def setCompoundDrawablesRelative(self, start, top, end, bottom):
        pass
    
    @args(void, [Drawable, Drawable, Drawable, Drawable])
    def setCompoundDrawablesRelativeWithIntrinsicBounds(self, start, top, end, bottom):
        pass
    
    @args(void, [int, int, int, int])
    def setCompoundDrawablesRelativeWithIntrinsicBounds(self, start, top, end, bottom):
        pass
    
    @args(void, [Drawable, Drawable, Drawable, Drawable])
    def setCompoundDrawablesWithIntrinsicBounds(self, left, top, right, bottom):
        pass
    
    @args(void, [int, int, int, int])
    def setCompoundDrawablesWithIntrinsicBounds(self, left, top, right, bottom):
        pass
    
    @args(void, [bool])
    def setCursorVisible(self, visible):
        pass
    
    @args(void, [ActionMode.Callback])
    def setCustomInsertionActionModeCallback(self, actionModeCallback):
        pass
    
    @args(void, [ActionMode.Callback])
    def setCustomSelectionActionModeCallback(self, actionModeCallback):
        pass
    
    @final
    @args(void, [Editable.Factory])
    def setEditableFactory(self, factory):
        pass
    
    @args(void, [bool])
    def setElegantTextHeight(self, elegant):
        pass
    
    @args(void, [TextUtils.TruncateAt])
    def setEllipsize(self, where):
        pass
    
    @args(void, [int])
    def setEms(self, ems):
        pass
    
    @args(void, [bool])
    def setEnabled(self, enabled):
        pass
    
    @args(void, [CharSequence])
    def setError(self, error):
        pass
    
    @args(void, [CharSequence, Drawable])
    def setError(self, error, icon):
        pass
    
    @args(void, [ExtractedText])
    def setExtractedText(self, text):
        pass
    
    @args(void, [[InputFilter]])
    def setFilters(self, filters):
        pass
    
    @args(void, [String])
    def setFontFeatureSettings(self, fontFeatureSettings):
        pass
    
    @args(void, [bool])
    def setFreezesText(self, freezesText):
        pass
    
    @args(void, [int])
    def setGravity(self, gravity):
        pass
    
    @args(void, [int])
    def setHeight(self, pixels):
        pass
    
    @args(void, [int])
    def setHighlightColor(self, color):
        pass

    @final
    @args(void, [CharSequence])
    def setHint(self, hint):
        pass
    
    @final
    @args(void, [int])
    def setHint(self, resId):
        pass
    
    @final
    @args(void, [ColorStateList])
    def setHintTextColor(self, colors):
        pass
    
    @final
    @args(void, [int])
    def setHintTextColor(self, color):
        pass
    
    @args(void, [bool])
    def setHorizontallyScrolling(self, whether):
        pass
    
    @args(void, [int])
    def setHyphenationFrequency(self, hyphenationFrequency):
        pass
    
    @args(void, [CharSequence, int])
    def setImeActionLabel(self, label, actionId):
        pass
    
    @args(void, [int])
    def setImeOptions(self, imeOptions):
        pass
    
    @args(void, [bool])
    def setIncludeFontPadding(self, includepad):
        pass
    
    @args(void, [int])
    def setInputExtras(self, xmlResId):
        pass
    
    @args(void, [int])
    def setInputType(self, type):
        pass
    
    @args(void, [KeyListener])
    def setKeyListener(self, input):
        pass
    
    @args(void, [float])
    def setLetterSpacing(self, letterSpacing):
        pass
    
    @args(void, [float, float])
    def setLineSpacing(self, add, mult):
        pass
    
    @args(void, [int])
    def setLines(self, lines):
        pass
    
    @final
    @args(void, [ColorStateList])
    def setLinkTextColor(self, colors):
        pass
    
    @final
    @args(void, [int])
    def setLinkTextColor(self, color):
        pass
    
    @final
    @args(void, [bool])
    def setLinksClickable(self, whether):
        pass
    
    @args(void, [int])
    def setMarqueeRepeatLimit(self, marqueeLimit):
        pass
    
    @args(void, [int])
    def setMaxEms(self, maxems):
        pass
    
    @args(void, [int])
    def setMaxHeight(self, maxHeight):
        pass
    
    @args(void, [int])
    def setMaxLines(self, maxlines):
        pass
    
    @args(void, [int])
    def setMaxWidth(self, maxpixels):
        pass
    
    @args(void, [int])
    def setMinEms(self, minems):
        pass
    
    @args(void, [int])
    def setMinHeight(self, minHeight):
        pass
    
    @args(void, [int])
    def setMinLines(self, minlines):
        pass
    
    @args(void, [int])
    def setMinWidth(self, minpixels):
        pass
    
    @final
    @args(void, [MovementMethod])
    def setMovementMethod(self, movement):
        pass
    
    @args(void, [TextView.OnEditorActionListener])
    def setOnEditorActionListener(self, l):
        pass
    
    @args(void, [int, int, int, int])
    def setPadding(self, left, top, right, bottom):
        pass
    
    @args(void, [int, int, int, int])
    def setPaddingRelative(self, start, top, end, bottom):
        pass
    
    @args(void, [int])
    def setPaintFlags(self, flags):
        pass
    
    @args(void, [String])
    def setPrivateImeOptions(self, type):
        pass
    
    @args(void, [int])
    def setRawInputType(self, type):
        pass
    
    @args(void, [Scroller])
    def setScroller(self, s):
        pass
    
    @args(void, [bool])
    def setSelectAllOnFocus(self, selectAllOnFocus):
        pass
    
    @args(void, [bool])
    def setSelected(self, selected):
        pass
    
    @args(void, [float, float, float, int])
    def setShadowLayer(self, radius, dx, dy, color):
        pass
    
    @final
    @args(void, [bool])
    def setShowSoftInputOnFocus(self, show):
        pass
    
    def setSingleLine(self):
        pass
    
    @args(void, [bool])
    def setSingleLine(self, singleLine):
        pass
    
    @final
    @args(void, [Spannable.Factory])
    def setSpannableFactory(self, factory):
        pass
    
    @final
    @args(void, [int])
    def setText(self, resid):
        pass
    
    @final
    @args(void, [[char], int, int])
    def setText(self, text, start, len):
        pass
    
    @final
    @args(void, [int, TextView.BufferType])
    def setText(self, resid, type):
        pass
    
    @args(void, [CharSequence])
    def setText(self, text):
        pass
    
    @args(void, [CharSequence, TextView.BufferType])
    def setText(self, text, type):
        pass
    
    @api(23)
    @args(void, [int])
    def setTextAppearance(self, resId):
        pass
    
    @api(1, 23)
    @args(void, [Context, int])
    def setTextAppearance(self, context, resId):
        pass
    
    @args(void, [ColorStateList])
    def setTextColor(self, colors):
        pass
    
    @args(void, [int])
    def setTextColor(self, colour):
        pass
    
    @args(void, [float])
    def setTextSize(self, size):
        pass
    
    @args(void, [bool])
    def setTextIsSelectable(self, selectable):
        pass
    
    @final
    @args(void, [CharSequence])
    def setTextKeepState(self, text):
        pass
    
    @final
    @args(void, [CharSequence, TextView.BufferType])
    def setTextKeepState(self, text, type):
        pass
    
    @args(void, [Locale])
    def setTextLocale(self, locale):
        pass
    
    @args(void, [float])
    def setTextScaleX(self, size):
        pass
    
    @args(void, [float])
    def setTextSize(self, size):
        pass
    
    @args(void, [int, float])
    def setTextSize(self, unit, size):
        pass
    
    @final
    @args(void, [TransformationMethod])
    def setTransformationMethod(self, method):
        pass
    
    @args(void, [Typeface, int])
    def setTypeface(self, tf, style):
        pass
    
    @args(void, [Typeface])
    def setTypeface(self, tf):
        pass
    
    @args(void, [int])
    def setWidth(self, pixels):
        pass


class ThemedSpinnerAdapter:

    @args(Resources.Theme, [])
    def getDropDownViewTheme(self):
        pass
    
    @args(void, [Resources.Theme])
    def setDropDownViewTheme(self, theme):
        pass


class Toast(Object):

    LENGTH_LONG  = 1
    LENGTH_SHORT = 0
    
    @args(void, [Context])
    def __init__(self, context):
        pass
    
    def cancel(self):
        pass
    
    @args(int, [])
    def getDuration(self):
        pass
    
    @args(int, [])
    def getGravity(self):
        pass
    
    @args(float, [])
    def getHorizontalMargin(self):
        pass
    
    @args(float, [])
    def getVerticalMargin(self):
        pass
    
    @args(View, [])
    def getView(self):
        pass
    
    @args(int, [])
    def getXOffset(self):
        pass
    
    @args(int, [])
    def getYOffset(self):
        pass
    
    @static
    @args(Toast, [Context, int, int])
    def makeText(context, resId, duration):
        pass
    
    @static
    @args(Toast, [Context, CharSequence, int])
    def makeText(context, text, duration):
        pass
    
    @args(void, [int])
    def setDuration(self, duration):
        pass
    
    @args(void, [int, int, int])
    def setGravity(self, gravity, xOffset, yOffset):
        pass
    
    @args(void, [float, float])
    def setMargin(self, horizontalMargin, verticalMargin):
        pass
    
    @args(void, [int])
    def setText(self, resId):
        pass
    
    @args(void, [CharSequence])
    def setText(self, text):
        pass
    
    @args(void, [View])
    def setView(self, view):
        pass
    
    def show(self):
        pass


class ToggleButton(CompoundButton):

    @args(void, [Context])
    def __init__(self, context):
        pass
    
    @args(void, [Context, AttributeSet])
    def __init__(self, context, attrs):
        pass
    
    @args(void, [Context, AttributeSet, int])
    def __init__(self, context, attrs, defStyleAttr):
        pass
    
    @args(void, [Context, AttributeSet, int, int])
    def __init__(self, context, attrs, defStyleAttr, defStyleRes):
        pass
    
    @args(CharSequence, [])
    def getAccessibilityClassName(self):
        pass
    
    @args(CharSequence, [])
    def getTextOff(self):
        pass
    
    @args(CharSequence, [])
    def getTextOn(self):
        pass
    
    @args(void, [bool])
    def setChecked(self, checked):
        pass
    
    @args(void, [CharSequence])
    def setTextOff(self, textOff):
        pass
    
    @args(void, [CharSequence])
    def setTextOn(self, textOn):
        pass


class Toolbar(ViewGroup):

    class LayoutParams(ActionBar.LayoutParams):
    
        @args(void, [Context, AttributeSet])
        def __init__(self, context, attrs):
            pass
        
        @args(void, [int, int])
        def __init__(self, width, height):
            pass
        
        @args(void, [int, int, int])
        def __init__(self, width, height, gravity):
            pass
        
        @args(void, [int])
        def __init__(self, gravity):
            pass
        
        @args(void, [Toolbar.LayoutParams])
        def __init__(self, source):
            pass
        
        @args(void, [ActionBar.LayoutParams])
        def __init__(self, source):
            pass
        
        @args(void, [ViewGroup.MarginLayoutParams])
        def __init__(self, source):
            pass
        
        @args(void, [ViewGroup.LayoutParams])
        def __init__(self, source):
            pass
    
    class OnMenuItemClickListener:
    
        @args(void, [MenuItem])
        def onMenuItemClick(self, item):
            pass
    
    @args(void, [Context])
    def __init__(self, context):
        pass
    
    @args(void, [Context, AttributeSet])
    def __init__(self, context, attrs):
        pass
    
    @args(void, [Context, AttributeSet, int])
    def __init__(self, context, attrs, defStyleAttr):
        pass
    
    @args(void, [Context, AttributeSet, int, int])
    def __init__(self, context, attrs, defStyleAttr, defStyleRes):
        pass
    
    def collapseActionView(self):
        pass
    
    def dismissPopupMenus(self):
        pass
    
    @args(ActionMenuView.LayoutParams, [AttributeSet])
    def generateLayoutParams(self, attrs):
        pass
    
    @args(int, [])
    def getContentInsetEnd(self):
        pass
    
    @args(int, [])
    def getContentInsetLeft(self):
        pass
    
    @args(int, [])
    def getContentInsetRight(self):
        pass
    
    @args(int, [])
    def getContentInsetStart(self):
        pass
    
    @args(Drawable, [])
    def getLogo(self):
        pass
    
    @args(CharSequence, [])
    def getLogoDescription(self):
        pass
    
    @args(Menu, [])
    def getMenu(self):
        pass
    
    @args(CharSequence, [])
    def getNavigationContentDescription(self):
        pass
    
    @args(Drawable, [])
    def getNavigationIcon(self):
        pass
    
    @args(Drawable, [])
    def getOverflowIcon(self):
        pass
    
    @args(int, [])
    def getPopupTheme(self):
        pass
    
    @args(CharSequence, [])
    def getSubtitle(self):
        pass
    
    @args(CharSequence, [])
    def getTitle(self):
        pass
    
    @args(bool, [])
    def hasExpandedActionView(self):
        pass
    
    @args(bool, [])
    def hideOverflowMenu(self):
        pass
    
    @args(void, [int])
    def inflateMenu(self, resId):
        pass
    
    @args(bool, [])
    def isOverflowMenuShowing(self):
        pass
    
    @args(void, [int])
    def onRtlPropertiesChanged(self, layoutDirection):
        pass
    
    @args(void, [MotionEvent])
    def onTouchEvent(self, event):
        pass
    
    @args(void, [int, int])
    def setContentInsetsAbsolute(self, contentInsetLeft, contentInsetRight):
        pass
    
    @args(void, [int, int])
    def setContentInsetsRelative(self, contentInsetStart, contentInsetEnd):
        pass
    
    @args(void, [int])
    def setLogo(self, resId):
        pass
    
    @args(void, [Drawable])
    def setLogo(self, drawable):
        pass
    
    @args(void, [int])
    def setLogoDescription(self, resId):
        pass
    
    @args(void, [CharSequence])
    def setLogoDescription(self, description):
        pass
    
    @args(void, [int])
    def setNavigationContentDescription(self, resId):
        pass
    
    @args(void, [CharSequence])
    def setNavigationContentDescription(self, description):
        pass
    
    @args(void, [Drawable])
    def setNavigationIcon(self, drawable):
        pass
    
    @args(void, [int])
    def setNavigationIcon(self, resId):
        pass
    
    @args(void, [View.OnClickListener])
    def setNavigationOnClickListener(self, listener):
        pass
    
    @args(void, [Toolbar.OnMenuItemClickListener])
    def setOnMenuItemClickListener(self, listener):
        pass
    
    @args(void, [Drawable])
    def setOverflowIcon(self, drawable):
        pass
    
    @args(void, [int])
    def setPopupTheme(self, resId):
        pass
    
    @args(void, [int])
    def setSubtitle(self, resId):
        pass
    
    @args(void, [CharSequence])
    def setSubtitle(self, subtitle):
        pass
    
    @args(void, [Context, int])
    def setSubtitleTextAppearance(self, context, resId):
        pass
    
    @args(void, [int])
    def setSubtitleTextColor(self, color):
        pass
    
    @args(void, [int])
    def setTitle(self, resId):
        pass
    
    @args(void, [CharSequence])
    def setTitle(self, title):
        pass
    
    @args(void, [Context, int])
    def setTitleTextAppearance(self, context, resId):
        pass
    
    @args(void, [int])
    def setTitleTextColor(self, color):
        pass
    
    @args(bool, [])
    def setShowOverflowMenu(self):
        pass


class ViewAnimator(FrameLayout):

    @args(void, [Context])
    def __init__(self, context):
        pass
    
    @args(void, [Context, AttributeSet])
    def __init__(self, context, attrs):
        pass
    
    @args(void, [View, int, ViewGroup.LayoutParams])
    def addView(self, child, index, params):
        pass
    
    @args(CharSequence, [])
    def getAccessibilityClassName(self):
        pass
    
    @args(bool, [])
    def getAnimateFirstView(self):
        pass
    
    @args(int, [])
    def getBaseline(self):
        pass
    
    @args(View, [])
    def getCurrentView(self):
        pass
    
    @args(int, [])
    def getDisplayedChild(self):
        pass
    
    @args(Animation, [])
    def getInAnimation(self):
        pass
    
    @args(Animation, [])
    def getOutAnimation(self):
        pass
    
    def removeAllViews(self):
        pass
    
    @args(void, [View])
    def removeView(self, view):
        pass
    
    @args(void, [int])
    def removeViewAt(self, index):
        pass
    
    @args(void, [View])
    def removeViewInLayout(self, view):
        pass
    
    @args(void, [int, int])
    def removeViews(self, start, count):
        pass
    
    @args(void, [int, int])
    def removeViewsInLayout(self, start, count):
        pass
    
    @args(void, [bool])
    def setAnimateFirstView(self, animate):
        pass
    
    @args(void, [int])
    def setDisplayedChild(self, whichChild):
        pass
    
    @args(void, [Context, int])
    def setInAnimation(self, context, resourceID):
        pass
    
    @args(void, [Animation])
    def setInAnimation(self, inAnimation):
        pass
    
    @args(void, [Animation])
    def setOutAnimation(self, outAnimation):
        pass
    
    @args(void, [Context, int])
    def setOutAnimation(self, context, resourceID):
        pass
    
    def showNext(self):
        pass
    
    def showPrevious(self):
        pass


class ViewSwitcher(ViewAnimator):

    class ViewFactory:
    
        @args(View, [])
        def makeView(self):
            pass
    
    @args(void, [Context])
    def __init__(self, context):
        pass
    
    @args(void, [Context, AttributeSet])
    def __init__(self, context, attrs):
        pass
    
    @args(CharSequence, [])
    def getAccessibilityClassName(self):
        pass
    
    @args(View, [])
    def getNextView(self):
        pass
    
    def reset(self):
        pass
    
    @args(ViewSwitcher.ViewFactory, [])
    def setFactory(self, factory):
        pass


class Window(Object):

    @args(void, [Context])
    def __init__(self, context):
        pass
    
    @abstract
    @args(void, [View, ViewGroup.LayoutParams])
    def addContentView(self, view, params):
        pass
    
    @args(void, [int])
    def addFlags(self, flags):
        pass
    
    @args(void, [int])
    def clearFlags(self, flags):
        pass
