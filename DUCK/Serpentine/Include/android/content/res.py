# Class declarations obtained from Android API documentation licensed under the
# Apache 2.0 license (http://www.apache.org/licenses/LICENSE-2.0) and rewritten
# in syntax that the Python compiler module can parse.

__package__ = "android.content.res"

from java.io import InputStream
from java.lang import AutoCloseable, CharSequence, Object, String
from java.util import Locale
from android.graphics.drawable import Drawable
from android.os import Parcelable
from android.util import AttributeSet, DisplayMetrics, TypedValue
from org.xmlpull.v1 import XmlPullParser

class AssetManager(Object):

    __interfaces__ = [AutoCloseable]
    
    ACCESS_BUFFER = 3
    ACCESS_RANDOM = 1
    ACCESS_STREAMING = 2
    ACCESS_UNKNOWN = 0
    
    def close(self):
        pass
    
    @final
    @args([String], [])
    def getLocales(self):
        pass
    
    @final
    @args([String], [String])
    def list(self, path):
        pass
    
    @final
    @args(InputStream, [String])
    def open(self, fileName):
        pass
    
    @final
    @args(InputStream, [String, int])
    def open(self, fileName, accessMode):
        pass
    
    @final
    @args(AssetFileDescriptor, [String])
    def openFd(self, fileName):
        pass
    
    @final
    @args(AssetFileDescriptor, [String])
    def openNonAssetFd(self, fileName):
        pass
    
    @final
    @args(AssetFileDescriptor, [int, String])
    def openNonAssetFd(self, cookie, fileName):
        pass
    
    @final
    @args(XmlResourceParser, [String])
    def openXmlResourceParser(self, fileName):
        pass
    
    @final
    @args(XmlResourceParser, [int, String])
    def openXmlResourceParser(self, cookie, fileName):
        pass


class ColorStateList(Object):

    __interfaces__ = [Parcelable]
    
    __static_fields__ = {"CREATOR": Parcelable.Creator(ColorStateList)}
    
    @args(void, [[[int]], [int]])
    def __init__(self, states, colors):
        pass
    
    @static
    @args(ColorStateList, [Resources, XmlPullParser])
    def createFromXml(r, parser):
        pass
    
    @static
    @args(ColorStateList, [Resources, XmlPullParser, Resources.Theme])
    def createFromXml(r, parser, theme):
        pass
    
    @args(int, [])
    def describeContents(self):
        pass
    
    @args(int, [])
    def getChangingConfigurations(self):
        pass
    
    @args(int, [[int], int])
    def getColorForState(self, stateSet, defaultColor):
        pass
    
    @args(int, [])
    def getDefaultColor(self):
        pass
    
    @args(bool, [])
    def isOpaque(self):
        pass
    
    @args(bool, [])
    def isStateful(self):
        pass
    
    @args(String, [])
    def toString(self):
        pass
    
    @static
    @args(ColorStateList, [int])
    def valueOf(color):
        pass
    
    @args(ColorStateList, [int])
    def withAlpha(self, alpha):
        pass
    
    @args(void, [Parcel, int])
    def writeToParcel(self, dest, flags):
        pass


class Configuration(Object):

    __interfaces__ = [Parcelable]
    
    __static_fields__ = {"CREATOR": Parcelable.Creator(Configuration)}
    
    __fields__ = {
        "densityDpi": int,
        "fontScale": float,
        "hardKeyboardHidden": int,
        "keyboard": int,
        "keyboardHidden": int,
        "locale": Locale,
        "mcc": int,
        "mnc": int,
        "navigation": int,
        "navigationHidden": int,
        "orientation": int,
        "screenHeightDp": int,
        "screenLayout": int,
        "screenWidthDp": int,
        "smallestScreenWidthDp": int,
        "touchscreen": int,
        "uiMode": int
        }
    
    def __init__(self):
        pass


class Resources(Object):

    class NotFoundException(RuntimeException):
    
        def __init__(self):
            pass
        
        @args(void, [String])
        def __init__(self, name):
            pass
    
    class Theme(Object):
    
        @args(void, [int, bool])
        def applyStyle(self, resId, force):
            pass
        
        @args(void, [int, String, String])
        def dump(self, priority, tag, prefix):
            pass
        
        @api(23)
        @args(int, [])
        def getChangingConfigurations(self):
            pass
        
        @api(21)
        @args(Drawable, [int])
        def getDrawable(self, id):
            pass
        
        @api(21)
        @args(Resources, [])
        def getResources(self):
            pass
        
        @args(TypedArray, [[int]])
        def obtainStyledAttributes(self, attributes):
            pass
        
        @args(TypedArray, [int, [int]])
        def obtainStyledAttributes(self, resId, attributes):
            pass
        
        @args(TypedArray, [AttributeSet, [int], int, int])
        def obtainStyledAttributes(self, set, attributes, defStyleAttr, defStyleRes):
            pass
        
        @args(bool, [int, TypedValue, bool])
        def resolveAttribute(self, resId, outValue, resolveRefs):
            pass
        
        @args(void, [Resources.Theme])
        def setTo(self, other):
            pass
    
    def __init__(self):
        pass
    
    @final
    def finishPreloading(self):
        pass
    
    @final
    def flushLayoutCache(self):
        pass
    
    @args(XmlResourceParser, [int])
    def getAnimation(self, id):
        pass
    
    @final
    @args(AssetManager, [])
    def getAssets(self):
        pass
    
    @args(bool, [int])
    def getBoolean(self, id):
        pass
    
    @api(23)
    @args(int, [int, Resources.Theme])
    def getColor(self, id, theme):
        pass
    
    @api(1, 23)
    @args(int, [int])
    def getColor(self, id):
        pass
    
    @api(1, 23)
    @args(ColorStateList, [int])
    def getColorStateList(self, id):
        pass
    
    @api(23)
    @args(ColorStateList, [int, Resources.Theme])
    def getColorStateList(self, id, theme):
        pass
    
    @args(ColorStateList, [int, Resources.Theme])
    def getColor(self, id, theme):
        pass
    
    @args(Configuration, [])
    def getConfiguration(self):
        pass
    
    @args(float, [int])
    def getDimension(self, id):
        pass
    
    @args(int, [int])
    def getDimensionPixelOffset(self, id):
        pass
    
    @args(int, [int])
    def getDimensionPixelSize(self, id):
        pass
    
    @args(DisplayMetrics, [])
    def getDisplayMetrics(self):
        pass
    
    @api(1, 22)
    @args(Drawable, [int])
    def getDrawable(self, id):
        pass
    
    @api(21)
    @args(Drawable, [int, Resources.Theme])
    def getDrawable(self, id, theme):
        pass
    
    @api(15, 22)
    @args(Drawable, [int, int])
    def getDrawableForDensity(self, id, density):
        pass
    
    @api(21)
    @args(Drawable, [int, int, Resources.Theme])
    def getDrawableForDensity(self, id, density, theme):
        pass
    
    @api(3)
    @args(float, [int, int, int])
    def getFraction(self, id, base, pbase):
        pass
    
    @args(String, [String, String, String])
    def getIdentifier(self, name, defType, defPackage):
        pass
    
    @args([int], [int])
    def getIntArray(self, id):
        pass
    
    @args(int, [int])
    def getInteger(self, id):
        pass
    
    @args(XmlResourceParser, [int])
    def getLayout(self, id):
        pass
    
    @args(Movie, [int])
    def getMovie(self, id):
        pass
    
    @args(String, [int, int, [Object]])
    def getQuantityString(self, id, quantity, formatArgs):
        pass
    
    @args(String, [int, int])
    def getQuantityString(self, id, quantity):
        pass
    
    @args(CharSequence, [int, int])
    def getQuantityText(self, id, quantity):
        pass
    
    @args(String, [int])
    def getResourceEntryName(self, id):
        pass
    
    @args(String, [int])
    def getResourceName(self, id):
        pass
    
    @args(String, [int])
    def getResourcePackageName(self, id):
        pass
    
    @args(String, [int])
    def getResourceTypeName(self, id):
        pass
    
    @args(String, [int])
    def getString(self, id):
        pass
    
    @args(String, [int, [Object]])
    def getString(self, id, formatArgs):
        pass
    
    @args([String], [int])
    def getStringArray(self, id):
        pass
    
    @static
    @args(Resources, [])
    def getSystem():
        pass
    
    @args(CharSequence, [int, CharSequence])
    def getText(self, id, default):
        pass
    
    @args(CharSequence, [int])
    def getText(self, id):
        pass
    
    @args([CharSequence], [int])
    def getTextArray(self, id):
        pass
    
    @args(void, [String, TypedValue, bool])
    def getValue(self, name, outValue, resolveRefs):
        pass
    
    @args(void, [int, TypedValue, bool])
    def getValue(self, id, outValue, resolveRefs):
        pass
    
    @api(15)
    @args(void, [int, int, TypedValue, bool])
    def getValueForDensity(self, id, density, outValue, resolveRefs):
        pass
    
    @args(XmlResourceParser, [int])
    def getXml(self, id):
        pass
    
    @final
    @args(Resources.Theme, [])
    def newTheme(self):
        pass
    
    @args(TypedArray, [AttributeSet, [int]])
    def obtainAttributes(self, set, attrs):
        pass
    
    @args(TypedArray, [int])
    def obtainTypedArray(self, id):
        pass
    
    @args(InputStream, [int])
    def openRawResource(self, id):
        pass
    
    @api(3)
    @args(InputStream, [int, TypedValue])
    def openRawResource(self, id, value):
        pass
    
    @args(AssetFileDescriptor, [int])
    def openRawResourceFd(self, id):
        pass
    
    @api(3)
    @args(void, [String, AttributeSet, Bundle])
    def parseBundleExtra(self, tagName, attrs, outBundle):
        pass
    
    @api(3)
    @args(void, [XmlResourceParser, Bundle])
    def parseBundleExtras(self, parser, outBundle):
        pass
    
    @args(void, [Configuration, DisplayMetrics])
    def updateConfiguration(self, config, metrics):
        pass


class TypedArray(Object):

    @args(bool, [int, bool])
    def getBoolean(self, index, defValue):
        pass
    
    @args(int, [])
    def getChangingConfigurations(self):
        pass
    
    @args(int, [int, int])
    def getColor(self, index, defValue):
        pass
    
    @args(ColorStateList, [int])
    def getColorStateList(self, index):
        pass
    
    @args(float, [int, float])
    def getDimension(self, index, defValue):
        pass
    
    @args(int, [int, int])
    def getDimensionPixelOffset(self, index, defValue):
        pass
    
    @args(int, [int, int])
    def getDimensionPixelSize(self, index, defValue):
        pass
    
    @args(Drawable, [int])
    def getDrawable(self, index):
        pass
    
    @args(float, [int, float])
    def getFloat(self, index, defValue):
        pass
    
    @args(float, [int, int, int, float])
    def getFraction(self, index, base, pbase, defValue):
        pass
    
    @args(int, [int])
    def getIndex(self, at):
        pass
    
    @args(int, [])
    def getIndexCount(self):
        pass
    
    @args(int, [int, int])
    def getInt(self, index, defValue):
        pass
    
    @args(int, [int, int])
    def getInteger(self, index, defValue):
        pass
    
    @args(int, [int, String])
    def getLayoutDimension(self, index, name):
        pass
    
    @args(int, [int, int])
    def getLayoutDimension(self, index, defValue):
        pass
    
    @args(String, [int])
    def getNonResourceString(self, index):
        pass
    
    @args(String, [])
    def getPositionDescription(self):
        pass
    
    @args(int, [int, int])
    def getResourceId(self, index, defValue):
        pass
    
    @args(Resources, [])
    def getResources(self):
        pass
    
    @args(String, [int])
    def getString(self, index):
        pass
    
    @args(CharSequence, [int])
    def getText(self, index):
        pass
    
    @args([CharSequence], [int])
    def getTextArray(self, index):
        pass
    
    @args(int, [int])
    def getType(self, index):
        pass
    
    @args(bool, [int, TypedValue])
    def getValue(self, index, outValue):
        pass
    
    @args(bool, [int])
    def hasValue(self, index):
        pass
    
    @args(bool, [int])
    def hasValueOrEmpty(self, index):
        pass
    
    @args(int, [])
    def length(self):
        pass
    
    @args(TypedValue, [int])
    def peekValue(self, index):
        pass
    
    def recycle(self):
        pass


class XmlResourceParser:

    __interfaces__ = [XmlPullParser, AttributeSet, AutoCloseable]
    
    def close(self):
        pass
