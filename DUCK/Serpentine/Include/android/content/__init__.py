# Class declarations obtained from Android API documentation licensed under the
# Apache 2.0 license (http://www.apache.org/licenses/LICENSE-2.0) and rewritten
# in syntax that the Python compiler module can parse.

__package__ = "android.content"

from java.io import File, FileOutputStream, Serializable
from java.lang import Boolean, Byte, CharSequence, Class, Cloneable, \
                      Comparable, Double, Exception, Float, Integer, Long, \
                      Object, RuntimeException, Short, String
from java.util import ArrayList, Map, Set
from android.content.res import Configuration, Resources
from android.database import Cursor
from android.graphics import Rect
from android.net import Uri
from android.os import Bundle, Handler, Parcel, Parcelable, UserHandle
import android.text
from android.util import AndroidException
import android.view

class ActivityNotFoundException(RuntimeException):

    def __init__(self):
        pass
    
    @args(void, [String])
    def __init__(self, name):
        pass


class BroadcastReceiver(Object):

    class PendingResult(Object):
    
        @final
        def abortBroadcast(self):
            pass
        
        @final
        def clearAbortBroadcast(self):
            pass
        
        @final
        def finish(self):
            pass
        
        @final
        @args(bool, [])
        def getAbortBroadcast(self):
            pass
        
        @final
        @args(int, [])
        def getResultCode(self):
            pass
        
        @final
        @args(String, [])
        def getResultData(self):
            pass
        
        @final
        @args(Bundle, [bool])
        def getResultExtras(self, makeMap):
            pass
        
        @final
        @args(void, [int, String, Bundle])
        def setResult(self, code, data, extras):
            pass
        
        @final
        @args(void, [int])
        def setResultCode(self, code):
            pass
        
        @final
        @args(void, [String])
        def setResultData(self, data):
            pass
        
        @final
        @args(void, [Bundle])
        def setResultExtras(self, extras):
            pass
    
    def __init__(self):
        pass
    
    @final
    def abortBroadcast(self):
        pass
    
    @final
    def clearAbortBroadcast(self):
        pass
    
    @final
    @args(bool, [])
    def getAbortBroadcast(self):
        pass
    
    @final
    @args(bool, [])
    def getDebugUnregister(self):
        pass
    
    @final
    @args(int, [])
    def getResultCode(self):
        pass
    
    @final
    @args(String, [])
    def getResultData(self):
        pass
    
    @final
    @args(Bundle, [bool])
    def getResultExtras(self, makeMap):
        pass
    
    @final
    @args(BroadcastReceiver.PendingResult, [])
    def goAsync(self):
        pass
    
    @final
    @args(bool, [])
    def isInitialStickyBroadcast(self):
        pass
    
    @final
    @args(bool, [])
    def isOrderedBroadcast(self):
        pass
    
    @abstract
    @args(void, [Context, Intent])
    def onReceive(self, context, intent):
        pass
    
    # ...
    
    @final
    @args(void, [bool])
    def setDebugUnregister(self, debug):
        pass
    
    @final
    @args(void, [bool])
    def setOrderedHint(self, isOrdered):
        pass
    
    @final
    @args(void, [int, String, Bundle])
    def setResult(self, code, data, extras):
        pass
    
    @final
    @args(void, [int])
    def setResultCode(self, code):
        pass
    
    @final
    @args(void, [String])
    def setResultData(self, data):
        pass
    
    @final
    @args(void, [Bundle])
    def setResultExtras(self, extras):
        pass


class ClipboardManager(android.text.ClipboardManager):

    class OnPrimaryClipChangedListener:
    
        @args(void, [])
        def onPrimaryClipChanged(self):
            pass
    
    @args(void, [ClipboardManager.OnPrimaryClipChangedListener])
    def addPrimaryClipChangedListener(self, what):
        pass
    
    @args(ClipData, [])
    def getPrimaryClip(self):
        pass
    
    @args(ClipDescription, [])
    def getPrimaryClipDescription(self):
        pass
    
    @api(11,11)
    @args(CharSequence, [])
    def getText(self):
        pass
    
    @args(bool, [])
    def hasPrimaryClip(self):
        pass
    
    @api(11,11)
    @args(bool, [])
    def hasText(self):
        pass
    
    @args(void, [ClipboardManager.OnPrimaryClipChangedListener])
    def removePrimaryClipChangedListener(self, what):
        pass
    
    @args(void, [ClipData])
    def setPrimaryClip(self, clip):
        pass
    
    @args(void, [CharSequence])
    def setText(self, text):
        pass


class ClipData(Object):

    __interfaces__ = [Parcelable]
    
    __static_fields__ = {"CREATOR": Parcelable.Creator(ClipData)}
    
    class Item(Object):
    
        @args(void, [CharSequence])
        def __init__(self, text):
            pass
        
        @args(void, [CharSequence, String])
        def __init__(self, text, htmlText):
            pass
        
        @args(void, [Intent])
        def __init__(self, intent):
            pass
        
        @args(void, [Uri])
        def __init__(self, uri):
            pass
        
        @args(void, [CharSequence, Intent, Uri])
        def __init__(self, text, intent, uri):
            pass
        
        @args(void, [CharSequence, String, Uri])
        def __init__(self, text, htmlText, uri):
            pass
        
        @args(String, [Context])
        def coerceToHtmlText(self, context):
            pass
        
        @args(CharSequence, [Context])
        def coerceToStyledText(self, context):
            pass
        
        @args(CharSequence, [Context])
        def coerceToText(self, context):
            pass
        
        @args(String, [])
        def getHtmlText(self):
            pass
        
        @args(Intent, [])
        def getIntent(self):
            pass
        
        @args(CharSequence, [])
        def getText(self):
            pass
        
        @args(Uri, [])
        def getUri(self):
            pass
    
    @args(void, [CharSequence, [String], ClipData.Item])
    def __init__(self, label, mimeTypes, item):
        pass
    
    @args(void, [ClipDescription, ClipData.Item])
    def __init__(self, description, item):
        pass
    
    @args(void, [ClipData])
    def __init__(self, other):
        pass
    
    @args(void, [ClipData.Item])
    def addItem(self, item):
        pass
    
    @args(ClipDescription, [])
    def getDescription(self):
        pass
    
    @args(ClipData.Item, [int])
    def getItemAt(self, index):
        pass
    
    @args(int, [])
    def getItemCount(self):
        pass
    
    @static
    @args(ClipData, [CharSequence, CharSequence, String])
    def newHtmlText(label, text, htmlText):
        pass
    
    @static
    @args(ClipData, [CharSequence, Intent])
    def newIntent(label, intent):
        pass
    
    @static
    @args(ClipData, [CharSequence, CharSequence])
    def newPlainText(label, text):
        pass
    
    @static
    @args(ClipData, [CharSequence, Uri])
    def newRawUri(label, uri):
        pass
    
    @static
    @args(ClipData, [ContentResolver, CharSequence, Uri])
    def newUri(resolver, text, uri):
        pass


class ClipDescription(Object):

    __interfaces__ = [Parcelable]
    
    MIMETYPE_TEXT_HTML = "text/html"
    MIMETYPE_TEXT_INTENT = "text/vnd.android.intent"
    MIMETYPE_TEXT_PLAIN = "text/plain"
    MIMETYPE_TEXT_URILIST = "text/uri-list"
    
    __static_fields__ = {"CREATOR": Parcelable.Creator(ClipDescription)}
    
    @args(void, [CharSequence, [String]])
    def __init__(self, label, mimeTypes):
        pass
    
    @args(void, [ClipDescription])
    def __init__(self, other):
        pass
    
    @args(bool, [String, String])
    def compareMimeTypes(self, concreteType, desiredType):
        pass
    
    @args([String], [String])
    def filterMimeTypes(self, mimeType):
        pass
    
    @args(CharSequence, [])
    def getLabel(self):
        pass
    
    @args(String, [int])
    def getMimeType(self, index):
        pass
    
    @args(int, [])
    def getMimeTypeCount(self):
        pass
    
    @args(bool, [String])
    def hasMimeType(self, mimeType):
        pass


class ComponentCallbacks:

    @args(void, [Configuration])
    def onConfigurationChanged(self, config):
        pass
    
    def onLowMemory(self):
        pass


class ComponentCallbacks2:

    __interfaces__ = [ComponentCallbacks]
    
    @args(void, [int])
    def onTrimMemory(self, level):
        pass


class ComponentName(Object):

    __interfaces__ = [Parcelable, Cloneable, Comparable]
    
    __static_fields__ = {"CREATOR": Parcelable.Creator(ComponentName)}
    
    @args(void, [String, String])
    def __init__(self, package, class_):
        pass
    
    @args(void, [Context, String])
    def __init__(self, package, class_):
        pass
    
    @args(void, [Parcel])
    def __init__(self, parcel):
        pass
    
    @args(String, [])
    def flattenToShortString(self):
        pass
    
    @args(String, [])
    def flattenToString(self):
        pass
    
    @args(String, [])
    def getClassName(self):
        pass
    
    @args(String, [])
    def getPackageName(self):
        pass
    
    @args(String, [])
    def getShortClassName(self):
        pass
    
    @static
    @args(ComponentName, [Parcel])
    def readFromParcel(parcel):
        pass
    
    @args(String, [])
    def toShortString(self):
        pass
    
    @static
    @args(ComponentName, [String])
    def unflattenFromString(string):
        pass
    
    @static
    @args(void, [ComponentName, Parcel])
    def writeToParcel(componentName, parcel):
        pass


class ContentProvider(Object):

    __interfaces__ = [ComponentCallbacks2]
    
    def __init__(self):
        pass
    
    #@args([ContentProviderResult], [ArrayList(ContentProviderOperation)])
    #def applyBatch(self, operations):
    #    pass
    
    @args(void, [Context, ProviderInfo])
    def attachInfo(self, context, info):
        pass
    
    @args(int, [Uri, [ContentValues]])
    def bulkInsert(self, uri, values):
        pass
    
    @args(Bundle, [String, String, Bundle])
    def call(self, method, arg, extras):
        pass
    
    @args(Uri, [Uri])
    def canonicalize(self, uri):
        pass
    
    @abstract
    @args(int, [Uri, String, [String]])
    def delete(self, uri, selection, selectionArgs):
        pass
    
    @args(void, [FileDescriptor, PrintWriter, [String]])
    def dump(self, fd, writer, args):
        pass
    
    @final
    def getCallingPackage(self):
        pass
    
    @final
    @args(Context, [])
    def getContext(self):
        pass
    
    @final
    @args([PathPermission], [])
    def getPathPermissions(self):
        pass
    
    @final
    @args([String], [Uri, String])
    def getStreamTypes(self, uri, mimeTypeFilter):
        pass
    
    @abstract
    @args(String, [Uri])
    def getType(self, uri):
        pass
    
    @final
    @args(String, [])
    def getWritePermission(self):
        pass
    
    @abstract
    @args(Uri, [Uri, ContentValues])
    def insert(self, uri, values):
        pass
    
    @args(void, [Configuration])
    def onConfigurationChanged(self, newConfig):
        pass
    
    @abstract
    @args(bool, [])
    def onCreate(self):
        pass
    
    #@args(AssetFileDescriptor, [Uri, String, CancellationSignal])
    #def openAssetFile(self, uri, mode, signal):
    #    pass
    
    #@args(AssetFileDescriptor, [Uri, String])
    #def openAssetFile(self, uri, mode):
    #    pass
    
    #@args(ParcelFileDescriptor, [Uri, String])
    #def openFile(self, uri, mode):
    #    pass
    
    #@args(ParcelFileDescriptor, [Uri, String, CancellationSignal])
    #def openFile(self, uri, mode, signal):
    #    pass
    
    ### ...
    
    #@args(AssetFileDescriptor, [Uri, String, Bundle])
    #def openTypedAssetFile(self, uri, mimeTypeFilter, options):
    #    pass
    
    #@args(AssetFileDescriptor, [Uri, String, Bundle, CancellationSignal])
    #def openTypedAssetFile(self, uri, mimeTypeFilter, options, signal):
    #    pass
    
    @abstract
    @args(Cursor, [Uri, [String], String, [String], String])
    def query(self, uri, projection, selection, selectionArgs, sortOrder):
        pass
    
    @args(Cursor, [Uri, [String], String, [String], String, CancellationSignal])
    def query(self, uri, projection, selection, selectionArgs, sortOrder, signal):
        pass
    
    def shutdown(self):
        pass
    
    @args(Uri, [Uri])
    def uncanonicalize(self, uri):
        pass
    
    @abstract
    @args(int, [Uri, ContentValues, String, [String]])
    def update(self, uri, values, selection, selectionArgs):
        pass
    
    ### Protected methods ...


class ContentResolver(Object):

    CURSOR_DIR_BASE_TYPE = "vnd.android.cursor.dir"
    CURSOR_ITEM_BASE_TYPE = "vnd.android.cursor.item"
    SCHEME_ANDROID_RESOURCE = "android.resource"
    SCHEME_CONTENT = "content"
    SCHEME_FILE = "file"
    SYNC_EXTRAS_ACCOUNT = "account"
    SYNC_EXTRAS_DISCARD_LOCAL_DELETIONS = "discard_deletions"
    SYNC_EXTRAS_DO_NOT_RETRY = "do_not_retry"
    SYNC_EXTRAS_EXPEDITED = "expedited"
    SYNC_EXTRAS_FORCE = "force"
    SYNC_EXTRAS_IGNORE_BACKOFF = "ignore_backoff"
    SYNC_EXTRAS_IGNORE_SETTINGS = "ignore_settings"
    SYNC_EXTRAS_INITIALIZE = "initialize"
    SYNC_EXTRAS_MANUAL = "force"
    SYNC_EXTRAS_OVERRIDE_TOO_MANY_DELETIONS = "deletions_override"
    SYNC_EXTRAS_UPLOAD = "upload"
    SYNC_OBSERVER_TYPE_ACTIVE = 4
    SYNC_OBSERVER_TYPE_PENDING = 2
    SYNC_OBSERVER_TYPE_SETTINGS = 1
    
    @args(void, [Context])
    def __init__(self, context):
        pass
    
    @final
    @args(ContentProviderClient, [Uri])
    def acquireContentProviderClient(self, uri):
        pass

    @final
    @args(ContentProviderClient, [String])
    def acquireContentProviderClient(self, name):
        pass

    @final
    @args(ContentProviderClient, [Uri])
    def acquireUnstableContentProviderClient(self, uri):
        pass

    @final
    @args(ContentProviderClient, [String])
    def acquireUnstableContentProviderClient(self, name):
        pass

    @static
    @args(void, [Account, String, Bundle, long])
    def addPeriodicSync(account, authority, extras, pollFrequency):
        pass

    @static
    @args(Object, [int, SyncStatusObserver])
    def addStatusChangeListener(mask, callback):
        pass

    @args([ContentProviderResult], [String, ArrayList(ContentProviderOperation)])
    def applyBatch(self, authority, operations):
        pass

    @final
    @args(int, [Uri, [ContentValues]])
    def bulkInsert(self, url, values):
        pass

    @final
    @args(Bundle, [Uri, String, String, Bundle])
    def call(self, uri, method, arg, extras):
        pass

    @static
    @args(void, [SyncRequest])
    def cancelSync(request):
        pass

    @static
    @args(void, [Account, String])
    def cancelSync(account, authority):
        pass

    @args(void, [Uri])
    def cancelSync(self, uri):
        pass

    @final
    @args(Uri, [Uri])
    def canonicalize(self, url):
        pass

    @final
    @args(int, [Uri, String, [String]])
    def delete(self, url, where, selectionArgs):
        pass

    @static
    @args(SyncInfo, [])
    def getCurrentSync():
        pass

    @static
    @args(List(SyncInfo), [])
    def getCurrentSyncs():
        pass

    @static
    @args(int, [Account, String])
    def getIsSyncable(account, authority):
        pass

    @static
    @args(bool, [])
    def getMasterSyncAutomatically():
        pass

    @args(List(UriPermission), [])
    def getOutgoingPersistedUriPermissions(self):
        pass

    @static
    @args(List(PeriodicSync), [Account, String])
    def getPeriodicSyncs(account, authority):
        pass

    @args(List(UriPermission), [])
    def getPersistedUriPermissions(self):
        pass

    @args([String], [Uri, String])
    def getStreamTypes(self, url, mimeTypeFilter):
        pass

    @static
    @args([SyncAdapterType], [])
    def getSyncAdapterTypes():
        pass

    @static
    @args(bool, [Account, String])
    def getSyncAutomatically(account, authority):
        pass

    @final
    @args(String, [Uri])
    def getType(self, url):
        pass

    @final
    @args(Uri, [Uri, ContentValues])
    def insert(self, url, values):
        pass

    @static
    @args(bool, [Account, String])
    def isSyncActive(account, authority):
        pass

    @static
    @args(bool, [Account, String])
    def isSyncPending(account, authority):
        pass

    @args(void, [Uri, ContentObserver, bool])
    def notifyChange(self, uri, observer, syncToNetwork):
        pass

    @args(void, [Uri, ContentObserver])
    def notifyChange(self, uri, observer):
        pass

    @final
    @args(AssetFileDescriptor, [Uri, String])
    def openAssetFileDescriptor(self, uri, mode):
        pass

    @final
    @args(AssetFileDescriptor, [Uri, String, CancellationSignal])
    def openAssetFileDescriptor(self, uri, mode, cancellationSignal):
        pass

    @final
    @args(ParcelFileDescriptor, [Uri, String, CancellationSignal])
    def openFileDescriptor(self, uri, mode, cancellationSignal):
        pass

    @final
    @args(ParcelFileDescriptor, [Uri, String])
    def openFileDescriptor(self, uri, mode):
        pass

    @final
    @args(InputStream, [Uri])
    def openInputStream(self, uri):
        pass

    @final
    @args(OutputStream, [Uri])
    def openOutputStream(self, uri):
        pass

    @final
    @args(OutputStream, [Uri, String])
    def openOutputStream(self, uri, mode):
        pass

    @final
    @args(AssetFileDescriptor, [Uri, String, Bundle, CancellationSignal])
    def openTypedAssetFileDescriptor(self, uri, mimeType, opts, cancellationSignal):
        pass

    @final
    @args(AssetFileDescriptor, [Uri, String, Bundle])
    def openTypedAssetFileDescriptor(self, uri, mimeType, opts):
        pass

    @final
    @args(Cursor, [Uri, [String], String, [String], String])
    def query(self, uri, projection, selection, selectionArgs, sortOrder):
        pass

    @final
    @args(Cursor, [Uri, [String], String, [String], String, CancellationSignal])
    def query(self, uri, projection, selection, selectionArgs, sortOrder, cancellationSignal):
        pass

    @final
    @args(void, [Uri, bool, ContentObserver])
    def registerContentObserver(self, uri, notifyForDescendents, observer):
        pass

    @args(void, [Uri, int])
    def releasePersistableUriPermission(self, uri, modeFlags):
        pass

    @static
    @args(void, [Account, String, Bundle])
    def removePeriodicSync(account, authority, extras):
        pass

    @static
    @args(void, [Object])
    def removeStatusChangeListener(handle):
        pass

    @static
    @args(void, [Account, String, Bundle])
    def requestSync(account, authority, extras):
        pass

    @static
    @args(void, [SyncRequest])
    def requestSync(request):
        pass

    @static
    @args(void, [Account, String, int])
    def setIsSyncable(account, authority, syncable):
        pass

    @static
    @args(void, [bool])
    def setMasterSyncAutomatically(sync):
        pass

    @static
    @args(void, [Account, String, bool])
    def setSyncAutomatically(account, authority, sync):
        pass

    @args(void, [Uri, Bundle])
    def startSync(self, uri, extras):
        pass

    @args(void, [Uri, int])
    def takePersistableUriPermission(self, uri, modeFlags):
        pass

    @final
    @args(Uri, [Uri])
    def uncanonicalize(self, url):
        pass

    @final
    @args(void, [ContentObserver])
    def unregisterContentObserver(self, observer):
        pass

    @final
    @args(int, [Uri, ContentValues, String, [String]])
    def update(self, uri, values, where, selectionArgs):
        pass

    @static
    @args(void, [Bundle])
    def validateSyncExtrasBundle(extras):
        pass


class ContentValues(Object):

    __interfaces__ = [Parcelable]
    __static_fields__ = {"CREATOR": Parcelable.Creator(ContentValues)}
    
    TAG = "ContentValues"
    
    def __init__(self):
        pass
    
    @args(void, [int])
    def __init__(self, size):
        pass
    
    @args(void, [ContentValues])
    def __init__(self, other):
        pass
    
    def clear(self):
        pass
    
    @args(bool, [String])
    def containsKey(self, key):
        pass
    
    @args(Object, [String])
    def get(self, key):
        pass
    
    @args(Boolean, [String])
    def getAsBoolean(self, key):
        pass
    
    @args(Byte, [String])
    def getAsByte(self, key):
        pass
    
    @args([byte], [String])
    def getAsByteArray(self, key):
        pass
    
    @args(Double, [String])
    def getAsDouble(self, key):
        pass
    
    @args(Float, [String])
    def getAsFloat(self, key):
        pass
    
    @args(Integer, [String])
    def getAsInteger(self, key):
        pass
    
    @args(Long, [String])
    def getAsLong(self, key):
        pass
    
    @args(Short, [String])
    def getAsShort(self, key):
        pass
    
    @args(String, [String])
    def getAsString(self, key):
        pass
    
    @args(Set(String), [])
    def keySet(self):
        pass
    
    @args(void, [String, Byte])
    def put(self, key, value):
        pass
    
    @args(void, [String, Integer])
    def put(self, key, value):
        pass
    
    @args(void, [String, Float])
    def put(self, key, value):
        pass
    
    @args(void, [String, Short])
    def put(self, key, value):
        pass
    
    @args(void, [String, [byte]])
    def put(self, key, value):
        pass
    
    @args(void, [String, String])
    def put(self, key, value):
        pass
    
    @args(void, [String, Double])
    def put(self, key, value):
        pass
    
    @args(void, [String, Long])
    def put(self, key, value):
        pass
    
    @args(void, [String, Boolean])
    def put(self, key, value):
        pass
    
    @args(void, [ContentValues])
    def putAll(self, others):
        pass
    
    @args(void, [String])
    def putNull(self, key):
        pass
    
    @args(void, [String])
    def remove(self, key):
        pass
    
    @args(int, [])
    def size(self):
        pass
    
    @args(Set(Map.Entry(String, Object)), [])
    def valueSet(self):
        pass


class Context(Object):

    ACCESSIBILITY_SERVICE = "accessibility"
    ACCOUNT_SERVICE = "account"
    ACTIVITY_SERVICE = "activity"
    ALARM_SERVICE = "alarm"
    APP_OPS_SERVICE = "appops"
    AUDIO_SERVICE = "audio"
    BLUETOOTH_SERVICE = "bluetooth"
    CAPTIONING_SERVICE = "captioning"
    CLIPBOARD_SERVICE = "clipboard"
    CONNECTIVITY_SERVICE = "connectivity"
    CONSUMER_IR_SERVICE = "consumer_ir"
    DEVICE_POLICY_SERVICE = "device_policy"
    DISPLAY_SERVICE = "display"
    DOWNLOAD_SERVICE = "download"
    DROPBOX_SERVICE = "dropbox"
    INPUT_METHOD_SERVICE = "input_method"
    INPUT_SERVICE = "input"
    KEYGUARD_SERVICE = "keyguard"
    LAYOUT_INFLATER_SERVICE = "layout_inflater"
    LOCATION_SERVICE = "location"
    MEDIA_ROUTER_SERVICE = "media_router"
    MODE_APPEND                     = 0x8000
    MODE_ENABLE_WRITE_AHEAD_LOGGING = 0x0008
    MODE_MULTI_PROCESS              = 0x0004
    MODE_PRIVATE                    = 0x0000
    MODE_WORLD_READABLE             = 0x0001 # deprecated
    MODE_WORLD_WRITEABLE            = 0x0002 # deprecated
    NFC_SERVICE = "nfc"
    NOTIFICATION_SERVICE = "notification"
    NSD_SERVICE = "servicediscovery"
    POWER_SERVICE = "power"
    PRINT_SERVICE = "print"
    SEARCH_SERVICE = "search"
    SENSOR_SERVICE = "sensor"
    STORAGE_SERVICE = "storage"
    TELEPHONY_SERVICE = "phone"
    TEXT_SERVICES_MANAGER_SERVICE = "textservices"
    UI_MODE_SERVICE = "uimode"
    USB_SERVICE = "usb"
    USER_SERVICE = "user"
    VIBRATOR_SERVICE = "vibrator"
    WALLPAPER_SERVICE = "wallpaper"
    WIFI_P2P_SERVICE = "wifip2p"
    WIFI_SERVICE = "wifi"
    WINDOW_SERVICE = "window"
    
    def __init__(self):
        pass
    
    @abstract
    @args(bool, [Intent, ServiceConnection, int])
    def bindService(self, service, conn, flags):
        pass
    
    @abstract
    @args(int, [String])
    def checkCallingOrSelfPermission(self, permission):
        pass
    
    @abstract
    @args(int, [Uri, int])
    def checkCallingOrSelfUriPermission(self, uri, modeFlags):
        pass
    
    @abstract
    @args(int, [String])
    def checkCallingPermission(self, permission):
        pass
    
    @abstract
    @args(int, [Uri, int])
    def checkCallingUriPermission(self, uri, modeFlags):
        pass
    
    @abstract
    @args(int, [String, int, int])
    def checkPermission(self, permission, pid, uid):
        pass
    
    @abstract
    @args(int, [Uri, int, int, int])
    def checkUriPermission(self, uri, pid, uid, modeFlags):
        pass
    
    @abstract
    @args(int, [Uri, String, String, int, int, int])
    def checkUriPermission(self, uri, readPermission, writePermission, pid, uid, modeFlags):
        pass
    
    @abstract
    @args(Context, [Configuration])
    def createConfigurationContext(self, overrideConfiguration):
        pass
    
    @abstract
    @args(Context, [Display])
    def createDisplayContext(self, display):
        pass
    
    @abstract
    @args(Context, [String, int])
    def createPackageContext(self, packageName, flags):
        pass
    
    @abstract
    @args([String], [])
    def databaseList(self):
        pass
    
    @abstract
    @args(bool, [String])
    def deleteDatabase(self, name):
        pass
    
    @abstract
    @args(bool, [String])
    def deleteFile(self, name):
        pass
    
    @abstract
    @args(void, [String, String])
    def enforceCallingOrSelfPermission(self, permission, message):
        pass
    
    @abstract
    @args(void, [Uri, int, String])
    def enforceCallingOrSelfUriPermission(self, uri, modeFlags, message):
        pass
    
    @abstract
    @args(void, [String, String])
    def enforceCallingPermission(self, permission, message):
        pass
    
    @abstract
    @args(void, [Uri, int, String])
    def enforceCallingUriPermission(self, uri, modeFlags, message):
        pass
    
    @abstract
    @args(void, [String, int, int, String])
    def enforcePermission(self, permission, pid, uid, message):
        pass
    
    @abstract
    @args(void, [Uri, int, int, int, String])
    def enforceUriPermission(self, uri, pid, uid, modeFlags, message):
        pass
    
    @abstract
    @args(void, [Uri, String, String, int, int, int, String])
    def enforceUriPermission(self, uri, readPermission, writePermission, pid,
                             uid, modeFlags, message):
        pass
    
    @abstract
    @args([String], [])
    def fileList(self):
        pass
    
    @abstract
    @args(Context, [])
    def getApplicationContext(self):
        pass
    
    @abstract
    @args(ApplicationInfo, [])
    def getApplicationInfo(self):
        pass
    
    @abstract
    @args(AssetManager, [])
    def getAssets(self):
        pass
    
    @abstract
    @args(ClassLoader, [])
    def getClassLoader(self):
        pass
    
    @api(21)
    @abstract
    @args(File, [])
    def getCodeCacheDir(self):
        pass
    
    @api(23)
    @final
    @args(int, [int])
    def getColor(self, id):
        pass
    
    @api(23)
    @final
    @args(ColorStateList, [int])
    def getColorStateList(self, id):
        pass
    
    @abstract
    @args(ContentResolver, [])
    def getContentResolver(self):
        pass
    
    @abstract
    @args(File, [String])
    def getDatabasePath(self, name):
        pass
    
    @abstract
    @args(File, [String, int])
    def getDir(self, name, mode):
        pass
    
    @api(21)
    @final
    @args(Drawable, [int])
    def getDrawable(self, id):
        pass
    
    @abstract
    @args(File, [])
    def getCacheDir(self):
        pass
    
    @abstract
    @args(File, [String])
    def getDatabasePath(self, name):
        pass
    
    @abstract
    @args(File, [String, int])
    def getDir(self, name, mode):
        pass
    
    @api(8)
    @permissions(android.permission.WRITE_EXTERNAL_STORAGE)
    @abstract
    @args(File, [])
    def getExternalCacheDir(self):
        pass
    
    @api(19)
    @abstract
    @args([File], [])
    def getExternalCacheDirs(self):
        pass
    
    @api(8)
    @permissions(android.permission.WRITE_EXTERNAL_STORAGE)
    @abstract
    @args(File, [String])
    def getExternalFilesDir(self, type):
        pass
    
    @api(19)
    @abstract
    @args([File], [String])
    def getExternalFilesDirs(self, type):
        pass
    
    @api(21)
    @abstract
    @args([File], [])
    def getExternalMediaDirs(self):
        pass
    
    @abstract
    @args(File, [String])
    def getFileStreamPath(self, name):
        pass
    
    @abstract
    @args(File, [])
    def getFilesDir(self):
        pass
    
    @abstract
    @args(Looper, [])
    def getMainLooper(self):
        pass
    
    @api(21)
    @abstract
    @args(File, [])
    def getNoBackupFilesDir(self):
        pass
    
    @api(11)
    @permissions(android.permission.WRITE_EXTERNAL_STORAGE)
    @abstract
    @args(File, [])
    def getObbDir(self):
        pass
    
    @api(19)
    @abstract
    @args([File], [])
    def getObbDirs(self):
        pass
    
    @api(8)
    @abstract
    @args(String, [])
    def getPackageCodePath(self):
        pass
    
    @abstract
    @args(PackageManager, [])
    def getPackageManager(self):
        pass
    
    @abstract
    @args(String, [])
    def getPackageName(self):
        pass
    
    @api(8)
    @abstract
    @args(String, [])
    def getPackageResourcePath(self):
        pass
    
    @abstract
    @args(Resources, [])
    def getResources(self):
        pass
    
    @abstract
    @args(SharedPreferences, [])
    def getSharedPreferences(self):
        pass
    
    @final
    @args(String, [int])
    def getString(self, resId):
        pass
    
    @final
    @args(String, [int, [Object]])
    def getString(self, resId, formatArgs):
        pass
    
    @api(23)
    @final
    @args(Class, [Class])
    def getSystemService(self, serviceClass):
        pass
    
    @abstract
    @args(Object, [String])
    def getSystemService(self, name):
        pass
    
    @api(23)
    @abstract
    @args(String, [Class])
    def getSystemServiceName(self, serviceClass):
        pass
    
    @args(CharSequence, [int])
    def getText(self, resId):
        pass
    
    @args(Resources.Theme, [])
    def getTheme(self):
        pass
    
    @abstract
    @args(void, [String, Uri, int])
    def grantUriPermission(self, toPackage, uri, modeFlags):
        pass
    
    @api(4)
    @args(bool, [])
    def isRestricted(self):
        pass
    
    @final
    @args(TypedArray, [[int]])
    def obtainStyledAttributes(self, attrs):
        pass
    
    @final
    @args(TypedArray, [AttributeSet, [int], int, int])
    def obtainStyledAttributes(self, set, attrs, defStyleAttr, defStyleRes):
        pass
    
    @final
    @args(TypedArray, [AttributeSet, [int]])
    def obtainStyledAttributes(self, set, attrs):
        pass
    
    @final
    @args(TypedArray, [int, [int]])
    def obtainStyledAttributes(self, resid, attrs):
        pass
    
    @abstract
    @args(FileInputStream, [String])
    def openFileInput(self, name):
        pass
    
    @abstract
    @args(FileOutputStream, [String, int])
    def openFileOutput(self, name, mode):
        pass
    
    @abstract
    @args(SQLiteDatabase, [String, int, SQLiteDatabase.CursorFactory])
    def openOrCreateDatabase(self, name, mode, factory):
        pass
    
    @api(11)
    @abstract
    @args(SQLiteDatabase, [String, int, SQLiteDatabase.CursorFactory, DatabaseErrorHandler])
    def openOrCreateDatabase(self, name, mode, factory, errorHandler):
        pass
    
    @api(1, 5)
    @abstract
    @args(Drawable, [])
    def peekWallpaper(self):
        pass
    
    @api(14)
    @args(void, [ComponentCallbacks])
    def registerComponentCallbacks(self, callback):
        pass
    
    @args(Intent, [BroadcastReceiver, IntentFilter])
    def registerReceiver(self, receiver, intentFilter):
        pass
    
    @args(Intent, [BroadcastReceiver, IntentFilter, String, Handler])
    def registerReceiver(self, receiver, intentFilter, broadcastPermission,
                               scheduler):
        pass
    
    @abstract
    @args(void, [Uri, int])
    def revokeUriPermission(self, uri, modeFlags):
        pass
    
    @abstract
    @args(void, [Intent, String])
    def sendBroadcast(self, intent, receiverPermission):
        pass
    
    @abstract
    @args(void, [Intent])
    def sendBroadcast(self, intent):
        pass
    
    @api(17)
    @abstract
    @args(void, [Intent, UserHandle])
    def sendBroadcastAsUser(self, intent, user):
        pass
    
    @api(17)
    @abstract
    @args(void, [Intent, UserHandle, String])
    def sendBroadcastAsUser(self, intent, user, receiverPermission):
        pass
    
    @abstract
    @args(void, [Intent, String, BroadcastReceiver, Handler, int, String, Bundle])
    def sendOrderedBroadcast(self, intent, receiverPermission, resultReceiver, scheduler, initialCode, initialData, initialExtras):
        pass
    
    @abstract
    @args(void, [Intent, String])
    def sendOrderedBroadcast(self, intent, receiverPermission):
        pass
    
    @api(17)
    @abstract
    @args(void, [Intent, UserHandle, String, BroadcastReceiver, Handler, int, String, Bundle])
    def sendOrderedBroadcastAsUser(self, intent, user, receiverPermission, resultReceiver, scheduler, initialCode, initialData, initialExtras):
        pass
    
    @args(void, [int])
    def setTheme(self, resId):
        pass

    @abstract
    @args(void, [[Intent], Bundle])
    def startActivities(self, intents, options):
        pass
    
    @abstract
    @args(void, [[Intent]])
    def startActivities(self, intents):
        pass
    
    @abstract
    @args(void, [Intent])
    def startActivity(self, intent):
        pass
    
    @abstract
    @args(void, [Intent, Bundle])
    def startActivity(self, intent, options):
        pass
    
    @abstract
    @args(boolean, [ComponentName, String, Bundle])
    def startInstrumentation(self, className, profileFile, arguments):
        pass
    
    @abstract
    @args(void, [IntentSender, Intent, int, int, int, Bundle])
    def startIntentSender(self, intent, fillInIntent, flagsMask, flagsValues, extraFlags, options):
        pass
    
    @abstract
    @args(void, [IntentSender, Intent, int, int, int])
    def startIntentSender(self, intent, fillInIntent, flagsMask, flagsValues, extraFlags):
        pass
    
    @abstract
    @args(ComponentName, [Intent])
    def startService(self, service):
        pass
    
    @abstract
    @args(bool, [Intent])
    def stopService(self, service):
        pass
    
    @abstract
    @args(void, [ServiceConnection])
    def unbindService(self, conn):
        pass
    
    @args(void, [ComponentCallbacks])
    def unregisterComponentCallbacks(self, callback):
        pass
    
    @args(void, [BroadcastReceiver])
    def unregisterReceiver(self, receiver):
        pass


class ContextWrapper(Context):

    @args(void, [Context])
    def __init__(self, base):
        pass
    
    @args(boolean, [Intent, ServiceConnection, int])
    def bindService(self, service, conn, flags):
        pass
    
    @args(int, [String])
    def checkCallingOrSelfPermission(self, permission):
        pass
    
    @args(int, [Uri, int])
    def checkCallingOrSelfUriPermission(self, uri, modeFlags):
        pass
    
    @args(int, [String])
    def checkCallingPermission(self, permission):
        pass
    
    @args(int, [Uri, int])
    def checkCallingUriPermission(self, uri, modeFlags):
        pass
    
    @args(int, [String, int, int])
    def checkPermission(self, permission, pid, uid):
        pass
    
    @args(int, [String])
    def checkSelfPermission(self, permission):
        pass
    
    @args(int, [Uri, int, int, int])
    def checkUriPermission(self, uri, pid, uid, modeFlags):
        pass
    
    @args(int, [Uri, String, String, int, int, int])
    def checkUriPermission(self, uri, readPermission, writePermission, pid, uid, modeFlags):
        pass
    
    @args(Context, [Configuration])
    def createConfigurationContext(self, overrideConfiguration):
        pass
    
    @args(Context, [Display])
    def createDisplayContext(self, display):
        pass
    
    @args(Context, [String, int])
    def createPackageContext(self, packageName, flags):
        pass
    
    @args([String], [])
    def databaseList(self):
        pass
    
    @args(bool, [String])
    def deleteDatabase(self, name):
        pass
    
    @args(bool, [String])
    def deleteFile(self, name):
        pass
    
    @args(void, [String, String])
    def enforceCallingOrSelfPermission(self, permission, message):
        pass
    
    @args(void, [Uri, int, String])
    def enforceCallingOrSelfUriPermission(self, uri, modeFlags, message):
        pass
    
    @args(void, [String, String])
    def enforceCallingPermission(self, permission, message):
        pass
    
    @args(void, [Uri, int, String])
    def enforceCallingUriPermission(self, uri, modeFlags, message):
        pass
    
    @args(void, [String, int, int, String])
    def enforcePermission(self, permission, pid, uid, message):
        pass
    
    @args(void, [Uri, int, int, int, String])
    def enforceUriPermission(self, uri, pid, uid, modeFlags, message):
        pass
    
    @args(void, [Uri, String, String, int, int, int, String])
    def enforceUriPermission(self, uri, readPermission, writePermission, pid,
                             uid, modeFlags, message):
        pass
    
    @args([String], [])
    def fileList(self):
        pass
    
    @args(Context, [])
    def getApplicationContext(self):
        pass
    
    @args(ApplicationInfo, [])
    def getApplicationInfo(self):
        pass
    
    @args(AssetManager, [])
    def getAssets(self):
        pass
    
    @args(Context, [])
    def getBaseContext(self):
        pass
    
    @args(File, [])
    def getCacheDir(self):
        pass
    
    @args(ClassLoader, [])
    def getClassLoader(self):
        pass
    
    @args(File, [])
    def getCodeCacheDir(self):
        pass
    
    @args(ContentResolver, [])
    def getContentResolver(self):
        pass
    
    @args(File, [String])
    def getDatabasePath(self, name):
        pass
    
    @args(File, [String, int])
    def getDir(self, name, mode):
        pass
    
    @permissions(android.permission.WRITE_EXTERNAL_STORAGE)
    @args(File, [])
    def getExternalCacheDir(self):
        pass
    
    @args([File], [])
    def getExternalCacheDirs(self):
        pass
    
    @permissions(android.permission.WRITE_EXTERNAL_STORAGE)
    @args(File, [String])
    def getExternalFilesDir(self, type):
        pass
    
    @args([File], [String])
    def getExternalFilesDirs(self, type):
        pass
    
    @args([File], [])
    def getExternalMediaDirs(self):
        pass
    
    @args(File, [String])
    def getFileStreamPath(self, name):
        pass
    
    @args(File, [])
    def getFilesDir(self):
        pass
    
    @args(Looper, [])
    def getMainLooper(self):
        pass
    
    @args(File, [])
    def getNoBackupFilesDir(self):
        pass
    
    @permissions(android.permission.WRITE_EXTERNAL_STORAGE)
    @args(File, [])
    def getObbDir(self):
        pass
    
    @args([File], [])
    def getObbDirs(self):
        pass
    
    @args(String, [])
    def getPackageCodePath(self):
        pass
    
    @args(PackageManager, [])
    def getPackageManager(self):
        pass
    
    @args(String, [])
    def getPackageName(self):
        pass
    
    @args(String, [])
    def getPackageResourcePath(self):
        pass
    
    @args(Resources, [])
    def getResources(self):
        pass
    
    @args(SharedPreferences, [String, int])
    def getSharedPreferences(self, name, mode):
        pass
    
    @args(Object, [String])
    def getSystemService(self, name):
        pass
    
    @args(String, [Class])
    def getSystemServiceName(self, name):
        pass
    
    @args(Resources.Theme, [])
    def getTheme(self):
        pass
    
    @args(void, [String, Uri, int])
    def grantUriPermission(self, toPackage, uri, modeFlags):
        pass
    
    @args(bool, [])
    def isRestricted(self):
        pass
    
    @args(FileInputStream, [String])
    def openFileInput(self, name):
        pass
    
    @args(FileOutputStream, [String, int])
    def openFileOutput(self, name, mode):
        pass
    
    @args(SQLiteDatabase, [String, int, SQLiteDatabase.CursorFactory])
    def openOrCreateDatabase(self, name, mode, factory):
        pass
    
    @args(SQLiteDatabase, [String, int, SQLiteDatabase.CursorFactory, DatabaseErrorHandler])
    def openOrCreateDatabase(self, name, mode, factory, errorHandler):
        pass
    
    @api(1, 5)
    @args(Drawable, [])
    def peekWallpaper(self):
        pass
    
    @args(Intent, [BroadcastReceiver, IntentFilter])
    def registerReceiver(self, receiver, intentFilter):
        pass
    
    @args(Intent, [BroadcastReceiver, IntentFilter, String, Handler])
    def registerReceiver(self, receiver, intentFilter, broadcastPermission,
                               scheduler):
        pass
    
    @args(void, [Uri, int])
    def revokeUriPermission(self, uri, modeFlags):
        pass
    
    @args(void, [Intent, String])
    def sendBroadcast(self, intent, receiverPermission):
        pass
    
    @args(void, [Intent])
    def sendBroadcast(self, intent):
        pass
    
    @args(void, [Intent, UserHandle])
    def sendBroadcastAsUser(self, intent, user):
        pass
    
    @args(void, [Intent, UserHandle, String])
    def sendBroadcastAsUser(self, intent, user, receiverPermission):
        pass
    
    @args(void, [Intent, String, BroadcastReceiver, Handler, int, String, Bundle])
    def sendOrderedBroadcast(self, intent, receiverPermission, resultReceiver, scheduler, initialCode, initialData, initialExtras):
        pass
    
    @args(void, [Intent, String])
    def sendOrderedBroadcast(self, intent, receiverPermission):
        pass
    
    @args(void, [Intent, UserHandle, String, BroadcastReceiver, Handler, int, String, Bundle])
    def sendOrderedBroadcastAsUser(self, intent, user, receiverPermission, resultReceiver, scheduler, initialCode, initialData, initialExtras):
        pass
    
    @args(void, [int])
    def setTheme(self, resId):
        pass
    
    @args(void, [[Intent], Bundle])
    def startActivities(self, intents, options):
        pass
    
    @args(void, [[Intent]])
    def startActivities(self, intents):
        pass
    
    @args(void, [Intent])
    def startActivity(self, intent):
        pass
    
    @args(void, [Intent, Bundle])
    def startActivity(self, intent, options):
        pass
    
    @args(boolean, [ComponentName, String, Bundle])
    def startInstrumentation(self, className, profileFile, arguments):
        pass
    
    @args(void, [IntentSender, Intent, int, int, int, Bundle])
    def startIntentSender(self, intent, fillInIntent, flagsMask, flagsValues, extraFlags, options):
        pass
    
    @args(void, [IntentSender, Intent, int, int, int])
    def startIntentSender(self, intent, fillInIntent, flagsMask, flagsValues, extraFlags):
        pass
    
    @args(ComponentName, [Intent])
    def startService(self, service):
        pass
    
    @args(bool, [Intent])
    def stopService(self, service):
        pass
    
    @args(void, [ServiceConnection])
    def unbindService(self, conn):
        pass
    
    @args(void, [BroadcastReceiver])
    def unregisterReceiver(self, receiver):
        pass


class DialogInterface:

    BUTTON_NEGATIVE = -2
    BUTTON_NEUTRAL  = -3
    BUTTON_POSITIVE = -1
    
    class OnCancelListener:
    
        @args(void, [DialogInterface])
        def onCancel(self, dialog):
            pass
    
    class OnClickListener:
    
        @args(void, [DialogInterface, int])
        def onClick(self, dialog, which):
            pass
    
    class OnDismissListener:
    
        @args(void, [DialogInterface])
        def onDismiss(self, dialog):
            pass
    
    class OnKeyListener:
    
        @args(bool, [DialogInterface, int, android.view.KeyEvent])
        def onKey(self, dialog, keyCode, event):
            pass
    
    class OnMultiChoiceClickListener:
    
        @args(void, [DialogInterface, int, bool])
        def onClick(self, dialog, which, isChecked):
            pass
    
    class OnShowListener:
    
        @args(void, [DialogInterface])
        def onShow(self, dialog):
            pass
    
    def cancel(self):
        pass
    
    def dismiss(self):
        pass


class Intent(Object):

    __interfaces__ = [Parcelable, Cloneable]
    
    ACTION_AIRPLANE_MODE_CHANGED = "android.intent.action.AIRPLANE_MODE"
    ACTION_ALL_APPS = "android.intent.action.ALL_APPS"
    ACTION_ANSWER = "android.intent.action.ANSWER"
    ACTION_APP_ERROR = "android.intent.action.APP_ERROR"
    ACTION_ASSIST = "android.intent.action.ASSIST"
    ACTION_ATTACH_DATA = "android.intent.action.ATTACH_DATA"
    ACTION_BATTERY_CHARGED = "android.intent.action.BATTERY_CHARGED"
    ACTION_BATTERY_LOW = "android.intent.action.BATTERY_LOW"
    ACTION_BATTERY_OKAY = "android.intent.action.BATTERY_OKAY"
    ACTION_BOOT_COMPLETED = "android.intent.action.BOOT_COMPLETED"
    ACTION_BUG_REPORT = "android.intent.action.BUG_REPORT"
    ACTION_CALL = "android.intent.action.CALL"
    ACTION_CALL_BUTTON = "android.intent.action.CALL_BUTTON"
    ACTION_CAMERA_BUTTON = "android.intent.action.CAMERA_BUTTON"
    ACTION_CHOOSER = "android.intent.action.CHOOSER"
    ACTION_CLOSE_SYSTEM_DIALOGS = "android.intent.action.CLOSE_SYSTEM_DIALOGS"
    ACTION_CONFIGURATION_CHANGED = "android.intent.action.CONFIGURATION_CHANGED"
    ACTION_CREATE_DOCUMENT = "android.intent.action.CREATE_DOCUMENT"
    ACTION_CREATE_SHORTCUT = "android.intent.action.CREATE_SHORTCUT"
    ACTION_DATE_CHANGED = "android.intent.action.DATE_CHANGED"
    ACTION_DEFAULT = "android.intent.action.VIEW"
    ACTION_DELETE = "android.intent.action.DELETE"
    ACTION_DEVICE_STORAGE_LOW = "android.intent.action.DEVICE_STORAGE_LOW"
    ACTION_DEVICE_STORAGE_OK = "android.intent.action.DEVICE_STORAGE_OK"
    ACTION_DIAL = "android.intent.action.DIAL"
    ACTION_DOCK_EVENT = "android.intent.action.DOCK_EVENT"
    ACTION_DREAMING_STARTED = "android.intent.action.DREAMING_STARTED"
    ACTION_DREAMING_STOPPED = "android.intent.action.DREAMING_STOPPED"
    ACTION_EDIT = "android.intent.action.EDIT"
    ACTION_EXTERNAL_APPLICATIONS_AVAILABLE = "android.intent.action.EXTERNAL_APPLICATIONS_AVAILABLE"
    ACTION_EXTERNAL_APPLICATIONS_UNAVAILABLE = "android.intent.action.EXTERNAL_APPLICATIONS_UNAVAILABLE"
    ACTION_FACTORY_TEST = "android.intent.action.FACTORY_TEST"
    ACTION_GET_CONTENT = "android.intent.action.GET_CONTENT"
    ACTION_GET_RESTRICTION_ENTRIES = "android.intent.action.GET_RESTRICTION_ENTRIES"
    ACTION_GTALK_SERVICE_CONNECTED = "android.intent.action.GTALK_CONNECTED"
    ACTION_GTALK_SERVICE_DISCONNECTED = "android.intent.action.GTALK_DISCONNECTED"
    ACTION_HEADSET_PLUG = "android.intent.action.HEADSET_PLUG"
    ACTION_INPUT_METHOD_CHANGED = "android.intent.action.INPUT_METHOD_CHANGED"
    ACTION_INSERT = "android.intent.action.INSERT"
    ACTION_INSERT_OR_EDIT = "android.intent.action.INSERT_OR_EDIT"
    ACTION_INSTALL_PACKAGE = "android.intent.action.INSTALL_PACKAGE"
    ACTION_LOCALE_CHANGED = "android.intent.action.LOCALE_CHANGED"
    ACTION_MAIN = "android.intent.action.MAIN"
    ACTION_MANAGE_NETWORK_USAGE = "android.intent.action.MANAGE_NETWORK_USAGE"
    ACTION_MANAGE_PACKAGE_STORAGE = "android.intent.action.MANAGE_PACKAGE_STORAGE"
    ACTION_MEDIA_BAD_REMOVAL = "android.intent.action.MEDIA_BAD_REMOVAL"
    ACTION_MEDIA_BUTTON = "android.intent.action.MEDIA_BUTTON"
    ACTION_MEDIA_CHECKING = "android.intent.action.MEDIA_CHECKING"
    ACTION_MEDIA_EJECT = "android.intent.action.MEDIA_EJECT"
    ACTION_MEDIA_MOUNTED = "android.intent.action.MEDIA_MOUNTED"
    ACTION_MEDIA_NOFS = "android.intent.action.MEDIA_NOFS"
    ACTION_MEDIA_REMOVED = "android.intent.action.MEDIA_REMOVED"
    ACTION_MEDIA_SCANNER_FINISHED = "android.intent.action.MEDIA_SCANNER_FINISHED"
    ACTION_MEDIA_SCANNER_SCAN_FILE = "android.intent.action.MEDIA_SCANNER_SCAN_FILE"
    ACTION_MEDIA_SCANNER_STARTED = "android.intent.action.MEDIA_SCANNER_STARTED"
    ACTION_MEDIA_SHARED = "android.intent.action.MEDIA_SHARED"
    ACTION_MEDIA_UNMOUNTABLE = "android.intent.action.MEDIA_UNMOUNTABLE"
    ACTION_MEDIA_UNMOUNTED = "android.intent.action.MEDIA_UNMOUNTED"
    ACTION_MY_PACKAGE_REPLACED = "android.intent.action.MY_PACKAGE_REPLACED"
    ACTION_NEW_OUTGOING_CALL = "android.intent.action.NEW_OUTGOING_CALL"
    ACTION_OPEN_DOCUMENT = "android.intent.action.OPEN_DOCUMENT"
    ACTION_PACKAGE_ADDED = "android.intent.action.PACKAGE_ADDED"
    ACTION_PACKAGE_CHANGED = "android.intent.action.PACKAGE_CHANGED"
    ACTION_PACKAGE_DATA_CLEARED = "android.intent.action.PACKAGE_DATA_CLEARED"
    ACTION_PACKAGE_FIRST_LAUNCH = "android.intent.action.PACKAGE_FIRST_LAUNCH"
    ACTION_PACKAGE_FULLY_REMOVED = "android.intent.action.PACKAGE_FULLY_REMOVED"
    ACTION_PACKAGE_INSTALL = "android.intent.action.PACKAGE_INSTALL"
    ACTION_PACKAGE_NEEDS_VERIFICATION = "android.intent.action.PACKAGE_NEEDS_VERIFICATION"
    ACTION_PACKAGE_REMOVED = "android.intent.action.PACKAGE_REMOVED"
    ACTION_PACKAGE_REPLACED = "android.intent.action.PACKAGE_REPLACED"
    ACTION_PACKAGE_RESTARTED = "android.intent.action.PACKAGE_RESTARTED"
    ACTION_PACKAGE_VERIFIED = "android.intent.action.PACKAGE_VERIFIED"
    ACTION_PASTE = "android.intent.action.PASTE"
    ACTION_PICK = "android.intent.action.PICK"
    ACTION_PICK_ACTIVITY = "android.intent.action.PICK_ACTIVITY"
    ACTION_POWER_CONNECTED = "android.intent.action.POWER_CONNECTED"
    ACTION_POWER_DISCONNECTED = "android.intent.action.POWER_DISCONNECTED"
    ACTION_POWER_USAGE_SUMMARY = "android.intent.action.POWER_USAGE_SUMMARY"
    ACTION_PROVIDER_CHANGED = "android.intent.action.PROVIDER_CHANGED"
    ACTION_QUICK_CLOCK = "android.intent.action.QUICK_CLOCK"
    ACTION_REBOOT = "android.intent.action.REBOOT"
    ACTION_RUN = "android.intent.action.RUN"
    ACTION_SCREEN_OFF = "android.intent.action.SCREEN_OFF"
    ACTION_SCREEN_ON = "android.intent.action.SCREEN_ON"
    ACTION_SEARCH = "android.intent.action.SEARCH"
    ACTION_SEARCH_LONG_PRESS = "android.intent.action.SEARCH_LONG_PRESS"
    ACTION_SEND = "android.intent.action.SEND"
    ACTION_SENDTO = "android.intent.action.SENDTO"
    ACTION_SEND_MULTIPLE = "android.intent.action.SEND_MULTIPLE"
    ACTION_SET_WALLPAPER = "android.intent.action.SET_WALLPAPER"
    ACTION_SHUTDOWN = "android.intent.action.ACTION_SHUTDOWN"
    ACTION_SYNC = "android.intent.action.SYNC"
    ACTION_SYSTEM_TUTORIAL = "android.intent.action.SYSTEM_TUTORIAL"
    ACTION_TIMEZONE_CHANGED = "android.intent.action.TIMEZONE_CHANGED"
    ACTION_TIME_CHANGED = "android.intent.action.TIME_SET"
    ACTION_TIME_TICK = "android.intent.action.TIME_TICK"
    ACTION_UID_REMOVED = "android.intent.action.UID_REMOVED"
    ACTION_UMS_CONNECTED = "android.intent.action.UMS_CONNECTED"
    ACTION_UMS_DISCONNECTED = "android.intent.action.UMS_DISCONNECTED"
    ACTION_UNINSTALL_PACKAGE = "android.intent.action.UNINSTALL_PACKAGE"
    ACTION_USER_BACKGROUND = "android.intent.action.USER_BACKGROUND"
    ACTION_USER_FOREGROUND = "android.intent.action.USER_FOREGROUND"
    ACTION_USER_INITIALIZE = "android.intent.action.USER_INITIALIZE"
    ACTION_USER_PRESENT = "android.intent.action.USER_PRESENT"
    ACTION_VIEW = "android.intent.action.VIEW"
    ACTION_VOICE_COMMAND = "android.intent.action.VOICE_COMMAND"
    ACTION_WALLPAPER_CHANGED = "android.intent.action.WALLPAPER_CHANGED"
    ACTION_WEB_SEARCH = "android.intent.action.WEB_SEARCH"
    CATEGORY_ALTERNATIVE = "android.intent.category.ALTERNATIVE"
    CATEGORY_APP_BROWSER = "android.intent.category.APP_BROWSER"
    CATEGORY_APP_CALCULATOR = "android.intent.category.APP_CALCULATOR"
    CATEGORY_APP_CALENDAR = "android.intent.category.APP_CALENDAR"
    CATEGORY_APP_CONTACTS = "android.intent.category.APP_CONTACTS"
    CATEGORY_APP_EMAIL = "android.intent.category.APP_EMAIL"
    CATEGORY_APP_GALLERY = "android.intent.category.APP_GALLERY"
    CATEGORY_APP_MAPS = "android.intent.category.APP_MAPS"
    CATEGORY_APP_MARKET = "android.intent.category.APP_MARKET"
    CATEGORY_APP_MESSAGING = "android.intent.category.APP_MESSAGING"
    CATEGORY_APP_MUSIC = "android.intent.category.APP_MUSIC"
    CATEGORY_BROWSABLE = "android.intent.category.BROWSABLE"
    CATEGORY_CAR_DOCK = "android.intent.category.CAR_DOCK"
    CATEGORY_CAR_MODE = "android.intent.category.CAR_MODE"
    CATEGORY_DEFAULT = "android.intent.category.DEFAULT"
    CATEGORY_DESK_DOCK = "android.intent.category.DESK_DOCK"
    CATEGORY_DEVELOPMENT_PREFERENCE = "android.intent.category.DEVELOPMENT_PREFERENCE"
    CATEGORY_EMBED = "android.intent.category.EMBED"
    CATEGORY_FRAMEWORK_INSTRUMENTATION_TEST = "android.intent.category.FRAMEWORK_INSTRUMENTATION_TEST"
    CATEGORY_HE_DESK_DOCK = "android.intent.category.HE_DESK_DOCK"
    CATEGORY_HOME = "android.intent.category.HOME"
    CATEGORY_INFO = "android.intent.category.INFO"
    CATEGORY_LAUNCHER = "android.intent.category.LAUNCHER"
    CATEGORY_LE_DESK_DOCK = "android.intent.category.LE_DESK_DOCK"
    CATEGORY_MONKEY = "android.intent.category.MONKEY"
    CATEGORY_OPENABLE = "android.intent.category.OPENABLE"
    CATEGORY_PREFERENCE = "android.intent.category.PREFERENCE"
    CATEGORY_SAMPLE_CODE = "android.intent.category.SAMPLE_CODE"
    CATEGORY_SELECTED_ALTERNATIVE = "android.intent.category.SELECTED_ALTERNATIVE"
    CATEGORY_TAB = "android.intent.category.TAB"
    CATEGORY_TEST = "android.intent.category.TEST"
    CATEGORY_UNIT_TEST = "android.intent.category.UNIT_TEST"
    EXTRA_ALARM_COUNT = "android.intent.extra.ALARM_COUNT"
    EXTRA_ALLOW_MULTIPLE = "android.intent.extra.ALLOW_MULTIPLE"
    EXTRA_ALLOW_REPLACE = "android.intent.extra.ALLOW_REPLACE"
    EXTRA_ASSIST_CONTEXT = "android.intent.extra.ASSIST_CONTEXT"
    EXTRA_ASSIST_PACKAGE = "android.intent.extra.ASSIST_PACKAGE"
    EXTRA_BCC = "android.intent.extra.BCC"
    EXTRA_BUG_REPORT = "android.intent.extra.BUG_REPORT"
    EXTRA_CC = "android.intent.extra.CC"
    EXTRA_CHANGED_COMPONENT_NAME = "android.intent.extra.changed_component_name"
    EXTRA_CHANGED_COMPONENT_NAME_LIST = "android.intent.extra.changed_component_name_list"
    EXTRA_CHANGED_PACKAGE_LIST = "android.intent.extra.changed_package_list"
    EXTRA_CHANGED_UID_LIST = "android.intent.extra.changed_uid_list"
    EXTRA_DATA_REMOVED = "android.intent.extra.DATA_REMOVED"
    EXTRA_DOCK_STATE = "android.intent.extra.DOCK_STATE"
    EXTRA_DOCK_STATE_CAR = 2
    EXTRA_DOCK_STATE_DESK = 1
    EXTRA_DOCK_STATE_HE_DESK = 4
    EXTRA_DOCK_STATE_LE_DESK = 3
    EXTRA_DOCK_STATE_UNDOCKED = 0
    EXTRA_DONT_KILL_APP = "android.intent.extra.DONT_KILL_APP"
    EXTRA_EMAIL = "android.intent.extra.EMAIL"
    EXTRA_HTML_TEXT = "android.intent.extra.HTML_TEXT"
    EXTRA_INITIAL_INTENTS = "android.intent.extra.INITIAL_INTENTS"
    EXTRA_INSTALLER_PACKAGE_NAME = "android.intent.extra.INSTALLER_PACKAGE_NAME"
    EXTRA_INTENT = "android.intent.extra.INTENT"
    EXTRA_KEY_EVENT = "android.intent.extra.KEY_EVENT"
    EXTRA_LOCAL_ONLY = "android.intent.extra.LOCAL_ONLY"
    EXTRA_MIME_TYPES = "android.intent.extra.MIME_TYPES"
    EXTRA_NOT_UNKNOWN_SOURCE = "android.intent.extra.NOT_UNKNOWN_SOURCE"
    EXTRA_ORIGINATING_URI = "android.intent.extra.ORIGINATING_URI"
    EXTRA_PHONE_NUMBER = "android.intent.extra.PHONE_NUMBER"
    EXTRA_REFERRER = "android.intent.extra.REFERRER"
    EXTRA_REMOTE_INTENT_TOKEN = "android.intent.extra.remote_intent_token"
    EXTRA_REPLACING = "android.intent.extra.REPLACING"
    EXTRA_RESTRICTIONS_BUNDLE = "android.intent.extra.restrictions_bundle"
    EXTRA_RESTRICTIONS_INTENT = "android.intent.extra.restrictions_intent"
    EXTRA_RESTRICTIONS_LIST = "android.intent.extra.restrictions_list"
    EXTRA_RETURN_RESULT = "android.intent.extra.RETURN_RESULT"
    EXTRA_SHORTCUT_ICON = "android.intent.extra.shortcut.ICON"
    EXTRA_SHORTCUT_ICON_RESOURCE = "android.intent.extra.shortcut.ICON_RESOURCE"
    EXTRA_SHORTCUT_INTENT = "android.intent.extra.shortcut.INTENT"
    EXTRA_SHORTCUT_NAME = "android.intent.extra.shortcut.NAME"
    EXTRA_SHUTDOWN_USERSPACE_ONLY = "android.intent.extra.SHUTDOWN_USERSPACE_ONLY"
    EXTRA_STREAM = "android.intent.extra.STREAM"
    EXTRA_SUBJECT = "android.intent.extra.SUBJECT"
    EXTRA_TEMPLATE = "android.intent.extra.TEMPLATE"
    EXTRA_TEXT = "android.intent.extra.TEXT"
    EXTRA_TITLE = "android.intent.extra.TITLE"
    EXTRA_UID = "android.intent.extra.UID"
    FILL_IN_ACTION = 1
    FILL_IN_CATEGORIES = 4
    FILL_IN_CLIP_DATA = 128
    FILL_IN_COMPONENT = 8
    FILL_IN_DATA = 2
    FILL_IN_PACKAGE = 16
    FILL_IN_SELECTOR = 64
    FILL_IN_SOURCE_BOUNDS = 32
    FLAG_ACTIVITY_BROUGHT_TO_FRONT          = 0x00400000
    FLAG_ACTIVITY_CLEAR_TASK                = 0x00008000
    FLAG_ACTIVITY_CLEAR_TOP                 = 0x04000000
    FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET     = 0x00080000
    FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS      = 0x00800000
    FLAG_ACTIVITY_FORWARD_RESULT            = 0x02000000
    FLAG_ACTIVITY_LAUNCHED_FROM_HISTORY     = 0x00100000
    FLAG_ACTIVITY_MULTIPLE_TASK             = 0x08000000
    FLAG_ACTIVITY_NEW_TASK                  = 0x10000000
    FLAG_ACTIVITY_NO_ANIMATION              = 0x00010000
    FLAG_ACTIVITY_NO_HISTORY                = 0x40000000
    FLAG_ACTIVITY_NO_USER_ACTION            = 0x00040000
    FLAG_ACTIVITY_PREVIOUS_IS_TOP           = 0x01000000
    FLAG_ACTIVITY_REORDER_TO_FRONT          = 0x00020000
    FLAG_ACTIVITY_RESET_TASK_IF_NEEDED      = 0x00200000
    FLAG_ACTIVITY_SINGLE_TOP                = 0x20000000
    FLAG_ACTIVITY_TASK_ON_HOME              = 0x00004000
    FLAG_DEBUG_LOG_RESOLUTION               = 0x00000008
    FLAG_EXCLUDE_STOPPED_PACKAGE            = 0x00000010
    FLAG_FROM_BACKGROUND                    = 0x00000004
    FLAG_GRANT_PERSISTABLE_URI_PERMISSION   = 0x00000040
    FLAG_GRANT_READ_URI_PERMISSION          = 0x00000001
    FLAG_GRANT_WRITE_URI_PERMISSION         = 0x00000002
    FLAG_INCLUDE_STOPPED_PACKAGES           = 0x00000020
    FLAG_RECEIVER_FOREGROUND                = 0x10000000
    FLAG_RECEIVER_NO_ABORT                  = 0x08000000
    FLAG_RECEIVER_REGISTERED_ONLY           = 0x40000000
    FLAG_RECEIVER_REPLACE_PENDING           = 0x20000000
    METADATA_DOCK_HOME = "android.dock_home"
    URI_INTENT_SCHEME = 1
    
    __static_fields__ = {"CREATOR": Parcelable.Creator(Intent)}
    
    class FilterComparison(Object):
    
        @args(void, [Intent])
        def __init__(self, intent):
            pass
    
    class ShortcutIconResource(Object):
    
        __static_fields__ = {
            "CREATOR": Parcelable.Creator(Intent.ShortcutIconResource),
            "packageName": String,
            "resourceName": String
            }
        
        def __init__(self):
            pass
        
        @args(Intent.ShortcutIconResource, [Context, int])
        def fromContext(self, context, resourceId):
            pass
    
    def __init__(self):
        pass
    
    @args(void, [Intent])
    def __init__(self, intent):
        pass
    
    @args(void, [String])
    def __init__(self, action):
        pass
    
    @args(void, [String, Uri])
    def __init__(self, action, uri):
        pass
    
    @args(void, [Context, Class])
    def __init__(self, packageContext, class_):
        pass
    
    @args(void, [String, Uri, Context, Class])
    def __init__(self, action, uri, packageContext, class_):
        pass
    
    @args(Intent, [String])
    def addCategory(self, category):
        pass
    
    @args(Intent, [int])
    def addFlags(self, flags):
        pass
    
    @args(Intent, [])
    def cloneFilter(self):
        pass
    
    @static
    @args(Intent, [Intent, CharSequence])
    def createChooser(target, title):
        pass
    
    #@static
    #@args(Intent, [Intent, CharSequence, IntentSender])
    #def createChooser(target, title, sender):
    #    pass
    
    @args(int, [])
    def describeContents(self):
        pass
    
    @args(int, [Intent, int])
    def fillIn(self, other, flags):
        pass
    
    @args(bool, [Intent])
    def filterEquals(self, other):
        pass
    
    @args(int, [])
    def filterHashCode(self):
        pass
    
    @args(String, [])
    def getAction(self):
        pass
    
    @args([bool], [String])
    def getBooleanArrayExtra(self, name):
        pass
    
    @args(bool, [String, bool])
    def getBooleanExtra(self, name, defaultValue):
        pass
    
    @args(Bundle, [String])
    def getBundleExtra(self, name):
        pass
    
    @args([byte], [String])
    def getByteArrayExtra(self, name):
        pass
    
    @args(byte, [String, byte])
    def getByteExtra(self, name, defaultValue):
        pass
    
    @args(Set(String), [])
    def getCategories(self):
        pass
    
    @args([char], [String])
    def getCharArrayExtra(self, name):
        pass
    
    @args(char, [String, char])
    def getCharExtra(self, name, defaultValue):
        pass
    
    @args([CharSequence], [String])
    def getCharSequenceArrayExtra(self, name):
        pass
    
    @args(ArrayList(CharSequence), [String])
    def getCharSequenceArrayListExtra(self, name):
        pass
    
    @args(CharSequence, [String, CharSequence])
    def getCharSequenceExtra(self, name, defaultValue):
        pass
    
    @args(ClipData, [])
    def getClipData(self):
        pass
    
    @args(ComponentName, [])
    def getComponent(self):
        pass
    
    @args(Uri, [])
    def getData(self):
        pass
    
    @args(String, [])
    def getDataString(self):
        pass
    
    @args([double], [String])
    def getDoubleArrayExtra(self, name):
        pass
    
    @args(double, [String, double])
    def getDoubleExtra(self, name, defaultValue):
        pass
    
    @args(Bundle, [])
    def getExtras(self):
        pass
    
    @args(int, [])
    def getFlags(self):
        pass
    
    @args([float], [String])
    def getFloatArrayExtra(self, name):
        pass
    
    @args(float, [String, float])
    def getFloatExtra(self, name, defaultValue):
        pass
    
    @args([int], [String])
    def getIntArrayExtra(self, name):
        pass
    
    @args(int, [String, int])
    def getIntExtra(self, name, defaultValue):
        pass
    
    @args(ArrayList(Integer), [String])
    def getIntegerArrayListExtra(self, name):
        pass
    
    @static
    @args(Intent, [String])
    def getIntent(uri):
        pass
    
    @static
    @args(Intent, [String])
    def getIntentOld(uri):
        pass
    
    @args([long], [String])
    def getLongArrayExtra(self, name):
        pass
    
    @args(long, [String, long])
    def getLongExtra(self, name, defaultValue):
        pass
    
    @args(String, [])
    def getPackage(self):
        pass
    
    @args([Parcelable], [String])
    def getParcelableArrayExtra(self, name):
        pass
    
    @args([ArrayList(Parcelable)], [String])
    def getParcelableArrayListExtra(self, name):
        pass
    
    @args(Parcelable, [String])
    def getParcelableExtra(self, name):
        pass
    
    @args(String, [])
    def getScheme(self):
        pass
    
    @args(Intent, [])
    def getSelector(self):
        pass
    
    @args(Serializable, [String])
    def getSerializableExtra(self, name):
        pass
    
    @args([short], [String])
    def getShortArrayExtra(self, name):
        pass
    
    @args(short, [String, short])
    def getShortExtra(self, name, defaultValue):
        pass
    
    @args(Rect, [])
    def getSourceBounds(self):
        pass
    
    @args([String], [String])
    def getStringArrayExtra(self, name):
        pass
    
    @args(ArrayList(String), [String])
    def getStringArrayListExtra(self, name):
        pass
    
    @args(String, [String])
    def getStringExtra(self, name):
        pass
    
    @args(String, [])
    def getType(self):
        pass
    
    @args(bool, [String])
    def hasCategory(self, category):
        pass
    
    @args(bool, [String])
    def hasExtra(self, name):
        pass
    
    @args(bool, [])
    def hasFileDescriptors(self):
        pass
    
    @static
    @args(Intent, [ComponentName])
    def makeMainActivity(mainActivity):
        pass
    
    @static
    @args(Intent, [String, String])
    def makeMainSelectorActivity(selectorAction, selectorCategory):
        pass
    
    @static
    @args(Intent, [ComponentName])
    def makeRestartActivityTask(mainActivity):
        pass
    
    @static
    @args(String, [String])
    def normalizeMimeType(type):
        pass
    
    @static
    @args(Intent, [Resources, XmlPullParser, AttributeSet])
    def parseIntent(resources, parser, attrs):
        pass
    
    @static
    @args(Intent, [String, int])
    def parseUri(uri, flags):
        pass
    
    @args(Intent, [String, ArrayList(CharSequence)])
    def putCharSequenceArrayListExtra(self, name, value):
        pass
    
    @args(Intent, [String, [double]])
    def putExtra(self, name, value):
        pass
    
    @args(Intent, [String, int])
    def putExtra(self, name, value):
        pass
    
    @args(Intent, [String, CharSequence])
    def putExtra(self, name, value):
        pass
    
    @args(Intent, [String, char])
    def putExtra(self, name, value):
        pass
    
    @args(Intent, [String, Bundle])
    def putExtra(self, name, value):
        pass
    
    @args(Intent, [String, [Parcelable]])
    def putExtra(self, name, value):
        pass
    
    @args(Intent, [String, Serializable])
    def putExtra(self, name, value):
        pass
    
    @args(Intent, [String, [int]])
    def putExtra(self, name, value):
        pass
    
    @args(Intent, [String, float])
    def putExtra(self, name, value):
        pass
    
    @args(Intent, [String, [byte]])
    def putExtra(self, name, value):
        pass
    
    @args(Intent, [String, [long]])
    def putExtra(self, name, value):
        pass
    
    @args(Intent, [String, Parcelable])
    def putExtra(self, name, value):
        pass
    
    @args(Intent, [String, [float]])
    def putExtra(self, name, value):
        pass
    
    @args(Intent, [String, long])
    def putExtra(self, name, value):
        pass
    
    @args(Intent, [String, [String]])
    def putExtra(self, name, value):
        pass
    
    @args(Intent, [String, bool])
    def putExtra(self, name, value):
        pass
    
    @args(Intent, [String, [bool]])
    def putExtra(self, name, value):
        pass
    
    @args(Intent, [String, short])
    def putExtra(self, name, value):
        pass
    
    @args(Intent, [String, double])
    def putExtra(self, name, value):
        pass
    
    @args(Intent, [String, [short]])
    def putExtra(self, name, value):
        pass
    
    @args(Intent, [String, String])
    def putExtra(self, name, value):
        pass
    
    @args(Intent, [String, byte])
    def putExtra(self, name, value):
        pass
    
    @args(Intent, [String, [char]])
    def putExtra(self, name, value):
        pass
    
    @args(Intent, [String, [CharSequence]])
    def putExtra(self, name, value):
        pass
    
    @args(Intent, [Intent])
    def putExtras(self, source):
        pass
    
    @args(Intent, [Bundle])
    def putExtras(self, extras):
        pass
    
    @args(Intent, [String, ArrayList(Integer)])
    def putIntegerArrayListExtra(self, name, value):
        pass
    
    @args(Intent, [String, ArrayList(Parcelable)])
    def putParcelableArrayListExtra(self, name, value):
        pass
    
    @args(Intent, [String, ArrayList(String)])
    def putStringArrayListExtra(self, name, value):
        pass
    
    @args(void, [String])
    def removeCategory(self, category):
        pass
    
    @args(void, [String])
    def removeExtra(self, name):
        pass
    
    @args(Intent, [Bundle])
    def replaceExtras(self, extras):
        pass
    
    @args(Intent, [Intent])
    def replaceExtras(self, source):
        pass
    
    @args(ComponentName, [PackageManager])
    def resolveActivity(self, packageManager):
        pass
    
    @args(ActivityInfo, [PackageManager, int])
    def resolveActivityInfo(self, packageManager, flags):
        pass
    
    @args(String, [ContentResolver])
    def resolveType(self, resolver):
        pass
    
    @args(String, [Context])
    def resolveType(self, context):
        pass
    
    @args(String, [ContentResolver])
    def resolveTypeIfNeeded(self, resolver):
        pass
    
    @args(Intent, [String])
    def setAction(self, action):
        pass
    
    @args(Intent, [Context, String])
    def setClassName(self, packageContext, className):
        pass
    
    @args(Intent, [String, String])
    def setClassName(self, packageName, className):
        pass
    
    @args(void, [ClipData])
    def setClipData(self, clip):
        pass
    
    @args(Intent, [ComponentName])
    def setComponent(self, component):
        pass
    
    @args(Intent, [Uri])
    def setData(self, data):
        pass
    
    @args(Intent, [Uri])
    def setDataAndNormalize(self, data):
        pass
    
    @args(Intent, [Uri, String])
    def setDataAndType(self, data, type_):
        pass
    
    @args(Intent, [Uri, String])
    def setDataAndTypeAndNormalize(self, data, type_):
        pass
    
    @args(void, [ClassLoader])
    def setExtrasClassLoader(self, loader):
        pass
    
    @args(Intent, [int])
    def setFlags(self, flags):
        pass
    
    @args(Intent, [String])
    def setPackage(self, packageName):
        pass
    
    @args(void, [Intent])
    def setSelector(self, selector):
        pass
    
    @args(void, [Rect])
    def setSourceBounds(self, rect):
        pass
    
    @args(Intent, [String])
    def setType(self, type_):
        pass
    
    @args(Intent, [String])
    def setTypeAndNormalize(self, type_):
        pass
    
    @args(String, [])
    def toString(self):
        pass
    
    @args(String, [int])
    def toUri(self, flags):
        pass
    
    @args(void, [Parcel, int])
    def writeToParcel(self, out, flags):
        pass


class IntentFilter(Object):

    __interfaces__ = [Parcelable]
    __static_fields__ = {
        "CREATOR": Parcelable.Creator(IntentFilter)
        }
    
    MATCH_ADJUSTMENT_MASK = 0x0000ffff
    MATCH_ADJUSTMENT_NORMAL = 0x00008000
    MATCH_CATEGORY_EMPTY = 0x00100000
    MATCH_CATEGORY_HOST = 0x00300000
    MATCH_CATEGORY_MASK = 0x0fff0000
    MATCH_CATEGORY_PATH = 0x00500000
    MATCH_CATEGORY_PORT = 0x00400000
    MATCH_CATEGORY_SCHEME = 0x00200000
    MATCH_CATEGORY_SCHEME_SPECIFIC_PART = 0x00580000
    MATCH_CATEGORY_TYPE = 0x00600000
    NO_MATCH_ACTION = -3
    NO_MATCH_CATEGORY = -4
    NO_MATCH_DATA = -2
    NO_MATCH_TYPE = -1
    SYSTEM_HIGH_PRIORITY = 1000
    SYSTEM_LOW_PRIORITY = -1000
    
    class AuthorityEntry(Object):
    
        @args(void, [String, String])
        def __init__(self, host, port):
            pass
        
        @args(String, [])
        def getHost(self):
            pass
        
        @args(int, [])
        def getPort(self):
            pass
        
        @args(int, [Uri])
        def match(self, data):
            pass
    
    class MalformedMimeTypeException:
    
        def __init__(self):
            pass
        
        @args(void, [String])
        def __init__(self, name):
            pass
    
    def __init__(self):
        pass
    
    @args(void, [String])
    def __init__(self, action):
        pass
    
    @args(void, [String, String])
    def __init__(self, action, dataType):
        pass
    
    @args(void, [IntentFilter])
    def __init__(self, intentFilter):
        pass
    
    @final
    @args(Iterator(String), [])
    def actionsIterator(self):
        pass
    
    @final
    @args(void, [String])
    def addAction(self, action):
        pass
    
    @final
    @args(void, [String])
    def addAction(self, action):
        pass
    
    @final
    @args(void, [String])
    def addCategory(self, category):
        pass
    
    @final
    @args(void, [String, String])
    def addDataAuthority(self, host, port):
        pass
    
    @final
    @args(void, [String, int])
    def addDataPath(self, path, type):
        pass
    
    @final
    @args(void, [String])
    def addDataScheme(self, scheme):
        pass
    
    @final
    @args(void, [String, int])
    def addDataSchemeSpecificPart(self, schemeSpecificPart, type):
        pass
    
    @final
    @args(void, [String])
    def addDataType(self, type):
        pass
    
    @final
    @args(Iterator(IntentFilter.AuthorityEntry), [])
    def authoritiesIterator(self):
        pass
    
    @final
    @args(Iterator(String), [])
    def categoriesIterator(self):
        pass
    
    @final
    @args(int, [])
    def countActions(self):
        pass
    
    @final
    @args(int, [])
    def countCategories(self):
        pass
    
    @final
    @args(int, [])
    def countDataAuthorities(self):
        pass
    
    @final
    @args(int, [])
    def countDataPaths(self):
        pass
    
    @final
    @args(int, [])
    def countDataSchemeSpecificParts(self):
        pass
    
    @final
    @args(int, [])
    def countDataSchemes(self):
        pass
    
    @final
    @args(int, [])
    def countDataTypes(self):
        pass
    
    @static
    @args(IntentFilter, [String, String])
    def create(action, dataType):
        pass
    
    @final
    @args(int, [])
    def describeContents(self):
        pass
    
    # ...
    
    @final
    @args(String, [int])
    def getAction(self, index):
        pass
    
    @final
    @args(String, [int])
    def getCategory(self, index):
        pass
    
    @final
    @args(IntentFilter.AuthorityEntry, [int])
    def getDataAuthority(self, index):
        pass
    
    # ...
    
    @final
    @args(String, [int])
    def getDataScheme(self, index):
        pass
    
    # ...
    
    @final
    @args(String, [int])
    def getDataType(self, index):
        pass
    
    @final
    @args(int, [])
    def getPriority(self, index):
        pass
    
    @final
    @args(bool, [String])
    def hasAction(self, action):
        pass
    
    @final
    @args(bool, [String])
    def hasCategory(self, category):
        pass
    
    @final
    @args(bool, [Uri])
    def hasDataAuthority(self, data):
        pass
    
    @final
    @args(bool, [String])
    def hasDataPath(self, data):
        pass
    
    @final
    @args(bool, [String])
    def hasDataScheme(self, scheme):
        pass
    
    @final
    @args(bool, [String])
    def hasDataSchemeSpecificPart(self, data):
        pass
    
    @final
    @args(bool, [String])
    def hasDataType(self, type):
        pass
    
    @final
    @args(int, [ContentResolver, Intent, bool, String])
    def match(self, resolver, intent, resolve, logTag):
        pass
    
    @final
    @args(int, [String, String, String, Uri, Set(String), String])
    def match(self, action, type, scheme, data, categories, logTag):
        pass
    
    @final
    @args(bool, [String])
    def matchAction(self, action):
        pass
    
    @final
    @args(bool, [Set(String)])
    def matchCategories(self, categories):
        pass
    
    @final
    @args(bool, [String, String, Uri])
    def matchData(self, type, scheme, data):
        pass
    
    @final
    @args(bool, [Uri])
    def matchDataAuthority(self, data):
        pass
    
    # ...
    
    @final
    @args(Iterator(String), [])
    def schemesIterator(self):
        pass
    
    @final
    @args(void, [int])
    def setPriority(self, priority):
        pass
    
    @final
    @args(Iterator(String), [])
    def typesIterator(self):
        pass
    
    @final
    @args(void, [Parcel, int])
    def writeToParcel(self, dest, flags):
        pass
    
    # ...


class IntentSender(Object):

    __interfaces__ = [Parcelable]
    __static_fields__ = {
        "CREATOR": Parcelable.Creator(IntentSender)
        }
    
    class OnFinished:
    
        @args(void, [IntentSender, Intent, int, String, Bundle])
        def onSendFinished(self, intentSender, intent, resultCode, resultData,
                           resultExtras):
            pass
    
    class SendIntentException(AndroidException):
    
        def __init__(self):
            pass
        
        @args(void, [String])
        def __init__(self, name):
            pass
        
        @args(void, [Exception])
        def __init__(self, cause):
            pass
    
    @args(String, [])
    def getCreatorPackage(self):
        pass
    
    @args(int, [])
    def getCreatorUid(self):
        pass
    
    @args(UserHandle, [])
    def getCreatorUserHandle(self):
        pass
    
    @args(String, [])
    def getTargetPackage(self):
        pass
    
    @static
    @args(IntentSender, [Parcel])
    def readIntentSenderOrNullFromParcel(input):
        pass
    
    @args(void, [Context, int, Intent, IntentSender.OnFinished, Handler, String])
    def sendIntent(self, context, code, intent, onFinished, handler, requiredPermission):
        pass
    
    @args(void, [Context, int, Intent, IntentSender.OnFinished, Handler])
    def sendIntent(self, context, code, intent, onFinished, handler):
        pass
    
    @static
    @args(void, [IntentSender, Parcel])
    def writeIntentSenderOrNullToParcel(sender, output):
        pass


class SharedPreferences:

    class Editor:
    
        def apply(self):
            pass
        
        @args(SharedPreferences.Editor, [])
        def clear(self):
            pass
        
        @args(bool, [])
        def commit(self):
            pass
        
        @args(SharedPreferences.Editor, [String, bool])
        def putBoolean(self, key, value):
            pass
        
        @args(SharedPreferences.Editor, [String, float])
        def putFloat(self, key, value):
            pass
        
        @args(SharedPreferences.Editor, [String, int])
        def putInt(self, key, value):
            pass
        
        @args(SharedPreferences.Editor, [String, long])
        def putLong(self, key, value):
            pass
        
        @args(SharedPreferences.Editor, [String, String])
        def putString(self, key, value):
            pass
        
        @args(SharedPreferences.Editor, [String, Set(String)])
        def putStringSet(self, key, value):
            pass
        
        @args(SharedPreferences.Editor, [String])
        def remove(self, key):
            pass
    
    class OnSharedPreferenceChangeListener:
    
        @args(void, [SharedPreferences, String])
        def onSharedPreferencesChanged(self, sharedPreferences, key):
            pass
    
    @args(bool, [String])
    def contains(self, key):
        pass
    
    @args(SharedPreferences.Editor, [])
    def edit(self):
        pass
    
    @args(Map(String,E), [])
    def getAll(self):
        pass
    
    @args(bool, [String, bool])
    def getBoolean(self, key, defaultValue):
        pass
    
    @args(float, [String, float])
    def getFloat(self, key, defaultValue):
        pass
    
    @args(int, [String, int])
    def getInt(self, key, defaultValue):
        pass
    
    @args(long, [String, long])
    def getLong(self, key, defaultValue):
        pass
    
    @args(String, [String, String])
    def getString(self, key, defaultValue):
        pass
    
    @args(Set(String), [String, Set(String)])
    def getStringSet(self, key, defaultValue):
        pass
    
    @args(void, [SharedPreferences.OnSharedPreferenceChangeListener])
    def registerOnSharedPreferenceChangeListener(self, listener):
        pass
    
    @args(void, [SharedPreferences.OnSharedPreferenceChangeListener])
    def unregisterOnSharedPreferenceChangeListener(self, listener):
        pass
