# Class declarations obtained from Android API documentation licensed under the
# Apache 2.0 license (http://www.apache.org/licenses/LICENSE-2.0) and rewritten
# in syntax that the Python compiler module can parse.

__package__ = "android.os"

from java.lang import Cloneable, Enum, Object, Runnable, String
import java.io
from java.util import ArrayList, Set
from java.util.concurrent import Executor
from android.os import UserHandle

class AsyncTask(Object):

    __parameters__ = [Params, Progress, Result]
    
    __fields__ = {
        "SERIAL_EXECUTOR": Executor,
        "THREAD_POOL_EXECUTOR": Executor
        }
    
    class Status(Enum):
    
        __static_fields__ = {
            "FINISHED": AsyncTask.Status,
            "PENDING": AsyncTask.Status,
            "RUNNING": AsyncTask.Status,
            }
        
        @static
        @args(AsyncTask.Status, [String])
        def valueOf(name):
            pass
        
        @static
        @args([AsyncTask.Status], [])
        def values():
            pass
    
    def __init__(self):
        pass
    
    @final
    @args(bool, [bool])
    def cancel(self, mayInterruptIfRunning):
        pass
    
    @abstract
    @args(Result, [[Params]])
    def doInBackground(self, params):
        pass
    
    @static
    @args(void, [Runnable])
    def execute(runnable):
        pass
    
    @final
    @args(AsyncTask(Params, Progress, Result), [[Params]])
    def execute(self, params):
        pass
    
    @final
    @args(AsyncTask(Params, Progress, Result), [Executor, [Params]])
    def executeOnExecutor(self, executor, params):
        pass
    
    @final
    @args(Result, [long, TimeUnit])
    def get(self, timeout, unit):
        pass
    
    @final
    @args(Result, [])
    def get(self):
        pass
    
    @final
    @args(AsyncTask.Status, [])
    def getStatus(self):
        pass
    
    @final
    @args(bool, [])
    def isCancelled(self):
        pass
    
    @args(void, [Result])
    def onCancelled(self, result):
        pass
    
    def onCancelled(self):
        pass
    
    @args(void, [Result])
    def onPostExecute(self, result):
        pass
    
    def onPreExecute(self):
        pass
    
    @args(void, [[Progress]])
    def onProgressUpdate(self, values):
        pass
    
    @final
    @args(void, [[Progress]])
    def publishProgress(self, value):
        pass


class Build(Object):

    UNKNOWN = "unknown"
    
    __static_fields__ = {
        "BOARD": String,
        "BOOTLOADER": String,
        "BRAND": String,
        "CPU_ABI": String,
        "CPU_ABI2": String,
        "DEVICE": String,
        "DISPLAY": String,
        "FINGERPRINT": String,
        "HARDWARE": String,
        "HOST": String,
        "ID": String,
        "MANUFACTURER": String,
        "MODEL": String,
        "PRODUCT": String,
        "RADIO": String,
        "SERIAL": String,
        "TAGS": String,
        "TIME": long,
        "TYPE": String,
        "USER": String
        }
    
    class VERSION(Object):
    
        __static_fields__ = {
            "CODENAME": String,
            "INCREMENTAL": String,
            "RELEASE": String,
            "SDK": String,
            "SDK_INT": int
            }
        
        def __init__(self):
            pass
    
    class VERSION_CODES(Object):
    
        BASE = 1
        BASE_1_1 = 2
        CUPCAKE = 3
        CUR_DEVELOPMENT = 10000
        DONUT = 4
        ECLAIR = 5
        ECLAIR_0_1 = 6
        ECLAIR_MR1 = 7
        FROYO = 8
        GINGERBREAD = 9
        GINGERBREAD_MR1 = 10
        HONEYCOMB = 11
        HONEYCOMB_MR1 = 12
        HONEYCOMB_MR2 = 13
        ICE_CREAM_SANDWICH = 14
        ICE_CREAM_SANDWICH_MR1 = 15
        JELLY_BEAN = 16
        JELLY_BEAN_MR1 = 17
        JELLY_BEAN_MR2 = 18
        KITKAT = 19
        
        def __init__(self):
            pass
    
    def __init__(self):
        pass
    
    @static
    @args(String, [])
    def getRadioVersion():
        pass


class BaseBundle(Object):

    pass


class Bundle(BaseBundle):

    __interfaces__ = [Parcelable]
    
    __static_fields__ = {
        "CREATOR": Parcelable.Creator(Bundle),
        "EMPTY": Bundle
        }
    
    def __init__(self):
        pass
    
    @args(void, [int])
    def __init__(self, capacity):
        pass
    
    @args(void, [Bundle])
    def __init__(self, bundle):
        pass
    
    def clear(self):
        pass
    
    @args(Object, [])
    def clone(self):
        pass
    
    @args(bool, [String])
    def containsKey(self, key):
        pass
    
    @args(int, [])
    def describeContents(self):
        pass
    
    @args(Object, [String])
    def get(self, key):
        pass
    
    @args(bool, [String])
    def getBoolean(self, key):
        pass
    
    @args(bool, [String, bool])
    def getBoolean(self, key, defaultValue):
        pass
    
    @args([bool], [String])
    def getBooleanArray(self, key):
        pass
    
    @args(Bundle, [String])
    def getBundle(self, key):
        pass
    
    @args(byte, [String])
    def getByte(self, key):
        pass
    
    @args(byte, [String, byte])
    def getByte(self, key, defaultValue):
        pass
    
    @args([byte], [String])
    def getByteArray(self, key):
        pass
    
    @args(char, [String])
    def getChar(self, key):
        pass
    
    @args(char, [String, char])
    def getChar(self, key, defaultValue):
        pass
    
    @args([char], [String])
    def getCharArray(self, key):
        pass
    
    @args(CharSequence, [String])
    def getCharSequence(self, key):
        pass
    
    @args(CharSequence, [String, CharSequence])
    def getCharSequence(self, key, defaultValue):
        pass
    
    @args([CharSequence], [String])
    def getCharSequenceArray(self, key):
        pass
    
    @args(ArrayList(CharSequence), [String])
    def getCharSequenceArrayList(self, key):
        pass
    
    @args(double, [String])
    def getDouble(self, key):
        pass
    
    @args(double, [String, double])
    def getDouble(self, key, defaultValue):
        pass
    
    @args([double], [String])
    def getDoubleArray(self, key):
        pass
    
    @args(float, [String])
    def getFloat(self, key):
        pass
    
    @args(float, [String, float])
    def getFloat(self, key, defaultValue):
        pass
    
    @args([float], [String])
    def getFloatArray(self, key):
        pass
    
    @args(int, [String])
    def getInt(self, key):
        pass
    
    @args(int, [String, int])
    def getInt(self, key, defaultValue):
        pass
    
    @args([int], [String])
    def getIntArray(self, key):
        pass
    
    @args(ArrayList(Integer), [String])
    def getIntegerArrayList(self, key):
        pass
    
    @args(long, [String])
    def getLong(self, key):
        pass
    
    @args(long, [String, long])
    def getLong(self, key, defaultValue):
        pass
    
    @args([long], [String])
    def getLongArray(self, key):
        pass
    
    @args(Serializable, [String])
    def getSerializable(self, key):
        pass
    
    @args(short, [String])
    def getShort(self, key):
        pass
    
    @args(short, [String, short])
    def get(self, key, defaultValue):
        pass
    
    @args([short], [String])
    def getShortArray(self, key):
        pass
    
    @args(String, [String])
    def getString(self, key):
        pass
    
    @args(String, [String, String])
    def getString(self, key, defaultValue):
        pass
    
    @args([String], [String])
    def getStringArray(self, key):
        pass
    
    @args(ArrayList(String), [String])
    def getStringArrayList(self, key):
        pass
    
    @args(bool, [])
    def hasFileDescriptors(self):
        pass
    
    @args(bool, [])
    def isEmpty(self):
        pass
    
    @args(Set(String), [])
    def keySet(self):
        pass
    
    @args(void, [Bundle])
    def putAll(self, map):
        pass
    
    @args(void, [String, bool])
    def putBoolean(self, key, value):
        pass
    
    @args(void, [String, [bool]])
    def putBooleanArray(self, key, value):
        pass
    
    @args(void, [String, Bundle])
    def putBundle(self, key, value):
        pass
    
    @args(void, [String, byte])
    def putByte(self, key, value):
        pass
    
    @args(void, [String, [byte]])
    def putByteArray(self, key, value):
        pass
    
    @args(void, [String, char])
    def putChar(self, key, value):
        pass
    
    @args(void, [String, [char]])
    def putCharArray(self, key, value):
        pass
    
    @args(void, [String, CharSequence])
    def putCharSequence(self, key, value):
        pass
    
    @args(void, [String, [CharSequence]])
    def putCharSequenceArray(self, key, value):
        pass
    
    @args(void, [String, ArrayList(CharSequence)])
    def putCharSequenceArrayList(self, key, value):
        pass
    
    @args(void, [String, double])
    def putDouble(self, key, value):
        pass
    
    @args(void, [String, [double]])
    def putDoubleArray(self, key, value):
        pass
    
    @args(void, [String, float])
    def putFloat(self, key, value):
        pass
    
    @args(void, [String, [float]])
    def putFloatArray(self, key, value):
        pass
    
    @args(void, [String, int])
    def putInt(self, key, value):
        pass
    
    @args(void, [String, [int]])
    def putIntArray(self, key, value):
        pass
    
    @args(void, [String, ArrayList(Integer)])
    def putIntegerArrayList(self, key, value):
        pass
    
    @args(void, [String, long])
    def putLong(self, key, value):
        pass
    
    @args(void, [String, [long]])
    def putLongArray(self, key, value):
        pass
    
    @args(void, [String, Parcelable])
    def putParcelable(self, key, value):
        pass
    
    @args(void, [String, [Parcelable]])
    def putParcelableArray(self, key, value):
        pass
    
    @args(void, [String, ArrayList(Parcelable)])
    def putParcelableArrayList(self, key, value):
        pass
    
    @args(void, [String, Serializable])
    def putSerializable(self, key, value):
        pass
    
    @args(void, [String, short])
    def putShort(self, key, value):
        pass
    
    @args(void, [String, [short]])
    def putShortArray(self, key, value):
        pass
    
    @args(void, [String, String])
    def putString(self, key, value):
        pass
    
    @args(void, [String, [String]])
    def putStringArray(self, key, value):
        pass
    
    @args(void, [String, ArrayList(String)])
    def putStringArrayList(self, key, value):
        pass
    
    @args(void, [Parcel])
    def readFromParcel(self, parcel):
        pass
    
    @args(void, [String])
    def remove(self, key):
        pass
    
    @args(int, [])
    def size(self):
        pass
    
    @args(String, [])
    def toString(self):
        pass
    
    @args(void, [Parcel, int])
    def writeToParcel(self, parcel, flags):
        pass


class Environment(Object):

    MEDIA_BAD_REMOVAL = "bad_removal"
    MEDIA_CHECKING = "checking"
    MEDIA_MOUNTED = "mounted"
    MEDIA_MOUNTED_READ_ONLY = "mounted_ro"
    MEDIA_NOFS = "nofs"
    MEDIA_REMOVED = "removed"
    MEDIA_UNKNOWN = "unknown"
    MEDIA_UNMOUNTABLE = "unmountable"
    MEDIA_UNMOUNTED = "unmounted"
    
    __static_fields__ = {
        "DIRECTORY_ALARMS": String,
        "DIRECTORY_DCIM": String,
        #"DIRECTORY_DOCUMENTS": String,
        "DIRECTORY_DOWNLOADS": String,
        "DIRECTORY_MOVIES": String,
        "DIRECTORY_MUSIC": String,
        "DIRECTORY_NOTIFICATIONS": String,
        "DIRECTORY_PICTURES": String,
        "DIRECTORY_PODCASTS": String,
        "DIRECTORY_RINGTONES": String
        }
    
    def __init__(self):
        pass
    
    @static
    @args(java.io.File, [])
    def getDataDirectory():
        pass
    
    @static
    @args(java.io.File, [])
    def getDownloadCacheDirectory():
        pass
    
    @permissions(android.permission.WRITE_EXTERNAL_STORAGE)
    @static
    @args(java.io.File, [])
    def getExternalStorageDirectory():
        pass
    
    @api(8)
    @static
    @args(java.io.File, [String])
    def getExternalStoragePublicDirectory(type):
        pass
    
    @api(21)
    @static
    @args(String, [])
    def getExternalStorageState():
        pass
    
    @static
    @args(java.io.File, [])
    def getRootDirectory():
        pass
    
    @api(19)
    @static
    @args(String, [java.io.File])
    def getStorageState(path):
        pass
    
    @api(11)
    @static
    @args(bool, [])
    def isExternalStorageEmulated():
        pass
    
    @api(21)
    @static
    @args(bool, [java.io.File])
    def isExternalStorageEmulated(path):
        pass
    
    @api(21)
    @static
    @args(boolean, [java.io.File])
    def isExternalStorageRemovable(path):
        pass
    
    @api(9)
    @static
    @args(boolean, [])
    def isExternalStorageRemovable():
        pass


class Handler(Object):

    class Callback:
    
        @args(bool, [Message])
        def handleMessage(self, msg):
            pass
    
    def __init__(self):
        pass
    
    @args(void, [Handler.Callback])
    def __init__(self, callback):
        pass
    
    @args(void, [Looper])
    def __init__(self, looper):
        pass
    
    @args(void, [Looper, Handler.Callback])
    def __init__(self, looper, callback):
        pass
    
    @args(void, [Message])
    def dispatchMessage(self, message):
        pass
    
    @final
    @args(Looper, [])
    def getLooper(self):
        pass
    
    @args(String, [Message])
    def getMessageName(self, message):
        pass
    
    @args(void, [Message])
    def handleMessage(self, message):
        pass
    
    @final
    @args(bool, [int, Object])
    def hasMessages(self, what, object):
        pass
    
    @final
    @args(bool, [int])
    def hasMessages(self, what):
        pass
    
    @final
    @args(Message, [int, int, int])
    def obtainMessage(self, what, arg1, arg2):
        pass
    
    @final
    @args(Message, [])
    def obtainMessage(self):
        pass
    
    @final
    @args(Message, [int, int, int, Object])
    def obtainMessage(self, what, arg1, arg2, object):
        pass
    
    @final
    @args(Message, [int])
    def obtainMessage(self, what):
        pass
    
    @final
    @args(Message, [int, Object])
    def obtainMessage(self, what, object):
        pass
    
    @final
    @args(bool, [Runnable])
    def post(self, runnable):
        pass
    
    @final
    @args(bool, [Runnable])
    def postAtFrontOfQueue(self, runnable):
        pass
    
    @final
    @args(bool, [Runnable, Object, long])
    def postAtTime(self, runnable, token, uptimeMillis):
        pass
    
    @final
    @args(bool, [Runnable, long])
    def postAtTime(self, runnable, uptimeMillis):
        pass
    
    @final
    @args(bool, [Runnable, long])
    def postDelayed(self, runnable, delayMillis):
        pass
    
    @final
    @args(void, [Runnable])
    def removeCallbacks(self, runnable):
        pass
    
    @final
    @args(void, [Runnable, Object])
    def removeCallbacks(self, runnable, token):
        pass
    
    @final
    @args(void, [Object])
    def removeCallbacksAndMessages(self, token):
        pass
    
    @final
    @args(void, [int])
    def removeCallbacks(self, what):
        pass
    
    @final
    @args(void, [int, Object])
    def removeCallbacks(self, what, object):
        pass
    
    @final
    @args(bool, [int])
    def sendEmptyMessage(self, what):
        pass
    
    @final
    @args(bool, [int, long])
    def sendEmptyMessageAtTime(self, what, uptimeMillis):
        pass
    
    @final
    @args(bool, [int, long])
    def sendEmptyMessageDelayed(self, what, delayMillis):
        pass
    
    @final
    @args(bool, [Message])
    def sendMessage(self, message):
        pass
    
    @final
    @args(bool, [Message])
    def sendMessageAtFrontOfQueue(self, message):
        pass
    
    @args(bool, [Message, long])
    def sendMessageAtTime(self, message, uptimeMillis):
        pass
    
    @final
    @args(bool, [Message, long])
    def sendMessageDelayed(self, message, delayMillis):
        pass


class IBinder:

    class DeathRecipient:
    
        def binderDied(self):
            pass
    
    DUMP_TRANSACTION = 0x5f444d50
    FIRST_CALL_TRANSACTION = 1
    FLAG_ONEWAY = 1
    INTERFACE_TRANSACTION = 0x5f4e5446
    LAST_CALL_TRANSACTION = 0x00ffffff
    LIKE_TRANSACTION = 0x5f4c494b
    PING_TRANSACTION = 0x5f504e47
    TWEET_TRANSACTION = 0x5f545754
    
    @api(3)
    @args(void, [FileDescriptor, [String]])
    def dump(self, fd, args):
        pass
    
    @api(13)
    @args(void, [FileDescriptor, [String]])
    def dumpAsync(self, fd, args):
        pass
    
    @args(String, [])
    def getInterfaceDescriptor(self):
        pass
    
    @args(bool, [])
    def isBinderAlive(self):
        pass
    
    @args(void, [IBinder.DeathRecipient, int])
    def linkToDeath(self, recipient, flags):
        pass
    
    @args(bool, [])
    def pingBinder(self):
        pass
    
    @args(IInterface, [String])
    def queryLocalInterface(self, descriptor):
        pass
    
    @args(bool, [int, Parcel, Parcel, int])
    def transact(self, code, data, reply, flags):
        pass
    
    @args(bool, [IBinder.DeathRecipient, int])
    def unlinkToDeath(self, recipient, flags):
        pass


class Looper(Object):

    @static
    @args(Looper, [])
    def getMainLooper():
        pass
    
    #@args(MessageQueue, [])
    #def getQueue(self):
    #    pass
    
    @args(Thread, [])
    def getThread(self):
        pass
    
    @args(bool, [])
    def isCurrentThread(self):
        pass
    
    @static
    def loop():
        pass
    
    @static
    @args(Looper, [])
    def myLooper():
        pass
    
    #@static
    #@args(MessageQueue, [])
    #def myQueue():
    #    pass
    
    @static
    def prepare():
        pass
    
    @static
    def prepareMainLooper():
        pass
    
    def quit(self):
        pass
    
    def quitSafely(self):
        pass
    
    #@args(void, [Printer])
    #def setMessageLogging(self, printer):
    #    pass


class Message(Object):

    __interfaces__ = [Parcelable]
    
    __static_fields__ = {
        "CREATOR": Parcelable.Creator(Message),
        "arg1": int,
        "arg2": int,
        "obj": Object,
        "replyTo": Messenger,
        "sendingUid": int,
        "what": int
        }
    
    def __init__(self):
        pass
    
    @args(void, [Message])
    def copyFrom(self, o):
        pass
    
    @args(int, [])
    def describeContents(self):
        pass
    
    @args(Runnable, [])
    def getCallback(self):
        pass
    
    @args(Bundle, [])
    def getData(self):
        pass
    
    @args(Handler, [])
    def getTarget(self):
        pass
    
    @args(long, [])
    def getWhen(self):
        pass
    
    @args(bool, [])
    def isAsynchronous(self):
        pass
    
    @static
    @args(Message, [Handler, int, int, Object])
    def obtain(self, h, what, arg1, arg2, obj):
        pass
    
    @static
    @args(Message, [Handler, int, Object])
    def obtain(self, h, what, obj):
        pass
    
    @static
    @args(Message, [Handler, int])
    def obtain(self, h, what):
        pass
    
    @static
    @args(Message, [Handler])
    def obtain(self, h):
        pass
    
    @static
    @args(Message, [Handler, Runnable])
    def obtain(self, h, callback):
        pass
    
    @static
    @args(Message, [])
    def obtain(self):
        pass
    
    @static
    @args(Message, [Handler, int, int, int])
    def obtain(self, h, what, arg1, arg2):
        pass
    
    @static
    @args(Message, [Message])
    def obtain(self, orig):
        pass
    
    @args(Bundle, [])
    def peekData(self):
        pass
    
    def recycle(self):
        pass
    
    def sendToTarget(self):
        pass
    
    @args(void, [bool])
    def setAsynchronous(self, enable):
        pass
    
    @args(void, [Bundle])
    def setData(self, data):
        pass
    
    @args(void, [Handler])
    def setTarget(self, target):
        pass
    
    @args(String, [])
    def toString(self):
        pass
    
    @args(void, [Parcel, int])
    def writeToParcel(self, dest, flags):
        pass


class Messenger(Object):

    __interfaces__ = [Parcelable]
    
    __static_fields__ = {
        "CREATOR": Parcelable.Creator(Messenger)
        }
    
    @args(void, [Handler])
    def __init__(self, target):
        pass
    
    @args(void, [IBinder])
    def __init__(self, target):
        pass
    
    @args(int, [])
    def describeContents(self):
        pass
    
    @args(bool, [Object])
    def equals(self, otherObj):
        pass
    
    @args(IBinder, [])
    def getBinder(self):
        pass
    
    @args(int, [])
    def hashCode(self):
        pass
    
    @static
    @args(Messenger, [Parcel])
    def readMessengerOrNullFromParcel(self, in_):
        pass
    
    @args(void, [Message])
    def send(self, message):
        pass
    
    @static
    @args(void, [Messenger, Parcel])
    def writeMessengerOrNullToParcel(self, messenger, out):
        pass
    
    @args(void, [Parcel, int])
    def writeToParcel(self, out, flags):
        pass


class Parcel(Object):

    pass


class Parcelable:

    class Creator:
    
        # These E placeholders may need to be T placeholders and handled
        # differently.
        @args(E, Parcel)
        def createFromParcel(self, source):
            pass
        
        @args([E], [int])
        def newArray(self, size):
            pass
    
    @args(int, [])
    def describeContents(self):
        pass
    
    @args(void, [Parcel, int])
    def writeToParcel(self, dest, flags):
        pass


class ParcelFileDescriptor(Object):

    __interfaces__ = [Parcelable, Closeable]
    __fields__ = {
        "CREATOR": Parcelable.Creator(ParcelFileDescriptor)
        }
    
    MODE_APPEND          = 0x02000000
    MODE_CREATE          = 0x08000000
    MODE_READ_ONLY       = 0x10000000
    MODE_READ_WRITE      = 0x30000000
    MODE_TRUNCATE        = 0x40000000
    MODE_WORLD_READABLE  = 0x00000001 # deprecated in API level 19
    MODE_WORLD_WRITEABLE = 0x00000002 # deprecated in API level 19
    MODE_WRITE_ONLY      = 0x20000000

    class AutoCloseInputStream(java.io.FileInputStream):
    
        @args(void, [ParcelFileDescriptor])
        def __init__(self, pfd):
            pass
        
        def close(self):
            pass
    
    class AutoCloseOutputStream(java.io.FileOutputStream):
    
        @args(void, [ParcelFileDescriptor])
        def __init__(self, pfd):
            pass
        
        def close(self):
            pass
    
    class FileDescriptorDetachedException(java.io.IOException):
    
        def __init__(self):
            pass
    
    class OnCloseListener:
    
        @abstract
        @args(void, [java.io.IOException])
        def onClose(self, exception):
            pass
    
    @args(void, [ParcelFileDescriptor])
    def __init__(self, wrapped):
        pass
    
    @static
    @args(ParcelFileDescriptor, [int])
    def adoptFd(fileDescriptor):
        pass
    
    @args(bool, [])
    def canDetectErrors(self):
        pass
    
    def checkError(self):
        pass
    
    def close(self):
        pass
    
    @args(void, [String])
    def closeWithError(self, message):
        pass
    
    @static
    @args([ParcelFileDescriptor], [])
    def createPipe():
        pass
    
    @static
    @args([ParcelFileDescriptor], [])
    def createReliablePipe():
        pass
    
    @static
    @args([ParcelFileDescriptor], [])
    def createReliableSocketPair():
        pass
    
    @static
    @args([ParcelFileDescriptor], [])
    def createSocketPair():
        pass
    
    @args(int, [])
    def describeContents(self):
        pass
    
    @args(int, [])
    def detachFd(self):
        pass
    
    @static
    @args(ParcelFileDescriptor, [java.io.FileDescriptor])
    def dup(original):
        pass
    
    @args(ParcelFileDescriptor, [])
    def dup(self):
        pass
    
    @static
    @args(ParcelFileDescriptor, [java.io.FileDescriptor])
    def fromDatagramSocket(datagramSocket):
        pass
    
    @static
    @args(ParcelFileDescriptor, [int])
    def fromFd(fileDescriptor):
        pass
    
    #@static
    #@args(ParcelFileDescriptor, [java.net.Socket])
    #def fromSocket(socket):
    #    pass
    
    @args(int, [])
    def getFd(self):
        pass
    
    @args(FileDescriptor, [])
    def getFileDescriptor(self):
        pass
    
    @args(long, [])
    def getStatSize(self):
        pass
    
    @static
    @args(ParcelFileDescriptor, [java.io.File, int, Handler, ParcelFileDescriptor.OnCloseListener])
    def open(file, mode, handler, listener):
        pass
    
    @static
    @args(ParcelFileDescriptor, [java.io.File, int])
    def open(file, mode):
        pass
    
    @static
    @args(int, [String])
    def parseMode(mode):
        pass


class PatternMatcher(Object):

    __interfaces__ = [Parcelable]
    __fields__ = {
        "CREATOR": Parcelable.Creator(UserHandle)
        }
    
    PATTERN_LITERAL = 0
    PATTERN_PREFIX = 1
    PATTERN_SIMPLE_GLOB = 2
    
    @final
    @args(String, [])
    def getPath(self):
        pass
    
    @final
    @args(int, [])
    def getType(self):
        pass
    
    @args(bool, [String])
    def match(self, str):
        pass


class SystemClock(Object):

    @static
    @args(long, [])
    def currentThreadTimeMillis(self):
        pass
    
    @static
    @args(long, [])
    def elapsedRealtime(self):
        pass
    
    @static
    @args(long, [])
    def elapsedRealtimeNanos(self):
        pass
    
    @static
    @args(bool, [long])
    def setCurrentTimeMillis(self, milliseconds):
        pass
    
    @static
    @args(void, [long])
    def sleep(self, milliseconds):
        pass
    
    @static
    @args(long, [])
    def uptimeMillis(self):
        pass


class UserHandle(Object):

    __interfaces__ = [Parcelable]
    __fields__ = {
        "CREATOR": Parcelable.Creator(UserHandle)
        }
    
    @args(void, [Parcel])
    def __init__(self, input):
        pass
    
    @static
    @args(UserHandle, [Parcel])
    def readFromParcel(input):
        pass
    
    @static
    @args(void, [UserHandle, Parcel])
    def writeToParcel(handle, out):
        pass
