# Class declarations obtained from Android API documentation licensed under the
# Apache 2.0 license (http://www.apache.org/licenses/LICENSE-2.0) and rewritten
# in syntax that the Python compiler module can parse.

__package__ = "android.view"

from java.lang import CharSequence, Object, RuntimeException, String
import android.content
from android.content.res import Configuration, Resources
from android.graphics import Canvas, Matrix, Point, Rect, SurfaceTexture
from android.graphics.drawable import Drawable
from android.os import Bundle, Handler, Parcel, Parcelable
from android.util import DisplayMetrics

class AbsSavedState(Object):

    __interfaces__ = [Parcelable]
    
    __static_fields__ = {"CREATOR": Creator(AbsSavedState),
                         "EMPTY_STATE": AbsSavedState}
    
    @args(void, [Parcelable])
    def __init__(self, superState):
        pass
    
    @args(void, [Parcel])
    def __init__(self, source):
        pass
    
    @args(int, [])
    def describeContents(self):
        pass
    
    @final
    @args(Parcelable, [])
    def getSuperState(self):
        pass
    
    @args(void, [Parcel, int])
    def writeToParcel(self, dest, flags):
        pass


class ActionMode(Object):

    DEFAULT_HIDE_DURATION = -1
    TYPE_FLOATING = 1
    TYPE_PRIMARY = 0
    
    class Callback:
    
        @args(bool, [ActionMode, MenuItem])
        def onActionItemClicked(self, mode, item):
            pass
        
        @args(bool, [ActionMode, Menu])
        def onCreateActionMode(self, mode, menu):
            pass
        
        @args(bool, [ActionMode])
        def onDestroyActionMode(self, mode):
            pass
        
        @args(bool, [ActionMode, Menu])
        def onPrepareActionMode(self, mode, menu):
            pass
    
    class Callback2(Object):
    
        def __init__(self):
            pass
        
        @args(void, [ActionMode, View, Rect])
        def onGetContentRect(self, mode, view, rect):
            pass
    
    def __init__(self):
        pass
    
    @abstract
    def finish(self):
        pass
    
    @abstract
    @args(View, [])
    def getCustomView(self):
        pass
    
    @abstract
    @args(Menu, [])
    def getMenu(self):
        pass
    
    @abstract
    @args(MenuInflater, [])
    def getMenuInflater(self):
        pass
    
    @abstract
    @args(CharSequence, [])
    def getSubtitle(self):
        pass
    
    @args(Object, [])
    def getTag(self):
        pass
    
    @abstract
    @args(CharSequence, [])
    def getTitle(self):
        pass
    
    @args(bool, [])
    def getTypeOptionalHint(self):
        pass
    
    @args(int, [])
    def getType(self):
        pass
    
    @args(void, [long])
    def getHide(self, duration):
        pass
    
    @abstract
    def invalidate(self):
        pass
    
    def invalidateContentRect(self):
        pass
    
    @args(bool, [])
    def isTitleOptional(self):
        pass
    
    @args(void, [bool])
    def onWindowFocusChanged(self, hasWindowFocus):
        pass
    
    @abstract
    @args(void, [View])
    def setCustomView(self, view):
        pass
    
    @abstract
    @args(void, [int])
    def setSubtitle(self, resId):
        pass
    
    @abstract
    @args(void, [CharSequence])
    def setSubtitle(self, subtitle):
        pass
    
    @args(void, [Object])
    def setTag(self, tag):
        pass
    
    @abstract
    @args(void, [CharSequence])
    def setTitle(self, subtitle):
        pass
    
    @abstract
    @args(void, [int])
    def setTitle(self, resId):
        pass
    
    @args(void, [bool])
    def setTitleOptionalHint(self, titleOptional):
        pass
    
    @args(void, [int])
    def setType(self, type):
        pass


class ContextMenu:

    __interfaces__ = [Menu]
    
    class ContextMenuInfo:
        pass
    
    def clearHeader(self):
        pass
    
    @args(ContextMenu, [Drawable])
    def setHeaderIcon(self, icon):
        pass
    
    @args(ContextMenu, [int])
    def setHeaderIcon(self, iconRes):
        pass
    
    @args(ContextMenu, [CharSequence])
    def setHeaderTitle(self, title):
        pass
    
    @args(ContextMenu, [int])
    def setHeaderTitle(self, titleRes):
        pass
    
    @args(ContextMenu, [View])
    def setHeaderView(self, view):
        pass


class ContextThemeWrapper(android.content.ContextWrapper):

    def __init__(self):
        pass
    
    @args(void, [Context, int])
    def __init__(self, base, themeResId):
        pass
    
    @args(void, [Context, Resources.Theme])
    def __init__(self, base, theme):
        pass
    
    @args(void, [Configuration])
    def applyOverrideConfiguration(self, overrideConfiguration):
        pass
    
    @args(Resources, [])
    def getResources(self):
        pass
    
    @args(Object, [String])
    def getSystemService(self, name):
        pass
    
    @args(Resources.Theme, [])
    def getTheme(self):
        pass
    
    @args(void, [int])
    def setTheme(self, resid):
        pass


class Display(Object):

    DEFAULT_DISPLAY = 0
    FLAG_PRESENTATION = 8
    FLAG_PRIVATE = 4
    FLAG_SECURE = 2
    FLAG_SUPPORTS_PROTECTED_BUFFERS = 1
    
    @args(void, [Point, Point])
    def getCurrentSizeRange(self, outSmallestSize, outLargestSize):
        pass
    
    @args(int, [])
    def getDisplayId(self):
        pass
    
    @args(int, [])
    def getFlags(self):
        pass
    
    @args(void, [DisplayMetrics])
    def getMetrics(self, outMetrics):
        pass
    
    @args(String, [])
    def getName(self):
        pass
    
    @args(void, [DisplayMetrics])
    def getRealMetrics(self, outMetrics):
        pass
    
    @args(void, [Point])
    def getRealSize(self, outSize):
        pass
    
    @args(void, [Rect])
    def getRectSize(self, outSize):
        pass
    
    @args(float, [])
    def getRefreshRate(self):
        pass
    
    @args(int, [])
    def getRotation(self):
        pass
    
    @args(void, [Point])
    def getSize(self, outSize):
        pass
    
    @args(bool, [])
    def isValid(self):
        pass


class Gravity(Object):

    AXIS_CLIP                           = 0x00000008
    AXIS_PULL_AFTER                     = 0x00000004
    AXIS_PULL_BEFORE                    = 0x00000002
    AXIS_SPECIFIED                      = 0x00000001
    AXIS_X_SHIFT                        = 0x00000000
    AXIS_Y_SHIFT                        = 0x00000004
    BOTTOM                              = 0x00000050
    CENTER                              = 0x00000011
    CENTER_HORIZONTAL                   = 0x00000001
    CENTER_VERTICAL                     = 0x00000010
    CLIP_HORIZONTAL                     = 0x00000008
    CLIP_VERTICAL                       = 0x00000080
    DISPLAY_CLIP_HORIZONTAL             = 0x01000000
    DISPLAY_CLIP_VERTICAL               = 0x10000000
    END                                 = 0x00800005
    FILL                                = 0x00000077
    FILL_HORIZONTAL                     = 0x00000007
    FILL_VERTICAL                       = 0x00000070
    HORIZONTAL_GRAVITY_MASK             = 0x00000007
    LEFT                                = 0x00000003
    NO_GRAVITY                          = 0x00000000
    RELATIVE_HORIZONTAL_GRAVITY_MASK    = 0x00800007
    RELATIVE_LAYOUT_DIRECTION           = 0x00800000
    RIGHT                               = 0x00000005
    START                               = 0x00800003
    TOP                                 = 0x00000030
    VERTICAL_GRAVITY_MASK               = 0x00000070
    
    def __init__(self):
        pass
    
    @static
    @args(void, [int, int, int, Rect, Rect, int])
    def apply(gravity, w, h, container, outRect, layoutDirection):
        pass
    
    @static
    @args(void, [int, int, int, Rect, int, int, Rect])
    def apply(gravity, w, h, container, xAdj, yAdj, outRect):
        pass
    
    @static
    @args(void, [int, int, int, Rect, Rect])
    def apply(gravity, w, h, container, outRect):
        pass
    
    @static
    @args(void, [int, int, int, Rect, int, int, Rect, int])
    def apply(gravity, w, h, container, xAdj, yAdj, outRect, layoutDirection):
        pass
    
    @static
    @args(void, [int, Rect, Rect])
    def applyDisplay(gravity, display, inoutObj):
        pass
    
    @static
    @args(void, [int, Rect, Rect, int])
    def applyDisplay(gravity, display, inoutObj, layoutDirection):
        pass
    
    @static
    @args(int, [int, int])
    def getAbsoluteGravity(gravity, layoutDirection):
        pass
    
    @static
    @args(bool, [int])
    def isHorizontal(gravity):
        pass
    
    @static
    @args(bool, [int])
    def isVertical(gravity):
        pass


class InputEvent(Object):

    pass


class KeyEvent(InputEvent):

    __interfaces__ = [Parcelable]
    
    __static_fields__ = {"CREATOR": Creator(KeyEvent)}
    
    class Callback:
    
        @args(bool, [int, KeyEvent])
        def onKeyDown(self, keyCode, event):
            pass
        
        @args(bool, [int, KeyEvent])
        def onKeyLongPress(self, keyCode, event):
            pass
        
        @args(bool, [int, int, KeyEvent])
        def onKeyMultiple(self, keyCode, count, event):
            pass
        
        @args(bool, [int, KeyEvent])
        def onKeyUp(self, keyCode, event):
            pass
    
    @args(void, [int, int])
    def __init__(self, action, code):
        pass
    
    @args(void, [long, long, int, int, int])
    def __init__(self, downTime, eventTime, action, code, repeat):
        pass
    
    @args(void, [long, long, int, int, int, int])
    def __init__(self, downTime, eventTime, action, code, repeat, metaState):
        pass
    
    @args(void, [long, long, int, int, int, int, int, int])
    def __init__(self, downTime, eventTime, action, code, repeat, metaState,
                 deviceId, scanCode):
        pass
    
    @args(void, [long, long, int, int, int, int, int, int, int])
    def __init__(self, downTime, eventTime, action, code, repeat, metaState,
                 deviceId, scanCode, flags):
        pass
    
    @args(void, [long, long, int, int, int, int, int, int, int, int])
    def __init__(self, downTime, eventTime, action, code, repeat, metaState,
                 deviceId, scanCode, flags, source):
        pass
    
    @args(void, [long, String, int, int])
    def __init__(self, time, characters, deviceId, flags):
        pass
    
    @args(void, [KeyEvent])
    def __init__(self, original):
        pass
    
    ### ...


class LayoutInflater(Object):

    class Filter:
    
        @args(bool, [Class])
        def onLoadClass(self, class_):
            pass
    
    @args(void, [Context])
    def __init__(self, context):
        pass
    
    @args(void, [LayoutInflater, Context])
    def __init__(self, original, newContext):
        pass
    
    @abstract
    @args(LayoutInflater, [Context])
    def cloneInContext(self, newContext):
        pass
    
    @final
    @args(View, [String, String, AttributeSet])
    def createView(self, name, prefix, attributes):
        pass
    
    @args(Context, [])
    def getContext(self):
        pass
    
    @args(View, [int, ViewGroup])
    def inflate(self, resource, root):
        pass
    
    @args(View, [int, ViewGroup, bool])
    def inflate(self, resource, root, attachToRoot):
        pass


class Menu:

    NONE = 0
    
    @abstract
    @args(MenuItem, [CharSequence])
    def add(self, title):
        pass
    
    @abstract
    @args(MenuItem, [int, int, int, int])
    def add(self, groupId, itemId, order, titleRes):
        pass
    
    @abstract
    @args(MenuItem, [int])
    def add(self, titleRes):
        pass
    
    @abstract
    @args(MenuItem, [int, int, int, CharSequence])
    def add(self, groupId, itemId, order, title):
        pass
    
    @abstract
    @args(void, [])
    def clear(self):
        pass
    
    @abstract
    @args(void, [])
    def close(self):
        pass
    
    @abstract
    @args(void, [int])
    def removeItem(self, id):
        pass
    
    @abstract
    @args(int, [])
    def size(self):
        pass


class MenuInflater(Object):

    @args(void, [Context])
    def __init__(self, context):
        pass
    
    @args(void, [int, Menu])
    def inflate(self, menuRes, menu):
        pass


class MenuItem:

    SHOW_AS_ACTION_NEVER = 0
    SHOW_AS_ACTION_IF_ROOM = 1
    SHOW_AS_ACTION_ALWAYS = 2
    SHOW_AS_ACTION_WITH_TEXT = 4
    SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW = 4
    
    class OnActionExpandListener:
    
        @abstract
        @args(bool, [MenuItem])
        def onMenuItemActionCollapse(self, item):
            pass
        
        @abstract
        @args(bool, [MenuItem])
        def onMenuItemActionExpand(self, item):
            pass
    
    class OnMenuItemClickListener:
    
        @abstract
        @args(bool, [MenuItem])
        def onMenuItemClick(self, item):
            pass
    
    @abstract
    @args(bool, [])
    def collapseActionView(self):
        pass
    
    @abstract
    @args(bool, [])
    def expandActionView(self):
        pass
    
    @abstract
    @args(ActionProvider, [])
    def getActionProvider(self):
        pass
    
    @abstract
    @args(View, [])
    def getActionView(self):
        pass
    
    @abstract
    @args(char, [])
    def getAlphabeticShortcut(self):
        pass
    
    @abstract
    @args(int, [])
    def getGroupId(self):
        pass
    
    @abstract
    @args(Drawable, [])
    def getIcon(self):
        pass
    
    @abstract
    @args(Intent, [])
    def getIntent(self):
        pass
    
    @abstract
    @args(int, [])
    def getItemId(self):
        pass
    
    @abstract
    @args(ContextMenu.ContextMenuInfo, [])
    def getMenuInfo(self):
        pass
    
    @abstract
    @args(char, [])
    def getNumericShortcut(self):
        pass
    
    @abstract
    @args(int, [])
    def getOrder(self):
        pass
    
    @abstract
    @args(SubMenu, [])
    def getSubMenu(self):
        pass
    
    @abstract
    @args(CharSequence, [])
    def getTitle(self):
        pass
    
    @abstract
    @args(CharSequence, [])
    def getTitleCondensed(self):
        pass
    
    @abstract
    @args(bool, [])
    def hasSubMenu(self):
        pass
    
    @abstract
    @args(boolean, [])
    def isActionViewExpanded(self):
        pass
    
    @abstract
    @args(bool, [])
    def isCheckable(self):
        pass
    
    @abstract
    @args(bool, [])
    def isChecked(self):
        pass
    
    @abstract
    @args(bool, [])
    def isEnabled(self):
        pass
    
    @abstract
    @args(bool, [])
    def isVisible(self):
        pass
    
    @abstract
    @args(MenuItem, [ActionProvider])
    def setActionProvider(self, actionProvider):
        pass
    
    @abstract
    @args(MenuItem, [int])
    def setActionView(self, resId):
        pass
    
    @abstract
    @args(MenuItem, [View])
    def setActionView(self, view):
        pass
    
    @abstract
    @args(MenuItem, [char])
    def setAlphabeticShortcut(self, alphaChar):
        pass
    
    @abstract
    @args(MenuItem, [bool])
    def setCheckable(self, checkable):
        pass
    
    @abstract
    @args(MenuItem, [bool])
    def setChecked(self, checked):
        pass
    
    @abstract
    @args(MenuItem, [bool])
    def setEnabled(self, enabled):
        pass
    
    @abstract
    @args(MenuItem, [Drawable])
    def setIcon(self, icon):
        pass
    
    @abstract
    @args(MenuItem, [int])
    def setIcon(self, iconRes):
        pass
    
    @abstract
    @args(MenuItem, [Intent])
    def setIntent(self, intent):
        pass
    
    @abstract
    @args(MenuItem, [char])
    def setNumericShortcut(self, numericChar):
        pass
    
    @abstract
    @args(MenuItem, [MenuItem.OnActionExpandListener])
    def setOnActionExpandListener(self, listener):
        pass
    
    @abstract
    @args(MenuItem, [MenuItem.OnMenuItemClickListener])
    def setOnMenuItemClickListener(self, menuItemClickListener):
        pass
    
    @abstract
    @args(MenuItem, [char, char])
    def setShortcut(self, numericChar, alphaChar):
        pass
    
    @abstract
    @args(void, [int])
    def setShowAsAction(self, actionEnum):
        pass
    
    @abstract
    @args(MenuItem, [int])
    def setShowAsActionFlags(self, actionEnum):
        pass
    
    @abstract
    @args(MenuItem, [CharSequencel])
    def setTitle(self, title):
        pass
    
    @abstract
    @args(MenuItem, [int])
    def setTitle(self, title):
        pass
    
    @abstract
    @args(MenuItem, [CharSequence])
    def setTitleCondensed(self, title):
        pass
    
    @abstract
    @args(MenuItem, [bool])
    def setVisible(self, visible):
        pass


class MotionEvent(InputEvent):

    ACTION_CANCEL = 3
    ACTION_DOWN = 0
    ACTION_HOVER_ENTER = 9
    ACTION_HOVER_EXIT = 10
    ACTION_HOVER_MOVE = 7
    ACTION_MASK = 0xff
    ACTION_MOVE = 2
    ACTION_OUTSIDE = 4
    ACTION_POINTER_DOWN = 5
    ACTION_POINTER_INDEX_MASK = 0xff00
    ACTION_POINTER_INDEX_SHIFT = 8
    ACTION_POINTER_UP = 6
    ACTION_SCROLL = 8
    ACTION_UP = 1
    
    ### More constants ...
    
    class PointerCoords(Object):
    
        __fields__ = {
            "orientation": float, "pressure": float,
            "size": float,
            "toolMajor": float, "toolMinor": float,
            "touchMajor": float, "touchMinor": float,
            "x": float, "y": float
            }
        
        def __init__(self):
            pass
        
        @args(void, [MotionEvent.PointerCoords])
        def __init__(self, other):
            pass
        
        def clear(self):
            pass
        
        @args(void, [MotionEvent.PointerCoords])
        def copyFrom(self, other):
            pass
        
        @args(float, [int])
        def getAxisValue(self, axis):
            pass
        
        @args(void, [int, float])
        def setAxisValue(self, axis, value):
            pass
    
    class PointerProperties(Object):
    
        __fields__ = {"id": int, "toolType": int}
        
        def __init__(self):
            pass
        
        @args(void, [MotionEvent.PointerProperties])
        def __init__(self, other):
            pass
        
        def clear(self):
            pass
        
        @args(void, [MotionEvent.PointerProperties])
        def copyFrom(self, other):
            pass
    
    @static
    @args(String, [int])
    def actionToString(self, action):
        pass
    
    @final
    @args(void, [long, [MotionEvent.PointerCoords], int])
    def addBatch(self, eventTime, pointerCoords, metaState):
        pass
    
    @final
    @args(void, [long, float, float, float, float, int])
    def addBatch(self, eventTime, x, y, pressure, size, metaState):
        pass
    
    @static
    @args(int, [String])
    def axisFromString(self, symbolicName):
        pass
    
    @static
    @args(String, [int])
    def axisToString(self, axis):
        pass
    
    @final
    @args(int, [int])
    def findPointerIndex(self, pointerId):
        pass
    
    @final
    @args(int, [])
    def getAction(self):
        pass
    
    @final
    @args(int, [])
    def getActionButton(self):
        pass
    
    @final
    @args(int, [])
    def getActionIndex(self):
        pass
    
    @final
    @args(int, [])
    def getActionMasked(self):
        pass
    
    @final
    @args(float, [int])
    def getAxisValue(self, axis):
        pass
    
    @final
    @args(float, [int, int])
    def getAxisValue(self, axis, pointerIndex):
        pass
    
    @final
    @args(float, [])
    def getButtonState(self):
        pass
    
    @final
    @args(int, [])
    def getDeviceId(self):
        pass
    
    @final
    @args(long, [])
    def getDownTime(self):
        pass
    
    @final
    @args(int, [])
    def getEdgeFlags(self):
        pass
    
    @final
    @args(long, [])
    def getEventTime(self):
        pass
    
    @final
    @args(int, [])
    def getFlags(self):
        pass
    
    @final
    @args(float, [int, int, int])
    def getHistoricalAxisValue(self, axis, pointerIndex, position):
        pass
    
    @final
    @args(float, [int, int])
    def getHistoricalAxisValue(self, axis, position):
        pass
    
    @final
    @args(long, [int])
    def getHistoricalEventTime(self, position):
        pass
    
    @final
    @args(float, [int, int])
    def getHistoricalOrientation(self, pointerIndex, position):
        pass
    
    @final
    @args(float, [int])
    def getHistoricalOrientation(self, position):
        pass
    
    @final
    @args(void, [int, int, MotionEvent.PointerCoords])
    def getHistoricalPointerCoords(self, pointerIndex, position, outPointerCoords):
        pass
    
    @final
    @args(float, [int])
    def getHistoricalPressure(self, position):
        pass
    
    @final
    @args(float, [int, int])
    def getHistoricalPressure(self, pointerIndex, position):
        pass
    
    @final
    @args(float, [int])
    def getHistoricalSize(self, position):
        pass
    
    @final
    @args(float, [int, int])
    def getHistoricalSize(self, pointerIndex, position):
        pass
    
    @final
    @args(float, [int, int])
    def getHistoricalToolMajor(self, pointerIndex, position):
        pass
    
    @final
    @args(float, [int])
    def getHistoricalToolMajor(self, position):
        pass
    
    @final
    @args(float, [int, int])
    def getHistoricalToolMinor(self, pointerIndex, position):
        pass
    
    @final
    @args(float, [int])
    def getHistoricalToolMinor(self, position):
        pass
    
    @final
    @args(float, [int, int])
    def getHistoricalTouchMajor(self, pointerIndex, position):
        pass
    
    @final
    @args(float, [int])
    def getHistoricalTouchMajor(self, position):
        pass
    
    @final
    @args(float, [int, int])
    def getHistoricalTouchMinor(self, pointerIndex, position):
        pass
    
    @final
    @args(float, [int])
    def getHistoricalTouchMinor(self, position):
        pass
    
    @final
    @args(float, [int, int])
    def getHistoricalX(self, pointerIndex, position):
        pass
    
    @final
    @args(float, [int])
    def getHistoricalX(self, position):
        pass
    
    @final
    @args(float, [int, int])
    def getHistoricalY(self, pointerIndex, position):
        pass
    
    @final
    @args(float, [int])
    def getHistoricalY(self, position):
        pass
    
    @final
    @args(int, [])
    def getHistorySize(self):
        pass
    
    @final
    @args(int, [])
    def getMetaState(self):
        pass
    
    @final
    @args(float, [int])
    def getOrientation(self, pointerIndex):
        pass
    
    @final
    @args(float, [])
    def getOrientation(self):
        pass
    
    @final
    @args(void, [int, MotionEvent.PointerCoords])
    def getOrientation(self, pointerIndex, outPointerCoords):
        pass
    
    @final
    @args(int, [])
    def getPointerCount(self):
        pass
    
    @final
    @args(int, [int])
    def getPointerId(self, pointerIndex):
        pass
    
    @final
    @args(void, [int, MotionEvent.PointerProperties])
    def getPointerProperties(self, pointerIndex, outPointerProperties):
        pass
    
    @final
    @args(float, [])
    def getPressure(self):
        pass
    
    @final
    @args(float, [int])
    def getPressure(self, pointerIndex):
        pass
    
    @final
    @args(float, [])
    def getRawX(self):
        pass
    
    @final
    @args(float, [])
    def getRawY(self):
        pass
    
    @final
    @args(float, [int])
    def getSize(self, pointerIndex):
        pass
    
    @final
    @args(float, [])
    def getSize(self):
        pass
    
    @final
    @args(int, [])
    def getSource(self):
        pass
    
    @final
    @args(float, [])
    def getToolMajor(self):
        pass
    
    @final
    @args(float, [int])
    def getToolMajor(self, pointerIndex):
        pass
    
    @final
    @args(float, [])
    def getToolMinor(self):
        pass
    
    @final
    @args(float, [int])
    def getToolMinor(self, pointerIndex):
        pass
    
    @final
    @args(int, [int])
    def getToolType(self, pointerIndex):
        pass
    
    @final
    @args(float, [])
    def getTouchMajor(self):
        pass
    
    @final
    @args(float, [int])
    def getTouchMajor(self, pointerIndex):
        pass
    
    @final
    @args(float, [])
    def getTouchMinor(self):
        pass
    
    @final
    @args(float, [int])
    def getTouchMinor(self, pointerIndex):
        pass
    
    @final
    @args(float, [int])
    def getX(self, pointerIndex):
        pass
    
    @final
    @args(float, [])
    def getX(self):
        pass
    
    @final
    @args(float, [])
    def getXPrecision(self):
        pass
    
    @final
    @args(float, [int])
    def getY(self, pointerIndex):
        pass
    
    @final
    @args(float, [])
    def getY(self):
        pass
    
    @final
    @args(float, [])
    def getYPrecision(self):
        pass
    
    @final
    @args(bool, [int])
    def isButtonPressed(self, button):
        pass
    
    @static
    @args(MotionEvent, [long, long, int, int, [MotionEvent.PointerProperties], [MotionEvent.PointerCoords], int, int, float, float, int, int, int, int])
    def obtain(downTime, eventTime, action, pointerCount, pointerProperties, pointerCoords, metaState, buttonState, xPrecision, yPrecision, deviceId, edgeFlags, source, flags):
        pass
    
    @static
    @args(MotionEvent, [long, long, int, float, float, float, float, int, float, float, int, int, int])
    def obtain(downTime, eventTime, action, x, y, pressure, size, buttonState, xPrecision, yPrecision, deviceId, edgeFlags):
        pass
    
    @static
    @args(MotionEvent, [MotionEvent])
    def obtain(other):
        pass
    
    @static
    @args(MotionEvent, [long, long, int, float, float, int])
    def obtain(downTime, eventTime, action, x, y, metaState):
        pass
    
    @static
    @args(MotionEvent, [MotionEvent])
    def obtainNoHistory(other):
        pass
    
    @final
    @args(void, [float, float])
    def offsetLocation(self, deltaX, deltaY):
        pass
    
    @final
    def recycle(self):
        pass
    
    @final
    @args(void, [int])
    def setAction(self, action):
        pass
    
    @final
    @args(void, [int])
    def setEdgeFlags(self, flags):
        pass
    
    @final
    @args(void, [float, float])
    def setLocation(self, x, y):
        pass
    
    @final
    @args(void, [int])
    def setSource(self, source):
        pass
    
    @final
    @args(void, [Matrix])
    def transform(self, matrix):
        pass


class SubMenu:

    __interfaces__ = [Menu]
    
    @abstract
    @args(MenuItem, [])
    def getItem(self):
        pass


class Surface(Object):

    __interfaces__ = [Parcelable]
    
    __fields__ = {"CREATOR": Creator(Surface)}
    
    ROTATION_0 = 0
    ROTATION_180 = 2
    ROTATION_270 = 3
    ROTATION_90 = 1
    
    class OutOfResourcesException(RuntimeException):
    
        def __init__(self):
            pass
        
        @args(void, [String])
        def __init__(self, name):
            pass
    
    @args(void, [SurfaceTexture])
    def __init__(self, surfaceTexture):
        pass
    
    @args(bool, [])
    def isValid(self):
        pass
    
    @args(Canvas, [Rect])
    def lockCanvas(self, inOutDirty):
        pass
    
    @args(Canvas, [])
    def lockHardwareCanvas(self):
        pass
    
    def release(self):
        pass
    
    @args(void, [Canvas])
    def unlockCanvasAndPost(self, canvas):
        pass


class SurfaceHolder:

    SURFACE_TYPE_GPU = 2
    SURFACE_TYPE_HARDWARE = 1
    SURFACE_TYPE_NORMAL = 0
    SURFACE_TYPE_PUSH_BUFFERS = 3
    
    class Callback:
    
        @args(void, [SurfaceHolder, int, int, int])
        def surfaceChanged(self, holder, format, width, height):
            pass
        
        @args(void, [SurfaceHolder])
        def surfaceCreated(self, holder):
            pass
        
        @args(void, [SurfaceHolder])
        def surfaceDestroyed(self, holder):
            pass
    
    @args(void, [SurfaceHolder.Callback])
    def addCallback(self, callback):
        pass
    
    @args(void, [int])
    def setType(self, type):
        pass


class SurfaceView(View):

    @args(void, [android.content.Context])
    def __init__(self, context):
        pass
    
    @args(SurfaceHolder, [])
    def getHolder(self):
        pass


class View(Object):

    ### Only some of the many constants...
    FOCUS_BACKWARD = 1
    FOCUS_DOWN = 130
    FOCUS_FORWARD = 2
    FOCUS_LEFT = 17
    FOCUS_RIGHT = 66
    FOCUS_UP = 33
    GONE = 8
    INVISIBLE = 4
    NO_ID = -1
    TEXT_ALIGNMENT_CENTER = 4
    TEXT_ALIGNMENT_GRAVITY = 1
    TEXT_ALIGNMENT_INHERIT = 0
    TEXT_ALIGNMENT_TEXT_END = 3
    TEXT_ALIGNMENT_TEXT_START = 2
    TEXT_ALIGNMENT_VIEW_END = 6
    TEXT_ALIGNMENT_VIEW_START = 5
    VISIBLE = 0
    
    class BaseSavedState(AbsSavedState):
    
        __static_fields__ = {"CREATOR": Creator(View.BaseSavedState)}
        
        @args(void, [Parcelable])
        def __init__(self, superState):
            pass
        
        @args(void, [Parcel])
        def __init__(self, source):
            pass
        
        @args(void, [Parcel, int])
        def writeToParcel(self, dest, flags):
            pass
    
    class MeasureSpec(Object):
    
        AT_MOST = 0x80000000
        EXACTLY = 0x40000000
        UNSPECIFIED = 0
        
        def __init__(self):
            pass
        
        @static
        @args(int, [int])
        def getMode(measureSpec):
            pass
        
        @static
        @args(int, [int])
        def getSize(measureSpec):
            pass
        
        @static
        @args(int, [int, int])
        def makeMeasureSpec(size, mode):
            pass
        
        @static
        @args(String, [int])
        def toString(measureSpec):
            pass
    
    class OnAttachStateChangeListener:
    
        @args(void, [View])
        def onViewAttachedToWindow(self, view):
            pass
        
        @args(void, [View])
        def onViewDetachedFromWindow(self, view):
            pass
    
    class OnClickListener:
    
        @args(void, [View])
        def onClick(self, view):
            pass
    
    class OnCreateContextMenuListener:
    
        @args(void, [ContextMenu, View, ContextMenu.ContextMenuInfo])
        def onCreateContextMenu(self, menu, v, menuInfo):
            pass
    
    class OnFocusChangeListener:
    
        @args(void, [View, bool])
        def onFocusChange(self, view, hasFocus):
            pass
    
    class OnLayoutChangeListener:
    
        @args(void, [View, int, int, int, int, int, int, int, int])
        def onLayoutChange(self, view, left, top, right, bottom,
                           oldLeft, oldTop, oldRight, oldBottom):
            pass
    
    class OnScrollChangeListener:
    
        @args(void, [View, int, int, int, int])
        def onScrollChange(self, view, scrollX, scrollY, oldScrollX, oldScrollY):
            pass
    
    @args(void, [android.content.Context])
    def __init__(self, context):
        pass
    
    @args(void, [ArrayList(View)])
    def addChildrenForAccessibility(self, outChildren):
        pass
    
    @args(void, [ArrayList(View), int, int])
    def addFocusables(self, views, direction, focusableMode):
        pass
    
    @args(void, [ArrayList(View), int])
    def addFocusables(self, views, direction):
        pass
    
    @args(void, [View.OnAttachStateChangeListener])
    def addOnAttachStateChangeListener(self, listener):
        pass
    
    @args(void, [View.OnLayoutChangeListener])
    def addOnLayoutChangeListener(self, listener):
        pass
    
    @args(void, [ArrayList(View)])
    def addTouchables(self, views):
        pass
    
    ### ...
    
    @args(void, [CharSequence])
    def announceForAccessibility(self, text):
        pass
    
    @args(bool, [int])
    def awakenScrollBars(self, startDelay):
        pass
    
    @args(bool, [int, bool])
    def awakenScrollBars(self, startDelay, invalidate):
        pass
    
    @args(bool, [])
    def awakenScrollBars(self):
        pass
    
    def bringToFront(self):
        pass
    
    def buildDrawingCache(self):
        pass
    
    @args(void, [bool])
    def buildDrawingCache(self, autoScale):
        pass
    
    def buildLayer(self):
        pass
    
    @args(bool, [])
    def callOnClick(self):
        pass
    
    @args(bool, [])
    def canResolveLayoutDirection(self):
        pass
    
    @args(bool, [])
    def canResolveTextAlignment(self):
        pass
    
    @args(bool, [])
    def canResolveTextDirection(self):
        pass
    
    @args(bool, [int])
    def canScrollHorizontally(self, direction):
        pass
    
    @args(bool, [int])
    def canScrollVertically(self, direction):
        pass
    
    def cancelLongPress(self):
        pass
    
    @final
    def cancelPendingInputEvents(self):
        pass
    
    @args(bool, [View])
    def checkInputConnectionProxy(self, view):
        pass
    
    def clearAnimation(self):
        pass
    
    def clearFocus(self):
        pass
    
    @static
    @args(int, [int, int])
    def combineMeasuredStates(currentState, newState):
        pass
    
    @args(int, [])
    def computeHorizontalScrollExtent(self):
        pass
    
    @args(int, [])
    def computeHorizontalScrollOffset(self):
        pass
    
    @args(int, [])
    def computeHorizontalScrollRange(self):
        pass
    
    def computeScroll(self):
        pass
    
    #@args(WindowInsets, [WindowInsets, Rect])
    #def computeSystemWindowInsets(self, insets, outLocalInsets):
    #    pass
    
    @args(int, [])
    def computeVerticalScrollExtent(self):
        pass
    
    @args(int, [])
    def computeVerticalScrollOffset(self):
        pass
    
    @args(int, [])
    def computeVerticalScrollRange(self):
        pass
    
    #@args(AccessibilityNodeInfo, [])
    #def createAccessibilityNodeInfo(self):
    #    pass
    
    @args(void, [ContextMenu])
    def createContextMenu(self, menu):
        pass
    
    def destroyDrawingCache(self):
        pass
    
    #@args(WindowInsets, [WindowInsets])
    #def dispatchApplyWindowInsets(self, insets):
    #    pass
    
    #@args(void, [Configuration])
    #def dispatchConfigurationChanged(self, newConfig):
    #    pass
    
    #@args(void, [int])
    #def dispatchDisplayHint(self, hint):
    #    pass
    
    #@args(bool, [DragEvent])
    #def dispatchDragEvent(self, event):
    #    pass
    
    #@args(void, [Canvas])
    #def dispatchDraw(self, canvas):
    #    pass
    
    ### ...
    
    @args(void, [Canvas])
    def draw(self, canvas):
        pass
    
    @args(void, [float, float])
    def drawableHotspotChanged(self, x, y):
        pass
    
    @args(void, [])
    def drawableStateChanged(self):
        pass
    
    @args(View, [])
    def findFocus(self):
        pass
    
    @final
    @args(View, [int])
    def findViewById(self, id):
        pass
    
    @final
    @args(View, [Object])
    def findViewWithTag(self, tag):
        pass
    
    @final
    @args(void, [ArrayList(View), CharSequence, int])
    def findViewsWithText(self, outViews, searched, flags):
        pass
    
    @args(View, [int])
    def focusSearch(self, direction):
        pass
    
    def forceLayout(self):
        pass
    
    @static
    @args(int, [])
    def generateViewId():
        pass
    
    @args(CharSequence, [])
    def getAccessibilityClassName(self):
        pass
    
    @args(int, [])
    def getAccessibilityLiveRegion(self):
        pass
    
    ### ...
    
    @args(float, [])
    def getAlpha(self):
        pass
    
    @args(Animation, [])
    def getAnimation(self):
        pass
    
    @args(IBinder, [])
    def getApplicationWindowToken(self):
        pass
    
    @args(Drawable, [])
    def getBackground(self):
        pass
    
    @args(ColorStateList, [])
    def getBackgroundTintList(self):
        pass
    
    @args(PorterDuff.Mode, [])
    def getBackgroundTintMode(self):
        pass
    
    @args(int, [])
    def getBaseline(self):
        pass
    
    @final
    @args(int, [])
    def getBottom(self):
        pass
    
    @args(float, [])
    def getBottomFadingEdgeStrength(self):
        pass
    
    @args(int, [])
    def getBottomPaddingOffset(self):
        pass
    
    @args(float, [])
    def getCameraDistance(self):
        pass
    
    @args(Rect, [])
    def getClipBounds(self):
        pass
    
    @args(bool, [Rect])
    def getClipBounds(self, outRect):
        pass
    
    @final
    @args(bool, [])
    def getClipToOutline(self):
        pass
    
    @args(CharSequence, [])
    def getContentDescription(self):
        pass
    
    @args(android.content.Context, [])
    def getContext(self):
        pass
    
    @args(ContextMenu.ContextMenuInfo, [])
    def getContextMenuInfo(self):
        pass
    
    @static
    @args(int, [int, int])
    def getDefaultSize(size, measureSpec):
        pass
    
    @args(Display, [])
    def getDisplay(self):
        pass
    
    @final
    @args([int], [])
    def getDrawableState(self):
        pass
    
    @args(Bitmap, [bool])
    def getDrawingCache(self, autoScale):
        pass
    
    @args(Bitmap, [])
    def getDrawingCache(self):
        pass
    
    @args(int, [])
    def getDrawingCacheBackgroundColor(self):
        pass
    
    @args(int, [])
    def getDrawingCacheQuality(self):
        pass
    
    @args(void, [Rect])
    def getDrawingRect(self, outRect):
        pass
    
    @args(long, [])
    def getDrawingTime(self):
        pass
    
    @args(float, [])
    def getElevation(self):
        pass
    
    @args(bool, [])
    def getFilterTouchesWhenObscured(self):
        pass
    
    @args(bool, [])
    def getFitsSystemWindows(self):
        pass
    
    @args(ArrayList(View), [int])
    def getFocusables(self, direction):
        pass
    
    @args(void, [Rect])
    def getFocusedRect(self, r):
        pass
    
    @args(Drawable, [])
    def getForeground(self):
        pass
    
    @args(int, [])
    def getForegroundGravity(self):
        pass
    
    @args(ColorStateList, [])
    def getForegroundTintList(self):
        pass
    
    @args(PorterDuff.Mode, [])
    def getForegroundTintMode(self):
        pass
    
    @args(bool, [Rect, Point])
    def getGlobalVisibleRect(self, r, globalOffset):
        pass
    
    @final
    @args(bool, [Rect])
    def getGlobalVisibleRect(self, r):
        pass
    
    @args(Handler, [])
    def getHandler(self):
        pass
    
    @final
    @args(int, [])
    def getHeight(self):
        pass
    
    @args(void, [Rect])
    def getHitRect(self, outRect):
        pass
    
    @args(int, [])
    def getHorizontalFadingEdgeLength(self):
        pass
    
    @args(int, [])
    def getHorizontalScrollbarHeight(self):
        pass
    
    @args(int, [])
    def getId(self):
        pass
    
    @args(int, [])
    def getImportantForAccessibility(self):
        pass
    
    @args(bool, [])
    def getKeepScreenOn(self):
        pass
    
    @args(KeyEvent.DispatcherState, [])
    def getKeyDispatcherState(self):
        pass
    
    @args(int, [])
    def getLabelFor(self):
        pass
    
    @args(int, [])
    def getLayerType(self):
        pass
    
    @args(int, [])
    def getLayoutDirection(self):
        pass
    
    @args(ViewGroup.LayoutParams, [])
    def getLayoutParams(self):
        pass
    
    @final
    @args(int, [])
    def getLeft(self):
        pass
    
    @args(float, [])
    def getLeftFadingEdgeStrength(self):
        pass
    
    @args(int, [])
    def getLeftPaddingOffset(self):
        pass
    
    @final
    @args(bool, [Rect])
    def getLocalVisibleRect(self, r):
        pass
    
    @args(void, [[int]])
    def getLocationInWindow(self, location):
        pass
    
    @args(void, [[int]])
    def getLocationOnScreen(self, location):
        pass
    
    @args(Matrix, [])
    def getMatrix(self):
        pass
    
    @final
    @args(int, [])
    def getMeasuredHeight(self):
        pass
    
    @final
    @args(int, [])
    def getMeasuredHeightAndState(self):
        pass
    
    @final
    @args(int, [])
    def getMeasuredState(self):
        pass
    
    @final
    @args(int, [])
    def getMeasuredWidth(self):
        pass
    
    @final
    @args(int, [])
    def getMeasuredWidthAndState(self):
        pass
    
    @args(int, [])
    def getMinimumHeight(self):
        pass
    
    @args(int, [])
    def getMinimumWidth(self):
        pass
    
    @args(int, [])
    def getNextFocusDownId(self):
        pass
    
    @args(int, [])
    def getNextFocusForwardId(self):
        pass
    
    @args(int, [])
    def getNextFocusLeftId(self):
        pass
    
    @args(int, [])
    def getNextFocusRightId(self):
        pass
    
    @args(int, [])
    def getNextFocusUpId(self):
        pass
    
    @args(View.OnFocusChangeListener, [])
    def getOnFocusChangeListener(self):
        pass
    
    @args(ViewOutlineProvider, [])
    def getOutlineProvider(self):
        pass
    
    @args(int, [])
    def getOverScrollMode(self):
        pass
    
    @args(ViewOverlay, [])
    def getOverlay(self):
        pass
    
    @args(int, [])
    def getPaddingBottom(self):
        pass
    
    @args(int, [])
    def getPaddingEnd(self):
        pass
    
    @args(int, [])
    def getPaddingLeft(self):
        pass
    
    @args(int, [])
    def getPaddingRight(self):
        pass
    
    @args(int, [])
    def getPaddingStart(self):
        pass
    
    @args(int, [])
    def getPaddingTop(self):
        pass
    
    @final
    @args(ViewParent, [])
    def getParent(self):
        pass
    
    @args(ViewParent, [])
    def getParentForAccessibility(self):
        pass
    
    @args(float, [])
    def getPivotX(self):
        pass
    
    @args(float, [])
    def getPivotY(self):
        pass
    
    @args(Resources, [])
    def getResources(self):
        pass
    
    @final
    @args(int, [])
    def getRight(self):
        pass
    
    @args(View, [])
    def getRootView(self):
        pass
    
    @args(WindowInsets, [])
    def getRootWindowInsets(self):
        pass
     
    @args(float, [])
    def getRotation(self):
        pass
    
    @args(float, [])
    def getRotationX(self):
        pass
    
    @args(float, [])
    def getRotationY(self):
        pass
    
    @args(float, [])
    def getScaleX(self):
        pass
    
    @args(float, [])
    def getScaleY(self):
        pass
    
    @args(int, [])
    def getScrollBarDefaultDelayBeforeFade(self):
        pass
    
    @args(int, [])
    def getScrollBarFadeDuration(self):
        pass
    
    @args(int, [])
    def getScrollBarSize(self):
        pass
    
    @args(int, [])
    def getScrollBarStyle(self):
        pass
    
    @args(int, [])
    def getScrollIndicators(self):
        pass
    
    @final
    @args(int, [])
    def getScrollX(self):
        pass
    
    @final
    @args(int, [])
    def getScrollY(self):
        pass
    
    @args(int, [])
    def getSolidColor(self):
        pass
    
    @args(StateListAnimator, [])
    def getStateListAnimator(self):
        pass
    
    @args(int, [])
    def getSuggestedMinimumHeight(self):
        pass
    
    @args(int, [])
    def getSuggestedMinimumWidth(self):
        pass
    
    @args(int, [])
    def getSystemUiVisibility(self):
        pass
    
    @args(Object, [int])
    def getTag(self, key):
        pass
    
    @args(Object, [])
    def getTag(self):
        pass
    
    @args(int, [])
    def getTextAlignment(self):
        pass
    
    @args(int, [])
    def getTextDirection(self):
        pass
    
    @final
    @args(int, [])
    def getTop(self):
        pass
    
    @args(float, [])
    def getTopFadingEdgeStrength(self):
        pass
    
    @args(int, [])
    def getTopPaddingOffset(self):
        pass
    
    @args(TouchDelegate, [])
    def getTouchDelegate(self):
        pass
    
    @args(ArrayList(View), [])
    def getTouchables(self):
        pass
    
    @args(String, [])
    def getTransitionName(self):
        pass
    
    @args(float, [])
    def getTranslationX(self):
        pass
    
    @args(float, [])
    def getTranslationY(self):
        pass
    
    @args(float, [])
    def getTranslationZ(self):
        pass
    
    @args(int, [])
    def getVerticalFadingEdgeLength(self):
        pass
    
    @args(int, [])
    def getVerticalScrollbarPosition(self):
        pass
    
    @args(int, [])
    def getVerticalScrollbarWidth(self):
        pass
    
    @args(ViewTreeObserver, [])
    def getViewTreeObserver(self):
        pass
    
    @args(int, [])
    def getVisibility(self):
        pass
    
    @final
    @args(int, [])
    def getWidth(self):
        pass
    
    @args(int, [])
    def getWindowAttachCount(self):
        pass
    
    @args(WindowId, [])
    def getWindowId(self):
        pass
    
    @args(int, [])
    def getWindowSystemUiVisibility(self):
        pass
    
    @args(IBinder, [])
    def getWindowToken(self):
        pass
    
    @args(int, [])
    def getWindowVisibility(self):
        pass
    
    @args(void, [Rect])
    def getWindowVisibleDisplayFrame(self, outRect):
        pass
    
    @args(float, [])
    def getX(self):
        pass
    
    @args(float, [])
    def getY(self):
        pass
    
    @args(float, [])
    def getZ(self):
        pass
    
    @args(bool, [])
    def hasFocus(self):
        pass
    
    @args(bool, [])
    def hasFocusable(self):
        pass
    
    @args(bool, [])
    def hasNestedScrollingParent(self):
        pass
    
    @args(bool, [])
    def hasOnClickListeners(self):
        pass
    
    @args(bool, [])
    def hasOverlappingRendering(self):
        pass
    
    @args(bool, [])
    def hasTransientState(self):
        pass
    
    @args(bool, [])
    def hasWindowFocus(self):
        pass
    
    @static
    @args(View, [Context, int, ViewGroup])
    def inflate(context, resource, root):
        pass
    
    @args(void, [Rect])
    def invalidate(self, dirty):
        pass
    
    @args(void, [int, int, int, int])
    def invalidate(self, l, t, r, b):
        pass
    
    def invalidate(self):
        pass
    
    @args(void, [int, int, int, int])
    def invalidate(self, left, top, right, bottom):
        pass
    
    @args(void, [Drawable])
    def invalidateDrawable(self, drawable):
        pass
    
    @args(void, [])
    def invalidateOutline(self):
        pass
    
    @args(bool, [])
    def isAccessibilityFocused(self):
        pass
    
    @args(bool, [])
    def isActivated(self):
        pass
    
    @args(bool, [])
    def isAttachedToWindow(self):
        pass
    
    @args(bool, [])
    def isClickable(self):
        pass
    
    @args(bool, [])
    def isContextClickable(self):
        pass
    
    @args(bool, [])
    def isDirty(self):
        pass
    
    @args(bool, [])
    def isDrawingCacheEnabled(self):
        pass
    
    @args(bool, [])
    def isDuplicateParentStateEnabled(self):
        pass
    
    @args(bool, [])
    def isEnabled(self):
        pass
    
    @final
    @args(bool, [])
    def isFocusable(self):
        pass
    
    @final
    @args(bool, [])
    def isFocusableInTouchMode(self):
        pass
    
    @args(bool, [])
    def isFocused(self):
        pass
    
    @args(bool, [])
    def isHapticFeedbackEnabled(self):
        pass
    
    @args(bool, [])
    def isHardwareAccelerated(self):
        pass
    
    @args(bool, [])
    def isHorizontalFadingEdgeEnabled(self):
        pass
    
    @args(bool, [])
    def isHorizontalScrollBarEnabled(self):
        pass
    
    @args(bool, [])
    def isHovered(self):
        pass
    
    @args(bool, [])
    def isImportantForAccessibility(self):
        pass
    
    @args(bool, [])
    def isInEditMode(self):
        pass
    
    @args(bool, [])
    def isInLayout(self):
        pass
    
    @args(bool, [])
    def isInTouchMode(self):
        pass
    
    @args(bool, [])
    def isLaidOut(self):
        pass
    
    @args(bool, [])
    def isLayoutDirectionResolved(self):
        pass
    
    @args(bool, [])
    def isLayoutRequested(self):
        pass
    
    @args(bool, [])
    def isLongClickable(self):
        pass
    
    @args(bool, [])
    def isNestedScrollingEnabled(self):
        pass
    
    @args(bool, [])
    def isOpaque(self):
        pass
    
    @args(bool, [])
    def isPaddingRelative(self):
        pass
    
    @args(bool, [])
    def isPressed(self):
        pass
    
    @args(bool, [])
    def isSaveEnabled(self):
        pass
    
    @args(bool, [])
    def isSaveFromParentEnabled(self):
        pass
    
    @args(bool, [])
    def isScrollContainer(self):
        pass
    
    @args(bool, [])
    def isScrollbarFadingEnabled(self):
        pass
    
    @args(bool, [])
    def isSelected(self):
        pass
    
    @args(bool, [])
    def isShown(self):
        pass
    
    @args(bool, [])
    def isSoundEffectsEnabled(self):
        pass
    
    @args(bool, [])
    def isTextAlignmentResolved(self):
        pass
    
    @args(bool, [])
    def isTextDirectionResolved(self):
        pass
    
    @args(bool, [])
    def isVerticalFadingEdgeEnabled(self):
        pass
    
    @args(bool, [])
    def isVerticalScrollBarEnabled(self):
        pass
    
    @args(void, [])
    def jumpDrawablesToCurrentState(self):
        pass
    
    @args(void, [int, int, int, int])
    def layout(self, left, top, right, bottom):
        pass
    
    @final
    @args(void, [int, int])
    def measure(self, widthMeasureSpec, heightMeasureSpec):
        pass
    
    @args(void, [int])
    def offsetLeftAndRight(self, offset):
        pass
    
    @args(void, [int])
    def offsetTopAndBottom(self, offset):
        pass
    
    ### ...
    
    @args(void, [ContextMenu])
    def onCreateContextMenu(self, menu):
        pass
    
    ### ...
    
    @args(void, [Canvas])
    def onDraw(self, canvas):
        pass
    
    ### ...
    
    @args(void, [bool, int, int, int, int])
    def onLayout(self, changed, left, top, right, bottom):
        pass
    
    @args(void, [int, int])
    def onMeasure(self, widthMeasureSpec, heightMeasureSpec):
        pass
    
    ### ...
    
    @args(void, [int])
    def onRtlPropertiesChanged(self, layoutDirection):
        pass
    
    @args(Parcelable, [])
    def onSaveInstanceState(self):
        pass
    
    @protected
    @args(void, [int, int, int, int])
    def onScrollChanged(self, left, top, oldLeft, oldTop):
        pass
    
    ### ...
    
    @args(void, [int, int, int, int])
    def onSizeChanged(self, width, height, oldWidth, oldHeight):
        pass
    
    ### ...
    
    @args(bool, [MotionEvent])
    def onTouchEvent(self, event):
        pass
    
    ### ...
    
    @args(bool, [])
    def performClick(self):
        pass
    
    ### ...
    
    @args(void, [int, int, int, int])
    def postInvalidate(self, left, top, right, bottom):
        pass
    
    def postInvalidate(self):
        pass
    
    @args(void, [long, int, int, int, int])
    def postInvalidateDelayed(self, delayMilliseconds, left, top, right, bottom):
        pass
    
    @args(void, [long])
    def postInvalidateDelayed(self, delayMilliseconds):
        pass
    
    ### ...
    
    def refreshDrawableState(self):
        pass
    
    ### ...
    
    @args(bool, [int, Rect])
    def requestFocus(self, direction, previouslyFocusedRect):
        pass
    
    @final
    @args(bool, [int])
    def requestFocus(self, direction):
        pass
    
    @final
    @args(bool, [])
    def requestFocus(self):
        pass
    
    @final
    @args(bool, [])
    def requestFocusFromTouch(self):
        pass
    
    def requestLayout(self):
        pass
    
    ### ...
    
    @api(16)
    @args(void, [Drawable])
    def setBackground(self, background):
        pass
    
    @args(void, [int])
    def setBackgroundColor(self, color):
        pass
    
    @api(1, 16)
    @args(void, [Drawable])
    def setBackgroundDrawable(self, background):
        pass
    
    @args(void, [int])
    def setBackgroundResource(self, resid):
        pass
    
    @api(21)
    @args(void, [ColorStateList])
    def setBackgroundTintList(self, tint):
        pass
    
    @api(21)
    @args(void, [PorterDuff.Mode])
    def setBackgroundTintMode(self, tintMode):
        pass
    
    @final
    @args(void, [int])
    def setBottom(self, bottom):
        pass
    
    ### ...
    
    @args(void, [bool])
    def setEnabled(self, enabled):
        pass
    
    ### ...
    
    @args(void, [bool])
    def setFocusable(self, focusable):
        pass
    
    @args(void, [bool])
    def setFocusableInTouchMode(self, focusable):
        pass
    
    ### ...
    
    @args(void, [int])
    def setId(self, id):
        pass
    
    ### ...
    
    @args(void, [int])
    def setLabel(self, id):
        pass
    
    ### ...
    
    @args(void, [int])
    def setLayoutDirection(self, layoutDirection):
        pass
    
    @args(void, [ViewGroup.LayoutParams])
    def setLayoutParams(self, params):
        pass
    
    @final
    @args(void, [int])
    def setLeft(self, left):
        pass
    
    ### ...
    
    @args(void, [int, int])
    def setMeasuredDimension(self, measuredWidth, measuredHeight):
        pass
    
    ### ...
    
    @args(void, [int])
    def setMinimumHeight(self, minHeight):
        pass
    
    @args(void, [int])
    def setMinimumWidth(self, minWidth):
        pass
    
    ### ...
    
    @args(void, [View.OnCreateContextMenuListener])
    def setOnCreateContextMenuListener(self, l):
        pass
    
    ### ...
    
    @args(void, [View.OnScrollChangeListener])
    def setOnScrollChangeListener(self, listener):
        pass
    
    ### ...
    
    @args(void, [int, int, int, int])
    def setPadding(self, left, top, right, bottom):
        pass
    
    @args(void, [int, int, int, int])
    def setPaddingRelative(self, left, top, right, bottom):
        pass
    
    ### ...
    
    @args(void, [View.OnClickListener])
    def setOnClickListener(self, listener):
        pass
    
    ### ...
    
    @args(void, [View.OnFocusChangeListener])
    def setOnFocusChangeListener(self, listener):
        pass
    
    ### ...
    
    @final
    @args(void, [int])
    def setRight(self, right):
        pass
    
    ### ...
    
    @args(void, [int, Object])
    def setTag(self, key, tag):
        pass
    
    @args(void, [Object])
    def setTag(self, tag):
        pass
    
    @api(17)
    @args(void, [int])
    def setTextAlignment(self, alignment):
        pass
    
    @args(void, [int])
    def setTextDirection(self, direction):
        pass
    
    @final
    @args(void, [int])
    def setTop(self, top):
        pass
    
    ### ...
    
    @args(bool, [])
    def showContextMenu(self):
        pass
    
    ### ...


class ViewGroup(View):

    CLIP_TO_PADDING_MASK        = 0x00022
    FOCUS_AFTER_DESCENDANTS     = 0x40000
    FOCUS_BEFORE_DESCENDANTS    = 0x20000
    FOCUS_BLOCK_DESCENDANTS     = 0x60000
    LAYOUT_MODE_CLIP_BOUNDS     = 0
    LAYOUT_MODE_OPTICAL_BOUNDS  = 1
    PERSISTENT_ALL_CACHES       = 3
    PERSISTENT_ANIMATION_CACHE  = 1
    PERSISTENT_NO_CACHE         = 0
    PERSISTENT_SCROLLING_CACHE  = 2
    
    class LayoutParams:
    
        MATCH_PARENT = -1
        WRAP_CONTENT = -2
        
        __fields__ = {"height": int, "width": int}
        
        @args(void, [int, int])
        def __init__(self, width, height):
            pass
        
        @args(void, [int])
        def resolveLayoutDirection(self, layoutDirection):
            pass
    
    class MarginLayoutParams(ViewGroup.LayoutParams):
    
        __fields__ = {
            "bottomMargin": int,
            "leftMargin": int,
            "rightMargin": int,
            "topMargin": int
            }
        
        @args(void, [int, int])
        def __init__(self, width, height):
            pass
        
        @args(void, [ViewGroup.MarginLayoutParams])
        def __init__(self, source):
            pass
        
        @args(void, [ViewGroup.LayoutParams])
        def __init__(self, source):
            pass
        
        @args(int, [])
        def getLayoutDirection(self):
            pass
        
        @args(int, [])
        def getMarginEnd(self):
            pass
        
        @args(int, [])
        def getMarginStart(self):
            pass
        
        @args(bool, [])
        def isMarginRelative(self):
            pass
        
        @args(void, [int])
        def setLayoutDirection(self, layoutDirection):
            pass
        
        @args(void, [int])
        def setMarginEnd(self, end):
            pass
        
        @args(void, [int])
        def setMarginStart(self, start):
            pass
        
        @args(int, [])
        def getMarginEnd(self):
            pass
        
        @args(void, [int, int, int, int])
        def setMargins(self, left, top, right, bottom):
            pass
    
    class OnHierarchyChangeListener:
    
        @args(void, [View, View])
        def onChildViewAdded(self, parent, child):
            pass
        
        @args(void, [View, View])
        def onChildViewRemoved(self, parent, child):
            pass
    
    @args(void, [android.content.Context])
    def __init__(self, context):
        pass
    
    @args(void, [View, int, ViewGroup.LayoutParams])
    def addView(self, child, index, params):
        pass
    
    @args(void, [View, ViewGroup.LayoutParams])
    def addView(self, child, params):
        pass
    
    @args(void, [View, int])
    def addView(self, child, index):
        pass
    
    @args(void, [View])
    def addView(self, child):
        pass
    
    @args(void, [View, int, int])
    def addView(self, child, width, height):
        pass
    
    ### ...
    
    @args(View, [int])
    def getChildAt(self, index):
        pass
    
    @args(int, [])
    def getChildCount(self):
        pass
    
    ### ...
    
    @final
    @args(void, [int, int, int, int])
    def layout(self, left, top, right, bottom):
        pass
    
    ### ...
    
    @args(bool, [MotionEvent])
    def onInterceptTouchEvent(self, event):
        pass
    
    ### ...
    
    @args(bool, [MotionEvent])
    def onTouchEvent(self, event):
        pass
    
    ### ...
    
    def removeAllViews(self):
        pass
    
    @args(void, [View])
    def removeView(self, child):
        pass
    
    @args(void, [int])
    def removeViewAt(self, index):
        pass
    
    ### ...
    
    @args(void, [View, View])
    def requestChildFocus(self, child, focused):
        pass
    
    @args(bool, [View, Rect, bool])
    def requestChildRectangleOnScreen(self, child, rectangle, immediate):
        pass
    
    @args(void, [bool])
    def requestDisallowInterceptTouchEvent(self, disallowIntercept):
        pass
    
    def requestLayout(self):
        pass
    
    ### ...


class ViewParent:

    @args(void, [View])
    def bringChildToFront(self, child):
        pass
    
    @args(bool, [])
    def canResolveLayoutDirection(self):
        pass
    
    @args(bool, [])
    def canResolveTextAlignment(self):
        pass
    
    @args(bool, [])
    def canResolveTextDirection(self):
        pass
    
    @args(void, [View])
    def childDrawableStateChanged(self, child):
        pass
    
    @args(void, [View, bool])
    def childHasTransientStateChanged(self, child, hasTransientState):
        pass
    
    @args(void, [View])
    def clearChildFocus(self, child):
        pass
    
    @args(void, [ContextMenu])
    def createContextMenu(self, menu):
        pass
    
    @args(View, [View, int])
    def focusSearch(self, v, direction):
        pass
    
    @args(void, [View])
    def focusableViewAvailable(self, v):
        pass
    
    @args(bool, [View, Rect, Point])
    def getChildVisibleRect(self, child, r, offset):
        pass
    
    @args(int, [])
    def getLayoutDirection(self):
        pass
    
    @args(ViewParent, [])
    def getParent(self):
        pass
    
    @args(ViewParent, [])
    def getParentForAccessibility(self):
        pass
    
    @args(int, [])
    def getTextAlignment(self):
        pass
    
    @args(int, [])
    def getTextDirection(self):
        pass
    
    @args(void, [View, Rect])
    def invalidateChild(self, child, r):
        pass
    
    @args(ViewParent, [[int], Rect])
    def invalidateChildInParent(self, location, r):
        pass
    
    @args(bool, [])
    def isLayoutDirectionResolved(self):
        pass
    
    @args(bool, [])
    def isLayoutRequested(self):
        pass
    
    @args(bool, [])
    def isTextAlignmentResolved(self):
        pass
    
    @args(bool, [])
    def isTextDirectionResolved(self):
        pass
    
    @args(void, [View, View, int])
    def notifySubtreeAccessibilityStateChanged(self, child, source, changeType):
        pass
    
    @args(boolean, [View, float, float, bool])
    def onNestedFling(self, target, velocityX, velocityY, consumed):
        pass
    
    @args(boolean, [View, float, float])
    def onNestedPreFling(self, target, velocityX, velocityY):
        pass
    
    @args(boolean, [View, int, Bundle])
    def onNestedPrePerformAccessibilityAction(self, target, action, arguments):
        pass
    
    @args(void, [View, int, int, [int]])
    def onNestedPreScroll(self, target, dx, dy, consumed):
        pass
    
    @args(void, [View, int, int, int, int])
    def onNestedScroll(self, target, dxConsumed, dyConsumed, dxUnconsumed, dyUnconsumed):
        pass
    
    @args(void, [View, View, int])
    def onNestedScrollAccepted(self, child, target, nestedScrollAxes):
        pass
    
    @args(boolean, [View, View, int])
    def onStartNestedScroll(self, child, target, nestedScrollAxes):
        pass
    
    @args(void, [View])
    def onStopNestedScroll(self, target):
        pass
    
    @args(void, [View])
    def recomputeViewAttributes(self, child):
        pass
    
    @args(void, [View, View])
    def requestChildFocus(self, child, focused):
        pass
    
    @args(boolean, [View, Rect, bool])
    def requestChildRectangleOnScreen(self, child, rectangle, immediate):
        pass
    
    @args(void, [bool])
    def requestDisallowInterceptTouchEvent(self, disallowIntercept):
        pass
    
    def requestFitSystemWindows(self):
        pass
    
    def requestLayout(self):
        pass
    
    @args(bool, [View, AccessibilityEvent])
    def requestSendAccessibilityEvent(self, child, event):
        pass
    
    @args(void, [View])
    def requestTransparentRegion(self, child):
        pass
    
    @args(boolean, [View])
    def showContextMenuForChild(self, originalView):
        pass
    
    @args(ActionMode, [View, ActionMode.Callback, int])
    def startActionModeForChild(self, originalView, callback, type):
        pass
    
    @args(ActionMode, [View, ActionMode.Callback])
    def startActionModeForChild(self, originalView, callback):
        pass


class ViewTreeObserver(Object):

    class OnDrawListener:
    
        def onDraw(self):
            pass
    
    class OnGlobalFocusChangeListener:
    
        @args(void, [View, View])
        def onGlobalFocusChanged(self, oldFocus, newFocus):
            pass
    
    class OnGlobalLayoutListener:
    
        def onGlobalLayout(self):
            pass
    
    class OnPreDrawListener:
    
        def onPreDraw(self):
            pass
    
    class OnScrollChangedListener:
    
        def onScrollChanged(self):
            pass
    
    class OnTouchModeChangeListener:
    
        @args(void, [bool])
        def onTouchModeChanged(self, isInTouchMode):
            pass
    
    class OnWindowAttachListener:
    
        def onWindowAttached(self):
            pass
        
        def onWindowDetached(self):
            pass
    
    class OnWindowFocusChangeListener:
    
        @args(void, [bool])
        def onWindowFocusChanged(self, hasFocus):
            pass
    
    @args(void, [ViewTreeObserver.OnDrawListener])
    def addOnDrawListener(self, listener):
        pass
    
    @args(void, [ViewTreeObserver.OnGlobalFocusChangeListener])
    def addOnGlobalFocusChangeListener(self, listener):
        pass
    
    @args(void, [ViewTreeObserver.OnGlobalLayoutListener])
    def addOnGlobalLayoutListener(self, listener):
        pass
    
    @args(void, [ViewTreeObserver.OnPreDrawListener])
    def addOnPreDrawListener(self, listener):
        pass
    
    @args(void, [ViewTreeObserver.OnScrollChangedListener])
    def addOnScrollChangedListener(self, listener):
        pass
    
    @args(void, [ViewTreeObserver.OnTouchModeChangeListener])
    def addOnTouchModeChangeListener(self, listener):
        pass
    
    @args(void, [ViewTreeObserver.OnWindowAttachListener])
    def addOnWindowAttachListener(self, listener):
        pass
    
    @args(void, [ViewTreeObserver.OnWindowFocusChangeListener])
    def addOnWindowFocusChangeListener(self, listener):
        pass
    
    @final
    @args(void, [])
    def dispatchOnDraw(self):
        pass
    
    @final
    @args(void, [])
    def dispatchOnGlobalLayout(self):
        pass
    
    @final
    @args(void, [])
    def dispatchOnPreDraw(self):
        pass
    
    @args(bool, [])
    def isAlive(self):
        pass
    
    ### Deprecated in API level 16
    @args(void, [ViewTreeObserver.OnGlobalLayoutListener])
    def removeGlobalOnLayoutListener(self, victim):
        pass
    
    @args(void, [ViewTreeObserver.OnDrawListener])
    def removeOnDrawListener(self, victim):
        pass
    
    @args(void, [ViewTreeObserver.OnGlobalFocusChangeListener])
    def removeOnGlobalFocusChangeListener(self, victim):
        pass
    
    @args(void, [ViewTreeObserver.OnGlobalLayoutListener])
    def removeOnGlobalLayoutListener(self, victim):
        pass
    
    @args(void, [ViewTreeObserver.OnPreDrawListener])
    def removeOnPreDrawListener(self, victim):
        pass
    
    @args(void, [ViewTreeObserver.OnScrollChangedListener])
    def removeOnScrollChangedListener(self, victim):
        pass
    
    @args(void, [ViewTreeObserver.OnTouchModeChangeListener])
    def removeOnTouchModeChangeListener(self, victim):
        pass
    
    @args(void, [ViewTreeObserver.OnWindowAttachListener])
    def removeOnWindowAttachListener(self, victim):
        pass
    
    @args(void, [ViewTreeObserver.OnWindowFocusChangeListener])
    def removeOnWindowFocusChangeListener(self, victim):
        pass


class Window(Object):

    DEFAULT_FEATURES                = 65
    FEATURE_ACTION_BAR              = 8
    FEATURE_ACTION_BAR_OVERLAY      = 9
    FEATURE_ACTION_MODE_OVERLAY     = 10
    FEATURE_CONTEXT_MENU            = 6
    FEATURE_CUSTOM_TITLE            = 7
    FEATURE_INDETERMINATE_PROGRESS  = 5
    FEATURE_LEFT_ICON               = 3
    FEATURE_NO_TITLE                = 1
    FEATURE_OPTIONS_PANEL           = 0
    FEATURE_PROGRESS                = 2
    FEATURE_RIGHT_ICON              = 4
    ID_ANDROID_CONTENT              = 0x01020002
    PROGRESS_END                    = 10000
    PROGRESS_INDETERMINATE_OFF      = -4
    PROGRESS_INDETERMINATE_ON       = -3
    PROGRESS_SECONDARY_END          = 30000
    PROGRESS_SECONDARY_START        = 20000
    PROGRESS_START                  = 0
    PROGRESS_VISIBILITY_OFF         = -2
    PROGRESS_VISIBILITY_ON          = -1
    
    class Callback:
    
        ### Handlers are yet to be defined.
        pass
    
    def __init__(self):
        pass
    
    @abstract
    @args(void, [View, ViewGroup.LayoutParams])
    def addContentView(self, view, params):
        pass
    
    @args(void, [int])
    def addFlags(self, flags):
        pass
    
    @args(void, [int])
    def clearFlags(self, flags):
        pass
    
    @abstract
    @args(void, [])
    def clearAllPanels(self):
        pass
    
    @abstract
    @args(void, [int])
    def closePanel(self, featureId):
        pass
    
    @args(View, [int])
    def findViewById(self, id):
        pass
    
    @args(WindowManager.LayoutParams, [])
    def getAttributes(self):
        pass
    
    @args(Window.Callback, [])
    def getCallback(self):
        pass
    
    @args(Window, [])
    def getContainer(self):
        pass
    
    @args(Context, [])
    def getContext(self):
        pass
    
    @abstract
    @args(View, [])
    def getCurrentFocus(self):
        pass

    @abstract
    @args(View, [])
    def getDecorView(self):
        pass
    
    @protected
    @args(int, [])
    def getFeatures(self):
        pass
    
    @args(bool, [])
    def hasChildren(self):
        pass
    
    @args(bool, [int])
    def hasFeature(self, feature):
        pass
    
    @args(bool, [])
    def isActive(self):
        pass
    
    @abstract
    @args(bool, [])
    def isFloating(self):
        pass
    
    def makeActive(self):
        pass
    
    @abstract
    @args(void, [Configuration])
    def onConfigurationChanged(self, newConfig):
        pass
    
    @args(bool, [int])
    def requestFeature(self, featureId):
        pass
    
    @abstract
    @args(void, [Bundle])
    def restoreHierarchyState(self, savedInstanceState):
        pass
    
    @abstract
    @args(Bundle, [])
    def saveHierarchyState(self):
        pass
    
    @args(void, [Window])
    def setContainer(self, container):
        pass
    
    @abstract
    @args(void, [View])
    def setContentView(self, view):
        pass
    
    @abstract
    @args(void, [int])
    def setContentView(self, layoutResID):
        pass
    
    @abstract
    @args(void, [View, ViewGroup.LayoutParams])
    def setContentView(self, view, params):
        pass
    
    @args(void, [int])
    def setFlags(self, flags):
        pass
    
    @args(void, [int])
    def setFormat(self, format):
        pass
    
    @args(void, [int])
    def setGravity(self, gravity):
        pass
    
    @args(void, [int])
    def setIcon(self, resId):
        pass
    
    @args(void, [int, int])
    def setLayout(self, width, height):
        pass
    
    @args(void, [int])
    def setLogo(self, resId):
        pass
    
    @abstract
    @args(void, [CharSequence])
    def setTitle(self, title):
        pass
    
    @abstract
    @args(void, [int])
    def setTitleColor(self, textColor):
        pass


class WindowManager:

    class LayoutParams(ViewGroup.LayoutParams):
    
        __interfaces__ = [Parcelable]
        
        __static_fields__ = {
            "CREATOR": Parcelable.Creator(WindowManager.LayoutParams)
            }
        
        __fields__ = {
            "alpha": float,
            "buttonBrightness": float,
            "dimAmount": float,
            "flags": int,
            "format": int,
            "gravity": int,
            "horizontalMargin": float,
            "horizontalWeight": float,
            "memoryType": int,
            "packageName": String,
            "preferredDisplayModeId": int,
            "preferredRefreshRate": float,
            "rotationAnimation": int,
            "screenBrightness": float,
            "screenOrientation": int,
            "softInputMode": int,
            "systemUiVisibility": int,
            "token": IBinder,
            "type": int,
            "verticalMargin": float,
            "verticalWeight": float,
            "windowAnimations": int,
            "x": int,
            "y": int
            }
        
        ALPHA_CHANGED = 128
        ANIMATION_CHANGED = 16
        BRIGHTNESS_OVERRIDE_FULL = 1.0
        BRIGHTNESS_OVERRIDE_NONE = -1.0
        BRIGHTNESS_OVERRIDE_OFF = 0.0
        DIM_AMOUNT_CHANGED = 32
        FIRST_APPLICATION_WINDOW = 1
        FIRST_SUB_WINDOW = 1000
        FIRST_SYSTEM_WINDOW = 2000
        FLAGS_CHANGED = 4
        FLAG_ALLOW_LOCK_WHILE_SCREEN_ON   = 0x00000001
        FLAG_ALT_FOCUSABLE_IM             = 0x00020000
        FLAG_BLUR_BEHIND                  = 0x00000004
        FLAG_DIM_BEHIND                   = 0x00000002
        FLAG_DISMISS_KEYGUARD             = 0x00400000
        FLAG_DITHER                       = 0x00001000
        FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS = 0x80000000
        FLAG_FORCE_NOT_FULLSCREEN         = 0x00000800
        FLAG_FULLSCREEN                   = 0x00000400
        FLAG_HARDWARE_ACCELERATED         = 0x01000000
        FLAG_IGNORE_CHEEK_PRESSES         = 0x00008000
        FLAG_KEEP_SCREEN_ON               = 0x00000080
        FLAG_LAYOUT_ATTACHED_IN_DECOR     = 0x40000000
        FLAG_LAYOUT_INSET_DECOR           = 0x00010000
        FLAG_LAYOUT_IN_OVERSCAN           = 0x02000000
        FLAG_LAYOUT_IN_SCREEN             = 0x00000100
        FLAG_LAYOUT_NO_LIMITS             = 0x00000200
        FLAG_LOCAL_FOCUS_MODE             = 0x10000000
        FLAG_NOT_FOCUSABLE                = 0x00000008
        FLAG_NOT_TOUCHABLE                = 0x00000010
        FLAG_NOT_TOUCH_MODAL              = 0x00000020
        FLAG_SCALED                       = 0x00004000
        FLAG_SECURE                       = 0x00002000
        FLAG_SHOW_WALLPAPER               = 0x00100000
        FLAG_SHOW_WHEN_LOCKED             = 0x00080000
        FLAG_SPLIT_TOUCH                  = 0x00800000
        FLAG_TOUCHABLE_WHEN_WAKING        = 0x00000040
        FLAG_TRANSLUCENT_NAVIGATION       = 0x08000000
        FLAG_TRANSLUCENT_STATUS           = 0x04000000
        FLAG_TURN_SCREEN_ON               = 0x00200000
        FLAG_WATCH_OUTSIDE_TOUCH          = 0x00040000
        FORMAT_CHANGED = 8
        LAST_APPLICATION_WINDOW = 99
        LAST_SUB_WINDOW = 1999
        LAST_SYSTEM_WINDOW = 2999
        LAYOUT_CHANGED = 1
        MEMORY_TYPE_CHANGED = 256
        MEMORY_TYPE_GPU = 2
        MEMORY_TYPE_HARDWARE = 1
        MEMORY_TYPE_NORMAL = 0
        MEMORY_TYPE_PUSH_BUFFERS = 3
        ROTATION_ANIMATION_CHANGED   = 0x1000
        ROTATION_ANIMATION_CROSSFADE = 0x0001
        ROTATION_ANIMATION_JUMPCUT   = 0x0002
        ROTATION_ANIMATION_ROTATE    = 0x0000
        SCREEN_BRIGHTNESS_CHANGED    = 0x0800
        SCREEN_ORIENTATION_CHANGED   = 0x0400
        SOFT_INPUT_ADJUST_NOTHING        = 0x030
        SOFT_INPUT_ADJUST_PAN            = 0x020
        SOFT_INPUT_ADJUST_RESIZE         = 0x010
        SOFT_INPUT_ADJUST_UNSPECIFIED    = 0x000
        SOFT_INPUT_IS_FORWARD_NAVIGATION = 0x100
        SOFT_INPUT_MASK_ADJUST           = 0x0f0
        SOFT_INPUT_MASK_STATE            = 0x00f
        SOFT_INPUT_MODE_CHANGED          = 0x200
        SOFT_INPUT_STATE_ALWAYS_HIDDEN   = 0x003
        SOFT_INPUT_STATE_ALWAYS_VISIBLE  = 0x005
        SOFT_INPUT_STATE_HIDDEN          = 0x002
        SOFT_INPUT_STATE_UNCHANGED       = 0x001
        SOFT_INPUT_STATE_UNSPECIFIED     = 0x000
        SOFT_INPUT_STATE_VISIBLE         = 0x004
        TITLE_CHANGED = 64
        TYPE_ACCESSIBILITY_OVERLAY = 2032
        TYPE_APPLICATION = 2
        TYPE_APPLICATION_ATTACHED_DIALOG = 1003
        TYPE_APPLICATION_MEDIA = 1001
        TYPE_APPLICATION_PANEL = 1000
        TYPE_APPLICATION_STARTING = 3
        TYPE_APPLICATION_SUB_PANEL = 1002
        TYPE_BASE_APPLICATION = 1
        TYPE_CHANGED = 2
        TYPE_INPUT_METHOD = 2011
        TYPE_INPUT_METHOD_DIALOG = 2012
        TYPE_KEYGUARD_DIALOG = 2009
        TYPE_PHONE = 2002
        TYPE_PRIORITY_PHONE = 2007
        TYPE_PRIVATE_PRESENTATION = 2030
        TYPE_SEARCH_BAR = 2001
        TYPE_STATUS_BAR = 2000
        TYPE_STATUS_BAR_PANEL = 2014
        TYPE_SYSTEM_ALERT = 2003
        TYPE_SYSTEM_DIALOG = 2008
        TYPE_SYSTEM_ERROR = 2010
        TYPE_SYSTEM_OVERLAY = 2006
        TYPE_TOAST = 2005
        TYPE_WALLPAPER = 2013
        
        def __init__(self):
            pass
        
        @args(void, [int])
        def __init__(self, type):
            pass
        
        @args(void, [int, int])
        def __init__(self, type, flags):
            pass
        
        @args(void, [int, int, int])
        def __init__(self, type, flags, format):
            pass
        
        @args(void, [int, int, int, int])
        def __init__(self, width, height, flags, format):
            pass
        
        @args(void, [int, int, int, int, int, int])
        def __init__(self, width, height, xpos, ypos, flags, format):
            pass
        
        @args(void, [Parcel])
        def __init__(self, input):
            pass
        
        @final
        @args(int, [WindowManager.LayoutParams])
        def copyFrom(self, other):
            pass
        
        @args(String, [String])
        def debug(self, output):
            pass
        
        @args(int, [])
        def describeContents(self):
            pass
        
        @final
        @args(CharSequence, [])
        def getTitle(self):
            pass
        
        @static
        @args(bool, [int])
        def mayUseInputMethod(self, flags):
            pass
        
        @final
        @args(void, [CharSequence])
        def setTitle(self, title):
            pass
        
        @args(String, [])
        def toString(self):
            pass
        
        @args(void, [Parcel, int])
        def writeToParcel(self, out, parcelableFlags):
            pass
    
    @abstract
    @args(Display, [])
    def getDefaultDisplay(self):
        pass
    
    @abstract
    @args(void, [View])
    def removeViewImmediate(self, view):
        pass
