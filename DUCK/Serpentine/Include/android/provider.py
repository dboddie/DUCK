# Class declarations obtained from Android API documentation licensed under the
# Apache 2.0 license (http://www.apache.org/licenses/LICENSE-2.0) and rewritten
# in syntax that the Python compiler module can parse.

__package__ = "android.provider"

from java.io import File
from java.lang import Object, String
from java.util import List, Set
from android.content import ContentResolver, Context
from android.net import Uri
from android.os import Parcel, Parcelable
from android.util import AndroidException

class BaseColumns:

    _COUNT = "_count"
    _ID = "_id"


class CalendarContract(Object):

    ACCOUNT_TYPE_LOCAL = "LOCAL"
    ACTION_EVENT_REMINDER = "android.intent.action.EVENT_REMINDER"
    ACTION_HANDLE_CUSTOM_EVENT = "android.provider.calendar.action.HANDLE_CUSTOM_EVENT"
    AUTHORITY = "com.android.calendar"
    CALLER_IS_SYNCADAPTER = "caller_is_syncadapter"
    EXTRA_CUSTOM_APP_URI = "customAppUri"
    EXTRA_EVENT_ALL_DAY = "allDay"
    EXTRA_EVENT_BEGIN_TIME = "beginTime"
    EXTRA_EVENT_END_TIME = "endTime"
    
    __fields__ = {
        "CONTENT_URI": Uri
        }
    
    class CalendarColumns:
    
        ALLOWED_ATTENDEE_TYPES = "allowedAttendeeTypes"
        ALLOWED_AVAILABILITY = "allowedAvailability"
        ALLOWED_REMINDERS = "allowedReminders"
        CALENDAR_ACCESS_LEVEL = "calendar_access_level"
        CALENDAR_COLOR = "calendar_color"
        CALENDAR_COLOR_KEY = "calendar_color_index"
        CALENDAR_DISPLAY_NAME = "calendar_displayName"
        CALENDAR_TIME_ZONE = "calendar_timezone"
        CAL_ACCESS_CONTRIBUTOR = 500
        CAL_ACCESS_EDITOR = 600
        CAL_ACCESS_FREEBUSY = 100
        CAL_ACCESS_NONE = 0
        CAL_ACCESS_OVERRIDE = 400
        CAL_ACCESS_OWNER = 700
        CAL_ACCESS_READ = 200
        CAL_ACCESS_RESPOND = 300
        CAL_ACCESS_ROOT = 800
        CAN_MODIFY_TIME_ZONE = "canModifyTimeZone"
        CAN_ORGANIZER_RESPOND = "canOrganizerRespond"
        IS_PRIMARY = "isPrimary"
        MAX_REMINDERS = "maxReminders"
        OWNER_ACCOUNT = "ownerAccount"
        SYNC_EVENTS = "sync_events"
        VISIBLE = "visible"
    
    class Calendars(Object):
    
        CALENDAR_LOCATION = "calendar_location"
        DEFAULT_SORT_ORDER = "calendar_displayName"
        NAME = "name"
    
        __interfaces__ = [BaseColumns, CalendarContract.SyncColumns, CalendarContract.CalendarColumns]
        __fields__ = {
            "CONTENT_URI": Uri
            }
    
    class CalendarSyncColumns:
    
        pass
    
    class Events(Object):
    
        __interfaces__ = [BaseColumns] # ...
        __fields__ = {
            "CONTENT_EXCEPTION_URI": Uri,
            "CONTENT_URI": Uri
            }
    
    class SyncColumns:
    
        __interfaces__ = [CalendarContract.CalendarSyncColumns]
        
        ACCOUNT_NAME = "account_name"
        ACCOUNT_TYPE = "account_type"
        CAN_PARTIALLY_UPDATE = "canPartiallyUpdate"
        DELETED = "deleted"
        DIRTY = "dirty"
        MUTATORS = "mutators"
        _SYNC_ID = "_sync_id"


class CallLog(Object):

    AUTHORITY = "call_log"
    
    __fields__ = {
        "CONTENT_URI": Uri
        }
    
    class Calls(Object):
    
        CACHED_FORMATTED_NUMBER = "formatted_number" 
        CACHED_LOOKUP_URI =  "lookup_uri" 
        CACHED_MATCHED_NUMBER =  "matched_number"
        CACHED_NAME = "name"
        CACHED_NORMALIZED_NUMBER = "normalized_number"
        CACHED_NUMBER_LABEL = "numberlabel"
        CACHED_NUMBER_TYPE = "numbertype"
        CACHED_PHOTO_ID = "photo_id"
        CACHED_PHOTO_URI = "photo_uri"
        CONTENT_ITEM_TYPE = "vnd.android.cursor.item/calls"
        CONTENT_TYPE = "vnd.android.cursor.dir/calls"
        COUNTRY_ISO = "countryiso"
        DATA_USAGE = "data_usage"
        DATE = "date"
        DEFAULT_SORT_ORDER = "date DESC"
        DURATION = "duration"
        EXTRA_CALL_TYPE_FILTER = "android.provider.extra.CALL_TYPE_FILTER"
        FEATURES = "features"
        FEATURES_VIDEO = 1
        GEOCODED_LOCATION = "geocoded_location"
        INCOMING_TYPE = 1
        IS_READ = "is_read"
        LIMIT_PARAM_KEY = "limit"
        MISSED_TYPE = 3
        NEW = "new"
        NUMBER = "number"
        NUMBER_PRESENTATION = "presentation"
        OFFSET_PARAM_KEY = "offset"
        OUTGOING_TYPE = 2
        PHONE_ACCOUNT_COMPONENT_NAME = "subscription_component_name"
        PHONE_ACCOUNT_ID = "subscription_id"
        PRESENTATION_ALLOWED = 1
        PRESENTATION_PAYPHONE = 4
        PRESENTATION_RESTRICTED = 2
        PRESENTATION_UNKNOWN = 3
        TRANSCRIPTION = "transcription"
        TYPE = "type"
        VOICEMAIL_TYPE = 4
        VOICEMAIL_URI = "voicemail_uri"
        
        __interfaces__ = [BaseColumns]
        __fields__ = {
            "CONTENT_FILTER_URI": Uri,
            "CONTENT_URI": Uri,
            "CONTENT_URI_WITH_VOICEMAIL": Uri
            }
        
        def __init__(self):
            pass
        
        @static
        @args(String, [Context])
        def getLastOutgoingCall(context):
            pass
    
    def __init__(self):
        pass


class ContactsContract(Object):

    class Contacts(Object):
    
        __fields__ = {
            "CONTENT_LOOKUP_URI": Uri,
            "CONTENT_URI": Uri
            }
    
    class ContactsColumns(Object):
    
        CONTACT_LAST_UPDATED_TIMESTAMP = "contact_last_updated_timestamp"
        DISPLAY_NAME = "display_name"
        HAS_PHONE_NUMBER = "has_phone_number"
        IN_DEFAULT_DIRECTORY = "in_default_directory"
        IN_VISIBLE_GROUP = "in_visible_group"
        IS_USER_PROFILE = "is_user_profile"
        LOOKUP_KEY = "lookup"
        NAME_RAW_CONTACT_ID = "name_raw_contact_id"
        PHOTO_FILE_ID = "photo_file_id"
        PHOTO_ID = "photo_id"
        PHOTO_THUMBNAIL_URI = "photo_thumb_uri"
        PHOTO_URI = "photo_uri"
    
    class CommonDataKinds(Object):
    
        class Organization(Object):
        
            CONTENT_ITEM_TYPE = "vnd.android.cursor.item/organization"
            COMPANY = "data1"
        
        class Phone(Object):
        
            CONTENT_ITEM_TYPE = "vnd.android.cursor.item/phone_v2"
            NORMALIZED_NUMBER = "data4"
            NUMBER = "data1"
    
    class Data(Object):
    
        __interfaces__ = [ContactsContract.DataColumnsWithJoins]
        
        __fields__ = {
            "CONTENT_URI": Uri
            }
    
    class DataColumns:
    
        MIMETYPE = "mimetype"
    
    class DataColumnsWithJoins:
    
        __interfaces__ = [ContactsContract.DataColumns]
    
    class DisplayNameSources:
    
        EMAIL = 10
        NICKNAME = 35
        ORGANIZATION = 30
        PHONE = 20
        STRUCTURED_NAME = 40
        STRUCTURED_PHONETIC_NAME = 37
        UNDEFINED = 0


class MediaStore(Object):

    ACTION_IMAGE_CAPTURE = "android.media.action.IMAGE_CAPTURE"
    ACTION_IMAGE_CAPTURE_SECURE = "android.media.action.IMAGE_CAPTURE_SECURE"
    EXTRA_OUTPUT = "output"
    
    class Audio(Object):
    
        pass
    
    class Files(Object):
    
        pass
    
    class Images(Object):
    
        class Media(Object):
        
            CONTENT_TYPE = "vnd.android.cursor.dir/image"
            DEFAULT_SORT_ORDER = "bucket_display_name"
            
            __fields__ = {
                "EXTERNAL_CONTENT_URI": Uri,
                "INTERNAL_CONTENT_URI": Uri
                }
            
            def __init__(self):
                pass
            
            @final
            @static
            @args(Bitmap, [ContentResolver, Uri])
            def getBitmap(cr, url):
                pass
            
            @static
            @args(Uri, [String])
            def getContentUri(volumeName):
                pass
            
            @final
            @static
            @args(String, [ContentResolver, String, String, String])
            def insertImage(cr, imagePath, name, description):
                pass
            
            @final
            @static
            @args(String, [ContentResolver, Bitmap, String, String])
            def insertImage(cr, source, title, description):
                pass
            
            @final
            @static
            @args(Cursor, [ContentResolver, Uri, [String], String, String])
            def query(cr, uri, projection, where, orderBy):
                pass
            
            @final
            @static
            @args(Cursor, [ContentResolver, Uri, [String], String, [String], String])
            def query(cr, uri, projection, selection, selectionArgs, orderBy):
                pass
            
            @final
            @static
            @args(Cursor, [ContentResolver, Uri, [String]])
            def query(cr, uri, projection):
                pass
    
    class MediaColumns(Object):
    
        pass
    
    class Video(Object):
    
        pass
    
    def __init__(self):
        pass
    
    @args(Uri, [])
    def getMediaScannerUri(self):
        pass
    
    @args(String, [Context])
    def getVersion(self, context):
        pass


class Settings(Object):

    ### API level 17
    class Global(Settings.NameValueTable):
    
        ADB_ENABLED = "adb_enabled"
        AIRPLANE_MODE_ON = "airplane_mode_on"
        AIRPLANE_MODE_RADIOS = "airplane_mode_radios"
        ALWAYS_FINISH_ACTIVITIES = "always_finish_activities"
        ANIMATOR_DURATION_SCALE = "animator_duration_scale"
        AUTO_TIME = "auto_time"
        AUTO_TIME_ZONE = "auto_time_zone"
        BLUETOOTH_ON = "bluetooth_on"
        DATA_ROAMING = "data_roaming"
        DEBUG_APP = "debug_app"
        DEVELOPMENT_SETTINGS_ENABLED = "development_settings_enabled"
        DEVICE_PROVISIONED = "device_provisioned"
        HTTP_PROXY = "http_proxy"
        
        # Deprecated in API level 21:
        INSTALL_NON_MARKET_APPS = "install_non_market_apps"
        
        MODE_RINGER = "mode_ringer"
        NETWORK_PREFERENCE = "network_preference"
        RADIO_BLUETOOTH = "bluetooth"
        RADIO_CELL = "cell"
        RADIO_NFC = "nfc"
        RADIO_WIFI = "wifi"
        SHOW_PROCESSES = "show_processes"
        STAY_ON_WHILE_PLUGGED_IN = "stay_on_while_plugged_in"
        SYS_PROP_SETTING_VERSION = "sys.settings_global_version"
        TRANSITION_ANIMATION_SCALE = "transition_animation_scale"
        USB_MASS_STORAGE_ENABLED = "usb_mass_storage_enabled"
        USE_GOOGLE_MAIL = "use_google_mail"
        WAIT_FOR_DEBUGGER = "wait_for_debugger"
        WIFI_DEVICE_OWNER_CONFIGS_LOCKDOWN = "wifi_device_owner_configs_lockdown"
        WIFI_MAX_DHCP_RETRY_COUNT = "wifi_max_dhcp_retry_count"
        WIFI_MOBILE_DATA_TRANSITION_WAKELOCK_TIMEOUT_MS = "wifi_mobile_data_transition_wakelock_timeout_ms"
        WIFI_NETWORKS_AVAILABLE_NOTIFICATION_ON = "wifi_networks_available_notification_on"
        WIFI_NETWORKS_AVAILABLE_REPEAT_DELAY = "wifi_networks_available_repeat_delay"
        WIFI_NUM_OPEN_NETWORKS_KEPT = "wifi_num_open_networks_kept"
        WIFI_ON = "wifi_on"
        WIFI_SLEEP_POLICY = "wifi_sleep_policy"
        WIFI_SLEEP_POLICY_DEFAULT = 0
        WIFI_SLEEP_POLICY_NEVER = 2
        WIFI_SLEEP_POLICY_NEVER_WHILE_PLUGGED = 1
        WIFI_WATCHDOG_ON = "wifi_watchdog_on"
        WINDOW_ANIMATION_SCALE = "window_animation_scale"
        
        __static_fields__ = {
            "CONTENT_URI": Uri
            }
        
        @api(17)
        def __init__(self):
            pass
        
        @api(17)
        @static
        @args(float, [ContentResolver, String, float])
        def getFloat(cr, name, def_):
            pass
        
        @api(17)
        @static
        @args(float, [ContentResolver, String])
        def getFloat(cr, name):
            pass
        
        @api(17)
        @static
        @args(int, [ContentResolver, String, int])
        def getInt(cr, name, def_):
            pass
        
        @api(17)
        @static
        @args(int, [ContentResolver, String])
        def getInt(cr, name):
            pass
        
        @api(17)
        @static
        @args(long, [ContentResolver, String, long])
        def getLong(cr, name, def_):
            pass
        
        @api(17)
        @static
        @args(long, [ContentResolver, String])
        def getLong(cr, name):
            pass
        
        @api(17)
        @static
        @args(String, [ContentResolver, String])
        def getString(resolver, name):
            pass
        
        @api(17)
        @static
        @args(Uri, [String])
        def getUriFor(name):
            pass
        
        @api(17)
        @static
        @args(boolean, [ContentResolver, String, float])
        def putFloat(cr, name, value):
            pass
        
        @api(17)
        @static
        @args(boolean, [ContentResolver, String, int])
        def putInt(cr, name, value):
            pass
        
        @api(17)
        @static
        @args(boolean, [ContentResolver, String, long])
        def putLong(cr, name, value):
            pass
        
        @api(17)
        @static
        @args(boolean, [ContentResolver, String, String])
        def putString(resolver, name, value):
            pass
    
    class NameValueTable(Object):
    
        __interfaces__ = [BaseColumns]
        
        NAME = "name"
        VALUE = "value"
        
        def __init__(self):
            pass
        
        @static
        @args(Uri, [Uri, String])
        def getUriFor(uri, name):
            pass
        
        @protected
        @static
        @args(bool, [ContentResolver, Uri, String, String])
        def putString(resolver, uri, name, value):
            pass
    
    ### API level 3
    class Secure(Settings.NameValueTable):
    
        # API level 21
        ACCESSIBILITY_DISPLAY_INVERSION_ENABLED = "accessibility_display_inversion_enabled"
        # API level 4
        ACCESSIBILITY_ENABLED = "accessibility_enabled"
        # API level 15
        ACCESSIBILITY_SPEAK_PASSWORD = "speak_password"
        
        # API level 3-17
        ADB_ENABLED = "adb_enabled"
        
        # API level 8
        ALLOWED_GEOLOCATION_ORIGINS = "allowed_geolocation_origins"
        
        # API level 3-23
        ALLOW_MOCK_LOCATION = "mock_location"
        
        ANDROID_ID = "android_id"
        
        # API level 3-14
        BACKGROUND_DATA = "background_data"
        
        # API level 3-17
        BLUETOOTH_ON = "bluetooth_on"
        DATA_ROAMING = "data_roaming"
        
        DEFAULT_INPUT_METHOD = "default_input_method"
        
        # API level 16-17
        DEVELOPMENT_SETTINGS_ENABLED = "development_settings_enabled"
        # API level 3-17
        DEVICE_PROVISIONED = "device_provisioned"
        
        # API level 4
        ENABLED_ACCESSIBILITY_SERVICES = "enabled_accessibility_services"
        
        ENABLED_INPUT_METHODS = "enabled_input_methods"
        
        # API level 3-17
        HTTP_PROXY = "http_proxy"
        
        # API level 11
        INPUT_METHOD_SELECTOR_VISIBILITY = "input_method_selector_visibility"
        INSTALL_NON_MARKET_APPS = "install_non_market_apps"
        # API level 19
        LOCATION_MODE = "location_mode"
        LOCATION_MODE_BATTERY_SAVING = 2
        LOCATION_MODE_HIGH_ACCURACY = 3
        LOCATION_MODE_OFF = 0
        LOCATION_MODE_SENSORS_ONLY = 1
        
        # API level 3-19
        LOCATION_PROVIDERS_ALLOWED = "location_providers_allowed"
        
        # API level 8-23
        LOCK_PATTERN_ENABLED = "lock_pattern_autolock"
        
        # API level 8-17
        LOCK_PATTERN_TACTILE_FEEDBACK_ENABLED = "lock_pattern_tactile_feedback_enabled"
        
        # API level 8-23
        LOCK_PATTERN_VISIBLE = "lock_pattern_visible_pattern"
        
        # Deprecated in API level 3
        LOGGING_ID = "logging_id"
        
        # Deprecated in API level 17
        NETWORK_PREFERENCE = "network_preference"
        
        PARENTAL_CONTROL_ENABLED = "parental_control_enabled"
        PARENTAL_CONTROL_LAST_UPDATE = "parental_control_last_update"
        PARENTAL_CONTROL_REDIRECT_URL = "parental_control_redirect_url"
        # API level 11
        SELECTED_INPUT_METHOD_SUBTYPE = "selected_input_method_subtype"
        SETTINGS_CLASSNAME = "settings_classname"
        # API level 21
        SKIP_FIRST_USE_HINTS = "skip_first_use_hints"
        SYS_PROP_SETTING_VERSION = "sys.settings_secure_version"
        # API level 14
        TOUCH_EXPLORATION_ENABLED = "touch_exploration_enabled"
        
        # API level 4-14
        TTS_DEFAULT_COUNTRY = "tts_default_country"
        TTS_DEFAULT_LANG = "tts_default_lang"
        
        # Added in API level 4
        TTS_DEFAULT_PITCH = "tts_default_rate"
        TTS_DEFAULT_RATE = "tts_default_rate"
        TTS_DEFAULT_SYNTH = "tts_default_synth"
        
        # API level 4-14
        TTS_DEFAULT_VARIANT = "tts_default_variant"
        
        # API level 8
        TTS_ENABLED_PLUGINS = "tts_enabled_plugins"
        
        # API level 4-14
        TTS_USE_DEFAULTS = "tts_use_defaults"
        
        # API level 3-17
        USB_MASS_STORAGE_ENABLED = "usb_mass_storage_enabled"
        USE_GOOGLE_MAIL = "use_google_mail"
        WIFI_MAX_DHCP_RETRY_COUNT = "wifi_max_dhcp_retry_count"
        WIFI_MOBILE_DATA_TRANSITION_WAKELOCK_TIMEOUT_MS = "wifi_mobile_data_transition_wakelock_timeout_ms"
        WIFI_NETWORKS_AVAILABLE_NOTIFICATION_ON = "wifi_networks_available_notification_on"
        WIFI_NETWORKS_AVAILABLE_REPEAT_DELAY = "wifi_networks_available_repeat_delay"
        WIFI_NUM_OPEN_NETWORKS_KEPT = "wifi_num_open_networks_kept"
        WIFI_ON = "wifi_on"
        
        # API level 3-14
        WIFI_WATCHDOG_ACCEPTABLE_PACKET_LOSS_PERCENTAGE = "wifi_watchdog_acceptable_packet_loss_percentage"
        WIFI_WATCHDOG_AP_COUNT = "wifi_watchdog_ap_count"
        WIFI_WATCHDOG_BACKGROUND_CHECK_DELAY_MS = "wifi_watchdog_background_check_delay_ms"
        WIFI_WATCHDOG_BACKGROUND_CHECK_ENABLED = "wifi_watchdog_background_check_enabled"
        WIFI_WATCHDOG_BACKGROUND_CHECK_TIMEOUT_MS = "wifi_watchdog_background_check_timeout_ms"
        WIFI_WATCHDOG_INITIAL_IGNORED_PING_COUNT = "wifi_watchdog_initial_ignored_ping_count"
        WIFI_WATCHDOG_MAX_AP_CHECKS = "wifi_watchdog_max_ap_checks"
        
        # API level 3-17
        WIFI_WATCHDOG_ON = "wifi_watchdog_on"
        
        # API level 3-14
        WIFI_WATCHDOG_PING_COUNT = "wifi_watchdog_ping_count"
        WIFI_WATCHDOG_PING_DELAY_MS = "wifi_watchdog_ping_delay_ms"
        WIFI_WATCHDOG_PING_TIMEOUT_MS = "wifi_watchdog_ping_timeout_ms"
        WIFI_WATCHDOG_WATCH_LIST = "wifi_watchdog_watch_list"
        
        __static_fields__ = {
            "CONTENT_URI": Uri
            }
        
        @api(17)
        def __init__(self):
            pass
        
        @api(17)
        @static
        @args(float, [ContentResolver, String, float])
        def getFloat(cr, name, def_):
            pass
        
        @api(17)
        @static
        @args(float, [ContentResolver, String])
        def getFloat(cr, name):
            pass
        
        @api(17)
        @static
        @args(int, [ContentResolver, String, int])
        def getInt(cr, name, def_):
            pass
        
        @api(17)
        @static
        @args(int, [ContentResolver, String])
        def getInt(cr, name):
            pass
        
        @api(17)
        @static
        @args(long, [ContentResolver, String, long])
        def getLong(cr, name, def_):
            pass
        
        @api(17)
        @static
        @args(long, [ContentResolver, String])
        def getLong(cr, name):
            pass
        
        @api(17)
        @static
        @args(String, [ContentResolver, String])
        def getString(resolver, name):
            pass
        
        @api(17)
        @static
        @args(Uri, [String])
        def getUriFor(name):
            pass
        
        @final
        @static
        @api(8, 19)
        @args(bool, [ContentResolver, String])
        def isLocationProviderEnabled(cr, provider):
            pass
        
        @api(17)
        @static
        @args(boolean, [ContentResolver, String, float])
        def putFloat(cr, name, value):
            pass
        
        @api(17)
        @static
        @args(boolean, [ContentResolver, String, int])
        def putInt(cr, name, value):
            pass
        
        @api(17)
        @static
        @args(boolean, [ContentResolver, String, long])
        def putLong(cr, name, value):
            pass
        
        @api(17)
        @static
        @args(boolean, [ContentResolver, String, String])
        def putString(resolver, name, value):
            pass
        
        @api(8, 19)
        @final
        @static
        @args(void, [ContentResolver, String, bool])
        def setLocationProviderEnabled(cr, provider, enabled):
            pass
    
    class SettingNotFoundException(AndroidException):
    
        @args(void, [String])
        def __init__(self, msg):
            pass
    
    class System(Settings.NameValueTable):
    
        # API level 3
        ACCELEROMETER_ROTATION = "accelerometer_rotation"
        # API level 1-3
        ADB_ENABLED = "adb_enabled"
        # API level 1-17
        AIRPLANE_MODE_ON = "airplane_mode_on"
        AIRPLANE_MODE_RADIOS = "airplane_mode_radios"
        # API level 5
        ALARM_ALERT = "alarm_alert"
        # API level 1-17
        ALWAYS_FINISH_ACTIVITIES = "always_finish_activities"
        # API level 1-3
        ANDROID_ID = "android_id"
        # API level 16-17
        ANIMATOR_DURATION_SCALE = "animator_duration_scale"
        # API level 1-17
        AUTO_TIME = "auto_time"
        # API level 11-17
        AUTO_TIME_ZONE = "auto_time_zone"
        BLUETOOTH_DISCOVERABILITY = "bluetooth_discoverability"
        BLUETOOTH_DISCOVERABILITY_TIMEOUT = "bluetooth_discoverability_timeout"
        # API level 1-3
        BLUETOOTH_ON = "bluetooth_on"
        DATA_ROAMING = "data_roaming"
        DATE_FORMAT = "date_format"
        # API level 1-17
        DEBUG_APP = "debug_app"
        # API level 1-3
        DEVICE_PROVISIONED = "device_provisioned"
        # API level 1-17
        DIM_SCREEN = "dim_screen"
        # API level 23
        DTMF_TONE_TYPE_WHEN_DIALING = "dtmf_tone_type"

        DTMF_TONE_WHEN_DIALING = "dtmf_tone"
        END_BUTTON_BEHAVIOR = "end_button_behavior"
        FONT_SCALE = "font_scale"

        # API level 3
        HAPTIC_FEEDBACK_ENABLED = "haptic_feedback_enabled"
        # API level 1-3
        HTTP_PROXY = "http_proxy"
        INSTALL_NON_MARKET_APPS = "install_non_market_apps"
        LOCATION_PROVIDERS_ALLOWED = "location_providers_allowed"
        # API level 1-8
        LOCK_PATTERN_ENABLED = "lock_pattern_autolock"
        # API level 3-8
        LOCK_PATTERN_TACTILE_FEEDBACK_ENABLED = "lock_pattern_tactile_feedback_enabled"
        # API level 1-8
        LOCK_PATTERN_VISIBLE = "lock_pattern_visible_pattern"
        # API level 1-3
        LOGGING_ID = "logging_id"
        # API level 1-17
        MODE_RINGER = "mode_ringer"

        MODE_RINGER_STREAMS_AFFECTED = "mode_ringer_streams_affected"
        MUTE_STREAMS_AFFECTED = "mute_streams_affected"

        # API level 1-3
        NETWORK_PREFERENCE = "network_preference"
        # API level 1-21
        NEXT_ALARM_FORMATTED = "next_alarm_formatted"

        NOTIFICATION_SOUND = "notification_sound"
        # API level 1-3
        PARENTAL_CONTROL_ENABLED = "parental_control_enabled"
        PARENTAL_CONTROL_LAST_UPDATE = "parental_control_last_update"
        PARENTAL_CONTROL_REDIRECT_URL = "parental_control_redirect_url"
        # API level 1-17
        RADIO_BLUETOOTH = "bluetooth"
        RADIO_CELL = "cell"
        # API level 14-17
        RADIO_NFC = "nfc"
        # API level 1-17
        RADIO_WIFI = "wifi"

        RINGTONE = "ringtone"
        SCREEN_BRIGHTNESS = "screen_brightness"
        # API level 8
        SCREEN_BRIGHTNESS_MODE = "screen_brightness_mode"
        SCREEN_BRIGHTNESS_MODE_AUTOMATIC = 1
        SCREEN_BRIGHTNESS_MODE_MANUAL = 0

        SCREEN_OFF_TIMEOUT = "screen_off_timeout"
        # API level 1-3
        SETTINGS_CLASSNAME = "settings_classname"

        SETUP_WIZARD_HAS_RUN = "setup_wizard_has_run"
        SHOW_GTALK_SERVICE_STATUS = "SHOW_GTALK_SERVICE_STATUS"
        # API level 1-17
        SHOW_PROCESSES = "show_processes"
        # API level 1-4
        SHOW_WEB_SUGGESTIONS = "show_web_suggestions"

        SOUND_EFFECTS_ENABLED = "sound_effects_enabled"
        # API level 1-17
        STAY_ON_WHILE_PLUGGED_IN = "stay_on_while_plugged_in"

        SYS_PROP_SETTING_VERSION = "sys.settings_system_version"
        TEXT_AUTO_CAPS = "auto_caps"
        TEXT_AUTO_PUNCTUATE = "auto_punctuate"
        TEXT_AUTO_REPLACE = "auto_replace"
        TEXT_SHOW_PASSWORD = "show_password"
        TIME_12_24 = "time_12_24"
        # API level 1-17
        TRANSITION_ANIMATION_SCALE = "transition_animation_scale"
        # API level 1-3
        USB_MASS_STORAGE_ENABLED = "usb_mass_storage_enabled"
        # API level 11
        USER_ROTATION = "user_rotation"
        # API level 1-3
        USE_GOOGLE_MAIL = "use_google_mail"
        VIBRATE_ON = "vibrate_on"
        # API level 23
        VIBRATE_WHEN_RINGING = "vibrate_when_ringing"
        # API level 1-17
        WAIT_FOR_DEBUGGER = "wait_for_debugger"
        WALLPAPER_ACTIVITY = "wallpaper_activity"
        # API level 3-3
        WIFI_MAX_DHCP_RETRY_COUNT = "wifi_max_dhcp_retry_count"
        WIFI_MOBILE_DATA_TRANSITION_WAKELOCK_TIMEOUT_MS = "wifi_mobile_data_transition_wakelock_timeout_ms"
        # API level 1-3
        WIFI_NETWORKS_AVAILABLE_NOTIFICATION_ON = "wifi_networks_available_notification_on"
        WIFI_NETWORKS_AVAILABLE_REPEAT_DELAY = "wifi_networks_available_repeat_delay"
        WIFI_NUM_OPEN_NETWORKS_KEPT = "wifi_num_open_networks_kept"
        WIFI_ON = "wifi_on"
        # API level 3-17
        WIFI_SLEEP_POLICY = "wifi_sleep_policy"
        WIFI_SLEEP_POLICY_DEFAULT = 0
        WIFI_SLEEP_POLICY_NEVER = 2
        WIFI_SLEEP_POLICY_NEVER_WHILE_PLUGGED = 1
        # API level 1-17
        WIFI_STATIC_DNS1 = "wifi_static_dns1"
        WIFI_STATIC_DNS2 = "wifi_static_dns2"
        WIFI_STATIC_GATEWAY = "wifi_static_gateway"
        WIFI_STATIC_IP = "wifi_static_ip"
        WIFI_STATIC_NETMASK = "wifi_static_netmask"
        WIFI_USE_STATIC_IP = "wifi_use_static_ip"
        # API level 1-3
        WIFI_WATCHDOG_ACCEPTABLE_PACKET_LOSS_PERCENTAGE = "wifi_watchdog_acceptable_packet_loss_percentage"
        WIFI_WATCHDOG_AP_COUNT = "wifi_watchdog_ap_count"
        WIFI_WATCHDOG_BACKGROUND_CHECK_DELAY_MS = "wifi_watchdog_background_check_delay_ms"
        WIFI_WATCHDOG_BACKGROUND_CHECK_ENABLED = "wifi_watchdog_background_check_enabled"
        WIFI_WATCHDOG_BACKGROUND_CHECK_TIMEOUT_MS = "wifi_watchdog_background_check_timeout_ms"
        WIFI_WATCHDOG_INITIAL_IGNORED_PING_COUNT = "wifi_watchdog_initial_ignored_ping_count"
        WIFI_WATCHDOG_MAX_AP_CHECKS = "wifi_watchdog_max_ap_checks"
        WIFI_WATCHDOG_ON = "wifi_watchdog_on"
        WIFI_WATCHDOG_PING_COUNT = "wifi_watchdog_ping_count"
        WIFI_WATCHDOG_PING_DELAY_MS = "wifi_watchdog_ping_delay_ms"
        WIFI_WATCHDOG_PING_TIMEOUT_MS = "wifi_watchdog_ping_timeout_ms"
        # API level 1-17
        WINDOW_ANIMATION_SCALE = "window_animation_scale"
        
        __static_fields__ = {
            "CONTENT_URI": Uri,
            "DEFAULT_ALARM_ALERT_URI": Uri,
            "DEFAULT_NOTIFICATION_URI": Uri,
            "DEFAULT_RINGTONE_URI": Uri
            }
        
        def __init__(self):
            pass
        
        @api(23)
        @static
        @args(bool, [Context])
        def canWrite(context):
            pass
        
        @static
        @args(void, [ContentResolver, Configuration])
        def getConfiguration(cr, outConfig):
            pass
        
        @static
        @args(float, [ContentResolver, String, float])
        def getFloat(cr, name, def_):
            pass
        
        @static
        @args(float, [ContentResolver, String])
        def getFloat(cr, name):
            pass
        
        @static
        @args(int, [ContentResolver, String, int])
        def getInt(cr, name, def_):
            pass
        
        @static
        @args(int, [ContentResolver, String])
        def getInt(cr, name):
            pass
        
        @api(3)
        @static
        @args(long, [ContentResolver, String, long])
        def getLong(cr, name, def_):
            pass
        
        @api(3)
        @static
        @args(long, [ContentResolver, String])
        def getLong(cr, name):
            pass
        
        @api(1, 17)
        @static
        @args(bool, [ContentResolver])
        def getShowGTalkServiceStatus(cr):
            pass
        
        @static
        @args(String, [ContentResolver, String])
        def getString(resolver, name):
            pass
        
        @static
        @args(Uri, [String])
        def getUriFor(name):
            pass
        
        @static
        @args(bool, [ContentResolver, Configuration])
        def putConfiguration(cr, config):
            pass
        
        @static
        @args(boolean, [ContentResolver, String, float])
        def putFloat(cr, name, value):
            pass
        
        @static
        @args(boolean, [ContentResolver, String, int])
        def putInt(cr, name, value):
            pass
        
        @api(3)
        @static
        @args(boolean, [ContentResolver, String, long])
        def putLong(cr, name, value):
            pass
        
        @static
        @args(boolean, [ContentResolver, String, String])
        def putString(resolver, name, value):
            pass
        
        @api(1, 17)
        @static
        @args(void, [ContentResolver, bool])
        def setShowGTalkServiceStatus(cr, flag):
            pass
    
    # API level 5
    ACTION_ACCESSIBILITY_SETTINGS = "android.settings.ACCESSIBILITY_SETTINGS"
    # API level 8
    ACTION_ADD_ACCOUNT = "android.settings.ADD_ACCOUNT_SETTINGS"
    # API level 3
    ACTION_AIRPLANE_MODE_SETTINGS = "android.settings.AIRPLANE_MODE_SETTINGS"
    
    ACTION_APN_SETTINGS = "android.settings.APN_SETTINGS"
    # API level 9
    ACTION_APPLICATION_DETAILS_SETTINGS = "android.settings.APPLICATION_DETAILS_SETTINGS"
    # API level 3
    ACTION_APPLICATION_DEVELOPMENT_SETTINGS = "android.settings.APPLICATION_DEVELOPMENT_SETTINGS"
    
    ACTION_APPLICATION_SETTINGS = "android.settings.APPLICATION_SETTINGS"
    # API level 22
    ACTION_BATTERY_SAVER_SETTINGS = "android.settings.BATTERY_SAVER_SETTINGS"
    
    ACTION_BLUETOOTH_SETTINGS = "android.settings.BLUETOOTH_SETTINGS"
    # API level 19
    ACTION_CAPTIONING_SETTINGS = "android.settings.CAPTIONING_SETTINGS"
    # API level 21
    ACTION_CAST_SETTINGS = "android.settings.CAST_SETTINGS"
    # API level 3
    ACTION_DATA_ROAMING_SETTINGS = "android.settings.DATA_ROAMING_SETTINGS"
    
    ACTION_DATE_SETTINGS = "android.settings.DATE_SETTINGS"
    # API level 8
    ACTION_DEVICE_INFO_SETTINGS = "android.settings.DEVICE_INFO_SETTINGS"
    
    ACTION_DISPLAY_SETTINGS = "android.settings.DISPLAY_SETTINGS"
    # API level 18
    ACTION_DREAM_SETTINGS = "android.settings.DREAM_SETTINGS"
    # API level 21
    ACTION_HOME_SETTINGS = "android.settings.HOME_SETTINGS"
    # API level 23
    ACTION_IGNORE_BATTERY_OPTIMIZATION_SETTINGS = "android.settings.IGNORE_BATTERY_OPTIMIZATION_SETTINGS"
    # API level 3
    ACTION_INPUT_METHOD_SETTINGS = "android.settings.INPUT_METHOD_SETTINGS"
    # API level 11
    ACTION_INPUT_METHOD_SUBTYPE_SETTINGS = "android.settings.INPUT_METHOD_SUBTYPE_SETTINGS"
    # API level 3
    ACTION_INTERNAL_STORAGE_SETTINGS = "android.settings.INTERNAL_STORAGE_SETTINGS"

    ACTION_LOCALE_SETTINGS = "android.settings.LOCALE_SETTINGS"
    ACTION_LOCATION_SOURCE_SETTINGS = "android.settings.LOCATION_SOURCE_SETTINGS"
    # API level 9
    ACTION_MANAGE_ALL_APPLICATIONS_SETTINGS = "android.settings.MANAGE_ALL_APPLICATIONS_SETTINGS"
    # API level 3
    ACTION_MANAGE_APPLICATIONS_SETTINGS = "android.settings.MANAGE_APPLICATIONS_SETTINGS"
    # API level 23
    ACTION_MANAGE_OVERLAY_PERMISSION = "android.settings.action.MANAGE_OVERLAY_PERMISSION"
    ACTION_MANAGE_WRITE_SETTINGS = "android.settings.action.MANAGE_WRITE_SETTINGS"
    # API level 3
    ACTION_MEMORY_CARD_SETTINGS = "android.settings.MEMORY_CARD_SETTINGS"
    ACTION_NETWORK_OPERATOR_SETTINGS = "android.settings.NETWORK_OPERATOR_SETTINGS"
    # API level 14
    ACTION_NFCSHARING_SETTINGS = "android.settings.NFCSHARING_SETTINGS"
    # API level 19
    ACTION_NFC_PAYMENT_SETTINGS = "android.settings.NFC_PAYMENT_SETTINGS"
    # API level 16
    ACTION_NFC_SETTINGS = "android.settings.NFC_SETTINGS"
    # API level 22
    ACTION_NOTIFICATION_LISTENER_SETTINGS = "android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS"
    # API level 23
    ACTION_NOTIFICATION_POLICY_ACCESS_SETTINGS = "android.settings.NOTIFICATION_POLICY_ACCESS_SETTINGS"
    # API level 19
    ACTION_PRINT_SETTINGS = "android.settings.ACTION_PRINT_SETTINGS"
    # API level 5
    ACTION_PRIVACY_SETTINGS = "android.settings.PRIVACY_SETTINGS"
    # API level 3
    ACTION_QUICK_LAUNCH_SETTINGS = "android.settings.QUICK_LAUNCH_SETTINGS"
    # API level 23
    ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS = "android.settings.REQUEST_IGNORE_BATTERY_OPTIMIZATIONS"
    # API level 8
    ACTION_SEARCH_SETTINGS = "android.search.action.SEARCH_SETTINGS"
    
    ACTION_SECURITY_SETTINGS = "android.settings.SECURITY_SETTINGS"
    ACTION_SETTINGS = "android.settings.SETTINGS"
    # API level 21
    ACTION_SHOW_REGULATORY_INFO = "android.settings.SHOW_REGULATORY_INFO"
    
    ACTION_SOUND_SETTINGS = "android.settings.SOUND_SETTINGS"
    # API level 3
    ACTION_SYNC_SETTINGS = "android.settings.SYNC_SETTINGS"
    # API level 21
    ACTION_USAGE_ACCESS_SETTINGS = "android.settings.USAGE_ACCESS_SETTINGS"
    # API level 3
    ACTION_USER_DICTIONARY_SETTINGS = "android.settings.USER_DICTIONARY_SETTINGS"
    # API level 23
    ACTION_VOICE_CONTROL_AIRPLANE_MODE = "android.settings.VOICE_CONTROL_AIRPLANE_MODE"
    ACTION_VOICE_CONTROL_BATTERY_SAVER_MODE = "android.settings.VOICE_CONTROL_BATTERY_SAVER_MODE"
    ACTION_VOICE_CONTROL_DO_NOT_DISTURB_MODE = "android.settings.VOICE_CONTROL_DO_NOT_DISTURB_MODE"
    # API level 21
    ACTION_VOICE_INPUT_SETTINGS = "android.settings.VOICE_INPUT_SETTINGS"
    # API level 3
    ACTION_WIFI_IP_SETTINGS = "android.settings.WIFI_IP_SETTINGS"
    
    ACTION_WIFI_SETTINGS = "android.settings.WIFI_SETTINGS"
    ACTION_WIRELESS_SETTINGS = "android.settings.WIRELESS_SETTINGS"
    AUTHORITY = "settings"
    # API level 18
    EXTRA_ACCOUNT_TYPES = "account_types"
    # API level 23
    EXTRA_AIRPLANE_MODE_ENABLED = "airplane_mode_enabled"
    # API level 8
    EXTRA_AUTHORITIES = "authorities"
    # API level 23
    EXTRA_BATTERY_SAVER_MODE_ENABLED = "android.settings.extra.battery_saver_mode_enabled"
    EXTRA_DO_NOT_DISTURB_MODE_ENABLED = "android.settings.extra.do_not_disturb_mode_enabled"
    EXTRA_DO_NOT_DISTURB_MODE_MINUTES = "android.settings.extra.do_not_disturb_mode_minutes"
    # API level 11
    EXTRA_INPUT_METHOD_ID = "input_method_id"
    # API level 23
    INTENT_CATEGORY_USAGE_ACCESS_CONFIG = "android.intent.category.USAGE_ACCESS_CONFIG"
    METADATA_USAGE_ACCESS_REASON = "android.settings.metadata.USAGE_ACCESS_REASON"
    
    def __init__(self):
        pass
    
    @api(23)
    @static
    @args(bool, [Context])
    def canDrawOverlays(context):
        pass


class Telephony(Object):

    class BaseMmsColumns:
    
        CONTENT_CLASS = "ct_cls"
        CONTENT_LOCATION = "ct_l"
        CONTENT_TYPE = "ct_t"
        CREATOR = "creator"
        DATE = "date"
        DATE_SENT = "date_sent"
        DELIVERY_REPORT = "d_rpt"
        DELIVERY_TIME = "d_tm"
        EXPIRY = "exp"
        LOCKED = "locked"
        MESSAGE_BOX = "msg_box"
        MESSAGE_BOX_ALL = 0
        MESSAGE_BOX_DRAFTS = 3
        MESSAGE_BOX_FAILED = 5
        MESSAGE_BOX_INBOX = 1
        MESSAGE_BOX_OUTBOX = 4
        MESSAGE_BOX_SENT = 2
        MESSAGE_CLASS = "m_cls"
        MESSAGE_ID = "m_id"
        MESSAGE_SIZE = "m_size"
        MESSAGE_TYPE = "m_type"
        MMS_VERSION = "v"
        PRIORITY = "pri"
        READ = "read"
        READ_REPORT = "rr"
        READ_STATUS = "read_status"
        REPORT_ALLOWED = "rpt_a"
        RESPONSE_STATUS = "resp_st"
        RESPONSE_TEXT = "resp_txt"
        RETRIEVE_STATUS = "retr_st"
        RETRIEVE_TEXT = "retr_txt"
        RETRIEVE_TEXT_CHARSET = "retr_txt_cs"
        SEEN = "seen"
        STATUS = "st"
        SUBJECT = "sub"
        SUBJECT_CHARSET = "sub_cs"
        SUBSCRIPTION_ID = "sub_id"
        TEXT_ONLY = "thread_id"
        THREAD_ID = "tr_id"
        TRANSACTION_ID = "tr_id"
    
    class CanonicalAddressesColumns:
    
        ADDRESS = "address"
    
    class Carriers(Object):

        APN = "apn"
        AUTH_TYPE = "authtype"
        BEARER = "bearer"
        CARRIER_ENABLED = "carrier_enabled"
        CURRENT = "current"
        DEFAULT_SORT_ORDER = "name ASC"
        MCC = "mcc"
        MMSC = "mmsc"
        MMSPORT = "mmsport"
        MMSPROXY = "mmsproxy"
        MNC = "mnc"
        MVNO_MATCH_DATA = "mvno_match_data"
        MVNO_TYPE = "mvno_type"
        NAME = "name"
        NUMERIC = "numeric"
        PASSWORD = "password"
        PORT = "port"
        PROTOCOL = "protocol"
        PROXY = "proxy"
        ROAMING_PROTOCOL = "roaming_protocol"
        SERVER = "server"
        SUBSCRIPTION_ID = "sub_id"
        TYPE = "type"
        USER = "user"
        
        __fields__ = {
            "CONTENT_URI": Uri
            }
    
    class Mms(Object):
    
        __interfaces__ = [Telephony.BaseMmsColumns]
        __fields__ = {
            "CONTENT_URI": Uri,
            "REPORT_REQUEST_URI": Uri,
            "REPORT_STATUS_URI": Uri
            }
        
        DEFAULT_SORT_ORDER = "date DESC"
        
        class Addr(Object):
        
            __interfaces__ = [BaseColumns]
            
            ADDRESS = "address"
            CHARSET = "charset"
            CONTACT_ID = "contact_id"
            MSG_ID = "msg_id"
            TYPE = "type"
        
        class Draft(Object):
        
            __interfaces__ = [Telephony.BaseMmsColumns]
            __fields__ = {
                "CONTENT_URI": Uri
                }
            
            DEFAULT_SORT_ORDER = "date DESC"
        
        class Inbox(Object):
        
            __interfaces__ = [Telephony.BaseMmsColumns]
            __fields__ = {
                "CONTENT_URI": Uri
                }
            
            DEFAULT_SORT_ORDER = "date DESC"
        
        class Intents(Object):
        
            CONTENT_CHANGED_ACTION =  "android.intent.action.CONTENT_CHANGED"
            DELETED_CONTENTS = "deleted_contents"
        
        class Outbox(Object):
        
            __interfaces__ = [Telephony.BaseMmsColumns]
            __fields__ = {
                "CONTENT_URI": Uri
                }
            
            DEFAULT_SORT_ORDER = "date DESC"
        
        class Part(Object):
        
            __interfaces__ = [BaseColumns]
            
            CHARSET = "chset"
            CONTENT_DISPOSITION = "cd"
            CONTENT_ID = "cid"
            CONTENT_LOCATION = "cl"
            CONTENT_TYPE = "ct"
            CT_START = "ctt_s"
            CT_TYPE = "ctt_t"
            FILENAME = "fn"
            MSG_ID = "mid"
            NAME = "name"
            SEQ = "seq"
            TEXT = "text"
            _DATA = "_data"
        
        class Rate(Object):
        
            __fields__ = {
                "CONTENT_URI": Uri
                }
            
            SENT_TIME = "sent_time"
        
        class Sent(Object):
        
            __interfaces__ = [Telephony.BaseMmsColumns]
            __fields__ = {
                "CONTENT_URI": Uri
                }
            
            DEFAULT_SORT_ORDER = "date DESC"
    
    class MmsSms(Object):
    
        __interfaces__ = [BaseColumns]
        __fields__ = {
            "CONTENT_CONVERSATIONS_URI": Uri,
            "CONTENT_DRAFT_URI": Uri,
            "CONTENT_FILTER_BYPHONE_URI": Uri,
            "CONTENT_LOCKED_URI": Uri,
            "CONTENT_UNDELIVERED_URI": Uri,
            "CONTENT_URI": Uri,
            "SEARCH_URI": Uri
            }
        
        ERR_TYPE_GENERIC = 1
        ERR_TYPE_GENERIC_PERMANENT = 10
        ERR_TYPE_MMS_PROTO_PERMANENT = 12
        ERR_TYPE_MMS_PROTO_TRANSIENT = 3
        ERR_TYPE_SMS_PROTO_PERMANENT = 11
        ERR_TYPE_SMS_PROTO_TRANSIENT = 2
        ERR_TYPE_TRANSPORT_FAILURE = 4
        MMS_PROTO = 1
        NO_ERROR = 0
        SMS_PROTO = 0
        TYPE_DISCRIMINATOR_COLUMN = "transport_type"
        
        class PendingMessages(Object):
        
            __interfaces__ = [BaseColumns]
            __fields__ = {
                "CONTENT_URI": Uri
                }
            
            DUE_TIME = "due_time"
            ERROR_CODE = "err_code"
            ERROR_TYPE = "err_type"
            LAST_TRY = "last_try"
            MSG_ID = "msg_id"
            MSG_TYPE = "msg_type"
            PROTO_TYPE = "proto_type"
            RETRY_INDEX = "retry_index"
            SUBSCRIPTION_ID = "pending_sub_id"
    
    class Sms(Object):
    
        __interfaces__ = [BaseColumns, Telephony.TextBasedSmsColumns]
        __fields__ = {
            "CONTENT_URI": Uri
            }
        
        DEFAULT_SORT_ORDER = "date DESC"
        
        class Conversations(Object):
        
            __interfaces__ = [BaseColumns, Telephony.TextBasedSmsColumns]
            __fields__ = {
                "CONTENT_URI": Uri
                }
            
            DEFAULT_SORT_ORDER = "date DESC"
            MESSAGE_COUNT = "msg_count"
            SNIPPET = "snippet"
        
        class Draft(Object):
        
            __interfaces__ = [BaseColumns, Telephony.TextBasedSmsColumns]
            __fields__ = {
                "CONTENT_URI": Uri
                }
            
            DEFAULT_SORT_ORDER = "date DESC"
        
        class Inbox(Object):
        
            __interfaces__ = [BaseColumns, Telephony.TextBasedSmsColumns]
            __fields__ = {
                "CONTENT_URI": Uri
                }
            
            DEFAULT_SORT_ORDER = "date DESC"
        
        class Intents(Object):
        
            ACTION_CHANGE_DEFAULT = "android.provider.Telephony.ACTION_CHANGE_DEFAULT"
            DATA_SMS_RECEIVED_ACTION = "android.intent.action.DATA_SMS_RECEIVED"
            EXTRA_PACKAGE_NAME = "package"
            RESULT_SMS_DUPLICATED = 5
            RESULT_SMS_GENERIC_ERROR = 2
            RESULT_SMS_HANDLED = 1
            RESULT_SMS_OUT_OF_MEMORY = 3
            RESULT_SMS_UNSUPPORTED = 4
            SIM_FULL_ACTION = "android.provider.Telephony.SIM_FULL"
            SMS_CB_RECEIVED_ACTION = "android.provider.Telephony.SMS_CB_RECEIVED"
            SMS_DELIVER_ACTION = "android.provider.Telephony.SMS_DELIVER"
            SMS_EMERGENCY_CB_RECEIVED_ACTION = "android.provider.Telephony.SMS_EMERGENCY_CB_RECEIVED"
            SMS_RECEIVED_ACTION = "android.provider.Telephony.SMS_RECEIVED"
            SMS_REJECTED_ACTION = "android.provider.Telephony.SMS_REJECTED"
            SMS_SERVICE_CATEGORY_PROGRAM_DATA_RECEIVED_ACTION = "android.provider.Telephony.SMS_SERVICE_CATEGORY_PROGRAM_DATA_RECEIVED"
            WAP_PUSH_DELIVER_ACTION = "android.provider.Telephony.WAP_PUSH_DELIVER"
            WAP_PUSH_RECEIVED_ACTION = "android.provider.Telephony.WAP_PUSH_RECEIVED"
            
            @static
            @args([SmsMessage], [Intent])
            def getMessagesFromIntent(intent):
                pass
        
        class Outbox(Object):
        
            __interfaces__ = [BaseColumns, Telephony.TextBasedSmsColumns]
            __fields__ = {
                "CONTENT_URI": Uri
                }
            
            DEFAULT_SORT_ORDER = "date DESC"
        
        class Sent(Object):
        
            __interfaces__ = [BaseColumns, Telephony.TextBasedSmsColumns]
            __fields__ = {
                "CONTENT_URI": Uri
                }
            
            DEFAULT_SORT_ORDER = "date DESC"
        
        @static
        @args(String, [Context])
        def getDefaultSmsPackage(context):
            pass
    
    class TextBasedSmsColumns:
    
        ADDRESS = "address"
        BODY = "body"
        CREATOR = "creator"
        DATE = "date"
        DATE_SENT = "date_sent"
        ERROR_CODE = "error_code"
        LOCKED = "locked"
        MESSAGE_TYPE_ALL = 0
        MESSAGE_TYPE_DRAFT = 3
        MESSAGE_TYPE_FAILED = 5
        MESSAGE_TYPE_INBOX = 1
        MESSAGE_TYPE_OUTBOX = 4
        MESSAGE_TYPE_QUEUED = 6
        MESSAGE_TYPE_SENT = 2
        PERSON = "person"
        PROTOCOL = "protocol"
        READ = "read"
        REPLY_PATH_PRESENT = "reply_path_present"
        SEEN = "seen"
        SERVICE_CENTER = "service_center"
        STATUS = "status"
        STATUS_COMPLETE = 0
        STATUS_FAILED = 64
        STATUS_NONE = -1
        STATUS_PENDING = 32
        SUBJECT = "subject"
        SUBSCRIPTION_ID = "sub_id"
        THREAD_ID = "thread_id"
        TYPE = "type"
    
    class Threads(Object):
    
        __interfaces__ = [Telephony.ThreadsColumns]
        __fields__ = {
            "CONTENT_URI": Uri,
            "OBSOLETE_THREADS_URI": Uri
            }
        
        BROADCAST_THREAD = 1
        COMMON_THREAD = 0
        
        @static
        @args(long, [Context, String])
        def getOrCreateThreadId(context, recipient):
            pass
        
        @static
        @args(long, [Context, Set(String)])
        def getOrCreateThreadId(context, recipients):
            pass
    
    class ThreadsColumns:
    
        __interfaces__ = [BaseColumns]
        
        ARCHIVED = "archived"
        DATE = "date"
        ERROR = "error"
        HAS_ATTACHMENT = "has_attachment"
        MESSAGE_COUNT = "message_count"
        READ = "read"
        RECIPIENT_IDS = "recipient_ids"
        SNIPPET = "snippet"
        SNIPPET_CHARSET = "snippet_cs"
        TYPE = "type"
