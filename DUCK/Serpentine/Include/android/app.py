# Class declarations obtained from Android API documentation licensed under the
# Apache 2.0 license (http://www.apache.org/licenses/LICENSE-2.0) and rewritten
# in syntax that the Python compiler module can parse.

__package__ = "android.app"

from java.lang import CharSequence, Exception, Object, String
from android.content import BroadcastReceiver, ComponentCallbacks2, \
    ComponentName, Context, Intent, IntentFilter, SharedPreferences
from android.content import ContextWrapper, DialogInterface
from android.content.res import Configuration, Resources
from android.graphics.drawable import Drawable
from android.os import Bundle, Handler, IBinder, Parcelable, UserHandle
from android.util import AndroidException
from android.view import ContextMenu, ContextThemeWrapper, KeyEvent, Menu, \
    MenuItem, View, ViewGroup, Window, WindowManager
from android.widget import SpinnerAdapter

class ActionBar(Object):

    DISPLAY_HOME_AS_UP = 4
    DISPLAY_SHOW_CUSTOM = 16
    DISPLAY_SHOW_HOME = 2
    DISPLAY_SHOW_TITLE = 8
    DISPLAY_USE_LOGO = 1
    NAVIGATION_MODE_LIST = 1
    NAVIGATION_MODE_STANDARD = 0
    NAVIGATION_MODE_TABS = 2
    
    class LayoutParams(ViewGroup.MarginLayoutParams):
    
        @args(void, [int, int])
        def __init__(self, width, height):
            pass
        
        @args(void, [int, int, int])
        def __init__(self, width, height, gravity):
            pass
        
        @args(void, [int])
        def __init__(self, gravity):
            pass
        
        @args(void, [ActionBar.LayoutParams])
        def __init__(self, source):
            pass
        
        @args(void, [ViewGroup.LayoutParams])
        def __init__(self, source):
            pass
    
    class OnMenuVisibilityListener:
    
        @args(void, [bool])
        def onMenuVisibilityChanged(self, isVisible):
            pass
    
    class OnNavigationListener:
    
        @args(bool, [int, long])
        def onNavigationItemSelected(self, itemPosition, itemId):
            pass
    
    class Tab(Object):
    
        INVALID_POSITION = 0xffffffff
        
        def __init__(self):
            pass
        
        @abstract
        @args(CharSequence, [])
        def getContentDescription(self):
            pass
        
        @abstract
        @args(View, [])
        def getCustomView(self):
            pass
        
        @abstract
        @args(Drawable, [])
        def getIcon(self):
            pass
        
        @abstract
        @args(int, [])
        def getPosition(self):
            pass
        
        @abstract
        @args(Object, [])
        def getTag(self):
            pass
        
        @abstract
        @args(CharSequence, [])
        def getText(self):
            pass
        
        @abstract
        def select(self):
            pass
        
        @abstract
        @args(ActionBar.Tab, [int])
        def setContentDescription(self, resId):
            pass
        
        @abstract
        @args(ActionBar.Tab, [CharSequence])
        def setContentDescription(self, contentDesc):
            pass
        
        @abstract
        @args(ActionBar.Tab, [int])
        def setCustomView(self, layoutResId):
            pass
        
        @abstract
        @args(ActionBar.Tab, [View])
        def setCustomView(self, view):
            pass
        
        @abstract
        @args(ActionBar.Tab, [Drawable])
        def setIcon(self, icon):
            pass
        
        @abstract
        @args(ActionBar.Tab, [int])
        def setIcon(self, resId):
            pass
        
        @abstract
        @args(ActionBar.Tab, [ActionBar.TabListener])
        def setTabListener(self, listener):
            pass
        
        @abstract
        @args(ActionBar.Tab, [Object])
        def setObject(self, tag):
            pass
        
        @abstract
        @args(ActionBar.Tab, [int])
        def setText(self, resId):
            pass
        
        @abstract
        @args(ActionBar.Tab, [CharSequence])
        def setText(self, text):
            pass
    
    class TabListener:
    
        @args(void, [ActionBar.Tab, FragmentTransaction])
        def onTabReselected(self, tab, ft):
            pass
        
        @args(void, [ActionBar.Tab, FragmentTransaction])
        def onTabSelected(self, tab, ft):
            pass
        
        @args(void, [ActionBar.Tab, FragmentTransaction])
        def onTabUnselected(self, tab, ft):
            pass
    
    def __init__(self):
        pass
    
    @abstract
    @args(void, [ActionBar.Tab, bool])
    def addTab(self, tab, setSelected):
        pass
    
    @abstract
    @args(void, [ActionBar.Tab, int])
    def addTab(self, tab, position):
        pass
    
    @abstract
    @args(void, [ActionBar.Tab, int, bool])
    def addTab(self, tab, position, setSelected):
        pass
    
    @abstract
    @args(void, [ActionBar.Tab])
    def addTab(self, tab):
        pass
    
    @abstract
    @args(View, [])
    def getCustomView(self):
        pass
    
    @abstract
    @args(int, [])
    def getDisplayOptions(self):
        pass
    
    @abstract
    @args(int, [])
    def getHeight(self):
        pass
    
    @abstract
    @args(int, [])
    def getHeight(self):
        pass
    
    @abstract
    @args(int, [])
    def getNavigationItemCount(self):
        pass
    
    @abstract
    @args(int, [])
    def getNavigationMode(self):
        pass
    
    @abstract
    @args(ActionBar.Tab, [])
    def getSelectedTab(self):
        pass
    
    @abstract
    @args(CharSequence, [])
    def getSubtitle(self):
        pass
    
    @abstract
    @args(ActionBar.Tab, [int])
    def getTabAt(self, index):
        pass
    
    @abstract
    @args(int, [])
    def getTabCount(self):
        pass
    
    @abstract
    @args(Context, [])
    def getThemedContext(self):
        pass
    
    @abstract
    @args(CharSequence, [])
    def getTitle(self):
        pass
    
    @abstract
    def hide(self):
        pass
    
    @abstract
    @args(bool, [])
    def isShowing(self):
        pass
    
    @abstract
    @args(ActionBar.Tab, [])
    def newTab(self):
        pass
    
    @abstract
    @args(void, [])
    def removeAllTabs(self):
        pass
    
    @abstract
    @args(void, [ActionBar.OnMenuVisibilityListener])
    def removeOnMenuVisibilityListener(self, listener):
        pass
    
    @abstract
    @args(void, [ActionBar.Tab])
    def removeTab(self, tab):
        pass
    
    @abstract
    @args(void, [int])
    def removeTabAt(self, position):
        pass
    
    @abstract
    @args(void, [ActionBar.Tab])
    def selectTab(self, tab):
        pass
    
    @abstract
    @args(void, [Drawable])
    def setBackgroundDrawable(self, drawable):
        pass
    
    @abstract
    @args(void, [int])
    def setCustomView(self, resId):
        pass
    
    @abstract
    @args(void, [View])
    def setCustomView(self, view):
        pass
    
    @abstract
    @args(void, [View, ActionBar.LayoutParams])
    def setCustomView(self, view, layoutParams):
        pass
    
    @abstract
    @args(void, [bool])
    def setDisplayHomeAsUpEnabled(self, showHomeAsUp):
        pass
    
    @abstract
    @args(void, [int, int])
    def setDisplayOptions(self, options, mask):
        pass
    
    @abstract
    @args(void, [int])
    def setDisplayOptions(self, options):
        pass
    
    @abstract
    @args(void, [bool])
    def setDisplayShowCustomEnabled(self, showCustom):
        pass
    
    @abstract
    @args(void, [bool])
    def setDisplayShowHomeEnabled(self, showHome):
        pass
    
    @abstract
    @args(void, [bool])
    def setDisplayShowTitleEnabled(self, showTitle):
        pass
    
    @abstract
    @args(void, [bool])
    def setDisplayUseLogoEnabled(self, showLogo):
        pass
    
    @args(void, [float])
    def setElevation(self, elevation):
        pass
    
    @args(void, [int])
    def setHideOffset(self, offset):
        pass
    
    @args(void, [bool])
    def setHideOnContentScrollEnabled(self, hideOnContentScroll):
        pass
    
    @args(void, [CharSequence])
    def setHomeActionContentDescription(self, description):
        pass
    
    @args(void, [int])
    def setHomeActionContentDescription(self, resId):
        pass
    
    @args(void, [Drawable])
    def setHomeAsUpIndicator(self, indicator):
        pass
    
    @args(void, [int])
    def setHomeAsUpIndicator(self, resId):
        pass
    
    @args(void, [bool])
    def setHomeButtonEnabled(self, enabled):
        pass
    
    @abstract
    @args(void, [Drawable])
    def setIcon(self, icon):
        pass
    
    @abstract
    @args(void, [int])
    def setIcon(self, resId):
        pass
    
    @abstract
    @args(void, [SpinnerAdapter, ActionBar.OnNavigationListener])
    def setListNavigationCallbacks(self, adapter, callback):
        pass
    
    @abstract
    @args(void, [int])
    def setLogo(self, resId):
        pass
    
    @abstract
    @args(void, [Drawable])
    def setLogo(self, logo):
        pass
    
    @abstract
    @args(void, [int])
    def setNavigationMode(self, mode):
        pass
    
    @abstract
    @args(void, [int])
    def setSelectedNavigationItem(self, position):
        pass
    
    @args(void, [Drawable])
    def setSplitBackgroundDrawable(self, drawable):
        pass
    
    @args(void, [Drawable])
    def setStackedBackgroundDrawable(self, drawable):
        pass
    
    @abstract
    @args(void, [int])
    def setSubtitle(self, resId):
        pass
    
    @abstract
    @args(void, [CharSequence])
    def setSubtitle(self, subtitle):
        pass
    
    @abstract
    @args(void, [int])
    def setTitle(self, resId):
        pass
    
    @abstract
    @args(void, [CharSequence])
    def setTitle(self, title):
        pass
    
    @abstract
    @args(void, [])
    def show(self):
        pass


class Activity(ContextThemeWrapper):

    __interfaces__ = [ComponentCallbacks2, View.OnCreateContextMenuListener,
                      Window.Callback]
    
    RESULT_CANCELED = 0
    RESULT_FIRST_USER = 1
    RESULT_OK = -1
    
    def __init__(self):
        pass
    
    @args(void, [View, ViewGroup.LayoutParams])
    def addContentView(self, view, params):
        pass
    
    def closeContextMenu(self):
        pass
    
    def closeOptionsMenu(self):
        pass
    
    @args(PendingIntent, [int, Intent, int])
    def createPendingResult(self, requestCode, data, flags):
        pass
    
    @args(final, [int])
    def dismissDialog(self, id):
        pass
    
    @args(bool, [MotionEvent])
    def dispatchGenericMotionEvent(self, ev):
        pass
    
    @args(bool, [KeyEvent])
    def dispatchKeyEvent(self, event):
        pass
    
    @args(bool, [KeyEvent])
    def dispatchKeyShortcutEvent(self, event):
        pass
    
    @args(bool, [AccessibilityEvent])
    def dispatchPopulateAccessibilityEvent(self, event):
        pass
    
    @args(bool, [MotionEvent])
    def dispatchTouchEvent(self, ev):
        pass
    
    @args(bool, [MotionEvent])
    def dispatchTrackballEvent(self, ev):
        pass
    
    @args(void, [String, FileDescriptor, PrintWriter, [String]])
    def dump(self, prefix, fd, writer, args):
        pass
    
    @args(View, [int])
    def findViewById(self, id):
        pass
    
    def finish(self):
        pass
    
    @args(void, [int])
    def finishActivity(self, requestCode):
        pass
    
    @args(void, [Activity, int])
    def finishActivityFromChild(self, child, requestCode):
        pass
    
    def finishAffinity(self):
        pass
    
    @args(void, [Activity])
    def finishFromChild(self, child):
        pass
    
    @args(ActionBar, [])
    def getActionBar(self):
        pass
    
    @args(ComponentName, [])
    def getCallingActivity(self):
        pass
    
    @args(String, [])
    def getCallingPackage(self):
        pass
    
    @args(int, [])
    def getChangingConfigurations(self):
        pass
    
    @args(ComponentName, [])
    def getComponentName(self):
        pass
    
    @args(View, [])
    def getCurrentFocus(self):
        pass
    
    @args(Intent, [])
    def getIntent(self):
        pass
    
    @args(String, [])
    def getLocalClassName(self):
        pass
    
    @final
    @args(Activity, [])
    def getParent(self):
        pass
    
    @args(Intent, [])
    def getParentActivityIntent(self):
        pass
    
    @args(SharedPreferences, [int])
    def getPreferences(self, mode):
        pass
    
    @args(int, [])
    def getRequestedOrientation(self):
        pass
    
    @args(Window, [])
    def getWindow(self):
        pass
    
    @args(WindowManager, [])
    def getWindowManager(self):
        pass
    
    @args(void, [int, int, Intent])
    def onActivityResult(self, requestCode, resultCode, data):
        pass
    
    def onBackPressed(self):
        pass
    
    @args(void, [Configuration])
    def onConfigurationChanged(self, config):
        pass

    def onContentChanged(self):
        pass
    
    @args(bool, [MenuItem])
    def onContextItemSelected(self, item):
        pass
    
    @args(void, [Menu])
    def onContextMenuClosed(self, menu):
        pass
    
    @args(void, [Bundle])
    def onCreate(self, bundle):
        pass
    
    @args(void, [Bundle, PersistableBundle])
    def onCreate(self, savedInstanceState, persistentState):
        pass
    
    @args(void, [ContextMenu, View, ContextMenu.ContextMenuInfo])
    def onCreateContextMenu(self, menu, v, menuInfo):
        pass
    
    @args(CharSequence, [])
    def onCreateDescription(self):
        pass
    
    @args(void, [TaskStackBuilder])
    def onCreateNavigateUpTaskStack(self, builder):
        pass
    
    @args(bool, [Menu])
    def onCreateOptionsMenu(self, menu):
        pass
    
    def onDestroy(self):
        pass
    
    @args(void, [Intent])
    def onNewIntent(self, intent):
        pass
    
    @args(bool, [MenuItem])
    def onOptionsItemSelected(self, item):
        pass
    
    @args(bool, [Menu])
    def onPrepareOptionsMenu(self, menu):
        pass
    
    def onPause(self):
        pass
    
    @args(void, [Bundle])
    def onPostCreate(self, savedInstanceState):
        pass
    
    def onPostResume(self):
        pass
    
    def onRestart(self):
        pass
    
    @args(void, [Bundle])
    def onRestoreInstanceState(self, savedInstanceState):
        pass
    
    def onResume(self):
        pass
    
    @args(void, [Bundle])
    def onSaveInstanceState(self, outState):
        pass
    
    def onStart(self):
        pass
    
    def onStop(self):
        pass
    
    @args(void, [CharSequence, int])
    def onTitleChanged(self, title, color):
        pass
    
    def onUserLeaveHint(self):
        pass
    
    # ...
    
    @args(void, [View])
    def openContextMenu(self, view):
        pass
    
    def openOptionsMenu(self):
        pass
    
    @args(void, [int, int])
    def overridePendingTransition(self, enterAnim, exitAnim):
        pass
    
    def postponeEnterTransition(self):
        pass
    
    def recreate(self):
        pass
    
    @args(void, [View])
    def registerForContextMenu(self, view):
        pass
    
    # ...
    
    @args(void, [int])
    def setContentView(self, layoutResID):
        pass
    
    @args(void, [View])
    def setContentView(self, view):
        pass
    
    @args(void, [Intent])
    def startActivity(self, intent):
        pass
    
    @args(void, [Intent, Bundle])
    def startActivity(self, intent, options):
        pass
    
    @args(void, [Intent, int])
    def startActivityForResult(self, intent, requestCode):
        pass
    
    @args(void, [Intent, int, Bundle])
    def startActivityForResult(self, intent, requestCode, options):
        pass
    
    # ...
    
    @args(void, [View])
    def unregisterForContextMenu(self, view):
        pass


class Dialog(Object):

    __interfaces__ = [DialogInterface, Window.Callback, KeyEvent.Callback,
                      View.OnCreateContextMenuListener]
    
    @args(void, [Context])
    def __init__(self, context):
        pass
    
    @args(void, [Context, int])
    def __init__(self, context, themeResId):
        pass
    
    @protected
    @args(void, [Context, bool, DialogInterface.OnCancelListener])
    def __init__(self, context, cancelable, cancelListener):
        pass
    
    @args(void, [View, ViewGroup.LayoutParams])
    def addContentView(self, view, params):
        pass
    
    def cancel(self):
        pass
    
    def closeOptionsMenu(self):
        pass
    
    def create(self):
        pass
    
    def dismiss(self):
        pass
    
    @args(bool, [MotionEvent])
    def dispatchGenericMotionEvent(self, event):
        pass
    
    @args(bool, [KeyEvent])
    def dispatchKeyEvent(self, event):
        pass
    
    @args(bool, [KeyEvent])
    def dispatchKeyShortcutEvent(self, event):
        pass
    
    @args(bool, [AccessibilityEvent])
    def dispatchPopulateAccessibilityEvent(self, event):
        pass
    
    @args(bool, [MotionEvent])
    def dispatchTouchEvent(self, event):
        pass
    
    @args(bool, [MotionEvent])
    def dispatchTrackballEvent(self, event):
        pass
    
    @args(View, [int])
    def findViewById(self, id):
        pass
    
    @args(ActionBar, [])
    def getActionBar(self):
        pass
    
    @final
    @args(Context, [])
    def getContext(self):
        pass
    
    @args(View, [])
    def getCurrentFocus(self):
        pass
    
    @args(LayoutInflater, [])
    def getLayoutInflater(self):
        pass
    
    @final
    @args(Activity, [])
    def getOwnerActivity(self):
        pass
    
    @final
    @args(SearchEvent, [])
    def getSearchEvent(self):
        pass
    
    @final
    @args(int, [])
    def getVolumeControlStream(self):
        pass
    
    @args(Window, [])
    def getWindow(self):
        pass
    
    def hide(self):
        pass
    
    def invalidateOptionsMenu(self):
        pass
    
    @args(bool, [])
    def isShowing(self):
        pass
    
    @args(void, [ActionMode])
    def onActionModeFinished(self, mode):
        pass
    
    @args(void, [ActionMode])
    def onActionModeStarted(self, mode):
        pass
    
    def onAttachedToWindow(self):
        pass
    
    def onBackPressed(self):
        pass
    
    def onContentChanged(self):
        pass
    
    @args(bool, [MenuItem])
    def onContextItemSelected(self, item):
        pass
    
    @args(void, [Menu])
    def onContextMenuClosed(self, menu):
        pass
    
    @args(void, [ContextMenu, View, ContextMenu.ContextMenuInfo])
    def onCreateContextMenu(self, menu, view, menuInfo):
        pass
    
    @args(bool, [Menu])
    def onCreateOptionsMenu(self, menu):
        pass
    
    @args(bool, [int, Menu])
    def onCreatePanelMenu(self, featureId, menu):
        pass
    
    @args(View, [int])
    def onCreatePanelView(self, featureId):
        pass
    
    def onDetachedFromWindow(self):
        pass
    
    @args(bool, [MotionEvent])
    def onGenericMotionEvent(self, event):
        pass
    
    @args(bool, [int, KeyEvent])
    def onKeyDown(self, keyCode, event):
        pass
    
    @args(bool, [int, KeyEvent])
    def onKeyLongPress(self, keyCode, event):
        pass
    
    @args(bool, [int, int, KeyEvent])
    def onKeyMultiple(self, keyCode, repeatCount, event):
        pass
    
    @args(bool, [int, KeyEvent])
    def onKeyShortcut(self, keyCode, event):
        pass
    
    @args(bool, [int, KeyEvent])
    def onKeyUp(self, keyCode, event):
        pass
    
    @args(bool, [int, MenuItem])
    def onMenuItemSelected(self, featureId, item):
        pass
    
    @args(bool, [int, Menu])
    def onMenuOpened(self, featureId, menu):
        pass
    
    @args(bool, [MenuItem])
    def onOptionsItemSelected(self, item):
        pass
    
    @args(void, [Menu])
    def onOptionsMenuClosed(self, menu):
        pass
    
    @args(void, [int, Menu])
    def onPanelClosed(self, featureId, menu):
        pass
    
    @args(bool, [Menu])
    def onPrepareOptionsMenu(self, menu):
        pass
    
    @args(bool, [int, View, Menu])
    def onPreparePanel(self, featureId, view, menu):
        pass
    
    @args(void, [Bundle])
    def onRestoreInstanceState(self, savedInstanceState):
        pass
    
    @args(Bundle, [])
    def onSaveInstanceState(self):
        pass
    
    @args(bool, [])
    def onSearchRequested(self):
        pass
    
    @args(bool, [SearchEvent])
    def onSearchRequested(self, searchEvent):
        pass
    
    @args(bool, [MotionEvent])
    def onTouchEvent(self, event):
        pass
    
    @args(bool, [MotionEvent])
    def onTrackballEvent(self, event):
        pass
    
    @args(void, [WindowManager.LayoutParams])
    def onWindowAttributesChanged(self, params):
        pass
    
    @args(void, [bool])
    def onWindowFocusChanged(self, hasFocus):
        pass
    
    @args(ActionMode, [ActionMode.Callback])
    def onWindowStartingActionMode(self, callback):
        pass
    
    @args(ActionMode, [ActionMode.Callback, int])
    def onWindowStartingActionMode(self, callback, type):
        pass
    
    @args(void, [View])
    def openContextMenu(self, view):
        pass
    
    def openOptionsMenu(self):
        pass
    
    @args(void, [View])
    def registerForContextMenu(self, view):
        pass
    
    @final
    @args(bool, [int])
    def requestWindowFeature(self, featureId):
        pass
    
    @args(void, [Message])
    def setCancelMessage(self, msg):
        pass
    
    @args(void, [bool])
    def setCancelable(self, flag):
        pass
    
    @args(void, [bool])
    def setCanceledOnTouchOutside(self, cancel):
        pass
    
    @args(void, [View])
    def setContentView(self, view):
        pass
    
    @args(void, [int])
    def setContentView(self, layoutResID):
        pass
    
    @args(void, [View, ViewGroup.LayoutParams])
    def setContentView(self, view, params):
        pass
    
    @args(void, [Message])
    def setDismissMessage(self, msg):
        pass
    
    @final
    @args(void, [int, Drawable])
    def setFeatureDrawable(self, featureId, drawable):
        pass
    
    @final
    @args(void, [int, int])
    def setFeatureDrawableAlpha(self, featureId, alpha):
        pass
    
    @final
    @args(void, [int, int])
    def setFeatureDrawableResource(self, featureId, resId):
        pass
    
    @final
    @args(void, [int, Uri])
    def setFeatureDrawableUri(self, featureId, uri):
        pass
    
    @args(void, [DialogInterface.OnCancelListener])
    def setOnCancelListener(self, listener):
        pass
    
    @args(void, [DialogInterface.OnDismissListener])
    def setOnDismissListener(self, listener):
        pass
    
    @args(void, [DialogInterface.OnKeyListener])
    def setOnKeyListener(self, onKeyListener):
        pass
    
    @args(void, [DialogInterface.OnShowListener])
    def setOnShowListener(self, listener):
        pass
    
    @final
    @args(void, [Activity])
    def setOwnerActivity(self, activity):
        pass
    
    @args(void, [int])
    def setTitle(self, titleId):
        pass
    
    @args(void, [CharSequence])
    def setTitle(self, title):
        pass
    
    @final
    @args(void, [int])
    def setVolumeControlStream(self, streamType):
        pass
    
    def show(self):
        pass
    
    @args(void, [bool])
    def takeKeyEvents(self, get):
        pass
    
    @args(void, [View])
    def unregisterForContextMenu(self, view):
        pass


class Fragment(Object):

    class SavedState(Object):
    
        pass
    
    def __init__(self):
        pass
    
    @args(Activity, [])
    def getActivity(self):
        pass
    
    @args(Bundle, [])
    def getArguments(self):
        pass
    
    @args(int, [])
    def getId(self):
        pass
    
    @args(Activity, [])
    def getActivity(self):
        pass
    
    @args(Fragment, [])
    def getParentFragment(self):
        pass
    
    @args(Resources, [])
    def getResources(self):
        pass
    
    @args(String, [int])
    def getString(self, resId):
        pass
    
    @args(String, [])
    def getTag(self):
        pass
    
    @args(Fragment, [])
    def getTargetFragment(self):
        pass
    
    @args(int, [])
    def getTargetRequestCode(self):
        pass
    
    @args(CharSequence, [int])
    def getText(self, resId):
        pass
    
    @args(bool, [])
    def getUserVisibleHint(self):
        pass
    
    @args(View, [])
    def getView(self):
        pass
    
    @args(int, [])
    def hashCode(self):
        pass
    
    @static
    @args(Fragment, [Context, String])
    def instantiate(context, fname):
        pass
    
    @static
    @args(Fragment, [Context, String, Bundle])
    def instantiate(context, fname, args):
        pass
    
    @args(bool, [])
    def isAdded(self):
        pass
    
    @args(bool, [])
    def isDetached(self):
        pass
    
    @args(bool, [])
    def isHidden(self):
        pass
    
    @args(bool, [])
    def isInLayout(self):
        pass
    
    @args(bool, [])
    def isRemoving(self):
        pass
    
    @args(bool, [])
    def isResumed(self):
        pass
    
    @args(bool, [])
    def isVisible(self):
        pass
    
    @args(void, [Bundle])
    def onActivityCreated(self, savedInstanceState):
        pass
    
    @args(void, [int, int, Intent])
    def onActivityResult(self, requestCode, resultCode, data):
        pass
    
    @args(void, [Activity])
    def onAttach(self, activity):
        pass
    
    @args(void, [Configuration])
    def onConfigurationChanged(self, newConfig):
        pass
    
    @args(void, [MenuItem])
    def onContextItemSelected(self, item):
        pass
    
    @args(void, [Bundle])
    def onCreate(self, savedInstanceState):
        pass
    
    @args(void, [Menu, MenuInflater])
    def onCreateOptionsMenu(self, menu, inflater):
        pass
    
    @args(View, [LayoutInflater, ViewGroup, Bundle])
    def onCreateView(self, inflater, container, savedInstanceState):
        pass
    
    def onDestroy(self):
        pass
    
    def onDestroyOptionsMenu(self):
        pass
    
    def onDestroyView(self):
        pass
    
    def onDetach(self):
        pass
    
    @args(void, [bool])
    def onHiddenChanged(self, hidden):
        pass
    
    def onLowMemory(self):
        pass
    
    @args(bool, [MenuItem])
    def onOptionsItemSelected(self, item):
        pass
    
    @args(void, [Menu])
    def onOptionsMenuClosed(self, menu):
        pass
    
    def onPause(self):
        pass
    
    @args(void, [Menu])
    def onPrepareOptionsMenu(self, menu):
        pass
    
    def onResume(self):
        pass
    
    @args(void, [Bundle])
    def onSaveInstanceState(self, outState):
        pass
    
    def onStart(self):
        pass
    
    def onStop(self):
        pass
    
    @args(void, [int])
    def onTrimMemory(self, level):
        pass
    
    @args(void, [View, Bundle])
    def onViewCreated(self, view, savedInstanceState):
        pass
    
    @args(void, [Bundle])
    def onViewStateRestored(self, savedInstanceState):
        pass
    
    @args(void, [View])
    def registerForContextMenu(self, view):
        pass
    
    @args(void, [Bundle])
    def setArguments(self, args):
        pass
    
    @args(void, [bool])
    def setHasOptionsMenu(self, hasMenu):
        pass
    
    @args(void, [Fragment.SavedState])
    def setInitialSavedState(self, state):
        pass
    
    @args(void, [bool])
    def setMenuVisibility(self, menuVisible):
        pass
    
    @args(void, [bool])
    def setRetainInstance(self, retain):
        pass
    
    @args(void, [Fragment, int])
    def setTargetFragment(self, fragment, requestCode):
        pass
    
    @args(void, [bool])
    def setUserVisibleHint(self, isVisibleToUser):
        pass
    
    @args(void, [Intent])
    def startActivity(self, intent):
        pass
    
    @args(void, [Intent, int])
    def startActivityForResult(self, intent, requestCode):
        pass
    
    @args(void, [Intent, int, Bundle])
    def startActivityForResult(self, intent, requestCode, options):
        pass
    
    @args(void, [View])
    def unregisterForContextMenu(self, view):
        pass


class FragmentTransaction(Object):

    TRANSIT_ENTER_MASK = 0x1000
    TRANSIT_EXIT_MASK = 0x2000
    TRANSIT_FRAGMENT_CLOSE = 0x2002
    TRANSIT_FRAGMENT_FADE = 0x1003
    TRANSIT_FRAGMENT_OPEN = 0x1001
    TRANSIT_NONE = 0
    TRANSIT_UNSET = 0xffffffff
    
    def __init__(self):
        pass
    
    @abstract
    @args(FragmentTransaction, [int, Fragment])
    def add(self, containerViewId, fragment):
        pass
    
    @abstract
    @args(FragmentTransaction, [Fragment, String])
    def add(self, fragment, tag):
        pass
    
    @abstract
    @args(FragmentTransaction, [int, Fragment, String])
    def add(self, containerViewId, fragment, tag):
        pass
    
    @abstract
    @args(FragmentTransaction, [View, String])
    def addSharedElement(self, sharedElement, name):
        pass
    
    @abstract
    @args(FragmentTransaction, [String])
    def addToBackStack(self, name):
        pass
    
    @abstract
    @args(FragmentTransaction, [Fragment])
    def attach(self, fragment):
        pass
    
    @abstract
    @args(int, [])
    def commit(self):
        pass
    
    @abstract
    @args(int, [])
    def commitAllowingStateLoss(self):
        pass
    
    @abstract
    @args(FragmentTransaction, [Fragment])
    def detach(self, fragment):
        pass
    
    @abstract
    @args(FragmentTransaction, [])
    def disallowAddToBackStack(self):
        pass
    
    @abstract
    @args(FragmentTransaction, [Fragment])
    def hide(self, fragment):
        pass
    
    @abstract
    @args(bool, [])
    def isAddToBackStackAllowed(self):
        pass
    
    @abstract
    @args(bool, [])
    def isEmpty(self):
        pass
    
    @abstract
    @args(FragmentTransaction, [Fragment])
    def remove(self, fragment):
        pass
    
    @abstract
    @args(FragmentTransaction, [int, Fragment, String])
    def replace(self, containerViewId, fragment, tag):
        pass
    
    @abstract
    @args(FragmentTransaction, [int, Fragment])
    def replace(self, containerViewId, fragment):
        pass
    
    @abstract
    @args(FragmentTransaction, [int])
    def setBreadCrumbShortTitle(self, res):
        pass
    
    @abstract
    @args(FragmentTransaction, [CharSequence])
    def setBreadCrumbShortTitle(self, text):
        pass
    
    @abstract
    @args(FragmentTransaction, [CharSequence])
    def setBreadCrumbTitle(self, text):
        pass
    
    @abstract
    @args(FragmentTransaction, [int])
    def setBreadCrumbTitle(self, res):
        pass
    
    @abstract
    @args(FragmentTransaction, [int, int, int, int])
    def setCustomAnimations(self, enter, exit, popEnter, popExit):
        pass
    
    @abstract
    @args(FragmentTransaction, [int, int])
    def setCustomAnimations(self, enter, exit):
        pass
    
    @abstract
    @args(FragmentTransaction, [int])
    def setTransition(self, transition):
        pass
    
    @abstract
    @args(FragmentTransaction, [int])
    def setTransitionStyle(self, styleResourceId):
        pass
    
    @abstract
    @args(FragmentTransaction, [Fragment])
    def show(self, fragment):
        pass


class ListActivity(Activity):

    def __init__(self):
        pass
    
    @args(ListAdapter, [])
    def getListAdapter(self):
        pass
    
    @args(ListView, [])
    def getListView(self):
        pass
    
    @args(long, [])
    def getSelectedItemId(self):
        pass
    
    @args(int, [])
    def getSelectedItemPosition(self):
        pass
    
    @args(void, [])
    def onContentChanged(self):
        pass
    
    @args(void, [ListAdapter])
    def setListAdapter(self, adapter):
        pass
    
    @args(void, [int])
    def setSelection(self, position):
        pass
    
    @protected
    def onDestroy(self):
        pass
    
    @protected
    @args(void, [ListView, View, int, long])
    def onListItemClick(self, l, v, position, id):
        pass
    
    @protected
    @args(void, [Bundle])
    def onRestoreInstanceState(self, state):
        pass


class Notification(Object):

    # API level 19
    class Action(Object):
    
        __interfaces__ = [Parcelable]
        
        # API level 19
        __fields__ = {
            "CREATOR": Parcelable.Creator(Notification.Action),
            "actionIntent": PendingIntent,
            "icon": int,                    # deprecated in API level 23
            "title": CharSequence
            }
        
        # API level 20
        class Builder(Object):
        
            @api(20)
            @args(void, [int, CharSequence, PendingIntent])
            def __init__(self, icon, title, intent):
                pass
            
            @api(23)
            @args(void, [Icon, CharSequence, PendingIntent])
            def __init__(self, icon, title, intent):
                pass
            
            @api(20)
            @args(void, [Notification.Action])
            def __init__(self, action):
                pass
            
            @api(20)
            @args(Notification.Action.Builder, [Bundle])
            def addExtras(self, extras):
                pass
            
            @api(20)
            @args(Notification.Action.Builder, [RemoteInput])
            def addRemoteInput(self, remoteInput):
                pass
            
            @api(20)
            @args(Notification.Action, [])
            def build(self):
                pass
            
            @api(20)
            @args(Notification.Action.Builder, [Notification.Action.Extender])
            def extend(self, extender):
                pass
            
            @api(20)
            @args(Bundle, [])
            def getExtras(self):
                pass
        
        # API level 20
        class Extender:
        
            @api(20)
            @args(Notification.Action.Builder, [Notification.Action.Builder])
            def extend(self, builder):
                pass
        
        # API level 20
        class WearableExtender(Object):
        
            __interfaces__ = [Notification.Action.Extender]
            
            @api(20)
            def __init__(self):
                pass
            
            @api(20)
            @args(void, [Notification.Action])
            def __init__(self, action):
                pass
            
            @api(20)
            @args(Notification.Action.WearableExtender, [])
            def clone(self):
                pass
            
            @api(20)
            @args(Notification.Action.Builder, [Notification.Action.Builder])
            def extend(self, builder):
                pass
            
            @api(22)
            @args(CharSequence, [])
            def getCancelLabel(self):
                pass
            
            @api(22)
            @args(CharSequence, [])
            def getConfirmLabel(self):
                pass
            
            @api(22)
            @args(CharSequence, [])
            def getInProgressLabel(self):
                pass
            
            @api(20)
            @args(bool, [])
            def isAvailableOffline(self):
                pass
            
            @api(20)
            @args(Notification.Action.WearableExtender, [bool])
            def setAvailableOffline(self, availableOffline):
                pass
            
            @api(22)
            @args(Notification.Action.WearableExtender, [CharSequence])
            def setCancelLabel(self, label):
                pass
            
            @api(22)
            @args(Notification.Action.WearableExtender, [CharSequence])
            def setConfirmLabel(self, label):
                pass
            
            @api(22)
            @args(Notification.Action.WearableExtender, [CharSequence])
            def setInProgressLabel(self, label):
                pass
        
        @api(19, 23)
        @args(void, [int, CharSequence, PendingIntent])
        def __init__(self, icon, title, intent):
            pass
        
        @api(19)
        @args(Notification.Action, [])
        def clone(self):
            pass
        
        @api(19)
        @args(int, [])
        def describeContents(self):
            pass
        
        @api(20)
        @args(Bundle, [])
        def getExtras(self):
            pass
        
        @api(23)
        @args(Icon, [])
        def getIcon(self):
            pass
        
        @api(20)
        @args([RemoteInput], [])
        def getRemoteInputs(self):
            pass
        
        @api(19)
        @args(void, [Parcel, int])
        def writeToParcel(self, out, flags):
            pass
    
    # API level 16
    class BigPictureStyle(Notification.Style):
    
        @api(16)
        def __init__(self):
            pass
        
        @api(16)
        @args(void, [Notification.Builder])
        def __init__(self, builder):
            pass
        
        @api(23)
        @args(Notification.BigPictureStyle, [Icon])
        def bigLargeIcon(self, icon):
            pass
        
        @api(16)
        @args(Notification.BigPictureStyle, [Bitmap])
        def bigLargeIcon(self, b):
            pass
        
        @api(16)
        @args(Notification.BigPictureStyle, [Bitmap])
        def bigPicture(self, b):
            pass
        
        @api(16)
        @args(Notification.BigPictureStyle, [CharSequence])
        def setBigContentTitle(self, title):
            pass
        
        @api(16)
        @args(Notification.BigPictureStyle, [CharSequence])
        def setSummaryText(self, cs):
            pass
    
    # API level 16
    class BigTextStyle(Notification.Style):
    
        @api(16)
        def __init__(self):
            pass
        
        @api(16)
        @api(void, [Notification.Builder])
        def __init__(self, builder):
            pass
        
        @api(16)
        @args(Notification.BigTextStyle, [CharSequence])
        def bigText(self, cs):
            pass
        
        @api(16)
        @args(Notification.BigTextStyle, [CharSequence])
        def setBigContentTitle(self, title):
            pass
        
        @api(16)
        @args(Notification.BigTextStyle, [CharSequence])
        def setSummaryText(self, cs):
            pass
    
    # API level 11
    class Builder(Object):
    
        @api(11)
        @args(void, [Context])
        def __init__(self, context):
            pass
        
        @api(20)
        @args(Notification.Builder, [Notification.Action])
        def addAction(self, action):
            pass
        
        @api(16)
        @args(Notification.Builder, [int, CharSequence, PendingIntent])
        def addAction(self, icon, title, intent):
            pass
        
        @api(20)
        @args(Notification.Builder, [Bundle])
        def addExtras(self, extras):
            pass
        
        @api(21)
        @args(Notification.Builder, [String])
        def addPerson(self, uri):
            pass
        
        @api(16)
        @args(Notification, [])
        def build(self):
            pass
        
        @api(20)
        @args(Notification.Builder, [Notification.Extender])
        def extend(self, extender):
            pass
        
        @api(20)
        @args(Bundle, [])
        def getExtras(self):
            pass
        
        @api(11, 16)
        @args(Notification, [])
        def getNotification(self):
            pass
        
        @api(11)
        @args(Notification.Builder, [bool])
        def setAutoCancel(self, autoCancel):
            pass
        
        @api(21)
        @args(Notification.Builder, [String])
        def setCategory(self, category):
            pass
        
        @api(21)
        @args(Notification.Builder, [int])
        def setColor(self, argb):
            pass
        
        @api(11)
        @args(Notification.Builder, [RemoteViews])
        def setContent(self, views):
            pass
        
        @api(11)
        @args(Notification.Builder, [CharSequence])
        def setContentInfo(self, info):
            pass
        
        @api(11)
        @args(Notification.Builder, [PendingIntent])
        def setContentIntent(self, intent):
            pass
        
        @api(11)
        @args(Notification.Builder, [CharSequence])
        def setContentText(self, text):
            pass
        
        @api(11)
        @args(Notification.Builder, [CharSequence])
        def setContentTitle(self, title):
            pass
        
        @api(11)
        @args(Notification.Builder, [int])
        def setDefaults(self, defaults):
            pass
        
        @api(11)
        @args(Notification.Builder, [PendingIntent])
        def setDeleteIntent(self, intent):
            pass
        
        @api(19)
        @args(Notification.Builder, [Bundle])
        def setExtras(self, extras):
            pass
        
        @api(11)
        @args(Notification.Builder, [PendingIntent, bool])
        def setFullScreenIntent(self, intent, highPriority):
            pass
        
        @api(20)
        @args(Notification.Builder, [String])
        def setGroup(self, groupKey):
            pass
        
        @api(20)
        @args(Notification.Builder, [bool])
        def setGroupSummary(self, isGroupSummary):
            pass
        
        @api(23)
        @args(Notification.Builder, [Icon])
        def setLargeIcon(self, icon):
            pass
        
        @api(11)
        @args(Notification.Builder, [Bitmap])
        def setLargeIcon(self, b):
            pass
        
        @api(11)
        @args(Notification.Builder, [int, int, int])
        def setLights(self, argb, onMs, offMs):
            pass
        
        @api(20)
        @args(Notification.Builder, [bool])
        def setLocalOnly(self, localOnly):
            pass
        
        @api(11)
        @args(Notification.Builder, [int])
        def setNumber(self, number):
            pass
        
        @api(11)
        @args(Notification.Builder, [bool])
        def setOngoing(self, ongoing):
            pass
        
        @api(11)
        @args(Notification.Builder, [bool])
        def setOnlyAlertOnce(self, onlyAlertOnce):
            pass
        
        @api(16)
        @args(Notification.Builder, [int])
        def setPriority(self, pri):
            pass
        
        @api(14)
        @args(Notification.Builder, [int, int, bool])
        def setProgress(self, max, progress, indeterminate):
            pass
        
        @api(21)
        @args(Notification.Builder, [Notification])
        def setPublicVersion(self, n):
            pass
        
        @api(17)
        @args(Notification.Builder, [bool])
        def setShowWhen(self, show):
            pass
        
        @api(11)
        @args(Notification.Builder, [int, int])
        def setSmallIcon(self, icon, level):
            pass
        
        @api(11)
        @args(Notification.Builder, [int])
        def setSmallIcon(self, icon):
            pass
        
        @api(23)
        @args(Notification.Builder, [Icon])
        def setSmallIcon(self, icon):
            pass
        
        @api(20)
        @args(Notification.Builder, [String])
        def setSortKey(self, sortKey):
            pass
        
        @api(21)
        @args(Notification.Builder, [Uri, AudioAttributes])
        def setSound(self, sound, audioAttributes):
            pass
        
        @api(11)
        @args(Notification.Builder, [Uri])
        def setSound(self, sound):
            pass
        
        @api(11, 21)
        @args(Notification.Builder, [Uri, int])
        def setSound(self, sound, streamType):
            pass
        
        @api(16)
        @args(Notification.Builder, [Notification.Style])
        def setStyle(self, style):
            pass
        
        @api(16)
        @args(Notification.Builder, [CharSequence])
        def setSubText(self, text):
            pass
        
        @api(11)
        @args(Notification.Builder, [CharSequence, RemoteViews])
        def setTicker(self, tickerText, views):
            pass
        
        @api(11)
        @args(Notification.Builder, [CharSequence])
        def setTicker(self, tickerText):
            pass
        
        @api(16)
        @args(Notification.Builder, [bool])
        def setUsesChronometer(self, b):
            pass
        
        @api(11)
        @args(Notification.Builder, [[long]])
        def setVibrate(self, pattern):
            pass
        
        @api(21)
        @args(Notification.Builder, [int])
        def setVisibility(self, visibility):
            pass
        
        @api(11)
        @args(Notification.Builder, [long])
        def setWhen(self, when):
            pass
    
    # API level 23
    class CarExtender(Object):
    
        __interfaces__ = [Notification.Extender]
        
        # API level 23
        class Builder(Object):
        
            @api(23)
            @args(void, [String])
            def __init__(self, name):
                pass
            
            @api(23)
            @args(Notification.CarExtender.Builder, [String])
            def addMessage(self, message):
                pass
            
            @api(23)
            @args(Notification.CarExtender.UnreadConversation, [])
            def build(self):
                pass
            
            @api(23)
            @args(Notification.CarExtender.Builder, [long])
            def setLatestTimestamp(self, timestamp):
                pass
            
            @api(23)
            @args(Notification.CarExtender.Builder, [PendingIntent])
            def setReadPendingIntent(self, pendingIntent):
                pass
            
            @api(23)
            @args(Notification.CarExtender.Builder, [PendingIntent, RemoteInput])
            def setReplyAction(self, pendingIntent, remoteInput):
                pass
        
        # API level 23
        class UnreadConversation(Object):
        
            @api(23)
            @args(long, [])
            def getLatestTimestamp(self):
                pass
            
            @api(23)
            @args([String], [])
            def getMessages(self):
                pass
            
            @api(23)
            @args(String, [])
            def getParticipant(self):
                pass
            
            @api(23)
            @args([String], [])
            def getParticipants(self):
                pass
            
            @api(23)
            @args(PendingIntent, [])
            def getReadPendingIntent(self):
                pass
            
            @api(23)
            @args(RemoteInput, [])
            def getRemoteInput(self):
                pass
            
            @api(23)
            @args(PendingIntent, [])
            def getReplyPendingIntent(self):
                pass
        
        @api(23)
        def __init__(self):
            pass
        
        @api(23)
        @args(void, [Notification])
        def __init__(self, notif):
            pass
        
        @api(23)
        @args(Notification.Builder, [Notification.Builder])
        def extend(self, builder):
            pass
        
        @api(23)
        @args(int, [])
        def getColor(self):
            pass
        
        @api(23)
        @args(Bitmap, [])
        def getLargeIcon(self):
            pass
        
        @api(23)
        @args(Notification.CarExtender.UnreadConversation, [])
        def getUnreadConversation(self):
            pass
        
        @api(23)
        @args(Notification.CarExtender, [int])
        def setColor(self, color):
            pass
        
        @api(23)
        @args(Notification.CarExtender, [Bitmap])
        def setLargeIcon(self, largeIcon):
            pass
        
        @api(23)
        @args(Notification.CarExtender, [Notification.CarExtender.UnreadConversation])
        def setUnreadConversation(self, unreadConversation):
            pass
    
    # API level 20
    class Extender:
    
        @api(20)
        @args(Notification.Builder, [Notification.Builder])
        def extend(self, builder):
            pass
    
    # API level 16
    class InboxStyle(Notification.Style):
    
        @api(16)
        def __init__(self):
            pass
        
        @api(16)
        @args(void, [Notification.Builder])
        def __init__(self, builder):
            pass
        
        @api(16)
        @args(Notification.InboxStyle, [CharSequence])
        def addLine(self, cs):
            pass
        
        @api(16)
        @args(Notification.InboxStyle, [CharSequence])
        def setBigContentTitle(self, title):
            pass
        
        @api(16)
        @args(Notification.InboxStyle, [CharSequence])
        def setSummaryText(self, cs):
            pass
    
    # API level 21
    class MediaStyle(Notification.Style):
    
        @api(21)
        def __init__(self):
            pass
        
        @api(21)
        @args(void, [Notification.Builder])
        def __init__(self, builder):
            pass
        
        @api(21)
        @args(Notification.MediaStyle, [MediaSession.Token])
        def setMediaSession(self, token):
            pass
        
        @api(21)
        @args(Notification.MediaStyle, [[int]]) # int...
        def setShowActionsInCompactView(self, actions):
            pass
    
    # API level 16
    class Style(Object):
    
        # protected
        __fields__ = {"mBuilder": Notification.Builder}
        
        @api(16)
        def __init__(self):
            pass
        
        @api(16)
        @args(Notification, [])
        def build(self):
            pass
        
        @api(16)
        @args(void, [Notification.Builder])
        def setBuilder(self, builder):
            pass
        
        @api(16)
        @protected
        @args(void, [])
        def checkBuilder(self):
            pass
        
        @api(16)
        @protected
        @args(RemoteViews, [int])
        def getStandardView(self, layoutId):
            pass
        
        @api(16)
        @protected
        @args(void, [CharSequence])
        def internalSetBigContentTitle(self, title):
            pass
        
        @api(16)
        @protected
        @args(void, [CharSequence])
        def internalSetSummaryText(self, cs):
            pass
    
    ### class WearableExtender
    
    # API level 21
    CATEGORY_ALARM = "alarm"
    CATEGORY_CALL = "call"
    CATEGORY_EMAIL = "email"
    CATEGORY_ERROR = "err"
    CATEGORY_EVENT = "event"
    CATEGORY_MESSAGE = "msg"
    CATEGORY_PROGRESS = "progress"
    CATEGORY_PROMO = "promo"
    CATEGORY_RECOMMENDATION = "recommendation"
    # API level 23
    CATEGORY_REMINDER = "reminder"
    # API level 21
    CATEGORY_SERVICE = "service"
    CATEGORY_SOCIAL = "social"
    CATEGORY_STATUS = "status"
    CATEGORY_SYSTEM = "sys"
    CATEGORY_TRANSPORT = "transport"
    COLOR_DEFAULT = 0
    
    DEFAULT_ALL = 0xffffffff
    DEFAULT_LIGHTS = 4
    DEFAULT_SOUND = 1
    DEFAULT_VIBRATE = 2
    
    # API level 21
    EXTRA_BACKGROUND_IMAGE_URI = "android.backgroundImageUri"
    EXTRA_BIG_TEXT = "android.bigText"
    EXTRA_COMPACT_ACTIONS = "android.compactActions"
    # API level 19
    EXTRA_INFO_TEXT = "android.infoText"
    EXTRA_LARGE_ICON = "android.largeIcon"
    EXTRA_LARGE_ICON_BIG = "android.largeIcon.big"
    EXTRA_MEDIA_SESSION = "android.mediaSession"
    EXTRA_PEOPLE = "android.people"
    EXTRA_PICTURE = "android.picture"
    EXTRA_PROGRESS = "android.progress"
    EXTRA_PROGRESS_INDETERMINATE = "android.progressIndeterminate"
    EXTRA_PROGRESS_MAX = "android.progressMax"
    EXTRA_SHOW_CHRONOMETER = "android.showChronometer"
    EXTRA_SHOW_WHEN = "android.showWhen"
    EXTRA_SMALL_ICON = "android.icon"
    EXTRA_SUB_TEXT = "android.subText"
    EXTRA_SUMMARY_TEXT = "android.summaryText"
    # API level 21
    EXTRA_TEMPLATE = "android.template"
    # API level 19
    EXTRA_TEXT = "android.text"
    EXTRA_TEXT_LINES = "android.textLines"
    EXTRA_TITLE = "android.title"
    EXTRA_TITLE_BIG = "android.title.big"

    FLAG_AUTO_CANCEL = 0x10
    # API level 5
    FLAG_FOREGROUND_SERVICE = 0x40
    # API level 20
    FLAG_GROUP_SUMMARY = 0x200
    # API level 11-16
    FLAG_HIGH_PRIORITY = 0x80
    
    FLAG_INSISTENT = 0x4
    
    # API level 20
    FLAG_LOCAL_ONLY = 0x100
    
    FLAG_NO_CLEAR = 0x20
    FLAG_ONGOING_EVENT = 0x2
    FLAG_ONLY_ALERT_ONCE = 0x8
    FLAG_SHOW_LIGHTS = 0x1
    
    # API level 21
    INTENT_CATEGORY_NOTIFICATION_PREFERENCES = "android.intent.category.NOTIFICATION_PREFERENCES"
    # API level 16
    PRIORITY_DEFAULT = 0
    PRIORITY_HIGH = 1
    PRIORITY_LOW = -1
    PRIORITY_MAX = 2
    PRIORITY_MIN = -2
    # API level 1-21
    STREAM_DEFAULT = -1
    # API level 21
    VISIBILITY_PRIVATE = 0
    VISIBILITY_PUBLIC = 1
    VISIBILITY_SECRET = -1
    
    __static_fields__ = {
        "AUDIO_ATTRIBUTES_DEFAULT": AudioAttributes,    # API level 21
        "CREATOR": Parcelable.Creator(Notification)
        }
    
    __fields__ = {
        "actions": [Action],                # API level 19
        "audioAttributes": AudioAttributes, # API level 21
        "audioStreamType": int,
        "bigContentView": RemoteViews,      # API level 16
        "category": String,                 # API level 21
        "color": int,
        "contentIntent": PendingIntent,
        "contentView": RemoteViews,
        "defaults": int,
        "deleteIntent": PendingIntent,
        "extras": Bundle,                   # API level 19
        "flags": int,
        "fullScreenIntent": PendingIntent,  # API level 9
        "headsUpContentView": RemoteViews,  # API level 21
        "icon": int,
        "iconLevel": int,
        "largeIcon": Bitmap,                # API level 11
        "ledARGB": int,
        "ledOffMS": int,
        "ledOnMS": int,
        "number": int,
        "priority": int,                    # API level 16
        "publicVersion": Notification,      # API level 21
        "sound": Uri,
        "tickerText": CharSequence,
        "tickerView": RemoteViews,          # API level 11
        "vibrate": [long],
        "visibility": int,                  # API level 21
        "when": long
        }
    
    def __init__(self):
        pass
    
    @args(void, [int, CharSequence, long])
    def __init__(self, icon, tickerText, when):
        pass
    
    @args(void, [Parcel])
    def __init__(self, parcel):
        pass
    
    @args(Notification, [])
    def clone(self):
        pass
    
    @args(int, [])
    def describeContents(self):
        pass
    
    @api(20)
    @args(String, [])
    def getGroup(self):
        pass
    
    @api(23)
    @args(Icon, [])
    def getLargeIcon(self):
        pass
    
    @api(23)
    @args(Icon, [])
    def getSmallIcon(self):
        pass
    
    @api(20)
    @args(String, [])
    def getSortKey(self):
        pass
    
    @args(String, [])
    def toString(self):
        pass
    
    @args(void, [Parcel, int])
    def writeToParcel(parcel, flags):
        pass


class NotificationManager(Object):

    # API level 23
    class Policy(Object):
    
        __interfaces__ = [Parcelable]
        
        # API level 23
        __fields__ = {
            "CREATOR": Parcelable.Creator(NotificationManager.Policy),
            "priorityCallSenders": int,
            "priorityCategories": int,
            "priorityMessageSenders": int
            }
        
        PRIORITY_CATEGORY_CALLS = 8
        PRIORITY_CATEGORY_EVENTS = 2
        PRIORITY_CATEGORY_MESSAGES = 4
        PRIORITY_CATEGORY_REMINDERS = 1
        PRIORITY_CATEGORY_REPEAT_CALLERS = 16
        PRIORITY_SENDERS_ANY = 0
        PRIORITY_SENDERS_CONTACTS = 1
        PRIORITY_SENDERS_STARRED = 2
        
        @api(23)
        @args(void, [int, int, int])
        def __init__(self, priorityCategories, priorityCallSenders, priorityMessageSenders):
            pass
        
        @api(23)
        @args(int, [])
        def describeContents(self):
            pass
        
        @api(23)
        @args(bool, [Object])
        def equals(self, o):
            pass
        
        @api(23)
        @args(int, [])
        def hashCode(self):
            pass
        
        @api(23)
        @static
        @args(String, [int])
        def priorityCategoriesToString(priorityCategories):
            pass
        
        @api(23)
        @static
        @args(String, [int])
        def prioritySendersToString(prioritySenders):
            pass
        
        @api(23)
        @args(String, [])
        def toString(self):
            pass
        
        @api(23)
        @args(void, [Parcel, int])
        def writeToParcel(self, dest, flags):
            pass
    
    # API level 23
    ACTION_INTERRUPTION_FILTER_CHANGED = "android.app.action.INTERRUPTION_FILTER_CHANGED"
    ACTION_NOTIFICATION_POLICY_ACCESS_GRANTED_CHANGED = "android.app.action.NOTIFICATION_POLICY_ACCESS_GRANTED_CHANGED"
    ACTION_NOTIFICATION_POLICY_CHANGED = "android.app.action.NOTIFICATION_POLICY_CHANGED"
    INTERRUPTION_FILTER_ALARMS = 4
    INTERRUPTION_FILTER_ALL = 1
    INTERRUPTION_FILTER_NONE = 3
    INTERRUPTION_FILTER_PRIORITY = 2
    INTERRUPTION_FILTER_UNKNOWN = 0
    
    @args(void, [int])
    def cancel(self, id):
        pass
    
    @api(5)
    @args(void, [String, int])
    def cancel(self, tag, id):
        pass
    
    def cancelAll(self):
        pass
    
    @api(23)
    @args([StatusBarNotification], [])
    def getActiveNotifications(self):
        pass
    
    @api(23)
    @final
    @args(int, [])
    def getCurrentInterruptionFilter(self):
        pass
    
    @api(23)
    @args(NotificationManager.Policy, [])
    def getNotificationPolicy(self):
        pass
    
    @api(23)
    @args(bool, [])
    def isNotificationPolicyAccessGranted(self):
        pass
    
    @args(void, [int, Notification])
    def notify(self, id, notification):
        pass
    
    @api(5)
    @args(void, [String, int, Notification])
    def notify(self, tag, id, notification):
        pass
    
    @api(23)
    @final
    @args(void, [int])
    def setInterruptionFilter(self, interruptionFilter):
        pass
    
    @api(23)
    @args(void, [NotificationManager.Policy])
    def setNotificationPolicy(self, policy):
        pass


class PendingIntent(Object):

    __interfaces__ = [Parcelable]
    __fields__ = {
        "CREATOR": Parcelable.Creator(PendingIntent)
        }
    
    FLAG_CANCEL_CURRENT = 0x10000000
    FLAG_IMMUTABLE      = 0x04000000
    FLAG_NO_CREATE      = 0x20000000
    FLAG_ONE_SHOT       = 0x40000000
    FLAG_UPDATE_CURRENT = 0x08000000
    
    class CanceledException(AndroidException):
    
        def __init__(self):
            pass
        
        @args(void, [String])
        def __init__(self):
            pass
        
        @args(void, [Exception])
        def __init__(self):
            pass
    
    class OnFinished:
    
        @args(void, [PendingIntent, Intent, int, String, Bundle])
        def onSendFinished(self, pendingIntent, intent, resultCode, resultData,
                           resultExtras):
            pass
    
    def cancel(self):
        pass
    
    @static
    @args(PendingIntent, [Context, int, [Intent], int])
    def getActivities(context, requestCode, intents, flags):
        pass
    
    @static
    @args(PendingIntent, [Context, int, [Intent], int, Bundle])
    def getActivities(context, requestCode, intents, flags, options):
        pass
    
    @static
    @args(PendingIntent, [Context, int, Intent, int])
    def getActivity(context, requestCode, intent, flags):
        pass
    
    @static
    @args(PendingIntent, [Context, int, Intent, int])
    def getActivity(context, requestCode, intent, flags):
        pass
    
    @static
    @args(PendingIntent, [Context, int, Intent, int])
    def getBroadcast(context, requestCode, intent, flags):
        pass
    
    @args(String, [])
    def getCreatorPackage(self):
        pass
    
    @args(int, [])
    def getCreatorUid(self):
        pass
    
    @args(UserHandle, [])
    def getCreatorUserHandle(self):
        pass
    
    @args(IntentSender, [])
    def getIntentSender(self):
        pass
    
    @static
    @args(PendingIntent, [Context, int, Intent, int])
    def getService(context, requestCode, intent, flags):
        pass
    
    @args(String, [])
    def getTargetPackage(self):
        pass
    
    @static
    @args(PendingIntent, [Parcel])
    def readPendingIntentOrNullFromParcel(input):
        pass
    
    @args(void, [int])
    def send(self, code):
        pass
    
    @args(void, [Context, int, Intent, PendingIntent.OnFinished, Handler, String])
    def send(self, context, code, intent, onFinished, handler, requiredPermission):
        pass
    
    @args(void, [Context, int, Intent, PendingIntent.OnFinished, Handler, String, Bundle])
    def send(self, context, code, intent, onFinished, handler, requiredPermission, options):
        pass
    
    @args(void, [int, PendingIntent.OnFinished, Handler])
    def send(self, code, onFinished, handler):
        pass
    
    @args(void, [Context, int, Intent, PendingIntent.OnFinished, Handler])
    def send(self, context, code, intent, onFinished, handler):
        pass
    
    def send(self):
        pass
    
    @args(void, [Context, int, Intent])
    def send(self, context, code, intent):
        pass
    
    @static
    @args(void, [PendingIntent, Parcel])
    def writePendingIntentOrNullToParcel(sender, output):
        pass


class Service(ContextWrapper):

    __interfaces__ = [ComponentCallbacks2]
    
    START_CONTINUATION_MASK = 15
    START_FLAG_REDELIVERY = 1
    START_FLAG_RETRY = 2
    START_NOT_STICKY = 2
    START_REDELIVER_INTENT = 3
    START_STICKY = 1
    START_STICKY_COMPATIBILITY = 0
    
    def __init__(self):
        pass
    
    @abstract
    @args(IBinder, [Intent])
    def onBind(self, intent):
        pass
    
    @args(void, [Configuration])
    def onConfigurationChanged(self, newConfig):
        pass
    
    def onCreate(self):
        pass
    
    def onDestroy(self):
        pass
    
    def onLowMemory(self):
        pass
    
    @args(void, [Intent])
    def onRebind(self, intent):
        pass
    
    @args(int, [Intent, int, int])
    def onStartCommand(self, intent, flags, startId):
        pass
    
    @args(void, [Intent])
    def onTaskRemoved(self, rootIntent):
        pass
    
    @args(void, [int])
    def onTrimMemory(self, level):
        pass
    
    @args(bool, [Intent])
    def onUnbind(self, intent):
        pass
    
    #@final
    #@args(void, [int, Notification])
    #def startForeground(self, id, notification):
    #    pass
    
    #@final
    #@args(void, [bool])
    #def stopForeground(self, removeNotification):
    #    pass
    
    @final
    def stopSelf(self):
        pass
    
    @final
    @args(void, [int])
    def stopSelf(self, startId):
        pass
    
    @final
    @args(bool, [int])
    def stopSelfResult(self, startId):
        pass


# API level 18
class StatusBarNotification(Object):

    __interfaces__ = [Parcelable]
    
    # API level 18
    __fields__ = {
        "CREATOR": Parcelable.Creator(StatusBarNotification)
        }
    
    @api(18)
    @args(void, [String, String, int, String, int, int, int, Notification, UserHandle, long])
    def __init__(self, pkg, opPkg, id, tag, uid, initialPid, score, notification, user, postTime):
        pass
    
    @api(18)
    @args(void, [Parcel])
    def __init__(self, in_):
        pass
    
    @api(18)
    @args(StatusBarNotification, [])
    def clone(self):
        pass
    
    @api(18)
    @args(int, [])
    def describeContents(self):
        pass
    
    @api(21)
    @args(String, [])
    def getGroupKey(self):
        pass
    
    @api(18)
    @args(int, [])
    def getId(self):
        pass
    
    @api(20)
    @args(String, [])
    def getKey(self):
        pass
    
    @api(18)
    @args(Notification, [])
    def getNotification(self):
        pass
    
    @api(18)
    @args(String, [])
    def getPackageName(self):
        pass
    
    @api(18)
    @args(long, [])
    def getPostTime(self):
        pass
    
    @api(18)
    @args(String, [])
    def getTag(self):
        pass
    
    @api(21)
    @args(UserHandle, [])
    def getUser(self):
        pass
    
    @args(int, [])
    def getUserId(self):
        pass
    
    @args(bool, [])
    def isClearable(self):
        pass
    
    @args(bool, [])
    def isOngoing(self):
        pass
    
    @args(String, [])
    def toString(self):
        pass
    
    @args(void, [Parcel, int])
    def writeToParcel(self, out, flags):
        pass
