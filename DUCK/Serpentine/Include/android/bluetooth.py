# Class declarations obtained from Android API documentation licensed under the
# Apache 2.0 license (http://www.apache.org/licenses/LICENSE-2.0) and rewritten
# in syntax that the Python compiler module can parse.

__package__ = "android.bluetooth"

from java.io import Closeable, InputStream, OutputStream
from java.lang import Object, String
from java.util import List, Set, UUID
from android.os import Parcel, Parcelable

class BluetoothAdapter(Object):

    ACTION_CONNECTION_STATE_CHANGED = "android.bluetooth.adapter.action.CONNECTION_STATE_CHANGED"
    ACTION_DISCOVERY_FINISHED = "android.bluetooth.adapter.action.DISCOVERY_FINISHED"
    ACTION_DISCOVERY_STARTED = "android.bluetooth.adapter.action.DISCOVERY_STARTED"
    ACTION_LOCAL_NAME_CHANGED = "android.bluetooth.adapter.action.LOCAL_NAME_CHANGED"
    ACTION_REQUEST_DISCOVERABLE = "android.bluetooth.adapter.action.REQUEST_DISCOVERABLE"
    ACTION_REQUEST_ENABLE = "android.bluetooth.adapter.action.REQUEST_ENABLE"
    ACTION_SCAN_MODE_CHANGED = "android.bluetooth.adapter.action.SCAN_MODE_CHANGED"
    ACTION_STATE_CHANGED = "android.bluetooth.adapter.action.STATE_CHANGED"
    ERROR = 0x80000000
    EXTRA_CONNECTION_STATE = "android.bluetooth.adapter.extra.CONNECTION_STATE"
    EXTRA_DISCOVERABLE_DURATION = "android.bluetooth.adapter.extra.DISCOVERABLE_DURATION"
    EXTRA_LOCAL_NAME = "android.bluetooth.adapter.extra.LOCAL_NAME"
    EXTRA_PREVIOUS_CONNECTION_STATE = "android.bluetooth.adapter.extra.PREVIOUS_CONNECTION_STATE"
    EXTRA_PREVIOUS_SCAN_MODE = "android.bluetooth.adapter.extra.PREVIOUS_SCAN_MODE"
    EXTRA_PREVIOUS_STATE = "android.bluetooth.adapter.extra.PREVIOUS_STATE"
    EXTRA_SCAN_MODE = "android.bluetooth.adapter.extra.SCAN_MODE"
    EXTRA_STATE = "android.bluetooth.adapter.extra.STATE"
    SCAN_MODE_CONNECTABLE              = 0x15
    SCAN_MODE_CONNECTABLE_DISCOVERABLE = 0x17
    SCAN_MODE_NONE                     = 0x14
    STATE_CONNECTED     = 0x02
    STATE_CONNECTING    = 0x01
    STATE_DISCONNECTED  = 0x00
    STATE_DISCONNECTING = 0x03
    STATE_OFF           = 0x0a
    STATE_ON            = 0x0c
    STATE_TURNING_OFF   = 0x0d
    STATE_TURNING_ON    = 0x0b

    class LeScanCallback:
    
        @args(void, [BluetoothDevice, int, [byte]])
        def onLeScan(self, device, rssi, scanRecord):
            pass
    
    @api(5)
    @permissions(android.permission.BLUETOOTH_ADMIN)
    @args(bool, [])
    def cancelDiscovery(self):
        pass
    
    @api(5)
    @static
    @args(bool, [String])
    def checkBluetoothAddress(address):
        pass
    
    @api(11)
    @args(void, [int, BluetoothProfile])
    def closeProfileProxy(self, profile, proxy):
        pass
    
    @api(5)
    @permissions(android.permission.BLUETOOTH_ADMIN)
    @args(bool, [])
    def disable(self):
        pass
    
    @api(5)
    @permissions(android.permission.BLUETOOTH_ADMIN)
    @args(bool, [])
    def enable(self):
        pass
    
    @api(5)
    @permissions(android.permission.BLUETOOTH)
    @args(String, [])
    def getAddress(self):
        pass
    
    @api(21)
    @args(BluetoothLeAdvertiser, [])
    def getBluetoothLeAdvertiser(self):
        pass
    
    @api(21)
    @args(BluetoothLeScanner, [])
    def getBluetoothLeScanner(self):
        pass
    
    @api(5)
    @permissions(android.permission.BLUETOOTH)
    @args(Set(BluetoothDevice), [])
    def getBondedDevices(self):
        pass
    
    @api(5)
    @synchronized
    @static
    @args(BluetoothAdapter, [])
    def getDefaultAdapter():
        pass
    
    @api(5)
    @permissions(android.permission.BLUETOOTH)
    @args(String, [])
    def getName(self):
        pass
    
    @api(14)
    @permissions(android.permission.BLUETOOTH)
    @args(int, [int])
    def getProfileConnectionState(self, profile):
        pass
    
    @api(11)
    @args(bool, [Context, BluetoothProfile.ServiceListener, int])
    def getProfileProxy(self, context, listener, profile):
        pass
    
    @api(16)
    @args(BluetoothDevice, [[byte]])
    def getRemoteDevice(self, address):
        pass
    
    @api(5)
    @args(BluetoothDevice, [String])
    def getRemoteDevice(self, address):
        pass
    
    @api(5)
    @permissions(android.permission.BLUETOOTH)
    @args(int, [])
    def getScanMode(self):
        pass
    
    @api(5)
    @permissions(android.permission.BLUETOOTH)
    @args(int, [])
    def getState(self):
        pass
    
    @api(5)
    @permissions(android.permission.BLUETOOTH)
    @args(bool, [])
    def isDiscovering(self):
        pass
    
    @api(5)
    @permissions(android.permission.BLUETOOTH)
    @args(bool, [])
    def isEnabled(self):
        pass
    
    @api(21)
    @args(bool, [])
    def isMultipleAdvertisementSupported(self):
        pass
    
    @api(21)
    @args(bool, [])
    def isOffloadedFilteringSupported(self):
        pass
    
    @api(21)
    @args(bool, [])
    def isOffloadedScanBatchingSupported(self):
        pass
    
    @api(10)
    @permissions(android.permission.BLUETOOTH)
    @args(BluetoothServerSocket, [String, UUID])
    def listenUsingInsecureRfcommWithServiceRecord(self, name, uuid):
        pass
    
    @api(5)
    @permissions(android.permission.BLUETOOTH)
    @args(BluetoothServerSocket, [String, UUID])
    def listenUsingRfcommWithServiceRecord(self, name, uuid):
        pass
    
    @api(5)
    @permissions(android.permission.BLUETOOTH_ADMIN)
    @args(bool, [String])
    def setName(self, name):
        pass
    
    @api(5)
    @permissions(android.permission.BLUETOOTH_ADMIN)
    @args(bool, [])
    def startDiscovery(self):
        pass
    
    @api(18, 21)
    @permissions(android.permission.BLUETOOTH_ADMIN)
    @args(bool, [BluetoothAdapter.LeScanCallback])
    def startLeScan(self, callback):
        pass
    
    @api(18, 21)
    @permissions(android.permission.BLUETOOTH_ADMIN)
    @args(bool, [[UUID], BluetoothAdapter.LeScanCallback])
    def startLeScan(self, serviceUuids, callback):
        pass
    
    @api(18, 21)
    @permissions(android.permission.BLUETOOTH_ADMIN)
    @args(void, [BluetoothAdapter.LeScanCallback])
    def stopLeScan(self, callback):
        pass


class BluetoothDevice(Object):

    ### Requires android.permission.BLUETOOTH_ADMIN
    
    __interfaces__ = [Parcelable]
    
    __static_fields__ = {"CREATOR": Creator(BluetoothDevice)}
    
    ACTION_ACL_CONNECTED = "android.bluetooth.device.action.ACL_CONNECTED"
    ACTION_ACL_DISCONNECTED = "android.bluetooth.device.action.ACL_DISCONNECTED"
    ACTION_ACL_DISCONNECT_REQUESTED = "android.bluetooth.device.action.ACL_DISCONNECT_REQUESTED"
    ACTION_BOND_STATE_CHANGED = "android.bluetooth.device.action.BOND_STATE_CHANGED"
    ACTION_CLASS_CHANGED = "android.bluetooth.device.action.CLASS_CHANGED"
    ACTION_FOUND = "android.bluetooth.device.action.FOUND"
    ACTION_NAME_CHANGED = "android.bluetooth.device.action.NAME_CHANGED"
    ACTION_PAIRING_REQUEST = "android.bluetooth.device.action.PAIRING_REQUEST"
    ACTION_UUID = "android.bluetooth.device.action.UUID"
    BOND_BONDED  = 0x0c
    BOND_BONDING = 0x0b
    BOND_NONE    = 0x0a
    DEVICE_TYPE_CLASSIC = 1
    DEVICE_TYPE_DUAL    = 3
    DEVICE_TYPE_LE      = 2
    DEVICE_TYPE_UNKNOWN = 0
    ERROR = 0x80000000
    EXTRA_BOND_STATE = "android.bluetooth.device.extra.BOND_STATE"
    EXTRA_CLASS = "android.bluetooth.device.extra.CLASS"
    EXTRA_DEVICE = "android.bluetooth.device.extra.DEVICE"
    EXTRA_NAME = "android.bluetooth.device.extra.NAME"
    EXTRA_PAIRING_KEY = "android.bluetooth.device.extra.PAIRING_KEY"
    EXTRA_PAIRING_VARIANT = "android.bluetooth.device.extra.PAIRING_VARIANT"
    EXTRA_PREVIOUS_BOND_STATE = "android.bluetooth.device.extra.PREVIOUS_BOND_STATE"
    EXTRA_RSSI = "android.bluetooth.device.extra.RSSI"
    EXTRA_UUID = "android.bluetooth.device.extra.UUID"
    PAIRING_VARIANT_PASSKEY_CONFIRMATION = 2
    PAIRING_VARIANT_PIN                  = 0
    TRANSPORT_AUTO  = 0
    TRANSPORT_BREDR = 1
    TRANSPORT_LE    = 2
    
    @api(18)
    @args(BluetoothGatt, [Context, bool, BluetoothGattCallback])
    def connectGatt(self, context, autoConnect, callback):
        pass
    
    @api(23)
    @args(BluetoothGatt, [Context, bool, BluetoothGattCallback, int])
    def connectGatt(self, context, autoConnect, callback, transport):
        pass
    
    @api(19)
    @args(bool, [])
    def createBond(self):
        pass
    
    @api(10)
    @args(BluetoothSocket, [UUID])
    def createInsecureRfcommSocketToServiceRecord(self, uuid):
        pass
    
    @api(5)
    @args(BluetoothSocket, [UUID])
    def createRfcommSocketToServiceRecord(self, uuid):
        pass
    
    @api(5)
    @args(int, [])
    def describeContents(self):
        pass
    
    @api(5)
    @args(bool, [Object])
    def equals(self, o):
        pass
    
    @api(15)
    @args(bool, [])
    def fetchUuidsWithSdp(self):
        pass
    
    @api(5)
    @args(String, [])
    def getAddress(self):
        pass
    
    @api(5)
    @args(BluetoothClass, [])
    def getBluetoothClass(self):
        pass
    
    @api(5)
    @args(int, [])
    def getBondState(self):
        pass
    
    @api(5)
    @args(String, [])
    def getName(self):
        pass
    
    @api(18)
    @args(int, [])
    def getType(self):
        pass
    
    @api(15)
    @args([ParcelUuid], [])
    def getUuids(self):
        pass
    
    @api(5)
    @args(int, [])
    def hashCode(self):
        pass
    
    @api(19)
    @args(bool, [bool])
    def setPairingConfirmation(self, confirm):
        pass
    
    @api(19)
    @permissions(android.permission.BLUETOOTH_ADMIN)
    @args(bool, [[byte]])
    def setPin(self, pin):
        pass
    
    @api(5)
    @args(String, [])
    def toString(self):
        pass
    
    @api(5)
    @args(void, [Parcel, int])
    def writeToParcel(self, out, flags):
        pass


class BluetoothGatt(Object):

    __interfaces__ = [BluetoothProfile]
    
    CONNECTION_PRIORITY_BALANCED     = 0
    CONNECTION_PRIORITY_HIGH         = 1
    CONNECTION_PRIORITY_LOW_POWER    = 2
    GATT_CONNECTION_CONGESTED        = 0x08f
    GATT_FAILURE                     = 0x101
    GATT_INSUFFICIENT_AUTHENTICATION = 5
    GATT_INSUFFICIENT_ENCRYPTION     = 15
    GATT_INVALID_ATTRIBUTE_LENGTH    = 13
    GATT_INVALID_OFFSET              = 7
    GATT_READ_NOT_PERMITTED          = 2
    GATT_REQUEST_NOT_SUPPORTED       = 6
    GATT_SUCCESS                     = 0
    GATT_WRITE_NOT_PERMITTED         = 3
    
    @api(19)
    @permissions(android.permission.BLUETOOTH)
    def abortReliableWrite(self):
        pass
    
    @api(8, 19)
    @args(void, [BluetoothDevice])
    def abortReliableWrite(self, mDevice):
        pass
    
    @api(18)
    @permissions(android.permission.BLUETOOTH)
    @args(bool, [])
    def beginReliableWrite(self):
        pass
    
    @api(18)
    def close(self):
        pass
    
    @api(18)
    @args(bool, [])
    def connect(self):
        pass
    
    @api(18)
    @permissions(android.permission.BLUETOOTH)
    def disconnect(self):
        pass
    
    @api(18)
    @permissions(android.permission.BLUETOOTH)
    @args(bool, [])
    def discoverServices(self):
        pass
    
    @api(18)
    @permissions(android.permission.BLUETOOTH)
    @args(bool, [])
    def executeReliableWrite(self):
        pass
    
    @api(18)
    @args(List(BluetoothDevice), [])
    def getConnectedDevices(self):
        pass
    
    @api(18)
    @args(int, [BluetoothDevice])
    def getConnectionState(self, device):
        pass
    
    @api(18)
    @args(BluetoothDevice, [])
    def getDevice(self):
        pass
    
    @api(18)
    @args(List(BluetoothDevice), [[int]])
    def getDevicesMatchingConnectionStates(self, states):
        pass
    
    @api(18)
    @permissions(android.permission.BLUETOOTH)
    @args(BluetoothGattService, [UUID])
    def getService(self, uuid):
        pass
    
    @api(18)
    @permissions(android.permission.BLUETOOTH)
    @args(List(BluetoothGattService), [])
    def getServices(self):
        pass
    
    @api(18)
    @permissions(android.permission.BLUETOOTH)
    @args(bool, [BluetoothGattCharacteristic])
    def readCharacteristic(self, characteristic):
        pass
    
    @api(18)
    @permissions(android.permission.BLUETOOTH)
    @args(bool, [BluetoothGattDescriptor])
    def readDescriptor(self, descriptor):
        pass
    
    @api(18)
    @permissions(android.permission.BLUETOOTH)
    @args(bool, [])
    def readRemoteRssi(self):
        pass
    
    @api(21)
    @args(bool, [int])
    def requestConnectionPriority(self, connectionPriority):
        pass
    
    @api(21)
    @permissions(android.permission.BLUETOOTH)
    @args(bool, [int])
    def requestMtu(self, mtu):
        pass
    
    @api(18)
    @permissions(android.permission.BLUETOOTH)
    @args(bool, [BluetoothGattCharacteristic, bool])
    def setCharacteristicNotification(self, characteristic, enable):
        pass
    
    @api(18)
    @permissions(android.permission.BLUETOOTH)
    @args(bool, [BluetoothGattCharacteristi])
    def writeCharacteristic(self, characteristic):
        pass
    
    @api(18)
    @permissions(android.permission.BLUETOOTH)
    @args(bool, [BluetoothGattDescriptor])
    def writeDescriptor(self, descriptor):
        pass


class BluetoothGattCharacteristic(Object):

    __fields__ = {"mDescriptors": List(BluetoothGattDescriptor)}
    
    FORMAT_FLOAT  = 0x34
    FORMAT_SFLOAT = 0x32
    FORMAT_SINT16 = 0x22
    FORMAT_SINT32 = 0x24
    FORMAT_SINT8  = 0x21
    FORMAT_UINT16 = 0x12
    FORMAT_UINT32 = 0x14
    FORMAT_UINT8  = 0x11
    PERMISSION_READ                 = 0x001
    PERMISSION_READ_ENCRYPTED       = 0x002
    PERMISSION_READ_ENCRYPTED_MITM  = 0x004
    PERMISSION_WRITE                = 0x010
    PERMISSION_WRITE_ENCRYPTED      = 0x020
    PERMISSION_WRITE_ENCRYPTED_MITM = 0x040
    PERMISSION_WRITE_SIGNED         = 0x080
    PERMISSION_WRITE_SIGNED_MITM    = 0x100
    PROPERTY_BROADCAST         = 0x01
    PROPERTY_EXTENDED_PROPS    = 0x80
    PROPERTY_INDICATE          = 0x20
    PROPERTY_NOTIFY            = 0x10
    PROPERTY_READ              = 0x02
    PROPERTY_SIGNED_WRITE      = 0x40
    PROPERTY_WRITE             = 0x08
    PROPERTY_WRITE_NO_RESPONSE = 0x04
    WRITE_TYPE_DEFAULT     = 2
    WRITE_TYPE_NO_RESPONSE = 1
    WRITE_TYPE_SIGNED      = 4
    
    @api(18)
    @permissions(android.permission.BLUETOOTH)
    @args(void, [UUID, int, int])
    def __init__(self, uuid, properties, permissions):
        pass
    
    @api(18)
    @permissions(android.permission.BLUETOOTH)
    @args(bool, [BluetoothGattDescriptor])
    def addDescriptor(self, descriptor):
        pass
    
    @api(18)
    @args(BluetoothGattDescriptor, [UUID])
    def getDescriptor(self, uuid):
        pass
    
    @api(18)
    @args(List(BluetoothGattDescriptor), [])
    def getDescriptors(self):
        pass
    
    @api(18)
    @args(Float, [int, int])
    def getFloatValue(self, formatType, offset):
        pass
    
    @api(18)
    @args(int, [])
    def getInstanceId(self):
        pass
    
    @api(18)
    @args(Integer, [int, int])
    def getIntValue(self, formatType, offset):
        pass
    
    @api(18)
    @args(int, [])
    def getPermissions(self):
        pass
    
    @api(18)
    @args(int, [])
    def getProperties(self):
        pass
    
    @api(18)
    @args(BluetoothGattService, [])
    def getService(self):
        pass
    
    @api(18)
    @args(String, [int])
    def getStringValue(self, offset):
        pass
    
    @api(18)
    @args(UUID, [])
    def getUuid(self):
        pass
    
    @api(18)
    @args([byte], [])
    def getValue(self):
        pass
    
    @api(18)
    @args(int, [])
    def getWriteType(self):
        pass
    
    @api(18)
    @args(bool, [int, int, int])
    def setValue(self, value, formatType, offset):
        pass
    
    @api(18)
    @args(bool, [[byte]])
    def setValue(self, value):
        pass
    
    @api(18)
    @args(bool, [String])
    def setValue(self, value):
        pass
    
    @api(18)
    @args(bool, [int, int, int, int])
    def setValue(self, mantissa, exponent, formatType, offset):
        pass
    
    @api(18)
    @args(void, [int])
    def setWriteType(self, writeType):
        pass


class BluetoothGattDescriptor(Object):

    __static_fields__ = {
        "DISABLE_NOTIFICATION_VALUE": [byte],
        "ENABLE_INDICATION_VALUE": [byte],
        "ENABLE_NOTIFICATION_VALUE": [byte]
        }
    
    PERMISSION_READ                 = 0x001
    PERMISSION_READ_ENCRYPTED       = 0x002
    PERMISSION_READ_ENCRYPTED_MITM  = 0x004
    PERMISSION_WRITE                = 0x010
    PERMISSION_WRITE_ENCRYPTED      = 0x020
    PERMISSION_WRITE_ENCRYPTED_MITM = 0x040
    PERMISSION_WRITE_SIGNED         = 0x080
    PERMISSION_WRITE_SIGNED_MITM    = 0x100
    
    @api(18)
    @permissions(android.permission.BLUETOOTH)
    @args(void, [UUID, int])
    def __init__(self, uuid, permissions):
        pass
    
    @api(18)
    @args(BluetoothGattCharacteristic, [])
    def getCharacteristic(self):
        pass
    
    @api(18)
    @args(int, [])
    def getPermissions(self):
        pass
    
    @api(18)
    @args(UUID, [])
    def getUuid(self):
        pass
    
    @api(18)
    @args([byte], [])
    def getValue(self):
        pass
    
    @api(18)
    @args(bool, [[byte]])
    def setValue(self, value):
        pass


class BluetoothGattService(Object):

    __fields__ = {
        "mCharacteristics": List(BluetoothGattCharacteristic),
        "mIncludedServices": List(BluetoothGattService)
        }
    
    SERVICE_TYPE_PRIMARY = 0
    SERVICE_TYPE_SECONDARY = 1
    
    @api(18)
    @permissions(android.permission.BLUETOOTH)
    @args(void, [UUID, int])
    def __init__(self, uuid, serviceType):
        pass
    
    @api(18)
    @permissions(android.permission.BLUETOOTH)
    @args(bool, [BluetoothGattCharacteristic])
    def addCharacteristic(self, characteristic):
        pass
    
    @api(18)
    @permissions(android.permission.BLUETOOTH)
    @args(bool, [BluetoothGattService])
    def addService(self, service):
        pass
    
    @api(18)
    @args(BluetoothGattCharacteristic, [UUID])
    def getCharacteristic(self, uuid):
        pass
    
    @api(18)
    @args(List(BluetoothGattCharacteristic), [])
    def getCharacteristics(self):
        pass
    
    @api(18)
    @args(List(BluetoothGattService), [])
    def getIncludedServices(self):
        pass
    
    @api(18)
    @args(int, [])
    def getInstanceId(self):
        pass
    
    @api(18)
    @args(int, [])
    def getType(self):
        pass
    
    @api(18)
    @args(UUID, [])
    def getUuid(self):
        pass


class BluetoothManager(Object):

    @api(18)
    @args(BluetoothAdapter, [])
    def getAdapter(self):
        pass
    
    @api(18)
    @args(List(BluetoothDevice), [int])
    def getConnectedDevices(self, profile):
        pass
    
    @api(18)
    @args(int, [BluetoothDevice, int])
    def getConnectionState(self, device, profile):
        pass
    
    @api(18)
    @args(List(BluetoothDevice), [int, [int]])
    def getDevicesMatchingConnectionStates(self, profile, states):
        pass
    
    @api(18)
    @args(BluetoothGattServer, [Context, BluetoothGattServerCallback])
    def openGattServer(self, context, callback):
        pass


class BluetoothProfile:

    class ServiceListener:
    
        @args(void, [int, BluetoothProfile])
        def onServiceConnected(self, profile, proxy):
            pass
        
        @args(void, [int])
        def onServiceDisconnected(self, profile):
            pass
    
    A2DP = 2
    EXTRA_PREVIOUS_STATE = "android.bluetooth.profile.extra.PREVIOUS_STATE"
    EXTRA_STATE = "android.bluetooth.profile.extra.STATE"
    GATT = 7
    GATT_SERVER = 8
    HEADSET = 1
    HEALTH = 3
    SAP = 10
    STATE_CONNECTED = 2
    STATE_CONNECTING = 1
    STATE_DISCONNECTED = 0
    STATE_DISCONNECTING = 3
    
    @api(11)
    @permissions(android.permission.BLUETOOTH)
    @args(List(BluetoothDevice), [])
    def getConnectedDevices(self):
        pass
    
    @api(11)
    @permissions(android.permission.BLUETOOTH)
    @args(int, [BluetoothDevice])
    def getConnectionState(self, device):
        pass
    
    @api(11)
    @permissions(android.permission.BLUETOOTH)
    @args(List(BluetoothDevice), [[int]])
    def getDevicesMatchingConnectionStates(self, states):
        pass


class BluetoothServerSocket(Object):

    __interfaces__ = [Closeable]
    
    # Requires the BLUETOOTH permission.
    
    @api(5)
    @args(BluetoothSocket, [int])
    def accept(self, timeout):
        pass
    
    @api(5)
    @args(BluetoothSocket, [])
    def accept(self):
        pass
    
    @api(5)
    def close(self):
        pass
    
    @api(5)
    @args(String, [])
    def toString(self):
        pass


class BluetoothSocket(Object):

    # Requires the BLUETOOTH permission.
    
    TYPE_L2CAP = 3
    TYPE_RFCOMM = 1
    TYPE_SCO = 2
    
    @api(5)
    def close(self):
        pass
    
    @api(5)
    def connect(self):
        pass
    
    @api(23)
    @args(int, [])
    def getConnectionType(self):
        pass
    
    @api(5)
    @args(InputStream, [])
    def getInputStream(self):
        pass
    
    @api(23)
    @args(int, [])
    def getMaxReceivePacketSize(self):
        pass
    
    @api(23)
    @args(int, [])
    def getMaxTransmitPacketSize(self):
        pass
    
    @api(5)
    @args(OutputStream, [])
    def getOutputStream(self):
        pass
    
    @api(5)
    @args(BluetoothDevice, [])
    def getRemoteDevice(self):
        pass
    
    @api(14)
    @args(bool, [])
    def isConnected(self):
        pass
