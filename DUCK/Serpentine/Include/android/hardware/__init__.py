# Class declarations obtained from Android API documentation licensed under the
# Apache 2.0 license (http://www.apache.org/licenses/LICENSE-2.0) and rewritten
# in syntax that the Python compiler module can parse.

__package__ = "android.hardware"

import java.lang
import java.util
import android.content
import android.view

class Camera(java.lang.Object):

    class ShutterCallback:
    
        @args(void, [])
        def onShutter(self):
            pass
    
    class PictureCallback:
    
        @args(void, [[byte], Camera])
        def onPictureTaken(self, data, camera):
            pass
    
    class Parameters(java.lang.Object):
    
        @args(java.util.List(Camera.Size), [])
        def getSupportedPreviewSizes(self):
            pass
        
        @args(void, [int, int])
        def setPreviewSize(self, width, height):
            pass
        
        @args(void, [int, int])
        def setPictureSize(self, width, height):
            pass
    
    class Size(java.lang.Object):
    
        __fields__ = {"width": int, "height": int}
    
    @static
    @args(int, [])
    def getNumberOfCameras():
        pass
    
    @args(Camera.Parameters, [])
    def getParameters(self):
        pass
    
    def lock(self):
        pass
    
    @static
    @args(Camera, [])
    def open():
        pass
    
    def release(self):
        pass
    
    @args(void, [Camera.Parameters])
    def setParameters(self, parameters):
        pass
    
    @args(void, [android.view.SurfaceHolder])
    def setPreviewDisplay(self, holder):
        pass
    
    def startPreview(self):
        pass
    
    def stopPreview(self):
        pass
    
    @args(void, [Camera.ShutterCallback, Camera.PictureCallback,
                 Camera.PictureCallback])
    def takePicture(self, shutter, raw, jpeg):
        pass
    
    @args(void, [Camera.ShutterCallback, Camera.PictureCallback,
                 Camera.PictureCallback, Camera.PictureCallback])
    def takePicture(self, shutter, raw, postview, jpeg):
        pass
    
    @args(java.lang.String, [])
    def toString(self):
        pass
    
    def unlock(self):
        pass

class GeomagneticField(java.lang.Object):

    @args(void, [float, float, float, long])
    def __init__(self, gdLatitudeDegrees, gdLongitudeDegrees, altitudeMeters, timeMilliseconds):
        pass
    
    @args(float, [])
    def getDeclination(self):
        pass
    
    @args(float, [])
    def getFieldStrength(self):
        pass
    
    @args(float, [])
    def getHorizontalStrength(self):
        pass
    
    @args(float, [])
    def getInclination(self):
        pass
    
    @args(float, [])
    def getX(self):
        pass
    
    @args(float, [])
    def getY(self):
        pass
    
    @args(float, [])
    def getZ(self):
        pass


class Sensor(java.lang.Object):

    REPORTING_MODE_CONTINUOUS = 0
    REPORTING_MODE_ONE_SHOT = 2
    REPORTING_MODE_ON_CHANGE = 1
    REPORTING_MODE_SPECIAL_TRIGGER = 3
    STRING_TYPE_ACCELEROMETER = "android.sensor.accelerometer"
    STRING_TYPE_AMBIENT_TEMPERATURE = "android.sensor.ambient_temperature"
    STRING_TYPE_GAME_ROTATION_VECTOR = "android.sensor.game_rotation_vector"
    STRING_TYPE_GEOMAGNETIC_ROTATION_VECTOR = "android.sensor.geomagnetic_rotation_vector"
    STRING_TYPE_GRAVITY = "android.sensor.gravity"
    STRING_TYPE_GYROSCOPE = "android.sensor.gyroscope"
    STRING_TYPE_GYROSCOPE_UNCALIBRATED = "android.sensor.gyroscope_uncalibrated"
    STRING_TYPE_HEART_RATE = "android.sensor.heart_rate"
    STRING_TYPE_LIGHT = "android.sensor.light"
    STRING_TYPE_LINEAR_ACCELERATION = "android.sensor.linear_acceleration"
    STRING_TYPE_MAGNETIC_FIELD = "android.sensor.magnetic_field"
    STRING_TYPE_MAGNETIC_FIELD_UNCALIBRATED = "android.sensor.magnetic_field_uncalibrated"
    STRING_TYPE_ORIENTATION = "android.sensor.orientation" # deprecated in API level 8
    STRING_TYPE_PRESSURE = "android.sensor.pressure"
    STRING_TYPE_PROXIMITY = "android.sensor.proximity"
    STRING_TYPE_RELATIVE_HUMIDITY = "android.sensor.relative_humidity"
    STRING_TYPE_ROTATION_VECTOR = "android.sensor.rotation_vector"
    STRING_TYPE_SIGNIFICANT_MOTION = "android.sensor.significant_motion"
    STRING_TYPE_STEP_COUNTER = "android.sensor.step_counter"
    STRING_TYPE_STEP_DETECTOR = "android.sensor.step_detector"
    TYPE_ACCELEROMETER = 1
    TYPE_ALL = -1
    TYPE_AMBIENT_TEMPERATURE = 13
    TYPE_GAME_ROTATION_VECTOR = 15
    TYPE_GEOMAGNETIC_ROTATION_VECTOR = 20
    TYPE_GRAVITY = 9
    TYPE_GYROSCOPE = 4
    TYPE_GYROSCOPE_UNCALIBRATED = 16
    TYPE_HEART_RATE = 21
    TYPE_LIGHT = 5
    TYPE_LINEAR_ACCELERATION = 10
    TYPE_MAGNETIC_FIELD = 2
    TYPE_MAGNETIC_FIELD_UNCALIBRATED = 14
    TYPE_ORIENTATION = 3 # deprecated in API level 8
    TYPE_PRESSURE = 6
    TYPE_PROXIMITY = 8
    TYPE_RELATIVE_HUMIDITY = 12
    TYPE_ROTATION_VECTOR = 11
    TYPE_SIGNIFICANT_MOTION = 17
    TYPE_STEP_COUNTER = 19
    TYPE_STEP_DETECTOR = 18
    
    @args(int, [])
    def getFifoMaxEventCount(self):
        pass
    
    @args(int, [])
    def getFifoReservedEventCount(self):
        pass
    
    @args(int, [])
    def getMaxDelay(self):
        pass
    
    @args(float, [])
    def getMaximumRange(self):
        pass
    
    @args(int, [])
    def getMinDelay(self):
        pass
    
    @args(java.lang.String, [])
    def getName(self):
        pass
    
    @args(float, [])
    def getPower(self):
        pass
    
    @args(int, [])
    def getReportingMode(self):
        pass
    
    @args(float, [])
    def getResolution(self):
        pass
    
    @args(java.lang.String, [])
    def getStringType(self):
        pass
    
    @args(int, [])
    def getType(self):
        pass
    
    @args(java.lang.String, [])
    def getVendor(self):
        pass
    
    @args(int, [])
    def getVersion(self):
        pass
    
    @args(bool, [])
    def isWakeUpSensor(self):
        pass


class SensorEvent(java.lang.Object):

    __fields__ = {
        "accuracy": int,
        "sensor": Sensor,
        "timestamp": long,
        "values": [float]
        }


class SensorEventListener:

    @abstract
    @args(void, [Sensor, int])
    def onAccuracyChanged(self, sensor, accuracy):
        pass
    
    @abstract
    @args(void, [SensorEvent])
    def onSensorChanged(self, event):
        pass


class SensorEventListener2:

    __interfaces__ = [SensorEventListener]
    
    @abstract
    @args(void, [Sensor])
    def onFlushCompleted(self, sensor):
        pass


class SensorManager(java.lang.Object):

    AXIS_MINUS_X = 129
    AXIS_MINUS_Y = 130
    AXIS_MINUS_Z = 131
    AXIS_X = 1
    AXIS_Y = 2
    AXIS_Z = 3
    GRAVITY_EARTH = 9.80665
    LIGHT_CLOUDY = 100.0
    LIGHT_FULLMOON = 0.25
    LIGHT_NO_MOON = 0.001
    LIGHT_OVERCAST = 10000.0
    LIGHT_SHADE = 20000.0
    LIGHT_SUNLIGHT = 110000.0
    LIGHT_SUNLIGHT_MAX = 120000.0
    LIGHT_SUNRISE = 400.0
    MAGNETIC_FIELD_EARTH_MAX = 60.0
    MAGNETIC_FIELD_EARTH_MIN = 30.0
    PRESSURE_STANDARD_ATMOSPHERE = 1013.25
    SENSOR_DELAY_FASTEST = 0
    SENSOR_DELAY_GAME = 1
    SENSOR_DELAY_UI = 2
    SENSOR_DELAY_NORMAL = 3
    SENSOR_STATUS_ACCURACY_HIGH = 3
    SENSOR_STATUS_ACCURACY_LOW = 1
    SENSOR_STATUS_ACCURACY_MEDIUM = 2
    SENSOR_STATUS_NO_CONTACT = -1
    SENSOR_STATUS_UNRELIABLE = 0
    STANDARD_GRAVITY = 9.80665
    
    #@args(bool, [TriggerEventListener, Sensor])
    #def cancelTriggerSensor(self, listener, sensor):
    #    pass
    
    @args(bool, [SensorEventListener])
    def flush(self, listener):
        pass
    
    @static
    @args(float, [float, float])
    def getAltitude(p0, p):
        pass
    
    @static
    @args(void, [[float], [float], [float]])
    def getAngleChange(angleChange, R, prevR):
        pass
    
    @args(Sensor, [int])
    def getDefaultSensor(self, type_):
        pass
    
    @args(Sensor, [int, bool])
    def getDefaultSensor(self, type_, wakeUp):
        pass
    
    @static
    @args(float, [[float]])
    def getInclination(I):
        pass
    
    @static
    @args([float], [[float], [float]])
    def getOrientation(R, values):
        pass
    
    @static
    @args(void, [[float], [float]])
    def getQuaternionFromVector(Q, rv):
        pass
    
    @static
    @args(bool, [[float], [float], [float], [float]])
    def getRotationMatrix(R, I, gravity, geomagnetic):
        pass
    
    @static
    @args(void, [[float], [float]])
    def getRotationMatrixFromVector(R, rotationVector):
        pass
    
    @args(java.util.List(Sensor), [int])
    def getSensorList(self, type_):
        pass
    
    @args(bool, [SensorEventListener, Sensor, int, int])
    def registerListener(self, listener, sensor, samplingPeriodUs, maxReportLatencyUs):
        pass
    
    @args(bool, [SensorEventListener, Sensor, int, int, Handler])
    def registerListener(self, listener, sensor, samplingPeriodUs, maxReportLatencyUs, handler):
        pass
    
    @args(bool, [SensorEventListener, Sensor, int, Handler])
    def registerListener(self, listener, sensor, samplingPeriodUs, handler):
        pass
    
    @args(bool, [SensorEventListener, Sensor, int])
    def registerListener(self, listener, sensor, samplingPeriodUs):
        pass
    
    @static
    @args(bool, [[float], int, int, [float]])
    def remapCoordinateSystem(inR, X, Y, outR):
        pass
    
    @args(bool, [TriggerEventListener, Sensor])
    def requestTriggerSensor(self, listener, sensor):
        pass
    
    @args(void, [SensorEventListener, Sensor])
    def unregisterListener(self, listener, sensor):
        pass
    
    @args(void, [SensorEventListener])
    def unregisterListener(self, listener):
        pass


class TriggerEvent(java.lang.Object):

    __fields__ = {"sensor": Sensor, "timestamp": long,
                  "values": [float]}


class TriggerEventListener(java.lang.Object):

    @abstract
    @args(void, [TriggerEvent])
    def onTrigger(self, event):
        pass
