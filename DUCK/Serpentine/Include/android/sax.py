# Class declarations obtained from Android API documentation licensed under the
# Apache 2.0 license (http://www.apache.org/licenses/LICENSE-2.0) and rewritten
# in syntax that the Python compiler module can parse.

__package__ = "android.sax"

from java.lang import Object, String
from org.xml.sax import ContentHandler

class Element(Object):

    @args(Element, [String, String])
    def getChild(self, uri, localName):
        pass
    
    @args(Element, [String])
    def getChild(self, localName):
        pass
    
    @args(Element, [String])
    def requireChild(self, localName):
        pass
    
    @args(Element, [String, String])
    def requireChild(self, uri, localName):
        pass
    
    @args(void, [ElementListener])
    def setElementListener(self, elementListener):
        pass
    
    @args(void, [EndElementListener])
    def setEndElementListener(self, endElementListener):
        pass
    
    @args(void, [EndTextElementListener])
    def setEndTextElementListener(self, endTextElementListener):
        pass
    
    @args(void, [StartElementListener])
    def setStartElementListener(self, startElementListener):
        pass
    
    @args(void, [TextElementListener])
    def setTextElementListener(self, elementListener):
        pass
    
    @args(String, [])
    def toString(self):
        pass


class ElementListener:

    __interfaces__ = [StartElementListener, EndElementListener]


class EndElementListener:

    def end(self):
        pass


class EndTextElementListener:

    @args(void, [String])
    def end(self, body):
        pass


class RootElement(Element):

    @args(void, [String, String])
    def __init__(self, uri, localName):
        pass
    
    @args(void, [String])
    def __init__(self, localName):
        pass
    
    @args(ContentHandler, [])
    def getContentHandler(self):
        pass


class StartElementListener:

    @args(void, [Attributes])
    def start(self, attributes):
        pass


class TextElementListener:

    __interfaces__ = [StartElementListener, EndTextElementListener]
