# Class declarations obtained from Android API documentation licensed under the
# Apache 2.0 license (http://www.apache.org/licenses/LICENSE-2.0) and rewritten
# in syntax that the Python compiler module can parse.

__package__ = "android.appwidget"

from java.lang import Object, String
from android.content import BroadcastReceiver, ComponentName, Context, Intent
import android.graphics
from android.os import Bundle, Parcel
from android.widget import RemoteViews

class AppWidgetManager(Object):

    ACTION_APPWIDGET_BIND = "android.appwidget.action.APPWIDGET_BIND"
    ACTION_APPWIDGET_CONFIGURE = "android.appwidget.action.APPWIDGET_CONFIGURE"
    ACTION_APPWIDGET_DELETED = "android.appwidget.action.APPWIDGET_DELETED"
    ACTION_APPWIDGET_DISABLED = "android.appwidget.action.APPWIDGET_DISABLED"
    ACTION_APPWIDGET_ENABLED = "android.appwidget.action.APPWIDGET_ENABLED"
    ACTION_APPWIDGET_HOST_RESTORED = "android.appwidget.action.APPWIDGET_HOST_RESTORED"
    ACTION_APPWIDGET_OPTIONS_CHANGED = "android.appwidget.action.APPWIDGET_UPDATE_OPTIONS"
    ACTION_APPWIDGET_PICK = "android.appwidget.action.APPWIDGET_PICK"
    ACTION_APPWIDGET_RESTORED = "android.appwidget.action.APPWIDGET_RESTORED"
    ACTION_APPWIDGET_UPDATE = "android.appwidget.action.APPWIDGET_UPDATE"
    EXTRA_APPWIDGET_ID = "appWidgetId"
    EXTRA_APPWIDGET_IDS = "appWidgetIds"
    EXTRA_APPWIDGET_OLD_IDS = "appWidgetOldIds"
    EXTRA_APPWIDGET_OPTIONS = "appWidgetOptions"
    EXTRA_APPWIDGET_PROVIDER = "appWidgetProvider"
    EXTRA_APPWIDGET_PROVIDER_PROFILE = "appWidgetProviderProfile"
    EXTRA_CUSTOM_EXTRAS = "customExtras"
    EXTRA_CUSTOM_INFO = "customInfo"
    EXTRA_HOST_ID = "hostId"
    INVALID_APPWIDGET_ID = 0
    META_DATA_APPWIDGET_PROVIDER = "android.appwidget.provider"
    OPTION_APPWIDGET_HOST_CATEGORY = "appWidgetCategory"
    OPTION_APPWIDGET_MAX_HEIGHT = "appWidgetMaxHeight"
    OPTION_APPWIDGET_MAX_WIDTH = "appWidgetMaxWidth"
    OPTION_APPWIDGET_MIN_HEIGHT = "appWidgetMinHeight"
    OPTION_APPWIDGET_MIN_WIDTH = "appWidgetMinWidth"
    
    @api(21)
    @args(bool, [int, UserHandle, ComponentName, Bundle])
    def bindAppWidgetIdIfAllowed(self, appWidgetId, user, provider, options):
        pass

    @api(16)
    @args(bool, [int, ComponentName])
    def bindAppWidgetIdIfAllowed(self, appWidgetId, provider):
        pass

    @api(17)
    @args(bool, [int, ComponentName, Bundle])
    def bindAppWidgetIdIfAllowed(self, appWidgetId, provider, options):
        pass
    
    @api(3)
    @args([int], [ComponentName])
    def getAppWidgetIds(self, provider):
        pass
    
    @api(3)
    @args(AppWidgetProviderInfo, [int])
    def getAppWidgetInfo(self, appWidgetId):
        pass

    @api(16)
    @args(Bundle, [int])
    def getAppWidgetOptions(self, appWidgetId):
        pass

    @api(3)
    @args(List(AppWidgetProviderInfo), [])
    def getInstalledProviders(self):
        pass

    @api(21)
    @args(List(AppWidgetProviderInfo), [UserHandle])
    def getInstalledProvidersForProfile(self, profile):
        pass
    
    @api(3)
    @static
    @args(AppWidgetManager, [Context])
    def getInstance(context):
        pass
    
    @api(11)
    @args(vpid, [[int], int])
    def notifyAppWidgetViewDataChanged(self, appWidgetIds, viewId):
        pass
    
    @api(11)
    @args(void, [int, int])
    def notifyAppWidgetViewDataChanged(self, appWidgetId, viewId):
        pass
    
    @api(11)
    @args(void, [int, RemoteViews])
    def partiallyUpdateAppWidget(self, appWidgetId, views):
        pass

    @api(11)
    @args(void, [[int], RemoteViews])
    def partiallyUpdateAppWidget(self, appWidgetIds, views):
        pass

    @api(3)
    @args(void, [[int], RemoteViews])
    def updateAppWidget(self, appWidgetIds, views):
        pass
    
    @api(3)
    @args(void, [ComponentName, RemoteViews])
    def updateAppWidget(self, provider, views):
        pass
    
    @api(3)
    @args(void, [int, RemoteViews])
    def updateAppWidget(self, appWidgetId, views):
        pass
    
    @api(16)
    @args(void, [int, Bundle])
    def updateAppWidgetOptions(self, appWidgetId, options):
        pass


class AppWidgetProvider(BroadcastReceiver):

    def __init__(self):
        pass
    
    @args(void, [Context, AppWidgetManager, int, Bundle])
    def onAppWidgetOptionsChanged(self, context, manager, id, newOptions):
        pass
    
    @args(void, [Context, [int]])
    def onDeleted(self, context, ids):
        pass
    
    @args(void, [Context])
    def onDisabled(self, context):
        pass
    
    @args(void, [Context])
    def onEnabled(self, context):
        pass
    
    @args(void, [Context, Intent])
    def onReceive(self, context, intent):
        pass
    
    @args(void, [Context, AppWidgetManager, [int]])
    def onUpdate(self, context, manager, ids):
        pass


class AppWidgetProviderInfo(Object):

    def __init__(self):
        pass
    
    @args(void, [Parcel])
    def __init__(self, in_):
        pass
    
    @args(AppWidgetProviderInfo, [])
    def clone(self):
        pass
    
    @args(int, [])
    def describeContents(self):
        pass
    
    @args(String, [])
    def toString(self):
        pass
    
    @args(void, [Parcel, int])
    def writeToParcel(self, out, flags):
        pass
