# Class declarations obtained from Android API documentation licensed under the
# Apache 2.0 license (http://www.apache.org/licenses/LICENSE-2.0) and rewritten
# in syntax that the Python compiler module can parse.

__package__ = "android.net"

from java.io import Closeable, File, FileDescriptor, InputStream, OutputStream
from java.lang import Comparable, Enum, Object, String
from java.util import List, Set
from android.os import Parcel, Parcelable

class ConnectivityManager(Object):

    ACTION_BACKGROUND_DATA_SETTING_CHANGED  = "android.net.conn.BACKGROUND_DATA_SETTING_CHANGED"
    ACTION_CAPTIVE_PORTAL_SIGN_IN           = "android.net.conn.CAPTIVE_PORTAL"
    CONNECTIVITY_ACTION                     = "android.net.conn.CONNECTIVITY_CHANGE"
    DEFAULT_NETWORK_PREFERENCE              = 1
    EXTRA_CAPTIVE_PORTAL                    = "android.net.extra.CAPTIVE_PORTAL"
    EXTRA_EXTRA_INFO                        = "extraInfo"
    EXTRA_IS_FAILOVER                       = "isFailover"
    EXTRA_NETWORK                           = "android.net.extra.NETWORK"
    EXTRA_NETWORK_INFO                      = "networkInfo"
    EXTRA_NETWORK_REQUEST                   = "android.net.extra.NETWORK_REQUEST"
    EXTRA_NETWORK_TYPE                      = "networkType"
    EXTRA_NO_CONNECTIVITY                   = "noConnectivity"
    EXTRA_OTHER_NETWORK_INFO                = "otherNetwork"
    EXTRA_REASON                            = "reason"
    
    TYPE_BLUETOOTH      = 7
    TYPE_DUMMY          = 8
    TYPE_ETHERNET       = 9
    TYPE_MOBILE         = 0
    TYPE_MOBILE_DUN     = 4
    TYPE_MOBILE_HIPRI   = 5
    TYPE_MOBILE_MMS     = 2
    TYPE_MOBILE_SUPL    = 3
    TYPE_VPN            = 17
    TYPE_WIFI           = 1
    TYPE_WIMAX          = 6
    
    class NetworkCallback:
    
        def __init__(self):
            pass
        
        @args(void, [Network])
        def onAvailable(self, network):
            pass
        
        @args(void, [Network, NetworkCapabilities])
        def onCapabilitiesChanged(self, network, capabilities):
            pass
        
        @args(void, [Network, LinkProperties])
        def onLinkPropertiesChanged(self, network, properties):
            pass
        
        @args(void, [Network, int])
        def onLosing(self, network, maxMillisecondsToLive):
            pass
        
        @args(void, [Network])
        def onLost(self, network):
            pass
    
    class OnNetworkActiveListener:
    
        ### API level 21
        def onNetworkActive(self):
            pass
    
    @args(void, [ConnectivityManager.OnNetworkActiveListener])
    def addDefaultNetworkActiveListener(self, listener):
        pass
    
    @args(bool, [Network])
    def bindProcessToNetwork(self, network):
        pass
    
    @args(Network, [])
    def getActiveNetwork(self):
        pass
    
    @args(NetworkInfo, [])
    def getActiveNetworkInfo(self):
        pass
    
    @args([NetworkInfo], [])
    def getAllNetworkInfo(self):
        pass
    
    @args([Network], [])
    def getAllNetworks(self):
        pass
    
    @args(bool, [])
    def getBackgroundDataSetting(self):
        pass
    
    @args(Network, [])
    def getBoundNetworkForProcess(self):
        pass
    
    @args(ProxyInfo, [])
    def getDefaultProxy(self):
        pass
    
    @args(LinkProperties, [Network])
    def getLinkProperties(self, network):
        pass
    
    @args(NetworkCapabilities, [Network])
    def getNetworkCapabilities(self, network):
        pass
    
    @args(NetworkInfo, [int])
    def getNetworkInfo(self, networkType):
        pass
    
    @args(NetworkInfo, [Network])
    def getNetworkInfo(self, network):
        pass
    
    @args(int, [])
    def getNetworkPreference(self):
        pass
    
    @static
    @args(Network, [])
    def getProcessDefaultNetwork():
        pass
    
    @args(bool, [])
    def isActiveNetworkMetered(self):
        pass
    
    ### API level 21
    @args(bool, [])
    def isDefaultNetworkActive(self):
        pass
    
    @static
    @args(bool, [int])
    def isNetworkTypeValid(networkType):
        pass
    
    ### API level 23
    @args(void, [NetworkRequest, PendingIntent])
    def registerNetworkCallback(self, request, operation):
        pass
    
    @args(void, [NetworkRequest, ConnectivityManager.NetworkCallback])
    def registerNetworkCallback(self, request, networkCallback):
        pass
    
    @args(void, [PendingIntent])
    def releaseNetworkRequest(self, operation):
        pass
    
    @args(void, [ConnectivityManager.OnNetworkActiveListener])
    def removeDefaultNetworkActiveListener(self, listener):
        pass
    
    @args(void, [Network])
    def reportBadNetwork(self, network):
        pass
    
    @args(void, [Network, bool])
    def reportNetworkConnectivity(self, network, hasConnectivity):
        pass
    
    @args(bool, [Network])
    def requestBandwidthUpdate(self, network):
        pass
    
    @args(void, [NetworkRequest, ConnectivityManager.NetworkCallback])
    def requestNetwork(self, request, networkCallback):
        pass
    
    @args(void, [NetworkRequest, PendingIntent])
    def requestNetwork(self, request, operation):
        pass
    
    @args(bool, [int, int])
    def requestRouteToHost(self, networkType, hostAddress):
        pass
    
    @args(void, [int])
    def setNetworkPreference(self, preference):
        pass
    
    @static
    @args(bool, [Network])
    def setProcessDefaultNetwork(network):
        pass
    
    @args(int, [int, String])
    def startUsingNetworkFeature(self, networkType, feature):
        pass
    
    @args(int, [int, String])
    def stopUsingNetworkFeature(self, networkType, feature):
        pass
    
    @args(void, [PendingIntent])
    def unregisterNetworkCallback(self, operation):
        pass
    
    @args(void, [ConnectivityManager.NetworkCallback])
    def unregisterNetworkCallback(self, networkCallback):
        pass


class Credentials(Object):

    @args(void, [int, int, int])
    def __init__(self, pid, uid, gid):
        pass
    
    @args(int, [])
    def getGid(self):
        pass
    
    @args(int, [])
    def getPid(self):
        pass
    
    @args(int, [])
    def getUid(self):
        pass


class LinkProperties(Object):

    __interfaces__ = [Parcelable]
    
    __fields__ = {"CREATOR": Creator(LinkProperties)}
    
    @args(List(InetAddress), [])
    def getDnsServers(self):
        pass
    
    @args(String, [])
    def getDomains(self):
        pass
    
    @args(ProxyInfo, [])
    def getHttpProxy(self):
        pass
    
    @args(String, [])
    def getInterfaceName(self):
        pass
    
    @args(List(LinkAddress), [])
    def getLinkAddresses(self):
        pass
    
    @args(List(RouteInfo), [])
    def getRoutes(self):
        pass


class LocalServerSocket(Object):

    @args(void, [String])
    def __init__(self, name):
        pass
    
    @args(void, [FileDescriptor])
    def __init__(self, fd):
        pass
    
    @args(LocalSocket, [])
    def accept(self):
        pass
    
    @args(void, [])
    def close(self):
        pass
    
    @args(FileDescriptor, [])
    def getFileDescriptor(self):
        pass
    
    @args(LocalSocketAddress, [])
    def getLocalSocketAddress(self):
        pass


class LocalSocket(Object):

    __interfaces__ = [Closeable]
    
    # API level 19
    SOCKET_DGRAM = 1
    SOCKET_SEQPACKET = 3
    SOCKET_STREAM = 2
    
    def __init__(self):
        pass
    
    @api(19)
    @args(void, [int])
    def __init__(self, sockType):
        pass
    
    @args(void, [LocalSocketAddress])
    def bind(self, bindpoint):
        pass
    
    def close(self):
        pass
    
    @args(void, [LocalSocketAddress, int])
    def connect(self, endpoint, timeout):
        pass
    
    @args(void, [LocalSocketAddress])
    def connect(self, endpoint):
        pass
    
    @args([FileDescriptor], [])
    def getAncillaryFileDescriptors(self):
        pass
    
    @args(FileDescriptor, [])
    def getFileDescriptor(self):
        pass
    
    @args(InputStream, [])
    def getInputStream(self):
        pass
    
    @args(LocalSocketAddress, [])
    def getLocalSocketAddress(self):
        pass
    
    @args(OutputStream, [])
    def getOutputStream(self):
        pass
    
    @args(Credentials, [])
    def getPeerCredentials(self):
        pass
    
    @args(int, [])
    def getReceiveBufferSize(self):
        pass
    
    @args(LocalSocketAddress, [])
    def getRemoteSocketAddress(self):
        pass
    
    @args(int, [])
    def getSendBufferSize(self):
        pass
    
    @args(int, [])
    def getSoTimeout(self):
        pass
    
    @synchronized
    @args(bool, [])
    def isBound(self):
        pass
    
    @args(bool, [])
    def isClosed(self):
        pass
    
    @synchronized
    @args(bool, [])
    def isConnected(self):
        pass
    
    @args(bool, [])
    def isInputShutdown(self):
        pass
    
    @args(bool, [])
    def isOutputShutdown(self):
        pass
    
    @args(void, [[FileDescriptor]])
    def setFileDescriptorsForSend(self, fds):
        pass
    
    @args(void, [int])
    def setReceiveBufferSize(self, size):
        pass
    
    @args(void, [int])
    def setSendBufferSize(self, n):
        pass
    
    @args(void, [int])
    def setSoTimeout(self, n):
        pass
    
    def shutdownInput(self):
        pass
    
    def shutdownOutput(self):
        pass
    
    @args(String, [])
    def toString(self):
        pass


class LocalSocketAddress(Object):

    class Namespace(Enum):
    
        __static_fields__ = {
            "ABSTRACT": LocalSocketAddress.Namespace,
            "FILESYSTEM": LocalSocketAddress.Namespace,
            "RESERVED": LocalSocketAddress.Namespace
            }
        
        @static
        @args(LocalSocketAddress.Namespace, [String])
        def valueOf(name):
            pass
        
        @final
        @static
        @args([LocalSocketAddress.Namespace], [])
        def values():
            pass
    
    @args(void, [String, LocalSocketAddress.Namespace])
    def __init__(self, name, namespace):
        pass
    
    @args(void, [String])
    def __init__(self, name):
        pass
    
    @args(String, [])
    def getName(self):
        pass
    
    @args(LocalSocketAddress.Namespace, [])
    def getNamespace(self):
        pass


class Network(Object):

    __interfaces__ = [Parcelable]
    
    __fields__ = {"CREATOR": Creator(Network)}
    
    @args(void, [Socket])
    def bindSocket(self, socket):
        pass
    
    #@args(void, [DatagramSocket])
    #def bindSocket(self, socket):
    #    pass
    
    @args(void, [FileDescriptor])
    def bindSocket(self, descriptor):
        pass
    
    @args([InetAddress], [String])
    def getAllByName(self, host):
        pass
    
    @args(InetAddress, [String])
    def getByName(self, host):
        pass
    
    @args(long, [])
    def getNetworkHandle(self):
        pass
    
    #@args(SocketFactory, [])
    #def getSocketFactory(self):
    #    pass
    
    @args(URLConnection, [URL])
    def openConnection(self, url):
        pass
    
    @args(URLConnection, [URL, Proxy])
    def openConnection(self, url, proxy):
        pass


class NetworkCapabilities(Object):

    __interfaces__ = [Parcelable]
    
    __static_fields__ = {
        "CREATOR": Parcelable.Creator(NetworkCapabilities)
        }
    
    NET_CAPABILITY_CAPTIVE_PORTAL   = 17
    NET_CAPABILITY_CBS              = 5
    NET_CAPABILITY_DUN              = 2
    NET_CAPABILITY_EIMS             = 10
    NET_CAPABILITY_FOTA             = 3
    NET_CAPABILITY_IA               = 7
    NET_CAPABILITY_IMS              = 4
    NET_CAPABILITY_INTERNET         = 12
    NET_CAPABILITY_MMS              = 0
    NET_CAPABILITY_NOT_METERED      = 11
    NET_CAPABILITY_NOT_RESTRICTED   = 13
    NET_CAPABILITY_NOT_VPN          = 15
    NET_CAPABILITY_RCS              = 8
    NET_CAPABILITY_SUPL             = 1
    NET_CAPABILITY_TRUSTED          = 14
    NET_CAPABILITY_VALIDATED        = 16
    NET_CAPABILITY_WIFI_P2P         = 6
    NET_CAPABILITY_XCAP             = 9
    
    TRANSPORT_BLUETOOTH = 2
    TRANSPORT_CELLULAR  = 0
    TRANSPORT_ETHERNET  = 3
    TRANSPORT_VPN       = 4
    TRANSPORT_WIFI      = 1
    
    @args(void, [NetworkCapabilities])
    def __init__(self, capabilities):
        pass
    
    @args(int, [])
    def getLinkDownstreamBandwidthKbps(self):
        pass
    
    @args(int, [])
    def getLinkUpstreamBandwidthKbps(self):
        pass
    
    @args(bool, [int])
    def hasCapability(self, capability):
        pass
    
    @args(bool, [int])
    def hasTransport(self, transportType):
        pass


class NetworkInfo(Object):

    __interfaces__ = [Parcelable]
    
    class DetailedState(Enum):
    
        __static_fields__ = {
            "AUTHENTICATING": NetworkInfo.DetailedState,
            "BLOCKED": NetworkInfo.DetailedState,
            "CAPTIVE_PORTAL_CHECK": NetworkInfo.DetailedState,
            "CONNECTED": NetworkInfo.DetailedState,
            "CONNECTING": NetworkInfo.DetailedState,
            "DISCONNECTED": NetworkInfo.DetailedState,
            "DISCONNECTING": NetworkInfo.DetailedState,
            "FAILED": NetworkInfo.DetailedState,
            "IDLE": NetworkInfo.DetailedState,
            "OBTAINING_IPADDR": NetworkInfo.DetailedState,
            "SCANNING": NetworkInfo.DetailedState,
            "SUSPENDED": NetworkInfo.DetailedState,
            "VERIFYING_POOR_LINK": NetworkInfo.DetailedState,
            }
        
        @static
        @args(NetworkInfo.DetailedState, [String])
        def valueOf(name): pass
        
        @final
        @static
        @args([NetworkInfo.DetailedState], [])
        def values(): pass
    
    class State(Enum):
    
        __static_fields__ = {
            "CONNECTED": NetworkInfo.DetailedState,
            "CONNECTING": NetworkInfo.DetailedState,
            "DISCONNECTED": NetworkInfo.DetailedState,
            "DISCONNECTING": NetworkInfo.DetailedState,
            "SUSPENDED": NetworkInfo.DetailedState,
            "UNKNOWN": NetworkInfo.DetailedState,
            }
        
        @static
        @args(NetworkInfo.State, [String])
        def valueOf(name): pass
        
        @final
        @static
        @args([NetworkInfo.State], [])
        def values(): pass
    
    @args(NetworkInfo.DetailedState, [])
    def getDetailedState(self):
        pass
    
    @args(String, [])
    def getExtraInfo(self):
        pass
    
    @args(String, [])
    def getReason(self):
        pass
    
    @args(NetworkInfo.State, [])
    def getState(self):
        pass
    
    @args(int, [])
    def getSubtype(self):
        pass
    
    @args(String, [])
    def getSubtypeName(self):
        pass
    
    @args(int, [])
    def getType(self):
        pass
    
    @args(String, [])
    def getTypeName(self):
        pass
    
    @args(bool, [])
    def isAvailable(self):
        pass
    
    @args(bool, [])
    def isConnected(self):
        pass
    
    @args(bool, [])
    def isConnectedOrConnecting(self):
        pass
    
    @args(bool, [])
    def isFailover(self):
        pass
    
    @args(bool, [])
    def isRoaming(self):
        pass


class Uri(Object):

    __interfaces__ = [Parcelable, Comparable] # Comparable(T)
    
    __fields__ = {
        "CREATOR": Parcelable.Creator(Uri),
        "EMPTY": Uri
        }
    
    class Builder(Object):
    
        def __init__(self):
            pass
        
        @args(Uri.Builder, [String])
        def appendEncodedPath(self, newSegment):
            pass
        
        @args(Uri.Builder, [String])
        def appendPath(self, newSegment):
            pass
        
        @args(Uri.Builder, [String, String])
        def appendQueryParameter(self, key, value):
            pass
        
        @args(Uri.Builder, [String])
        def authority(self, authority):
            pass
        
        @args(Uri, [])
        def build(self):
            pass
        
        @args(Uri.Builder, [])
        def clearQuery(self):
            pass
        
        @args(Uri.Builder, [String])
        def encodedAuthority(self, authority):
            pass
        
        @args(Uri.Builder, [String])
        def encodedFragment(self, fragment):
            pass
        
        @args(Uri.Builder, [String])
        def encodedOpaquePart(self, opaquePart):
            pass
        
        @args(Uri.Builder, [String])
        def path(self, path):
            pass
        
        @args(Uri.Builder, [String])
        def query(self, query):
            pass
        
        @args(Uri.Builder, [String])
        def scheme(self, scheme):
            pass
        
        @args(String, [])
        def toString(self):
            pass
    
    @abstract
    @args(Uri.Builder, [])
    def buildUpon(self):
        pass
    
    @args(int, [Uri])
    def compareTo(self, other):
        pass
    
    @static
    @args(String, [String])
    def decode(s):
        pass
    
    @static
    @args(String, [String, String])
    def encode(s, allow):
        pass
    
    @static
    @args(String, [String])
    def encode(s):
        pass
    
    @static
    @args(Uri, [File])
    def fromFile(file):
        pass
    
    @static
    @args(Uri, [String, String, String])
    def fromParts(scheme, ssp, fragment):
        pass
    
    @abstract
    @args(String, [])
    def getAuthority(self):
        pass
    
    @args(bool, [String, bool])
    def getBooleanQueryParameter(self, key, defaultValue):
        pass
    
    @abstract
    @args(String, [])
    def getEncodedAuthority(self):
        pass
    
    @abstract
    @args(String, [])
    def getEncodedFragment(self):
        pass
    
    @abstract
    @args(String, [])
    def getEncodedPath(self):
        pass
    
    @abstract
    @args(String, [])
    def getEncodedQuery(self):
        pass
    
    @abstract
    @args(String, [])
    def getEncodedSchemeSpecificPart(self):
        pass
    
    @abstract
    @args(String, [])
    def getEncodedUserInfo(self):
        pass
    
    @abstract
    @args(String, [])
    def getFragment(self):
        pass
    
    @abstract
    @args(String, [])
    def getHost(self):
        pass
    
    @abstract
    @args(String, [])
    def getLastPathSegment(self):
        pass
    
    @abstract
    @args(String, [])
    def getPath(self):
        pass
    
    @abstract
    @args(List(String), [])
    def getPathSegments(self):
        pass
    
    @abstract
    @args(int, [])
    def getPort(self):
        pass
    
    @abstract
    @args(String, [])
    def getQuery(self):
        pass
    
    @args(String, [String])
    def getQueryParameter(self, key):
        pass
    
    @args(Set(String), [])
    def getQueryParameterNames(self):
        pass
    
    @args(List(String), [String])
    def getQueryParameters(self, key):
        pass
    
    @abstract
    @args(String, [])
    def getScheme(self):
        pass
    
    @abstract
    @args(String, [])
    def getSchemeSpecificPart(self):
        pass
    
    @abstract
    @args(String, [])
    def getUserInfo(self):
        pass
    
    @args(bool, [])
    def isAbsolute(self):
        pass
    
    @abstract
    @args(bool, [])
    def isHierarchical(self):
        pass
    
    @args(bool, [])
    def isOpaque(self):
        pass
    
    @abstract
    @args(bool, [])
    def isRelative(self):
        pass
    
    @args(Uri, [])
    def normalizeScheme(self):
        pass
    
    @static
    @args(Uri, [String])
    def parse(uriString):
        pass
    
    @static
    @args(Uri, [Uri, String])
    def withAppendedPath(baseUri, pathSegment):
        pass
    
    @static
    @args(void, [Parcel, Uri])
    def writeToParcel(out, uri):
        pass
