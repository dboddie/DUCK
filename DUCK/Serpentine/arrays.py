"""
Copyright (C) 2017 David Boddie <david@boddie.org.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

"""~
## Arrays

The `arrays` module contains the `Arrays` class that provides methods for
creating arrays. This class is inherited by the `Compiler` class.

It also provides a function for determining whether the type of a variable
is an array or some other container, and a function for obtaining the type and
number of dimensions of an array.
"""

import ast

from Dalvik.dextypes import *

from common import *
from macros import *
import visitor

class Arrays:

    # Define a convenience method for creating arrays.
    
    def create_array(self, item_type, count_args, node, name, context, scope):
    
        """Returns a new variable whose type is an array type based on the
        item_type and item_dex_type specified, and whose size is given by the
        contents of the count_args variable, which is a list of unresolved
        objects."""
        
        # Register the item type.
        item_dex_type = create_dex_type(item_type)
        self.dex_objects.add_type(item_dex_type)
        
        return self._create_array(item_type, item_dex_type, count_args, node,
                                  name, context, scope)
    
    def _create_array(self, item_type, item_dex_type, count_args, node, name,
                      context, scope):
    
        dimensions = len(count_args)
        
        # Register the array type.
        array_type = ArrayContainer(dimensions, item_type, Array(("[" * dimensions) + item_dex_type.desc))
        array_dex_type = create_dex_type(array_type)
        self.dex_objects.add_type(array_dex_type)
        
        array_var = context.allocate(type_=array_type)
        count_obj = self.find_argument_object(node, name, count_args[0], context, scope)
        context.append(NewArray(array_var, count_obj, array_dex_type))
        
        # If the array contains other arrays then create those in a loop.
        if len(count_args) > 1:
        
            # Use a temporary variable as an index into the array and another to
            # hold the size of the array.
            index_obj = context.allocate(type_=dInt)
            context.append(LoadConstant(index_obj, 0))
            
            begin_target = Target("begin-%x" % id(array_var))
            
            # Begin the loop.
            context.append(begin_target)
            
            # Create a loop exit target for use after the body suite.
            loop_exit_target = Target("loop-exit-%x" % id(array_var))
            
            # Compare the index to the count.
            context.append(ComparisonMacro(if_eq, loop_exit_target, [index_obj, count_obj]))
            
            # Create an array for the item and assign it to the corresponding
            # entry in this array before freeing the variable allocated for it.
            item_array = self._create_array(item_type, item_dex_type,
                count_args[1:], node, name, context, scope)
            context.append(ArrayPut(item_array, array_var, index_obj))
            context.free()
            
            # Increment the array index.
            context.append(ConstantOperationMacro(add_int_lit8, index_obj, [index_obj, Constant(1, 8)]))
            
            # Jump to the test again.
            context.append(Goto(begin_target))
            
            # Append the loop exit target so that any else suite can be executed.
            context.append(loop_exit_target)
            
            # Free the index variable.
            context.free()
        
        # Free the count object if it is temporary.
        if count_obj.temporary():
            context.free()
        
        return array_var


def get_array_dimensions_and_item_type(value):

    """Retrieve the item type of an array as specified in either a Python
    list or a ast.List node and return the number of dimensions of the array
    and the item type."""
    
    # Use the convention that the number of nested lists is the number
    # of dimensions if given, otherwise the array is one-dimensional.
    dim = 1
    item_type = value
    while True:
    
        if type(item_type) == list:
        
            item_type = item_type[0]
            if type(item_type) != list:
                break

        elif isinstance(item_type, ast.List):
        
            item_type = item_type.elts[0]
            if not isinstance(item_type, ast.List):
                break
        
        dim += 1
    
    return dim, item_type


def array_or_container(obj, node):

    if isinstance(obj.type_, ArrayContainer):
        is_array = True
        container_type = None
        if obj.type_.dimensions > 1:
            item_types = [ArrayContainer(obj.type_.dimensions - 1, obj.type_.item_type,
                                         obj.type_.dex_type, obj.type_.wrapped_items)]
        else:
            item_types = [obj.type_.item_type]
    
    elif isinstance(obj.type_, TemplateClass):
        is_array = False
        # Use the container class that this template actually uses.
        container_type = obj.type_.container_class
        item_types = obj.type_.item_types_
    
    elif isinstance(obj.type_, visitor.ClassVisitor) and obj.type_.has_item_types():
        is_array = False
        # Use the container class that this template actually uses.
        container_type = obj.type_
        item_types = container_type.item_types()
    
    else:
        is_array = False
        container_type = None
        item_types = []
    
    return is_array, container_type, item_types
