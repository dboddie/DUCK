# The Serpentine Package

The compiler transforms nodes from a syntax tree into instructions for the
virtual machine. The implementation in the `Compiler` class performs the
transformation, as well as setting up and managing structures related to
defined classes, methods, variables and attributes/fields.


```python
import os, sys

from Dalvik.dex import *

from arrays import *
from builtins import Builtins
from macros import *
from methods import Methods
from utils import DexObjects
import visitor
import writer
```

## Compiler class

To incorporate common features, the `Compiler` class is derived from classes in
the [arrays](arrays.md), [methods](methods.md) and [builtins](builtins.md)
modules. *Ideally the implementations of the methods provided by these base
classes would not mutually depend on the presence of each other. Unfortunately,
the development of the compiler resulted in an instance that aggregates
features, making it difficult to create completely separate implementations.*

Initialisation of the compiler involves the creation of a `DexObjects` object
that holds information about the different categories of objects that are
needed for the final DEX file. It also holds information about resources, such
such as a list of include paths, and compiler options such as the warnings to
suppress and whether to keep source information for producing diagnostic
listings.


```python
class Compiler(Arrays, Methods, Builtins):

    def __init__(self):
    
        Methods.__init__(self)
        
        # Create an object to hold the objects that are found in Dex files.
        self.dex_objects = DexObjects()
        self.dex_objects.listing_file_name = os.getenv("DUCK_LISTING")
        self.dex_objects.keep_source = (self.dex_objects.listing_file_name != None)
        self.dex_objects.debug = os.getenv("DEBUG")
    
    def define_application_classes(self, package_name, code_file_name,
        res_module, app_base_classes = None):
    
        if not app_base_classes:
            # Find a class based on the standard Activity class.
            app_base_classes = ["android/app/Activity"]
        
        # Define the built-in types and register the module as the default
        # namespace in order to make it the first place we search for unknown
        # names.
        builtins = self.define_builtin_types()
        self.dex_objects.builtins = builtins
        
        # Share the cache of imported modules with other modules. This ensures that
        # there is only ever one definition per class.
        import_cache = builtins.imports
        
        # Add the application resources to the module cache.
        import_cache.update({"app_resources": res_module})
        
        vtr = visitor.Visitor(self.dex_objects.include_paths, package_name)
        module = vtr.load(code_file_name, import_cache)
        
        for class_ in module.ordered_classes():
            self.dex_objects.add_pending_class(class_)
        
        self.define_classes(module, code_file_name)
        
        # Find classes based on those specified in the app_base_classes list.
        app_classes = {}
        
        for base in app_base_classes:
        
            for class_ in module.ordered_classes():
            
                if class_.package() == module.package() == package_name and \
                    class_.superclass():
                    
                    # Check whether this class inherits from the current base class.
                    super_class = class_.superclass()
                    
                    while super_class:
                        if super_class.full_name() == base:
                            # It inherits from the base class, so break past the
                            # while loop's else clause.
                            break
                        else:
                            super_class = super_class.superclass()
                    else:
                        # Try the next class.
                        continue
                    
                    # Find the most specialised subclass of the base class.
                    while True:
                        for c in module.ordered_classes():
                            if class_.name_ in c.bases:
                                # Make this subclass the current class and break so
                                # that we search again.
                                class_ = c
                                break
                        else:
                            # No further subclasses were found.
                            break
                    
                    app_classes[base] = class_
                    break
            
            else:
                # If the classes are provided in a dictionary then check to see
                # if this missing one is required.
                if isinstance(app_base_classes, dict) and app_base_classes[base] != None:
                    raise NameLookupError("Failed to find an %s class for package %s." % (
                        base, repr(package_name)))
        
        return app_classes
    
    
    def define_build_classes(self, package_name, build_info_template,
                             app_classes):
    
        vtr = visitor.Visitor(self.dex_objects.include_paths, package_name)
        module = vtr.load(build_info_template)
        module.attributes["__package__"] = ast.Str(app_classes[0].package())
        
        for class_ in module.ordered_classes():
            self.dex_objects.add_pending_class(class_)
        
        self.define_classes(module, build_info_template)
    
    
    def define_builtin_types(self):
    
        vtr = visitor.Visitor(self.dex_objects.include_paths)
        for include_path in self.dex_objects.include_paths:
            try:
                module = vtr.load(os.path.join(include_path, "serpentine", "builtins.py"))
                break
            except IOError:
                pass
        else:
            raise ImportError("Failed to find the 'builtins.py' module on the include paths.")
        
        # Load the java.lang module and store the visitor for the String
        # class in the type map.
        stdlib_path = None
        
        for path in self.dex_objects.include_paths:
            if module.import_module("java.lang", path, True) != None:
                stdlib_path = path
                break
        
        module.import_module("java.lang.annotation", stdlib_path, True)
        
        class_ = module.find_object("java.lang.String")
        value_to_type_map[str] = value_to_type_map[unicode] = class_
        type_name_to_type_map["str"] = class_
	
        # Register Object as the generic type for template parameter types, but
        # mark it as a placeholder to make it easier for us to replace it later.
        ### This should be generalised, perhaps by inspecting the declared
        ### parameters for a class.
        obj = Placeholder(module.find_object("java.lang.Object"))
        type_name_to_type_map["E"] = obj
        type_name_to_type_map["T"] = obj
        type_name_to_type_map["K"] = obj
        type_name_to_type_map["V"] = obj
        
        wrapper_types = self.dex_objects.wrapper_types
        wrapper_types[Boolean] = module.find_object("java.lang.Boolean")
        wrapper_types[Int] = module.find_object("java.lang.Integer")
        wrapper_types[Float] = module.find_object("java.lang.Float",)
        wrapper_types[Long] = module.find_object("java.lang.Long")
        wrapper_types[Double] = module.find_object("java.lang.Double")
        wrapper_types[Short] = module.find_object("java.lang.Short")
        wrapper_types[Byte] = module.find_object("java.lang.Byte")
        wrapper_types[Void] = module.find_object("java.lang.Void")
        
        unwrapped_types = self.dex_objects.unwrapped_types
        unwrapped_types[wrapper_types[Boolean]] = Boolean
        unwrapped_types[wrapper_types[Int]] = Int
        unwrapped_types[wrapper_types[Float]] = Float
        unwrapped_types[wrapper_types[Long]] = Long
        unwrapped_types[wrapper_types[Double]] = Double
        unwrapped_types[wrapper_types[Byte]] = Byte
        unwrapped_types[wrapper_types[Short]] = Short
        unwrapped_types[wrapper_types[Void]] = Void
        
        wrapper_unpack_methods = self.dex_objects.wrapper_unpack_methods
        wrapper_unpack_methods[wrapper_types[Boolean]] = "booleanValue"
        wrapper_unpack_methods[wrapper_types[Int]] = "intValue"
        wrapper_unpack_methods[wrapper_types[Float]] = "floatValue"
        wrapper_unpack_methods[wrapper_types[Long]] = "longValue"
        wrapper_unpack_methods[wrapper_types[Double]] = "doubleValue"
        wrapper_unpack_methods[wrapper_types[Short]] = "shortValue"
        wrapper_unpack_methods[wrapper_types[Byte]] = "byteValue"
        
        # Load the java.util module and store the visitors for the LinkedList,
        # HashMap and HashSet classes.
        module.import_module("java.util", stdlib_path, True)
        default_container_classes = self.dex_objects.default_container_classes
        
        linked_list = module.find_object("java.util.LinkedList")
        hash_map = module.find_object("java.util.HashMap")
        hash_set = module.find_object("java.util.HashSet")
        
        default_container_classes["java.util.LinkedList"] = linked_list
        default_container_classes["java.util.HashMap"] = hash_map
        default_container_classes["java.util.HashSet"] = hash_set
        
        type_name_to_type_map["list"] = linked_list
        type_name_to_type_map["dict"] = hash_map
        type_name_to_type_map["set"] = hash_set
        
        custom_exceptions = self.dex_objects.custom_exceptions
        custom_exceptions[IndexError] = module.find_object("IndexError")
        custom_exceptions[IOError] = module.find_object("IOError")
        custom_exceptions[KeyError] = module.find_object("KeyError")
        custom_exceptions[TypeError] = module.find_object("TypeError")
        custom_exceptions[ValueError] = module.find_object("ValueError")
        
        return module
```

### Defining classes

The following method is used to process pending classes that have been
found in the source files, and also those that are generated as part of
compiling template classes.


```python
    def define_classes(self, module, code_file_name):
    
        while self.dex_objects.pending:
            self.define_class(self.dex_objects.pop_pending_class(), module,
                              code_file_name)
```

Each class is analysed and processed to create meta-data for the resulting
DEX file. Fields (attributes) are recorded, methods and interfaces are
catalogued, and annotations are processed to produce the correct access
and visibility properties for methods and inner classes.

Non-abstract methods are passed to the `create_code` method so that code
can be generated from the abstract syntax tree they contain.

Finally, inner classes of the class are queued for processing.


```python
    def define_class(self, class_, module, code_file_name):
    
        # If the class is a template class, but does not define any replacement
        # types then it has not been specialised and is not included in the output
        # file. This may cause problems if we want to allow template classes to
        # contain inner classes.
        if class_.replace_ and not class_.specialised:
            return
        
        # Establish some basic information about the class.
        class_name = Name(u"L%s;" % class_.full_name())
        external = class_.external(module) and \
                   not class_.package().startswith("serpentine.")
        reference_only = class_.annotations() == [] and class_.interfaces() == []
        interface = class_.is_interface()
        
        # Record whether the class is a specialised version of a template class.
        specialised = class_.specialised
        
        if external:
            # Only external classes containing interfaces and/or annotations
            # are included in the set of classes.
            access = 0
            if class_.annotations():
                access = access | ACC_ANNOTATION
            
            # The class was purely there for reference so don't actually define it.
            if access == 0:
                return
            
            if class_.interfaces():
                access = access | ACC_INTERFACE
            
            access = access | ACC_ABSTRACT | ACC_PUBLIC
        else:
            access = ACC_PUBLIC
        
        if class_.static():
            access = access | ACC_STATIC
        if class_.final():
            access = access | ACC_FINAL
        if interface:
            access = access | ACC_ABSTRACT | ACC_INTERFACE
        
        # Record the methods of the class that have been processed.
        self.dex_objects.processed.setdefault(class_, set())
        self.dex_objects.implemented.setdefault(class_, set())
        
        # Extract static fields and static values.
        static_fields = []
        static_values = []
        
        for key, field in class_.fields.items():
        
            if not isinstance(field, visitor.StaticField):
                field.type_ = self.create_value_type(field.type_)
                continue
            
            # Static fields that are types or classes can be converted directly
            # to fields. Otherwise, their types need to be resolved.
            if isinstance(field.type_, ast.Name) and \
               field.type_.id in value_name_to_type_map:
            
                field.type_, value = self.value_type_value_from_value(field.type_)
                ### Store the static value. Note that some static values will need
                ### to be initialised in a <clinit> method which we need to create
                ### ourselves.
                static_values.append(value)
            
            if is_primitive(field.type_):
                static_fields.append(
                    (convert_field(field), AccessFlags(ACC_STATIC | ACC_PUBLIC))
                    )
            else:
                raise TypeError, "Unhandled static field '%s' of type %s in %s." % (
                    key, visitor.repr_ast(field.type_), class_.name())
        
        # Dereference the references in the list of interfaces, collecting methods
        # that must be implemented by this class.
        interfaces = []
        must_implement = {}
        
        for ref in class_.interfaces():
            obj = class_.module.find_object(ref.value)
            if obj is None:
                raise NameLookupError, "Could not find interface '%s' referred to in %s." % (
                    ref.value, class_.name())
            else:
                interfaces.append(create_class_name(obj))
                # Ensure that we visit this interface class so that it can be
                # included in the package as required.
                self.dex_objects.add_pending_class(obj)
            
            # Ensure that this class implements the declared interfaces if it is
            # more than just a declaration.
            if not external and not interface:
                for method_name, methods in obj.methods.items():
                    for method in methods:
                        # Create a prototype for the method from its decoration.
                        prototype = self.create_prototype(method)
                        signature = (method.name_, prototype)
                        must_implement[signature] = method
        
        # Extract methods and additional undeclared instance fields.
        direct_methods = []
        virtual_methods = []
        signatures = set()
        
        # Process the methods in the order in which they were declared.
        
        for method_name in class_.method_names:
        
            for method in class_.methods[method_name]:
            
                # Skip the method if it has already been processed.
                if class_ in self.dex_objects.processed and \
                   method in self.dex_objects.processed[class_]: continue
                
                # If the class is specialised but the method is not then skip it.
                if specialised and not method.specialised:
                    continue
                
                # Mark the method as processed.
                self.dex_objects.processed[class_].add(method)
                
                virtual = method.virtual()
                static = method.static()
                
                if method_name == u"__init__":
                    constructor = True
                    virtual = False
                else:
                    constructor = False
                    virtual = not static
                
                method_access = create_access_flags(constructor, external,
                    reference_only, method.decorators, interface, static)
                
                # Create code if code should be created for this method.
                # We pass the dictionary of instance fields so that it can be
                # updated as new fields are defined.
                if not external and not interface:
                    code = self.create_code(method)
                else:
                    code = None
                
                if code != None or not reference_only or (not external and interface):
                
                    # Create a prototype for the method from its decoration.
                    prototype = self.create_prototype(method)
                    signature = (method.name_, prototype)
                    self.dex_objects.implemented[class_].add(signature)
                    
                    # If a method with the same name and prototype has already been
                    # defined for this class then report an error.
                    if signature in signatures:
                        raise NameLookupError("%s.%s cannot be defined at line %i of file %s "
                            "because a method with the same signature already exists." % (
                            method.class_.name(), method.name_, method.line_number,
                            method.file_name()))
                    
                    signatures.add(signature)
                    
                    dex_method = Method(
                        class_name,
                        prototype,
                        MemberName(method.name()),
                        method_access,
                        code)
                    
                    if virtual:
                        virtual_methods.append(dex_method)
                        self.dex_objects.methods.add(dex_method)
                    else:
                        direct_methods.append(dex_method)
        
        # Report any methods required by interfaces that are missing unless they
        # are suppressed by the build script.
        if must_implement:
            for signature, method in must_implement.items():
                if signature in self.dex_objects.implemented[class_]:
                    continue
                elif (signature[0], str(method.class_)) not in self.dex_objects.suppress_warnings:
                    print ("Warning: Method '%s' from interface '%s' not "
                        "implemented in class '%s' in file %s." % (signature[0],
                        method.class_, class_, class_.file_name()))
                    # Don't warn about this again if the class is revisited.
                    self.dex_objects.implemented[class_].add(signature)
        
        class_annotations = []
        
        for anno_name, pairs in class_.annotations():
        
            # Reconstruct the pairs of values from the syntax tree data in the
            # list of pairs.
            new_pairs = []
            for key, value in pairs:
            
                if type(value) == list:
                    values = value
                else:
                    values = [value]
                
                new_values = []
                for value in values:
                
                    if isinstance(value, ast.Call) and isinstance(value.func, ast.Name):
                    
                        if value.func.id == "Field" and len(value.args) == 2:
                            # Fields should be specified as a fully-qualified
                            # class name and a member name. The arguments should
                            # both be string constants.
                            field_name = value.args[0].s
                            member_name = value.args[1].s
                            
                            new_values.append(Field(Name("L%s;" % field_name),
                                                    Name("L%s;" % field_name),
                                                    MemberName(member_name)))
                        
                        elif value.func.id == "Type" and len(value.args) == 1:
                            # Types should only contain a single fully-qualified
                            # name.
                            type_name = value.args[0].id
                            new_values.append(Type(Name("L%s;" % type_name)))
                        
                        else:
                            raise ValueError("Unhandled type '%s' in annotation." % value.func.id)
                    
                    elif isinstance(value, ast.Num) and type(value.n) == int:
                        new_values.append(Int(value.n))
                    
                    elif isinstance(value, ast.Str):
                        if type(value.s) == str or type(value.s) == unicode:
                            new_values.append(value.s)
                        else:
                            raise ValueError("Unhandled type '%s' in annotation." % value.s)
                    else:
                        raise ValueError("Unhandled type '%s' in annotation." % value)
                
                if len(new_values) == 1:
                    new_values = new_values[0]
                
                # The key itself must be a constant string.
                new_pairs.append((key.s, new_values))
            
            # The annotation name must be a constant string.
            class_annotations.append(
                (Visibility(VISIBILITY_RUNTIME),
                 Annotation(Name("L%s;" % anno_name.s), new_pairs))
                )
        
        # Append class hierarchy annotations.
        if class_.classes:
        
            inner_classes = []
            for inner_class in class_.ordered_classes():
                inner_classes.append(Type(create_class_name(inner_class)))
            
            class_annotations.append(
                (Visibility(VISIBILITY_SYSTEM),
                 Annotation(Name("Ldalvik/annotation/MemberClasses;"), [
                    ("value", inner_classes)]))
                )
        
        if isinstance(class_.parent, visitor.ClassVisitor):
        
            class_annotations.append(
                (Visibility(VISIBILITY_SYSTEM),
                 Annotation(Name("Ldalvik/annotation/EnclosingClass;"), [
                    ("value", Type(create_class_name(class_.parent)))
                    ]))
                )
            class_annotations.append(
                (Visibility(VISIBILITY_SYSTEM),
                 Annotation(Name("Ldalvik/annotation/InnerClass;"), [
                    ("accessFlags", Int(ACC_FINAL | ACC_STATIC | ACC_PUBLIC)),
                    ("name", class_.name_)
                    ]))
                )
        
        # If the class is an implementation of a template class then include a
        # signature annotation.
        if class_.has_item_types():
        
            item_type_full_names = []
            
            for item_type_obj in class_.item_types():
            
                if item_type_obj is None:
                    raise NameLookupError("Could not find '%s' referred to in %s." % (
                        class_.item_type_name(), class_.name()))
                
                # Resolve the type.
                item_type = self.create_value_type(item_type_obj)
                
                # Wrap the type in an Object if it is a primitive type.
                wrapper_type = self.get_wrapper_type(item_type)
                item_type_full_names.append("L%s;" % wrapper_type.full_name())
            
            if not class_.superclass():
                raise TypeError("Unknown superclass '%s' referred to in %s." % (
                        class_.bases[0], class_.name()))
            
            class_annotations.append(
                (Visibility(VISIBILITY_SYSTEM),
                 Annotation(Name("Ldalvik/annotation/Signature;"), [
                    ("value", [
                        "L%s" % class_.superclass().full_name(),
                        "<"] + item_type_full_names + [">;"])
                    ]))
                )
        
        instance_fields = []
        field_annotations = []
        
        # Extract the final instance fields from the class.
        for key, field in class_.fields.items():
            if not isinstance(field, visitor.StaticField):
                instance_fields.append((convert_field(field), AccessFlags(ACC_PUBLIC)))
        
        if class_.superclass():
            superclass = Name(u"L%s;" % class_.superclass().full_name())
        else:
            superclass = Name(u"Ljava/lang/Object;")
        
        if class_annotations or field_annotations:
            annotations = Annotations(class_annotations, field_annotations, [], [])
        else:
            annotations = None
        
        try:
            # If we are revisiting this class definition then only add new methods
            # to it.
            classdef = self.dex_objects.classes[class_]
            classdef.class_data.direct_methods += direct_methods
            classdef.class_data.virtual_methods += virtual_methods
        
        except KeyError:
            classdef = ClassDef(
                class_name,
                AccessFlags(access),
                superclass,
                interfaces,
                os.path.split(code_file_name)[1],
                annotations,
                ClassData(static_fields, instance_fields,
                          direct_methods, virtual_methods),
                static_values)
        
        self.dex_objects.classes[class_] = classdef
        
        # Examine any inner classes.
        for inner_class in class_.ordered_classes():
            self.dex_objects.add_pending_class(inner_class)
```

### Code generation

The following method prepares for code generation by performing a number
of tasks.

It determines the number of register slots that will be occupied by the
method parameters and resolves their types so that code can be generated
to perform type checks and casts on entry to the method.

A `Context` object is used to keep track of the register use during code
generation. This is passed to the `process_node` method along with the
object representing the method and the main syntax tree node.

When generation has finished, the register allocation is optimised and the
number of incoming and outgoing registers is determined. Other requirements
involving register usage are also handled.

A `Code` object is returned for further processing.


```python
        # Temporary variables allocated as a result of this function must be freed
        # by the caller.
        
        array_obj = self.process_node(node.value, context, scope)
        if array_obj is None:
            raise NameLookupError, "Failed to find '%s' at line %i of file %s." % (
                node.value, node.lineno, scope.file_name())
        
        is_an_array, container_type, item_types = array_or_container(array_obj, node)
        
        index_obj = self.process_node(node.slice.value, context, scope)
        if index_obj is None:
            raise ValueError, "Invalid array subscript '%s' at line %i of file %s." % (
                node.slice.value, node.lineno, scope.file_name())
        
        return array_obj, index_obj, is_an_array, container_type, item_types
    
    
    def complete_dex_objects(self, package_name):
    
        # Extract types, strings, fields and methods from the classes.
        for class_ in self.dex_objects.classes.values():
        
            # Add types and string to the main collection.
            self.dex_objects.types.add(class_.class_type)
            self.dex_objects.types.add(class_.superclass_type)
            self.dex_objects.types.update(class_.interfaces)
            self.dex_objects.strings.add(class_.source_file)
            
            # Add the fields and methods to the main collection.
            
            # Extract the fields from the class data.
            static_fields = map(lambda (field, access): field, class_.class_data.static_fields)
            instance_fields = map(lambda (field, access): field, class_.class_data.instance_fields)
            self.dex_objects.fields.update(static_fields)
            self.dex_objects.fields.update(instance_fields)
            
            # Include all the methods from classes in the main package, even if
            # they are not called, and ensure that they are registered.
            # Only include methods from external packages if they are called.
            direct_methods = []
            for method in class_.class_data.direct_methods:
            
                method_package = method.package()
                included = (method_package == method.package()) or \
                           method_package.startswith("serpentine.")
                
                if included:
                    # If the method was called before it was defined then we need
                    # to replace its entry in the collection.
                    if method in self.dex_objects.methods:
                        self.dex_objects.methods.remove(method)
                    
                    self.dex_objects.methods.add(method)
                    direct_methods.append(method)
                else:
                    if method in self.dex_objects.methods:
                        direct_methods.append(method)
                        # Replace the existing entry with one with code.
                        if method.code != None:
                            # Note that the set type forces us to remove the entry
                            # if we want to replace it.
                            self.dex_objects.methods.remove(method)
                            self.dex_objects.methods.add(method)
            
            class_.class_data.direct_methods = direct_methods
            
            for method in class_.class_data.virtual_methods:
            
                if method in self.dex_objects.methods:
                    self.dex_objects.methods.remove(method)
                self.dex_objects.methods.add(method)
            
            # Extract strings and types from any annotations.
            if class_.annotations:
                self.read_annotations(class_.annotations.class_annotations)
                for field, annotations in class_.annotations.fields:
                    self.read_annotations(annotations)
                for method, annotations in class_.annotations.methods:
                    self.read_annotations(annotations)
                for parameters, annotations in class_.annotations.parameters:
                    self.read_annotations(annotations)
        
        # Extract types and strings from the fields.
        for field in self.dex_objects.fields:
        
            self.dex_objects.types.add(field.class_type)
            self.dex_objects.types.add(field.type_)
            self.dex_objects.strings.add(field.name.desc)
        
        # Extract types, prototypes and strings from the methods.
        for method in self.dex_objects.methods:
        
            self.dex_objects.types.add(method.class_type)
            self.dex_objects.prototypes.add(method.proto)
            self.dex_objects.strings.add(method.name.desc)
            
            # Extract types from the prototypes.
            for type_ in method.proto.shorty:
                if not isinstance(type_, ReferenceType):
                    self.dex_objects.types.add(type_)
            self.dex_objects.types.add(method.proto.return_type)
            self.dex_objects.types.update(method.proto.parameters)
            
            # Create strings from the prototypes.
            self.dex_objects.strings.add(u"".join(map(lambda t: t.desc, method.proto.shorty)))
        
        # Extract value types from arrays.
        value_types = set()
        for type_ in self.dex_objects.types:
        
            if isinstance(type_, Array):
                value_types.add(type_.item_type())
        
        self.dex_objects.types.update(value_types)
        
        # Extract strings from the types.
        for type_ in self.dex_objects.types:
        
            self.dex_objects.strings.add(type_.desc)
        
        # Sort the strings.
        self.dex_objects.strings = list(self.dex_objects.strings)
        self.dex_objects.strings.sort(cmp=utf8_sort)
        
        # Sort the types by the indices of their strings in the string list.
        type_indices = map(lambda type_: (self.dex_objects.strings.index(type_.desc), type_),
                           self.dex_objects.types)
        type_indices.sort()
        self.dex_objects.types = map(lambda (i, type_): type_, type_indices)
        
        # Sort the prototypes by the indices of their return types then by their
        # parameters in the types list.
        proto_indices = []
        
        for proto in self.dex_objects.prototypes:
        
            major = self.dex_objects.types.index(proto.return_type)
            minor = map(lambda type_: self.dex_objects.types.index(type_), proto.parameters)
            proto_indices.append((major, minor, proto))
        
        proto_indices.sort()
        self.dex_objects.prototypes = map(lambda (major, minor, proto): proto,
                                          proto_indices)
        
        # Sort the fields by the index of the class type in the types list, the
        # index of the field name in the strings list and the index of the type in
        # the types list.
        field_indices = []
        
        for field in self.dex_objects.fields:
        
            major = self.dex_objects.types.index(field.class_type)
            inter = self.dex_objects.strings.index(field.name.desc)
            minor = self.dex_objects.types.index(field.type_)
            field_indices.append((major, inter, minor, field))
        
        field_indices.sort()
        self.dex_objects.fields = map(lambda (major, inter, minor, field):
                                      field, field_indices)
        
        # Sort the methods by the index of the class type in the types list, the
        # index of the method name in the strings list and the index of the
        # prototype in the prototypes list.
        method_indices = []
        
        for method in self.dex_objects.methods:
        
            major = self.dex_objects.types.index(method.class_type)
            inter = self.dex_objects.strings.index(method.name.desc)
            minor = self.dex_objects.prototypes.index(method.proto)
            method_indices.append((major, inter, minor, method))
        
        method_indices.sort()
        self.dex_objects.methods = map(lambda (major, inter, minor, method): method,
                                       method_indices)
        
        # Ensure that superclasses and interfaces are listed before the classes
        # that refer to them.
        class_dict = {}
        
        for class_ in self.dex_objects.classes.values():
            class_dict[class_.class_type] = class_
        
        # Keep a copy of this dictionary for future reference. The original will
        # be gradually deleted as classes are added to the ordered list.
        self.dex_objects.class_dict = class_dict.copy()
        
        ordered_classes = []
        self.add_ordered_classes(self.dex_objects.classes.values(), class_dict, ordered_classes)
        
        self.dex_objects.ordered_classes = ordered_classes
    
    
    def read_annotations(self, annotations):
    
        # The annotations are a list of tuples, each of which contains the
        # visibility of the annotation and the annotation itself.
        for visibility, annotation in annotations:
        
            # Add the type to the types in the main collection.
            self.dex_objects.types.add(annotation.type)
            
            # Each annotation contains a type and a list of key, value pairs.
            for key, value in annotation.values:
            
                # Add the key to the strings in the main collection.
                self.dex_objects.strings.add(key)
                
                # The value in each pair can be a list of values or any of the
                # other encoded value types.
                if type(value) == list or type(value) == tuple:
                    values = value
                else:
                    values = [value]
                
                for var in values:
                    if isinstance(var, Type):
                        self.dex_objects.types.add(var.value)
                    elif type(var) == str or type(var) == unicode:
                        self.dex_objects.strings.add(var)
                    elif isinstance(var, Field):
                        self.dex_objects.fields.add(var)
    
    
    def add_ordered_classes(self, classes, class_dict, ordered_classes):
    
        # Sort the classes in order to generate a deterministic sequence of them
        # each time the same program is compiled.
        classes = map(lambda class_: (class_.class_type, class_), classes)
        classes.sort()
        
        for class_type, class_ in classes:
        
            # If the class itself is not in the dictionary - perhaps because it was
            # a superclass of another class - then do not try to add it to the
            # ordered list again.
            if class_type not in class_dict:
                continue
            
            # Append the class to the list of ordered classes and remove its name
            # from the dictionary of named classes to ensure that it is not
            # appended again.
            ordered_classes.append(class_)
            del class_dict[class_type]
            
            # If the class has a superclass that is in the dictionary of named
            # classes we need to describe then add that to the list of ordered
            # classes.
            if class_.superclass_type in class_dict:
                self.add_ordered_classes([class_dict[class_.superclass_type]],
                                    class_dict, ordered_classes)
            
            # If the class has interfaces that are in the dictionary of named
            # classes we need to describe, add them to the list of ordered classes.
            interfaces = []
            for name in class_.interfaces:
                if name in class_dict:
                    interfaces.append(class_dict[name])
            
            self.add_ordered_classes(interfaces, class_dict, ordered_classes)
    
    
    def assemble_method_code(self):
    
        # Keep information about the source code for methods if specified.
        sources = []
        
        for method in self.dex_objects.methods:
        
            if method.code is None:
                continue
            
            # Remove redundant Goto macros.
            i = 0
            while i < len(method.code.instructions):
            
                instruction = method.code.instructions[i]
                
                if isinstance(instruction, Target):
                
                    # Backtrack, looking for a Goto instruction.
                    j = i - 1
                    current = instruction
                    while j > 0:
                    
                        instruction = method.code.instructions[j]
                        
                        if isinstance(instruction, Branch):
                            if instruction.target == current:
                                # Remove the redundant branch and continue
                                # looking for preceding ones.
                                del method.code.instructions[j]
                                # Compensate for the removed macro.
                                i -= 1
                            else:
                                # A branch to another label.
                                break
                        
                        elif not isinstance(instruction, Target):
                            # A non-branch or target.
                            break
                        
                        j -= 1
                i += 1
            
            # Expand any macros but keep branches and targets for later
            # resolution. Some macros will expand to contain branch macros.
            # Calculate the addresses of the targets in the newly expanded
            # code, taking care to calculate the initial sizes of branch
            # placeholder instructions.
            instructions = []
            targets = {}
            addr = 0
            
            for instruction in method.code.instructions:
            
                if isinstance(instruction, Target):
                    targets[instruction] = addr
                    instructions.append(instruction)
                
                elif isinstance(instruction, Branch):
                    instructions.append(instruction)
                    addr += instruction.size()
                
                elif isinstance(instruction, Macro):
                    for inst in instruction.expand(self.dex_objects):
                        instructions.append(inst)
                        if isinstance(inst, Branch):
                            addr += inst.size()
                        else:
                            addr += inst.size
                else:
                    # Source objects and anything else
                    instructions.append(instruction)
                    addr += instruction.size
            
            # Provisionally expand branch instructions in order to determine
            # their sizes - some goto instructions are larger than the initial
            # size.
            rescan = True
            
            while rescan:
            
                new_targets = {}
                addr = 0
                rescan = False
                
                for instruction in instructions:
                
                    if isinstance(instruction, Target):
                        new_targets[instruction] = addr
                    
                    elif isinstance(instruction, Branch):
                        # Expand the macro to determine the size of its contents
                        # but do not replace it yet.
                        dest = targets[instruction.target]
                        original_size = instruction.size()
                        inst = instruction.resolve(dest - addr, self.dex_objects)
                        addr += inst.size
                        if original_size != inst.size:
                            rescan = True
                    else:
                        addr += instruction.size
                
                targets = new_targets
            
            addr = 0
            
            # Prepare a dictionary to hold the addresses of instructions needed
            # for exception handling.
            try_offsets = {}
            
            for try_info in method.code.tries_and_handlers:
            
                try_offsets[try_info.begin] = None
                try_offsets[try_info.end] = None
                for handler_target, handler_type in try_info.handlers:
                    try_offsets[handler_target] = None
            
            new_instructions = []
            
            for instruction in instructions:
            
                if isinstance(instruction, Branch):
                    # Replace the placeholder with a resolved instruction.
                    dest = targets[instruction.target]
                    instruction = instruction.resolve(dest - addr, self.dex_objects)
                    new_instructions.append(instruction)
                
                elif isinstance(instruction, Target):
                    if instruction in try_offsets:
                        try_offsets[instruction] = addr
                    
                    continue
                
                else:
                    new_instructions.append(instruction)
                
                addr += instruction.size
            
            if self.dex_objects.keep_source:
                sources.append((method, new_instructions))
            
            # Remove any Source placeholders.
            new_instructions = filter(lambda inst: not isinstance(inst, Source),
                                      new_instructions)
            
            # Assemble the resulting instructions.
            method.code.instructions = assemble(new_instructions)
            
            # Construct a structure containing information about the tries and
            # handlers used in the method.
            tries_and_handlers = []
            
            for try_info in method.code.tries_and_handlers:
            
                catch_type_handlers = []
                catch_all_offset = None
                
                for handler_target, handler_type in try_info.handlers:
                    if handler_type is None:
                        catch_all_offset = try_offsets[handler_target]
                    else:
                        catch_type_handlers.append((create_dex_type(handler_type),
                                                    try_offsets[handler_target]))
                
                # No catch-all handler at the moment.
                catch_handler = (catch_type_handlers, catch_all_offset)
                
                start = try_offsets[try_info.begin]
                count = try_offsets[try_info.end] - start
                tries_and_handlers.append((start, count, catch_handler))
            
            method.code.tries_and_handlers = tries_and_handlers
        
        return sources
```

### Creating a DEX file

The `create_file` method is the interface to the compiler for front-end
tools, such as the [buildhelper](../../Tools/docs/buildhelper.md) module.


```python
    def create_file(self, package_name, res_module, code_file, include_paths,
                    output_file, app_base_classes = None, suppress_warnings = None):
    
        self.dex_objects.include_paths = include_paths
        self.dex_objects.suppress_warnings = suppress_warnings or []
        
        # Create application classes and classes that describe the build.
        try:
            app_classes = self.define_application_classes(package_name,
                code_file, res_module, app_base_classes)
        except:
            if self.dex_objects.debug:
                raise
            else:
                type_, value, traceback = sys.exc_info()
                sys.stderr.write(str(value) + "\n")
                sys.exit(1)
        
        build_info_template = os.path.join(os.path.split(include_paths[-1])[0],
                                           "Meta", "buildinfo.py")
        self.define_build_classes(package_name, build_info_template,
                                  app_classes.values())
        
        # Complete the other entries in the collection of objects.
        self.complete_dex_objects(package_name)
        
        # Add code to the methods now that the final order of them has been
        # established.
        sources = self.assemble_method_code()
        
        if self.dex_objects.keep_source:
            writer.write_listing(self.dex_objects.listing_file_name, sources,
                                 self.dex_objects)
        d = Dex()
        d.create(self.dex_objects.strings, self.dex_objects.types,
                 self.dex_objects.prototypes, self.dex_objects.fields,
                 self.dex_objects.methods, self.dex_objects.ordered_classes)
        d.save(output_file)
        
        app_class_names = {}
        for base_name, class_ in app_classes.items():
            app_class_names[base_name] = class_.name()
        
        return app_class_names, self.dex_objects.permissions
```



## Files

* [\_\_init\_\_.py](../\_\_init\_\_.py)
* [arrays.py](../arrays.py)
* [builtins.py](../builtins.py)
* [common.py](../common.py)
* [macros.py](../macros.py)
* [methods.py](../methods.py)
* [utils.py](../utils.py)
* [visitor.py](../visitor.py)
* [writer.py](../writer.py)
