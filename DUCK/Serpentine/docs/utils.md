
```python
    def __init__(self):
    
        # Collections corresponding to those in the final DEX file. These will
        # be replaced by lists when the sections of the file are constructed.
        self.strings = set()
        self.types = set()
        self.prototypes = set()
        self.fields = set()
        self.methods = set()
        self.classes = {}
        
        self.processed = {}
        self.implemented = {}
        self.pending = set()
        self.pending_order = []
        
        # Maintain a stack of suites that allow break/continue.
        self.suites = []
        
        # Potentially keep information about the source for methods for when
        # a listing needs to be produced.
        self.keep_source = False
        self.method_args = {}
        self.method_vars = {}
        self.method_tree = {}
        
        # Record the methods that need permissions.
        self.permissions = {}
        
        # Record whether an __init__ call was made in a method. This should
        # help to catch cases where an __init__ method does not contain a call
        # to an __init__ method in the base class.
        self.init_call = False
        
        # Keep a reference to the builtins module.
        self.builtins = None
        
        # Report debugging information.
        self.debug = False
        
        # The following dictionaries are initialised when the built-in types are
        # defined.
        self.wrapper_types = {}
        self.wrapper_unpack_methods = {}
        self.unwrapped_types = {}
        self.default_container_classes = {}
        self.custom_exceptions = {}
        
        self.include_paths = []
        self.suppress_warnings = []
        self.listing_file_name = None
    
    def add_type(self, type_):
    
        self.types.add(type_)
        self.strings.add(type_.desc)
    
    def add_pending_class(self, class_):
    
        if isinstance(class_, TemplateClass):
            # If the class is a specialised version of a template class then
            # add the template class instead.
            class_ = class_.container_class
        
        if class_ not in self.processed and class_ not in self.pending:
            self.pending.add(class_)
            self.pending_order.append(class_)
    
    def add_pending_class_for_method(self, class_, method):
    
        if isinstance(class_, TemplateClass):
            class_ = class_.container_class
        
        if class_ in self.pending:
            return
        
        if class_ in self.processed and method in self.processed[class_]:
            return
        
        self.pending.add(class_)
        self.pending_order.append(class_)
    
    def pop_pending_class(self):
    
        class_ = self.pending_order.pop(0)
        self.pending.remove(class_)
        return class_
```



## Files

* [\_\_init\_\_.py](../\_\_init\_\_.py)
* [arrays.py](../arrays.py)
* [builtins.py](../builtins.py)
* [common.py](../common.py)
* [macros.py](../macros.py)
* [methods.py](../methods.py)
* [utils.py](../utils.py)
* [visitor.py](../visitor.py)
* [writer.py](../writer.py)
