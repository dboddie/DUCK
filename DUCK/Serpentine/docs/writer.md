
```python
import codecs

from Dalvik.bytecode import check_cast, const_string, filled_new_array_range, \
    format_10t, format_20t, format_22c, format_30t, format_35c, format_3rc, \
    instance_of, new_array, new_instance
from Dalvik.dextypes import TypeDescriptor
from macros import ArrayContainer, Null, Source, TemplateClass
import ast

class HTMLWriter:

    inline_markup = set(["em", "h1", "h2", "h3", "span", "strong", "tt"])
    augmented = {
        ast.Add: "+=", ast.Sub: "-=", ast.Mult: "*=", ast.Div: "/=",
        ast.FloorDiv: "//=", ast.Mod: "%=", ast.Pow: "**=",
        ast.RShift: ">>=", ast.LShift: "<<=", ast.BitAnd: "&=",
        ast.BitXor: "^=", ast.BitOr: "|="
        }
    
    def __init__(self, f, dex_objects):
    
        self.f = f
        self.dex_objects = dex_objects
        self.tags = []
        self.in_pre = False
        self.c = 0
        self.indent = 0
    
    def _html(self, text):
    
        return text.replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;")
    
    def html(self, text):
    
        pieces = text.split(" ")
        for i, piece in enumerate(pieces):
        
            self.f.write(self._html(piece))
            self.c += len(piece)
            
            if i < len(pieces) - 1:
                if self.c >= 80:
                    self.f.write("\n")
                    self.c = 0
                else:
                    self.f.write(" ")
                    self.c += 1
    
    def raw(self, text):
    
        self.f.write(text)
    
    def begin(self, name, attributes = []):
    
        attr = ""
        if attributes:
            attr = " " + " ".join(attributes)
        self.tags.append(name)
        
        self.f.write("<%s%s>" % (name, attr))
        
        if not self.in_pre and name not in self.inline_markup:
            self.f.write("\n")
        
        if name == "pre":
            self.in_pre = True
    
    def end(self):
    
        name = self.tags.pop()
        if name == "pre":
            self.in_pre = False
        
        self.f.write("</%s>" % name)
        if not self.in_pre:
            self.f.write("\n")
    
    def ele(self, name, text = "", attributes = []):
    
        if text or attributes:
            self.begin(name, attributes)
            if text:
                self.html(text)
            self.end()
        else:
            self.f.write("<%s />" % name)
            if not self.in_pre:
                self.f.write("\n")
    
    def write_indent(self, indent):
    
        i = 0
        while i < indent:
            self.f.write('<span class="indent-odd">')
            self.f.write("    ")
            self.f.write('</span>')
            i += 4
            if i >= indent:
                break
            
            self.f.write('<span class="indent-even">')
            self.f.write("    ")
            self.f.write('</span>')
            i += 4
    
    def write_node(self, node, lookup, pending_text = (), statement = False):
    
        if node in lookup:
        
            # This node is associated with a list of instructions.
            if self.pending:
            
                # Close the current table cell, write any pending instructions
                # to a new table cell, then start a new row.
                self.end() # pre
                self.end() # td
                
                self.begin("td", ['class="code"'])
                notes = self.write_instructions(self.pending)
                self.end() # td
                
                self.begin("td")
                self.write_notes(notes)
                self.end() # td
                self.end() # tr
                
                self.begin("tr")
                self.begin("td")
                self.begin("pre", ['class="listing"'])
            
            self.pending = lookup[node]
        
        if pending_text:
            self.write_indent(self.indent - 4)
            self.c = 0
            self.ele("span", pending_text[0], pending_text[1])
            self.html("\n")
        
        if statement:
            self.write_indent(self.indent)
        
        #self.f.write("<!-- %s -->\n" % node.__class__.__name__)
        
        if isinstance(node, ast.Add):
            self.html(" + ")
        
        elif isinstance(node, ast.And):
            self.ele("span", " and ", ['class="keyword"'])
        
        elif isinstance(node, ast.Assign):
        
            self.c = 0
            for child in node.targets:
                self.write_node(child, lookup)
                self.html(" = ")
            
            self.write_node(node.value, lookup)
        
        elif isinstance(node, ast.Attribute):
            self.write_node(node.value, lookup)
            self.ele("span", ".")
            self.ele("span", node.attr, ['class="attribute"'])
        
        elif isinstance(node, ast.AugAssign):
        
            self.c = 0
            self.write_node(node.target, lookup)
            self.html(" " + self.augmented[node.op.__class__] + " ")
            self.write_node(node.value, lookup)
        
        elif isinstance(node, ast.BinOp):
        
            self.write_node(node.left, lookup)
            self.write_node(node.op, lookup)
            self.write_node(node.right, lookup)
        
        elif isinstance(node, ast.BitAnd):
            self.html(" & ")
        
        elif isinstance(node, ast.BitOr):
            self.html(" | ")
        
        elif isinstance(node, ast.BitXor):
            self.html(" ^ ")
        
        elif isinstance(node, ast.BoolOp):
        
            for i, child in enumerate(node.values):
                self.write_node(child, lookup)
                if i < len(node.values) - 1:
                    self.write_node(node.op, lookup)
        
        elif isinstance(node, ast.Break):
            self.c = 0
            self.ele("span", "break", ['class="keyword"'])
            self.html("\n")
        
        elif isinstance(node, ast.Call):
            self.write_node(node.func, lookup)
            self.html("(")
            for i, arg in enumerate(node.args):
                self.write_node(arg, lookup)
                if i < len(node.args) - 1:
                    self.html(", ")
            self.html(")")
        
        elif isinstance(node, ast.Compare):
        
            self.write_node(node.left, lookup)
            for op, expr in zip(node.ops, node.comparators):
                self.write_node(op, lookup)
                self.write_node(expr, lookup)
        
        elif isinstance(node, ast.Continue):
            self.c = 0
            self.ele("span", "continue", ['class="keyword"'])
            self.html("\n")
        
        elif isinstance(node, ast.Dict):
        
            self.html("{")
            for i, (key, value) in enumerate(zip(node.keys, node.values)):
                self.write_node(key, lookup)
                self.html(": ")
                self.write_node(value, lookup)
                if i < len(node.keys) - 1:
                    self.html(", ")
            self.html("}")
        
        elif isinstance(node, ast.Div):
            self.html(" / ")
        
        elif isinstance(node, ast.Eq):
            self.html(" == ")
        
        elif isinstance(node, ast.Expr):
            self.write_node(node.value, lookup)
            self.html("\n")
        
        elif isinstance(node, ast.For):
            self.c = 0
            self.ele("span", "for ", ['class="keyword"'])
            self.write_node(node.target, lookup)
            self.ele("span", " in ", ['class="keyword"'])
            self.write_node(node.iter, lookup)
            self.html(":\n")
            self.indent += 4
            self.write_nodes(node.body, lookup)
            self.indent -= 4
            
            if node.orelse:
                self.indent += 4
                self.write_node(node.orelse[0], lookup,
                    pending_text = ("else:", ['class="keyword"']))
                self.write_nodes(node.orelse[1:], lookup)
                self.indent -= 4
        
        elif isinstance(node, ast.Gt):
            self.html(" > ")
        
        elif isinstance(node, ast.GtE):
            self.html(" >= ")
        
        elif isinstance(node, ast.If):
        
            self.c = 0
            next_node = node
            first = True
            
            while next_node:
            
                if first:
                    self.ele("span", "if ", ['class="keyword"'])
                    first = False
                else:
                    self.write_indent(self.indent)
                    self.c = 0
                    self.ele("span", "elif ", ['class="keyword"'])
                
                self.write_node(next_node.test, lookup)
                self.html(":\n")
                self.indent += 4
                self.write_nodes(next_node.body, lookup)
                self.indent -= 4
                
                if next_node.orelse:
                    if isinstance(next_node.orelse[0], ast.If):
                        next_node = next_node.orelse[0]
                    else:
                        self.indent += 4
                        self.write_node(node.orelse[0], lookup,
                            pending_text = ("else:", ['class="keyword"']),
                            statement = True)
                        self.write_nodes(node.orelse[1:], lookup)
                        self.indent -= 4
                        break
                else:
                    break
        
        elif isinstance(node, ast.In):
            self.ele("span", " in ", ['class="keyword"'])
        
        elif isinstance(node, ast.Index):
            self.write_node(node.value, lookup)
        
        elif isinstance(node, ast.List):
        
            self.html("[")
            for i, child in enumerate(node.elts):
                self.write_node(child, lookup)
                if i < len(node.elts) - 1:
                    self.html(", ")
            self.html("]")
        
        elif isinstance(node, ast.LShift):
            self.html(" << ")
        
        elif isinstance(node, ast.Lt):
            self.html(" < ")
        
        elif isinstance(node, ast.LtE):
            self.html(" <= ")
        
        elif isinstance(node, ast.Mod):
            self.html(" % ")
        
        elif isinstance(node, ast.Mult):
            self.html(" * ")
        
        elif isinstance(node, ast.Name):
            self.ele("span", node.id, ['class="name"'])
        
        elif isinstance(node, ast.Not):
            self.ele("span", "not ", ['class="keyword"'])
        
        elif isinstance(node, ast.NotEq):
            self.html(" != ")
        
        elif isinstance(node, ast.Num):
            self.ele("span", repr(node.n), ['class="value"'])
        
        elif isinstance(node, ast.Or):
            self.ele("span", " or ", ['class="keyword"'])
        
        elif isinstance(node, ast.Pass):
            self.c = 0
            self.ele("span", "pass", ['class="keyword"'])
            self.html("\n")
        
        elif isinstance(node, ast.Raise):
            self.c = 0
            self.ele("span", "raise ", ['class="keyword"'])
            self.write_node(node.type, lookup)
        
        elif isinstance(node, ast.Return):
            self.c = 0
            self.ele("span", "return ", ['class="keyword"'])
            self.write_node(node.value, lookup)
        
        elif isinstance(node, ast.RShift):
            self.html(" >> ")
        
        elif isinstance(node, ast.Set):
            self.html("{")
            for i, child in enumerate(node.elts):
                self.write_node(child, lookup)
                if i < len(node.elts) - 1:
                    self.html(", ")
            self.html("}")
            
        elif isinstance(node, ast.Slice):
            self.html("[")
            self.write_node(node.lower, lookup)
            self.html(":")
            self.write_node(node.upper, lookup)
            self.html(":")
            self.write_node(node.step, lookup)
            self.html("]")
        
        elif isinstance(node, ast.Str):
            self.ele("span", repr(node.s), ['class="string"'])
        
        elif isinstance(node, ast.Sub):
            self.html(" - ")
        
        elif isinstance(node, ast.Subscript):
            self.write_node(node.value, lookup)
            self.html("[")
            self.write_node(node.slice, lookup)
            self.html("]")
        
        elif isinstance(node, ast.TryExcept):
            self.c = 0
            self.ele("span", "try", ['class="keyword"'])
            self.html(":\n")
            self.indent += 4
            self.write_nodes(node.body, lookup)
            self.indent -= 4
            
            for handler in node.handlers:
            
                self.write_indent(self.indent)
                self.c = 0
                self.ele("span", "except", ['class="keyword"'])
                if handler.type:
                    self.html(" ")
                    self.write_node(handler.type, lookup)
                    if handler.name:
                        self.html(", ")
                if handler.name:
                    self.write_node(handler.name, lookup)
                self.html(":\n")
                
                self.indent += 4
                self.write_nodes(handler.body, lookup)
                self.indent -= 4
            
            if node.orelse:
                self.indent += 4
                self.write_node(node.orelse[0], lookup,
                    pending_text = ("else:", ['class="keyword"']),
                    statement = True)
                self.write_nodes(node.orelse[1:], lookup)
                self.indent -= 4
        
        elif isinstance(node, ast.Tuple):
            self.html("(")
            for i, child in enumerate(node.elts):
                self.write_node(child, lookup)
                if i < len(node.elts) - 1:
                    self.html(", ")
            self.html(")")
            
        elif isinstance(node, ast.UAdd):
            self.html("+")
        
        elif isinstance(node, ast.UnaryOp):
            self.write_node(node.op, lookup)
            self.html(" ")
            self.write_node(node.operand, lookup)
        
        elif isinstance(node, ast.USub):
            self.html("-")
        
        elif isinstance(node, ast.While):
        
            self.c = 0
            self.ele("span", "while ", ['class="keyword"'])
            self.write_node(node.test, lookup)
            self.html(":\n")
            self.indent += 4
            self.write_nodes(node.body, lookup)
            self.indent -= 4
            
            if node.orelse:
                self.indent += 4
                self.write_node(node.orelse[0], lookup,
                    pending_text = ("else:", ['class="keyword"']),
                    statement = True)
                self.write_nodes(node.orelse[1:], lookup)
                self.indent -= 4
        
        elif node is None:
            pass
        else:
            print "Unhandled node type:", node.__class__.__name__
        
        return True
    
    def write_nodes(self, nodes, lookup):
    
        for child in nodes:
            self.write_node(child, lookup, statement = True)
            self.html("\n")
    
    def write_vars(self, variables):
    
        l = len(variables)
        
        for i, arg in enumerate(variables):
            self.html("v%i: " % arg.n)
            if arg.temporary():
                self.ele("em", "[temporary]")
            else:
                self.ele("strong", arg.name)
            
            self.html(": ")
            self.write_type(arg.type_)
            
            if i < l - 1:
                self.ele("br")
    
    def write_type(self, type_):
    
        if isinstance(type_, TypeDescriptor):
            self.html("%s" % type_)
        elif isinstance(type_, Null):
            self.html("null")
        elif isinstance(type_, ArrayContainer):
            self.html("%s" % type_)
        elif isinstance(type_, TemplateClass):
            self.html("%s" % type_)
        else:
            self.html("%s" % type_.full_name())
    
    def write_instructions(self, instructions):
    
        notes = []
        
        for instruction in instructions:
        
            current_note = ""
            
            for note in instruction.notes:
            
                current_note += " "
                
                if "=" in note:
                    left, right = note.split("=")
                    current_note += self._html(left) + "="
                    
                    if right.startswith("("):
                        current_note += "<em>" + self._html(right) + "</em>"
                    else:
                        pieces = right.split()
                        current_note += "<b>" + self._html(pieces.pop(0)) + "</b> "
                        current_note += " ".join(map(self._html, pieces))
                else:
                    current_note += self._html(note)
            
            if isinstance(instruction, format_35c):
                offset = instruction.args[-1].value
                method = self.dex_objects.methods[offset]
                method_str = method.class_type.value + "." + str(method.name)
                
                s = self.format % (self.address, instruction)
                self.write_instruction_string(s)
                current_note += " @%i=<em>%s</em>" % (offset, self._html(method_str))
            
            elif isinstance(instruction, format_3rc) and \
                not isinstance(instruction, filled_new_array_range):
                offset = instruction.args[1].value
                method = self.dex_objects.methods[offset]
                method_str = method.class_type.value + "." + str(method.name)
                
                s = self.format % (self.address, instruction)
                self.write_instruction_string(s)
                current_note += " @%i=<em>%s</em>" % (offset, self._html(method_str))
            
            elif isinstance(instruction, const_string):
                offset = instruction.args[-1].value
                string = self.dex_objects.strings[offset]
                s = self.format % (self.address, instruction)
                self.write_instruction_string(s)
                current_note += " @%i=<em>%s</em>" % (offset, self._html(repr(string)))
            
            elif isinstance(instruction, new_instance) or \
                isinstance(instruction, check_cast):
                offset = instruction.args[-1].value
                type_ = self.dex_objects.types[offset]
                type_str = type_.value
                s = self.format % (self.address, instruction)
                self.write_instruction_string(s)
                current_note += " @%i=<em>%s</em>" % (offset, self._html(type_str))
            
            elif isinstance(instruction, format_22c) and \
                not isinstance(instruction, new_array):
            
                # Resolve fields only for relevant format_22c instructions.
                offset = instruction.args[-1].value
                field = self.dex_objects.fields[offset]
                field_str = str(field.name)
                s = self.format % (self.address, instruction)
                self.write_instruction_string(s)
                current_note += " @%i=<em>%s</em>" % (offset, self._html(field_str))
            
            elif isinstance(instruction, format_10t) or \
                isinstance(instruction, format_20t) or \
                isinstance(instruction, format_30t):
            
                s = self.format % (self.address, instruction)
                self.write_instruction_string(s)
                current_note += "<b>&rarr;</b>" + self._html(" " + self.address_format % (
                    self.address + instruction.args[0].value))
            
            else:
                s = self.format % (self.address, instruction)
                self.write_instruction_string(s)
            
            if current_note:
                notes.append(current_note)
            else:
                notes.append("&nbsp;")
            
            self.address += instruction.size
            self.ele("br")
        
        return notes
    
    def write_instruction_string(self, s):
    
        pieces = s.split()
        self.html(pieces[0])
        self.ele("b", pieces[1])
        self.html(" ".join(pieces[2:]))
    
    def write_notes(self, notes):
    
        for note in notes:
            self.raw(note)
            self.ele("br")


class Span:

    def __init__(self):
    
        self.begin = None
        self.end = None
        self.instructions = []


def write_listing(listing_file_name, sources, dex_objects):

    f = codecs.open(listing_file_name, "w", "utf-8")
    w = HTMLWriter(f, dex_objects)
    
    w.begin("html")
    w.begin("head")
    w.ele("meta", "", ['http-equiv="Content-Type"', 'content="text/html; charset=utf-8"'])
    w.begin("style", ["type=text/css"])
    w.html('td.code { text-align: left; vertical-align: top; padding-left: 1em }\n')
    w.html('.attribute { color: #777777; font-weight: bold }\n')
    w.html('.comment { color: darkgreen }\n')
    w.html('.decorator { color: #555500 }\n')
    w.html('.function { color: #3333aa; font-weight: bold }\n')
    w.html('.listing { font-size: 1em; margin: 0; border: none }\n')
    w.html('.listings th { text-align: center; vertical-align: top; border: 1px solid #e0e0e0 }\n')
    w.html('.odd { background-color: #e0e0e0 }\n')
    w.html('.even { background-color: #efefef }\n')
    w.html('.listings td { padding-left: 0.5em; border: 1px solid #e0e0e0; '
           'vertical-align: top }\n')
    w.html('.keyword { color: red }\n')
    w.html('.method-heading { font-weight: bold; font-size: 1.2em }\n')
    w.html('.name { color: #337777; font-weight: bold }\n')
    w.html('.string { color: blue }\n')
    w.html('.value { color: #777733; font-weight: bold }\n')
    w.html('.indent-odd { background-color: #e0e0e0 }\n')
    w.html('.indent-even { background-color: #efefef }\n')
    w.end() # style
    w.end() # head
    
    w.begin("body")
    
    w.begin("ul")
    previous_class_name = None
    
    for method, instructions in sources:
    
        class_name = method.class_type.value
        
        if class_name != previous_class_name:
        
            if previous_class_name:
                w.end() # ul
            
            w.ele("li", class_name)
            w.begin("ul")
            previous_class_name = class_name
        
        w.begin("li")
        w.ele("a", str(method.name), [
              'href="#%s-%s"' % (class_name, w._html(str(method.name)))])
        w.end() # li
    
    if previous_class_name:
        w.end() # ul
    
    w.end() # ul
    
    for method, instructions in sources:
    
        if not instructions:
            continue
        
        w.ele("h3", "%s %s" % (method.class_type.value, method.name),
              ['id="%s-%s"' % (method.class_type.value, w._html(str(method.name)))])
        w.begin("p")
        w.ele("strong", "Registers:")
        w.ele("br")
        w.ele("em", "Input:")
        w.html(str(method.code.ins_size) + "\n")
        w.ele("em", "Output:")
        w.html(str(method.code.outs_size) + "\n")
        w.ele("em", "Total:")
        w.html(str(method.code.registers_size))
        w.end() # p
        
        if dex_objects.method_vars[method.code]:
            w.begin("p")
            w.ele("strong", "Variables:")
            w.ele("br")
            variables = {}
            for variable in dex_objects.method_vars[method.code]:
                variables[(variable.n, repr(variable.type_))] = variable
            variables = variables.items()
            variables.sort()
            w.write_vars(map(lambda ((n, t), var): var, variables))
            w.end() # p
        
        w.begin("p")
        w.ele("strong", "Arguments:")
        w.ele("br")
        w.write_vars(dex_objects.method_args[method.code])
        w.end() # p
        
        w.begin("p")
        w.ele("strong", "Return type:")
        w.ele("br")
        w.write_type(method.proto.return_type)
        w.end() # p
        
        w.begin("table", ['class="listings"', 'width="100%"', 'cellspacing="0"'])
        w.begin("tr")
        w.begin("th", ['class="odd"'])
        w.html("Source code")
        w.end() # th
        w.begin("th", ['class="even"'])
        w.html("Assembly")
        w.end() # th
        w.begin("th", ['class="odd"'])
        w.html("Notes")
        w.end() # th
        w.end() # tr
        
        # Create a dictionary to map AST nodes to lists of more-or-less
        # corresponding instructions.
        lookup = {}
        current_node = None
        current = []
        pending = []
        
        for instruction in instructions:
        
            if isinstance(instruction, Source):
            
                if current:
                    if current_node:
                        lookup[current_node] = current
                        current_node = instruction.node
                        current = []
                    else:
                        pending = current
                else:
                    current_node = instruction.node
            else:
                current.append(instruction)
        
        if current:
            if current_node:
                lookup[current_node] = current
            else:
                pending = current
        
        w.pending = pending
        w.address = 0
        max_field = "%x" % sum(map(lambda i: i.size, instructions))
        w.format = "%%0%ix: %%s" % len(max_field)
        w.address_format = "%%0%ix" % len(max_field)
        
        w.begin("tr")
        w.begin("td")
        w.begin("pre", ['class="listing"'])
        w.write_nodes(dex_objects.method_tree[method.code], lookup)
        w.end() # pre
        w.end() # td
        
        w.begin("td", ['class="code"'])
        notes = w.write_instructions(w.pending)
        w.end() # td
        
        w.begin("td")
        w.write_notes(notes)
        w.end() # td
        w.end() # tr
        w.end() # table
        w.html("\n")
    
    w.end() # body
    w.end() # html
    
    f.close()
```



## Files

* [\_\_init\_\_.py](../\_\_init\_\_.py)
* [arrays.py](../arrays.py)
* [builtins.py](../builtins.py)
* [common.py](../common.py)
* [macros.py](../macros.py)
* [methods.py](../methods.py)
* [utils.py](../utils.py)
* [visitor.py](../visitor.py)
* [writer.py](../writer.py)
