
```python
import ast

from arrays import array_or_container
from common import create_class_name, create_dex_type, find_object, MethodCall
from macros import *
import visitor
```

## Builtins

Like Python, the Serpentine language provides built-in functions, such as
`int` and `float` to make it easy to convert between different fundamental data
types, a `len` function to obtain the number of elements in a container, and an
`array` function to construct arrays of specific types.

The `Builtins` class is inherited by the main `Compiler` class in order to
provide methods for processing nodes of the abstract syntax tree for built-in
function calls. These methods perform parameter checks and generate code for
run-time casts.

The recognised built-in functions are defined later in the class and their
names are checked by the compiler when processing function calls. Because the
underlying object model used by the virtual machine only provides support for
methods, not top-level functions, there is no danger of a built-in function
clashing with a method on a class.

Primitive and object-based types are handled differently. See
[Primitive types](#primitive-types) for more information.


```python
class Builtins:

    # Define a method to handle built-in functions that Python programs often use
    # and additional helper functions.
    
    def process_builtin(self, node, name, context, scope):
    
        # The check for a handler occurred in the caller.
        handler = self.builtin_functions[name]
        
        return handler(self, node, name, context, scope)
    
    def process_library_function(self, name, context, scope):
    
        # The check for a handler occurred in the caller.
        class_name, method_name = self.builtin_indirected[name]
        
        # Find the helper class. Since this is imported as part of the builtins
        # library module, this should always succeed.
        library_class = self.dex_objects.builtins.find_object(class_name)
        
        return MethodCall(library_class, method_name)
    
    def check_args(self, name, args, node, scope, minimum = 0, maximum = None):
    
        if len(node.args) < minimum or (maximum != None and len(node.args) > maximum):
        
            if minimum != maximum:
                raise TypeError, (
                    "Incorrect number of arguments passed to '%s' at line %i of file %s. "
                    "Expected between %i and %i arguments."
                    ) % (name, node.lineno, scope.file_name(), minimum, maximum)
            else:
                raise TypeError, (
                    "Incorrect number of arguments passed to '%s' at line %i of file %s. "
                    "Expected %i arguments."
                    ) % (
                    name, node.lineno, scope.file_name(), minimum)
    
    def find_argument_object(self, node, name, obj_arg, context, scope):
    
        obj = self.process_node(obj_arg, context, scope)
        
        if obj is None:
            raise TypeError("Object %s passed to '%s' as first argument was not found at line %i of file %s." % (
                repr(obj_arg), name, node.lineno, scope.file_name()))
        
        if not isinstance(obj, Variable):
            raise TypeError("Unhandled object %s in built-in '%s' at line %i of file %s." % (
                repr(obj), name, node.lineno, scope.file_name()))
        
        return obj
```

### Object types

Casts between object types are performed by sequences of instructions which
usually involve just a single specialised instruction for all casts. This
takes advantage of the virtual machine's built-in support for object types.

The `Move` and `CheckCast` macro instructions are defined in the
[macros](macros.md) module.


```python
    def process_CAST(self, node, name, context, scope):
    
        # Arguments: <name>, <type>
        self.check_args(name, node.args, node, scope, minimum = 2, maximum = 2)
        
        obj_arg, type_node = node.args
        
        # Find the object to be cast.
        obj = self.find_argument_object(node, name, obj_arg, context, scope)
        
        # Find the type to cast to.
        type_obj = self.process_node(type_node, context, scope)
        
        if type_obj is None:
            raise TypeError, "Unhandled object type in '%s' at line %i." % (
                name, node.lineno)
        
        # Resolve the type of the type object.
        type_ = create_class_name(type_obj)
        
        # Create a temporary variable based on the object, but with the type
        # of the type object, first freeing the original variable if temporary.
        if obj.temporary():
            context.free()
        
        temporary = context.allocate(type_=type_obj)
        if temporary != obj:
            context.append(Move(temporary, obj))
        context.append(CheckCast(temporary, type_))
        self.dex_objects.types.add(type_)
        return temporary
```

### Primitive types

Casts between primitive types are performed by sequences of instructions
that are different for each pair of types.

The following method is used to handle all primitive type casts. It begins
by checking the types of the arguments passed to the built-in function,
then generates code to handle the constant or variable containing the
value to be cast. Constants can be substituted for appropriately-typed
constants at compile-time.

The `cast_primitive` method, defined in the [Methods](methods.md) class,
is used to generate macro instructions for performing the type cast at
run-time.


```python
    def process_primitive_type_cast(self, node, name, context, scope):
    
        # Arguments: <double|int|float|long|short|char|byte>
        self.check_args(name, node.args, node, scope, minimum = 1, maximum = 1)
        
        obj_arg = node.args[0]
        
        type_ = type_name_to_type_map[name]
        
        if isinstance(obj_arg, ast.Num):
            # If the object is a constant then generate a temporary variable to
            # hold its casted value.
            temporary = context.allocate(type_=type_)
            context.append(LoadConstant(temporary, obj_arg.n, type_))
            return temporary
        else:
            # Find the object to be cast.
            obj = self.find_argument_object(node, name, obj_arg, context, scope)
        
        if type_ == obj.type_:
            # Return the original object itself if the types are the same.
            return obj
        
        # If the variable holding the original value is temporary then free it.
        if obj.temporary():
            context.free()
        
        temporary = context.allocate(type_=type_)
        self.cast_primitive(obj, temporary, node, context, scope)
        
        return temporary
```

### Lengths of containers

The `len` function is used to count the number of elements in containers
and arrays. This method checks the container type and generates appropriate
code for that type.


```python
    def process_len(self, node, name, context, scope):
    
        # Arguments: <array|container>
        self.check_args(name, node.args, node, scope, minimum = 1, maximum = 1)
        
        obj_arg = node.args[0]
        
        # Find the object to be counted.
        obj = self.find_argument_object(node, name, obj_arg, context, scope)
        
        is_array, container_type, item_types = array_or_container(obj, node)
        
        if not item_types:
            if isinstance(obj.type_, visitor.ClassVisitor):
                # Handle regular classes as a special case.
                is_array = False
                container_type = obj.type_
                item_types = None
            else:
                raise TypeError, "Expected array or container instead of type %s at line %i." % (
                    repr(obj.type_), node.lineno)
        
        # Check whether the array or object is valid and return zero if not.
        
        is_zero_target = Target("is-zero-%x" % id(node))
        end_target = Target("end-%x" % id(node))
        
        # Perform a comparison. This will copy the operands to temporary
        # variables that are suitable for comparison operations.
        ins = CompareZeroMacro(if_eqz, is_zero_target, obj)
        context.append(ins)
        
        # Free the original object if it is temporary so that the count replaces it.
        if obj.temporary():
            context.free()
        
        count_var = context.allocate(type_=dInt)
        
        if is_array:
            ins = ArrayLength(count_var, obj)
            context.append(ins)
            context.append(Goto(end_target))
        
        else:
            self._len(container_type, obj, count_var, node, context, scope)
            context.append(Goto(end_target))
        
        # Branch to here if the array object was null.
        context.append(is_zero_target)
        context.append(LoadConstant(count_var, 0))
        
        # Both branches end here.
        context.append(end_target)
        
        return count_var
    
    def _len(self, container_type, container_obj, result_obj, node, context, scope):
    
        try:
            # Look for a suitable size method in the object.
            callee = MethodCall(container_obj, "size", [])
            chosen = self.find_matching_method(callee, scope, node)
        except NameLookupError:
            # Look for a suitable length method in the object.
            callee = MethodCall(container_obj, "length", [])
            chosen = self.find_matching_method(callee, scope, node)
        
        # Call the method to obtain the length.
        self.add_method_call(container_type, callee, chosen, node, context, scope)
        
        context.append(MoveResult(result_obj))
```

### Array construction

The `array` function is used to create arrays with default values, create
new arrays from existing sequences of values, and convert other containers
to arrays. The use of the function to create empty arrays is useful for
allocating arrays to pass to Java and Android API methods.

There are two variants of this function, both implemented in this method.
The first accepts a container literal or variable referring to a container:

    array(listObject)
    array([1, 2, 3])

The second accepts a primitive type name and a length:

    array(float, 3)

The compiler produces the appropriate code for each variant.


```python
    def process_array(self, node, name, context, scope):
    
        # Arguments: <container> | (<type>, <count>...)
        self.check_args(name, node.args, node, scope, minimum = 1)
        
        if len(node.args) == 1:
        
            # Convert an existing container to an array.
            obj_arg = node.args[0]
            
            # Find the object to be converted.
            obj = self.find_argument_object(node, name, obj_arg, context, scope)
            
            if obj.temporary():
                context.free()
            
            # Try to use Arrays.create to convert the object to an array.
            library_class = self.dex_objects.builtins.find_object("Arrays")
            # Use a temporary variable for the result.
            temporary = self.call_method(library_class, "create", [obj], node,
                                         context, scope)
            return temporary
        
        elif len(node.args) >= 2:
        
            type_obj = None
            
            if isinstance(node.args[0], ast.Name):
            
                # Resolve the type and descriptive name of the item type.
                type_arg = node.args[0]
                
                if type_arg.id in self.primitive_types:
                    type_obj = self.create_value_type(type_arg)
                else:
                    type_obj = find_object(type_arg.id, scope, context,
                                           self.dex_objects)
            
            if type_obj is None:
                raise TypeError("Unhandled object type in '%s' at line %i "
                    "of file %s." % (name, node.lineno, scope.file_name()))
            
            # Create a new array of the specified type with the number of dimensions
            # defined by the number of arguments following the type where each of
            # the following arguments defines the length of each dimension.
            count_args = node.args[1:]
            
            return self.create_array(type_obj, count_args, node, name, context,
                                     scope)
        
        raise SyntaxError("Incorrect number of arguments passed to array "
            "built-in at line %i of file %s." % (node.lineno, scope.file_name()))
```

### Supported functions

The built-in functions supported by the compiler are defined here. These
are checked by the compiler when processing a `Call` node in the abstract
syntax tree.


```python
    # Define a dictionary to map built-in function names to functions.
    
    builtin_functions = {
        "CAST": process_CAST,
        "int": process_primitive_type_cast,
        "float": process_primitive_type_cast,
        "long": process_primitive_type_cast,
        "double": process_primitive_type_cast,
        "short": process_primitive_type_cast,
        "char": process_primitive_type_cast,
        "byte": process_primitive_type_cast,
        "len": process_len,
        "array": process_array
        }
```

Some built-in functions are implemented as static methods in classes in the
`serpentine` module for the Serpentine language itself. Those classes are
imported into the `serpentine.builtins` module so that the references in
the following list are valid.


```python
    # Map certain built-in functions to Serpentine classes and methods.
    
    builtin_indirected = {
        "dict": ("Collections", "dict"),
        "iter": ("Iterators", "iter"),
        "open": ("Files", "open"),
        "range": ("Collections", "range"),
        "str": ("Strings", "str"),
        "zip": ("Collections", "zip")
        }
```

A set of primitive types is defined to help match arguments to the variant
of the `array` function that accepts a type name and a length.


```python
    primitive_types = set(["int", "float", "long", "double", "short", "char", "byte"])
```



## Files

* [\_\_init\_\_.py](../\_\_init\_\_.py)
* [arrays.py](../arrays.py)
* [builtins.py](../builtins.py)
* [common.py](../common.py)
* [macros.py](../macros.py)
* [methods.py](../methods.py)
* [utils.py](../utils.py)
* [visitor.py](../visitor.py)
* [writer.py](../writer.py)
