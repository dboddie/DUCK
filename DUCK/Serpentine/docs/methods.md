
```python
        proposed = variable.type_
        resolved = self.compatible_types(proposed, constraint, add_missing)
        
        # Are types the same?
        if resolved == proposed:
            return variable
        
        # Quickly check for failure (no compatible types).
        if resolved is None:
            return None
        
        if isinstance(proposed, Placeholder) and \
           isinstance(proposed.class_, visitor.ClassVisitor):
        
            # Replace the object's placeholder type with the resolved type and
            # generate code to cast the value to the resolved type.
            variable.set_type(resolved)
            cast_value(variable, resolved, context, self.dex_objects)
            return variable
        
        elif resolved == constraint and is_primitive(resolved):
        
            # For primitive type casts, cast to the result object's type, or
            # allocate a temporary variable to hold the converted value.
            if not result_obj:
                temporary = context.allocate(type_=constraint)
            else:
                temporary = result_obj
            
            self.cast_primitive(variable, temporary, node, context, scope)
            return temporary
        
        return variable
    
    
    def get_array_item(self, iterable_obj, item_var, item_type, index_obj, node,
                       context, scope):
    
        # See the handling code for the Subscript node for an explanation of
        # this Object unwrapping code.
        
        iterable_type = iterable_obj.type_
        
        if iterable_type.wrapped_items:
        
            if isinstance(item_type, TypeDescriptor):
            
                # Find the wrapper used for the specified items and extract an
                # item into a variable with that type.
                wrapper_type = self.get_wrapper_type(item_type)
                wrapper_var = context.allocate(type_=wrapper_type)
                context.append(ArrayGet(wrapper_var, iterable_obj, index_obj))
                cast_value(wrapper_var, wrapper_type, context, self.dex_objects)
                context.free()
                
                # Call the method to convert the wrapped object into a primitive
                # value.
                self.unwrap(wrapper_type, wrapper_var, node, context, scope,
                            result_obj = item_var, exception_type = IndexError)
            else:
                # Cast the item type to the more specialised Object type.
                context.append(ArrayGet(item_var, iterable_obj, index_obj))
                cast_value(item_var, item_type, context, self.dex_objects)
        else:
            context.append(ArrayGet(item_var, iterable_obj, index_obj))
```



## Files

* [\_\_init\_\_.py](../\_\_init\_\_.py)
* [arrays.py](../arrays.py)
* [builtins.py](../builtins.py)
* [common.py](../common.py)
* [macros.py](../macros.py)
* [methods.py](../methods.py)
* [utils.py](../utils.py)
* [visitor.py](../visitor.py)
* [writer.py](../writer.py)
