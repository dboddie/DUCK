
```python
    size = 0
    
    def __init__(self, node):
        self.node = node
    
    def expand(self, dex_objects):
        return []
    
    def used_vars(self):
        return []
```



## Files

* [\_\_init\_\_.py](../\_\_init\_\_.py)
* [arrays.py](../arrays.py)
* [builtins.py](../builtins.py)
* [common.py](../common.py)
* [macros.py](../macros.py)
* [methods.py](../methods.py)
* [utils.py](../utils.py)
* [visitor.py](../visitor.py)
* [writer.py](../writer.py)
