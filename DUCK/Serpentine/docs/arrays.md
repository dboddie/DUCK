
```python
import ast

from Dalvik.dextypes import *

from common import *
from macros import *
import visitor
```

## Arrays

The `arrays` module contains the `Arrays` class that provides methods for
creating arrays. This class is inherited by the `Compiler` class.

It also provides a function for determining whether the type of a variable
is an array or some other container, and a function for obtaining the type and
number of dimensions of an array.


```python
    # Use the convention that the number of nested lists is the number
    # of dimensions if given, otherwise the array is one-dimensional.
    dim = 1
    item_type = value
    while True:
    
        if type(item_type) == list:
        
            item_type = item_type[0]
            if type(item_type) != list:
                break

        elif isinstance(item_type, ast.List):
        
            item_type = item_type.elts[0]
            if not isinstance(item_type, ast.List):
                break
        
        dim += 1
    
    return dim, item_type


def array_or_container(obj, node):

    if isinstance(obj.type_, ArrayContainer):
        is_array = True
        container_type = None
        if obj.type_.dimensions > 1:
            item_types = [ArrayContainer(obj.type_.dimensions - 1, obj.type_.item_type,
                                         obj.type_.dex_type, obj.type_.wrapped_items)]
        else:
            item_types = [obj.type_.item_type]
    
    elif isinstance(obj.type_, TemplateClass):
        is_array = False
        # Use the container class that this template actually uses.
        container_type = obj.type_.container_class
        item_types = obj.type_.item_types_
    
    elif isinstance(obj.type_, visitor.ClassVisitor) and obj.type_.has_item_types():
        is_array = False
        # Use the container class that this template actually uses.
        container_type = obj.type_
        item_types = container_type.item_types()
    
    else:
        is_array = False
        container_type = None
        item_types = []
    
    return is_array, container_type, item_types
```



## Files

* [\_\_init\_\_.py](../\_\_init\_\_.py)
* [arrays.py](../arrays.py)
* [builtins.py](../builtins.py)
* [common.py](../common.py)
* [macros.py](../macros.py)
* [methods.py](../methods.py)
* [utils.py](../utils.py)
* [visitor.py](../visitor.py)
* [writer.py](../writer.py)
