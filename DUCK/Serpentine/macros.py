"""
Copyright (C) 2016 David Boddie <david@boddie.org.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

"""~
# Macros

The compiler defines a set of macros that represent sequences of instructions
for the virtual machine. These provide a level of abstraction, allowing the
compiler to generate code without needing to know too much about the low level
details of the code itself. Macros also offer flexibility in that they can be
expanded differently depending on the circumstances.

The macros implemented in this module are not completely abstract since they
were originally defined with generation of DEX bytecode in mind. As a result,
some of the operations they represent are specialised or map to particular
kinds of instructions performed by the virtual machine. They also manage lower
level implementation details, such as register allocation.
"""

import ast
import struct

from Dalvik.bytecode import *
from Dalvik.dextypes import *

from common import *
import visitor

TRUE = 1
FALSE = 0

class Macro:

    def copy_variable_to_low_registers(self, variable, start, ins, dex_objects):
    
        if variable.n + variable.size() > 16:
            n = start
            ins += Move(Variable(None, n, variable.type_), variable).expand(dex_objects)
            next = start + variable.size()
        else:
            n = variable.n
            next = start
        
        return n, next
    
    def copy_variables_to_low_registers(self, variables, ins, dex_objects):
    
        next = 0
        numbers = []
        
        for variable in variables:
            n, next = self.copy_variable_to_low_registers(variable, next, ins, dex_objects)
            numbers.append(n)
        
        return numbers
    
    def copy_variable_from_low_registers(self, variable, n, ins, dex_objects):
    
        if variable.n + variable.size() > 16:
            ins += Move(variable, Variable(None, n, variable.type_)).expand(dex_objects)
    
    def copy_variables_from_low_registers(self, variables, numbers, ins, dex_objects):
    
        for variable, n in zip(variables, numbers):
            self.copy_variable_from_low_registers(variable, n, ins, dex_objects)
    
    def used_vars(self):
        return []
    
    def annotate(self, ins, dex_objects):
    
        if dex_objects.keep_source:
        
            ins.notes = []
            
            # Filter out non-variables from the variable list.
            # Sort the variables by number.
            variables = map(lambda v: (v.n, v), self.used_vars())
            
            used = set()
            for n, v in variables:
                if n not in used:
                    name = v.name
                    if name == None:
                        ins.notes.append("v%i=(%s)" % (v.n, v.type_))
                    else:
                        ins.notes.append("v%i=%s (%s)" % (v.n, name, v.type_))
                    used.add(n)
        
        return ins


class ExtendedMacro(Macro):

    def extra_vars(self, left, right = None):
    
        # If the contents of the argument variables need to be copied into low
        # registers for the comparison then record the use of those registers.
        extra = []
        
        if left.n + left.size() > 16:
            extra.append(Variable(None, 0, left.type_))
        
        if right is not None:
            if right.n + right.size() > 16:
                extra.append(Variable(None, 2, right.type_))
        
        return extra


class InvokeMacro(Macro):

    def __init__(self, method, arguments):
    
        self.method = method
        self.args = arguments
        self.extras = []
    
    def __repr__(self):
        return "%s(%s, %s)" % (self.__class__.__name__, repr(self.method),
            repr(self.args))
    
    def used_vars(self):
        return self.args
    
    def needs_range(self):
    
        total = 0
        for arg in self.args:
            total += arg.size()
        
        if total > 5:
            return True
        
        for arg in self.args:
            if arg.n + arg.size() > 16:
                return True
        
        return False
    
    def allocate_extra_registers(self, start):
    
        # Allocate variables in ascending register order from the start register.
        self.extras = []
        i = start
        
        for arg in self.args:
            self.extras.append(Variable(None, i, arg.type_))
            i += arg.size()
        
        return i
    
    def expand(self, dex_objects):
    
        method_index = dex_objects.methods.index(self.method)
        
        # The arguments are variables that need to be converted to registers.
        
        if self.extras:
        
            insts = []
            
            # The arguments must be a contiguous block of registers.
            
            for arg, extra in zip(self.args, self.extras):
                insts += Move(extra, arg).expand(dex_objects)
            
            registers = self.extras[-1].n + self.extras[-1].size() - self.extras[0].n
            ins = self.RangeInstruction(Count8(registers), Index16(method_index),
                                        v(self.extras[0].n, 16))
            insts.append(self.annotate(ins, dex_objects))
            return insts
        else:
            arguments = []
            for arg in self.args:
                for n in range(arg.n, arg.n + arg.size()):
                    arguments.append(v(n, 4))
            
            args = [Count4(len(arguments)), Index16(method_index)] + arguments
            ins = self.Instruction(*args)
            self.annotate(ins, dex_objects)
            return [ins]

class InvokeDirect(InvokeMacro):

    Instruction = invoke_direct
    RangeInstruction = invoke_direct_range
    size = Instruction.size

class InvokeInterface(InvokeMacro):

    Instruction = invoke_interface
    RangeInstruction = invoke_interface_range
    size = Instruction.size

class InvokeSuper(InvokeMacro):

    Instruction = invoke_super
    RangeInstruction = invoke_super_range
    size = Instruction.size

class InvokeStatic(InvokeMacro):

    Instruction = invoke_static
    RangeInstruction = invoke_static_range
    size = Instruction.size

class InvokeVirtual(InvokeMacro):

    Instruction = invoke_virtual
    RangeInstruction = invoke_virtual_range
    size = Instruction.size

class LoadConstant(Macro):

    def __init__(self, variable, value, dex_type = None):
    
        self.variable = variable
        self.value = value
        
        # Allow the dex type to be either explicitly specified or determined
        # from the Python value.
        if dex_type is None:
            self.dex_type, self.value = value_to_type(value)
        else:
            self.dex_type = dex_type
    
    def __repr__(self):
        return "LoadConstant(%s, %s)" % (repr(self.variable), repr(self.value))
    
    def used_vars(self):
        return [self.variable]
    
    def expand(self, dex_objects):
    
        # Generate bytecode according to the dex type for cases where the
        # Python type is not specific enough or when the dex type is specified.
        
        ins = None
        
        if isinstance(self.dex_type, Int):
        
            ins = const(v(self.variable.n, 8), self.value)
        
        elif isinstance(self.dex_type, Float):
        
            value = struct.unpack("<I", struct.pack("<f", self.value))[0]
            ins = const(v(self.variable.n, 8), value)
        
        elif isinstance(self.dex_type, Double):
            # Extract the unsigned integer value that corresponds to the
            # bitwise representation of the signed floating point number.
            ### struct.unpack("<L") appears to expect an incorrect string size.
            value = 0L
            shift = 0
            for c in struct.pack("<d", self.value):
                value = value | (ord(c) << shift)
                shift += 8
            ins = const_wide(v(self.variable.n, 8), value)
        
        elif isinstance(self.dex_type, Long):
        
            value = struct.unpack("<Q", struct.pack("<q", self.value))[0]
            ins = const_wide(v(self.variable.n, 8), value)
        
        elif isinstance(self.dex_type, Short):
        
            if -32768 <= self.value <= 32767:
                value = struct.unpack("<I", struct.pack("<i", self.value))[0]
                ins = const(v(self.variable.n, 8), value)
            else:
                raise ValueError("Constant value %s out of bounds for type '%s'." % (self.value, self.dex_type))
        
        elif isinstance(self.dex_type, Byte):
        
            if -128 <= self.value <= 127:
                value = struct.unpack("<I", struct.pack("<i", self.value))[0]
                ins = const(v(self.variable.n, 8), value)
            else:
                raise ValueError("Constant value %s out of bounds for type '%s'." % (self.value, self.dex_type))
        
        elif isinstance(self.dex_type, Char):
        
            if 0 <= self.value <= 65535:
                value = struct.unpack("<I", struct.pack("<H", self.value) + "\x00\x00")[0]
                ins = const(v(self.variable.n, 8), value)
            else:
                raise ValueError("Constant value %s out of bounds for type '%s'." % (self.value, self.dex_type))
        
        if ins != None:
            self.annotate(ins, dex_objects)
            return [ins]
        
        # Otherwise, handle Python types.
        
        if self.value is None:
        
            if 0 <= self.variable.n < 16:
                ins = const_4(v(self.variable.n, 4), 0)
            else:
                ins = const_16(v(self.variable.n, 8), 0)
        
        if type(self.value) == int:
            if 0 <= self.variable.n < 16 and -8 <= self.value <= 7:
                ins = const_4(v(self.variable.n, 4), self.value)
            elif 0 <= self.variable.n < 256 and -32768 <= self.value <= 32767:
                ins = const_16(v(self.variable.n, 8), self.value)
            elif self.value & 0xffff0000 == self.value:
                ins = const_high_16(v(self.variable.n, 8), self.value >> 16)
            else:
                ins = const(v(self.variable.n, 8), self.value)
        
        elif type(self.value) == str or type(self.value) == unicode:
            ins = const_string(v(self.variable.n, 8), dex_objects.strings.index(self.value))
        
        elif type(self.value) == bool:
            value = {False: FALSE, True: TRUE}[self.value]
            if 0 <= self.variable.n < 16:
                ins = const_4(v(self.variable.n, 4), value)
            else:
                ins = const_16(v(self.variable.n, 8), value)
        
        if ins == None:
            raise ValueError("Failed to encode constant value for type '%s'." % type(self.value))
        
        self.annotate(ins, dex_objects)
        return [ins]


class Move(Macro):

    def __init__(self, dest_variable, src_variable):
    
        self.dest_variable = dest_variable
        self.src_variable = src_variable
    
    def __repr__(self):
    
        return "Move(%s, %s)" % (repr(self.dest_variable), repr(self.src_variable))
    
    def used_vars(self):
        return [self.dest_variable, self.src_variable]
    
    def expand(self, dex_objects):
    
        if self.src_variable.n == self.dest_variable.n:
            return []
        
        elif isinstance(self.src_variable.type_, visitor.ClassVisitor) or \
           isinstance(self.src_variable.type_, TemplateClass) or \
           isinstance(self.src_variable.type_, ArrayContainer):
        
            # Handle objects differently to ordinary values.
            if self.dest_variable.n < 16 and self.src_variable.n < 16:
                ins = move_object(v(self.dest_variable.n, 4),
                                  v(self.src_variable.n, 4))
            else:
                ins = move_object_from16(v(self.dest_variable.n, 8),
                                         v(self.src_variable.n, 16))
        
        elif self.dest_variable.n < 16 and self.src_variable.n < 16:
            
            if self.dest_variable.size() == 2:
                ins = move_wide(v(self.dest_variable.n, 4),
                                v(self.src_variable.n, 4))
            else:
                ins = move(v(self.dest_variable.n, 4),
                           v(self.src_variable.n, 4))
        else:
            if self.dest_variable.size() == 2:
                ins = move_wide_from16(v(self.dest_variable.n, 8),
                                       v(self.src_variable.n, 16))
            else:
                ins = move_from16(v(self.dest_variable.n, 8),
                                  v(self.src_variable.n, 16))
        
        self.annotate(ins, dex_objects)
        return [ins]

class MoveResult(Macro):

    def __init__(self, variable):
        self.variable = variable
    
    def used_vars(self):
        return [self.variable]
    
    def expand(self, dex_objects):
    
        if isinstance(self.variable.type_, visitor.ClassVisitor) or \
           isinstance(self.variable.type_, TemplateClass):
            ins = move_result_object(v(self.variable.n, 8))
        elif isinstance(self.variable.type_, ArrayContainer):
            ins = move_result_object(v(self.variable.n, 8))
        elif self.variable.size() == 1:
            ins = move_result(v(self.variable.n, 8))
        else:
            ins = move_result_wide(v(self.variable.n, 8))
        
        self.annotate(ins, dex_objects)
        return [ins]


class MoveException(Macro):

    def __init__(self, variable):
        self.variable = variable
    
    def used_vars(self):
        return [self.variable]
    
    def expand(self, dex_objects):
        ins = move_exception(v(self.variable.n, 8))
        self.annotate(ins, dex_objects)
        return [ins]


class Return(Macro):

    def __init__(self, variable):
        self.variable = variable
    
    def used_vars(self):
        return [self.variable]
    
    def expand(self, dex_objects):
    
        if isinstance(self.variable.type_, visitor.ClassVisitor) or \
           isinstance(self.variable.type_, TemplateClass) or \
           isinstance(self.variable.type_, Null) or \
           isinstance(self.variable.type_, ArrayContainer):
            ins = return_object(v(self.variable.n, 8))
        elif self.variable.size() == 1:
            ins = return_(v(self.variable.n, 8))
        else:
            ins = return_wide(v(self.variable.n, 8))
        
        self.annotate(ins, dex_objects)
        return [ins]

class ReturnVoid(Macro):

    def expand(self, dex_objects):
        return [return_void()]


class OperationMacro(Macro):

    def __init__(self, instruction, result, arguments):
    
        self.instruction = instruction
        self.result = result
        self.args = arguments
    
    def used_vars(self):
        return [self.result] + self.args
    
    def expand(self, dex_objects):
    
        arguments = [v(self.result.n, 8)]
        for arg in self.args:
            arguments.append(v(arg.n, 8))
        
        ins = self.instruction(*arguments)
        self.annotate(ins, dex_objects)
        return [ins]


class ConstantOperationMacro(Macro):

    """Wraps operations that accept a constant as their second operand, such
    as add_int_lit8 dest, src, constant.
    """
    def __init__(self, instruction, result, arguments):
    
        self.instruction = instruction
        self.result = result
        self.args = arguments
    
    def used_vars(self):
        return [self.result] + filter(lambda arg: isinstance(arg, Variable), self.args)
    
    def expand(self, dex_objects):
    
        arguments = [v(self.result.n, 8)]
        for arg in self.args:
            if isinstance(arg, Variable):
                arguments.append(v(arg.n, 8))
            else:
                arguments.append(arg)
        
        ins = self.instruction(*arguments)
        self.annotate(ins, dex_objects)
        return [ins]


class ComparisonMacro(ExtendedMacro):

    def __init__(self, instruction, target, arguments):
    
        self.instruction = instruction
        self.target = target
        self.args = arguments
    
    def __repr__(self):
    
        return "ComparisonMacro(%s, %s, %s)" % (
            repr(self.instruction), repr(self.target), repr(self.args))
    
    def used_vars(self):
        return self.args
    
    def expand(self, dex_objects):
    
        ins = []
        ln, rn = self.copy_variables_to_low_registers(self.args, ins, dex_objects)
        
        # Append the branch instruction that compares the operands and jumps to
        # the supplied target if the comparison succeeds.
        ins.append(Branch(self.target, self.instruction, [v(ln, 4), v(rn, 4)], self.used_vars()))
        
        # We do not need to copy any values back to high registers.
        
        return ins
    
    def extra_vars(self):
    
        left, right = self.args
        return ExtendedMacro.extra_vars(self, left, right)


class FloatComparisonMacro(Macro):

    def __init__(self, result, left, right):
    
        self.result = result
        self.left = left
        self.right = right
    
    def used_vars(self):
        return [self.result, self.left, self.right]
    
    def expand(self, dex_objects):
    
        if isinstance(self.left.type_, Float):
            ins = cmpl_float(v(self.result.n, 8), v(self.left.n, 8), v(self.right.n, 8))
        elif isinstance(self.left.type_, Double):
            ins = cmpl_double(v(self.result.n, 8), v(self.left.n, 8), v(self.right.n, 8))
        elif isinstance(self.left.type_, Long):
            ins = cmp_long(v(self.result.n, 8), v(self.left.n, 8), v(self.right.n, 8))
        else:
            raise TypeError("Type %s not supported in comparison." % repr(self.left.type_))
        
        self.annotate(ins, dex_objects)
        return [ins]


class CompareZeroMacro(ExtendedMacro):

    def __init__(self, instruction, target, argument):
    
        self.instruction = instruction
        self.target = target
        self.arg = argument
    
    def used_vars(self):
        return [self.arg]
    
    def expand(self, dex_objects):
    
        ins = []
        n, next = self.copy_variable_to_low_registers(self.arg, 0, ins, dex_objects)
        
        # Append the branch instruction that compares the operands and jumps to
        # the supplied target if the comparison succeeds.
        ins.append(Branch(self.target, self.instruction, [v(n, 4)], self.used_vars()))
        
        # We do not need to copy any values back to high registers.
        
        return ins
    
    def extra_vars(self):
    
        return ExtendedMacro.extra_vars(self, self.arg)


class Branch(Macro):

    def __init__(self, target, instruction, arguments, variables):
    
        self.target = target
        self.instruction = instruction
        # The arguments and variables are different for a branch instruction.
        self.args = arguments
        self.variables = variables
    
    def __repr__(self):
        return "Branch(%s)" % repr(self.target)
    
    def used_vars(self):
        return self.variables
    
    def expand(self, dex_objects):
        return [self]
    
    def resolve(self, offset, dex_objects):
    
        args = self.args[:]
        
        # The offset is always the last argument in the instructions we use,
        # so append it when we need it.
        args.append(Index16(offset))
        
        ins = self.instruction(*args)
        self.annotate(ins, dex_objects)
        return ins
    
    def size(self):
        return self.instruction.size


class Target(Macro):

    def __init__(self, label):
        self.label = label
    
    def __repr__(self):
        return "Target(%s)" % repr(self.label)
    
    def expand(self, dex_objects):
        return []


class Goto(Branch):

    def __init__(self, target):
        self.target = target
        self.instruction = goto
    
    def __repr__(self):
        return "Goto(%s)" % repr(self.target)
    
    def used_vars(self):
        return []
    
    def expand(self, dex_objects):
        # Unlike other macros, Goto is not expanded until later when the
        # resolve method is called to determine which size of branch is needed.
        return [self]
    
    def resolve(self, offset, dex_objects):
    
        if -128 <= offset < 127:
            index = Index(offset, 8)
            self.instruction = goto
        elif -32768 <= offset < 32767:
            index = Index(offset, 16)
            self.instruction = goto_16
        else:
            index = Index(offset, 32)
            self.instruction = goto_32
        
        return self.instruction(index)
    
    def size(self):
        return self.instruction.size


class GetAttr(ExtendedMacro):

    def __init__(self, variable, field, instance = None):
    
        self.variable = variable
        self.field = field
        self.instance = instance
    
    def used_vars(self):
        if self.instance:
            return [self.variable, self.instance]
        else:
            return [self.variable]
    
    def expand(self, dex_objects):
    
        # Find the index of the field in the main collection's list of fields.
        field_index = dex_objects.fields.index(self.field)
        insts = []
        
        if self.instance is None:
            if isinstance(self.field.type_, Int):
                ins = sget(v(self.variable.n, 8), Index(field_index, 16))
            elif isinstance(self.field.type_, Float):
                ins = sget(v(self.variable.n, 8), Index(field_index, 16))
            elif isinstance(self.field.type_, Long):
                ins = sget_wide(v(self.variable.n, 8), Index(field_index, 16))
            elif isinstance(self.field.type_, Double):
                ins = sget_wide(v(self.variable.n, 8), Index(field_index, 16))
            elif isinstance(self.field.type_, Name):
                ins = sget_object(v(self.variable.n, 8), Index(field_index, 16))
            elif isinstance(self.field.type_, Array):
                ins = sget_object(v(self.variable.n, 8), Index(field_index, 16))
            elif isinstance(self.field.type_, Boolean):
                ins = sget_boolean(v(self.variable.n, 8), Index(field_index, 16))
            elif isinstance(self.field.type_, Short):
                ins = sget_short(v(self.variable.n, 8), Index(field_index, 16))
            elif isinstance(self.field.type_, Char):
                ins = sget_char(v(self.variable.n, 8), Index(field_index, 16))
            elif isinstance(self.field.type_, Byte):
                ins = sget_byte(v(self.variable.n, 8), Index(field_index, 16))
            else:
                raise TypeError("Failed to create instructions to encode value of type %s." % repr(self.field.type_))
            
            insts.append(ins)
        else:
            # Instance field. Only copy the instance to a low register.
            rn, ln = self.copy_variable_to_low_registers(
                self.instance, 0, insts, dex_objects)
            
            if self.variable.n + self.variable.size() <= 16:
                ln = self.variable.n
            
            if isinstance(self.field.type_, Int):
                ins = iget(v(ln, 4), v(rn, 4), Index(field_index, 16))
            elif isinstance(self.field.type_, Float):
                ins = iget(v(ln, 4), v(rn, 4), Index(field_index, 16))
            elif isinstance(self.field.type_, Long):
                ins = iget_wide(v(ln, 4), v(rn, 4), Index(field_index, 16))
            elif isinstance(self.field.type_, Double):
                ins = iget_wide(v(ln, 4), v(rn, 4), Index(field_index, 16))
            elif isinstance(self.field.type_, Name):
                ins = iget_object(v(ln, 4), v(rn, 4), Index(field_index, 16))
            elif isinstance(self.field.type_, Array):
                ins = iget_object(v(ln, 4), v(rn, 4), Index(field_index, 16))
            elif isinstance(self.field.type_, Boolean):
                ins = iget_boolean(v(ln, 4), v(rn, 4), Index(field_index, 16))
            elif isinstance(self.field.type_, Short):
                ins = iget_short(v(ln, 4), v(rn, 4), Index(field_index, 16))
            elif isinstance(self.field.type_, Char):
                ins = iget_char(v(ln, 4), v(rn, 4), Index(field_index, 16))
            elif isinstance(self.field.type_, Byte):
                ins = iget_byte(v(ln, 4), v(rn, 4), Index(field_index, 16))
            else:
                raise TypeError("Failed to create instructions to encode value of type %s." % repr(self.field.type_))
            
            insts.append(ins)
            
            # Only copy the result to a high register.
            self.copy_variable_from_low_registers(self.variable, ln, insts, dex_objects)
        
        self.annotate(ins, dex_objects)
        return insts
    
    def extra_vars(self):
    
        if self.instance is not None:
            return ExtendedMacro.extra_vars(self, self.variable, self.instance)
        else:
            return []


class SetAttr(ExtendedMacro):

    def __init__(self, variable, field, instance = None):
    
        self.variable = variable
        self.field = field
        self.instance = instance
    
    def __repr__(self):
    
        return "SetAttr(%s, %s, %s)" % (repr(self.variable), repr(self.field),
            repr(self.instance))
    
    def used_vars(self):
        if self.instance:
            return [self.variable, self.instance]
        else:
            return [self.variable]
    
    def expand(self, dex_objects):
    
        # Find the index of the field in the main collection's list of fields.
        field_index = dex_objects.fields.index(self.field)
        insts = []
        
        if self.instance is None:
        
            # Static field
            if isinstance(self.field.type_, Int):
                ins = sput(v(self.variable.n, 8), Index(field_index, 16))
            elif isinstance(self.field.type_, Float):
                ins = sput(v(self.variable.n, 8), Index(field_index, 16))
            elif isinstance(self.field.type_, Long):
                ins = sput_wide(v(self.variable.n, 8), Index(field_index, 16))
            elif isinstance(self.field.type_, Double):
                ins = sput_wide(v(self.variable.n, 8), Index(field_index, 16))
            elif isinstance(self.field.type_, Name):
                ins = sput_object(v(self.variable.n, 8), Index(field_index, 16))
            elif isinstance(self.field.type_, Array):
                ins = sput_object(v(self.variable.n, 8), Index(field_index, 16))
            elif isinstance(self.field.type_, Boolean):
                ins = sput_boolean(v(self.variable.n, 8), Index(field_index, 16))
            elif isinstance(self.field.type_, Byte):
                ins = sput_byte(v(self.variable.n, 8), Index(field_index, 16))
            elif isinstance(self.field.type_, Char):
                ins = sput_char(v(self.variable.n, 8), Index(field_index, 16))
            elif isinstance(self.field.type_, Short):
                ins = sput_short(v(self.variable.n, 8), Index(field_index, 16))
            else:
                raise TypeError("Failed to create instructions to encode value of type %s." % repr(self.field.type_))
            
            insts.append(ins)
        else:
            # Instance field. Copy both the instance object and value to low
            # registers.
            ln, rn = self.copy_variables_to_low_registers(
                [self.variable, self.instance], insts, dex_objects)
            
            if isinstance(self.field.type_, Int):
                ins = iput(v(ln, 4), v(rn, 4), Index(field_index, 16))
            elif isinstance(self.field.type_, Float):
                ins = iput(v(ln, 4), v(rn, 4), Index(field_index, 16))
            elif isinstance(self.field.type_, Long):
                ins = iput_wide(v(ln, 4), v(rn, 4), Index(field_index, 16))
            elif isinstance(self.field.type_, Double):
                ins = iput_wide(v(ln, 4), v(rn, 4), Index(field_index, 16))
            elif isinstance(self.field.type_, Name):
                ins = iput_object(v(ln, 4), v(rn, 4), Index(field_index, 16))
            elif isinstance(self.field.type_, Array):
                ins = iput_object(v(ln, 4), v(rn, 4), Index(field_index, 16))
            elif isinstance(self.field.type_, Boolean):
                ins = iput_boolean(v(ln, 4), v(rn, 4), Index(field_index, 16))
            elif isinstance(self.field.type_, Byte):
                ins = iput_byte(v(ln, 4), v(rn, 4), Index(field_index, 16))
            elif isinstance(self.field.type_, Char):
                ins = iput_char(v(ln, 4), v(rn, 4), Index(field_index, 16))
            elif isinstance(self.field.type_, Short):
                ins = iput_short(v(ln, 4), v(rn, 4), Index(field_index, 16))
            else:
                raise TypeError("Failed to create instructions to encode value of type %s." % repr(self.field.type_))
            
            insts.append(ins)
            
            # No need to copy the values back to high registers.
        
        self.annotate(ins, dex_objects)
        return insts
    
    def extra_vars(self):
    
        if self.instance is not None:
            return ExtendedMacro.extra_vars(self, self.variable, self.instance)
        else:
            return []


class NewInstance(Macro):

    def __init__(self, variable, class_name):
    
        self.variable = variable
        self.class_name = class_name
    
    def __repr__(self):
        return "NewInstance(%s, %s)" % (repr(self.variable), repr(self.class_name))
    
    def used_vars(self):
        return [self.variable]
    
    def expand(self, dex_objects):
    
        # Find the index of the type in the main collection's list of types.
        type_index = dex_objects.types.index(self.class_name)
        
        ins = new_instance(v(self.variable.n, 8), Index(type_index, 16))
        self.annotate(ins, dex_objects)
        return [ins]


class Negate(ExtendedMacro):

    def __init__(self, dest, src):
    
        self.dest_variable = dest
        self.src_variable = src
    
    def used_vars(self):
        return [self.dest_variable, self.src_variable]
    
    def expand(self, dex_objects):
    
        insts = []
        rn, ln = self.copy_variable_to_low_registers(
            self.src_variable, 0, insts, dex_objects)
        
        if self.dest_variable.n + self.dest_variable.size() <= 16:
            ln = self.dest_variable.n
        
        if isinstance(self.src_variable.type_, Int):
            ins = neg_int(v(ln, 4), v(rn, 4))
        elif isinstance(self.src_variable.type_, Long):
            ins = neg_long(v(ln, 4), v(rn, 4))
        elif isinstance(self.src_variable.type_, Float):
            ins = neg_float(v(ln, 4), v(rn, 4))
        elif isinstance(self.src_variable.type_, Double):
            ins = neg_double(v(ln, 4), v(rn, 4))
        elif isinstance(self.src_variable.type_, Boolean):
            # Negate the integer but keep only the bottom bit.
            ins = neg_int(v(ln, 4), v(rn, 4))
            insts.append(self.annotate(ins, dex_objects))
            
            ins = and_int_lit8(v(ln, 4), v(ln, 4), 1)
        else:
            raise TypeError("Failed to create instructions to negate value of type %s." % repr(self.variable.type_))
        
        self.annotate(ins, dex_objects)
        insts.append(ins)
        
        self.copy_variable_from_low_registers(self.dest_variable, ln, insts, dex_objects)
        return insts
    
    def extra_vars(self):
    
        return ExtendedMacro.extra_vars(self, self.dest_variable, self.src_variable)


class Not(ExtendedMacro):

    def __init__(self, dest, src):
    
        self.dest_variable = dest
        self.src_variable = src
    
    def used_vars(self):
        return [self.dest_variable, self.src_variable]
    
    def expand(self, dex_objects):
    
        insts = []
        rn, ln = self.copy_variable_to_low_registers(
            self.src_variable, 0, insts, dex_objects)
        
        if self.dest_variable.n + self.dest_variable.size() <= 16:
            ln = self.dest_variable.n
        
        if isinstance(self.src_variable.type_, Int):
            ins = not_int(v(ln, 4), v(rn, 4))
        elif isinstance(self.src_variable.type_, Long):
            ins = not_long(v(ln, 4), v(rn, 4))
        elif isinstance(self.src_variable.type_, Float):
            ins = not_float(v(ln, 4), v(rn, 4))
        elif isinstance(self.src_variable.type_, Double):
            ins = not_double(v(ln, 4), v(rn, 4))
        elif isinstance(self.src_variable.type_, Boolean):
            # Negate the integer but keep only the bottom bit.
            insts.append(self.annotate(if_eqz(v(rn, 4), Index8(4)), dex_objects))
            insts.append(self.annotate(const_4(v(ln, 4), 0), dex_objects))
            insts.append(goto(2))
            ins = const_4(v(ln, 4), 1)
        else:
            raise TypeError("Failed to create instructions to negate value of type %s." % repr(self.variable.type_))
        
        self.annotate(ins, dex_objects)
        insts.append(ins)
        
        self.copy_variable_from_low_registers(self.dest_variable, ln, insts, dex_objects)
        return insts
    
    def extra_vars(self):
    
        return ExtendedMacro.extra_vars(self, self.dest_variable, self.src_variable)


class CheckCast(Macro):

    def __init__(self, variable, class_name):
    
        self.variable = variable
        self.class_name = class_name
    
    def used_vars(self):
        return [self.variable]
    
    def expand(self, dex_objects):
    
        # Find the index of the type in the main collection's list of types.
        type_index = dex_objects.types.index(self.class_name)
        ins = check_cast(v(self.variable.n, 8), Index(type_index, 16))
        
        self.annotate(ins, dex_objects)
        return [ins]


class CastPrimitive(ExtendedMacro):

    cast_ins = {
        Int: {
            Float: int_to_float, Long: int_to_long, Double: int_to_double,
            Short: int_to_short, Char: int_to_char, Byte:   int_to_byte
            },
        Float: {
            Int: float_to_int, Long: float_to_long, Double: float_to_double
            },
        Long: {
            Int: long_to_int, Float: long_to_float, Double: long_to_double
            },
        Double: {
            Int: double_to_int, Float: double_to_float, Long: double_to_long
            }
        }
    
    def __init__(self, dest_variable, src_variable):
    
        self.dest = dest_variable
        self.src = src_variable
        
        # Check that a cast is available for the two types involved.
        self.instruction = self.cast_ins[src_variable.type_.__class__][dest_variable.type_.__class__]
    
    def used_vars(self):
        return [self.dest, self.src]
    
    def expand(self, dex_objects):
    
        insts = []
        
        # Only copy the source value to a low register.
        rn, ln = self.copy_variable_to_low_registers(self.src, 0, insts, dex_objects)
        
        if self.dest.n + self.dest.size() <= 16:
            ln = self.dest.n
        
        insts.append(self.annotate(self.instruction(v(ln, 4), v(rn, 4)), dex_objects))
        
        # Only copy the result from a low register.
        self.copy_variable_from_low_registers(self.dest, ln, insts, dex_objects)
        return insts
    
    def extra_vars(self):
    
        return ExtendedMacro.extra_vars(self, self.dest, self.src)


class ArrayGet(Macro):

    def __init__(self, variable, array_var, index_var):
    
        self.variable = variable
        self.array_var = array_var
        self.index_var = index_var
    
    def used_vars(self):
        return [self.variable, self.array_var, self.index_var]
    
    def expand(self, dex_objects):
    
        type_ = self.variable.type_
        
        if isinstance(type_, Int):
            ins = aget(v(self.variable.n, 8), v(self.array_var.n, 8), v(self.index_var.n, 8))
        elif isinstance(type_, Float):
            ins = aget(v(self.variable.n, 8), v(self.array_var.n, 8), v(self.index_var.n, 8))
        elif isinstance(type_, Long):
            ins = aget_wide(v(self.variable.n, 8), v(self.array_var.n, 8), v(self.index_var.n, 8))
        elif isinstance(type_, Double):
            ins = aget_wide(v(self.variable.n, 8), v(self.array_var.n, 8), v(self.index_var.n, 8))
        elif isinstance(type_, visitor.ClassVisitor) or isinstance(type_, TemplateClass):
            ins = aget_object(v(self.variable.n, 8), v(self.array_var.n, 8), v(self.index_var.n, 8))
        elif isinstance(type_, ArrayContainer):
            ins = aget_object(v(self.variable.n, 8), v(self.array_var.n, 8), v(self.index_var.n, 8))
        elif isinstance(type_, Boolean):
            ins = aget_boolean(v(self.variable.n, 8), v(self.array_var.n, 8), v(self.index_var.n, 8))
        elif isinstance(type_, Byte):
            ins = aget_byte(v(self.variable.n, 8), v(self.array_var.n, 8), v(self.index_var.n, 8))
        elif isinstance(type_, Char):
            ins = aget_char(v(self.variable.n, 8), v(self.array_var.n, 8), v(self.index_var.n, 8))
        elif isinstance(type_, Short):
            ins = aget_short(v(self.variable.n, 8), v(self.array_var.n, 8), v(self.index_var.n, 8))
        else:
            raise TypeError("Failed to create instructions to encode value of type %s." % repr(type_))
        
        self.annotate(ins, dex_objects)
        return [ins]


class ArrayPut(Macro):

    def __init__(self, variable, array_var, index_var):
    
        self.variable = variable
        self.array_var = array_var
        self.index_var = index_var
    
    def used_vars(self):
        return [self.variable, self.array_var, self.index_var]
    
    def expand(self, dex_objects):
    
        type_ = self.variable.type_
        
        if isinstance(type_, Int):
            ins = aput(v(self.variable.n, 8), v(self.array_var.n, 8), v(self.index_var.n, 8))
        elif isinstance(type_, Float):
            ins = aput(v(self.variable.n, 8), v(self.array_var.n, 8), v(self.index_var.n, 8))
        elif isinstance(type_, Long):
            ins = aput_wide(v(self.variable.n, 8), v(self.array_var.n, 8), v(self.index_var.n, 8))
        elif isinstance(type_, Double):
            ins = aput_wide(v(self.variable.n, 8), v(self.array_var.n, 8), v(self.index_var.n, 8))
        elif isinstance(type_, visitor.ClassVisitor) or isinstance(type_, TemplateClass):
            ins = aput_object(v(self.variable.n, 8), v(self.array_var.n, 8), v(self.index_var.n, 8))
        elif isinstance(type_, ArrayContainer):
            ins = aput_object(v(self.variable.n, 8), v(self.array_var.n, 8), v(self.index_var.n, 8))
        elif isinstance(type_, Boolean):
            ins = aput_boolean(v(self.variable.n, 8), v(self.array_var.n, 8), v(self.index_var.n, 8))
        elif isinstance(type_, Byte):
            ins = aput_byte(v(self.variable.n, 8), v(self.array_var.n, 8), v(self.index_var.n, 8))
        elif isinstance(type_, Char):
            ins = aput_char(v(self.variable.n, 8), v(self.array_var.n, 8), v(self.index_var.n, 8))
        elif isinstance(type_, Short):
            ins = aput_short(v(self.variable.n, 8), v(self.array_var.n, 8), v(self.index_var.n, 8))
        else:
            raise TypeError("Failed to create instructions to encode value of type %s." % repr(type_))
        
        self.annotate(ins, dex_objects)
        return [ins]


class ArrayLength(ExtendedMacro):

    def __init__(self, variable, array_var):
    
        self.variable = variable
        self.array_var = array_var
    
    def used_vars(self):
        return [self.variable, self.array_var]
    
    def expand(self, dex_objects):
    
        insts = []
        
        # Only copy the array object to a low register.
        rn, ln = self.copy_variable_to_low_registers(self.array_var, 0, insts, dex_objects)
        
        if self.variable.n + self.variable.size() <= 16:
            ln = self.variable.n
        
        insts.append(self.annotate(array_length(v(ln, 4), v(rn, 4)), dex_objects))
        
        # Only copy the result from a low register.
        self.copy_variable_from_low_registers(self.variable, ln, insts, dex_objects)
        
        return insts
    
    def extra_vars(self):
    
        return ExtendedMacro.extra_vars(self, self.variable, self.array_var)


class NewArray(Macro):

    def __init__(self, variable, count_var, type_name):
        self.variable = variable
        self.count_var = count_var
        self.type_name = type_name
    
    def used_vars(self):
        return [self.variable, self.count_var]
    
    def expand(self, dex_objects):
    
        insts = []
        ln, rn = self.copy_variables_to_low_registers(
            [self.variable, self.count_var], insts, dex_objects)
        
        # Find the index of the type in the main collection's list of types.
        type_index = dex_objects.types.index(self.type_name)
        
        insts.append(self.annotate(new_array(v(ln, 4), v(rn, 4), type_index), dex_objects))
        
        # Only copy the result from a low register.
        self.copy_variable_from_low_registers(self.variable, ln, insts, dex_objects)
        
        return insts
    
    def extra_vars(self):
    
        return ExtendedMacro.extra_vars(self, self.variable, self.count_var)


class Throw(Macro):

    def __init__(self, variable):
        self.variable = variable
    
    def used_vars(self):
        return [self.variable]
    
    def expand(self, dex_objects):
        ins = throw(v(self.variable.n, 8))
        self.annotate(ins, dex_objects)
        return [ins]


class TryHandlers:

    def __init__(self, begin, end, handlers):
    
        self.begin = begin
        self.end = end
        self.handlers = handlers


type_operation_instructions = {
    Int: {
        ast.Sub: sub_int,
        ast.Add: add_int,
        ast.Mult: mul_int,
        ast.Div: div_int,
        ast.Mod: rem_int,
        ast.BitOr: or_int,
        ast.BitAnd: and_int,
        ast.BitXor: xor_int,
        ast.LShift: shl_int,
        ast.RShift: shr_int
        },
    Float: {
        ast.Sub: sub_float,
        ast.Add: add_float,
        ast.Mult: mul_float,
        ast.Div: div_float,
        ast.Mod: rem_float,
        },
    Long: {
        ast.Sub: sub_long,
        ast.Add: add_long,
        ast.Mult: mul_long,
        ast.Div: div_long,
        ast.Mod: rem_long,
        ast.BitOr: or_long,
        ast.BitAnd: and_long,
        ast.BitXor: xor_long,
        ast.LShift: shl_long, ### This needs the second argument to be truncated.
        ast.RShift: shr_long ### This needs the second argument to be truncated.
        },
    Double: {
        ast.Sub: sub_double,
        ast.Add: add_double,
        ast.Mult: mul_double,
        ast.Div: div_double,
        ast.Mod: rem_double,
        },
    Boolean: {
        ast.And: and_int,
        ast.Or: or_int
        }
    }

def get_type_operation_instruction(type_, op, node, scope):

    try:
        return type_operation_instructions[type_.__class__][op]
    except KeyError:
        raise NameLookupError("Operation '%s' not handled for type '%s' at line %i "
            " of file %s." % (op, type_, node.lineno, scope.file_name()))

type_comparison_instructions = {
    Int: {
        ast.Eq: if_eq,
        ast.NotEq: if_ne,
        ast.Lt:  if_lt,
        ast.Gt:  if_gt,
        ast.LtE: if_le,
        ast.GtE: if_ge
        },
    Boolean: {
        ast.Eq: if_eq,
        ast.NotEq: if_ne,
        ast.Lt:  if_lt,
        ast.Gt:  if_gt,
        ast.LtE: if_le,
        ast.GtE: if_ge
        },
    Short: {
        ast.Eq: if_eq,
        ast.NotEq: if_ne,
        ast.Lt:  if_lt,
        ast.Gt:  if_gt,
        ast.LtE: if_le,
        ast.GtE: if_ge
        },
    Char: {
        ast.Eq: if_eq,
        ast.NotEq: if_ne,
        ast.Lt:  if_lt,
        ast.Gt:  if_gt,
        ast.LtE: if_le,
        ast.GtE: if_ge
        },
    Byte: {
        ast.Eq: if_eq,
        ast.NotEq: if_ne,
        ast.Lt:  if_lt,
        ast.Gt:  if_gt,
        ast.LtE: if_le,
        ast.GtE: if_ge
        },
    visitor.ClassVisitor: {
        ast.Eq: if_eq,
        ast.NotEq: if_ne,
        ast.Lt:  if_lt,
        ast.Gt:  if_gt,
        ast.LtE: if_le,
        ast.GtE: if_ge
        },
    Null: {
        ast.Eq: if_eqz,
        ast.NotEq: if_nez,
        ast.Lt:  if_ltz,
        ast.Gt:  if_gtz,
        ast.LtE: if_lez,
        ast.GtE: if_gez
        },
    Float: {
        ast.Eq: if_eqz,
        ast.NotEq: if_nez,
        ast.Lt:  if_ltz,
        ast.Gt:  if_gtz,
        ast.LtE: if_lez,
        ast.GtE: if_gez
        },
    Double: {
        ast.Eq: if_eqz,
        ast.NotEq: if_nez,
        ast.Lt:  if_ltz,
        ast.Gt:  if_gtz,
        ast.LtE: if_lez,
        ast.GtE: if_gez
        },
    Long: {
        ast.Eq: if_eqz,
        ast.NotEq: if_nez,
        ast.Lt:  if_ltz,
        ast.Gt:  if_gtz,
        ast.LtE: if_lez,
        ast.GtE: if_gez
        }
    }

library_type_casts = {
    Byte: {Int: "cast_to_int"},
    Short: {Int: "cast_to_int"}
    }

library_type_cast_methods = {
    Int: "cast_to_int"
    }

def get_type_comparison_instruction(type_, op, node, scope):

    try:
        return type_comparison_instructions[type_.__class__][op]
    except KeyError:
        raise NameLookupError("Comparison '%s' not handled for type '%s' at line %i "
            "of file %s." % (op, type_, node.lineno, scope.file_name()))


# Convenience function for casting Objects, not values of primitive types.

def cast_value(variable, type_, context, dex_objects):

    dex_type = create_dex_type(type_)
    context.append(CheckCast(variable, dex_type))
    dex_objects.types.add(dex_type)


def can_cast_primitive(original_type, new_type):

    # Use the dictionary in the relevant macro to determine whether the types
    # are compatible (and a casting operation supported).
    try:
        if new_type.__class__ in CastPrimitive.cast_ins[original_type.__class__]:
            return True
    except KeyError:
        pass
    
    return False

def can_cast_primitive_with_library(original_type, new_type):

    # Ideally, we would look at the available library methods for casting and
    # determine if there is one for the pair of types supplied.
    return original_type.__class__ in library_type_casts and \
           new_type.__class__ in library_type_casts[original_type.__class__]

def can_cast_with_library(new_type):

    return new_type.__class__ in library_type_cast_methods

def get_type_to_cast_to(t1, t2):

    # By default, we try to cast to the larger type. For equally-sized types,
    # we need to choose the type that can be casted to. For example, integers
    # and short integers are treated as having the same same, but we can only
    # cast from integers to shorts, not the other way round.
    
    if t1.size() > t2.size():
        return t1
    elif t1.size() < t2.size():
        return t2
    
    if can_cast_primitive(t1, t2):
        return t2
    elif can_cast_primitive(t2, t1):
        return t1
    else:
        None
