"""
visitor.py - Abstract syntax tree visitor classes.

Copyright (c) 2016, David Boddie <david@boddie.org.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import ast, os, sys

class VisitorError(Exception):
    pass

class Visitor:

    """Traverses the abstract syntax tree.
    
    This class defines how the tree is traversed so that subclasses only need
    to implement handler methods for the nodes they are interested in.
    """
    
    type_names = ("void", "bool", "boolean", "byte", "short", "char", "int",
                  "long", "float", "double", "E", "K", "V", "T", "False",
                  "True", "None", "P", "Q", "R", "S", "str", "list", "dict", "set")
    
    def __init__(self, paths, default_package = ""):
    
        # Record the module search paths.
        self.paths = paths
        self.default_package = default_package
        self.indent = 0
        self.module = None
        self.name = None
        self.imports = {}
    
    def load(self, file_name, imports = None):
    
        if imports is None:
            imports = {}
        
        self.imports = imports
        self.file_name_ = file_name
        
        try:
            self.root = ast.parse(open(file_name).read())
        except SyntaxError as err:
            sys.stderr.write("In '%s': " % file_name)
            raise err
        
        module = self.visit(self.root)
        self.imports[os.path.splitext(file_name)[0]] = module
        return module
    
    def parse(self, source, file_name, imports = None):
    
        if imports is None:
            imports = {}
        
        self.imports = imports
        self.file_name_ = file_name
        
        try:
            self.root = ast.parse(source)
        except SyntaxError:
            print "In", file_name
            raise
        
        module = self.visit(self.root)
        self.imports[os.path.splitext(file_name)[0]] = module
        return module
    
    def visit(self, node):
        """Generic visitor method that handles the visit to the given node."""
        
        if node is None:
            return
        
        self.indent += 1
        #print (" "*self.indent) + node.__class__.__name__
        
        method = {
            ast.Assign: self.visitAssign,
            ast.Attribute: self.visitAttribute,
            ast.BinOp: self.visitIgnored,
            ast.Call: self.visitCall,
            ast.ClassDef: self.visitClassDef,
            ast.Dict: self.visitDict,
            ast.Expr: self.visitExpr,
            ast.FunctionDef: self.visitFunctionDef,
            ast.If: self.visitIgnored,
            ast.Import: self.visitImport,
            ast.ImportFrom: self.visitImportFrom,
            ast.List: self.visitList,
            ast.Module: self.visitModule,
            ast.Name: self.visitName,
            ast.Num: self.visitNum,
            ast.Pass: self.visitPass,
            ast.Str: self.visitStr,
            ast.Tuple: self.visitTuple,
            ast.UnaryOp: self.visitUnaryOp
            }.get(node.__class__)
        
        if method:
            result = method(node)
        else:
            raise VisitorError("Unhandled node type %s in %s at line %s." % (
                node.__class__, self, node.lineno))
        
        self.indent -= 1
        return result
    
    # AST node handlers
    
    # Handlers that are common to some or all visitors are implemented.
    # Those that need to be handled specially are empty.
    
    def visitAssign(self, node):
        pass
    
    def visitAttribute(self, node):
        return node
    
    def visitBase(self, node):
    
        if isinstance(node, ast.Attribute):
            return self.visitBase(node.value) + "." + node.attr
        else:
            return node.id
    
    def visitCall(self, node):
        pass
    
    def visitClassDef(self, node):
        pass
    
    def visitDict(self, node):
        return node
    
    def visitDiscard(self, node):
        pass
    
    def visitExpr(self, node):
        self.visit(node.value)
    
    def visitFrom(self, node):
        pass
    
    def visitFunctionDef(self, node):
        pass
    
    def visitGetattr(self, node):
        return node
    
    def visitIgnored(self, node):
        # Ignore certain top-level statements and expressions.
        # Perhaps we should warn about them.
        pass
    
    def visitImport(self, node):
        pass
    
    def visitImportFrom(self, node):
        pass
    
    def visitList(self, node):
    
        for child in node.elts:
            self.visit(child)
    
    def visitModule(self, node):
    
        name = os.path.splitext(os.path.split(self.file_name_)[1])[0]
        module_visitor = ModuleVisitor(name, self.file_name_, self.paths,
                                       self.imports, self.default_package,
                                       placeholder = False)
        module_visitor.visit(node)
        self.module = module_visitor
        return module_visitor
    
    def visitName(self, node):
        return node
    
    def visitNum(self, node):
        return node
    
    def visitPass(self, node):
        pass
    
    def visitStr(self, node):
        return node
    
    def visitTryExcept(self, node):
        pass
    
    def visitTuple(self, node):
    
        for child in node.elts:
            self.visit(child)
    
    # Unary operators
    
    def visitUnaryOp(self, node):
        pass
    
    def simplify(self, node):
    
        if isinstance(node, ast.Name):
        
            if node.id in Visitor.type_names:
                return node
            else:
                return ClassReference(node.id, self, node, self.file_name())
        
        elif isinstance(node, ast.Call):
            # Simplify the name and arguments.
            node.func = self.simplify(node.func)
            node.args = map(self.simplify, node.args)
            
            if isinstance(node.func, Parameter):
                args = node.args
                node = node.func
                node.args = args
            
            return node
        
        elif isinstance(node, ast.Dict):
        
            # The dictionary keys must be string constants.
            d = {}
            for key, value in zip(node.keys, node.values):
                try:
                    d[key.s] = self.simplify(value)
                except:
                    raise VisitorError("Invalid type used for dictionary key "
                        "at line %i of file %s." % (node.lineno,
                        self.module.file_name()))
            
            return d
        
        elif isinstance(node, ast.List):
            return map(self.simplify, node.elts)
        
        elif isinstance(node, ast.Attribute):
        
            first = self.simplify(node.value)
            if isinstance(first, ast.Name):
                first = first.id
            else:
                first = first.value
            
            return ClassReference(first + "." + node.attr, self, node,
                                  self.file_name())
        
        else:
            return node
    
    def ordered_classes(self):
    
        classes = []
        for name in self.class_order:
        
            class_ = self.classes[name]
            
            if not class_.visited:
                for child in class_.code:
                    class_.visit(child)
                
                class_.visited = True
            
            classes.append(class_)
        
        return classes
    
    def file_name(self):
        return self.file_name_


class ModuleVisitor(Visitor):

    def __init__(self, name, file_name, paths, imports, default_package,
                 placeholder = True, indent = 0):
    
        self.name = name
        self.file_name_ = file_name
        self.paths = paths
        self.classes = {}
        self.class_order = []
        self.attributes = {}
        self.deferred = {}
        
        self.parent = None
        self.indent = indent
        self.imports = imports
        self.default_package = default_package
        self.placeholder = placeholder
    
    def __str__(self):
    
        return self.name
    
    def visitAssign(self, node):
    
        value = self.visit(node.value)
        
        for target in node.targets:
            name = self.visit(target).id
            self.attributes[name] = value
    
    def visitClassDef(self, node):
    
        bases = []
        for child in node.bases:
            bases.append(self.visitBase(child))
        
        class_visitor = ClassVisitor(unicode(node.name), bases, node.body,
                                     self, self, visit = False,
                                     indent = self.indent)
        
        self.classes[node.name] = class_visitor
        self.class_order.append(node.name)
    
    def visitImportFrom(self, node):
    
        # Try to import from the current module's directory first.
        current_dir = os.path.split(self.file_name_)[0]
        
        if current_dir not in self.paths:
            paths = [current_dir] + self.paths
        else:
            paths = self.paths
        
        for path in paths:
            module = self.import_module(node.module, path, False)
            if module != None:
                break
        else:
            raise ImportError("Failed to import module '%s' at line %i in '%s'." % (
                node.module, node.lineno, self.file_name_))
        
        for alias in node.names:
            self.deferred[alias.name] = DeferredName(module, alias.name, node, self)
    
    def visitImport(self, node):
    
        for module_name in node.names:
        
            # Try to import from the current module's directory first.
            current_dir = os.path.split(self.file_name_)[0]
            if current_dir not in self.paths:
                paths = [current_dir] + self.paths
            else:
                paths = self.paths
            
            for path in paths:
                module = self.import_module(module_name.name, path, True)
                if module != None:
                    break
            else:
                raise ImportError("Failed to import module '%s' at line %i in '%s'." % (
                    module_name.name, node.lineno, self.file_name_))
    
    def visitModule(self, node):
    
        self.name = os.path.splitext(os.path.split(self.file_name_)[1])[0]
        for child in node.body:
            self.visit(child)
        
        return self
    
    def import_module(self, module_name, path, import_names):
    
        # Retrieve special modules like app_resources here since they have no
        # file locations we can use to compare them with other modules. Their
        # locations are their module names.
        special = self.imports.get(module_name)
        
        if special:
            if import_names:
                # Ensure that the module is imported into the current module.
                self.attributes[module_name] = special
            return special
        
        pieces = module_name.split(".")
        current = []
        container = self
        
        for piece in pieces:
        
            current.append(piece)
            dir_path = os.path.join(path, os.sep.join(current))
            abs_path = os.path.abspath(dir_path)
            module_path = ".".join(current)
            
            if len(current) == len(pieces):
            
                # At the end of the module path.
                
                # Try to retrieve a cached module.
                old_module = self.imports.get(abs_path)
                
                # Find the module file, either a normal module file or a
                # package file.
                if os.path.isdir(dir_path):
                    file_path = os.path.join(dir_path, "__init__.py")
                elif os.path.isfile(dir_path + ".py"):
                    file_path = dir_path + ".py"
                elif not old_module:
                    # Catch cases where there is no corresponding file or
                    # directory for a module being searched for. Resource
                    # modules will have already been registered and will not
                    # cause None to be returned here.
                    return None
                
                if not old_module:
                    module = ModuleVisitor(piece, file_path, self.paths,
                                           self.imports, self.default_package)
                else:
                    module = old_module
                
                # Register the module before visiting it. This causes
                # circular references to this module to be resolved without
                # causing it to be visited again.
                self.imports[abs_path] = module
                
                if import_names:
                    container.attributes[piece] = module
                
                return module
            
            elif os.path.isdir(dir_path):
            
                # Create an empty placeholder module if one does not already exist.
                try:
                    module = self.imports[dir_path]
                except KeyError:
                    module = ModuleVisitor(piece, dir_path, self.paths,
                               self.imports, module_path)
                
                self.imports[abs_path] = module
                
                if import_names:
                    container.attributes[piece] = module
                
                container = module
            
            else:
                return None
    
    def package(self):
    
        try:
            # The attribute must refer to a string constant.
            return self.attributes["__package__"].s
        except KeyError:
            return self.default_package
    
    def find_object(self, name, in_superclass = False, in_parent = True):
    
        """Find and return the object with given name in the module. The
        optional parameters are ignored for modules."""
        
        # Navigate the path components individually.
        at = name.find(".")
        if at != -1:
        
            parent = self.find_object(name[:at])
            if parent is None:
                return None
            child = parent.find_object(name[at+1:])
            return child
        
        # Check for a deferred name.
        if name in self.deferred:
        
            obj = self.deferred[name]
            obj = obj.resolve()
            
            if isinstance(obj, ClassVisitor):
                self.classes[name] = obj
                self.class_order.append(name)
            else:
                self.attributes[name] = obj
            
            # Remove the name from the dictionary to prevent it from being
            # processed again.
            del self.deferred[name]
        
        # Now check whether the name exists in the classes or attributes lists.
        if name in self.attributes:
        
            module = self.attributes[name].resolve()
            self.attributes[name] = module
            return module
        
        elif name in self.classes:
        
            class_ = self.classes[name]
            if isinstance(class_, ClassVisitor):
                if not class_.visited:
                    for child in class_.code:
                        class_.visit(child)
                    class_.visited = True
            
            return class_
        else:
            return None
    
    def resolve(self):
    
        if self.placeholder:
        
            self.placeholder = False
            
            if os.path.isdir(self.file_name_):
                file_name = os.path.join(self.file_name_, "__init__.py")
            elif os.path.isfile(self.file_name_):
                file_name = self.file_name_
            else:
                file_name = self.file_name_ + ".py"
            
            if os.path.isfile(file_name):
                self.load(file_name, self.imports)
        
        return self


class ClassVisitor(Visitor):

    def __init__(self, name, bases, code, parent, module, visit = True, indent = 0):
    
        self.name_ = name
        self.bases = bases
        self.code = code
        self.parent = parent
        self.module = module
        self.indent = indent
        self.attributes = {}
        self.fields = {}
        self.methods = {}
        self.method_names = []
        self.classes = {}
        self.class_order = []
        
        self.parameters_ = []
        self.item_types_ = []
        self.replace_ = []
        self.replacement_types = {}
        self.specialised = False
        self.specialises = None
        
        # Cached attributes
        self.interfaces_ = None
        
        self.visited = visit
        
        if visit:
            for child in self.code:
                self.visit(child)
    
    def __repr__(self):
    
        return "ClassVisitor(%s, %s)" % (repr(self.name()), self.bases)
    
    def __str__(self):
    
        name = self.name()
        
        # Remove the name extension that specialised classes are given.
        if self.replacement_types:
            name = name[:name.rfind("$")]
        
        return name
    
    def size(self):
        return 1
    
    def copy(self):
    
        class_ = ClassVisitor(self.name_, self.bases, self.code, self.parent,
                              self.module, True, self.indent)
        class_.specialises = self
        return class_
    
    def visitAssign(self, node):
    
        value = self.simplify(self.visit(node.value))
        
        for target in node.targets:
            name = self.visit(target).id
            if name == "__fields__":
                for key, type_ in self.simplify(value).items():
                    self.fields[key] = Field(self, type_, key)
            elif name == "__static_fields__":
                for key, type_ in self.simplify(value).items():
                    self.fields[key] = StaticField(self, type_, key)
            elif name == "__parameters__":
                for name in self.simplify(value):
                    self.parameters_.append(name.id)
            elif name == "__item_types__":
                for name in self.simplify(value):
                    self.item_types_.append(self.simplify(name))
            elif name == "__replace__":
                for name in self.simplify(value):
                    self.replace_.append(name.id)
            else:
                self.attributes[name] = value
    
    def visitCall(self, node):
    
        # Calls are limited within class bodies. Only fixed objects can be
        # called and their arguments must be constants.
        if not isinstance(node.func, ast.Name):
            raise VisitorError("Only fixed named objects can be called from "
                "within class bodies at line %i of file %s." % (node.lineno,
                self.module.file_name()))
        
        return node
    
    def visitClassDef(self, node):
    
        bases = []
        for child in node.bases:
            bases.append(self.visitBase(child))
        
        class_visitor = ClassVisitor(unicode(node.name), bases, node.body,
                                     self, self.module, indent = self.indent)
        
        self.classes[node.name] = class_visitor
        self.class_order.append(node.name)
    
    def visitFunctionDef(self, node):
    
        # Unpack decorators into a dictionary.
        decorators = {}
        if node.decorator_list:
            for decorator in node.decorator_list:
                value = self.visit(decorator)
                if isinstance(value, ast.Call):
                    decorators[value.func.id] = value.args
                else:
                    decorators[value.id] = True
        
        # Automatically make methods of interface classes abstract.
        if not self.bases and "abstract" not in decorators:
            decorators["abstract"] = True
        
        method_visitor = Method(node.name,
            map(lambda arg: arg.id, node.args.args),
            node.args.defaults, decorators, node.body, self, node.lineno,
            indent = self.indent)
        
        methods = self.methods.setdefault(node.name, [])
        methods.append(method_visitor)
        if node.name not in self.method_names:
            self.method_names.append(node.name)
    
    def visitList(self, node):
    
        nodes = []
        for child in node.elts:
            nodes.append(self.visit(child))
        
        return nodes
    
    def visitTuple(self, node):
    
        nodes = []
        for child in node.elts:
            nodes.append(self.visit(child))
        
        return tuple(nodes)
    
    def visitUnaryOp(self, node):
    
        # In a class definition, the only valid use for a unary operator is
        # with a constant.
        if not isinstance(node.operand, ast.Num):
            raise VisitorError("Only numeric constants can be used within class bodies.")
        
        return node
    
    def static(self):
    
        try:
            return self.attributes["__static__"].name == "True"
        except KeyError:
            return False
    
    def final(self):
    
        try:
            return self.attributes["__final__"].name == "True"
        except KeyError:
            return False
    
    def annotations(self):
    
        try:
            return self.attributes["__annotations__"]
        except KeyError:
            return []
    
    def interfaces(self):
    
        if self.interfaces_ != None:
            return self.interfaces_
        
        try:
            self.interfaces_ = map(self.simplify, self.attributes["__interfaces__"])
            return self.interfaces_
        except KeyError:
            return []
    
    def package(self):
    
        try:
            # The attribute must refer to a constant string.
            return self.attributes["__package__"].s
        except KeyError:
            return self.module.package()
    
    def item_types(self):
    
        # The attribute must refer to a list of class references or names.
        item_types = []
        for item_type in self.item_types_:
            if isinstance(item_type, ClassReference):
                item_types.append(item_type.dereference())
            else:
                item_types.append(item_type)
        
        return item_types
    
    def item_type_names(self):
    
        # The attribute must refer to a list of class references or names.
        names = []
        for item_type in self.item_types_:
            if isinstance(item_type, ClassReference):
                names.append(item_type.value)
            else:
                names.append(item_type.name)
        
        return names
    
    def has_item_types(self):
    
        return self.item_types_ != []
    
    def has_parameters(self):
    
        return self.parameters_ != []
    
    def name(self):
    
        name = self.name_
        
        if isinstance(self.parent, ClassVisitor):
            return self.parent.name() + "." + name
        else:
            return name
    
    def full_name(self):
    
        package = self.package().replace(".", "/")
        
        if isinstance(self.parent, ClassVisitor):
            return package + "/" + self.parent.name_ + "$" + self.name_
        else:
            return package + "/" + self.name_
    
    def unique_name(self):
    
        name = self.full_name()
        
        if self.has_item_types():
            name += "<" + ",".join(map(str, self.item_types_)) + ">"
        
        return name
    
    def superclass(self):
    
        # There should only be at most one base class.
        if self.bases:
            return self.module.find_object(self.bases[0])
        else:
            return None
    
    def ancestors(self):
    
        if self.bases:
            base = self.module.find_object(self.bases[0])
            return [base] + base.ancestors()
        else:
            return []
    
    def find_object(self, name, in_superclass = False, in_parent = True):
    
        if name in self.classes:
            return self.classes[name]
        
        elif name in self.methods:
            # Although there may be more than one method with the same name,
            # only return one of them. Anything that needs a specific method
            # can call find_method to get it.
            return Methods(self, self.methods[name])
        
        elif name in self.attributes:
            return self.attributes[name]
        
        elif name in self.fields:
            return self.fields[name]
        
        if in_superclass and self.bases:
            base = self.module.find_object(self.bases[0])
            if base:
                obj = base.find_object(name, in_superclass, in_parent)
                if obj:
                    return obj
            else:
                raise TypeError, "Failed to find the base class '%s' of '%s'." % (
                    self.bases[0], self.name())
        
        elif in_parent:
            return self.module.find_object(name)
        
        for interface in self.interfaces():
            interface_obj = interface.dereference()
            if interface_obj:
                obj = interface_obj.find_object(name, in_superclass = True,
                                                      in_parent = False)
                if obj:
                    return obj
        
        return None
    
    def find_methods(self, name, only_in_this_class = False):
    
        # Yield any methods in the class with the appropriate name.
        for method in self.methods.get(name, []):
            yield method
        
        if only_in_this_class:
            return
        
        # Yield any matching methods in the base class.
        if self.bases:
            base = self.module.find_object(self.bases[0])
            if base:
                for method in base.find_methods(name):
                    yield method
            else:
                raise TypeError, "Failed to find the base class '%s' of '%s'." % (
                    self.bases[0], self.name())
        
        # Yield any matching methods in any of the interfaces.
        for interface in self.interfaces():
            interface_obj = interface.dereference()
            if interface_obj:
                for method in interface_obj.find_methods(name):
                    yield method
    
    def inherits(self, class_):
    
        if self == class_:
            return True
        
        if self.specialises == class_:
            return True
        
        # Only check the first base class.
        for base in self.bases:
            base_class = self.module.find_object(base)
            if base_class is None:
                raise TypeError, "Failed to find the base class '%s' of '%s'." % (
                    base, self.name())
            return base_class.inherits(class_)
        
        return False
    
    def implements(self, class_):
    
        # If the class is found in the list of interfaces then return True.
        # Since some interfaces implement other interfaces, check each
        # interface to determine whether they implement the interface described
        # by the class.
        for interface in self.interfaces():
            obj = interface.dereference()
            if obj == class_:
                return True
            elif obj.implements(class_):
                return True
        
        # Inherit interfaces from the first base class.
        for base in self.bases:
            base_class = self.module.find_object(base)
            if base_class is None:
                raise TypeError, "Failed to find the base class '%s' of '%s'." % (
                    base, self.name())
            return base_class.implements(class_)
        
        return False
    
    def external(self, module):
    
        return self.package() != module.package()
    
    def is_interface(self):
    
        # Return True if this class has no base classes.
        return self.bases == []
    
    def parameters(self):
    
        if self.parameters_:
            return self.parameters_
        
        # Search for defined parameters in the base class.
        for name in self.bases[:1]:
            base = self.module.find_object(name)
            if base:
                pars = base.parameters()
                if pars:
                    return pars
        
        # Search for defined parameters in the interfaces.
        for interface in self.interfaces():
            interface_obj = interface.dereference()
            if interface_obj:
                pars = interface_obj.parameters()
                if pars:
                    return pars
        return []
    
    def file_name(self):
        return self.module.file_name()


class Method(Visitor):

    def __init__(self, name, argnames, defaults, decorators, code, class_,
                       line_number, indent = 0):
    
        self.name_ = name
        self.argnames = argnames
        self.defaults = defaults
        self.decorators = decorators
        self.code = code
        self.parent = self.class_ = class_
        self.specialised = False
        self.line_number = line_number
        self.indent = indent
        
        # Cached attributes
        self.parameters_ = None     # template parameters (E, T, P, Q, etc.)
    
    def __repr__(self):
    
        return "Method(%s, %s, %s, %s, %s)" % (repr(self.name_), self.argnames,
            self.defaults, self.decorators, self.parent.name())
    
    def static(self):
    
        return "static" in self.decorators
    
    def virtual(self):
    
        return "virtual" in self.decorators
    
    def abstract(self):
    
        return "abstract" in self.decorators or self.class_.is_interface()
    
    def final(self):
    
        return "final" in self.decorators
    
    def prototype(self):
    
        try:
            return_type, method_params = self.decorators["args"]
        except KeyError:
        
            # Find a suitable equivalent in a base class or interface.
            for method in self.class_.find_methods(self.name_):
            
                if method == self:
                    continue
                elif method.static() == self.static() and \
                     len(method.argnames) == len(self.argnames):
                    return method.prototype()
            
            return [ast.Name("void", ast.Load()), []]
        
        return_type = self.simplify(return_type)
        method_params = self.simplify(method_params)
        
        if type(method_params) != list:
            raise TypeError("Expected a list of method parameters for '%s' at "
                "line %i of file %s." % (self.name_, self.line_number,
                self.file_name()))
        
        #self.decorators["args"] = (return_type, method_params)
        
        # Compare the number of declared types against the number of declared
        # argument names, compensating for the extra instance argument in
        # non-static methods.
        
        args = len(self.argnames)
        if not self.static():
            args -= 1
        
        if len(method_params) != args:
            raise TypeError(
                "The number of method parameters for '%s' differs from the number "
                "specified in the prototype at line %i of file %s." % (self.name_,
                self.line_number, self.file_name())
                )
        
        return return_type, method_params
    
    def permissions(self):
    
        permissions = []
        for name in self.decorators.get("permissions", []):
            # Each of the permissions is given as an object reference, but it
            # is really just a string constant.
            permissions.append(self.simplify(name).value)
        
        return permissions
    
    def api_levels(self):
    
        api_levels = []
        for value in self.decorators.get("api", []):
            # The decorator defines a series of integers representing the
            # API levels at which the method was introduced and deprecated.
            api_levels.append(self.simplify(value).value)
        
        return api_levels
    
    def return_type(self):
    
        t = self.prototype()[0]
        if isinstance(t, ast.Name):
            return t.id
        else:
            ### Perhaps we should simplify the return type.
            return t
    
    def find_object(self, name):
        return self.class_.find_object(name)
    
    def name(self):
    
        if self.name_ == u"__init__":
            return u"<init>"
        else:
            # Return the unchanged name unless it has trailing underscores, in
            # which case remove them first. It might be necessary to make this
            # stricter if we start supporting more magic methods.
            return self.name_.rstrip("_")
    
    def parameters(self):
        if self.parameters_ == None:
            self.parameters_ = self.class_.parameters()
        
        return self.parameters_
    
    def full_name(self):
        return self.class_.full_name() + "." + self.name()
    
    def file_name(self):
        return self.class_.file_name()
    
    def simplify(self, node):
    
        if isinstance(node, ast.Name):
            if node.id in self.parameters():
                return Parameter(node.id)
            elif node.id in self.class_.replace_:
                return Parameter(node.id)
        
        return Visitor.simplify(self, node)


class DeferredName:

    def __init__(self, module, name, import_node, parent):
    
        self.module = module
        self.name = name
        self.import_node = import_node
        self.parent = parent
    
    def __repr__(self):
        return "DeferredName(%s, %s, %s)" % (repr(self.module),
            repr(self.name), repr(self.parent))
    
    def resolve(self):
    
        # Resolve the deferred module that hosts this class.
        module = self.module.resolve()
        
        # The object retrieved from the module might have been imported
        # from another module.
        if self.name in module.deferred:
            return module.deferred[self.name].resolve()
        elif self.name in module.classes:
            return module.classes[self.name]
        elif self.name in module.attributes:
            return module.attributes[self.name]
        else:
            raise VisitorError, "Failed to import class '%s' at line %i in '%s'." % (
                self.name, self.import_node.lineno, self.parent.file_name_)


# Classes describing special objects

class Methods:

    def __init__(self, container_obj, methods):
    
        self.container_obj = container_obj
        self.methods = methods
    
    def __repr__(self):
    
        return "Methods(%s, %s)" % (repr(self.container_obj), repr(self.methods))

class ClassReference:

    def __init__(self, value, scope, node, file_name):
    
        self.value = value
        self.scope = scope
        self.node = node
        self.file_name = file_name
    
    def __repr__(self):
    
        return "ClassReference(%s)" % repr(self.value)
    
    def __str__(self):
    
        # This ensures that the class is dereferenced before it is used to
        # create part of a specialised class name.
        return str(self.dereference())
    
    def dereference(self):
    
        obj = self.scope.find_object(self.value)
        if obj is None:
            raise VisitorError("Could not find '%s' referred to in %s in %s on line %i." % (
                self.value, self.scope.name(), self.file_name, self.node.lineno))
        else:
            return obj

class Field:

    def __init__(self, class_, type_, name):
    
        self.class_ = class_
        self.type_ = type_
        self.name = name

class StaticField(Field):

    pass

class Parameter:

    def __init__(self, name):
        self.name = name
        self.args = None
    
    def __repr__(self):
        if self.args:
            return "Parameter(%s(%s))" % (self.name, ",".join(map(str, self.args)))
        else:
            return "Parameter(%s)" % repr(self.name)
    
    def __str__(self):
        if self.args:
            return "%s(%s)" % (self.name, ",".join(map(str, self.args)))
        else:
            return self.name
    
    def __hash__(self):
        return hash(str(self))
    
    def __cmp__(self, other):
        if str(self) == str(other):
            return 0
        else:
            return 1

def repr_ast(obj):

    # Because providing repr support in the ast module was apparently too much.
    if isinstance(obj, ast.Name):
        return "ast.Name(%s)" % repr_ast(obj.id)
    elif isinstance(obj, ast.Call):
        return "ast.Call(%s, %s)" % (repr_ast(obj.func), repr_ast(obj.args))
    elif isinstance(obj, ast.List):
        return "ast.List(%s)" % repr_ast(obj.elts)
    elif isinstance(obj, list):
        return "[" + ", ".join(map(repr_ast, obj)) + "]"
    elif isinstance(obj, dict):
        return "{" + ", ".join(
            map(lambda (k, v): repr_ast(k) + ": " + repr_ast(v), obj.items())
            ) + "}"
    else:
        return repr(obj)


def main():

    import sys
    import readline, rlcompleter
    readline.parse_and_bind("tab:complete")
    
    if len(sys.argv) < 2:
        sys.stderr.write("Usage: python -i %s <module file> [<include path> ...]\n" % sys.argv[0])
        sys.exit(1)
    
    if len(sys.argv) < 3:
        paths = ["Include"]
    else:
        paths = sys.argv[2:]
    
    v = Visitor(paths)
    module = v.load(sys.argv[1])
    print "v is the visitor instance."
    print "module is the root module."
