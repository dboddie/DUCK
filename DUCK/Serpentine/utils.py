"""
Copyright (C) 2017 David Boddie <david@boddie.org.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import ast

from Dalvik.dextypes import *

from arrays import *
from common import *
from macros import can_cast_primitive, library_type_casts
import visitor

class Utils:

    def __init__(self):
    
        # Maintain dictionaries of specialised classes and methods.
        self.specialised_classes = {}
        self.specialised = {}
    
    def create_value_type(self, value):
    
        """Returns a type for the value. This can be a visitor Node object or a
        type that has already been established."""
        
        if is_placeholder(value):
            value = value.class_
        
        # If a named object was passed then translate that to the corresponding
        # value type and create an instance of it.
        if isinstance(value, ast.Name):
            return type_name_to_type_map[value.id]
        
        elif isinstance(value, ast.Call):
        
            if isinstance(value.func, visitor.Parameter):
                return value
            
            elif isinstance(value.func, visitor.ClassReference):
            
                # A template type containing an item type. Just return the name of the
                # template type, but include information about the item type.
                obj = value.func.dereference()
                
                item_types = []
                
                for item_type in value.args:
                    item_types.append(self.create_value_type(item_type))
                
                class_ = TemplateClass(obj, item_types)
                
                if len(obj.parameters()) != len(item_types):
                    ### Report incomplete definitions. We should also try to include
                    ### file information.
                    raise TypeError("Incorrect number of item types (%i) specified in "
                        "template class declaration %s at line %i. Expected %i item "
                        "types." % (len(item_types), class_, value.lineno, len(obj.parameters())))
                
                return class_
            else:
                raise TypeError("Unhandled type '%s'." % value)
        
        elif isinstance(value, visitor.ClassReference):
            return value.dereference()
        
        elif type(value) == list:
        
            dim, item_type = get_array_dimensions_and_item_type(value)
            item_type = self.create_value_type(item_type)
            element_type = create_dex_type(item_type)
            # Incorporate the type description for each element into the array
            # description.
            return ArrayContainer(dim, item_type, Array(("[" * dim) + element_type.desc))
        
        elif is_primitive(value):
            return value
        
        elif is_class(value):
            return value
        
        elif is_array(value):
            return value
        
        elif isinstance(value, visitor.Parameter):
            # Try to find the corresponding type in the default map. Otherwise,
            # just use the type for generic Object elements.
            if value.name in type_name_to_type_map:
                return type_name_to_type_map[value.name]
            else:
                return type_name_to_type_map["E"]
        
        else:
            raise TypeError("Unhandled type '%s'." % value)
    
    
    def create_prototype(self, method):
    
        """Creates a Prototype object from the list of types containing the
        return value and the parameters of a method."""
        
        prototype_list = method.prototype()
        module = method.class_.module
        
        return_type, parameters = prototype_list
        return_type = self.create_value_type(return_type)
        dex_return_type = create_dex_type(return_type)
        
        # Register types used in templates and arrays.
        if isinstance(return_type, TemplateClass):
        
            # Items should be Object subclasses.
            for item_type in return_type.item_types_:
                self.dex_objects.types.add(create_dex_type(item_type))
        
        elif is_array(return_type) and not is_primitive(return_type.item_type):
        
            # Only register Object subclasses.
            self.dex_objects.types.add(create_dex_type(return_type.item_type))
        
        params = []
        for parameter in parameters:
        
            par_type = self.create_value_type(parameter)
            dex_par_type = create_dex_type(par_type)
            params.append(dex_par_type)
            
            if isinstance(par_type, TemplateClass):
            
                # Items should be Object subclasses.
                for item_type in par_type.item_types_:
                    self.dex_objects.types.add(create_dex_type(item_type))
        
            elif is_array(par_type) and not is_primitive(par_type.item_type):
            
                # Only register Object subclasses.
                self.dex_objects.types.add(create_dex_type(par_type.item_type))
        
        shorty = []
        for type_ in [dex_return_type] + params:
        
            if type_.desc in ShortDescriptors:
                shorty.append(type_)
            else:
                shorty.append(dReferenceType)
        
        return Prototype(shorty, dex_return_type, params)
    
    
    def value_type_value_from_value(self, value):
    
        if isinstance(value, ast.Name):
            type_ = value_name_to_type_map[value.id]
            value_ = value_name_to_value_map[value.id]
        
        elif type(value) == list or type(value) == tuple:
            # Find the type and value of the first element.
            element_type, element_value = self.value_type_value_from_value(value[0])
            
            type_ = Array("[" + element_type.desc)
            value_ = value
        
        else:
            # The value must be a constant.
            type_, value_ = value_to_type(value.value)
            
            if type_.__class__ != visitor.ClassVisitor:
                # Create an instance of the type using the value held by the constant.
                value_ = type_.__class__(value_)
        
        return type_, value_
    
    
    # Convenience functions for handling primitive values wrapped in Objects.
    
    def get_wrapper_type(self, type_):
    
        # If the type is a raw name from the parser then resolve its type. This is
        # the case for primitive types.
        if isinstance(type_, ast.Name):
            type_ = self.create_value_type(type_)
        
        # Return the wrapper for the type.
        
        if is_primitive(type_) and not isinstance(type_, Char):
            return self.dex_objects.wrapper_types[type_.__class__]
        
        elif is_array(type_):
        
            item_type = self.get_wrapper_type(type_.item_type)
            dex_type = create_dex_type(item_type)
            
            return ArrayContainer(type_.dimensions, item_type,
                    Array(("[" * type_.dimensions) + dex_type.desc),
                    wrapped = True)
        else:
            return type_
    
    
    def get_unwrapped_type(self, type_):
    
        if isinstance(type_, visitor.ClassVisitor):
            type_ = self.dex_objects.unwrapped_types.get(type_)
            
            # If a valid unwrapped primitive type was found then instantiate it.
            if type_:
                type_ = type_()
        
        # Return the original type, the unwrapped type or None.
        return type_
    
    
    def wrap_item(self, node, item, context, scope):
    
        if not is_primitive(item.type_):
            return item
        
        object_type = self.dex_objects.wrapper_types[item.type_.__class__]
        return self.create_object(node, object_type, [item], context, scope)
    
    
    # Type compatibility functions
    
    def compatible_types(self, proposed, constraint, add_missing = False):
    
        """Returns a suitable compatible type based on the proposed type for the
        given constraint type, or None if no compatible type exists. It is up to
        the caller to check whether the returned type differs from the proposed
        type and perform suitable casting operations where appropriate."""
        
        # If the arguments refer to classes with additional item type information
        # then make them full template classes for the purpose of comparison.
        
        if isinstance(proposed, visitor.ClassVisitor) and proposed.has_item_types():
        
            item_types = self.get_item_types(proposed)
            proposed = TemplateClass(proposed, item_types)
        
        if isinstance(constraint, visitor.ClassVisitor) and constraint.has_item_types():
        
            item_types = self.get_item_types(constraint)
            constraint = TemplateClass(constraint, item_types)
        
        if is_placeholder(constraint):
            constraint = constraint.class_
        
        # Now compare the types for compatibility.
        
        if isinstance(constraint, TemplateClass):
        
            # Allow null values to be passed instead of instances of template classes.
            if isinstance(proposed, Null):
                return proposed
            
            if isinstance(proposed, visitor.ClassVisitor) and proposed.specialises:
                # If the proposed type is a specialised class then extract its
                # replacement types.
                item_types = []
                for p in proposed.replace_:
                    item_types.append(proposed.replacement_types[p])
            
            elif not isinstance(proposed, TemplateClass):
                # If the proposed type is not a template class then fail.
                return None
            
            else:
                # If the proposed type is a container class then extract its item
                # types.
                item_types = proposed.item_types_
            
            # Check the compatibility of the actual container classes.
            if not proposed.container_class.inherits(constraint.container_class) and \
               not proposed.container_class.implements(constraint.container_class):
                return None
            
            # Check the item types of the two template classes.
            for i, (ptype, ctype) in enumerate(zip(item_types, constraint.item_types_)):
            
                # If the proposed item types are not fully defined, fill in the
                # definitions if required
                if ptype is None and add_missing:
                    ptype = ctype
                    item_types[i] = ptype
                
                # Short-circuit item types that are identical in order to prevent
                # unnecessary recursion and disallow item types that are not
                # identical - this simplifies handling of containers.
                if ptype == ctype:
                    continue
                elif is_primitive(ctype):
                    return None
                
                if not self.compatible_types(ptype, ctype):
                    return None
            
            return proposed
        
        elif is_array(constraint):
        
            # Allow null values to be passed instead of arrays.
            if isinstance(proposed, Null):
                return proposed
            
            # If the proposed type is not an array then fail immediately.
            if not is_array(proposed):
                return None
            
            # Check the item types of the two arrays.
            if proposed.item_type == constraint.item_type:
                return proposed
            
            # If the item types are compatible then return the constraint type so
            # that the caller knows they are compatible but not an exact match.
            if self.compatible_types(proposed.item_type, constraint.item_type):
                return constraint
        
        elif isinstance(constraint, visitor.ClassVisitor):
        
            # Allow null values to be passed instead of instances.
            if isinstance(proposed, Null):
                return proposed
            
            # If the proposed type is not also a class then fail immediately unless
            # the argument can be converted to the constraint type.
            if not isinstance(proposed, visitor.ClassVisitor):
                proposed = self.get_wrapper_type(proposed)
            
            # Obtain the actual class if the proposed type is a template.
            if isinstance(proposed, TemplateClass):
                proposed = proposed.container_class
            
            if not isinstance(proposed, visitor.ClassVisitor):
                return None
            
            # Examine the inheritance and interfaces of the proposed type for
            # compatibility with the constraint type.
            
            if proposed.inherits(constraint) or proposed.implements(constraint):
                # The proposed type is more specialised than the constraint type.
                return proposed
            
            elif is_placeholder(proposed) and \
                 (constraint.inherits(proposed) or constraint.implements(proposed)):
                # The proposed type is less specialised than the constraint type
                # but can be cast to a compatible type.
                return constraint
        
        elif is_primitive(constraint):
        
            # If the proposed type is not also a value type then get its
            # corresponding primitive type, if any.
            if not is_primitive(proposed):
                proposed = self.get_unwrapped_type(proposed)
            
            if proposed == None:
                return None
            
            if proposed == constraint:
                return proposed
            
            elif can_cast_primitive(proposed, constraint):
                return constraint
            
            ### Restrict library casts to primitive types.
            ### Proposed template classes will be processed incorrectly if the
            ### constraint type has a potential library method to use.
            elif is_primitive(proposed) and can_cast_with_library(constraint):
                return constraint
        
        elif isinstance(constraint, visitor.Parameter):
        
            ### We should check whether the parameter requires a container and
            ### ensure that the proposed type qualifies.
            return proposed
        
        elif type(constraint) == list:
        
            # If the proposed type is not an array then fail immediately.
            if not is_array(proposed):
                return None
            
            # Check the item types of the two arrays.
            if not self.compatible_types(proposed.item_type, constraint[0]):
                return None
            
            return proposed
        
        elif isinstance(constraint, ast.Call) and \
            isinstance(constraint.func, visitor.ClassReference):
        
            # A parameterised template type.
            obj = constraint.func.dereference()
            
            # Check compatibility of the container type.
            if not self.compatible_types(proposed, obj):
                return None
            
            # Check the number of arguments.
            if len(proposed.item_types_) != len(constraint.args):
                return None
            
            # Check compatibility of the item types.
            for arg_type, par_type in zip(proposed.item_types_, constraint.args):
                if not self.compatible_types(arg_type, par_type):
                    return None
            
            return proposed
        
        return None
    
    
    def get_item_types(self, container_type):
    
        if isinstance(container_type, TemplateClass):
            # The item type is included in the container type.
            return container_type.item_types_
        else:
            # Obtain the specialised class.
            class_ = container_type
            
            # Find the item type from the specialised class.
            item_types = map(self.create_value_type, class_.item_types())
            
            if not item_types:
                if class_.has_parameters():
                    # An unspecialised template class.
                    return [type_name_to_type_map["E"]] * len(class_.parameters())
                else:
                    raise TypeError("No item types specified in class '%s'" % class_.name())
            
            return item_types
    
    
    def resolve_type(self, declared_type, container_type, template_params,
                     default_type):
    
        """Resolve and return a type for the declared type node (or structure
        containing nodes) associated with the container type with template
        parameters, returning a default type if no resolution is required."""
        
        ### We should check whether the callee's container object is a
        ### container before handling placeholders.
        ### We need to handle parameter definitions in specialised classes.
        
        if isinstance(declared_type, visitor.Parameter):
        
            # The type is a parameter type of the container type we are accessing.
            if is_specialised(container_type):
                item_type = container_type.replacement_types[declared_type.name]
            else:
                par_index = template_params.index(declared_type.name)
                item_type = self.get_item_types(container_type)[par_index]
            
            return item_type
        
        elif is_specialised(container_type):
        
            definitions = {}
            type_ = self.substitute_parameters(container_type,
                container_type.replacement_types, declared_type, definitions)
            return type_
        
        elif is_container(default_type):
        
            # If the type is a container then check its item types to determine
            # whether it should be updated with specialised types.
            return self.resolve_type_from_container(default_type, container_type)
        
        elif is_array(default_type):
        
            # The declared type will be a list containing one element (e.g. [int])
            # or an ArrayContainer instance.
            if is_array(declared_type):
                declared_item_type = declared_type.item_type
            else:
                declared_item_type = declared_type[0]
            
            item_type = self.resolve_type(declared_item_type, container_type,
                                          template_params, default_type.item_type)
            
            if item_type != default_type.item_type:
            
                # The resolved type is different, so it must have contained a
                # parameter.
                
                if is_specialised(container_type):
                    # Specialised classes can contain primitive types, so indicate
                    # that no unwrapping will be needed.
                    wrapped = False
                else:
                    # Since primitive types must be wrapped before being used as
                    # parameter types in regular template classes, the array will
                    # contain wrapped objects (e.g. Integer). We won't unwrap all
                    # the elements when dealing with these, so indicate that the
                    # items are wrapped.
                    wrapped = True
                
                element_type = create_dex_type(item_type)
                
                type_ = ArrayContainer(default_type.dimensions, item_type,
                    Array(("[" * default_type.dimensions) + element_type.desc),
                    wrapped = wrapped)
                
                return type_
        
        return default_type
    
    
    def resolve_type_from_container(self, type_, container_type):
    
        # If any of the item types is a placeholder then find its real item type.
        for i, item_type in enumerate(self.get_item_types(type_)):
        
            if is_placeholder(item_type) and \
               isinstance(item_type.class_, visitor.ClassVisitor):
                container_item_type = self.get_item_types(container_type)[i]
            
            elif isinstance(item_type, TemplateClass):
                container_item_type = self.resolve_type_from_container(
                    item_type, container_type)
            else:
                continue
            
            # Failed to fill in the item type from the container's item types.
            if container_item_type is None:
                return None
            
            type_.item_types_[i] = container_item_type
        
        return type_
    
    
    def get_parameter_types(self, par, arg_type, defined = None):
    
        """Return a dictionary containing parameter definitions extracted from the
        given method parameter and argument type. For example, if the parameter in
        a method declaration is List(E) and the argument type is List(String) then
        the dictionary returned should contain {"E": String}."""
        
        if defined is None:
            defined = {}
        
        # Nested structures in nodes require nested calls to this method.
        # Once we encounter a Parameter then we can store the corresponding
        # type supplied in the argument in the dictionary of definitions.
        
        if isinstance(par, ast.Call):
        
            if isinstance(arg_type, TemplateClass):
            
                self.get_parameter_types(par.func, arg_type.container_class, defined)
                
                for par_item_type, arg_item_type in zip(par.args, arg_type.item_types_):
                    self.get_parameter_types(par_item_type, arg_item_type, defined)
        
        elif type(par) == list and is_array(arg_type):
        
            par_dim, par_item_type = get_array_dimensions_and_item_type(par)
            
            if arg_type.dimensions == par_dim:
                self.get_parameter_types(par_item_type, arg_type.item_type, defined)
        
        elif isinstance(par, visitor.Parameter):
        
            ### Check for compatibility.
            if par.name not in defined:
                if par.args and isinstance(arg_type, TemplateClass):
                    defined[par.name] = arg_type.container_class
                    for par_item_type, arg_item_type in zip(par.args, arg_type.item_types_):
                        self.get_parameter_types(par_item_type, arg_item_type, defined)
                else:
                    defined[par.name] = arg_type
        
        return defined
    
    
    def get_specialised_class_method(self, method, arg_types, obj, specialised_types):
    
        if not method.class_.replacement_types:
        
            # A class with no replacement types is a pure template class.
            # Create a copy that we can specialise.
            class_ = method.class_.copy()
            
            # Find the corresponding method in the copy of the class.
            for found in class_.find_methods(method.name_, True):
            
                if found.decorators == method.decorators:
                
                    if specialised_types:
                        # Specialise the method and obtain a dictionary of the
                        # definitions used to do so.
                        defined = self.specialise(found, arg_types)
                    else:
                        # No specialised types were passed to the method so
                        # there is no need to specialise it any further.
                        defined = {}
                    
                    # If None was returned try the next method. Otherwise, we
                    # break out of the loop and register the method.
                    if defined is not None:
                        break
            else:
                # If specialisation fails, return None to allow other possible
                # methods to be specialised.
                return None
            
            # Register the specialised method and its parent class as required.
            found = self.register_specialised_method(found, class_, method, defined)
        
        else:
            # Otherwise, just use the class supplied.
            class_ = method.class_
            if self.specialise(method, arg_types) == None:
                # If specialisation fails, return None to allow other possible
                # methods to be specialised.
                return None
            
            found = method
        
        found.specialised = True
        return found
    
    def register_specialised_method(self, found, class_, method, defined):
    
        # Check for an existing specialised method that corresponds to the
        # one found.
        
        spec_key = self.make_spec_key(method, class_, defined)
        
        # Either use an existing method or register the one just created
        # along with the copy of the original class.
        if spec_key in self.specialised:
            found = self.specialised[spec_key]
        else:
            self.specialised[spec_key] = found
            
            # Try to reuse an existing specialised class to host the method.
            new_name, class_key = self.make_class_spec_key(class_, defined)
            
            if class_key in self.specialised_classes:
                class_ = self.specialised_classes[class_key]
                # Copy the method to the existing specialised class and inform
                # the method of its new parent.
                class_.methods[method.name_].append(found)
                found.parent = found.class_ = class_
            else:
                # Register the specialised class containing the method.
                class_.specialised = True
                class_.name_ = new_name
                self.specialised_classes[class_key] = class_
        
        return found
    
    def make_spec_key(self, method, class_, defined):
    
        defs = []
        for key in method.class_.replace_:
            if key in defined:
                defs.append(str(defined[key].unique_name()))
            else:
                defs.append("None")
        
        return (method, tuple(defs))
    
    def make_class_spec_key(self, class_, defined):
    
        defs = []
        for key in class_.replace_:
            if key in defined:
                defs.append(str(defined[key].unique_name()))
            else:
                defs.append("None")
        
        extra = "$"
        if defined != {}:
            extra += self.extra_special(defs)
        
        return class_.name_ + extra, (class_.full_name() + extra, tuple(defs))
    
    def specialise(self, method, arg_types):
    
        """Specialise the method passed, using the argument types to define any
        parameters used in the method's argument parameter definitions. Returns
        None if specialisation fails; otherwise returns a dictionary of
        definitions, which can be empty."""
        
        return_type, params = method.prototype()
        defined = {}
        
        for i, (type_, par) in enumerate(zip(arg_types, params)):
        
            par_type = self.specialise_argument(method.class_, method, par,
                                                type_, defined)
            if par_type == None:
                return None
            
            params[i] = par_type
        
        # If the return type is a parameter that has already been specialised
        # then update it with the specialised type.
        return_type = self.substitute_parameters(method.class_,
            method.class_.replacement_types, return_type, defined)
        
        method.decorators["args"] = (return_type, params)
        
        # Return the dictionary containing the definitions made while
        # specialising the method.
        return defined
    
    def specialise_argument(self, class_, method, par, type_, defined):
    
        if isinstance(par, visitor.Parameter):
        
            if not self.define_specialised_type(class_, method, par, type_, defined):
                # Specialisation failed, so return None to indicate this.
                return None
            
            return type_
        
        elif type(par) == list:
        
            if not self.define_specialised_type(class_, method, par[0],
                                                type_.item_type, defined):
                # Specialisation failed, so return None to indicate this.
                return None
            
            return type_
        
        elif isinstance(par, ast.Call) and \
             isinstance(par.func, visitor.ClassReference):
        
            par_type = par.func.dereference()
            
            if isinstance(type_, TemplateClass):
                item_types = type_.item_types_
            elif type_.replace_:
                item_types = map(lambda t: type_.replacement_types[t], type_.replace_)
            else:
                item_types = type_.item_types_
            
            if not self.compatible_types(type_, par_type):
                return None
            
            if len(par.args) != len(item_types):
                return None
            
            for i in range(len(par.args)):
                if not self.define_specialised_type(class_, method,
                    par.args[i], item_types[i], defined):
                    # Specialisation failed, so return None to indicate this.
                    return None
            
            return type_
        
        elif isinstance(par, visitor.ClassReference):
        
            ### Perhaps check for specialised classes.
            par_class = par.dereference()
            if self.compatible_types(type_, par_class) != None:
                return par_class
        
        else:
            ### Handle other forms of parameters as required.
            if self.compatible_types(type_, self.create_value_type(par)) != None:
                # Return the declared type, not the one passed.
                return self.create_value_type(par)
        
        return None
    
    def define_specialised_type(self, class_, method, par, type_, defined):
    
        """Specialise the given class_ for the parameter, par, using the type_
        specified, recording the definitions in the defined dictionary. The
        method is only used to provide context."""
        
        # Check for a conflict between the argument type and any predefined
        # replacement type for the parameter.
        if par.name in class_.replacement_types:
        
            if isinstance(type_, TemplateClass):
                check_type = type_.container_class
            else:
                check_type = type_
            
            replacement_type = class_.replacement_types[par.name]
            
            if replacement_type != check_type:
                raise TypeError, "Inconsistent types (%s and %s) for parameter %s used in %s.%s." % (
                    check_type, replacement_type, par.name, class_.name(),
                    method.name_)
        
        if isinstance(type_, TemplateClass) and par.args:
            
            # Both the parameter and type represent template classes.
            # Define types for the parameters that correspond to the item types.
            class_.replacement_types[par.name] = type_.container_class
            
            for par_arg, item_type in zip(par.args, type_.item_types_):
                if not self.specialise_argument(class_, method, par_arg, item_type, defined):
                    return False
            
            type_ = type_.container_class
        
        elif not isinstance(type_, TemplateClass) and not par.args:
        
            # Both the parameter and type are simple types. Just define the
            # type for the parameter.
            class_.replacement_types[par.name] = type_
        
        else:
            # The parameter and proposed argument type are not compatible - one
            # is complex and the other is simple.
            return False
        
        # The type was defined successfully.
        defined[par.name] = type_
        return True
    
    def substitute_parameters(self, class_, item_type_defs, parameter, definitions):
    
        """Returns the type for the parameter in the given class_ using the
        definitions supplied. A dictionary containing the mappings between
        the parameters used for item types and types is supplied in the
        item_type_defs dictionary."""
        
        # Obtain the type for the parameter.
        
        if isinstance(parameter, ast.Call) and \
            isinstance(parameter.func, visitor.ClassReference):
        
            # This represents a container. Dereference the class and record the
            # arguments to the call for specialisation.
            type_ = parameter.func.dereference()
            args = parameter.args
        
        elif type(parameter) == list:
        
            dim, item_type = get_array_dimensions_and_item_type(parameter)
            item_type = self.substitute_parameters(class_, item_type_defs,
                                                   item_type, definitions)
            element_type = create_dex_type(item_type)
            
            # Return a proper array description that can be compared against
            # other descriptions when matching method arguments.
            type_ = ArrayContainer(dim, item_type, Array(("[" * dim) + element_type.desc))
            return type_
        
        elif isinstance(parameter, visitor.Parameter):
            type_ = item_type_defs[parameter.name]
            args = parameter.args
        
        else:
            return self.create_value_type(parameter)
        
        if args:
            if type_.replace_:
                type_ = self.specialise_class(type_, definitions)
            else:
                # If the parameter represented a container class then reconstruct
                # it with the item types described by the parameter's arguments.
                item_types = []
                
                for arg in args:
                    item_types.append(self.substitute_parameters(class_,
                        item_type_defs, arg, definitions))
                
                type_ = TemplateClass(type_, item_types)
        
        definitions[parameter] = type_
        return type_
    
    def specialise_class(self, class_, defined):
    
        new_name, class_key = self.make_class_spec_key(class_, defined)
        
        if class_key in self.specialised_classes:
            return self.specialised_classes[class_key]
        
        class_ = class_.copy()
        
        for key in class_.replace_:
            if key in defined:
                class_.replacement_types[key] = defined[key]
        
        class_.specialised = True
        class_.name_ = new_name
        self.specialised_classes[class_key] = class_
        return class_
    
    def extra_special(self, defs):
    
        # Encode the specialisation information for the class using Unicode
        # code points instead of ASCII characters not allowed in SimpleNames.
        pieces = []
        for v in defs:
            v = v.replace(u"<", u"\u2329").replace(u">", u"\u232a")
            v = v.replace(u"[", u"\u228f").replace(u"]", u"\u2290")
            v = v.replace(u"(", u"\u2039").replace(u")", u"\u203a")
            v = v.replace(u" ", u"_").replace(u",", u"_")
            pieces.append(v)
        
        return u"_".join(pieces)


class DexObjects:

    """Holds the collections of objects required for the final DEX file.
    These are updated as the types, methods, fields and classes are compiled
    from the source code.
    
    Also holds state that would be annoying to pass back and forth between
    functions, such as the targets delimiting the current suite/scope."""
    
    def __init__(self):
    
        # Collections corresponding to those in the final DEX file. These will
        # be replaced by lists when the sections of the file are constructed.
        self.strings = set()
        self.types = set()
        self.prototypes = set()
        self.fields = set()
        self.methods = set()
        self.classes = {}
        
        self.processed = {}
        self.implemented = {}
        self.pending = set()
        self.pending_order = []
        
        # Maintain a stack of suites that allow break/continue.
        self.suites = []
        
        # Potentially keep information about the source for methods for when
        # a listing needs to be produced.
        self.keep_source = False
        self.method_args = {}
        self.method_vars = {}
        self.method_tree = {}
        
        # Record the methods that need permissions.
        self.permissions = {}
        
        # Record whether an __init__ call was made in a method. This should
        # help to catch cases where an __init__ method does not contain a call
        # to an __init__ method in the base class.
        self.init_call = False
        
        # Keep a reference to the builtins module.
        self.builtins = None
        
        # Report debugging information.
        self.debug = False
        
        # The following dictionaries are initialised when the built-in types are
        # defined.
        self.wrapper_types = {}
        self.wrapper_unpack_methods = {}
        self.unwrapped_types = {}
        self.default_container_classes = {}
        self.custom_exceptions = {}
        
        self.include_paths = []
        self.suppress_warnings = []
        self.listing_file_name = None
    
    def add_type(self, type_):
    
        self.types.add(type_)
        self.strings.add(type_.desc)
    
    def add_pending_class(self, class_):
    
        if isinstance(class_, TemplateClass):
            # If the class is a specialised version of a template class then
            # add the template class instead.
            class_ = class_.container_class
        
        if class_ not in self.processed and class_ not in self.pending:
            self.pending.add(class_)
            self.pending_order.append(class_)
    
    def add_pending_class_for_method(self, class_, method):
    
        if isinstance(class_, TemplateClass):
            class_ = class_.container_class
        
        if class_ in self.pending:
            return
        
        if class_ in self.processed and method in self.processed[class_]:
            return
        
        self.pending.add(class_)
        self.pending_order.append(class_)
    
    def pop_pending_class(self):
    
        class_ = self.pending_order.pop(0)
        self.pending.remove(class_)
        return class_
