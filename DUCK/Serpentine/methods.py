"""
Copyright (C) 2018 David Boddie <david@boddie.org.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from common import *
from macros import *
from utils import Utils

# Method look-up and calling functions

class Methods(Utils):

    def __init__(self):
    
        Utils.__init__(self)
    
    def find_matching_method(self, callee, scope, node, quiet = False):
    
        """Finds the method that matches the MethodCall object passed as the callee
        that is accessible from the given scope and associated with the given AST
        node. A suitable method will be returned, or a NameLookupError exception
        will be raised."""
        
        class_ = class_from_container(callee.container_obj)
        
        arg_parameters = []
        for arg in callee.arguments:
            arg_parameters.append(arg.type_)
        
        if isinstance(class_, TemplateClass):
            container_class = class_.container_class
        else:
            container_class = class_
        
        # Whether the class is a template class with item types defined or a
        # regular class with fixed item types, obtain a dictionary mapping
        # parameters to item types.
        class_item_types = {}
        
        if class_.has_item_types():
            for key, value in zip(class_.parameters(), self.get_item_types(class_)):
                if value != None:
                    class_item_types[key] = value
        
        # If the call is to an initialisation method, only search for methods within
        # the callee's class.
        if callee.name == "__init__":
            only_in_callee_class = True
        else:
            only_in_callee_class = False
        
        possible = []
        
        for chosen in container_class.find_methods(callee.name, only_in_callee_class):
        
            # The way different methods are invoked in Python compared to Java
            # means that the way arguments are compared to prototype parameters
            # varies with the type of invocation.
            
            if chosen.static():
            
                # If the method found is static but the containing class does not
                # match the one specified then look for another method.
                if is_class(callee.container_obj) and container_class != chosen.class_:
                    continue
                
                start = 0
            
            elif chosen.name_ == "__init__":
                # Direct method call. Requires an explicit instance argument in
                # Python which must be removed before comparison.
                start = 1
            
            elif is_class(callee.container_obj):
            
                if scope.class_.inherits(class_) or scope.class_.implements(class_):
                
                    # Superclass method call. Remove the explicit instance argument
                    # supplied. If one was not specified then this should cause the
                    # arguments to fail to match the expected parameters.
                    start = 1
                
                elif arg_parameters and is_class(arg_parameters[0]) and \
                     arg_parameters[0].inherits(class_):
                    # A class method call that passes the instance as its first
                    # argument is treated as an instance call.
                    start = 1
                
                else:
                    # Don't allow any other types of call to class methods.
                    continue
            
            else:
                # Virtual method call or interface. There shouldn't be an
                # explicit instance argument supplied, so compare all the
                # arguments against the prototype parameters.
                start = 0
            
            # Skip this method if the number of arguments passed does not match
            # the number of parameters required.
            if len(arg_parameters) - start != len(chosen.prototype()[1]):
                continue
            
            # Record the translated parameter types for the method and the
            # substitutions for any item parameters found in its signature.
            parameter_types = []
            item_types = class_item_types.copy()
            
            # Record the specialised types for all arguments.
            specialised_types = {}
            
            aborted_argument_check = False
            
            for i, proto_node in enumerate(chosen.prototype()[1]):
            
                if is_container(class_):
                
                    # Handle normal template classes.
                    
                    # Find the appropriate type for the prototype node,
                    # recording any parameters that were encountered, and
                    # storing the definitions in the item types dictionary so
                    # that we can update the template class later if this
                    # method is used.
                    defined = self.get_parameter_types(proto_node, arg_parameters[start + i])
                    
                    # Copy any new item type definitions into the provisional
                    # dictionary.
                    for def_name, def_type in defined.items():
                    
                        if def_name not in item_types:
                            item_types[def_name] = def_type
                        elif not self.compatible_types(def_type, item_types[def_name], add_missing = True):
                            raise TypeError("Inconsistent types '%s' (expected '%s') used for parameters at line %i of file %s." % (
                                def_type, item_types[def_name], node.lineno, scope.file_name()))
                    
                    # Construct a type from the method parameter declaration,
                    # existing item type definitions and any new definitions.
                    par_type = self.substitute_parameters(class_, item_types,
                                                          proto_node, defined)
                    
                    # Determine whether the method parameter is a container
                    # parameter and if the type that it represents is primitive and
                    # will therefore require wrapping in a Object type.
                    
                    if has_parameter(proto_node) and has_primitive(par_type):
                        # If the parameter type is not an Object subclass then
                        # find the appropriate wrapper type.
                        par_type = self.get_wrapper_type(par_type)
                
                elif class_.replace_:
                
                    # Handle specialisable template classes.
                    
                    defined = self.get_parameter_types(proto_node,
                                                       arg_parameters[start + i])
                    if defined:
                        for key, value in defined.items():
                            try:
                                dest = specialised_types[key]
                                if dest != value:
                                    if self.dex_objects.debug:
                                        print "Info: Skipping method with inconsistent types (%s and %s) used for parameter %s at line %i of file %s." % (
                                            dest, value, key, node.lineno, scope.file_name())
                                    
                                    aborted_argument_check = True
                                    break
                            except KeyError:
                                pass
                            
                            if value != None:
                                specialised_types[key] = value
                        
                        # For classes that can be specialised, allow the parameter
                        # to be included in the list. It will be checked later.
                        par_type = proto_node
                    else:
                        # If no parameters were defined in this node then handle
                        # the type normally.
                        par_type = self.create_value_type(proto_node)
                
                else:
                    par_type = self.create_value_type(proto_node)
                
                # If the type is a container then check its index and item types to
                # determine whether it should be updated with types from the
                # container the method is being called on.
                
                if is_container(par_type) and is_container(class_):
                    new_par_type = self.resolve_type_from_container(par_type, class_)
                    
                    if new_par_type != None:
                        par_type = new_par_type
                
                parameter_types.append(par_type)
            
            # If the argument check was aborted then try the next method candidate.
            if aborted_argument_check:
                continue
            
            # Try to match the argument types with the method's parameter types.
            arg_types = arg_parameters[start:]
            matched = self.match_arguments(callee, chosen, arg_types,
                                           chosen.prototype()[1], parameter_types)
            
            if matched == arg_types:
            
                if chosen.class_.replace_ != []:
                    # If the method is part of a class that needs to be specialised
                    # then make a copy of the class that can be specialised later
                    # and return the equivalent method in that class.
                    chosen = self.get_specialised_class_method(
                        chosen, arg_types, class_, specialised_types)
                    
                    if chosen:
                        return chosen
                else:
                    # Return the method if the types of the arguments supplied
                    # match those of the method parameters exactly.
                    update_item_types(callee, class_, item_types)
                    
                    return chosen
            
            elif matched != False:
                # If the match succeeded but was not exact, record the method in
                # case no exact matches are found and the types of the arguments
                # that should be supplied to it.
                
                if start == 1:
                    # Include the type of the instance.
                    matched.insert(0, arg_parameters[0])
                
                possible.append((chosen, arg_types, matched, item_types, specialised_types))
        
        if possible:
        
            # No exact matches were found, but one or more inexact matches were
            # recorded, so examine each of these to try and find a match.
            for chosen, arg_types, matched_types, item_types, specialised_types in possible:
            
                if chosen.class_.replace_ != []:
                    # If the method is part of a class that needs to be specialised
                    # then make a copy of the class that can be specialised later
                    # and return the equivalent method in that class.
                    chosen = self.get_specialised_class_method(
                        chosen, arg_types, class_, specialised_types)
                    
                    if chosen:
                        # The method may contain non-parameterised arguments, so
                        # ensure that these contain updates types so that any
                        # necessary casting can occur.
                        callee.updated_types = matched_types
                        return chosen
                else:
                    # Update the arguments in the callee object to use the updated
                    # argument types. This causes arguments to be cast to the
                    # correct types if necessary.
                    callee.updated_types = matched_types
                    update_item_types(callee, callee.container_obj, item_types)
                    
                    return chosen
        
        if not quiet:
            repr_params = ", ".join(map(str, arg_parameters))
            raise NameLookupError("Failed to find a matching method for %s.%s(%s) at line %i of file %s." % (
                container_class.name(), callee.name, repr_params, node.lineno, scope.file_name()))
        else:
            return None
    
    
    def match_arguments(self, callee, method, arg_types, prototype_params, par_types):
    
        ### If/when we support trailing argument lists, this check will need to be
        ### removed or modified to handle it.
        if len(arg_types) != len(par_types):
            return False
        
        updated_arg_types = []
        
        for arg_type, proto, par_type in zip(arg_types, prototype_params, par_types):
        
            updated_type = self.compatible_types(arg_type, par_type, add_missing = True)
            if updated_type is None:
                return False
            else:
                updated_arg_types.append(updated_type)
        
        return updated_arg_types
    
    
    def add_method_call(self, class_, callee, chosen, node, context, scope,
                        check_base_class = True):
    
        # Determine the full name of the class containing the method, or the class
        # from which the method was called in the case where the method is called
        # from a reimplementation of itself.
        class_name = create_class_name(class_)
        args = callee.arguments[:]
        
        # Keep track of any new variables allocated in this function so that we can
        # free them when we return.
        context.allocate_group()
        
        # Check the argument types against the arguments supplied and perform any
        # necessary casting/wrapping/unwrapping.
        for i, arg_type in enumerate(callee.updated_types):
        
            if args[i].type_ == arg_type:
                continue
            
            if is_primitive(args[i].type_) and \
               isinstance(arg_type, visitor.ClassVisitor):
            
                args[i] = self.create_object(node, arg_type, args[i:i+1],
                                             context, scope)
            
            elif isinstance(args[i].type_, visitor.ClassVisitor) and \
               is_primitive(arg_type):
            
                args[i] = self.unwrap(args[i], args[i].type_, node, context,
                                      scope)
            
            elif is_primitive(args[i].type_) and is_primitive(arg_type):
            
                if args[i].size() == arg_type.size() and args[i].temporary():
                    # Reuse the temporary argument variable for the casted value.
                    casted = context.alias(args[i], arg_type)
                else:
                    casted = context.allocate(type_=arg_type)
                
                self.cast_primitive(args[i], casted, node, context, scope)
                args[i] = casted
            
            elif is_array(args[i].type_) and is_array(arg_type):
            
                a = has_primitive(args[i].type_)
                b = has_primitive(arg_type)
                
                if a != b:
                    if a:
                        # Wrap the items in the array.
                        arrays = self.dex_objects.builtins.find_object("Arrays")
                        array_var = self.call_method(arrays, "wrap", [args[i]],
                                                     node, context, scope)
                    else:
                        # Unwrap the items in the array.
                        arrays = self.dex_objects.builtins.find_object("Arrays")
                        array_var = self.call_method(arrays, "unwrap", [args[i]],
                                                     node, context, scope)
                    
                    args[i] = array_var
        
        # Record that the method was called and put its class in a queue for
        # processing.
        method_name = MemberName(chosen.name())
        prototype = self.create_prototype(chosen)
        method = Method(class_name, prototype, method_name)
        self.dex_objects.methods.add(method)
        self.dex_objects.add_pending_class_for_method(class_, chosen)
        
        if chosen.static():
            context.append(InvokeStatic(method, args))
        
        elif chosen.name_ == "__init__":
            # invoke-direct: non-static instance methods (constructors, private
            # instance methods)
            context.append(InvokeDirect(method, args))
        
        elif is_class(callee.container_obj) and chosen.name() == scope.name() and \
             scope.class_ != chosen.class_ and \
             (scope.class_.inherits(chosen.class_) or \
              scope.class_.implements(chosen.class_)):
            
            # Calling a method on a base class, not an instance, is equivalent
            # to calling the super() function in Java or Python.
            # invoke-super: closest superclass's virtual method with the
            # same name as the caller
            context.append(InvokeSuper(method, args))
        
        elif class_.is_interface() and (
            not isinstance(class_, visitor.ClassVisitor) or \
            class_.name_ != "Object"):
        
            # Interface classes (including all TemplateClass and ClassVisitor types
            # except Object)
            
            # invoke-interface: abstract/template classes
            args.insert(0, callee.container_obj)
            context.append(InvokeInterface(method, args))
        
        else:
            # Virtual calls include the register containing the instance.
            # invoke-virtual: non-private, non-static, non-final, non-constructor
            
            # If the method call was not on a class then insert the instance as the
            # first argument to the virtual method call.
            if not is_class(callee.container_obj):
                args.insert(0, callee.container_obj)
            
            context.append(InvokeVirtual(method, args))
        
        # Record how many variables were used to call other methods.
        context.max_outgoing = max(context.max_outgoing,
            sum(map(lambda var: var.size(), args)))
        
        context.free_group()
        
        # Record any use of methods that require permissions to be granted.
        for permission in chosen.permissions():
            self.dex_objects.permissions.setdefault(permission, []).append(chosen)
        
        return True
    
    
    def call_method(self, obj, name, arguments, node, context, scope,
                    result_obj = None, exception_type = None,
                    free_group_before_return = False):
    
        """Calls the method of obj with the given name and arguments, returning a
        variable of the appropriate return type if the method returns a value, or
        None if not. If result_obj is False then any return value will not be
        handled."""
        
        # Construct a method call object and pass it to the method below to find
        # the relevant Method object.
        callee = MethodCall(obj, name, arguments)
        chosen = self.find_matching_method(callee, scope, node)
        self.add_method_call(chosen.class_, callee, chosen, node, context, scope)
        
        if result_obj != False:
        
            if result_obj == None and free_group_before_return:
                # Discard all the variables needed to get the result.
                context.free_group()
            
            return self.process_return_value(None, callee, chosen, node,
                context, scope, result_obj, exception_type)
    
    
    def process_return_value(self, instance_var, callee, chosen, node, context, scope,
                             result_obj = None, exception_type = None):
    
        """Returns a variable of the correct type to hold the return value for
        the method specified by callee and chosen. If the method does not return a
        value then None is returned unless instance_var is not None, in which case
        instance_var will itself be returned. This is used when the method called
        is a constructor."""
        
        # Obtain the declared return type for the method. A default type will
        # be used for templates and we will need to replace this.
        original_return_type = self.create_value_type(chosen.prototype()[0])
        
        if not isinstance(original_return_type, Void):
            return_type = self.resolve_type(chosen.return_type(),
                class_from_container(callee.container_obj),
                chosen.parameters(), original_return_type)
        else:
            return_type = original_return_type
        
        if not isinstance(return_type, Void):
        
            ### Check for unresolved types resulting from incomplete item type
            ### definitions. This should be handled earlier in the type
            ### definition process since there could be other places where
            ### allocations could occur based on incomplete types.
            if return_type is None:
                raise TypeError("Incomplete type information obtained from call to '%s' at line %i of file %s" % (
                    callee, node.lineno, scope.file_name()))
            
            if result_obj is None:
                # No existing result object - allocate a new one.
                result_obj = dest_obj = context.allocate(type_=return_type)
            
            elif return_type != result_obj.type_:
                # The return value needs casting to the result object's type.
                dest_obj = context.allocate(type_=return_type)
            else:
                # No casting is required to store the return value in the result.
                dest_obj = result_obj
            
            # If the return type was changed for a template class, ensure that
            # it is cast to the new type.
            if return_type != original_return_type:
            
                if is_primitive(return_type):
                
                    # If the value was wrapped, unwrap it.
                    wrapper_type = self.get_wrapper_type(return_type)
                    wrapper_var = context.allocate(type_=wrapper_type)
                    context.append(MoveResult(wrapper_var))
                    cast_value(wrapper_var, wrapper_type, context, self.dex_objects)
                    context.free()
                    
                    self.unwrap(wrapper_type, wrapper_var, node, context, scope,
                                result_obj = dest_obj, exception_type = exception_type)
                
                elif is_array(return_type):
                    context.append(MoveResult(dest_obj))
                
                else:
                    # Store the result and cast it. Perhaps it would be useful
                    # to modify the variable's type to reflect this.
                    context.append(MoveResult(dest_obj))
                    cast_value(dest_obj, return_type, context, self.dex_objects)
                    
                    # If an exception type was supplied, check whether the result
                    # if null and raise an exception. This helps us to wrap method
                    # calls for subscript operations in a Pythonic way.
                    if exception_type != None and is_class(dest_obj.type_):
                        self.check_null(dest_obj, node, context, scope, exception_type)
            
            else:
                # No conversion was needed. Just put the result in the variable.
                context.append(MoveResult(dest_obj))
            
            if result_obj != dest_obj:
                # The result object is not the same as the object the return value
                # was assigned to. Cast the variable to the type expected by the
                # result object.
                self.use_compatible_type(dest_obj, result_obj.type_, node,
                    context, scope, result_obj = result_obj)
                
                context.free()
            
            return result_obj
        
        elif instance_var:
            return instance_var
        else:
            return None
    
    
    # Convenience function for creating an instance of a particular Object type.
    
    def create_object(self, node, class_, arguments, context, scope):
    
        """Creates an instance of an Object subclass specified by class_,
        passing the arguments supplied in addition to the instance created to the
        __init__ method. Returns the variable containing the instance."""
        
        instance_var = context.allocate(type_=class_)
        callee = MethodCall(class_, "__init__", [instance_var] + arguments)
        chosen = self.find_matching_method(callee, scope, node)
        
        if chosen.class_ != instance_var.type_:
            if isinstance(instance_var.type_, TemplateClass):
                instance_var.type_.container_class = chosen.class_
            else:
                type_ = class_ = chosen.class_
                instance_var.set_type(type_)
        
        class_name = create_class_name(class_)
        context.append(NewInstance(instance_var, class_name))
    
        # Call the init method with any additional arguments.
        self.add_method_call(chosen.class_, callee, chosen, node, context, scope,
                             check_base_class = False)
        
        return instance_var
    
    
    def cast_primitive(self, original_var, new_var, node, context, scope):
    
        """Generates a macro to cast the value in the original_var to the type in
        the new_var, placing the result in the new_var."""
        
        if can_cast_primitive(original_var.type_, new_var.type_):
        
            ins = CastPrimitive(new_var, original_var)
            context.append(ins)
        
        else:
            # Try and use a library method to cast the value.
            library_class = self.dex_objects.builtins.find_object("Types")
            
            to_type = new_var.type_.__class__
            
            # Use the relevant library method for casting between the pair of types
            # supplied. Unwrapping of non-primitive types will be done automatically.
            try:
                method_name = library_type_cast_methods[to_type]
            except KeyError:
                raise TypeError("Cannot cast a value of type %s to type %s at line "
                    "%i of file %s." % (original_type, new_var.type_, node.lineno,
                    scope.file_name()))
            
            self.call_method(library_class, method_name, [original_var], node,
                context, scope, result_obj = new_var)
    
    
    def unwrap(self, wrapper_type, wrapper_var, node, context, scope,
               result_obj = None, exception_type = ValueError):
    
        if exception_type != None:
            self.check_null(wrapper_var, node, context, scope, exception_type)
        
        try:
            method_name = self.dex_objects.wrapper_unpack_methods[wrapper_type]
        except KeyError:
            raise TypeError("Cannot unpack values of type '%s' at line %i of file %s." % (
                wrapper_type, node.lineno, scope.file_name()))
        
        return self.call_method(wrapper_var, method_name, [], node, context,
                                scope, result_obj)
    
    
    def check_null(self, variable, node, context, scope, exception_type):
    
        # First check the value held by the variable, raising an exception
        # if it is null.
        not_null_target = Target("not-null-target-%x" % id(node))
        
        # Use the integer type instructions for null comparisons.
        context.append(CompareZeroMacro(if_nez, not_null_target, variable))
        
        # Raise an exception.
        exception_obj = self.create_object(node,
            self.dex_objects.custom_exceptions[exception_type], [], context, scope)
        context.append(Throw(exception_obj))
        context.free()
        
        # Add the exception class to the pending classes.
        self.dex_objects.add_pending_class(self.dex_objects.custom_exceptions[exception_type])
        
        # Jump to here if the variable does not contain null.
        context.append(not_null_target)
    
    
    def cast_operand(self, left, right, context, scope, node):
    
        # Try to cast to higher precision.
        if left.size() == right.size():
        
            if isinstance(left.type_, Float) or isinstance(left.type_, Double):
                to_keep = left
                to_cast = right
            else:
                to_keep = right
                to_cast = left
            
            if to_cast.temporary():
                # Reuse the registers used by the variable to cast. The space it
                # occupies will be freed later as in the non-casting case.
                casted = context.alias(to_cast, to_keep.type_)
            else:
                # Allocate space for the result.
                casted = context.allocate(type_=to_keep.type_)
            
            # The operand that will be casted does not need to be freed. Either it
            # was replaced or it is a named variable.
            to_free = None
        
        elif left.size() > right.size():
        
            # Cast the right hand operand.
            to_keep = left
            to_cast = right
            
            if right.temporary():
                # Reuse the registers used by the variable to cast.
                context.free()
            
            # Allocate a new variable to hold the result, either replacing the
            # temporary operand, which has just been freed, or in addition to it,
            # in which case it will be freed by the calling function.
            casted = context.allocate(type_=to_keep.type_)
            
            # The replaced operand does not need to be freed.
            to_free = None
        
        else:
            # Cast the left hand operand. Note that the size of it is smaller than
            # that of the right hand operand so we cannot replace it if it is a
            # temporary variable.
            to_keep = right
            to_cast = left
            
            casted = context.allocate(type_=to_keep.type_)
            
            # The replaced operand will need to be freed later if it is temporary.
            if to_cast.temporary():
                to_free = to_cast
            else:
                to_free = None
        
        self.cast_primitive(to_cast, casted, node, context, scope)
        
        if to_cast == left:
            left = casted
        else:
            right = casted
        
        return left, right, to_free
    
    
    def use_compatible_type(self, variable, constraint, node, context, scope,
                            add_missing = False, result_obj = None):
    
        """Checks the given variable's type for compatibility with the constraint
        type, returning the original variable if identical, allocating a new
        variable of a compatible type if possible, or returning None if no
        compatible types exist.
        
        Code will be generated to cast the original value to the compatible type
        and assign it to any allocated variable. The caller must check for a new
        variable and free it."""
        
        proposed = variable.type_
        resolved = self.compatible_types(proposed, constraint, add_missing)
        
        # Are types the same?
        if resolved == proposed:
            return variable
        
        # Quickly check for failure (no compatible types).
        if resolved is None:
            return None
        
        if isinstance(proposed, Placeholder) and \
           isinstance(proposed.class_, visitor.ClassVisitor):
        
            # Replace the object's placeholder type with the resolved type and
            # generate code to cast the value to the resolved type.
            variable.set_type(resolved)
            cast_value(variable, resolved, context, self.dex_objects)
            return variable
        
        elif resolved == constraint and is_primitive(resolved):
        
            # For primitive type casts, cast to the result object's type, or
            # allocate a temporary variable to hold the converted value.
            if not result_obj:
                temporary = context.allocate(type_=constraint)
            else:
                temporary = result_obj
            
            self.cast_primitive(variable, temporary, node, context, scope)
            return temporary
        
        return variable
    
    
    def get_array_item(self, iterable_obj, item_var, item_type, index_obj, node,
                       context, scope):
    
        # See the handling code for the Subscript node for an explanation of
        # this Object unwrapping code.
        
        iterable_type = iterable_obj.type_
        
        if iterable_type.wrapped_items:
        
            if isinstance(item_type, TypeDescriptor):
            
                # Find the wrapper used for the specified items and extract an
                # item into a variable with that type.
                wrapper_type = self.get_wrapper_type(item_type)
                wrapper_var = context.allocate(type_=wrapper_type)
                context.append(ArrayGet(wrapper_var, iterable_obj, index_obj))
                cast_value(wrapper_var, wrapper_type, context, self.dex_objects)
                context.free()
                
                # Call the method to convert the wrapped object into a primitive
                # value.
                self.unwrap(wrapper_type, wrapper_var, node, context, scope,
                            result_obj = item_var, exception_type = IndexError)
            else:
                # Cast the item type to the more specialised Object type.
                context.append(ArrayGet(item_var, iterable_obj, index_obj))
                cast_value(item_var, item_type, context, self.dex_objects)
        else:
            context.append(ArrayGet(item_var, iterable_obj, index_obj))
