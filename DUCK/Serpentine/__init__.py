"""
Copyright (C) 2016 David Boddie <david@boddie.org.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

"""~
# The Serpentine Package

The compiler transforms nodes from a syntax tree into instructions for the
virtual machine. The implementation in the `Compiler` class performs the
transformation, as well as setting up and managing structures related to
defined classes, methods, variables and attributes/fields.
"""

import os, sys

from Dalvik.dex import *

from arrays import *
from builtins import Builtins
from macros import *
from methods import Methods
from utils import DexObjects
import visitor
import writer

"""~
## Compiler class

To incorporate common features, the `Compiler` class is derived from classes in
the [arrays](arrays.md), [methods](methods.md) and [builtins](builtins.md)
modules. *Ideally the implementations of the methods provided by these base
classes would not mutually depend on the presence of each other. Unfortunately,
the development of the compiler resulted in an instance that aggregates
features, making it difficult to create completely separate implementations.*

Initialisation of the compiler involves the creation of a `DexObjects` object
that holds information about the different categories of objects that are
needed for the final DEX file. It also holds information about resources, such
such as a list of include paths, and compiler options such as the warnings to
suppress and whether to keep source information for producing diagnostic
listings.
"""

class Compiler(Arrays, Methods, Builtins):

    def __init__(self):
    
        Methods.__init__(self)
        
        # Create an object to hold the objects that are found in Dex files.
        self.dex_objects = DexObjects()
        self.dex_objects.listing_file_name = os.getenv("DUCK_LISTING")
        self.dex_objects.keep_source = (self.dex_objects.listing_file_name != None)
        self.dex_objects.debug = os.getenv("DEBUG")
    
    def define_application_classes(self, package_name, code_file_name,
        res_module, app_base_classes = None):
    
        if not app_base_classes:
            # Find a class based on the standard Activity class.
            app_base_classes = ["android/app/Activity"]
        
        # Define the built-in types and register the module as the default
        # namespace in order to make it the first place we search for unknown
        # names.
        builtins = self.define_builtin_types()
        self.dex_objects.builtins = builtins
        
        # Share the cache of imported modules with other modules. This ensures that
        # there is only ever one definition per class.
        import_cache = builtins.imports
        
        # Add the application resources to the module cache.
        import_cache.update({"app_resources": res_module})
        
        vtr = visitor.Visitor(self.dex_objects.include_paths, package_name)
        module = vtr.load(code_file_name, import_cache)
        
        for class_ in module.ordered_classes():
            self.dex_objects.add_pending_class(class_)
        
        self.define_classes(module, code_file_name)
        
        # Find classes based on those specified in the app_base_classes list.
        app_classes = {}
        
        for base in app_base_classes:
        
            for class_ in module.ordered_classes():
            
                if class_.package() == module.package() == package_name and \
                    class_.superclass():
                    
                    # Check whether this class inherits from the current base class.
                    super_class = class_.superclass()
                    
                    while super_class:
                        if super_class.full_name() == base:
                            # It inherits from the base class, so break past the
                            # while loop's else clause.
                            break
                        else:
                            super_class = super_class.superclass()
                    else:
                        # Try the next class.
                        continue
                    
                    # Find the most specialised subclass of the base class.
                    while True:
                        for c in module.ordered_classes():
                            if class_.name_ in c.bases:
                                # Make this subclass the current class and break so
                                # that we search again.
                                class_ = c
                                break
                        else:
                            # No further subclasses were found.
                            break
                    
                    app_classes[base] = class_
                    break
            
            else:
                # If the classes are provided in a dictionary then check to see
                # if this missing one is required.
                if isinstance(app_base_classes, dict) and app_base_classes[base] != None:
                    raise NameLookupError("Failed to find an %s class for package %s." % (
                        base, repr(package_name)))
        
        return app_classes
    
    
    def define_build_classes(self, package_name, build_info_template,
                             app_classes):
    
        vtr = visitor.Visitor(self.dex_objects.include_paths, package_name)
        module = vtr.load(build_info_template)
        module.attributes["__package__"] = ast.Str(app_classes[0].package())
        
        for class_ in module.ordered_classes():
            self.dex_objects.add_pending_class(class_)
        
        self.define_classes(module, build_info_template)
    
    
    def define_builtin_types(self):
    
        vtr = visitor.Visitor(self.dex_objects.include_paths)
        for include_path in self.dex_objects.include_paths:
            try:
                module = vtr.load(os.path.join(include_path, "serpentine", "builtins.py"))
                break
            except IOError:
                pass
        else:
            raise ImportError("Failed to find the 'builtins.py' module on the include paths.")
        
        # Load the java.lang module and store the visitor for the String
        # class in the type map.
        stdlib_path = None
        
        for path in self.dex_objects.include_paths:
            if module.import_module("java.lang", path, True) != None:
                stdlib_path = path
                break
        
        module.import_module("java.lang.annotation", stdlib_path, True)
        
        class_ = module.find_object("java.lang.String")
        value_to_type_map[str] = value_to_type_map[unicode] = class_
        type_name_to_type_map["str"] = class_
	
        # Register Object as the generic type for template parameter types, but
        # mark it as a placeholder to make it easier for us to replace it later.
        ### This should be generalised, perhaps by inspecting the declared
        ### parameters for a class.
        obj = Placeholder(module.find_object("java.lang.Object"))
        type_name_to_type_map["E"] = obj
        type_name_to_type_map["T"] = obj
        type_name_to_type_map["K"] = obj
        type_name_to_type_map["V"] = obj
        
        wrapper_types = self.dex_objects.wrapper_types
        wrapper_types[Boolean] = module.find_object("java.lang.Boolean")
        wrapper_types[Int] = module.find_object("java.lang.Integer")
        wrapper_types[Float] = module.find_object("java.lang.Float",)
        wrapper_types[Long] = module.find_object("java.lang.Long")
        wrapper_types[Double] = module.find_object("java.lang.Double")
        wrapper_types[Short] = module.find_object("java.lang.Short")
        wrapper_types[Byte] = module.find_object("java.lang.Byte")
        wrapper_types[Void] = module.find_object("java.lang.Void")
        
        unwrapped_types = self.dex_objects.unwrapped_types
        unwrapped_types[wrapper_types[Boolean]] = Boolean
        unwrapped_types[wrapper_types[Int]] = Int
        unwrapped_types[wrapper_types[Float]] = Float
        unwrapped_types[wrapper_types[Long]] = Long
        unwrapped_types[wrapper_types[Double]] = Double
        unwrapped_types[wrapper_types[Byte]] = Byte
        unwrapped_types[wrapper_types[Short]] = Short
        unwrapped_types[wrapper_types[Void]] = Void
        
        wrapper_unpack_methods = self.dex_objects.wrapper_unpack_methods
        wrapper_unpack_methods[wrapper_types[Boolean]] = "booleanValue"
        wrapper_unpack_methods[wrapper_types[Int]] = "intValue"
        wrapper_unpack_methods[wrapper_types[Float]] = "floatValue"
        wrapper_unpack_methods[wrapper_types[Long]] = "longValue"
        wrapper_unpack_methods[wrapper_types[Double]] = "doubleValue"
        wrapper_unpack_methods[wrapper_types[Short]] = "shortValue"
        wrapper_unpack_methods[wrapper_types[Byte]] = "byteValue"
        
        # Load the java.util module and store the visitors for the LinkedList,
        # HashMap and HashSet classes.
        module.import_module("java.util", stdlib_path, True)
        default_container_classes = self.dex_objects.default_container_classes
        
        linked_list = module.find_object("java.util.LinkedList")
        hash_map = module.find_object("java.util.HashMap")
        hash_set = module.find_object("java.util.HashSet")
        
        default_container_classes["java.util.LinkedList"] = linked_list
        default_container_classes["java.util.HashMap"] = hash_map
        default_container_classes["java.util.HashSet"] = hash_set
        
        type_name_to_type_map["list"] = linked_list
        type_name_to_type_map["dict"] = hash_map
        type_name_to_type_map["set"] = hash_set
        
        custom_exceptions = self.dex_objects.custom_exceptions
        custom_exceptions[IndexError] = module.find_object("IndexError")
        custom_exceptions[IOError] = module.find_object("IOError")
        custom_exceptions[KeyError] = module.find_object("KeyError")
        custom_exceptions[TypeError] = module.find_object("TypeError")
        custom_exceptions[ValueError] = module.find_object("ValueError")
        
        return module
    
    """~
    ### Defining classes
    
    The following method is used to process pending classes that have been
    found in the source files, and also those that are generated as part of
    compiling template classes.
    """
    
    def define_classes(self, module, code_file_name):
    
        while self.dex_objects.pending:
            self.define_class(self.dex_objects.pop_pending_class(), module,
                              code_file_name)
    
    """~
    Each class is analysed and processed to create meta-data for the resulting
    DEX file. Fields (attributes) are recorded, methods and interfaces are
    catalogued, and annotations are processed to produce the correct access
    and visibility properties for methods and inner classes.
    
    Non-abstract methods are passed to the `create_code` method so that code
    can be generated from the abstract syntax tree they contain.
    
    Finally, inner classes of the class are queued for processing.
    """
    
    def define_class(self, class_, module, code_file_name):
    
        # If the class is a template class, but does not define any replacement
        # types then it has not been specialised and is not included in the output
        # file. This may cause problems if we want to allow template classes to
        # contain inner classes.
        if class_.replace_ and not class_.specialised:
            return
        
        # Establish some basic information about the class.
        class_name = Name(u"L%s;" % class_.full_name())
        external = class_.external(module) and \
                   not class_.package().startswith("serpentine.")
        reference_only = class_.annotations() == [] and class_.interfaces() == []
        interface = class_.is_interface()
        
        # Record whether the class is a specialised version of a template class.
        specialised = class_.specialised
        
        if external:
            # Only external classes containing interfaces and/or annotations
            # are included in the set of classes.
            access = 0
            if class_.annotations():
                access = access | ACC_ANNOTATION
            
            # The class was purely there for reference so don't actually define it.
            if access == 0:
                return
            
            if class_.interfaces():
                access = access | ACC_INTERFACE
            
            access = access | ACC_ABSTRACT | ACC_PUBLIC
        else:
            access = ACC_PUBLIC
        
        if class_.static():
            access = access | ACC_STATIC
        if class_.final():
            access = access | ACC_FINAL
        if interface:
            access = access | ACC_ABSTRACT | ACC_INTERFACE
        
        # Record the methods of the class that have been processed.
        self.dex_objects.processed.setdefault(class_, set())
        self.dex_objects.implemented.setdefault(class_, set())
        
        # Extract static fields and static values.
        static_fields = []
        static_values = []
        
        for key, field in class_.fields.items():
        
            if not isinstance(field, visitor.StaticField):
                field.type_ = self.create_value_type(field.type_)
                continue
            
            # Static fields that are types or classes can be converted directly
            # to fields. Otherwise, their types need to be resolved.
            if isinstance(field.type_, ast.Name) and \
               field.type_.id in value_name_to_type_map:
            
                field.type_, value = self.value_type_value_from_value(field.type_)
                ### Store the static value. Note that some static values will need
                ### to be initialised in a <clinit> method which we need to create
                ### ourselves.
                static_values.append(value)
            
            if is_primitive(field.type_):
                static_fields.append(
                    (convert_field(field), AccessFlags(ACC_STATIC | ACC_PUBLIC))
                    )
            else:
                raise TypeError, "Unhandled static field '%s' of type %s in %s." % (
                    key, visitor.repr_ast(field.type_), class_.name())
        
        # Dereference the references in the list of interfaces, collecting methods
        # that must be implemented by this class.
        interfaces = []
        must_implement = {}
        
        for ref in class_.interfaces():
            obj = class_.module.find_object(ref.value)
            if obj is None:
                raise NameLookupError, "Could not find interface '%s' referred to in %s." % (
                    ref.value, class_.name())
            else:
                interfaces.append(create_class_name(obj))
                # Ensure that we visit this interface class so that it can be
                # included in the package as required.
                self.dex_objects.add_pending_class(obj)
            
            # Ensure that this class implements the declared interfaces if it is
            # more than just a declaration.
            if not external and not interface:
                for method_name, methods in obj.methods.items():
                    for method in methods:
                        # Create a prototype for the method from its decoration.
                        prototype = self.create_prototype(method)
                        signature = (method.name_, prototype)
                        must_implement[signature] = method
        
        # Extract methods and additional undeclared instance fields.
        direct_methods = []
        virtual_methods = []
        signatures = set()
        
        # Process the methods in the order in which they were declared.
        
        for method_name in class_.method_names:
        
            for method in class_.methods[method_name]:
            
                # Skip the method if it has already been processed.
                if class_ in self.dex_objects.processed and \
                   method in self.dex_objects.processed[class_]: continue
                
                # If the class is specialised but the method is not then skip it.
                if specialised and not method.specialised:
                    continue
                
                # Mark the method as processed.
                self.dex_objects.processed[class_].add(method)
                
                virtual = method.virtual()
                static = method.static()
                
                if method_name == u"__init__":
                    constructor = True
                    virtual = False
                else:
                    constructor = False
                    virtual = not static
                
                method_access = create_access_flags(constructor, external,
                    reference_only, method.decorators, interface, static)
                
                # Create code if code should be created for this method.
                # We pass the dictionary of instance fields so that it can be
                # updated as new fields are defined.
                if not external and not interface:
                    code = self.create_code(method)
                else:
                    code = None
                
                if code != None or not reference_only or (not external and interface):
                
                    # Create a prototype for the method from its decoration.
                    prototype = self.create_prototype(method)
                    signature = (method.name_, prototype)
                    self.dex_objects.implemented[class_].add(signature)
                    
                    # If a method with the same name and prototype has already been
                    # defined for this class then report an error.
                    if signature in signatures:
                        raise NameLookupError("%s.%s cannot be defined at line %i of file %s "
                            "because a method with the same signature already exists." % (
                            method.class_.name(), method.name_, method.line_number,
                            method.file_name()))
                    
                    signatures.add(signature)
                    
                    dex_method = Method(
                        class_name,
                        prototype,
                        MemberName(method.name()),
                        method_access,
                        code)
                    
                    if virtual:
                        virtual_methods.append(dex_method)
                        self.dex_objects.methods.add(dex_method)
                    else:
                        direct_methods.append(dex_method)
        
        # Report any methods required by interfaces that are missing unless they
        # are suppressed by the build script.
        if must_implement:
            for signature, method in must_implement.items():
                if signature in self.dex_objects.implemented[class_]:
                    continue
                elif (signature[0], str(method.class_)) not in self.dex_objects.suppress_warnings:
                    print ("Warning: Method '%s' from interface '%s' not "
                        "implemented in class '%s' in file %s." % (signature[0],
                        method.class_, class_, class_.file_name()))
                    # Don't warn about this again if the class is revisited.
                    self.dex_objects.implemented[class_].add(signature)
        
        class_annotations = []
        
        for anno_name, pairs in class_.annotations():
        
            # Reconstruct the pairs of values from the syntax tree data in the
            # list of pairs.
            new_pairs = []
            for key, value in pairs:
            
                if type(value) == list:
                    values = value
                else:
                    values = [value]
                
                new_values = []
                for value in values:
                
                    if isinstance(value, ast.Call) and isinstance(value.func, ast.Name):
                    
                        if value.func.id == "Field" and len(value.args) == 2:
                            # Fields should be specified as a fully-qualified
                            # class name and a member name. The arguments should
                            # both be string constants.
                            field_name = value.args[0].s
                            member_name = value.args[1].s
                            
                            new_values.append(Field(Name("L%s;" % field_name),
                                                    Name("L%s;" % field_name),
                                                    MemberName(member_name)))
                        
                        elif value.func.id == "Type" and len(value.args) == 1:
                            # Types should only contain a single fully-qualified
                            # name.
                            type_name = value.args[0].id
                            new_values.append(Type(Name("L%s;" % type_name)))
                        
                        else:
                            raise ValueError("Unhandled type '%s' in annotation." % value.func.id)
                    
                    elif isinstance(value, ast.Num) and type(value.n) == int:
                        new_values.append(Int(value.n))
                    
                    elif isinstance(value, ast.Str):
                        if type(value.s) == str or type(value.s) == unicode:
                            new_values.append(value.s)
                        else:
                            raise ValueError("Unhandled type '%s' in annotation." % value.s)
                    else:
                        raise ValueError("Unhandled type '%s' in annotation." % value)
                
                if len(new_values) == 1:
                    new_values = new_values[0]
                
                # The key itself must be a constant string.
                new_pairs.append((key.s, new_values))
            
            # The annotation name must be a constant string.
            class_annotations.append(
                (Visibility(VISIBILITY_RUNTIME),
                 Annotation(Name("L%s;" % anno_name.s), new_pairs))
                )
        
        # Append class hierarchy annotations.
        if class_.classes:
        
            inner_classes = []
            for inner_class in class_.ordered_classes():
                inner_classes.append(Type(create_class_name(inner_class)))
            
            class_annotations.append(
                (Visibility(VISIBILITY_SYSTEM),
                 Annotation(Name("Ldalvik/annotation/MemberClasses;"), [
                    ("value", inner_classes)]))
                )
        
        if isinstance(class_.parent, visitor.ClassVisitor):
        
            class_annotations.append(
                (Visibility(VISIBILITY_SYSTEM),
                 Annotation(Name("Ldalvik/annotation/EnclosingClass;"), [
                    ("value", Type(create_class_name(class_.parent)))
                    ]))
                )
            class_annotations.append(
                (Visibility(VISIBILITY_SYSTEM),
                 Annotation(Name("Ldalvik/annotation/InnerClass;"), [
                    ("accessFlags", Int(ACC_FINAL | ACC_STATIC | ACC_PUBLIC)),
                    ("name", class_.name_)
                    ]))
                )
        
        # If the class is an implementation of a template class then include a
        # signature annotation.
        if class_.has_item_types():
        
            item_type_full_names = []
            
            for item_type_obj in class_.item_types():
            
                if item_type_obj is None:
                    raise NameLookupError("Could not find '%s' referred to in %s." % (
                        class_.item_type_name(), class_.name()))
                
                # Resolve the type.
                item_type = self.create_value_type(item_type_obj)
                
                # Wrap the type in an Object if it is a primitive type.
                wrapper_type = self.get_wrapper_type(item_type)
                item_type_full_names.append("L%s;" % wrapper_type.full_name())
            
            if not class_.superclass():
                raise TypeError("Unknown superclass '%s' referred to in %s." % (
                        class_.bases[0], class_.name()))
            
            class_annotations.append(
                (Visibility(VISIBILITY_SYSTEM),
                 Annotation(Name("Ldalvik/annotation/Signature;"), [
                    ("value", [
                        "L%s" % class_.superclass().full_name(),
                        "<"] + item_type_full_names + [">;"])
                    ]))
                )
        
        instance_fields = []
        field_annotations = []
        
        # Extract the final instance fields from the class.
        for key, field in class_.fields.items():
            if not isinstance(field, visitor.StaticField):
                instance_fields.append((convert_field(field), AccessFlags(ACC_PUBLIC)))
        
        if class_.superclass():
            superclass = Name(u"L%s;" % class_.superclass().full_name())
        else:
            superclass = Name(u"Ljava/lang/Object;")
        
        if class_annotations or field_annotations:
            annotations = Annotations(class_annotations, field_annotations, [], [])
        else:
            annotations = None
        
        try:
            # If we are revisiting this class definition then only add new methods
            # to it.
            classdef = self.dex_objects.classes[class_]
            classdef.class_data.direct_methods += direct_methods
            classdef.class_data.virtual_methods += virtual_methods
        
        except KeyError:
            classdef = ClassDef(
                class_name,
                AccessFlags(access),
                superclass,
                interfaces,
                os.path.split(code_file_name)[1],
                annotations,
                ClassData(static_fields, instance_fields,
                          direct_methods, virtual_methods),
                static_values)
        
        self.dex_objects.classes[class_] = classdef
        
        # Examine any inner classes.
        for inner_class in class_.ordered_classes():
            self.dex_objects.add_pending_class(inner_class)
    
    """~
    ### Code generation
    
    The following method prepares for code generation by performing a number
    of tasks.
    
    It determines the number of register slots that will be occupied by the
    method parameters and resolves their types so that code can be generated
    to perform type checks and casts on entry to the method.
    
    A `Context` object is used to keep track of the register use during code
    generation. This is passed to the `process_node` method along with the
    object representing the method and the main syntax tree node.
    
    When generation has finished, the register allocation is optimised and the
    number of incoming and outgoing registers is determined. Other requirements
    involving register usage are also handled.
    
    A `Code` object is returned for further processing.
    """
    
    def create_code(self, method):
    
        """Creates instructions for the given method (a visitor.Method) with
        its associated prototype using information in the classes dictionary and
        dex_objects collection to relate it to other resources."""
        
        # Determine the register requirements of the method first so that we can
        # determine what the starting register will be. (The variables inside the
        # method are allocated low registers and the arguments are passed in
        # higher registers.) The Context object we create below lets us keep
        # track of the requirements.
        
        # Determine the size of each of the method arguments, including the return
        # value.
        
        n = 0
        arguments = []
        argnames = method.argnames[:]
        prototype = method.prototype()
        
        try:
            if not method.static():
                arguments.append(Variable(argnames.pop(0), 0, method.class_))
                n += 1
                first_argument = 1
            else:
                first_argument = 0
            
            if len(argnames) != len(prototype[1]):
                raise TypeError

        except (IndexError, TypeError):
            raise TypeError(
                "The number of method parameters for '%s' differs from the number "
                "specified in the prototype at line %i of file %s." % (method.name_,
                method.line_number, method.file_name())
                )
        
        for name, arg_type in zip(argnames, prototype[1]):
        
            default_type = self.create_value_type(arg_type)
            if method.specialised:
                type_ = self.resolve_type(arg_type, method.class_,
                                          method.parameters(), default_type)
            else:
                type_ = default_type
            
            arguments.append(Variable(name, n, type_))
            n += type_.size()
        
        # The number of input words is the number of words required for the method
        # arguments.
        ins_size = n
        
        # Set up an object to hold the variables used and the instructions created.
        context = Context(arguments, 4, self.dex_objects.keep_source)
        context.append(Source(method.code[0]))
        
        # Convert arguments in regular template classes with defined parameters and
        # specialised template classes.
        
        for arg_type, variable in zip(prototype[1], arguments[first_argument:]):
        
            type_ = self.resolve_type(arg_type, method.class_,
                                      method.parameters(), variable.type_)
            
            # The type should only be different if a template parameter was in use.
            if type_ != variable.type_:
            
                if type_.size() > variable.type_.size():
                    ### We could simply update the original argument variable if it
                    ### was the last one allocated and no more have been added.
                    temporary = context.allocate(type_=type_)
                
                elif is_array(variable.type_):
                    # The array should contain wrapped values but we won't unwrap
                    # them here. Instead, we redeclare the variable type to provide
                    # the correct item type in the array. This shouldn't change its
                    # size.
                    variable.set_type(type_)
                    
                    # Only perform a cast of the array if it does not contain
                    # wrapped primitive types. For some reason the necessity of
                    # having to declare methods with Objects as parameters for
                    # template classes such as AsyncTask in combination with the
                    # use of Number classes to wrap primitive types causes
                    # problems. The Java compiler performs a cast before delegating
                    # to another method to deal with this but it's not clear why
                    # that works. This workaround will have to do for now.
                    
                    if not has_primitive(variable.type_):
                        # We add an instruction to cast the array to the type
                        # supplied by the compiler.
                        wrapper_type = self.get_wrapper_type(type_)
                        cast_value(variable, wrapper_type, context, self.dex_objects)
                    
                    continue
                else:
                    # The argument is an Object but it should be a subclass of
                    # Object. Redeclare its type in preparation for casting.
                    temporary = variable
                    temporary.set_type(type_)
                
                if is_primitive(type_):
                    # If the argument should have a primitive type then add code to
                    # unwrap it.
                    wrapper_type = self.get_wrapper_type(type_)
                    cast_value(variable, wrapper_type, context, self.dex_objects)
                    self.unwrap(wrapper_type, variable, method.code, context,
                                method, result_obj = temporary)
                
                elif not is_array(type_):
                    # If the parameter type is more specialised than Object then
                    # cast it.
                    cast_value(temporary, type_, context, self.dex_objects)
                
                # Update the variable list to use the updated or new variable
                # definition.
                context[variable.name] = temporary
        
        # Set the default value of a flag to monitor __init__ calls.
        self.dex_objects.init_call = False
        
        # Process the top level method node and everything beneath it.
        result = self.process_node(method.code, context, method)
        
        # Check for the presence of a call to an __init__ method in the base class,
        # raising an exception if none were made.
        if method.name_ == "__init__" and not self.dex_objects.init_call:
            raise TypeError("No __init__ call to a base class in %s.%s at line %i of "
                "file %s." % (method.class_.name(), method.name_, method.line_number, method.file_name()))
        
        # Check that there are no leftover temporary variables allocated. If there
        # is then this is a sign that there is a failure to free them somewhere.
        temp_vars = []
        
        for var in context.allocated:
            if var.temporary():
                temp_vars.append(var)
        
        if len(temp_vars) > 0:
            print ("Warning: %i temporary variable(s) remained allocated at the "
                   "end of %s:" % (len(temp_vars), method.full_name()))
            for var in temp_vars:
                print "", repr(var)
        
        # If there is no terminating Return statement, add one that represents
        # a void return only if the method has no return type.
        if not context.instructions or not isinstance(context.instructions[-1], Return):
            proto = self.create_prototype(method)
            if isinstance(proto.return_type, Void):
                context.instructions.append(ReturnVoid())
            else:
                raise TypeError("Expected a return value of type '%s' at end of "
                    "method '%s' at line %i of file %s." % (proto.return_type,
                    method.class_.name() + "." + method.name(), method.line_number,
                    method.file_name()))
        
        # Analyse the initially allocated registers used by the code by reading the
        # instructions used. Each variable should only occur once, so we can use a
        # list to hold them.
        registers = set()
        used = set()
        
        # Reserve registers for named and temporary variables.
        for ins in context.instructions:
        
            for var in ins.used_vars():
            
                if var in context.arguments:
                    continue
                
                used.add(var)
                
                for i in range(var.n, var.n + var.size()):
                    registers.add(i)
        
        # Leave space for outgoing method arguments.
        if len(registers) > 0:
            outgoing_base = max(registers) + 1
        else:
            outgoing_base = 0
        
        calls = filter(lambda instruction: isinstance(instruction, InvokeMacro),
                       context.instructions)
        
        # Provisionally assign registers to arguments and check if we need to
        # allocate space for outgoing method arguments. We fall back to the worst
        # case (max_outgoing registers) if extended method calls are needed. We
        # could determine the maximum number of extra registers but further
        # iterations would be needed in case argument registers fall outside the
        # low register window.
        
        arg_base_diff = outgoing_base
        
        # Move the arguments up to the end of the variables.
        for arg in context.arguments:
            arg.n += arg_base_diff
        
        for instruction in calls:
            if instruction.needs_range():
                arg_base_diff = context.max_outgoing
                break
        else:
            arg_base_diff = 0
        
        # Reserve registers for incoming method arguments and those in the space
        # set aside for outgoing method arguments.
        
        for arg in context.arguments:
            arg.n += arg_base_diff
            for i in range(arg.n, arg.n + arg.size()):
                registers.add(i)
        
        for instruction in calls:
            if instruction.needs_range():
                # Reserve registers in the space left for outgoing arguments.
                highest = instruction.allocate_extra_registers(outgoing_base)
                for i in range(outgoing_base, highest):
                    registers.add(i)
        
        # Now that the extra method argument registers have been reserved, record
        # the use of any additional registers from those that require low registers.
        for instruction in filter(lambda inst: isinstance(inst, ExtendedMacro),
                                  context.instructions):
        
            for variable in instruction.extra_vars():
                for i in range(variable.size()):
                    registers.add(variable.n + i)
        
        if registers:
        
            # Compact the variables so that there are no unused registers.
            max_allocated = max(registers)
            registers = list(registers)
            registers.sort()
            
            # Calculate the offsets needed to relocate variables.
            offsets = []
            n = 0
            o = 0
            while n <= max_allocated:
                if n < registers[0]:
                    offsets.append(0)
                    o -= 1
                else:
                    offsets.append(o)
                    registers.pop(0)
                n += 1
            
            # Renumber the registers used by each variable.
            for variable in used:
                variable.n += offsets[variable.n]
            
            # Renumber the registers used by extended method calls.
            for instruction in calls:
                for variable in instruction.extras:
                    variable.n += offsets[variable.n]
            
            # Renumber the registers used by incoming method arguments.
            for arg in context.arguments:
                arg.n += offsets[arg.n]
            
            # Update the maximum allocated register with the relevant offset.
            max_allocated += offsets[max_allocated]
            
            registers_size = max_allocated + 1
        
        else:
            registers_size = ins_size
        
        # Record the maximum number of outgoing registers needed by this method.
        outs_size = context.max_outgoing
        
        code = Code(registers_size, ins_size, outs_size, context.tries, 0, context.instructions)
        
        if self.dex_objects.keep_source:
            # Record the arguments to the method, the variables used and the AST
            # nodes describing the source.
            self.dex_objects.method_args[code] = context.arguments
            self.dex_objects.method_vars[code] = used
            self.dex_objects.method_tree[code] = method.code
        
        # Create a code object containing invalid tries and instructions objects.
        # These will be fixed when the code is assembled.
        return code
    
    
    def process_node(self, node, context, scope):
    
        if type(node) == list:
        
            # Insert a placeholder instruction for use when creating listings.
            context.append(Source(node[0]))
            
            for child in node:
                result = self.process_node(child, context, scope)
                
                if isinstance(result, Variable) and result.temporary():
                    context.free()
            
            return None
        
        if isinstance(node, ast.Expr):
            return self.process_node(node.value, context, scope)
        
        elif isinstance(node, ast.Call):
        
            # Handle special built-in functions separately.
            if isinstance(node.func, ast.Name):
                if node.func.id in self.builtin_functions:
                    return self.process_builtin(node, node.func.id, context,
                                                scope)
                elif node.func.id in self.builtin_indirected:
                    callee = self.process_library_function(node.func.id,
                                                           context, scope)
                else:
                    callee = self.process_node(node.func, context, scope)
            
            elif isinstance(node.func, ast.Attribute):
                # Resolve the attribute access up until the name in this outermost
                # node so that we can look specifically for a method.
                container_obj = self.process_node(node.func.value, context, scope)
                
                if container_obj is None:
                    raise NameLookupError("Failed to find '%s' at line %i of file %s." % (
                        node.func.value, node.func.value.lineno, scope.file_name()))
                
                if (isinstance(container_obj, visitor.ClassVisitor) or \
                    isinstance(container_obj, visitor.ModuleVisitor)) and \
                    node.func.attr in container_obj.classes:
                
                    # The callable is a class. Handle this below.
                    callee = container_obj.find_object(node.func.attr)
                else:
                    callee = MethodCall(container_obj, node.func.attr)
            else:
                callee = self.process_node(node.func, context, scope)
            
            if callee is None:
                raise NameLookupError("Failed to find '%s' at line %i of file %s." % (
                    node.func, node.func.lineno, scope.file_name()))
            
            instance_var = None
            
            if isinstance(callee, visitor.ClassVisitor):
            
                # The callee is a class. We need to create an instance of it and
                # call its __init__ method.
                class_ = callee
                
                if is_container(class_):
                    if not class_.has_item_types():
                        class_ = TemplateClass(class_, len(class_.parameters()) * [None])
                    else:
                        class_ = TemplateClass(class_, self.get_item_types(class_))
                
                instance_var = context.allocate(type_=class_)
                
                # Unlike the case where an __init__ method is called explicitly, we
                # need to include the instance variable as the first argument.
                # Additional arguments are added below.
                callee = MethodCall(class_, "__init__", [instance_var])
                
                ### The ___init__ methods of interface classes seem to be implicit
                ### and perform additional initialisation that we don't yet do.
            
            elif is_primitive(callee):
            
                # The callee is a primitive type, obtained from a template
                # parameter definition. Call the corresponding built-in function
                # for the type.
                name = type_class_to_type_name_map[callee.__class__]
                return self.process_builtin(node, name, context, scope)
            
            elif not isinstance(callee, MethodCall):
                raise TypeError, "Unhandled type of callee '%s' at line %i of file %s." % (
                    repr(callee), node.lineno, scope.file_name())
            
            # Arrays and primitive types don't have methods.
            container_type = class_from_container(callee.container_obj)
            if not is_class(container_type):
                raise TypeError("Cannot invoke methods on type %s at line %i of file %s." % (
                    container_type, node.lineno, scope.file_name()))
            
            # Allocate a group of variables to contain the arguments.
            context.allocate_group()
            args = []
            
            for arg in node.args:
            
                value = self.process_node(arg, context, scope)
                if value is None:
                    raise NameLookupError, "Failed to find '%s' at line %i of file %s." % (
                        arg, node.lineno, scope.file_name())
                
                callee.arguments.append(value)
            
            chosen = self.find_matching_method(callee, scope, node)
            
            if instance_var:
                # If the method found for creating the instance involved using a
                # more specialised class then redefine the instance's type and use
                # the new class from now on.
                if chosen.class_ != instance_var.type_:
                    if isinstance(instance_var.type_, TemplateClass):
                        instance_var.type_.container_class = chosen.class_
                    else:
                        type_ = class_ = chosen.class_
                        instance_var.set_type(type_)
                
                # Create a new instance of the class.
                class_name = create_class_name(class_)
                context.append(NewInstance(instance_var, class_name))
            else:
                # Use the method's class in the method definition.
                class_ = chosen.class_
            
            normal_call = (instance_var is None)
            if not self.add_method_call(class_, callee, chosen, node, context, scope,
                                        check_base_class = normal_call):
                raise TypeError, "Invalid method call '%s.%s' at line %i of file %s." % (
                    callee.container_obj.name(), chosen.name(), node.lineno,
                    scope.file_name())
            
            # Free the group of variables used for the arguments.
            context.free_group()
            
            # The only remaining variable that may have been allocated for this
            # function call is for the instance whose method we called. If so, free
            # it now so that we can reuse its space.
            if is_temporary_variable(callee.container_obj):
                context.free()
            
            # If this method is an __init__ method, record whether the call was to
            # an __init__ method in the base class.
            if scope.name_ == "__init__" and chosen.name_ == "__init__" and \
               chosen.class_ == scope.class_.superclass():
                self.dex_objects.init_call = True
            
            return self.process_return_value(instance_var, callee, chosen, node,
                                             context, scope)
        
        elif isinstance(node, ast.Attribute):
        
            # Find the container to access. This can be anything that supports
            # attributes of some kind.
            container_obj = self.process_node(node.value, context, scope)
            
            if container_obj is None:
                raise NameLookupError("Failed to find '%s' at line %i of file %s." % (
                    node.value, node.lineno, scope.file_name()))
            
            # Find the object referred to in the container. This method resolves
            # the container to a class or module that can be searched.
            name = node.attr
            name_obj = find_attribute(container_obj, name, node, scope)
            
            if name_obj is None:
                raise NameLookupError("Failed to find '%s' in '%s' at line %i of file %s." % (
                    name, container_obj, node.lineno, scope.file_name()))
            
            if isinstance(name_obj, visitor.ClassVisitor):
                return name_obj
            
            elif isinstance(name_obj, visitor.ModuleVisitor):
                return name_obj
            
            elif isinstance(name_obj, ast.Num) or isinstance(name_obj, ast.Str):
                return self.process_node(name_obj, context, scope)
            
            # Other objects are accessed via static or instance operations.
            
            if isinstance(container_obj, TemplateClass):
            
                # Resolve the template class to its container class.
                container_obj = container_obj.container_class
            
            # Field access
            
            # Resolve the field's type, just in case it hasn't been accessed
            # before.
            try:
                name_obj.type_ = self.create_value_type(name_obj.type_)
            except AttributeError:
                raise TypeError("'%s' is not a field at line %i of file %s." % (
                    name_obj, node.lineno, scope.file_name()))
            
            field = convert_field(name_obj)
            
            # Record that the field was used.
            self.dex_objects.fields.add(field)
            
            # If the container object is a temporary variable then free it.
            if is_temporary_variable(container_obj):
                context.free()
            
            # Allocate a variable to hold the value.
            temporary = context.allocate(type_=name_obj.type_)
            
            if isinstance(container_obj, visitor.ClassVisitor):
                # Static field access
                ins = GetAttr(temporary, field)
            else:
                # Instance field access
                ins = GetAttr(temporary, field, container_obj)
            
            context.append(ins)
            
            return temporary
        
        elif isinstance(node, ast.Name):
        
            if node.id in value_name_to_value_map:
                # Handle named constants.
                value = value_name_to_value_map[node.id]
                obj = context.allocate(type_=value_name_to_type_map[node.id])
                context.append(LoadConstant(obj, value))
            else:
                obj = find_object(node.id, scope, context, self.dex_objects)
            
            if obj is None:
                raise TypeError("Failed to find %s at line %i of file %s." % (
                        repr(node.id), node.lineno, scope.file_name()))
            
            return obj
        
        elif isinstance(node, ast.Num):
            return self.process_constant(node.n, node, scope, context)
        
        elif isinstance(node, ast.Str):
            return self.process_constant(node.s, node, scope, context)
        
        elif isinstance(node, ast.Return):
        
            # Insert a placeholder instruction for use when creating listings.
            context.append(Source(node))
            
            if node.value is None:
            
                if scope.return_type() == "void":
                
                    # When implicitly returning a None value from methods that
                    # return void, we take a short cut to avoid generating
                    # redundant instructions.
                    context.append(ReturnVoid())
                    return ast.Name("void", ast.Load())
                else:
                    # There is no return value but one was expected.
                    raise TypeError("Expected a return value on line %i of file %s." % (
                        node.lineno, scope.file_name()))
            
            # The return value is an expression which must be processed.
            return_value = self.process_node(node.value, context, scope)
            
            return_type = self.create_value_type(scope.prototype()[0])
            
            if isinstance(return_type, Void) and isinstance(return_value.type_, Null):
            
                # Discard None return values for void methods.
                if return_value.temporary():
                    context.free()
                
                context.append(ReturnVoid())
            
            else:
                # Store the original return value and check that it is compatible
                # with the declared return type for the method, obtaining a
                # suitably compatible value. As with assignments, if the proposed
                # type's definition is incomplete, use the constraint type to
                # complete it.
                original_return_value = return_value
                return_value = self.use_compatible_type(original_return_value,
                    return_type, node, context, scope, add_missing = True)
                
                if not return_value:
                    raise TypeError("Return value has type '%s' where type "
                        "'%s' was expected at line %i of file %s." % (
                        original_return_value.type_, return_type, node.lineno,
                        scope.file_name()))
                
                elif original_return_value != return_value:
                    # Free the temporary casted variable, if one was allocated.
                    context.free()
                
                # Free the variable used for the original return value if it is
                # temporary.
                if original_return_value.temporary():
                    context.free()
                
                # If returning None, cast the value to the return type. Perhaps it
                # would be useful to modify the variable's type to reflect this.
                if isinstance(return_value.type_, Null):
                    cast_value(return_value, return_type, context, self.dex_objects)
                
                context.append(Return(return_value))
            
            return None
        
        elif isinstance(node, ast.BinOp):
        
            return self.process_binary_operation(node.left, node.right,
                node.op.__class__, context, scope)
        
        elif isinstance(node, ast.BoolOp):
        
            return self.process_boolean_operations(node, context, scope)
        
        elif isinstance(node, ast.Assign):
        
            # Insert a placeholder instruction for use when creating listings.
            context.append(Source(node))
            
            # Pass the AST node for the expression and the list of assignment nodes
            # to the function that handles assignments.
            self.process_assignment(node.value, None, node.targets, node,
                                    context, scope)
            
            return None
        
        elif isinstance(node, ast.AugAssign):
        
            # Insert a placeholder instruction for use when creating listings.
            context.append(Source(node))
            
            # Resolve the variable being assigned to.
            if isinstance(node.target, ast.Name):
            
                obj = find_object(node.target.id, scope, context, self.dex_objects)
            
            elif isinstance(node.target, ast.Attribute):
            
                # Assigning to an attribute of a container.
                container_obj = self.process_node(node.target.value, context, scope)
                
                if container_obj is None:
                    raise NameLookupError("Failed to find '%s' at line %i of file %s." % (
                        node.target.value, node.target.value.lineno, scope.file_name()))
                
                # Find the attribute of the container, regardless of the
                # container's type, and record both the container and the
                # attribute.
                obj = find_attribute(container_obj, node.target.attr,
                                     node.target, scope)
            
            if obj is None:
                raise NameLookupError("Failed to find '%s' at line %i of file %s." % (
                    node.target, node.target.lineno, scope.file_name()))
            
            # Process the operation.
            if node.op.__class__ not in self.augmented_operations:
                raise TypeError("Unhandled augmented operation %s at line %i of file %s." % (
                    node.op.__class__.__name__, node.lineno, scope.file_name()))
            
            result = self.process_binary_operation(node.target, node.value,
                                           node.op.__class__, context, scope)
            
            # Copy the result into the appropriate variable or field.
            if isinstance(node.target, ast.Name) and obj != result:
                context.append(Move(obj, result))
            else:
                class_name = create_dex_type(container_obj)
                field = Field(class_name, create_dex_type(result),
                              MemberName(node.target.attr))
    
                # Record that the field was used.
                self.dex_objects.fields.add(field)
                
                ins = SetAttr(result, field, container_obj)
                context.append(ins)
            
            if result.temporary():
                context.free()
            
            return None
        
        elif isinstance(node, ast.Compare):
        
            # Comparisons that are not used directly by control flow statements are
            # evaluated to produce temporary boolean results.
            return self.process_comparison(node, context, scope, keep_result = True)
        
        elif isinstance(node, ast.If):
        
            # Insert a placeholder instruction for use when creating listings.
            context.append(Source(node))
            
            self.process_conditional_structure(node, context, scope)
            return None
        
        elif isinstance(node, ast.Pass):
            return None
        
        elif isinstance(node, ast.UnaryOp):
        
            if isinstance(node.op, ast.UAdd):
                return self.process_node(node.operand, context, scope)
            
            elif isinstance(node.op, ast.USub):
            
                if isinstance(node.operand, ast.Num) and type(node.operand.n) == int:
                    # Optimise the constant value.
                    temp_node = ast.Num(-node.operand.n)
                    value = self.process_constant(temp_node.n, temp_node, scope, context)
                    return value
                
                else:
                    value = self.process_node(node.operand, context, scope)
                    
                    if value.temporary():
                        context.free()
                    
                    result = context.allocate(type_=value.type_)
                    
                    ins = Negate(result, value)
                    context.append(ins)
                    
                    return result
            
            elif isinstance(node.op, ast.Not):
            
                value = self.process_node(node.operand, context, scope)
                
                if not isinstance(value.type_, Boolean):
                    raise TypeError("Invalid type for not operator at line %i of file "
                        "%s." % (node.lineno, scope.file_name()))
                
                if value.temporary():
                    context.free()
                
                result = context.allocate(type_=value.type_)
                
                ins = Not(result, value)
                context.append(ins)
                
                return result
            else:
                raise TypeError("Unhandled unary operation %s at line %i of file %s." % (
                    node.op.__class__.__name__, node.lineno, scope.file_name()))
        
        elif isinstance(node, ast.Subscript) and isinstance(node.slice, ast.Index):
        
            context.allocate_group()
            
            array_obj, index_obj, is_an_array, container_type, item_types = \
                self.get_subscript_objects(node, context, scope)
            
            if not is_an_array and item_types == [None]:
                raise TypeError("Undefined item type for %s at line %i of file %s." % (
                    repr(array_obj), node.lineno, scope.file_name()))
            
            # If not a container, try to find a suitable library method to process
            # the operation.
            if not item_types:
                context.free_group()
                
                # Try to use a library method to access the object.
                library_class = self.dex_objects.builtins.find_object("Collections")
                return self.call_method(library_class, "subscript_get",
                    [array_obj, index_obj], node, context, scope)
            
            # Obtain the item type.
            item_type = item_types[-1]
            
            if is_an_array:
            
                if not isinstance(index_obj.type_, Int):
                    raise TypeError, "Invalid type %s used for array index at line %i of file %s." % (
                        repr(index_obj.type_), node.lineno, scope.file_name())
                
                # Although arrays can contain both primitive and Object-based
                # values, template classes need to use Object-based classes to
                # wrap primitive types. This means that, when we specify primitive
                # types for these template classes, we want to automatically unwrap
                # values in arrays supplied to method of these classes.
                # See android.os.AsyncTask for an example of one of these classes.
                
                if array_obj.type_.wrapped_items:
                
                    if is_primitive(item_type):
                    
                        # Find the wrapper used for the specified items and extract an
                        # item into a variable with that type.
                        wrapper_type = self.get_wrapper_type(item_type)
                        wrapper_var = context.allocate(type_=wrapper_type)
                        context.append(ArrayGet(wrapper_var, array_obj, index_obj))
                        cast_value(wrapper_var, wrapper_type, context, self.dex_objects)
                        context.free()
                        
                        # Call the method to convert the wrapped object into a primitive
                        # value.
                        context.free_group()
                        temporary = self.unwrap(wrapper_type, wrapper_var, node,
                                                context, scope, exception_type = IndexError)
                    else:
                        # Cast the item type to the more specialised Object type.
                        context.free_group()
                        temporary = context.allocate(None, type_=item_type)
                        context.append(ArrayGet(temporary, array_obj, index_obj))
                        cast_value(temporary, item_type, context, self.dex_objects)
                else:
                    context.free_group()
                    temporary = context.allocate(None, type_=item_type)
                    context.append(ArrayGet(temporary, array_obj, index_obj))
                
                return temporary
            
            # Define the default type of exception that will be raised if the item
            # retrieved from the container is null.
            access_exception_type = IndexError
            
            if len(item_types) == 2:
            
                # Check the index type.
                index_type = item_types[0]
                
                original_index_obj = index_obj
                index_obj = self.use_compatible_type(original_index_obj,
                                index_type, node, context, scope)
                if not index_obj:
                    raise TypeError, "Invalid type '%s' for key type '%s' at line %i of file %s." % (
                        original_index_obj.type_, index_type, node.lineno, scope.file_name())
                
                elif original_index_obj != index_obj:
                    # Free the temporary casted variable, if one was allocated.
                    context.free()
                
                # Override the type of exception to reflect the type of container.
                access_exception_type = KeyError
            
            elif not isinstance(index_obj.type_, Int):
            
                raise TypeError, "Invalid type '%s' for index at line %i of file %s." % (
                    index_obj.type_, node.lineno, scope.file_name())
            
            # Look for a suitable get method in the object and call the method to
            # obtain the item.
            try:
                return self.call_method(array_obj, "get", [index_obj], node,
                    context, scope, exception_type = access_exception_type,
                    free_group_before_return = True)
            
            except NameLookupError:
                library_class = self.dex_objects.builtins.find_object("Collections")
                return self.call_method(library_class, "subscript_get",
                    [array_obj, index_obj], node, context, scope)
        
        elif isinstance(node, ast.Subscript) and isinstance(node.slice, ast.Slice):
        
            array_obj = self.process_node(node.value, context, scope)
            if array_obj is None:
                raise NameLookupError("Failed to find '%s' at line %i of file %s." % (
                    node.expr, node.lineno, scope.file_name()))
            
            ### Add support for objects that provide get and set methods.
            
            if node.slice.lower is None:
                # Allocate a temporary variable to pass as the lower bound and set
                # its value to zero.
                lower_obj = context.allocate(None, type_=dInt)
                context.append(LoadConstant(lower_obj, 0))
            else:
                lower_obj = self.process_node(node.slice.lower, context, scope)
                if lower_obj is None:
                    raise ValueError("Invalid lower slice bound '%s' at line %i of file %s." % (
                        node.slice.lower, node.lineno, scope.file_name()))
            
            if node.slice.upper is None:
            
                # Allocate a temporary variable to pass as the upper bound.
                upper_obj = context.allocate(None, type_=dInt)
                
                if is_array(array_obj.type_):
                    # Add code to determine the array length.
                    context.append(ArrayLength(upper_obj, array_obj))
                else:
                    self._len(array_obj.type_, array_obj, upper_obj, node,
                              context, scope)
            else:
                upper_obj = self.process_node(node.slice.upper, context, scope)
                if upper_obj is None:
                    raise ValueError("Invalid upper slice bound '%s' at line %i of file %s." % (
                        node.slice.upper, node.lineno, scope.file_name()))
            
            if upper_obj.temporary():
                context.free()
            if lower_obj.temporary():
                context.free()
            if array_obj.temporary():
                context.free()
            
            library_class = self.dex_objects.builtins.find_object("Collections")
            args = [array_obj, lower_obj, upper_obj]
            temporary = self.call_method(library_class, "slice", args, node,
                                         context, scope)
            return temporary
        
        elif isinstance(node, ast.While):
        
            # Insert a placeholder instruction for use when creating listings.
            context.append(Source(node))
            
            self.process_conditional_structure(node, context, scope)
            return None
        
        elif isinstance(node, ast.For):
        
            # Insert a placeholder instruction for use when creating listings.
            context.append(Source(node))
            
            self.process_iteration(node, context, scope)
            return None
        
        elif isinstance(node, ast.Break):
        
            try:
                context.append(Goto(self.dex_objects.suites[-1][1]))
            except IndexError:
                raise SyntaxError("Use of break outside a loop at line %i of file "
                    "%s." % (node.lineno, scope.file_name()))
            
            return None
        
        elif isinstance(node, ast.Continue):
        
            try:
                context.append(Goto(self.dex_objects.suites[-1][0]))
            except IndexError:
                raise SyntaxError("Use of continue outside a loop at line %i of file "
                    "%s." % (node.lineno, scope.file_name()))
            
            return None
        
        elif isinstance(node, ast.TryExcept):
        
            # Insert a placeholder instruction for use when creating listings.
            context.append(Source(node))
            
            # Create targets for the span of code to be trapped.
            try_begin_target = Target("try-begin-%x" % id(node))
            try_end_target = Target("try-end-%x" % id(node))
            
            # Also create a target for after all the suites.
            end_target = Target("try-after-%x" % id(node))
            
            # Process the body of the try suite without creating a new scope.
            context.append(try_begin_target)
            self.process_node(node.body, context, scope)
            context.append(Goto(end_target))
            context.append(try_end_target)
            
            # Enter a local scope. Variables defined are local to the scope.
            ### Consider using a separate scope for each except suite.
            context.enter_scope()
            context.allocate_group()
            
            handlers = []
            
            # Process each handler suite in this scope, creating a target for each
            # of the handlers.
            for handler in node.handlers:
            
                target = Target("try-handler-%x" % id(handler.body))
                context.append(target)
                
                exception_names = []
                
                if is_sequence(handler.type):
                    exception_names = handler.type.elts
                
                elif isinstance(handler.type, ast.Name):
                    exception_names = [handler.type]
                
                elif handler.type is None:
                    exception_names = [None]
                
                else:
                    raise TypeError("Value '%s' of unknown type used for "
                        "exception names at line %i of file %s." % (
                        repr(handler.type), exception_types.lineno),
                        scope.file_name())
                
                type_ = None
                
                for exception_name in exception_names:
                
                    if isinstance(exception_name, ast.Name):
                    
                        exception_name = exception_name.id
                        this_type = find_object(exception_name, scope, context, self.dex_objects)
                        if this_type is None:
                            raise NameLookupError("Failed to find '%s' at line %i "
                                "of file %s." % (exception_name, handler.type.lineno,
                                scope.file_name()))
                        
                        # Add the exception class to the pending classes in case it
                        # is a non-standard exception.
                        self.dex_objects.add_pending_class(this_type)
                        # Add its type in case it is a standard exception.
                        self.dex_objects.types.add(create_dex_type(this_type))
                        
                        # Only set the type once.
                        if not type_:
                            type_ = this_type
                    
                    elif exception_name is None:
                        # Catch-all
                        type_ = None
                    
                    else:
                        raise TypeError("Value '%s' of unknown type used for "
                            "exception names at line %i of file %s." % (
                            repr(exception_name), handler.type.lineno, scope.file_name()))
                    
                    handlers.append((target, type_))
                
                if handler.name:
                
                    # Assign the exception object to an existing or new local variable.
                    name = handler.name.id
                    variable = find_object(name, scope, context, self.dex_objects)
                    
                    if not variable:
                        variable = context.allocate(name, type_)
                        context[name] = variable
                    
                    elif not isinstance(variable, Variable):
                        raise TypeError("Can only assign an exception to a "
                            "local variable at line %i of file %s." % (
                            assignment.lineno, scope.file_name()))
                    
                    context.append(MoveException(variable))
                
                self.process_node(handler.body, context, scope)
                
                # Add a new suite to the current scope.
                context.add_suite()
                
                context.append(Goto(end_target))
            
            if node.orelse:
                raise TypeError("Unsupported else clause at line %i of file %s." % (
                    node.lineno, scope.file_name()))
            else:
                # Discard the last empty suite added.
                context.drop_suite()
            
            context.append(end_target)
            
            # Free the variables in the local scope and leave the scope.
            context.free_group()
            context.leave_scope()
            
            # Add the information about this structure to the context.
            try_info = TryHandlers(try_begin_target, try_end_target, handlers)
            context.tries.append(try_info)
            
            return None
        
        elif isinstance(node, ast.Raise):
        
            # Insert a placeholder instruction for use when creating listings.
            context.append(Source(node))
            
            if node.type == node.inst == node.tback == None:
                ### Raise the existing exception.
                raise TypeError("Raising an existing exception is currently "
                    "unsupported at line %i of file %s." % node.lineno, scope.file_name())
            
            if node.type != None:
                exception_obj = self.process_node(node.type, context, scope)
            else:
                exception_obj = None
            
            if not isinstance(exception_obj, Variable):
                raise TypeError("Only an instance can be passed to the raise "
                    "statement at line %i of file %s." % (node.lineno, scope.file_name()))
            
            context.append(Throw(exception_obj))
            
            if exception_obj.temporary():
                context.free()
            
            return None
        
        elif is_sequence(node) or isinstance(node, ast.Set):
        
            # A literal list can be empty or contain values which are constants or
            # existing variables or fields.
            
            # Create a linked list with an item type that must be filled in later.
            if isinstance(node, ast.Set):
                container_class = self.dex_objects.default_container_classes["java.util.HashSet"]
            else:
                container_class = self.dex_objects.default_container_classes["java.util.LinkedList"]
            
            class_ = TemplateClass(container_class, [None])
            class_name = create_class_name(class_)
            instance_var = context.allocate(type_=class_)
            context.append(NewInstance(instance_var, class_name))
            
            # Call the "argumentless" init method specifically for this class,
            # previously using the check_base_class=False argument to the
            # add_method_call function.
            self.call_method(class_, "__init__", [instance_var], node, context,
                             scope, result_obj = False)
            
            for item_node in node.elts:
            
                item = self.process_node(item_node, context, scope)
                
                # Define the item type if not yet defined. Otherwise check the
                # items in the list for consistency.
                if class_.item_types_ == [None]:
                    class_.item_types_ = [item.type_]
                
                elif not self.compatible_types(item.type_, class_.item_types_[0]):
                
                    # Find a common type and update the item types.
                    common_type = get_common_type(item.type_, class_.item_types_[0])
                    
                    if common_type:
                        class_.item_types_ = [common_type]
                    else:
                        raise TypeError("Inconsistent types in definition at "
                            "line %i of file %s." % (item_node.lineno, scope.file_name()))
                
                # Add each item to the list.
                self.call_method(instance_var, "add", [item], node, context, scope,
                                 result_obj = False)
                
                if item.temporary():
                    context.free()
            
            return instance_var
        
        elif isinstance(node, ast.Dict):
        
            # A literal dictionary can be empty or contain values which are
            # constants or existing variables or fields.
            
            # Create a hash map with an item type that must be filled in later.
            container_class = self.dex_objects.default_container_classes["java.util.HashMap"]
            class_ = TemplateClass(container_class, [None, None])
            
            class_name = create_class_name(class_)
            instance_var = context.allocate(type_=class_)
            context.append(NewInstance(instance_var, class_name))
            
            # Call the "argumentless" init method specifically for this class,
            # previously using the check_base_class=False argument to the
            # add_method_call function.
            self.call_method(class_, "__init__", [instance_var], node, context,
                             scope, result_obj = False)
            
            for key_node, value_node in zip(node.keys, node.values):
            
                key = self.process_node(key_node, context, scope)
                value = self.process_node(value_node, context, scope)
                
                # Define the item types if not yet defined. Otherwise check the
                # items in the list for consistency.
                if class_.item_types_ == [None, None]:
                    class_.item_types_ = [key.type_, value.type_]
                
                elif key.type_ != class_.item_types_[0]:
                    raise TypeError("Inconsistent types in definition at line "
                        "%i of file %s. Found %s but expected %s." % (
                        key_node.lineno, class_.item_types_[0], key.type_))
                
                elif value.type_ != class_.item_types_[1]:
                    raise TypeError("Inconsistent types in definition at line %i "
                        "of file %s. Found %s but expected %s." % (
                        value_node.lineno, class_.item_types_[1], value.type_))
                
                # Add each entry to the dictionary, discarding any return value.
                self.call_method(instance_var, "put", [key, value], node, context,
                                 scope, result_obj = False)
                
                if value.temporary():
                    context.free()
                
                if key.temporary():
                    context.free()
            
            return instance_var
        
        elif isinstance(node, ast.FunctionDef):
            raise SyntaxError("Nested functions are not supported at line %i "
                    "of file %s." % (node.lineno, scope.file_name()))
        
        raise ValueError("Unhandled node %s" % repr(node))
    
    
    def process_constant(self, value, node, scope, context):
    
        # Assign the constant to a temporary variable. This could be optimised
        # away if we returned constants to the caller for them to be processed
        # with other values there.
        node_type = type(value)
        
        try:
            const_type, value = value_to_type(value)
        except KeyError:
            raise TypeError("Unhandled constant type '%s' at line %i of file %s." % (
                node_type, node.lineno, scope.file_name()))
        
        temporary = context.allocate(type_=const_type)
        context.append(LoadConstant(temporary, value))
        
        # If the constant is a string then add it to the main collection now
        # so that it can be sorted before its index is taken.
        if type(value) == str or type(value) == unicode:
            self.dex_objects.strings.add(value)
        
        return temporary
    
    
    augmented_operations = {
        ast.Add, ast.Sub, ast.Mult, ast.Div, ast.FloorDiv, ast.Mod, ast.Pow,
        ast.RShift, ast.LShift, ast.BitAnd, ast.BitXor, ast.BitOr
        }
    
    def process_binary_operation(self, left_node, right_node, operation,
                                 context, scope):
    
        ### We may need to change the way allocation is performed if the
        ### resulting type has a different size to the operand types. For now,
        ### use the type of the first operand.
        
        left = self.process_node(left_node, context, scope)
        if left is None:
            raise NameLookupError("Failed to find '%s' at line %i of file %s." % (
                left_node, left_node.lineno, scope.file_name()))
        
        right = self.process_node(right_node, context, scope)
        if right is None:
            raise NameLookupError("Failed to find '%s' at line %i of file %s." % (
                right_node, right_node.lineno, scope.file_name()))
        
        original_left = left
        original_right = right
        
        # If the types differ then try to cast one to match the other.
        if left.type_ != right.type_:
            left, right, to_free = self.cast_operand(left, right, context,
                                                     scope, left_node)
        
        elif is_class(left.type_) and is_class(right.type_):
            # Handle convenience operations on object types.
            result = self.process_binary_convenience_operation(left, right,
                operation, left_node, context, scope)
            
            if not result:
                raise TypeError("Unsupported types in '%s' operation on line %i of file %s." % (
                    operation, left_node.lineno, scope.file_name()))
            
            return result
        else:
            to_free = None
        
        # Handle operand types.
        type_ = left.type_
        
        operator = get_type_operation_instruction(type_, operation, left_node, scope)
        
        if to_free is None:
        
            if left == original_left and right == original_right:
            
                # No casting or in-place casting. Sizes of operands should be
                # compatible.
                
                if left.temporary():
                    # Use the left operand's slot for the result. Either the right
                    # operand is a named variable or can be freed.
                    
                    # left right (t?) -> result (t) right (t?)
                    result = left
                    
                    if right.temporary():
                        # -> result
                        context.free()
                
                elif right.temporary():
                    # The left operand is not temporary and the right operand is.
                    # Use the right operand's slot for the result. 
                    
                    # right -> result
                    result = right
                
                else:
                    # If neither of the operands are temporary variables then allocate
                    # a temporary variable for the result.
                    
                    # ... -> result
                    result = context.allocate(type_=type_)
                
                context.append(OperationMacro(operator, result, [left, right]))
            
            else:
            
                if original_left != left:
                
                    # A temporary variable was allocated to hold a casted left
                    # operand:
                    #    original_left right left
                    # or the original left operand was aliased:
                    #    left right
                    
                    if original_left.is_alias(left):
                        # The original left slot was reused, which means that it is
                        # temporary.
                        to_free = [right, left]
                    else:
                        # A new slot was allocated for the casted variable.
                        to_free = [left, right, original_left]
                
                elif original_right != right:
                
                    # A temporary variable was allocated to hold a casted right
                    # operand:
                    #    left original_right right
                    # or the original right operand was aliased:
                    #    left right
                    
                    if original_right.is_alias(right):
                        # The original right slot was reused, which means that it
                        # is temporary.
                        to_free = [right, left]
                    else:
                        # A new slot was allocated for the casted variable.
                        to_free = [right, original_right, left]
                
                # Free all temporary operand variables and allocate a new variable
                # for the result.
                
                for operand in to_free:
                    if operand.temporary():
                        context.free()
                
                result = context.allocate(type_=type_)
                context.append(OperationMacro(operator, result, [left, right]))
        
        else:
            # An additional variable was allocated to hold a casted value to
            # replace an original temporary left operand of a smaller size.
            # The original operand needs to be freed in addition to others.
            #
            # Conceptually, we have this situation with allocated variables:
            #   left right -> to_free right left'
            # where left' is the newly-allocated variable and right may be
            # temporary.
            #
            # If the right operand is temporary then the allocated temporary
            # variables are these:
            #   to_free right left
            #
            # If the right operand is not temporary then the situation is this:
            #   to_free left
            #
            # So we either free two or three variables and allocate a new variable
            # in place of the original left operand to hold the result.
            
            if right.temporary():
                context.free()
            
            context.free()
            context.free()
            
            result = context.allocate(type_=left.type_)
            
            context.append(OperationMacro(operator, result, [left, right]))
        
        # Leave the result variable on the stack.
        return result
    
    
    def process_binary_convenience_operation(self, left, right, operation, node,
                                             context, scope):
        if operation == ast.Add:
        
            # Concatenation or addition
            
            if left.temporary():
                result = left
            elif right.temporary():
                result = right
            else:
                result = context.allocate(type_=left.type_)
            
            for method_name in "add", "concat":
            
                # Try to call a suitable method in the object.
                try:
                    self.call_method(left, method_name, [right], node, context,
                                     scope, result_obj = result)
                except NameLookupError:
                    continue
                
                if result == left and right.temporary():
                    context.free()
                
                return result
            
            else:
                raise NameLookupError("Failed to find a suitable operator "
                    "method in '%s' at line %i of file %s." % (
                    left.type_.name(), node.lineno, scope.file_name()))
        
        return None
    
    
    def process_boolean_operations(self, node, context, scope):
    
        # Assume that the result will have the same size as the operands.
        
        # Start with the first value and apply each following value.
        left = self.process_node(node.values[0], context, scope)
        
        # Handle operand types.
        type_ = left.type_
        
        operator = get_type_operation_instruction(type_, node.op.__class__,
                                                  node, scope)
        
        # If the first operand is temporary then reuse it for the result.
        if left.temporary():
            result = left
        else:
            result = context.allocate(type_=type_)
        
        short_circuit_target = Target("short-circuit-%x" % id(node))
        
        i = 1
        while i < len(node.values):
        
            # Multiple operations combined with And operators can be short-circuited
            # if any of them yield a false result. Those combined with Or operators
            # can be short-circuited with a true result.
            
            # If the first operand in the sequence is held in a separate variable
            # to the result, move it into the result in case the short-circuit
            # succeeds. Otherwise, we could arrive at the end before the result is
            # defined.
            if result.temporary() and i == 1:
                context.append(Move(result, left))
            
            if isinstance(node.op, ast.And):
                context.append(CompareZeroMacro(if_eqz, short_circuit_target, left))
            
            elif isinstance(node.op, ast.Or):
                context.append(CompareZeroMacro(if_nez, short_circuit_target, left))
            
            # Process the next operand. This may result in a variable being
            # allocated.
            right = self.process_node(node.values[i], context, scope)
            context.append(OperationMacro(operator, result, [left, right]))
            
            if right.temporary():
                context.free()
            
            left = result
            
            i += 1
        
        context.append(short_circuit_target)
        
        # Leave the result variable on the stack.
        return result
    
    
    def process_comparison(self, node, context, scope, keep_result = True):
    
        """Process the comparison node, creating jump targets for successful
        comparisons as well as a failure target and an end target. The end target
        is only useful for the case where we generate code to store the result of
        the comparison in a boolean variable.
        """
        
        # A comparison performs a chained sequence of operations, jumping to
        # a failure routine if any of them fail, and falling through to a
        # success routine if they all succeed.
        
        context.allocate_group()
        
        # Evaluate the expression first.
        left = self.process_node(node.left, context, scope)
        if left is None:
            raise TypeError("Unknown type %s in expression at line %i of file "
                "%s." % (repr(node.left), node.left.lineno, scope.file_name()))
        
        # Create a target object that will be used as a branch target for when
        # the comparison fails.
        failure_target = Target("fail-%x" % id(node))
        
        previous = left
        
        for i, (op, expr) in enumerate(zip(node.ops, node.comparators)):
        
            op = op.__class__
            
            right = self.process_node(expr, context, scope)
            if right is None:
                raise TypeError, "Unknown type %s in expression at line %i of file %s." % (
                    expr, expr.lineno, scope.file_name())
            
            # Use a variable to hold the type of the previous operand to save typing.
            type_ = previous.type_
            
            ### We should be able to optimise cases where one operand is a None
            ### constant to a if-**z instruction, discarding the previous
            ### instruction and freeing the temporary variable.
            do_comparison = True
            object_comparison = False
            contains_comparison = False
            null_operand = None
            
            if op == ast.In:
                contains_comparison = True
            
            elif right.type_ != type_ or is_class(type_) or is_class(right.type_):
            
                if isinstance(type_, Null) and is_class(right.type_):
                    null_operand = right
                
                elif isinstance(right.type_, Null) and is_class(type_):
                    null_operand = previous
                
                elif is_class(right.type_) and is_class(type_):
                    # Both operands are objects.
                    object_comparison = True
                
                else:
                    casted_type = get_type_to_cast_to(type_, right.type_)
                    
                    if casted_type == type_:
                    
                        # The operand types are the same size or the previous one is
                        # larger. Cast the right hand variable to the previous type.
                        
                        if right.temporary():
                            # Reuse the existing variable for the casted variable.
                            casted = context.alias(right, type_)
                        else:
                            casted = context.allocate(type_=type_)
                        
                        self.cast_primitive(right, casted, node, context, scope)
                        right = casted
                    
                    elif casted_type == right.type_:
                    
                        # The previous operand type is smaller than the one on the
                        # right hand side. Cast the previous variable to the type of
                        # the right hand operand.
                        
                        if previous.temporary() and not right.temporary():
                            # Reuse the existing variable for the casted variable.
                            casted = context.alias(previous, right.type_)
                        else:
                            # Allocate a new temporary variable to hold the casted
                            # result. In theory we would need to ensure that the
                            # original previous variable is freed if it is also
                            # temporary but the free_group call below deals with
                            # this. This works fairly well because, in a chain of
                            # comparisons, casting to a larger type should occur at
                            # most once.
                            casted = context.allocate(type_=right.type_)
                        
                        self.cast_primitive(previous, casted, node, context, scope)
                        
                        # Update the variable holding the previous operand and
                        # record its new type in the variable holding its type.
                        previous = casted
                        type_ = previous.type_
                    
                    else:
                        raise TypeError("Invalid combination of types in "
                            "comparison '%s %s %s' at line %i of file %s." % (
                            type_, op, right.type_, expr.lineno, scope.file_name()))
            
            elif isinstance(right.type_, Null) and isinstance(type_, Null):
                # The comparison automatically succeeds.
                do_comparison = False
            
            if do_comparison:
            
                # Create a target object that will be used as a branch target for when
                # the comparison succeeds.
                success_target = Target("success-%x" % id(op))
                
                if contains_comparison:
                    self.perform_contains_comparison(previous, right, expr, op,
                        success_target, context, scope)
                elif object_comparison:
                    self.perform_object_comparison(previous, right, expr, op,
                        success_target, context, scope)
                else:
                    self.perform_comparison(type_, op, previous, right,
                        null_operand, success_target, context, node, scope)
                
                # Include a branch to the failure target followed by the success
                # target so that comparisons can continue.
                context.append(Goto(failure_target))
                context.append(success_target)
            
            if previous.temporary() and right.temporary():
                # If the current and previous operands are temporary then free
                # the current one and copy the current one over the previous
                # one. This only works if both operands are the same type.
                context.free()
                # Only copy the current operand over the previous if there are more
                # comparisons to process.
                if i < len(node.ops) - 1 and previous != right:
                    context.append(Move(previous, right))
                    # Create a new variable with the same register as the previous
                    # operand but with the type of the right hand operand.
                    right = context.alias(previous, right.type_)
            
            elif previous.temporary():
                # If only the previous operand is temporary then it is at the top
                # of the stack, so free it.
                context.free()
            
            previous = right
        
        # Free the last operand, if it was temporary.
        context.free_group()
        
        if keep_result:
            # Allocate a temporary variable and assign a boolean result to it.
            temporary = context.allocate(type_=dBoolean)
            
            # Assign the result (true) to the temporary variable.
            context.append(LoadConstant(temporary, TRUE))
            # Create an ending target and jump to it.
            end_target = Target("end-%x" % id(node))
            context.append(Goto(end_target))
            
            # Append the failure target so that the failure code can be executed.
            context.append(failure_target)
            
            # Assign the result (false) to the temporary variable.
            context.append(LoadConstant(temporary, FALSE))
            # Append the end target.
            context.append(end_target)
            return temporary
        else:
            # Return the as-yet-unused failure target.
            return failure_target
    
    
    def perform_comparison(self, type_, op, previous, right, null_operand,
                           success_target, context, node, scope):
    
        # Perform a comparison. This will copy the operands to temporary
        # registers that are suitable for comparison operations.
        
        if null_operand is None:
        
            # Neither of the operands are None (null).
            
            if isinstance(type_, Float) or isinstance(type_, Double) or \
               isinstance(type_, Long):
            
                result = context.allocate(type_=dInt)
                context.append(FloatComparisonMacro(result, previous, right))
                
                # Compare the result with the expected value for the operation
                # being performed.
                operator = get_type_comparison_instruction(type_, op, node, scope)
                ins = CompareZeroMacro(operator, success_target, result)
                context.free()
            
            else:
                operator = get_type_comparison_instruction(type_, op, node, scope)
                ins = ComparisonMacro(operator, success_target, [previous, right])
        else:
            # Use the integer type instructions for null comparisons.
            operator = type_comparison_instructions[Int][op]
            ins = ComparisonMacro(operator, success_target, [previous, right])
        
        context.append(ins)
    
    
    def perform_object_comparison(self, left, right, node, op, success_target,
                                  context, scope):
    
        if op == ast.Eq or op == ast.NotEq:
            # Assume that the first operand supports the equals method, allowing
            # the search to raise an exception on failure.
            temporary = self.call_method(left, "equals", [right], node, context,
                                         scope)
        else:
            # Assume that the first operand supports comparison, allowing the
            # search to raise an exception on failure.
            temporary = self.call_method(left, "compareTo", [right], node,
                                         context, scope)
        
        if op == ast.Eq:
            context.append(CompareZeroMacro(if_nez, success_target, temporary))
        elif op == ast.NotEq:
            context.append(CompareZeroMacro(if_eqz, success_target, temporary))
        
        # The result of the compareTo call is an integer that corresponds to the
        # relative values of the operands. This value is tested to see if it
        # matches the values expected by the relevant operation.
        
        elif op == ast.LtE:
            context.append(CompareZeroMacro(if_lez, success_target, temporary))
        elif op == ast.Lt:
            context.append(CompareZeroMacro(if_ltz, success_target, temporary))
        elif op == ast.GtE:
            context.append(CompareZeroMacro(if_gez, success_target, temporary))
        elif op == ast.Gt:
            context.append(CompareZeroMacro(if_gtz, success_target, temporary))
        else:
            raise TypeError("Unknown comparison operation at line %i of file %s." % (
                node.lineno, scope.file_name()))
        
        # Free the temporary variable.
        context.free()
    
    
    def perform_contains_comparison(self, left, right, node, op, success_target,
                                    context, scope):
        
        # Determine whether the right hand operand is an array or other container.
        is_an_array, container_type, item_types = array_or_container(right, node)
        
        if is_an_array:
            container_type = self.dex_objects.builtins.find_object("Arrays")
            temporary = self.call_method(container_type, "contains",
                                         [right, left], node, context, scope)
        
        else:
            callee = MethodCall(right, "containsKey", [left])
            
            if self.find_matching_method(callee, scope, node, quiet=True):
                temporary = self.call_method(right, "containsKey", [left], node,
                                             context, scope)
            else:
                temporary = self.call_method(right, "contains", [left], node,
                                             context, scope)
        
        # Jump to the success target if the result is non-zero (true).
        context.append(CompareZeroMacro(if_nez, success_target, temporary))
        
        # Free the temporary variable.
        context.free()
    
    
    def process_conditional_structure(self, node, context, scope):
    
        # Enter a local scope. Variables defined are local to the scope.
        context.enter_scope()
        context.allocate_group()
        
        begin_target = Target("begin-%x" % id(node))
        end_target = Target("end-%x" % id(node))
        
        if isinstance(node, ast.While):
            context.append(begin_target)
        
        # Collect the new local variables defined in each of the child suites of
        # this structure, as well as the number of suites present.
        new_locals = {}
        
        # Record whether the node body should be visited.
        visit_body = True
        
        # Record whether the else suite is needed.
        need_else = True
        
        # Visit the test.
        test = node.test
        
        # The test can be a boolean constant, a variable or a comparison.
        if isinstance(test, ast.Compare):
            failure_target = self.process_comparison(test, context, scope,
                keep_result = False)
        
        elif isinstance(test, ast.Name) and test.id == "False":
            visit_body = False
        
        elif isinstance(test, ast.Name) and test.id == "True":
            # If the test node is a True constant then we do not need an
            # else node for new locals to be exported into the enclosing
            # scope.
            need_else = False
        
        else:
            # Process the node and handle the result, which should be a
            # temporary variable of boolean type.
            result = self.process_node(test, context, scope)
            
            if not isinstance(result.type_, Boolean):
            
                # If the result type cannot be interpreted as a boolean then
                # raise an exception.
                if not is_primitive(result.type_) or \
                   result.type_.__class__ not in (Int, Float, Double, Long):
                
                    raise TypeError("Invalid result type '%s' for test at "
                        "line %i of file %s." % (repr(result.type_),
                        test.lineno, scope.file_name()))
            
            # Create a failure target for use after the child suite.
            failure_target = Target("failure-%x" % id(test))
            
            # Perform a comparison. This will copy the operands to temporary
            # registers that are suitable for comparison operations.
            context.append(CompareZeroMacro(if_eqz, failure_target, result))
            
            # Free the result variable if temporary.
            if result.temporary():
                context.free()
        
        if visit_body:
        
            if isinstance(node, ast.While):
                self.dex_objects.suites.append((begin_target, end_target))
            
            # Visit the child suite.
            self.process_node(node.body, context, scope)
            
            # Record any assignments in this suite.
            for new_name, new_var in context.current_suite().items():
                new_locals.setdefault(new_name, []).append(new_var)
            
            # Add a new suite to the current scope so that the local assignments
            # can be collected for it.
            context.add_suite()
            
            if isinstance(node, ast.While):
                self.dex_objects.suites.pop()
            
            if isinstance(node, ast.If):
                # Immediately after the child suite, append a jump to the end of
                # the if suites.
                context.append(Goto(end_target))
            
            elif isinstance(node, ast.While):
                # Jump to the test again.
                context.append(Goto(begin_target))
            
            if need_else:
                # Append the failure target so that the next test can be applied.
                context.append(failure_target)
        
        # Process the else suite if it exists and is needed.
        if need_else and node.orelse:
        
            # Include the else suite.
            self.process_node(node.orelse, context, scope)
            
            # Record any assignments in this suite.
            for new_name, new_var in context.current_suite().items():
                new_locals.setdefault(new_name, []).append(new_var)
        else:
            # Discard the last empty suite added.
            context.drop_suite()
        
        # Append the end target.
        context.append(end_target)
        
        suites = len(context.all_suites())
        
        # Free the variables in the local scope and leave the scope.
        context.free_group()
        context.leave_scope()
        
        if not new_locals or (need_else and not node.orelse):
            return
        
        # Redefine the new locals.
        
        # Only retain the local variables that are assigned in all of the suites.
        # Additionally, there must be more than one suite defined, and if
        # statements must have corresponding else statements for variables to be
        # exported.
        common_locals = []
        
        for new_name, new_vars in new_locals.items():
            if len(new_vars) == suites:
                common_locals.append((new_vars[0].n, new_vars[0]))
        
        common_locals.sort()
        
        # Since we do not distinguish between assignments to new local variables
        # in the suites and assignments to existing ones in scopes outside the
        # structure, we need to separate them here. We only need to recreate new
        # local variables and ensure that their contents are moved into them.
        # The Move macro is necessary because other variables may also have been
        # allocated within the structure's scope so their new locations may be
        # different.
        
        for n, new_var in common_locals:
        
            name = new_var.name
            
            if name not in context.vars:
                v = context.allocate(name, new_var.type_)
            else:
                v = context.vars[name]
            
            context.append(Move(v, new_var))
            
            if name not in context.current_suite():
                context.current_suite()[name] = v
    
    
    def process_assignment(self, value_node, value_var, assignment_nodes, node,
                           context, scope):
    
        """Processes the assignment of a value to a number of objects, some of
        which may be undefined at the time of the assignment."""
        
        # Find the variables corresponding to the names in the assignment node,
        # checking consistency of types.
        variables = {}
        undefined = []
        type_ = None
        
        for exprnode in assignment_nodes:
        
            # Complete the variables dictionary and undefined list, discarding the
            # name, object and type found for each node.
            name, obj, type_ = self.process_assignment_node(exprnode, variables,
                undefined, type_, context, scope)
        
        # If the type has not been defined because none of the named variables
        # have been defined then process the assignment's expression.
        if type_ is None:
        
            type_from_expression = True
            if value_var != None:
                result = value_var
            else:
                result = self.process_node(value_node, context, scope)
            
            if result is None:
                raise ValueError, "Failed to evaluate value for assignment '%s' on line %i of file %s." % (
                    repr(value_node), value_node.lineno, scope.file_name())
            
            # The value to be assigned should be a value or an instance, so
            # acquire the type from it.
            type_ = result.type_
            
            # Allocate variables for undefined names, reusing the result of the
            # expression if it is a temporary variable.
            can_reassign = result.temporary()
            reassigned_result = False
            
            for name in undefined:
            
                if can_reassign and not reassigned_result:
                
                    # Reassign the result to be a named variable and add it to
                    # the dictionary of local variables.
                    #result = context.alias(result, result.type_)
                    result.name = name
                    reassigned_result = True
                    context[name] = result
                    context.current_suite()[name] = result
                
                else:
                    # Allocate a new variable and add it to the dictionary of
                    # variables to be assigned the value.
                    variable = context.allocate(name, type_)
                    
                    # Add the newly defined variable to the dictionary of local
                    # variables and update the assignment variables dictionary to
                    # use it.
                    context[name] = variable
                    variables[name] = variable
        else:
            # Since the types are consistent, create variables for the names
            # that are not defined.
            type_from_expression = False
            
            for name in undefined:
            
                # Add a new variable to the dictionary of local variables.
                variables[name] = context.allocate(name, type_)
                context[name] = variables[name]
            
            # Process the assignment's expression. This may allocate a
            # temporary variable (unless we later perform optimisations with
            # constants) that needs to be after any new variables allocated so
            # it can be freed later.
            if value_var != None:
                result = value_var
            else:
                result = self.process_node(value_node, context, scope)
            
            if result is None:
                raise ValueError, "Failed to evaluate value for assignment '%s' on line %i of file %s." % (
                    repr(value_node), value_node.lineno, scope.file_name())
            
            # Check that the result type can be assigned to the variable.
            if not self.compatible_types(result.type_, type_, add_missing = True):
                raise TypeError, (
                    "Assignment value type '%s' does not match that of the "
                    "variables assigned to '%s' at line %i of file %s." % (
                        result.type_, type_, value_node.lineno, scope.file_name())
                    )
        
        # Copy the result into the assignment targets, keeping the original
        # result so that it can be cast for each target as necessary.
        original_result = result
        
        # Record whether the result object itself is assigned - this will already
        # have been done if there were undefined local variables referred to.
        # If only a tuple assignment occurs then the object will need to be
        # discarded and any newly defined variables moved.
        result_assigned = (undefined != [])
        
        for name, obj in variables.items():
        
            if isinstance(obj, ast.Attribute):
                self.process_attribute_assignment_node(obj, original_result,
                    type_, node, context, scope)
                result_assigned = True
            
            elif isinstance(obj, ast.Tuple):
                self.process_tuple_assignment_node(obj, original_result, type_,
                    node, context, scope)
            
            elif isinstance(obj, ast.Subscript):
                self.process_subscript_assignment_node(obj, original_result,
                    type_, node, context, scope)
                result_assigned = True
            else:
                # Undefined variables may have already been handled above.
                # Only handle ones that were already defined and those
                # that use different variables.
                if obj is not None and obj != result:
                
                    result = self.use_compatible_type(original_result, type_,
                        node, context, scope, add_missing = True)
                    
                    if not result:
                        raise TypeError, (
                            "Assignment value type '%s' does not match that of the "
                            "variables assigned to '%s' at line %i of file %s." % (
                                original_result.type_, type_, value_node.lineno, scope.file_name())
                            )
                    elif original_result != result:
                        # Free the temporary casted variable, if one was allocated.
                        context.free()
                    
                    context.append(Move(obj, result))
                    context.current_suite()[obj.name] = obj
                    result_assigned = True
        
        if not result_assigned:
            # If the result was not assigned then discard it, moving the values
            # held in any new local variables to new local variables. Ideally we
            # would just reassign the variable register numbers but temporary
            # variables will have been allocated in the meantime and we don't have
            # access to those any longer.
            
            if original_result.temporary():
            
                # Find all the new locals that follow the original result.
                index = context.allocated.index(original_result)
                new_locals = context.allocated[index + 1:]
                
                # Free the result and all the new locals before allocating new
                # variables.
                context.free_including(original_result)
                
                for new_local in new_locals:
                    v = context.allocate(name=new_local.name, type_=new_local.type_)
                    context.append(Move(v, new_local))
        
        elif type_from_expression:
            # If the result is temporary and has not been reassigned to a new
            # variable then it should still be at the top of the stack, so we
            # can now free it. If the result is temporary then it will have
            # been reassigned. Otherwise, it referred to a persistent variable.
            if can_reassign and not reassigned_result:
                context.free()
        else:
            # If the result is temporary then it should be at the top of the
            # stack, so we can now free it.
            if original_result.temporary():
                context.free()
    
    
    def process_assignment_node(self, exprnode, variables, undefined, type_,
                                context, scope):
    
        """Updates the dictionary of variables referred to, the list of undefined
        variables and the list of types involved in the assignment.
        If the assignment is to a local variable, the name will be that of the
        variable and the corresponding object will be a Variable instance, or None
        if the variable has not yet been defined.
        If the assignment is to an attribute or an element in a collection, the
        name will be None and the exprnode will be returned as the object for
        further processing by the caller."""
        
        name = None
        
        if isinstance(exprnode, ast.Name):
        
            # Assigning to a local variable.
            name = exprnode.id
            obj = find_object(name, scope, context, self.dex_objects, for_assignment = True)
            variables[name] = obj
        
        elif isinstance(exprnode, ast.Attribute):
        
            # Assigning to an attribute of a container. Since this may
            # involve generation of instructions and creation of temporary
            # variables, we defer processing until later.
            obj = exprnode
            variables[obj] = obj
        
        elif isinstance(exprnode, ast.Tuple):
        
            # Assigning to elements of a tuple.
            obj = exprnode
            variables[obj] = obj
        
        elif isinstance(exprnode, ast.Subscript):
        
            # Assigning to an item in a container. As for the previous
            # case, defer processing.
            obj = exprnode
            variables[obj] = obj
        
        else:
            raise NameLookupError("Unhandled assignment node '%s' at line %i of file %s." % (
                exprnode, exprnode.lineno, scope.file_name()))
        
        # If the assignment target was found, check that its type matches
        # those of any targets already found. If not, add it to the list of
        # undefined targets.
        if obj is not None:
            if type_ is not None:
                if not self.compatible_types(obj.type_, type_):
                    raise TypeError("Inconsistent types (%s and %s) in assignment "
                        "at line %i of file %s." % (type_, obj.type_,
                        exprnode.lineno, scope.file_name()))
            
            elif isinstance(exprnode, ast.Name):
                type_ = obj.type_
        
        elif name not in undefined:
            # Only put undefined local variables in the list.
            undefined.append(name)
        
        # Return the assignment type, whether it was defined or not.
        return name, obj, type_
    
    
    def process_attribute_assignment_node(self, obj, original_result, type_,
                                          node, context, scope):
    
        # Unless the value being assigned to the attribute needs to be converted to
        # a more suitable type, it will be identical to the value obtained from the
        # expression.
        result = original_result
        
        exprnode, expr, attrname = obj, obj.value, obj.attr
        
        container_obj = self.process_node(expr, context, scope)
        
        if container_obj is None:
            raise NameLookupError("Failed to find '%s' at line %i of file %s." % (
                expr, expr.lineno, scope.file_name()))
        
        # Find the attribute of the container, regardless of the
        # container's type, and record both the container and the
        # attribute.
        obj = find_attribute(container_obj, attrname, exprnode, scope)
        
        # If the attribute target is a field then resolve its type, in
        # case it hasn't been accessed before.
        if isinstance(obj, visitor.Field):
            type_ = self.create_value_type(obj.type_)
            
            # Check that the result of the expression has the same type
            # as the field, filling in missing information if the
            # result type is incomplete, as it will be for empty lists
            # and dictionaries.
            result = self.use_compatible_type(original_result, type_, node,
                context, scope, add_missing = True)
            
            if not result:
                raise TypeError, (
                    "Assignment value type '%s' does not match that of the "
                    "variables assigned to field '%s' of type '%s' at line %i of file %s." % (
                        original_result.type_, obj.name, type_, node.lineno, scope.file_name())
                    )
            elif original_result != result:
                # Free the temporary casted variable, if one was allocated.
                context.free()
        
        elif type(obj) == list:
            # A list of methods was found.
            raise TypeError, "Cannot assign to '%s' at line %i of file %s." % (
                obj[0].full_name(), expr.lineno, scope.file_name())
        
        elif obj is not None:
            # Safety: obj can be None or a field, but nothing else.
            raise TypeError, "Cannot assign to '%s' at line %i of file %s." % (
                repr(obj), expr.lineno, scope.file_name())
        
        else:
            # Find the class for the container object type.
            class_ = class_from_container(container_obj)
            
            # Use the result type to define the field.
            if isinstance(type_, Null):
                raise TypeError("Cannot set a field to null without a __fields__ "
                    "declaration at line %i of file %s." % (
                    node.lineno, scope.file_name()))
            
            field = visitor.Field(class_, type_, attrname)
            
            try:
                # Check against any existing field.
                if class_.fields[attrname].type_ != type_:
                    raise TypeError("Cannot redefine the type of field '%s.%s' at "
                        "line %i of file %s." % (class_.name(), attrname,
                        node.lineno, scope.file_name()))
            except KeyError:
                # No existing field, so try to define one if we are in
                # the same class.
                if scope.parent == class_:
                    class_.fields[attrname] = field
                else:
                    raise TypeError("Cannot define the type of instance field "
                        "'%s.%s' from here at line %i of file %s." % (
                        container_obj.name, attrname, node.lineno, scope.file_name()))
        
        # Write the instruction to set the attribute.
        class_name = create_dex_type(container_obj)
        
        dex_field = Field(class_name, create_dex_type(type_),
                          MemberName(attrname))
        
        # Record that the field was used.
        self.dex_objects.fields.add(dex_field)
        
        context.append(SetAttr(result, dex_field, container_obj))
        
        if container_obj.temporary():
            context.free()
    
    
    def process_tuple_assignment_node(self, obj, sequence, type_, node, context,
                                      scope):
    
        is_an_array, container_type, item_types = array_or_container(sequence, node)
        
        if not item_types:
            raise TypeError("Incomplete type '%s' used in tuple assignment at "
                "line %i of file %s." % (sequence.type_, node.lineno, scope.file_name()))
        
        # Iterate over the nodes stored in the AST node, obtaining a destination
        # object for each node. Generate code to extract the corresponding value
        # from the sequence object and store it in the destination object.
        for i, assign_node in enumerate(obj.elts):
        
            # Allocate space for a value.
            item_var = context.allocate(type_=item_types[0])
            
            # Allocate space for an index to use with the container.
            index_obj = context.allocate(type_=dInt)
            context.append(LoadConstant(index_obj, i))
            
            if is_array(sequence.type_):
                self.get_array_item(sequence, item_var, item_types[0],
                                    index_obj, node, context, scope)
            else:
                self.call_method(sequence, "get", [index_obj], node, context,
                                 scope, result_obj = item_var,
                                 exception_type = IndexError)
            
            # Discard the index object.
            context.free()
            
            # Pass the item variable instead of an AST node to the assignment
            # handling function with a list containing only this assignment node.
            self.process_assignment(None, item_var, [assign_node], node,
                context, scope)
    
    
    def process_subscript_assignment_node(self, obj, original_result, type_,
                                          node, context, scope):
    
        # Unless the value being assigned to the subscripted object needs to be
        # converted to a more suitable type, it will be identical to the value
        # obtained from the expression.
        result = original_result
        
        context.allocate_group()
        
        # Get the array and index objects. If these are temporary variables then
        # they will be freed so that we are ready to allocate a variable to hold
        # the result.
        array_obj, index_obj, is_an_array, container_type, item_types = \
            self.get_subscript_objects(obj, context, scope)
        
        # Don't allow assignment to lists that have not been initialised.
        if not is_an_array and item_types == [None]:
            raise TypeError, "Undefined item type for %s at line %i of file %s." % (
                repr(array_obj), node.lineno, scope.file_name())
        
        # If not a container of any type, report an error.
        if not item_types:
            raise TypeError, "Cannot assign to elements of %s at line %i of file %s." % (
                repr(array_obj), node.lineno, scope.file_name())
        
        item_type = item_types[-1]
        
        if item_type is not None:
        
            # Check type compatibility for the item being stored.
            result = self.use_compatible_type(original_result, item_type,
                                              node, context, scope)
            
            if not result:
                raise TypeError, "Inconsistent types in assignment at line %i of file %s." % (
                    node.lineno, scope.file_name())
            
            elif original_result != result:
                # Free the temporary casted variable, if one was allocated.
                context.free()
        else:
            # Define the item type for the container.
            array_obj.type_.item_types_[-1] = type_
        
        if len(item_types) == 1:
        
            if is_an_array:
                if array_obj.type_.wrapped_items:
                    # The array contains wrapped items instead of the
                    # items directly. 
                    result = self.wrap_item(node, result, context, scope)
                
                context.append(ArrayPut(result, array_obj, index_obj))
            else:
                # Call a suitable set method in the object, discarding the result.
                self.call_method(array_obj, "set", [index_obj, result], node,
                                 context, scope, result_obj = False)
        
        elif len(item_types) == 2:
        
            # Check the index type.
            index_type = item_types[0]
            
            if index_type is not None:
            
                original_index_obj = index_obj
                index_obj = self.use_compatible_type(original_index_obj,
                    index_type, node, context, scope)
                
                if not index_obj:
                    raise TypeError, "Invalid type '%s' for key at line %i." % (
                        original_index_obj.type_, node.lineno)
                
                elif original_index_obj != index_obj:
                    # Free the temporary casted variable, if one was allocated.
                    context.free()
            else:
                # Define the key type for the container.
                array_obj.type_.item_types_[0] = index_obj.type_
            
            # Call the method to set the item, discarding the result.
            self.call_method(array_obj, "put", [index_obj, result], node,
                             context, scope, result_obj = False)
        else:
            raise TypeError, "Invalid type '%s' for index at line %i." % (
                index_obj.type_, node.lineno)
        
        context.free_group()
    
    
    def process_iteration(self, node, context, scope):
    
        name, obj, type_ = self.process_assignment_node(node.target, {}, [],
            None, context, scope)
        
        begin_target = Target("begin-%x" % id(node))
        end_target = Target("end-%x" % id(node))
        
        # Process the list node to find an iterable collection.
        iterable_obj = self.process_node(node.iter, context, scope)
        iterable_type = iterable_obj.type_
        
        if is_array(iterable_type):
            # Find the type of the items in the array.
            item_type = iterable_type.item_type
        else:
            # Find the types of the items in the iterable. Dictionaries contain two
            # item types, but sets and lists contain only one. In any case, we only use
            # the first type.
            item_type = self.get_item_types(iterable_type)[0]
        
        temporary_item = False
        
        if obj is None:
        
            # Assignment to a new local variable with the first item type.
            
            if iterable_obj.temporary():
                # If the iterable is temporary then allow the item to use the
                # slot occupied by it.
                context.free()
            
            # Allocate the new item variable. Even though it is a named variable,
            # it will be freed after the loop.
            item_var = context.allocate(name, item_type)
            temporary_item = True
            
            if iterable_obj.temporary():
                # The item will occupy the slot previously used by the iterable
                # but we need to move the iterable to the top of the stack first.
                old_iterable_obj = iterable_obj
                iterable_obj = context.allocate(None, iterable_obj.type_)
                context.append(Move(iterable_obj, old_iterable_obj))
        
        elif name is not None:
        
            # Assignment to an existing local variable.
            item_var = obj
        
        else:
            # For attribute and subscript assignments, create a temporary variable
            # to hold values of the item type.
            
            if iterable_obj.temporary():
                # For consistency, put the iterable at the top of the stack.
                context.free()
            
            item_var = context.allocate(type_=item_type)
            
            if iterable_obj.temporary():
                # The item will occupy the slot previously used by the iterable
                # but we need to move the iterable to the top of the stack first.
                old_iterable_obj = iterable_obj
                iterable_obj = context.allocate(None, iterable_obj.type_)
                context.append(Move(iterable_obj, old_iterable_obj))
        
        # Check whether the iterable object is actually null and skip any iteration
        # if so.
        context.append(CompareZeroMacro(if_eqz, end_target, iterable_obj))
        
        if is_array(iterable_type):
            # For arrays, use a temporary variable as an index into the array and
            # another to hold the size of the array.
            index_obj = context.allocate(type_=dInt)
            count_obj = context.allocate(type_=dInt)
            context.append(LoadConstant(index_obj, 0))
            context.append(ArrayLength(count_obj, iterable_obj))
        else:
            # For other containers, obtain an iterator, looking for a suitable
            # iterator method in the object.
            iterator_obj = self.call_method(iterable_obj, "iterator", [],
                                            node.iter, context, scope)
        
        # Enter the loop's scope. Variables defined are local to the loop.
        context.enter_scope()
        context.allocate_group()
        
        # Begin the loop.
        context.append(begin_target)
        
        # Create a loop exit target for use after the body suite.
        loop_exit_target = Target("loop-exit-%x" % id(node))
        
        if is_array(iterable_type):
            # Compare the index to the count.
            context.append(ComparisonMacro(if_eq, loop_exit_target, [index_obj, count_obj]))
        else:
            # Call the iterator's hasNext method.
            hasNext_obj = self.call_method(iterator_obj, "hasNext", [],
                                           node.iter, context, scope)
            
            if not isinstance(hasNext_obj.type_, Boolean):
                raise TypeError("Invalid result type '%s' for test at line %i of file %s." % (
                    repr(hasNext_obj.type_), node.iter.lineno, scope.file_name()))
            
            # Perform a comparison with a zero (False) value, jumping to the end target
            # if successful.
            context.append(CompareZeroMacro(if_eqz, loop_exit_target, hasNext_obj))
            
            # Free the variable holding the result of the hasNext call.
            context.free()
        
        if is_array(iterable_type):
        
            self.get_array_item(iterable_obj, item_var, item_type, index_obj,
                                node, context, scope)
        else:
            # Call the iterator's next method and assign the return value to the
            # item variable.
            next_obj = self.call_method(iterator_obj, "next", [], node.iter,
                                        context, scope, result_obj = item_var)
        
        # For attribute and subscript assigments, copy the result from the
        # temporary item to the relevant destination.
        if isinstance(obj, ast.Attribute):
            self.process_attribute_assignment_node(obj, item_var, item_var.type_,
                node, context, scope)
        
        elif isinstance(obj, ast.Tuple):
            self.process_tuple_assignment_node(obj, item_var, item_var.type_,
                node, context, scope)
        
        elif isinstance(obj, ast.Subscript):
            self.process_subscript_assignment_node(obj, item_var, item_var.type_,
                node, context, scope)
        
        # Visit the loop body.
        self.dex_objects.suites.append((begin_target, end_target))
        
        self.process_node(node.body, context, scope)
        
        self.dex_objects.suites.pop()
        
        if is_array(iterable_type):
            # Increment the array index.
            context.append(ConstantOperationMacro(add_int_lit8, index_obj, [index_obj, Constant(1, 8)]))
        
        # Jump to the test again.
        context.append(Goto(begin_target))
        
        # Append the loop exit target so that any else suite can be executed.
        context.append(loop_exit_target)
        
        if node.orelse:
        
            # Include the else suite.
            self.process_node(node.orelse, context, scope)
        
        # Append the end target.
        context.append(end_target)
        
        context.free_group()
        context.leave_scope()
        
        # If we wanted to allow loops that defined variables in their enclosing
        # scope, we could remove the enter_scope and leave_scope calls as well as
        # the allocate_group and free_group calls. At this point, we would then
        # reorder any newly allocated variables so that the iterator has the
        # highest register number, enabling us to free it cleanly.
        
        if is_array(iterable_type):
            # Free the index and count variables.
            context.free()
            context.free()
        else:
            # Free the iterator variable.
            context.free()
    
        # Free the iterable variable if temporary and not replaced by the item
        # variable.
        if iterable_obj.temporary():
            context.free()
        
        # Free the item variable if temporary.
        if item_var.temporary():
            context.free()
        elif temporary_item:
            context.free(warn = False)
    
    
    def get_subscript_objects(self, node, context, scope):
    
        """Returns the array or container object for the given subscript node, its
        index object, a boolean value indicating whether or not it is an array,
        the general container type (not a specific template class) and the item
        types if it is a template class."""
        
        # Temporary variables allocated as a result of this function must be freed
        # by the caller.
        
        array_obj = self.process_node(node.value, context, scope)
        if array_obj is None:
            raise NameLookupError, "Failed to find '%s' at line %i of file %s." % (
                node.value, node.lineno, scope.file_name())
        
        is_an_array, container_type, item_types = array_or_container(array_obj, node)
        
        index_obj = self.process_node(node.slice.value, context, scope)
        if index_obj is None:
            raise ValueError, "Invalid array subscript '%s' at line %i of file %s." % (
                node.slice.value, node.lineno, scope.file_name())
        
        return array_obj, index_obj, is_an_array, container_type, item_types
    
    
    def complete_dex_objects(self, package_name):
    
        # Extract types, strings, fields and methods from the classes.
        for class_ in self.dex_objects.classes.values():
        
            # Add types and string to the main collection.
            self.dex_objects.types.add(class_.class_type)
            self.dex_objects.types.add(class_.superclass_type)
            self.dex_objects.types.update(class_.interfaces)
            self.dex_objects.strings.add(class_.source_file)
            
            # Add the fields and methods to the main collection.
            
            # Extract the fields from the class data.
            static_fields = map(lambda (field, access): field, class_.class_data.static_fields)
            instance_fields = map(lambda (field, access): field, class_.class_data.instance_fields)
            self.dex_objects.fields.update(static_fields)
            self.dex_objects.fields.update(instance_fields)
            
            # Include all the methods from classes in the main package, even if
            # they are not called, and ensure that they are registered.
            # Only include methods from external packages if they are called.
            direct_methods = []
            for method in class_.class_data.direct_methods:
            
                method_package = method.package()
                included = (method_package == method.package()) or \
                           method_package.startswith("serpentine.")
                
                if included:
                    # If the method was called before it was defined then we need
                    # to replace its entry in the collection.
                    if method in self.dex_objects.methods:
                        self.dex_objects.methods.remove(method)
                    
                    self.dex_objects.methods.add(method)
                    direct_methods.append(method)
                else:
                    if method in self.dex_objects.methods:
                        direct_methods.append(method)
                        # Replace the existing entry with one with code.
                        if method.code != None:
                            # Note that the set type forces us to remove the entry
                            # if we want to replace it.
                            self.dex_objects.methods.remove(method)
                            self.dex_objects.methods.add(method)
            
            class_.class_data.direct_methods = direct_methods
            
            for method in class_.class_data.virtual_methods:
            
                if method in self.dex_objects.methods:
                    self.dex_objects.methods.remove(method)
                self.dex_objects.methods.add(method)
            
            # Extract strings and types from any annotations.
            if class_.annotations:
                self.read_annotations(class_.annotations.class_annotations)
                for field, annotations in class_.annotations.fields:
                    self.read_annotations(annotations)
                for method, annotations in class_.annotations.methods:
                    self.read_annotations(annotations)
                for parameters, annotations in class_.annotations.parameters:
                    self.read_annotations(annotations)
        
        # Extract types and strings from the fields.
        for field in self.dex_objects.fields:
        
            self.dex_objects.types.add(field.class_type)
            self.dex_objects.types.add(field.type_)
            self.dex_objects.strings.add(field.name.desc)
        
        # Extract types, prototypes and strings from the methods.
        for method in self.dex_objects.methods:
        
            self.dex_objects.types.add(method.class_type)
            self.dex_objects.prototypes.add(method.proto)
            self.dex_objects.strings.add(method.name.desc)
            
            # Extract types from the prototypes.
            for type_ in method.proto.shorty:
                if not isinstance(type_, ReferenceType):
                    self.dex_objects.types.add(type_)
            self.dex_objects.types.add(method.proto.return_type)
            self.dex_objects.types.update(method.proto.parameters)
            
            # Create strings from the prototypes.
            self.dex_objects.strings.add(u"".join(map(lambda t: t.desc, method.proto.shorty)))
        
        # Extract value types from arrays.
        value_types = set()
        for type_ in self.dex_objects.types:
        
            if isinstance(type_, Array):
                value_types.add(type_.item_type())
        
        self.dex_objects.types.update(value_types)
        
        # Extract strings from the types.
        for type_ in self.dex_objects.types:
        
            self.dex_objects.strings.add(type_.desc)
        
        # Sort the strings.
        self.dex_objects.strings = list(self.dex_objects.strings)
        self.dex_objects.strings.sort(cmp=utf8_sort)
        
        # Sort the types by the indices of their strings in the string list.
        type_indices = map(lambda type_: (self.dex_objects.strings.index(type_.desc), type_),
                           self.dex_objects.types)
        type_indices.sort()
        self.dex_objects.types = map(lambda (i, type_): type_, type_indices)
        
        # Sort the prototypes by the indices of their return types then by their
        # parameters in the types list.
        proto_indices = []
        
        for proto in self.dex_objects.prototypes:
        
            major = self.dex_objects.types.index(proto.return_type)
            minor = map(lambda type_: self.dex_objects.types.index(type_), proto.parameters)
            proto_indices.append((major, minor, proto))
        
        proto_indices.sort()
        self.dex_objects.prototypes = map(lambda (major, minor, proto): proto,
                                          proto_indices)
        
        # Sort the fields by the index of the class type in the types list, the
        # index of the field name in the strings list and the index of the type in
        # the types list.
        field_indices = []
        
        for field in self.dex_objects.fields:
        
            major = self.dex_objects.types.index(field.class_type)
            inter = self.dex_objects.strings.index(field.name.desc)
            minor = self.dex_objects.types.index(field.type_)
            field_indices.append((major, inter, minor, field))
        
        field_indices.sort()
        self.dex_objects.fields = map(lambda (major, inter, minor, field):
                                      field, field_indices)
        
        # Sort the methods by the index of the class type in the types list, the
        # index of the method name in the strings list and the index of the
        # prototype in the prototypes list.
        method_indices = []
        
        for method in self.dex_objects.methods:
        
            major = self.dex_objects.types.index(method.class_type)
            inter = self.dex_objects.strings.index(method.name.desc)
            minor = self.dex_objects.prototypes.index(method.proto)
            method_indices.append((major, inter, minor, method))
        
        method_indices.sort()
        self.dex_objects.methods = map(lambda (major, inter, minor, method): method,
                                       method_indices)
        
        # Ensure that superclasses and interfaces are listed before the classes
        # that refer to them.
        class_dict = {}
        
        for class_ in self.dex_objects.classes.values():
            class_dict[class_.class_type] = class_
        
        # Keep a copy of this dictionary for future reference. The original will
        # be gradually deleted as classes are added to the ordered list.
        self.dex_objects.class_dict = class_dict.copy()
        
        ordered_classes = []
        self.add_ordered_classes(self.dex_objects.classes.values(), class_dict, ordered_classes)
        
        self.dex_objects.ordered_classes = ordered_classes
    
    
    def read_annotations(self, annotations):
    
        # The annotations are a list of tuples, each of which contains the
        # visibility of the annotation and the annotation itself.
        for visibility, annotation in annotations:
        
            # Add the type to the types in the main collection.
            self.dex_objects.types.add(annotation.type)
            
            # Each annotation contains a type and a list of key, value pairs.
            for key, value in annotation.values:
            
                # Add the key to the strings in the main collection.
                self.dex_objects.strings.add(key)
                
                # The value in each pair can be a list of values or any of the
                # other encoded value types.
                if type(value) == list or type(value) == tuple:
                    values = value
                else:
                    values = [value]
                
                for var in values:
                    if isinstance(var, Type):
                        self.dex_objects.types.add(var.value)
                    elif type(var) == str or type(var) == unicode:
                        self.dex_objects.strings.add(var)
                    elif isinstance(var, Field):
                        self.dex_objects.fields.add(var)
    
    
    def add_ordered_classes(self, classes, class_dict, ordered_classes):
    
        # Sort the classes in order to generate a deterministic sequence of them
        # each time the same program is compiled.
        classes = map(lambda class_: (class_.class_type, class_), classes)
        classes.sort()
        
        for class_type, class_ in classes:
        
            # If the class itself is not in the dictionary - perhaps because it was
            # a superclass of another class - then do not try to add it to the
            # ordered list again.
            if class_type not in class_dict:
                continue
            
            # Append the class to the list of ordered classes and remove its name
            # from the dictionary of named classes to ensure that it is not
            # appended again.
            ordered_classes.append(class_)
            del class_dict[class_type]
            
            # If the class has a superclass that is in the dictionary of named
            # classes we need to describe then add that to the list of ordered
            # classes.
            if class_.superclass_type in class_dict:
                self.add_ordered_classes([class_dict[class_.superclass_type]],
                                    class_dict, ordered_classes)
            
            # If the class has interfaces that are in the dictionary of named
            # classes we need to describe, add them to the list of ordered classes.
            interfaces = []
            for name in class_.interfaces:
                if name in class_dict:
                    interfaces.append(class_dict[name])
            
            self.add_ordered_classes(interfaces, class_dict, ordered_classes)
    
    
    def assemble_method_code(self):
    
        # Keep information about the source code for methods if specified.
        sources = []
        
        for method in self.dex_objects.methods:
        
            if method.code is None:
                continue
            
            # Remove redundant Goto macros.
            i = 0
            while i < len(method.code.instructions):
            
                instruction = method.code.instructions[i]
                
                if isinstance(instruction, Target):
                
                    # Backtrack, looking for a Goto instruction.
                    j = i - 1
                    current = instruction
                    while j > 0:
                    
                        instruction = method.code.instructions[j]
                        
                        if isinstance(instruction, Branch):
                            if instruction.target == current:
                                # Remove the redundant branch and continue
                                # looking for preceding ones.
                                del method.code.instructions[j]
                                # Compensate for the removed macro.
                                i -= 1
                            else:
                                # A branch to another label.
                                break
                        
                        elif not isinstance(instruction, Target):
                            # A non-branch or target.
                            break
                        
                        j -= 1
                i += 1
            
            # Expand any macros but keep branches and targets for later
            # resolution. Some macros will expand to contain branch macros.
            # Calculate the addresses of the targets in the newly expanded
            # code, taking care to calculate the initial sizes of branch
            # placeholder instructions.
            instructions = []
            targets = {}
            addr = 0
            
            for instruction in method.code.instructions:
            
                if isinstance(instruction, Target):
                    targets[instruction] = addr
                    instructions.append(instruction)
                
                elif isinstance(instruction, Branch):
                    instructions.append(instruction)
                    addr += instruction.size()
                
                elif isinstance(instruction, Macro):
                    for inst in instruction.expand(self.dex_objects):
                        instructions.append(inst)
                        if isinstance(inst, Branch):
                            addr += inst.size()
                        else:
                            addr += inst.size
                else:
                    # Source objects and anything else
                    instructions.append(instruction)
                    addr += instruction.size
            
            # Provisionally expand branch instructions in order to determine
            # their sizes - some goto instructions are larger than the initial
            # size.
            rescan = True
            
            while rescan:
            
                new_targets = {}
                addr = 0
                rescan = False
                
                for instruction in instructions:
                
                    if isinstance(instruction, Target):
                        new_targets[instruction] = addr
                    
                    elif isinstance(instruction, Branch):
                        # Expand the macro to determine the size of its contents
                        # but do not replace it yet.
                        dest = targets[instruction.target]
                        original_size = instruction.size()
                        inst = instruction.resolve(dest - addr, self.dex_objects)
                        addr += inst.size
                        if original_size != inst.size:
                            rescan = True
                    else:
                        addr += instruction.size
                
                targets = new_targets
            
            addr = 0
            
            # Prepare a dictionary to hold the addresses of instructions needed
            # for exception handling.
            try_offsets = {}
            
            for try_info in method.code.tries_and_handlers:
            
                try_offsets[try_info.begin] = None
                try_offsets[try_info.end] = None
                for handler_target, handler_type in try_info.handlers:
                    try_offsets[handler_target] = None
            
            new_instructions = []
            
            for instruction in instructions:
            
                if isinstance(instruction, Branch):
                    # Replace the placeholder with a resolved instruction.
                    dest = targets[instruction.target]
                    instruction = instruction.resolve(dest - addr, self.dex_objects)
                    new_instructions.append(instruction)
                
                elif isinstance(instruction, Target):
                    if instruction in try_offsets:
                        try_offsets[instruction] = addr
                    
                    continue
                
                else:
                    new_instructions.append(instruction)
                
                addr += instruction.size
            
            if self.dex_objects.keep_source:
                sources.append((method, new_instructions))
            
            # Remove any Source placeholders.
            new_instructions = filter(lambda inst: not isinstance(inst, Source),
                                      new_instructions)
            
            # Assemble the resulting instructions.
            method.code.instructions = assemble(new_instructions)
            
            # Construct a structure containing information about the tries and
            # handlers used in the method.
            tries_and_handlers = []
            
            for try_info in method.code.tries_and_handlers:
            
                catch_type_handlers = []
                catch_all_offset = None
                
                for handler_target, handler_type in try_info.handlers:
                    if handler_type is None:
                        catch_all_offset = try_offsets[handler_target]
                    else:
                        catch_type_handlers.append((create_dex_type(handler_type),
                                                    try_offsets[handler_target]))
                
                # No catch-all handler at the moment.
                catch_handler = (catch_type_handlers, catch_all_offset)
                
                start = try_offsets[try_info.begin]
                count = try_offsets[try_info.end] - start
                tries_and_handlers.append((start, count, catch_handler))
            
            method.code.tries_and_handlers = tries_and_handlers
        
        return sources
    
    """~
    ### Creating a DEX file
    
    The `create_file` method is the interface to the compiler for front-end
    tools, such as the [buildhelper](../../Tools/docs/buildhelper.md) module.
    """
    
    def create_file(self, package_name, res_module, code_file, include_paths,
                    output_file, app_base_classes = None, suppress_warnings = None):
    
        self.dex_objects.include_paths = include_paths
        self.dex_objects.suppress_warnings = suppress_warnings or []
        
        # Create application classes and classes that describe the build.
        try:
            app_classes = self.define_application_classes(package_name,
                code_file, res_module, app_base_classes)
        except:
            if self.dex_objects.debug:
                raise
            else:
                type_, value, traceback = sys.exc_info()
                sys.stderr.write(str(value) + "\n")
                sys.exit(1)
        
        build_info_template = os.path.join(os.path.split(include_paths[-1])[0],
                                           "Meta", "buildinfo.py")
        self.define_build_classes(package_name, build_info_template,
                                  app_classes.values())
        
        # Complete the other entries in the collection of objects.
        self.complete_dex_objects(package_name)
        
        # Add code to the methods now that the final order of them has been
        # established.
        sources = self.assemble_method_code()
        
        if self.dex_objects.keep_source:
            writer.write_listing(self.dex_objects.listing_file_name, sources,
                                 self.dex_objects)
        d = Dex()
        d.create(self.dex_objects.strings, self.dex_objects.types,
                 self.dex_objects.prototypes, self.dex_objects.fields,
                 self.dex_objects.methods, self.dex_objects.ordered_classes)
        d.save(output_file)
        
        app_class_names = {}
        for base_name, class_ in app_classes.items():
            app_class_names[base_name] = class_.name()
        
        return app_class_names, self.dex_objects.permissions
