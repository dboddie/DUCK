#!/usr/bin/env python

import commands, os, shutil, sys

export_types = set([".py", ".png", ".svg", ".xml"])

def mkdir(directory):

    print "Making directory", directory
    os.system("mkdir -p -m 0755 " + commands.mkarg(directory))


def process_file(file_name, dest_dir, relpath):

    output_file = os.path.join(dest_dir, file_name)
    output_dir = os.path.split(output_file)[0]
    
    if not os.path.isdir(output_dir):
        mkdir(output_dir)
    
    shutil.copy2(file_name, output_file)
    os.system("chmod 0644 " + commands.mkarg(output_file))
    print "Copied", file_name, "to", output_file


def process(directory, dest_dir, relpath = []):

    for name in os.listdir(directory):
    
        obj = os.path.join(directory, name)
        
        if os.path.isdir(obj):
            process(obj, dest_dir, relpath + [name])
        
        elif obj.endswith(".html"):
            process_file(obj, dest_dir, relpath)
        
        elif os.path.splitext(obj)[1] in export_types:
        
            if relpath and relpath[0] in ("Demos", "Examples"):
                process_file(obj, dest_dir, relpath)


if __name__ == "__main__":

    if len(sys.argv) != 2:
        sys.stderr.write("Usage: %s <destination directory>\n" % sys.argv[0])
        sys.exit(1)
    
    dest_dir = sys.argv[1]
    
    process(os.curdir, dest_dir)
    sys.exit()
