
```python
import time, types

counts = {}
exceptions = {}
times = {}
tstack = []
time_within = 0

def profile(class_name, func):

    global counts, exceptions, times
    
    fname = class_name + "." + func.__name__
    
    if fname in counts:
        return func
    
    counts[fname] = 0
    times[fname] = 0
    exceptions[fname] = 0
    
    def wrapper(*args, **kwargs):
    
        global counts, exceptions, times, tstack, time_within
        
        tstack.append(time_within)
        
        # Record the amount of time spent by functions called from within the
        # wrapped function.
        time_within = 0
        
        t = time.time()
        try:
            v = func(*args, **kwargs)
            raised = False
        except:
            raised = True
        
        # The total time elapsed while the function was running.
        elapsed = time.time() - t
        # Add the time, minus the time spent in other functions, to the total
        # for this function.
        times[fname] += elapsed - time_within
        counts[fname] += 1
        
        # The time spent within the wrapped function is added to the total
        # for functions called by the enclosing function.
        time_within = tstack.pop() + elapsed
        
        if raised:
            exceptions[fname] += 1
            raise
        
        return v
    
    return wrapper


def profile_class(class_):

    for name, obj in class_.__dict__.items():
    
        if type(obj) == types.FunctionType:
            class_.__dict__[name] = profile(class_.__name__, obj)
    
    for base in class_.__bases__:
        profile_class(base)


def profile_module(module):

    for name, obj in module.__dict__.items():
    
        if type(obj) == types.FunctionType:
            module.__dict__[name] = profile(module.__name__, obj)
        
        elif type(obj) == types.ClassType:
            profile_class(obj)


def show_profiled():

    global counts, times
    
    max_length = max(map(len, times.keys()) + [len("Name")])
    max_count = max(map(len, map(str, counts.values())) + [len("Count")])
    max_exc = max(map(len, map(str, exceptions.values())) + [len("Exceptions")])
    max_time = max(map(lambda t: len("{:.6f}".format(t)), times.values()) + [len("Time")])
    
    pairs = map(lambda (name, elapsed): (elapsed, name), times.items())
    pairs.sort()
    
    heading_format = "{:<%i}   {:<%i}   {:<%i}   {:<%i}" % (max_length, max_count, max_exc, max_time)
    row_format = "{:<%i}   {:<%i}   {:<%i}   {:<.6f}" % (max_length, max_count, max_exc)
    
    print heading_format.format("Name", "Count", "Exceptions", "Time")
    print "=" * (max_length + max_count + max_exc + max_time + 9)
    
    total_time = 0
    total_calls = 0
    total_exceptions = 0
    
    for elapsed, name in pairs:
        calls = counts[name]
        exc = exceptions[name]
        if calls != 0:
            print row_format.format(name, calls, exc, elapsed)
            total_time += elapsed
            total_calls += calls
            total_exceptions += exc
    
    print "=" * (max_length + max_count + max_exc + max_time + 9)
    print "Total calls      = {}".format(total_calls)
    print "Total exceptions = {}".format(total_exceptions)
    print "Total time       = {:<.6f} seconds".format(total_time)
    print
```



## Files

* [\_\_init\_\_.py](../\_\_init\_\_.py)
* [buildhelper.py](../buildhelper.py)
* [common.py](../common.py)
* [dexinfo.py](../dexinfo.py)
* [dumpdex.py](../dumpdex.py)
* [gendocs.py](../gendocs.py)
* [generate\_internal\_docs.py](../generate\_internal\_docs.py)
* [makeandroidmanifest.py](../makeandroidmanifest.py)
* [makelayout.py](../makelayout.py)
* [makemanifest.py](../makemanifest.py)
* [makepackage.py](../makepackage.py)
* [makeresources.py](../makeresources.py)
* [profiling.py](../profiling.py)
* [publish\_docs.py](../publish\_docs.py)
* [readbinxml.py](../readbinxml.py)
* [takepic.py](../takepic.py)
* [tasks.py](../tasks.py)
* [update\_docs.py](../update\_docs.py)
