
```python
    output_dir = os.path.split(output_file)[0]
    
    res_dir = os.path.join(output_dir, "res")
    if not os.path.exists(res_dir):
        os.mkdir(res_dir)
    
    # Copy the res_files dictionary since we will modify it.
    new_res_files = {}
    for key, value in res_files.items():
        new_res_files[key] = value.copy()
    
    if u"drawable" in res_files:
    
        # Create a dictionary mapping keys containing tuples of resource names and
        # SVG file names to lists of PNG files. Each PNG file corresponds to a
        # drawable in the following list.
        
        conversions = {}
        drawables = []
        
        for dirname in resolutions:
        
            size, resolution = drawable_resolutions[dirname]
            drawables.append((dirname, size, resolution))
            
            drawable_dir = os.path.join(res_dir, dirname)
            
            if not os.path.exists(drawable_dir):
                os.mkdir(drawable_dir)
            
            drawable_items = res_files[u"drawable"].items()
            
            for res_name, original_name in drawable_items:
                src_path = os.path.join(resource_root, original_name)
                group = conversions.setdefault((res_name, src_path), [])
                group.append(os.path.join(drawable_dir, res_name + ".png"))
        
        # Generate the PNG files for each SVG file at the resolutions given in the
        # drawables list.
        create_icons(package_name, drawables, conversions, options)
        
        # Now that the PNG files have been created, convert the paths to package
        # relative paths for inclusion in the resources file.
        for (res_name, original_name), png_files in conversions.items():
        
            new_res_files[u"drawable"][res_name] = map(lambda png_file:
                os.path.relpath(png_file, output_dir), png_files)
    
    if u"raw" in res_files:
    
        raw_dir = os.path.join(res_dir, "raw")
        if not os.path.exists(raw_dir):
            os.mkdir(raw_dir)
        
        for key, value in res_files[u"raw"].items():
            src = os.path.join(resource_root, value)
            dest = os.path.join(raw_dir, os.path.split(value)[1])
            shutil.copy2(src, dest)
            new_res_files[u"raw"][key] = os.path.join(u"res", u"raw", os.path.split(dest)[1])
    
    if u"xml" in res_files:
    
        # Create a preliminary resources file so that XML files can refer to other
        # resources then process the XML files.
        resources = create_resources(app_name, package_name, new_res_files, drawables,
                                     sdk_version = sdk_version)
        
        xml_dir = os.path.join(res_dir, "xml")
        if not os.path.exists(xml_dir):
            os.mkdir(xml_dir)
        
        for key, value in res_files[u"xml"].items():
            src = os.path.join(resource_root, value)
            dest = os.path.join(xml_dir, os.path.split(value)[1])
            
            # Encode the XML file as binary XML, using the preliminary
            # resources file to resolve references to other resources.
            binxml_file = to_binxml(from_xml(src), resources)
            binxml_file.save(dest)
            
            new_res_files[u"xml"][key] = os.path.join(u"res", u"xml", os.path.split(dest)[1])
    
    resources = create_resources(app_name, package_name, new_res_files, drawables)
    resources.save(output_file)
    return resources
```



## Files

* [\_\_init\_\_.py](../\_\_init\_\_.py)
* [buildhelper.py](../buildhelper.py)
* [common.py](../common.py)
* [dexinfo.py](../dexinfo.py)
* [dumpdex.py](../dumpdex.py)
* [gendocs.py](../gendocs.py)
* [generate\_internal\_docs.py](../generate\_internal\_docs.py)
* [makeandroidmanifest.py](../makeandroidmanifest.py)
* [makelayout.py](../makelayout.py)
* [makemanifest.py](../makemanifest.py)
* [makepackage.py](../makepackage.py)
* [makeresources.py](../makeresources.py)
* [profiling.py](../profiling.py)
* [publish\_docs.py](../publish\_docs.py)
* [readbinxml.py](../readbinxml.py)
* [takepic.py](../takepic.py)
* [tasks.py](../tasks.py)
* [update\_docs.py](../update\_docs.py)
