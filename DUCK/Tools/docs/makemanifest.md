
```python
import binascii, Crypto.Hash.SHA, os, shutil, sys

__version__ = "1.0"
__creator__ = "Android"
info = (
"Writes manifest files for the contents of the specified package directory.\n"
"Once manifest files have been written, they can be signed and the package\n"
"directory can then be archived for deployment.\n"
"Note that the makepackage.py tool can be used to handle all of these steps\n"
"with a single command.\n"
    )

def make_digest(text):

    sha = Crypto.Hash.SHA.new()
    sha.update(text)
    return binascii.b2a_base64(sha.digest()).rstrip()

def make_digest_digest((name, text)):

    return ("Name: %s\r\n" % name + \
            "SHA1-Digest: %s\r\n\r\n" % make_digest(text))

def handle_dir((package_dir, entries), directory, files):

    for file_name in files:
    
        # Obtain the path of the file containing the package directory itself.
        obj_path = os.path.join(directory, file_name)
        
        if os.path.isfile(obj_path):
        
            rel_dir = os.path.relpath(directory, package_dir)
            if rel_dir == os.curdir:
                rel_dir = ""
            
            digest = make_digest(open(obj_path, "rb").read())
            name = os.path.join(rel_dir, file_name)
            entries[name] = ("Name: %s\r\n" % name + \
                             "SHA1-Digest: %s\r\n\r\n" % digest)

def make_manifest(package_dir):

    meta_inf_dir = os.path.join(package_dir, "META-INF")
    if os.path.exists(meta_inf_dir):
        shutil.rmtree(meta_inf_dir)
    
    os.mkdir(meta_inf_dir)
    
    manifest_entries = {}
    cert_entries = {}
    
    manifest_header = ("Manifest-Version: 1.0\r\n" + \
        "Created-By: %s (%s)\r\n\r\n" % (__version__, __creator__))
    os.path.walk(package_dir, handle_dir, (package_dir, manifest_entries))
    
    entries = manifest_entries.items()
    entries.sort()
    manifest = manifest_header + "".join(map(lambda (key, value): value, entries))
    
    cert_header = ("Signature-Version: 1.0\r\n" + \
        "Created-By: %s (%s)\r\n" % (__version__, __creator__) + \
        "SHA1-Digest-Manifest: %s\r\n\r\n" % make_digest(manifest))
    cert_entries = map(make_digest_digest, entries)
    cert = cert_header + "".join(cert_entries)
    
    manifest_f = open(os.path.join(meta_inf_dir, "MANIFEST.MF"), "wb")
    manifest_f.write(manifest)
    manifest_f.close()
    
    cert_f = open(os.path.join(meta_inf_dir, "CERT.SF"), "wb")
    cert_f.write(cert)
    cert_f.close()


if __name__ == "__main__":

    if len(sys.argv) != 2:
        sys.stderr.write("Usage: %s <package directory>\n" % sys.argv[0])
        sys.stderr.write(info)
        sys.exit(1)
    
    package_dir = sys.argv[1]
    
    try:
        make_manifest(package_dir)
    
    except IOError:
        sys.stderr.write("Failed to write manifest files.\n")
        sys.exit(1)
    
    sys.exit()
```



## Files

* [\_\_init\_\_.py](../\_\_init\_\_.py)
* [buildhelper.py](../buildhelper.py)
* [common.py](../common.py)
* [dexinfo.py](../dexinfo.py)
* [dumpdex.py](../dumpdex.py)
* [gendocs.py](../gendocs.py)
* [generate\_internal\_docs.py](../generate\_internal\_docs.py)
* [makeandroidmanifest.py](../makeandroidmanifest.py)
* [makelayout.py](../makelayout.py)
* [makemanifest.py](../makemanifest.py)
* [makepackage.py](../makepackage.py)
* [makeresources.py](../makeresources.py)
* [profiling.py](../profiling.py)
* [publish\_docs.py](../publish\_docs.py)
* [readbinxml.py](../readbinxml.py)
* [takepic.py](../takepic.py)
* [tasks.py](../tasks.py)
* [update\_docs.py](../update\_docs.py)
