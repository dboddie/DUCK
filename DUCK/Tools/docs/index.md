# Tools

A collection of tools are provided with the compiler. Some of these are
intended to be run as tools from the command line; others are Python modules
that are imported by build scripts and other tools.

## Build helper

The [buildhelper](buildhelper.md) module provides a simple interface that build
scripts use to compile and package an application's components. This is
accessed by calling the [buildhelper.main](buildhelper.md#entry-point) function
with values for the following parameters:

 * `script_file` is the location of the application's build script.
 * `app_name` is the user-visible name of the application. For example,
   `"Hello World"`.
 * `package_name` is the fully-qualified package name of the application.
   For example, `"com.example.hello"`.
 * `res_files` is a dictionary that defines the resources to be included in the
   application.
 * `layout` is a list of layout objects.
 * `code_file` is the location of the application's main source code file.
 * `include_paths` is a list of paths that the compiler searches when trying to
   resolve definitions.
 * `features` is a list of features the application can declare that it uses.
   This helps package stores filter or select applications based on their
   features.
 * `permissions` is a list of permissions that the application needs in order
   to run.
 * `args` is usually a copy of the list of command line arguments. The function
   processes these on behalf of the build script.

Callers can supply a number of optional arguments to the function:

 * `description` provides a way to override the standard activity description
   for custom application types and widgets.
 * `app_template` contains the type of the application. The default is
   `"activity"` but `"custom"` and `"widget"` are also accepted.
 * `include_sources` is a boolean value used to specify whether an archive of
   the source code will be included in the application package.
 * `docs_dir` is used to specify a directory relative to the directory
   containing the build script where documentation for the application will be
   generated.
 * `details` can be used to pass a dictionary for the build helper to fill in
   with information about the application.
 * `sdk_version` is the minimum version of the API that the application
   requires.
 * `suppress_warnings` can be used to pass a list of methods that will
   cause the compiler to produce warnings.
 * `options` can be used to pass additional options that are mainly used to
   specify how icons should be generated.
 * `doc_options` can be used to specify additional options for documentation
   generation and is currently unused.
 * `version` specifies the version of the packaging metadata used.

See the build scripts for the
[Hello](../../../Examples/Serpentine/Hello/docs/hello.md) and
[XML Layout](../../../Examples/Serpentine/XMLLayout/docs/xmllayout.md) examples for
samples of the values to pass for these parameters.
