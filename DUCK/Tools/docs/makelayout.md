
```python
    resources = set()
    attributes = []
    
    # Find arguments corresponding to resource attributes in the layout.
    find_xml_resources(elements, resources, attributes)
    
    # The index of each resource_ids needs to match the indexes of the
    # corresponding string in the string pool that we will create later.
    # They also need to be sorted in ascending numerical order.
    resources = list(resources)
    resources.sort()
    
    resource_ids = []
    strings = []
    
    for resource_id, string in resources:
        resource_ids.append(resource_id)
        strings.append(string)
    
    for attribute in attributes:
        if attribute not in strings:
            strings.append(attribute)
    
    # First, start the contents with some boilerplate elements and add the
    # strings they use to the string pool.
    contents = [XMLResourceMap(resource_ids),
                XMLStartNamespace(prefix="android",
                uri="http://schemas.android.com/apk/res/android")]
    
    strings += [u"android", u"http://schemas.android.com/apk/res/android"]
    
    # Construct the list of contents from the layout structure itself and
    # complete the string pool.
    ids = {}
    contents += create_contents(elements, strings, ids = ids)
    
    # Append trailing boilerplate.
    contents.append(XMLEndNamespace(prefix="android",
                    uri="http://schemas.android.com/apk/res/android"))
    
    string_pool = StringPool(strings)
    
    # Create a binary XML file to hold the layout but don't save it yet. It may
    # contain id attribute definitions that need to be turned into resource ID
    # values.
    xml = XML(string_pool, contents)
    layout_file = File(root=xml)
    return layout_file, ids
```



## Files

* [\_\_init\_\_.py](../\_\_init\_\_.py)
* [buildhelper.py](../buildhelper.py)
* [common.py](../common.py)
* [dexinfo.py](../dexinfo.py)
* [dumpdex.py](../dumpdex.py)
* [gendocs.py](../gendocs.py)
* [generate\_internal\_docs.py](../generate\_internal\_docs.py)
* [makeandroidmanifest.py](../makeandroidmanifest.py)
* [makelayout.py](../makelayout.py)
* [makemanifest.py](../makemanifest.py)
* [makepackage.py](../makepackage.py)
* [makeresources.py](../makeresources.py)
* [profiling.py](../profiling.py)
* [publish\_docs.py](../publish\_docs.py)
* [readbinxml.py](../readbinxml.py)
* [takepic.py](../takepic.py)
* [tasks.py](../tasks.py)
* [update\_docs.py](../update\_docs.py)
