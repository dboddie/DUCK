
```python
    try:
        makemanifest.make_manifest(package_dir)
    
    except IOError:
        sys.stderr.write("Failed to write manifest files.\n")
        return False
    
    # Sign the package only if key and certificate files have been specified.
    if key_file and cert_file:
    
        cert_sf = os.path.join(package_dir, "META-INF", "CERT.SF")
        rsa_file = os.path.join(package_dir, "META-INF", "CERT.RSA")
        
        if os.system("openssl smime -sign -noverify -noattr -binary " + \
                     "-inkey " + commands.mkarg(key_file) + \
                     " -signer " + commands.mkarg(cert_file) + \
                     " -md sha1 " + \
                     " -in " + commands.mkarg(cert_sf) + \
                     " -outform der -out " + commands.mkarg(rsa_file)) != 0:
            return False
    
    apk = zipfile.ZipFile(package_file, "w", zipfile.ZIP_DEFLATED)
    os.path.walk(package_dir, handle_dir, (package_dir, apk))
    apk.close()
    
    return True


if __name__ == "__main__":

    if len(sys.argv) != 5:
        sys.stderr.write("Usage: %s <package directory> <key file> <certificate file> <package file>\n" % sys.argv[0])
        sys.exit(1)
    
    package_dir = sys.argv[1]
    key_file = sys.argv[2]
    cert_file = sys.argv[3]
    package_file = sys.argv[4]
    
    if not create_file(package_dir, key_file, cert_file, package_file):
        sys.exit(1)
    
    sys.exit()
```



## Files

* [\_\_init\_\_.py](../\_\_init\_\_.py)
* [buildhelper.py](../buildhelper.py)
* [common.py](../common.py)
* [dexinfo.py](../dexinfo.py)
* [dumpdex.py](../dumpdex.py)
* [gendocs.py](../gendocs.py)
* [generate\_internal\_docs.py](../generate\_internal\_docs.py)
* [makeandroidmanifest.py](../makeandroidmanifest.py)
* [makelayout.py](../makelayout.py)
* [makemanifest.py](../makemanifest.py)
* [makepackage.py](../makepackage.py)
* [makeresources.py](../makeresources.py)
* [profiling.py](../profiling.py)
* [publish\_docs.py](../publish\_docs.py)
* [readbinxml.py](../readbinxml.py)
* [takepic.py](../takepic.py)
* [tasks.py](../tasks.py)
* [update\_docs.py](../update\_docs.py)
