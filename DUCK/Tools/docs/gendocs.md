
```python
from ast import ClassDef, Expr, For, FunctionDef, If, Module, Str, TryExcept, \
                While, get_docstring
import os, string, sys

from DUCK.Serpentine.visitor import Visitor

class Doc:

    def __init__(self, text):
        self.text = text
    
    def add_text(self, text):
        self.text += "\n\n" + text
    
    def __str__(self):
        return self.text


class Writer:

    export_types = set([".py", ".png", ".svg", ".xml"])
    
    def __init__(self, lines, internal=True):
    
        self.lines = lines
        self.internal = internal
        
        self.text = ""
        self.offsets = {}
        self.indices = {}
        offset = 0
        i = 0
        
        for i, line in enumerate(lines):
            self.offsets[i] = offset
            self.indices[offset] = i
            self.text += line
            offset += len(line)
        else:
            # Add entries for the end of the file.
            i += 1
            self.offsets[i] = offset
            self.indices[offset] = i
        
        self.line = 0
        self.indent = 0
        self.output = []
        self.title = ""
    
    def add_node(self, node):
    
        if isinstance(node, ClassDef):
            self.write_node(node.body)
        
        elif isinstance(node, FunctionDef):
            self.write_node(node.body)
        
        elif isinstance(node, Module):
            self.write_node(node.body)
        
        elif isinstance(node, If):
        
            self.write_node(node.body)
            
            if node.orelse:
                self.write_node(node.orelse)
        
        elif isinstance(node, For) or isinstance(node, While):
        
            self.write_node(node.body)
            
            if node.orelse:
                self.write_node(node.orelse)
        
        elif isinstance(node, TryExcept):
        
            self.write_node(node.body)
            
            for handler in node.handlers:
                self.write_node(handler.body)
            
            if node.orelse:
                self.write_node(node.orelse)
    
    def write_node(self, body):
    
        for child in body:
        
            # For external documentation (examples) docstrings are written as
            # paragraphs between code blocks. For internal documentation,
            # docstrings are included as normal strings unless they begin with
            # an exclamation mark.
            
            if isinstance(child, Expr) and isinstance(child.value, Str):
            
                text = child.value.s
                
                # Look up the offset of the line after the one containing the
                # node, remembering that the line numbers in nodes start at 1,
                # so we don't subtract 1 in this case.
                end = self.offsets[child.lineno]
                
                # The beginning of the search text is the last line processed.
                begin = self.offsets[self.line]
                
                at = self.text.rfind(text, begin, end)
                
                # Only include regular docstrings for non-internal documentation.
                if not self.internal or child.value.s.startswith("~"):
                
                    # Find the start of the line on which the text appears and
                    # measure the indentation level.
                    start = self.text.rfind("\n", begin, at) + 1
                    indent = 0
                    i = start
                    while self.text[i] == " " and i < at:
                        i += 1
                    indent = i - start
                    
                    # Look up the index of the line.
                    index = self.indices[start]
                    
                    while self.line < index:
                        line = self.lines[self.line]
                        self.output.append(line)
                        self.line += 1

                    self.add_docstring(text, indent)
                
                # Skip to the end of the string.
                next = self.text.find("\n", at + len(text))
                self.line = self.indices[next + 1]
            else:
                self.add_node(child)
    
    def add_docstring(self, text, indent):
    
        lines = []
        i = indent * " "
        started = False
        previous = ""
        # Remove leading notation for internal documentation.
        text = text.lstrip("~")
        
        for line in text.split("\n"):
        
            if not started and not line.lstrip():
                continue
            
            started = True
            
            if not self.title:
                # Check for title markup (# <title>).
                if line.startswith("# "):
                    self.title = line[2:].strip()
                else:
                    # Check for title markup (sequence of = on line below).
                    l = line.strip()
                    if l == (len(l) * "=") and len(l) == len(previous):
                        self.title = previous
                
                previous = line.strip()
            
            # Strip the indentation from the start of the line.
            if line.startswith(i):
                line = line[indent:]
            
            lines.append(line)
        
        self.output.append(Doc("\n".join(lines)))
    
    def write(self, f):
    
        # Append any remaining text to the output list.
        while self.line < len(self.lines):
            self.output.append(self.lines[self.line])
            self.line += 1
        
        l = []
        in_code = False
        
        for i in self.output:
        
            if isinstance(i, Doc):
            
                while l:
                    if isinstance(l[-1], Doc):
                        # Merge the previous Doc element with this one.
                        l[-1].add_text(i.text)
                        break
                    elif l[-1].strip():
                        # Append the Doc element to the list.
                        l.append(i)
                        break
                    else:
                        # Discard the previous code string.
                        l.pop()
                else:
                    # The first element in the list.
                    l.append(i)
                
                in_code = False
            
            else:
                if not in_code:
                    in_code = True
                    l.append(i)
                elif l:
                    # Merge the code element with the previous one.
                    l[-1] += i
                else:
                    # The first element in the list.
                    l.append(i)
        
        in_code = False
        j = 0
        
        for i in l:
        
            if isinstance(i, Doc):
                if in_code:
                    f.write("\n")
                    in_code = False
                
                text = str(i)
            else:
                if not in_code:
                    if j > 0:
                        f.write("\n")
                    
                    f.write("\n")
                    in_code = True
                
                lines = i.split("\n")
                
                while lines:
                    if lines[0].strip() != "":
                        break
                    else:
                        lines.pop(0)
                
                while lines and lines[-1].strip() == "":
                    lines.pop()
                
                text = "\n".join(lines)
                text = "```python\n" + text + "\n```\n"
            
            f.write(text)
            j += 1
        
        if in_code:
            f.write("\n")
    
    def write_file_references(self, f, source_dir, output_dir):
    
        text = self.get_file_references(source_dir, "..", output_dir)
        text = text.replace("_", "\\_")
        
        if text:
            text = "## Files\n\n" + text
            text = "\n\n" + text
            
            f.write(text)
    
    def get_file_references(self, source_dir, path, output_dir):
    
        files = os.listdir(source_dir)
        files.sort()
        
        text = ""
        
        for name in files:
        
            obj = os.path.join(source_dir, name)
            
            if os.path.isdir(obj) and obj != output_dir and not self.internal:
                text += self.get_file_references(obj, path + "/" + name, output_dir)
            
            elif os.path.isfile(obj) and os.path.splitext(obj)[1] in self.export_types:
                visible_path = path[3:]
                if visible_path: visible_path += "/"
                    
                text += "* [%s%s](%s/%s)\n" % (visible_path, name, path, name)
        
        return text


def generate_docs(file_name, include_paths, generated, res_module, docs_dir,
                  doc_options, internal=False):

    source_file = os.path.abspath(file_name)
    
    if source_file in generated or os.path.isdir(source_file):
        return
    
    source_dir, source_file_name = os.path.split(source_file)
    
    if docs_dir == os.path.abspath(docs_dir):
        # An absolute directory was given for the documentation.
        output_dir = docs_dir
    else:
        # A relative directory was given, so place the documentation inside the
        # source directory.
        output_dir = os.path.join(source_dir, docs_dir)
    
    if not os.path.exists(output_dir):
        os.mkdir(output_dir)
    
    print "Creating documentation for", file_name, "in", output_dir
        
    # Parse the source file.
    v = Visitor(include_paths + [source_dir])
    if res_module:
        imports = {"app_resources": res_module}
    else:
        imports = None
    
    module = v.load(source_file, imports)
    
    # Determine the output file name.
    source_name, suffix = os.path.splitext(source_file_name)
    
    # Write the documentation as Markdown.
    output_md = os.path.join(output_dir, source_name + ".md")
    
    lines = open(source_file).readlines()
    w = Writer(lines, internal=internal)
    w.add_node(v.root)
    f = open(output_md, "w")
    title = w.write(f)
    w.write_file_references(f, os.path.abspath(source_dir),
                               os.path.abspath(output_dir))
    f.close()
    
    # Mark this source file as generated to avoid reprocessing it.
    generated.add(source_file)
    
    # Generate modules imported by this one.
    for name, imported_module in module.imports.items():
    
        module_file_name = os.path.abspath(imported_module.file_name())
        
        if not internal and module_file_name.startswith(source_dir):
            generate_docs(module_file_name, include_paths, generated,
                          res_module, docs_dir, doc_options, internal=internal)
        
        elif os.path.split(module_file_name)[0] == source_dir:
            # Only document internal files in the source directory.
            generate_docs(module_file_name, include_paths, generated,
                          res_module, docs_dir, doc_options, internal=internal)


# The main code allows the module to be run as a script from the command line
# but doesn't generate code for resources that some applications need.

if __name__ == "__main__":

    from common import find_option
    
    args = sys.argv[:]
    include_paths = []
    
    while True:
        inc, inc_path = find_option(args, "-I", 1)
        if inc:
            include_paths.append(os.path.abspath(inc_path))
        else:
            break
    
    out_dir, out_dir_path = find_option(args, "-o", 1)
    
    if len(args) < 2 or not include_paths:
        sys.stderr.write("Usage: %s -I <path> [(-I <path>) ...] -o <output directory> "
                         "<source file> ...\n" % sys.argv[0])
        sys.exit(1)
    
    source_files = args[1:]
    generated = set()
    
    for source_file in source_files:
    
        source_file = os.path.abspath(source_file)
        generate_docs(source_file, include_paths, generated, None, out_dir_path, {})
    
    sys.exit()
```



## Files

* [\_\_init\_\_.py](../\_\_init\_\_.py)
* [buildhelper.py](../buildhelper.py)
* [common.py](../common.py)
* [dexinfo.py](../dexinfo.py)
* [dumpdex.py](../dumpdex.py)
* [gendocs.py](../gendocs.py)
* [generate\_internal\_docs.py](../generate\_internal\_docs.py)
* [makeandroidmanifest.py](../makeandroidmanifest.py)
* [makelayout.py](../makelayout.py)
* [makemanifest.py](../makemanifest.py)
* [makepackage.py](../makepackage.py)
* [makeresources.py](../makeresources.py)
* [profiling.py](../profiling.py)
* [publish\_docs.py](../publish\_docs.py)
* [readbinxml.py](../readbinxml.py)
* [takepic.py](../takepic.py)
* [tasks.py](../tasks.py)
* [update\_docs.py](../update\_docs.py)
