
```python
import os, shutil, stat

class ArgumentError(Exception):
    pass

def find_option(args, label, number = 0):

    try:
        i = args.index(label)
    except ValueError:
        if number == 0:
            return False
        else:
            return False, None
    
    values = args[i + 1:i + number + 1]
    args[:] = args[:i] + args[i + number + 1:]
    
    if number == 0:
        return True
    
    if len(values) < number:
        raise ArgumentError, "Not enough values for argument '%s': %s" % (label, repr(values))
    
    if number == 1:
        values = values[0]
    
    return True, values


def mkdirs(file_name):

    abspath = os.path.abspath(file_name)
    dirs, name = os.path.split(abspath)
    if abspath.startswith(os.sep):
        dirname = os.sep
    else:
        dirname = ""
    
    for piece in dirs.split(os.sep):
        dirname = os.path.join(dirname, piece)
        if not os.path.exists(dirname):
            os.mkdir(dirname)


def full_path(this_file, target_file):

    return os.path.join(os.path.split(this_file)[0], target_file)


class IconCache:

    def __init__(self, enabled):
    
        if not enabled:
            self.path = None
            return
        
        self.path = os.getenv("DUCK_ICON_DIR")
        
        if not self.path:
            self.path = self.default_dir()
        
        if self.path and not os.path.exists(self.path):
            os.mkdir(self.path)
    
    def default_dir(self):
    
        home = os.getenv("HOME")
        if home == None:
            return None
        
        cache_dir = os.path.join(home, ".cache")
        if cache_dir == None:
            return None
        
        duck_icon_dir = os.path.join(cache_dir, "DUCK")
        
        return duck_icon_dir
    
    def get_icon(self, svg_file, icon_str, png_file):
    
        if not self.path:
            return False
        
        icon_file = os.path.join(self.path, icon_str)
        
        if not os.path.exists(icon_file):
            return False
        
        if os.stat(svg_file)[stat.ST_MTIME] > os.stat(icon_file)[stat.ST_MTIME]:
            return False
        
        try:
            shutil.copy2(icon_file, png_file)
            return True
        except IOError:
            return False
    
    def set_icon(self, icon_str, png_file):
    
        if not self.path:
            return
        
        icon_file = os.path.join(self.path, icon_str)
        
        try:
            shutil.copy2(png_file, icon_file)
        except IOError:
            pass
```



## Files

* [\_\_init\_\_.py](../\_\_init\_\_.py)
* [buildhelper.py](../buildhelper.py)
* [common.py](../common.py)
* [dexinfo.py](../dexinfo.py)
* [dumpdex.py](../dumpdex.py)
* [gendocs.py](../gendocs.py)
* [generate\_internal\_docs.py](../generate\_internal\_docs.py)
* [makeandroidmanifest.py](../makeandroidmanifest.py)
* [makelayout.py](../makelayout.py)
* [makemanifest.py](../makemanifest.py)
* [makepackage.py](../makepackage.py)
* [makeresources.py](../makeresources.py)
* [profiling.py](../profiling.py)
* [publish\_docs.py](../publish\_docs.py)
* [readbinxml.py](../readbinxml.py)
* [takepic.py](../takepic.py)
* [tasks.py](../tasks.py)
* [update\_docs.py](../update\_docs.py)
