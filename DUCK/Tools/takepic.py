#!/usr/bin/env python

import commands, os, subprocess, sys, time

def system(command):

    s = subprocess.Popen(command.split(), stdout=subprocess.PIPE)
    result = s.wait()
    if result != 0:
        sys.exit(result)
    
    lines = s.stdout.readlines()
    if filter(lambda line: line.startswith("Error:"), lines):
        print "".join(lines)
        sys.exit(1)

def usage():

    sys.stderr.write("Usage: %s <package name> <activity class name> "
                     "<image file name> A|S <delay (s)> [scale]\n" % sys.argv[0])
    sys.exit(1)


if __name__ == "__main__":

    if not 6 <= len(sys.argv) <= 7:
        usage()
    
    package_name, activity_class_name, image_file, type_, delay = sys.argv[1:6]
    if type_ not in "AS":
        usage()
    
    if len(sys.argv) == 7:
        scale = float(sys.argv[6])
    else:
        scale = 1
    
    image_file = os.path.abspath(image_file)
    image_file_name = os.path.split(image_file)[1]
    delay = int(delay)
    
    system("adb shell am start -n %s/.%s" % (package_name, activity_class_name))
    time.sleep(delay)
    if type_ == "A":
        system("adb shell screenshot /sdcard/Download/%s" % image_file_name)
    else:
        system("adb shell screencap /sdcard/Download/%s" % image_file_name)
    system("adb shell am force-stop %s" % package_name)
    system("adb pull /sdcard/Download/%s %s" % (image_file_name, image_file))
    system("adb shell rm /sdcard/Download/%s" % image_file_name)
    
    if scale != 1:
        try:
            from PIL import Image
            im = Image.open(image_file)
            im = im.resize((int(im.size[0] * scale), int(im.size[1] * scale)),
                           Image.ANTIALIAS)
            im.save(image_file)
        except ImportError:
            os.system("convert -antialias -scale %i%% " % (scale * 100) + " " + \
                commands.mkarg(image_file) + " " + commands.mkarg(image_file))
    
    sys.exit()
