#!/usr/bin/env python

"""
Copyright (C) 2016 David Boddie <david@boddie.org.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import codecs, sys
from Dalvik import binxml

if __name__ == "__main__":

    args = sys.argv[:]
    if "-o" in args:
        at = args.index("-o")
        output_file = args[at + 1]
        args = args[:at] + args[at + 2:]
    else:
        output_file = None
    
    if not 2 <= len(args) <= 3:
        sys.stderr.write("Usage: %s [-o <output file>] <binary XML file> [resources file]\n" % sys.argv[0])
        sys.exit(1)
    
    binxml_file = sys.argv[1]
    
    doc = binxml.File(binxml_file)
    
    if output_file:
        output_file = codecs.open(output_file, "wb", "utf-8")
    
    if isinstance(doc.root, binxml.Table):
        doc.root.toxml(f = output_file)
    else:
        if len(sys.argv) == 3:
            res_file = sys.argv[2]
            res = binxml.File(res_file)
            table = res.root
        else:
            table = binxml.Table(binxml.StringPool(), {})
        
        doc.root.toxml(table, f = output_file)
    
    sys.exit()
