#!/usr/bin/env python

"""
Copyright (C) 2016 David Boddie <david@boddie.org.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import commands, os, sys, zipfile
import makemanifest

def handle_dir((package_dir, apk), directory, files):

    for file_name in files:
    
        obj_path = os.path.join(directory, file_name)
        
        if os.path.isfile(obj_path):
        
            rel_dir = os.path.relpath(directory, package_dir)
            if rel_dir == os.curdir:
                rel_dir = ""
            
            apk.write(obj_path, os.path.join(rel_dir, file_name))


def create_file(package_dir, key_file, cert_file, package_file):

    """Makes a manifest for the files in package_dir, signs it using key_file
    and cert_file, then archives the package directory to package_file.
    Returns True if successful, False if unsuccessful."""
    
    try:
        makemanifest.make_manifest(package_dir)
    
    except IOError:
        sys.stderr.write("Failed to write manifest files.\n")
        return False
    
    # Sign the package only if key and certificate files have been specified.
    if key_file and cert_file:
    
        cert_sf = os.path.join(package_dir, "META-INF", "CERT.SF")
        rsa_file = os.path.join(package_dir, "META-INF", "CERT.RSA")
        
        if os.system("openssl smime -sign -noverify -noattr -binary " + \
                     "-inkey " + commands.mkarg(key_file) + \
                     " -signer " + commands.mkarg(cert_file) + \
                     " -md sha1 " + \
                     " -in " + commands.mkarg(cert_sf) + \
                     " -outform der -out " + commands.mkarg(rsa_file)) != 0:
            return False
    
    apk = zipfile.ZipFile(package_file, "w", zipfile.ZIP_DEFLATED)
    os.path.walk(package_dir, handle_dir, (package_dir, apk))
    apk.close()
    
    return True


if __name__ == "__main__":

    if len(sys.argv) != 5:
        sys.stderr.write("Usage: %s <package directory> <key file> <certificate file> <package file>\n" % sys.argv[0])
        sys.exit(1)
    
    package_dir = sys.argv[1]
    key_file = sys.argv[2]
    cert_file = sys.argv[3]
    package_file = sys.argv[4]
    
    if not create_file(package_dir, key_file, cert_file, package_file):
        sys.exit(1)
    
    sys.exit()
