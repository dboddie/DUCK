"""
Copyright (C) 2016 David Boddie <david@boddie.org.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

"""~
# The buildhelper module

Applications written in Serpentine are typically built using build scripts.
This module provides support for those build scripts, performing many of the
tasks needed to prepare input for the compiler while hiding as much of the
internal machinery as possible.
"""

import commands, os, shutil, sys, tempfile

from DUCK import Serpentine
import common, gendocs, makeandroidmanifest, makelayout, makepackage, \
       makeresources, profiling

"""~
## Building source archives

Application developers are encouraged to build and distribute application
packages that include their own source code. The `make_source_archive` function
reads the root directory of a repository and builds a ZIP archive based on the
application's name. This will be included in the package as a raw resource.

*The aim was to include an archive with exactly the same layout and contents of
a snapshot from an application repository. As a result, the contents may
duplicate a lot of the actual application's resources.*
"""

def make_source_archive(root_dir, app_name):

    # Create a temporary directory where the archive will be created.
    temp_dir = tempfile.mkdtemp()
    src_name = app_name.replace(" ", "_")
    
    if os.path.isdir(os.path.join(root_dir, ".hg")):
    
        source_archive = os.path.join(temp_dir, src_name + ".zip")
        if os.system("hg archive -t uzip -p " + commands.mkarg(src_name) + \
                     " " + source_archive) != 0:
            source_archive = None
    
    elif os.path.isdir(os.path.join(root_dir, ".git")):
    
        source_archive = os.path.join(temp_dir, src_name + ".zip")
        if os.system("git archive --format=zip --prefix=" + \
            commands.mkarg(src_name).lstrip() + "/ -o " + source_archive + " HEAD") != 0:
            source_archive = None
    else:
        source_archive = os.path.join(temp_dir, src_name + ".zip")
        temp_source_dir = os.path.join(temp_dir, src_name)
        shutil.copytree(os.path.abspath(root_dir), temp_source_dir)
        
        this_dir = os.path.abspath(os.curdir)
        os.chdir(temp_dir)
        
        if os.system("zip -9r " + commands.mkarg(source_archive) + " " + commands.mkarg(src_name)) != 0:
            source_archive = None
        
        os.chdir(this_dir)
    
    return temp_dir, source_archive

"""~
## Defining resources

The `define_resources` function processes the source code that describes the
application resources for the application, using the supplied include paths to
locate additional definitions for resources such as standard Java classes.

The `res_sources` argument is a dictionary that relates the package number to
the source code defining all the constants that an application can use to
obtain resources at run-time.

*In theory, this function can be called with multiple resources. This is why
it parses the source code that defines the resources numbers and checks the
package name against the application's package name.*
"""

def define_resources(package_name, res_sources, include_paths):

    for source in res_sources.values():
    
        vtr = Serpentine.visitor.Visitor(include_paths, package_name)
        res_module = vtr.parse(source, "app_resources.py (generated)")
        if res_module.attributes["__package__"].s == package_name:
            return res_module

"""~
## Entry point

The `main` function is called by build scripts with a series of arguments
that define the component parts of an application.

See the [description of this API](index.md#build-helper) for details of the
types of values to pass to this function.
"""

def main(script_file, app_name, package_name, res_files, layout, code_file,
         include_paths, features, permissions, args, description = {},
         app_template = "activity", include_sources = False, docs_dir = None,
         details = None, sdk_version = None, suppress_warnings = None,
         options = {}, doc_options = {}, version = "1.0"):

    verbose = common.find_option(args, "-v") or \
              common.find_option(args, "--verbose")
    profile = common.find_option(args, "-p") or \
              common.find_option(args, "--profile")
    
    if common.find_option(args, "-i") or \
       common.find_option(args, "--icon-cache") or \
       common.find_option(args, "--image-cache"):
        options["icon cache"] = True
    
    # Discard the program from the argument list.
    args.pop(0)
    
    if len(args) == 1:
        key_file = cert_file = None
        temp_dir = output_dir = tempfile.mkdtemp()
        package_file = args[0]
    
    elif len(args) == 2:
        key_file = cert_file = None
        output_dir, package_file = args
        temp_dir = None
        if not os.path.exists(output_dir):
            os.mkdir(output_dir)
    
    elif len(args) == 3:
        temp_dir = output_dir = tempfile.mkdtemp()
        key_file, cert_file, package_file = args
    
    elif len(args) == 4:
        output_dir, key_file, cert_file, package_file = args
        temp_dir = None
        if not os.path.exists(output_dir):
            os.mkdir(output_dir)
    
    else:
        sys.stderr.write("Usage: %s [<output directory>] <key file> "
            "<certificate file> <package file>\n" % sys.argv[0])
        return 1
    
    # For convenience, when processing lots of packages where we don't want to
    # specify the SDK version in the build scripts, if the SDK version is not
    # specified then we check to see if it has been set in an environment
    # variable.
    if sdk_version == None:
        sdk_version = int(os.getenv("TARGET_SDK_VERSION", "4"))
    
    # The directory containing the build script.
    resource_root = os.path.split(script_file)[0]
    
    # Convert relative paths from this script to absolute paths.
    code_file = common.full_path(script_file, code_file)
    
    # Convert the include path specified by the build script and the directory
    # containing the build script to absolute paths. Append the standard
    # include path from the compiler package to these.
    include_paths += [resource_root,
                      common.full_path(Serpentine.__file__, "Include")]
    
    # Create the layout for the application and write a binary XML file
    # containing it.
    if layout:
        layout_file_name = os.path.join(output_dir, "res/layout/main.xml")
        common.mkdirs(layout_file_name)
        layout_file, ids = makelayout.create_file(layout, layout_file_name)
        res_files["layout"] = {"main": "res/layout/main.xml"}
        res_files["id"] = ids
    
    if include_sources:
    
        # Try to create an archive of the source tree.
        if verbose:
            print "Creating a source archive."
        
        sources_temp_dir, source_archive = make_source_archive(resource_root, app_name)
        if source_archive:
            res_files.setdefault("raw", {})["source_archive"] = source_archive
    
    # Write the resources file and obtain information about what it contains.
    resources_arsc_file = os.path.join(output_dir, "resources.arsc")
    if verbose:
        print "Writing resources to", resources_arsc_file
    
    res_info = makeresources.create_file(resource_root, app_name, package_name,
        res_files, resources_arsc_file, sdk_version = sdk_version,
        options = options)
    
    # Any references used in the layout should have been updated when the
    # resources were written, so now we can save the layout file.
    if layout:
        layout_file.save(layout_file_name)
    
    # Read the resources file.
    if verbose:
        print "Making resource classes"
    
    res_sources = makeresources.create_resource_classes(res_info)
    res_module = define_resources(package_name, res_sources, include_paths)
    
    # Write the classes file using the resource information.
    if app_template == "widget":
        app_base_classes = {"android/appwidget/AppWidgetProvider": "",
                            "android/app/Service": None,
                            "android/content/ContentProvider": None}
    else:
        app_base_classes = {"android/app/Activity": ""}
    
    classes_dex_file = os.path.join(output_dir, "classes.dex")
    if verbose:
        print "Writing classes file", classes_dex_file
    
    # If compiler profiling is enabled then wrap the methods of the compiler
    # with profiling code.
    if profile:
        import DUCK.Serpentine
        import DUCK.Serpentine.visitor
        profiling.profile_module(DUCK.Serpentine)
        profiling.profile_module(DUCK.Serpentine.visitor)
        profiling.profile_module(DUCK.Serpentine.visitor.ast)
    
    compiler = Serpentine.Compiler()
    
    app_class_names, permissions_used = compiler.create_file(package_name,
        res_module, code_file, include_paths, classes_dex_file,
        app_base_classes, suppress_warnings)
    
    # If profiling is enabled then show the results.
    if profile:
        profiling.show_profiled()
    
    # Warn about permissions that were used but not declared.
    for permission, methods in permissions_used.items():
        if permission not in permissions:
            sys.stderr.write("Permission '%s' may be required for methods:\n" % permission)
            for method in methods:
                sys.stderr.write("    %s\n" % method.full_name())
    
    # Create a general manifest description containing the package and version
    # names, and the version code.
    manifest_desc = {"package": package_name, "android:versionName": version}
    
    # Write the manifest file using the resource information.
    if app_template == "widget":
        manifest = makeandroidmanifest.create_widget_manifest(package_name,
            app_class_names, manifest_desc, receiver_desc = description)
    
    elif app_template == "custom":
        # For custom application templates the description given is the
        # complete manifest definition, not just attributes of the activity
        # element.
        manifest = description
    else:
        manifest = makeandroidmanifest.create_activity_manifest(package_name,
            app_class_names.values()[0], manifest_desc, activity_desc = description)
    
    manifest_file = os.path.join(output_dir, "AndroidManifest.xml")
    if verbose:
        print "Writing manifest to", manifest_file
    
    makeandroidmanifest.create_file(manifest, res_info, features, permissions,
                                    manifest_file, sdk_version)
    
    success = makepackage.create_file(output_dir, key_file, cert_file, package_file)
    
    # Generate documentation if required.
    if docs_dir:
        generated = set()
        gendocs.generate_docs(code_file, include_paths, generated, res_module,
                              docs_dir, doc_options)
    
    # Delete the temporary directory that is created when including sources.
    if include_sources:
        shutil.rmtree(sources_temp_dir)
    
    if details != None:
        details["class name"] = app_class_name[0]
    
    if not success:
        return 1
    
    if temp_dir:
        shutil.rmtree(temp_dir)
    
    return 0
