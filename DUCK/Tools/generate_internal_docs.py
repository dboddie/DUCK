#!/usr/bin/env python

"""
Copyright (C) 2021 David Boddie <david@boddie.org.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import os, sys

from DUCK.Tools.gendocs import generate_docs

# The main code allows the module to be run as a script from the command line
# but doesn't generate code for resources that some applications need.

if __name__ == "__main__":

    from common import find_option
    
    args = sys.argv[:]
    include_paths = map(os.path.abspath, [
        os.path.join("DUCK", "Serpentine", "Include"),
        os.curdir,
        os.path.join("DUCK", "Serpentine", "Include", "python-stdlib"),
        ])
    
    out_dir_path = "docs"
    
    source_files = [
        os.path.join("DUCK", "Serpentine", "__init__.py"),
        os.path.join("DUCK", "Tools", "buildhelper.py")
        ]
    generated = set()
    
    for source_file in source_files:
    
        source_file = os.path.abspath(source_file)
        generate_docs(source_file, include_paths, generated, None,
                      out_dir_path, {}, internal=True)
    
    sys.exit()
