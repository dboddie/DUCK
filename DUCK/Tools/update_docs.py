#!/usr/bin/env python

import os, re, sys
from pygments import highlight
from pygments.lexers import BashLexer, PythonLexer, TextLexer
from pygments.formatters import HtmlFormatter
from pygments.style import Style
from pygments.token import Comment, Error, Escape, Generic, Keyword, Literal, \
                           Name, Number, Operator, Other, Punctuation, \
                           String, Text
from markdown import markdown

extra_styles = \
'''.highlight pre {
  background: #f0f0f0;
  padding: 0.5em;
  border-left: 1px solid #a0a0a0;
  border-right: 1px solid #a0a0a0;
  border-top: 1px dashed #c0c0c0;
  border-bottom: 1px dashed #c0c0c0
}

.navbar {
  background: #e0e8f0;
  padding-top: 0.25em;
  padding-bottom: 0.25em
}

.navbar span {
  float: left;
  text-align: center;
  width: 33%
}

h1 {
  text-align: center
}'''

navigation_bar = \
'''<div class="navbar">&nbsp;
<span><a href="%(home)s">DUCK</a></span>
<span><a href="%(examples)s">Examples</a> and
      <a href="%(demos)s">Demos</a></span>
<span><a href="%(docs)s">Documentation</a></span>
</div>'''

class DocStyle(Style):

    default_style = ""
    styles = {
        Comment: "#005000",
        Error: "#ff0000",
        Escape: "#000000",
        Generic: "#000000",
        Keyword: "bold #803030",
        Literal: "#000000",
        Name: "#101010",
        Number: "#603000",
        Operator: "bold #404000",
        Other: "#000000",
        Punctuation: "#303030",
        String: "#0040b0",
        Text: "#0040b0"
        }

py_to_html = re.compile("(\\w+)\\.py\\)")

def py_to_html_sub(match):
    return "docs/" + match.groups()[0] + ".html)"

headings = re.compile("<h([1-6])>([^>]+)</h")

def heading_links(match):
    title = match.groups()[1]
    link = title.replace(" ", "-").lower()
    return '<h%s id="%s">%s</h' % (match.groups()[0], link, title)

def markup(text, in_code, code_type, formatter):

    if in_code and text:
        if code_type == '"':
            return [highlight(text, TextLexer(), formatter)]
        elif code_type == "\\":
            return [highlight(text, BashLexer(), formatter)]
        else:
            return [highlight(text, PythonLexer(), formatter)]
    elif not in_code and text:
        # Quick hack to rewrite links to Markdown and Python files.
        text = text.replace(".md)", ".html)")
        text = text.replace(".md#", ".html#")
        text = re.sub(py_to_html, py_to_html_sub, text)
        text = markdown(text)
        return [re.sub(headings, heading_links, text)]
    else:
        return []

def process_file(file_name, relpath):

    formatter = HtmlFormatter(style=DocStyle)
    
    lines = open(file_name).readlines()
    title = ""
    previous = ""
    new = []
    current = ""
    in_code = False
    code_type = ""
    
    for line in lines:
    
        if not title:
            if line.startswith("# "):
                title = line[2:].strip()
            else:
                l = line.strip()
                if l == (len(l) * "=") and len(l) == len(previous):
                    title = previous
            
            previous = line.strip()
        
        if line.startswith("```"):
        
            new += markup(current, in_code, code_type, formatter)
            
            in_code = not in_code
            if in_code:
                code_type = line[3:].strip()
            
            current = ""
        
        else:
            current += line
    
    # Process any unprocessed text.
    new += markup(current, in_code, code_type, formatter)
    
    output_file = file_name[:-3] + ".html"
    f = open(output_file, "w")
    f.write('<html>\n<head>\n')
    f.write('<title>%s</title>\n' % title)
    f.write('<style type="text/css">\n')
    f.write(formatter.get_style_defs() + '\n')
    f.write(extra_styles)
    f.write('\n</style>\n')
    f.write('</head>\n')
    
    f.write('<body>\n')
    f.write(navigation_bar % {
        "home": (len(relpath) * "../") + "README.html",
        "examples": (len(relpath) * "../") + "Examples/Serpentine/README.html",
        "demos": (len(relpath) * "../") + "Demos/Serpentine/README.html",
        "docs": (len(relpath) * "../") + "Documents/index.html"
        })
    
    for html in new:
        f.write(html)
    f.write('</body>\n</html>\n')
    f.close()
    
    print "Processed", file_name, "to create", output_file


def process(directory, relpath = []):

    for name in os.listdir(directory):
    
        obj = os.path.join(directory, name)
        
        if os.path.isdir(obj):
            process(obj, relpath + [name])
        
        elif obj.endswith(".md"):
            process_file(obj, relpath)


if __name__ == "__main__":

    process(os.curdir)
    sys.exit()
