"""
Copyright (C) 2016 David Boddie <david@boddie.org.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import os, sys

from Dalvik.binxml import *
from Dalvik.binxmltools import Element, create_contents, find_xml_resources, \
                               resource_map, type_map

class LinearLayout(Element):

    name = "LinearLayout"
    args = {"android:orientation":   1,             # vertical
            "android:layout_width":  0xffffffff,    # fill_parent
            "android:layout_height": 0xffffffff}    # fill_parent

class TextView(Element):

    name = "TextView"
    args = {"android:layout_width":  0xffffffff,    # fill_parent
            "android:layout_height": 0xfffffffe}    # wrap_content

class ImageView(Element):

    name = "ImageView"
    args = {"android:layout_width":  0xffffffff,    # fill_parent
            "android:layout_height": 0xfffffffe}    # wrap_content


def create_file(elements, file_name):

    """Create and write a layout file with the given file_name containing the
    elements specified."""
    
    resources = set()
    attributes = []
    
    # Find arguments corresponding to resource attributes in the layout.
    find_xml_resources(elements, resources, attributes)
    
    # The index of each resource_ids needs to match the indexes of the
    # corresponding string in the string pool that we will create later.
    # They also need to be sorted in ascending numerical order.
    resources = list(resources)
    resources.sort()
    
    resource_ids = []
    strings = []
    
    for resource_id, string in resources:
        resource_ids.append(resource_id)
        strings.append(string)
    
    for attribute in attributes:
        if attribute not in strings:
            strings.append(attribute)
    
    # First, start the contents with some boilerplate elements and add the
    # strings they use to the string pool.
    contents = [XMLResourceMap(resource_ids),
                XMLStartNamespace(prefix="android",
                uri="http://schemas.android.com/apk/res/android")]
    
    strings += [u"android", u"http://schemas.android.com/apk/res/android"]
    
    # Construct the list of contents from the layout structure itself and
    # complete the string pool.
    ids = {}
    contents += create_contents(elements, strings, ids = ids)
    
    # Append trailing boilerplate.
    contents.append(XMLEndNamespace(prefix="android",
                    uri="http://schemas.android.com/apk/res/android"))
    
    string_pool = StringPool(strings)
    
    # Create a binary XML file to hold the layout but don't save it yet. It may
    # contain id attribute definitions that need to be turned into resource ID
    # values.
    xml = XML(string_pool, contents)
    layout_file = File(root=xml)
    return layout_file, ids
