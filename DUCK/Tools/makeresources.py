#!/usr/bin/env python

"""
Copyright (C) 2016 David Boddie <david@boddie.org.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import commands, os, shutil, subprocess, sys

from Dalvik.binxml import *
from Dalvik.binxmltools import from_xml, to_binxml
from common import find_option, IconCache

known_types = [u"attr", u"drawable", u"layout", u"menu", u"id", u"raw",
               u"string", u"values", u"xml"]
known_value_types = [u"bool", u"color", u"integer", u"array"]
type_sections = [u"attr", u"drawable", u"layout", u"menu", u"id", u"raw",
                 u"string", u"xml"]

# See Android's docs/guide/topics/resources/providing-resources.html
# for a list of resolutions.
drawable_resolutions = {
    "drawable-ldpi": (36, 120),
    "drawable-mdpi": (48, 160),
    "drawable-hdpi": (72, 240),
    "drawable-xdpi": (96, 320),
    "drawable-xxdpi": (144, 480),
    "drawable-xxxdpi": (192, 640)
    }

default_resolutions = ("drawable-ldpi", "drawable-mdpi", "drawable-hdpi")
all_resolutions = default_resolutions + ("drawable-xdpi", "drawable-xxdpi",
                                         "drawable-xxxdpi")

def create_resources(app_name, package_name, res_map, drawables, sdk_version = 4):

    """Writes a resources.arsc file containing the information supplied.
    The app_name is the name of the application as shown by the launcher,
    the package_name is the period-separated name that identifies the package
    containing the application.
    The layout_file is the name of the XML file containing the application's
    GUI layout relative the root directory of the package.
    The res_map dictionary contains file names of PNG files for different
    resolutions relative to the root directory of the package. Information
    related to these file names is provided in the drawables argument."""
    
    string_pool = StringPool([])
    type_strings = []
    key_strings = []
    
    res_map.setdefault(u"string", {})[u"app_name"] = app_name
    
    # Add the type of each resource in the resource map to the list of type
    # strings, add the keys to the key strings, and calculate the type indices
    # for each of the types. Types are 1-indexed.
    type_numbers = {}
    
    for key in known_types:
    
        if key in res_map:
        
            # If the key is one of those that will be used to reference
            # collections of resources then add it to the list of type strings
            # and create an index for it.
            if key in type_sections:
                type_strings.append(key)
                type_numbers[key] = len(type_strings)
            
            key_strings += res_map[key].keys()
    
    # Create a package and fill in its types and type specifications.
    package = Package(id=0x7f, package_name=package_name,
        type_strings=StringPool(type_strings),
        last_public_type=len(type_strings),
        key_strings=StringPool(key_strings),
        last_public_key=len(key_strings))
    
    # Define the types for each resource type.
    package.types = {}
    entry_types = {}
    # Keep track of type string indices.
    indices = {}
    
    # Look in any values entry in the resources and move individual strings
    # to the string entry.
    for res_name, value in res_map.get(u"values", {}).items():
        value_type, type_string = get_value_type(value)
        if type_string == u"string":
            res_map[u"string"][res_name] = value
            del res_map[u"values"][res_name]
    
    for key in res_map:
    
        if key == u"drawable":
        
            type_ = type_numbers[key]
            types = []
            
            for i, drawable in enumerate(drawables):
            
                file_name, size, resolution = drawable
                entries = []
                
                drawable_items = res_map[key].items()
                drawable_items.sort()
                
                for res_name, png_files in drawable_items:
                
                    png_file = png_files[i]
                    entries.append(Resource(key=package.key_strings.index(res_name),
                                            values=[(TYPE_STRING, len(string_pool.strings))]))
                    string_pool.strings.append(png_file)
                
                types.append(Type(type_,
                             config=Config(density=resolution,
                                           sdk_version=sdk_version),
                             entries=entries))
            
            package.types[type_] = types
        
        elif key == u"values":
        
            for res_name, value in res_map[key].items():
            
                key_index = package.key_strings.index(res_name)
                value_type, type_string = get_value_type(value)
                
                if type_string not in type_strings:
                    type_strings.append(type_string)
                    type_ = type_numbers[type_string] = len(type_strings)
                else:
                    type_ = type_numbers[type_string]
                
                entry = add_value(string_pool, key_index, value_type, value)
                
                et = entry_types.setdefault((type_, None), [])
                et.append(entry)
        
        elif key == u"id":
        
            type_ = type_numbers[key]
            entries = []
            
            for res_name, attr in res_map[key].items():
            
                # Generate a resource ID for each id value.
                value = 0x7f000000 | (type_ << 16) | attr.value
                attr.value = value
                
                # Apparently, id values are stored as false boolean values.
                key_index = package.key_strings.index(res_name)
                entry = add_value(string_pool, key_index, TYPE_BOOLEAN, 0)
                entries.append(entry)
            
            package.types[type_] = [Type(type_, config=Config(sdk_version=sdk_version),
                                         entries=entries)]
        elif key in known_types:
        
            # Handle other known types as strings, including "string", which is
            # used for the application name.
            type_ = type_numbers[key]
            # Create a list to contain the entries and record the indices in
            # the list that correspond to each entry. This allows alternative
            # sets of strings, for example, to produce lists where the entries
            # have the same positions as those in the original list.
            et = entry_types[(type_, None)] = []
            indices[type_] = {}
            
            for res_name, value in res_map[key].items():
            
                key_index = package.key_strings.index(res_name)
                entry = add_value(string_pool, key_index, TYPE_STRING, value)
                
                # Record where this res_name is in the list of entries.
                indices[res_name] = len(et)
                et.append(entry)
        
        # Ignore non-standard keys. Any that are relevant should have already
        # been processed.
    
    for key in res_map:

        if key.startswith(u"string-"):
        
            # Handle strings with localisation (and maybe other) qualifiers.
            type_key, qualifier = key.split(u"-")
            
            type_ = type_numbers[type_key]
            
            # Reserve space to match the number of entries in the unqualified list.
            et = entry_types[(type_, qualifier)] = [None] * len(entry_types[(type_, None)])
            
            for res_name, value in res_map[key].items():
            
                try:
                    key_index = package.key_strings.index(res_name)
                except ValueError:
                    raise KeyError("Resource '%s' used in '%s' dictionary is not defined in '%s' dictionary." % (
                        res_name, key, type_key))
                
                entry = add_value(string_pool, key_index, TYPE_STRING, value)
                # Find the index of the entry with the same resource name in
                # the unqualified list and store this entry in this list at
                # the same index.
                type_index = indices[res_name]
                et[type_index] = entry
    
    # Create type elements for each of the types used, taking into account any
    # qualifiers (currently, only the language).
    for specifier, entries in entry_types.items():
        type_, language = specifier
        pt = package.types.setdefault(type_, [])
        cfg = Config(sdk_version=sdk_version, language=language)
        pt.append(Type(type_, config=cfg, entries=entries))
    
    # Define the type specifications.
    package.type_specs = {}
    
    for key in type_strings:
    
        type_ = type_numbers[key]
        
        if key == u"attr":
            type_spec = TypeSpec(type_)
        elif key == u"drawable":
            type_spec = TypeSpec(type_,
                [0x100] * len(res_map[key])) # CONFIG_DENSITY
        elif key == u"id":
            type_spec = TypeSpec(type_,
                [0] * len(res_map[key]))
        elif key == u"layout":
            type_spec = TypeSpec(type_,
                [0] * len(res_map[key]))
        elif key == u"menu":
            type_spec = TypeSpec(type_,
                [0] * len(res_map[key]))
        elif key == u"string":
            type_spec = TypeSpec(type_,
                [0] * len(entry_types[(type_, None)]))
        elif key == u"raw":
            type_spec = TypeSpec(type_,
                [0] * len(res_map[key]))
        elif key == u"values":
            type_spec = TypeSpec(type_,
                [0] * len(res_map[key]))
        elif key == u"xml":
            type_spec = TypeSpec(type_,
                [0] * len(res_map[key]))
        elif key in known_value_types:
            type_spec = TypeSpec(type_,
                [0] * len(package.types[type_][0].entries))
        else:
            raise KeyError("Unknown key type %s specified in the application's resources." % repr(key))
        
        package.type_specs[type_] = type_spec
    
    packages = {0x7f: package}
    
    table = Table(string_pool, packages)
    resources = File(root=table)
    return resources


colour_types = {3: TYPE_INT_COLOR_RGB4, 4: TYPE_INT_COLOR_ARGB4,
                6: TYPE_INT_COLOR_RGB8, 8: TYPE_INT_COLOR_ARGB8}

def get_value_type(value):

    type_ = type(value)
    
    if type_ == bool:
        return TYPE_BOOLEAN, u"bool"
    
    elif type_ == int:
        return TYPE_INT_DEC, u"integer"
    
    elif type_ == str or type_ == unicode:
    
        if value and value[0] == '#':
            try:
                v = int(value[1:], 16)
            except ValueError:
                return TYPE_STRING, u"string"
            
            try:
                return colour_types[len(value) - 1], u"color"
            except KeyError:
                return TYPE_STRING, u"string"
        else:
            return TYPE_STRING, u"string"
    
    elif type_ == list or type_ == tuple:
    
        item_type, item_type_string = get_value_type(value[0])
        return item_type, u"array"
    
    else:
        return TYPE_STRING, u"string"


def add_value(string_pool, key_index, type_, value):

    if type(value) == list or type(value) == tuple:
    
        flags = FLAG_COMPLEX
        res_values = []
        values = value
        
        if type_ != TYPE_INT_DEC and type_ != TYPE_STRING:
            is_reference = True
        else:
            is_reference = False
        
        for value in values:
            item_type, item_type_string = get_value_type(value)
            res_values.append(encode_value(string_pool, key_index, item_type,
                                           value, False))
    else:
        flags = 0
        res_values = [encode_value(string_pool, key_index, type_, value)]
    
    return Resource(key=key_index, flags=flags, values=res_values)


def encode_value(string_pool, key_index, type_, value, is_reference = False):

    if type_ == TYPE_STRING:
    
        res_value = (type_, len(string_pool.strings))
        string_pool.strings.append(value)
    
    elif type_ == TYPE_BOOLEAN:
    
        if value == True:
            res_value = (type_, 0xffffffff)
        else:
            res_value = (type_, 0)
    
    elif type_ == TYPE_INT_DEC:
    
        if value < 0:
            value = 0x100000000 + value
        
        res_value = (type_, value)
    
    elif type_ in colour_types.values():
        res_value = (type_, int(value[1:], 16))
    
    else:
        raise TypeError("Unknown resource type %i specified in the application's resources." % type_)
    
    return res_value


image_convertors = {
    ".svg": [
        ("-W %i -H %i", "cairosvg-py2 %s -o %s %l"),
        ("-w %i -h %i", "inkscape %l %s -e %s"),
        ("%i %i", "ksvgtopng %l %s %s"),
        ("%ix%i", "convert -background 'rgba(0,0,0,0)' -geometry %l %s %s")
        ],
    ".png": [
        ("%ix%i", "convert -background 'rgba(0,0,0,0)' -geometry %l %s %s")
        ],
    ".ppm": [
        ("%ix%i", "convert -background 'rgba(0,0,0,0)' -geometry %l %s %s")
        ]
    }

def create_icons(package_name, drawables, conversions, options):

    """Creates PNG files from the given SVG file at each size in the list of
    sizes specified, writing the images to the corresponding file names in the
    png_files list provided."""
    
    cache = IconCache(options.get("icon cache", False))
    
    for (res_name, svg_file), png_files in conversions.items():
    
        for (dirname, size, resolution), png_file in zip(drawables, png_files):
        
            icon_str = "%s_%s_%s_%i_%i.png" % (package_name, res_name, dirname,
                                               size, resolution)
            if cache.get_icon(svg_file, icon_str, png_file):
                continue
            
            suffix = os.path.splitext(svg_file)[1]
            
            for size_template, command_template in image_convertors[suffix]:
            
                size_str = size_template % (size, size)
                command = []
                file_names = [svg_file, png_file]
                
                for piece in command_template.split():
                    if piece == "%s":
                        # Substitute the string for the placeholder.
                        command.append(file_names.pop(0))
                    elif piece == "%l":
                        # Split the replacement string and append the pieces.
                        command += size_str.split()
                    else:
                        # Append the original non-placeholder string.
                        command.append(piece)
                
                try:
                    s = subprocess.Popen(command, stdout=subprocess.PIPE,
                                                  stderr=subprocess.STDOUT)
                    if s.wait() == 0:
                        break
                except:
                    pass
            else:
                raise NameError("Failed to run a SVG to PNG conversion tool.")
            
            # Allow the build script to specify that the pngquant tool should
            # be used to reduce the number of colours in the generated PNGs,
            # running it on each icon and renaming the result to replace the
            # existing PNG file.
            if "pngquant" in options:
                os.system("cat " + commands.mkarg(png_file) + " | pngquant " + \
                          options["pngquant"] + " > " + \
                          commands.mkarg(png_file + ".new"))
                os.rename(png_file + ".new", png_file)
            
            cache.set_icon(icon_str, png_file)


def create_resource_classes(res_info):

    """Create a string containing the source code describing the resource
    identifiers available from the res_info binxml.File object containing a
    Table as a root element."""
    
    # The table contains a dictionary of packages, each of which contains
    # information about the resources they contain.
    keys = res_info.root.packages.keys()
    keys.sort()
    
    sources = {}
    
    for key in keys:
    
        package = res_info.root.packages[key]
        
        # Use the package name to declare a root resource class for this package.
        source = u'__package__ = "%s"\n\n' % package.package_name
        source += u'from java.lang import Object\n\n'
        source += u'class R(Object):\n\n'
        source += u'    __final__ = True\n\n'
        source += u'    def __init__(self):\n'
        source += u'        Object.__init__(self)\n\n'
        
        for i, res_member_name in enumerate(package.type_strings):
        
            source += u'    class %s(Object):\n\n' % res_member_name
            source += u'        __final__ = True\n\n'
            
            # Create fields for each of the resources held by the package for
            # this type, remembering that the type indices are 1-based.
            
            fields = set()
            
            for type_ in package.types.get(i + 1, []):
            
                for j, resource in enumerate(type_.entries):
                
                    if resource == None: continue
                    
                    key_string = package.key_strings[resource.key]
                    # Only handle the first value.
                    value_type, value_index = resource.values[0]
                    identifier = (package.id << 24) | ((i + 1) << 16) | j
                    fields.add((identifier, key_string))
            
            fields = list(fields)
            fields.sort()
            
            for identifier, key_string in fields:
                source += u'        %s = 0x%x\n' % (key_string, identifier)
            
            if fields:
                source += u'\n'
            
            source += u'        def __init__(self):\n'
            source += u'            Object.__init__(self)\n\n'
        
        sources[key] = source
    
    return sources


def create_file(resource_root, app_name, package_name, res_files, output_file,
    resolutions = default_resolutions, sdk_version = 4, options = {}):

    """Builds the resources file for the application with the given app_name
    and corresponding package_name, including information about the layout_file
    specified as a path within the package structure. Writes the resources to
    the output_file specified.
    """
    
    output_dir = os.path.split(output_file)[0]
    
    res_dir = os.path.join(output_dir, "res")
    if not os.path.exists(res_dir):
        os.mkdir(res_dir)
    
    # Copy the res_files dictionary since we will modify it.
    new_res_files = {}
    for key, value in res_files.items():
        new_res_files[key] = value.copy()
    
    if u"drawable" in res_files:
    
        # Create a dictionary mapping keys containing tuples of resource names and
        # SVG file names to lists of PNG files. Each PNG file corresponds to a
        # drawable in the following list.
        
        conversions = {}
        drawables = []
        
        for dirname in resolutions:
        
            size, resolution = drawable_resolutions[dirname]
            drawables.append((dirname, size, resolution))
            
            drawable_dir = os.path.join(res_dir, dirname)
            
            if not os.path.exists(drawable_dir):
                os.mkdir(drawable_dir)
            
            drawable_items = res_files[u"drawable"].items()
            
            for res_name, original_name in drawable_items:
                src_path = os.path.join(resource_root, original_name)
                group = conversions.setdefault((res_name, src_path), [])
                group.append(os.path.join(drawable_dir, res_name + ".png"))
        
        # Generate the PNG files for each SVG file at the resolutions given in the
        # drawables list.
        create_icons(package_name, drawables, conversions, options)
        
        # Now that the PNG files have been created, convert the paths to package
        # relative paths for inclusion in the resources file.
        for (res_name, original_name), png_files in conversions.items():
        
            new_res_files[u"drawable"][res_name] = map(lambda png_file:
                os.path.relpath(png_file, output_dir), png_files)
    
    if u"raw" in res_files:
    
        raw_dir = os.path.join(res_dir, "raw")
        if not os.path.exists(raw_dir):
            os.mkdir(raw_dir)
        
        for key, value in res_files[u"raw"].items():
            src = os.path.join(resource_root, value)
            dest = os.path.join(raw_dir, os.path.split(value)[1])
            shutil.copy2(src, dest)
            new_res_files[u"raw"][key] = os.path.join(u"res", u"raw", os.path.split(dest)[1])
    
    if u"xml" in res_files:
    
        # Create a preliminary resources file so that XML files can refer to other
        # resources then process the XML files.
        resources = create_resources(app_name, package_name, new_res_files, drawables,
                                     sdk_version = sdk_version)
        
        xml_dir = os.path.join(res_dir, "xml")
        if not os.path.exists(xml_dir):
            os.mkdir(xml_dir)
        
        for key, value in res_files[u"xml"].items():
            src = os.path.join(resource_root, value)
            dest = os.path.join(xml_dir, os.path.split(value)[1])
            
            # Encode the XML file as binary XML, using the preliminary
            # resources file to resolve references to other resources.
            binxml_file = to_binxml(from_xml(src), resources)
            binxml_file.save(dest)
            
            new_res_files[u"xml"][key] = os.path.join(u"res", u"xml", os.path.split(dest)[1])
    
    resources = create_resources(app_name, package_name, new_res_files, drawables)
    resources.save(output_file)
    return resources
