"""
Copyright (C) 2016 David Boddie <david@boddie.org.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import os

from Dalvik.binxml import *
from Dalvik.binxmltools import Element, create_contents, find_xml_resources, \
                               resource_map, type_map

class Manifest(Element):

    name = "manifest"
    args = {"android:versionCode": 1,
            "android:versionName": "1.0"}

class UsesPermission(Element):

    name = "uses-permission"
    args = {"android:name": ""}

class UsesFeature(Element):

    name = "uses-feature"
    args = {"android:name": ""}

class UsesSDK(Element):

    name = "uses-sdk"
    args = {}

class Application(Element):

    name = "application"
    args = {"android:label": "@string/app_name"}

class Activity(Element):

    name = "activity"
    args = {"android:label": "@string/app_name"}

class Provider(Element):

    name = "provider"

class GrantUriPermission(Element):

    name = "grant-uri-permission"

class Receiver(Element):

    name = "receiver"

class Service(Element):

    name = "service"
    args = {"android:permission": "android.permission.BIND_REMOTEVIEWS",
            "android:exported": False}

class IntentFilter(Element):

    name = "intent-filter"

class Action(Element):

    name = "action"
    args = {"android:name": "android.intent.action.MAIN"}

class Category(Element):

    name = "category"
    args = {"android:name": "android.intent.category.LAUNCHER"}

class Data(Element):

    name = "data"
    args = {}

class MetaData(Element):

    name = "meta-data"
    args = {"android:name": "android.appwidget.provider"}


# Define template manifests for differents kinds of applications.

def create_activity_manifest(package_name, app_class_name, manifest_desc,
                             activity_desc = {}):

    manifest_desc["android:versionCode"] = version_code(manifest_desc["android:versionName"])
    activity_desc.update({"android:name": app_class_name})
    
    return [
        Manifest(manifest_desc, [
            # Features and permissions must be inserted here.
            Application({"android:icon": "@drawable/ic_launcher"}, [
                Activity(activity_desc, [
                    IntentFilter({}, [
                        Action(),
                        Category()
                        ])
                    ])
                ])
            ])
        ]


def create_widget_manifest(package_name, app_class_names, manifest_desc,
                           receiver_desc = {}):

    manifest_desc["android:versionCode"] = version_code(manifest_desc["android:versionName"])
    
    widget_provider_name = app_class_names["android/appwidget/AppWidgetProvider"]
    
    service = []
    provider = []
    
    app_service_name = app_class_names.get("android/app/Service")
    if app_service_name:
        service.append(Service({"android:name": app_service_name}))
    
    app_provider_name = app_class_names.get("android/content/ContentProvider")
    if app_provider_name:
        provider.append(Provider({"android:name": app_provider_name,
                        "android:authorities": package_name + ".provider"}))
    
    resource_location = "@xml/" + receiver_desc["resource"]
    
    return [
        Manifest({"package": package_name}, [
            # Features and permissions must be inserted here.
            Application({}, [
                Receiver({"android:name": widget_provider_name}, [
                    IntentFilter({}, [
                        Action({"android:name": "android.appwidget.action.APPWIDGET_UPDATE"}),
                        ]),
                    MetaData({"android:resource": resource_location}), 
                    ])
                ] + \
                service + provider) # optional features
            ])
        ]


def create_file(elements, res, features, permissions, file_name, sdk_version):

    """Create and write a layout file with the given file_name containing the
    elements specified."""
    
    # If the SDK version is not described in the permissions list, set the SDK
    # version to try to ensure that there are no default permissions.
    if not filter(lambda p: isinstance(p, UsesSDK), permissions):
        permissions.insert(0, UsesSDK({"android:minSdkVersion": sdk_version,
                                       "android:targetSdkVersion": sdk_version}))
    
    # Insert elements for features and permissions into the above structure.
    for feature in features:
        elements[0].children.insert(-1, UsesFeature({"android:name": feature}))
    
    for permission in permissions:
        if isinstance(permission, Element):
            elements[0].children.insert(-1, permission)
        else:
            elements[0].children.insert(-1, UsesPermission({"android:name": permission}))
    
    resources = set()
    attributes = []
    
    # Find arguments corresponding to resource attributes.
    find_xml_resources(elements, resources, attributes)
    
    # The index of each resource_ids needs to match the indexes of the
    # corresponding string in the string pool that we will create later.
    # They also need to be sorted in ascending numerical order.
    resources = list(resources)
    resources.sort()
    
    resource_ids = []
    strings = []
    
    for resource_id, string in resources:
        resource_ids.append(resource_id)
        strings.append(string)
    
    for attribute in attributes:
        if attribute not in strings:
            strings.append(attribute)
    
    # First, start the contents with some boilerplate elements and add the
    # strings they use to the string pool.
    contents = [XMLResourceMap(resource_ids),
                XMLStartNamespace(prefix="android",
                uri="http://schemas.android.com/apk/res/android")]
    
    strings += [u"android", u"http://schemas.android.com/apk/res/android"]
    
    # Construct the list of contents from the document structure itself and
    # complete the string pool.
    contents += create_contents(elements, strings, resources = res)
    
    # Append trailing boilerplate.
    contents.append(XMLEndNamespace(prefix="android",
                    uri="http://schemas.android.com/apk/res/android"))
    
    string_pool = StringPool(strings)
    
    xml = XML(string_pool, contents)
    manifest_file = File(root=xml)
    manifest_file.save(file_name)


def version_code(version):

    """Returns an integer version code for a period-separated version string
    containing a number of fields, where each field contains an integer value
    with a value in the range [0, 9]."""

    pieces = version.split(".")
    pieces.reverse()
    
    value = 0
    factor = 1
    for piece in pieces:
        value = value + (int(piece) * factor)
        factor *= 10
    
    return value
