#!/usr/bin/env python

"""
Copyright (C) 2016 David Boddie <david@boddie.org.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from Dalvik import bytecode, dex
import sys

type_code_strings = {
    0x0000: "TYPE_HEADER_ITEM",
    0x0001: "TYPE_STRING_ID_ITEM",
    0x0002: "TYPE_TYPE_ID_ITEM",
    0x0003: "TYPE_PROTO_ID_ITEM",
    0x0004: "TYPE_FIELD_ID_ITEM",
    0x0005: "TYPE_METHOD_ID_ITEM",
    0x0006: "TYPE_CLASS_DEF_ITEM",
    0x1000: "TYPE_MAP_LIST",
    0x1001: "TYPE_TYPE_LIST",
    0x1002: "TYPE_ANNOTATION_SET_REF_LIST",
    0x1003: "TYPE_ANNOTATION_SET_ITEM",
    0x2000: "TYPE_CLASS_DATA_ITEM",
    0x2001: "TYPE_CODE_ITEM",
    0x2002: "TYPE_STRING_DATA_ITEM",
    0x2003: "TYPE_DEBUG_INFO_ITEM",
    0x2004: "TYPE_ANNOTATION_ITEM",
    0x2005: "TYPE_ENCODED_ARRAY_ITEM",
    0x2006: "TYPE_ANNOTATIONS_DIRECTORY_ITEM"
    }

tags = []

def html(text):
    return text.replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;").encode("utf-8")

def begin(name, attributes = []):
    global tags
    attr = ""
    if attributes:
        attr = " " + " ".join(attributes)
    tags.append(name)
    return (u"<%s%s>" % (name, attr)).encode("utf-8")

def end():
    global tags
    name = tags.pop()
    return (u"</%s>" % name).encode("utf-8")

def ele(name, text = "", attributes = []):
    if text or attributes:
        return begin(name, attributes) + html(text) + end()
    else:
        return (u"<%s />" % name).encode("utf-8")

def print_fields(fields):

    format = "%%%ii " % len(str(len(fields)))
    print begin("pre")
    for i, field in enumerate(fields):
        print html((format % i) + "%s %s %s" % (field.class_type, field.type_, field.name))
    print end()

def print_methods(methods):

    for method in methods:
        print begin("li")
        print html("%x %s %s %s(%s)" % (
            method.access_flags.value, method.access_flags,
            method.proto.return_type, method.name,
            ",".join(map(str, method.proto.parameters))
            ))
        if method.code:
            print ele("br")
            print ele("strong", "Offset:")
            print html(hex(method.code.offset))
            print ele("br")
            print ele("strong", "Input:")
            print html(str(method.code.ins_size))
            print ele("br")
            print ele("strong", "Output:")
            print html(str(method.code.outs_size))
            print ele("br")
            print ele("strong", "Registers used:")
            print html(str(method.code.registers_size))
            print ele("br")
            
            if method.code.tries_and_handlers:
                print ele("strong", "Tries:")
                print ele("br")
            
            for start, count, catch_handler in method.code.tries_and_handlers:
            
                print ele("em", "Start:")
                print html("%i " % start)
                print ele("em", "Count:")
                print html("%i " % count)
                print ele("br")
                
                catch_type_handlers, catch_all_handler = catch_handler
                for catch_type, offset in catch_type_handlers:
                    print ele("em", "Type:")
                    print html("%s " % catch_type)
                    print ele("em", "Offset:")
                    print html("%s " % hex(offset))
                    print ele("br")
                
                if catch_all_handler != None:
                    print ele("em", "Catch all:")
                    print html("%s " % hex(catch_all_handler))
                    print ele("br")
            
            instructions = bytecode.disassemble(method.code.instructions,
                                                return_offsets=True)
            if instructions:
                offset, instruction = instructions[-1]
                last_offset = offset + instruction.size - 1
                w = len("%x" % last_offset)
                t = "%%0%ix %%04x " % w
            
            print begin("pre")
            o = 0
            for offset, instruction in instructions:
                while o < offset:
                    print html(t % (o, method.code.instructions[o]))
                    o += 1
                print html(t % (o, method.code.instructions[o]) + repr(instruction))
                o += 1
            print end()
            if method.code.debug_info_offset != 0:
                print ele("strong", "Debug information:")
                print html(hex(method.code.debug_info_offset))
                print ele("br")
        else:
            print ele("br")
            print html("No code (%s)" % repr(method.code))
        
        print end()

if __name__ == "__main__":

    if len(sys.argv) != 2:
        sys.stderr.write("Usage: %s <dex file>\n" % sys.argv[0])
        sys.exit(1)
    
    dex_file = sys.argv[1]
    
    d = dex.Dex()
    d.read(open(dex_file))
    
    print begin("html")
    print begin("head")
    print ele("meta", "", ['http-equiv="Content-Type"', 'content="text/html; charset=utf-8"'])
    print begin("style", ["type=text/css"])
    print html('th { text-align: left }\n'
               'td { text-align: left }')
    print end()
    print end()
    
    print begin("body")
    print ele("h1", "Header")
    print ele("strong", "Signature")
    print ele("span", "".join(map(lambda x: "%02x" % ord(x), d.header.signature)))
    print ele("br")
    print ele("strong", "Checksum")
    print ele("span", hex(d.header.checksum))
    print ele("br")
    if d.header.endian_tag == 0x12345678:
        print ele("strong", "Little-endian")
    else:
        print ele("strong", "Big-endian")
    print
    print begin("table")
    for typecode, name in d.header._format:
        print begin("tr")
        if name.endswith("_offset"):
            print ele("th", name)
            print ele("td", hex(getattr(d.header, name)))
            size_name = name.replace("_offset", "_size")
            if hasattr(d.header, size_name):
                print ele("th", size_name)
                print ele("td", hex(getattr(d.header, size_name)))
        print end()
    print end()
    
    if d.map:
        print ele("h1", "Map")
        print begin("table")
        print begin("tr")
        print ele("th", "Type code name")
        print ele("th", "Type code")
        print ele("th", "Offset")
        print ele("th", "Number of entries")
        print end()
        for type_code, unused, number, offset in d.map:
            print begin("tr")
            print ele("td", "%s" % type_code_strings[type_code])
            print ele("td", hex(type_code))
            print ele("td", hex(offset))
            print ele("td", str(number))
            print end()
        print end()
    
    print ele("h1", "Strings")
    format = "%%%ii " % len(str(len(d.string_ids)))
    print begin("pre")
    for i, string in enumerate(d.string_ids):
        print html((format % i) + string)
    print end()
    print
    
    print ele("h1", "Types")
    format = u"%%%ii " % len(str(len(d.type_ids)))
    print begin("pre")
    for i, type_ in enumerate(d.type_ids):
        print html((format % i) + type_.desc)
    print end()
    print
    
    print ele("h1", "Prototypes")
    format = "%%%ii " % len(str(len(d.proto_ids)))
    print begin("pre")
    for i, prototype in enumerate(d.proto_ids):
        print html((format % i) + "%s (%s)" % (prototype.return_type, ",".join(map(str, prototype.parameters))))
    print end()
    print
    
    print ele("h1", "Fields")
    print_fields(d.field_ids)
    print
    
    print ele("h1", "Methods")
    format = "%%%ii " % len(str(len(d.method_ids)))
    print begin("pre")
    for i, method in enumerate(d.method_ids):
        print html((format % i) + "%s %s.%s(%s)" % (method.proto.return_type, method.class_type.desc[1:-1], method.name, ",".join(map(str, method.proto.parameters))))
    print end()
    print
    
    print ele("h1", "Class definitions")
    print begin("ul")
    for class_def in d.class_defs:
        print begin("li")
        print ele("a", class_def.class_type.value, attributes=['href="#%s"' % class_def.class_type.value])
        print end()
    print end()
    
    for class_def in d.class_defs:
    
        print ele("h2", class_def.class_type.value, attributes=['id="%s"' % class_def.class_type.value])
        print ele("strong", "Access flags")
        print ele("span", "%s (%s)" % (class_def.access_flags, hex(class_def.access_flags.value)))
        print ele("br")
        
        print ele("strong", "Superclass type")
        print ele("span", class_def.superclass_type.value)
        print ele("br")
        
        if class_def.interfaces:
            print ele("strong", "Interfaces")
            print begin("ul")
            for interface in class_def.interfaces:
                print ele("li", interface.value)
            print end()
        
        print ele("strong", "Source file")
        print ele("span", class_def.source_file)
        print ele("br")
        
        if class_def.annotations:
            print ele("strong", "Annotations")
            print begin("dl")
            
            annotations = class_def.annotations
            
            if annotations.class_annotations:
                print ele("dt", "Class annotations")
                print begin("dd")
                print begin("ul")
                for visibility, annotation in annotations.class_annotations:
                    print begin("li")
                    print html("%s %s" % (visibility, annotation))
                    print end()
                print end()
                print end()
            
            if annotations.fields:
                print ele("dt", "Field annotations")
                print begin("dd")
                print begin("ul")
                for field in annotations.fields:
                    print begin("li")
                    print html("%s" % repr(field))
                    print end()
                print end()
                print end()
            
            if annotations.methods:
                print ele("dt", "Method annotations")
                print begin("dd")
                print begin("ul")
                for method, method_annotations in annotations.methods:
                    print begin("li")
                    print html("%s %s(%s)" % (method.proto.return_type,
                        method.name, ",".join(map(str, method.proto.parameters))))
                    if method_annotations:
                        print begin("ul")
                        for annotation in method_annotations:
                            print ele("li", repr(annotation))
                        print end()
                    print end()
                print end()
                print end()
            
            if annotations.parameters:
                print ele("dt", "Parameter annotations")
                print begin("dd")
                print begin("ul")
                for method in annotations.parameters:
                    print begin("li")
                    print html("%s" % repr(method))
                    print end()
                print end()
                print end()
            
            print end()
        
        print ele("br")
        print ele("strong", "Class data")
        if class_def.class_data:
            print begin("dl")
            if class_def.class_data.static_fields:
                print ele("dt", "Static fields")
                print begin("dd")
                print begin("ul")
                for field, access_flags in class_def.class_data.static_fields:
                    print begin("li")
                    print html("%s %s %s" % (access_flags, field.type_, field.name))
                    print end()
                print end()
                print end()
            if class_def.class_data.instance_fields:
                print ele("dt", "Instance fields")
                print begin("dd")
                print begin("ul")
                for field, access_flags in class_def.class_data.instance_fields:
                    print begin("li")
                    print html("%s %s %s" % (access_flags, field.type_, field.name))
                    print end()
                print end()
                print end()
            if class_def.class_data.direct_methods:
                print ele("dt", "Direct methods")
                print begin("dd")
                print begin("ul")
                print_methods(class_def.class_data.direct_methods)
                print end()
                print end()
            if class_def.class_data.virtual_methods:
                print ele("dt", "Virtual methods")
                print begin("dd")
                print begin("ul")
                print_methods(class_def.class_data.virtual_methods)
                print end()
                print end()
            print end()
        
        if class_def.static_values:
            print ele("strong", "Static values")
            print begin("ul")
            for value in class_def.static_values:
                print ele("li", repr(value))
            print end()
    
    print
    while tags:
        print end()
