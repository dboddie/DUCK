Demonstration Serpentine Programs
=================================

This directory contains demonstration applications written in the Serpentine
language that need to be run on a device with the Dalvik or ART virtual
machines. They are also used as regression tests for any API or compiler
breakage since changes to either of these may cause compilation to fail
completely.

To compile the demos, invoke the buildall.py script, making sure that the
Compiler package directory is in the PYTHONPATH. For example, from the root of
the distribution type the following, remembering to substitute your own [key
and certificate files](../../Documents/Keys_and_Certificates.md) for the
placeholders used:

```\
PYTHONPATH=. Demos/Serpentine/buildall.py <key.pem> <cert.pem> /tmp/packages
```

Demos can also be built individually. For example:

```\
PYTHONPATH=. Demos/Serpentine/JEFViewer/build.py <key.pem> <cert.pem> \
             /tmp/JEFViewer.apk
```

These demos are more extensive tests of functionality than those in the
[Examples](../../Examples/Serpentine/README.md) directory and rely on more of
the Java and Android APIs.

Demonstrations
--------------

 * [Fluid](Fluid/fluid.py) is a graphical demonstration that could form
   the basis for a simple game.
 * [HTTP Server](HTTPServer/httpserver.py) implements an extremely
   simple Web server.
 * [JEF Viewer](JEFViewer/jefviewer.py) displays thumbnails of
   embroidery patterns stored in Janome JEF format files.

Documentation
-------------

Some of the demos are documented using inline docstrings. These are processed
when the examples are built if the `DOCS_DIR` environment variable is set to
the name of a directory. If an absolute path is not used, the name is
interpreted as a subdirectory of the relevant example directory, as in the
following example:

```\
DOCS_DIR=docs PYTHONPATH=. Demos/Serpentine/JEFViewer/build.py \
                           <key.pem> <cert.pem> /tmp/JEFViewer.apk
```

This requires the demo's build script to have been written so that it reads the
`DOCS_DIR` environment variable and passes it to the `buildhelper.main`
function. The script uses the `gendocs` module to create an HTML file in the
documentation directory.
