#!/usr/bin/env python

import bluetooth, socket, sys

class Client:

    def __init__(self, sock):
        self.socket = sock
    
    def get_response(self):
    
        result = ""
        c = ""
        while c != "\n":
            c = self.socket.recv(1)
            result += c
        
        return result
    
    def list(self, path=""):
    
        self.socket.sendall("list " + path + "\n")
        result = self.get_response()
        
        if result.endswith("OK\n"):
            return result[:-3].rstrip().split("\t")
        else:
            return []
    
    def play(self, file_name):
    
        self.socket.sendall("play " + file_name + "\n")
        result = self.get_response()
        
        if result.endswith("OK\n"):
            return True
        else:
            return False


if __name__ == "__main__":

    if len(sys.argv) != 2:
        sys.stderr.write("Usage: %s <host address>\n" % sys.argv[0])
        sys.exit(1)
    
    host_address = sys.argv[1]
    
    services = bluetooth.find_service(
        address=host_address, uuid="7b65d594-391f-4439-9454-659d71a807e5")
    
    if not services:
        sys.stderr.write("Failed to find a host with the service.\n")
        sys.exit(1)
    
    port = services[0]["port"]
    
    s = socket.socket(socket.AF_BLUETOOTH, socket.SOCK_STREAM, socket.BTPROTO_RFCOMM)
    
    try:
        s.connect((host_address, port))
    except socket.error:
        sys.stderr.write("Failed to connect to the service.\n")
        sys.exit(1)
    
    client = Client(s)
    
    print "Use the client methods to interact with the server."
    
    client
