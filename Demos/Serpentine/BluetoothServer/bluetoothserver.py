"""
# Bluetooth Server Example

This example shows how to serve requests via a Bluetooth socket.

![A screenshot of the application.](bluetoothserver.png)

We import the classes and modules needed by our application. The most
relevant ones for this example are the `BluetoothAdapter` and `BluetoothDevice`
classes."""

from java.io import BufferedInputStream, BufferedOutputStream, File, \
                    IOException, OutputStream
from java.lang import Exception, Runnable, String, Thread
from java.net import BindException, ServerSocket, SocketException
from java.util import UUID

from android.bluetooth import BluetoothAdapter, BluetoothDevice, BluetoothServerSocket
from android.content import ActivityNotFoundException, Intent
from android.net import Uri
from android.os import Environment, Handler, Looper
from android.view import View
from android.widget import Button, LinearLayout, ScrollView, TextView, Toast

from serpentine.activities import Activity

"""The BluetoothServerActivity is derived from the custom Activity class
provided by the `serpentine` package and represents the application. Android
will create an instance of this class when the user runs it."""

class BluetoothServerActivity(Activity):

    __interfaces__ = [View.OnClickListener]
    
    START_TEXT = "Start server"
    STOP_TEXT = "Stop server"
    SERVING = "Serving"
    NOT_SERVING = "Not serving"

    """The initialisation method only needs to call the corresponding method in
    the base class."""
    
    def __init__(self):
    
        Activity.__init__(self)
        self.tracks = []
        self.playing = False
        self.job = 1
    
    """The onCreate method is called when the activity is created. Our
    implementation calls the onCreate method of the base class, obtains the
    available Bluetooth adapter and sets up a user interface."""
    
    def onCreate(self, bundle):
    
        Activity.onCreate(self, bundle)
        
        """Obtain the device's Bluetooth adapter which is itself obtained from
        the Bluetooth service. The custom activity provides a method to do this
        for us."""
        
        self.adapter = self.getBluetoothAdapter()
        
        # Define whether the server is running.
        self.running = False
        
        """We create a label to show the server's status, a button to allow the
        user to start and stop it, and a label in a scroll view to show a log
        of requests that the server has received."""
        
        self.label = TextView(self)
        
        self.button = Button(self)
        self.button.setText(self.START_TEXT)
        self.button.setOnClickListener(self)
        
        self.requestLabel = TextView(self)
        self.scrollView = ScrollView(self)
        self.scrollView.addView(self.requestLabel)
        
        """We also create a handler that will receive messages sent from the
        worker thread."""
        
        self.handler = MessageHandler(Looper.getMainLooper(), self)
        
        """Finally, we put all the widgets in a layout and make that the main
        content of the application."""
        
        layout = LinearLayout(self)
        layout.setOrientation(layout.VERTICAL)
        layout.addView(self.label)
        layout.addView(self.button)
        layout.addView(self.scrollView)
        
        self.setContentView(layout)
    
    def onClick(self, view):
    
        if not self.running:
        
            self.thread = ServerThread(self, self.adapter, self.handler)
            self.thread.start()
            self.running = True
            
            # Change the button and status label text to indicate that the
            # server is running.
            self.button.setText(self.STOP_TEXT)
            self.label.setText(self.SERVING)
        else:
            self.thread.requestStop()
    
    def serverStopped(self):
    
        # Wait until the thread has finished.
        self.thread.join()

        # Reset the button and status label.
        self.button.setText(self.START_TEXT)
        self.label.setText(self.NOT_SERVING)
        
        self.running = False
    
    @args(void, [String])
    def addLogText(self, text):
    
        s = str(self.requestLabel.getText()) + text
        self.requestLabel.setText(s)
        self.scrollView.fullScroll(View.FOCUS_DOWN)
    
    @args(void, [String])
    def playTrack(self, file_path):
    
        if not self.playing:
        
            intent = Intent()
            intent.setAction(Intent.ACTION_VIEW)
            intent.setDataAndType(Uri.parse(file_path), "audio/*")
            try:
                self.startActivityForResult(intent, self.job)
                self.job += 1
                self.playing = True
            except ActivityNotFoundException:
                pass
        else:
            self.tracks.add(file_path)
    
    def onActivityResult(self, requestCode, resultCode, data):
    
        self.addLogText(str(requestCode) + " " + str(resultCode) + "\n")
        self.playing = False
        
        if len(self.tracks) > 0:
            self.playTrack(self.tracks.removeFirst())


class MessageHandler(Handler):

    ADD_LOG_TEXT = 0
    SERVER_STOPPED = 1
    PLAY_TRACK = 2

    """Creates and dispatches messages between threads."""
    
    @args(void, [Looper, BluetoothServerActivity])
    def __init__(self, looper, activity):
    
        Handler.__init__(self, looper)
        self.activity = activity
    
    def handleMessage(self, inputMessage):
    
        if inputMessage.what == self.ADD_LOG_TEXT:
            self.activity.addLogText(str(inputMessage.obj))
        
        elif inputMessage.what == self.SERVER_STOPPED:
            self.activity.serverStopped()
        
        elif inputMessage.what == self.PLAY_TRACK:
            self.activity.playTrack(str(inputMessage.obj))


class ServerThread(Thread):

    NAME = "BluetoothServer"
    UUID = "7b65d594-391f-4439-9454-659d71a807e5"
    
    BAD_REQUEST = "Bad request"
    ERROR_TEXT = "Error text"
    
    __fields__ = {"running": bool,
                  "inputStream": BufferedInputStream,
                  "outputStream": BufferedOutputStream,
                  "serverSocket": BluetoothServerSocket}
    
    @args(void, [Activity, BluetoothAdapter, Handler])
    def __init__(self, activity, adapter, handler):
    
        Thread.__init__(self)
        
        self.activity = activity
        self.adapter = adapter
        self.handler = handler
        self.running = False
        self.inputStream = None
        self.outputStream = None
        self.serverSocket = None
    
    def run(self):
    
        # Create a server socket to handle new connections.
        try:
            self.serverSocket = self.adapter.listenUsingRfcommWithServiceRecord(
                self.NAME, UUID.fromString(self.UUID))
            self.running = True
            
        except IOException:
            self.writeLog("Listen failed\n")
        
        while self.running:
        
            try:
                self.writeLog("Waiting for a connection...\n")
                socket = self.serverSocket.accept()
                
                # Close the server socket so that new connections cannot be made.
                self.serverSocket.close()
                
                self.inputStream = BufferedInputStream(socket.getInputStream())
                self.outputStream = BufferedOutputStream(socket.getOutputStream())
                
                self.writeLog("Started handling requests...\n")
                
                while self.running:
                
                    # Handle requests on the same socket until the connection
                    # is closed by the client.
                    request = []
                    
                    l = []
                    
                    while self.running:
                    
                        b = self.inputStream.read()
                        
                        if b == 10:         # newline
                            break
                        elif b == -1:
                            raise IOException()
                        
                        l.add(byte(b))
                    
                    s = String(array(l), "UTF-8")
                    
                    self.handleRequest(s)
            
            except (SocketException,), se:
                self.writeLog(str(se) + "\n")
                self.running = False
            
            except (IOException,), ioe:
                self.writeLog(str(ioe) + "\n")
                self.running = False
        
        self.stopServer()
    
    def stopServer(self):
    
        self.writeLog("Stopped\n")
        
        message = self.handler.obtainMessage(MessageHandler.SERVER_STOPPED, None)
        message.sendToTarget()
    
    @args(void, [String])
    def handleRequest(self, line):
    
        self.writeLog(u"\u2192 " + line + "\n")
        
        if line[:5] == "play ":
            response = self.playTrack(line[5:])
        elif line[:5] == "list ":
            response = self.listFiles(line[5:])
        else:
            response = False
        
        if response == True:
            self.writeResponse('OK\n')
        else:
            self.writeResponse('Not understood\n')
    
    @args(void, [String])
    def writeResponse(self, text):
    
        self.writeLog(u"\u2190 " + text)
        self.outputStream.write(text.getBytes("UTF-8"))
        self.outputStream.flush()
    
    def requestStop(self):
    
        # Close the input stream, if opened, so that pending connection
        # socket reads are interrupted.
        if self.inputStream != None:
            self.inputStream.close()
        
        if self.serverSocket != None:
            self.serverSocket.close()
        
        self.running = False
    
    @args(void, [String])
    def writeLog(self, s):
    
        message = self.handler.obtainMessage(MessageHandler.ADD_LOG_TEXT, s)
        message.sendToTarget()
    
    @args(bool, [String])
    def playTrack(self, track):
    
        if ".." in track:
            return False
        
        directory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MUSIC)
        file = File(directory, track)
        
        if file.exists():
            self.writeLog("Playing " + track.trim() + "\n")
            file_path = "file://" + file.getPath()
            message = self.handler.obtainMessage(MessageHandler.PLAY_TRACK, file_path)
            message.sendToTarget()
            return True
        
        return False
    
    @args(bool, [String])
    def listFiles(self, subdir_path):
    
        if ".." in subdir_path:
            return False
        
        #directory = self.activity.getExternalFilesDir(Environment.DIRECTORY_MUSIC)
        directory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MUSIC)
        subdir = File(directory, subdir_path)
        
        files = subdir.list()
        if len(files) == 0:
            return False
        
        for file_name in files:
            self.writeResponse(file_name + '\t')
        
        return True
