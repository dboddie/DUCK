"""
# Display Server Example

This demonstration serves a single client on a Unix domain socket, receiving
drawing commands and displaying the result of them on a view.

We import the classes and modules that will be needed by the application. The
most relevant are the classes from the android.net module."""

from java.io import InputStream, IOException
from java.lang import Math, Runnable, String, Thread
from java.nio import ByteBuffer, ByteOrder
from android.content import Context
from android.graphics import Bitmap, BitmapFactory, Color, Canvas, Paint, Rect
from android.net import LocalServerSocket, LocalSocket, LocalSocketAddress
from android.os import Handler, Looper
from android.view import View
from android.util import Log
from android.widget import Button, LinearLayout, ScrollView, TextView, Toast
from serpentine.activities import Activity

"""We define a class based on the specialised Activity class provided by the
`serpentine.activities` module. This represents the application, and will be
used to present a graphical interface to the user."""

class LocalServerActivity(Activity):

    __interfaces__ = [Runnable, View.OnClickListener]
    __fields__ = {"screen": Bitmap, "handler": Handler}

    START_TEXT = "Start server"
    STOP_TEXT = "Stop server"
    SERVING = "Serving at "
    NOT_SERVING = "Not serving"
    PORT_IN_USE = "Port already in use"

    def __init__(self):
        Activity.__init__(self)

    def onCreate(self, bundle):

        Activity.onCreate(self, bundle)

        """We create a custom view to display the data from the client and make
        that the main content of the application."""

        self.view = DrawView(self)
        self.setContentView(self.view)

        """We also create a handler that will receive messages sent from the
        worker thread."""

        self.handler = MessageHandler(Looper.getMainLooper(), self)

        # Create a server socket to handle new connections.
        try:
            self.serverSocket = LocalServerSocket("displayserver")
        except IOException:
            Toast.makeText(self, self.PORT_IN_USE, Toast.LENGTH_SHORT).show()
            return

        # Indicate that the server is running.
        self.running = True

        # No client is currently being served.
        self.serving = False

        # Start a new thread with this instance as its runnable object.
        self.thread = Thread(self)
        self.thread.start()

    def onResume(self):

        Activity.onResume(self)
        self.handler.postDelayed(self.view, long(25))

    def onPause(self):

        Activity.onPause(self)
        self.handler.removeCallbacks(self.view)

    def stopServer(self):

        if not self.running:
            return

        # Indicate that the server is not running, mostly for the GUI but
        # also for the server thread.
        self.running = False

        # Work around a bug in Android that prevents the server socket from
        # closing if it is still waiting for accept to return.
        if not self.serving:
            s = LocalSocket()
            s.connect(LocalSocketAddress("displayserver"))

        # Close the server socket so that new connections cannot be made.
        self.serverSocket.close()

        # Wait until the thread has finished.
        self.thread.join()

    @args(void, [ByteBuffer])
    def handleMessage(self, buf):

        x = buf.getInt(0)
        y = buf.getInt(4)
        bitmap = BitmapFactory.decodeByteArray(buf.array(), 8, buf.capacity() - 8)
        if bitmap != None:
            canvas = Canvas(self.screen)
            canvas.drawBitmap(bitmap, x, y, None)

    def onClick(self, view):
        pass

    def run(self):

        while self.running:

            try:
                socket = self.serverSocket.accept()
                self.serveConnection(socket)

            except (IOException,), ioe:
                Log.d("DUCK", str(ioe))

        message = self.handler.obtainMessage(1, None)
        message.sendToTarget()

    @args(void, [LocalSocket])
    def serveConnection(self, socket):

        self.serving = True
        inputStream = socket.getInputStream()

        while self.running:
            length = self.readData(inputStream, 4).getInt()
            Log.d("DUCK", "length=" + str(length))
            if length < 0 or length > self.view.maxBufferSize():
                raise IOException("Invalid length")
            data = self.readData(inputStream, length)
            self.handleMessage(data)

        self.serving = False
        socket.close()

    @args(ByteBuffer, [InputStream, int])
    def readData(self, inputStream, size):

        buf = ByteBuffer.allocate(size)
        buf.order(ByteOrder.LITTLE_ENDIAN)
        read = 0
        waited = 0

        while read < size:

            a = Math.min(inputStream.available(), size - read)
            if a >= 1:
                r = inputStream.read(buf.array(), read, a)
                if r == -1:
                    raise IOException("Stream closed")
                read += r
                waited = 0
            else:
                self.thread.sleep(10)
                waited += 1
                if waited == 10000:
                    raise IOException("Timed out")
                elif not self.running:
                    raise IOException("Stopped")

        return buf


class MessageHandler(Handler):

    @args(void, [Looper, LocalServerActivity])
    def __init__(self, looper, activity):

        Handler.__init__(self, looper)
        self.activity = activity

    def handleMessage(self, inputMessage):

        if inputMessage.what == 0:
            Log.d("DUCK", "what=0")
            self.activity.handleMessage(CAST(inputMessage.obj, ByteBuffer))

        elif inputMessage.what == 1:
            self.activity.stopServer()


class DrawView(View):

    __interfaces__ = [Runnable]
    __fields__ = {"bitmap": Bitmap}

    @args(void, [LocalServerActivity])
    def __init__(self, activity):

        View.__init__(self, activity)
        self.bitmap = None
        self.width = self.height = 0
        self.activity = activity

    def onSizeChanged(self, width, height, oldWidth, oldHeight):

        if self.bitmap == None:
            self.width = width
            self.height = height
            self.bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
            self.bitmap.eraseColor(Color.argb(255, 40, 80, 160))
            self.srcRect = Rect(0, 0, width, height)
            self.destRect = Rect(0, 0, width, height)
            self.activity.screen = self.bitmap

    def onDraw(self, canvas):

        View.onDraw(self, canvas)
        if self.bitmap != None:
            canvas.drawBitmap(self.bitmap, self.srcRect, self.destRect, None)

    @args(int, [])
    def maxBufferSize(self):

        return self.width * self.height * 4

    def run(self):

        self.invalidate()
        self.activity.handler.postDelayed(self, long(25))
