#!/usr/bin/env python

"""
Copyright (C) 2016 David Boddie <david@boddie.org.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import math

template = """<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN"
  "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">

<svg version="1.1"
     xmlns="http://www.w3.org/2000/svg"
     xmlns:xlink="http://www.w3.org/1999/xlink"
     width="1.5cm" height="1.5cm"
     viewBox="0 0 %(width)i %(height)i">

%(paths)s
</svg>
"""

path_template = """<path d="%(path)s"
      style="stroke:%(colour)s;stroke-width:1;fill:none" />
"""

def p(r, angle):

    return (cx + (r * math.cos(angle)), cy - (r * math.sin(angle)))

def make_sky(y1, y2, r):

    path = []
    d = 0
    x = 0
    y = y1
    outside = True
    ty1 = 0
    ty2 = 0
    
    while y <= y2:
    
        if y < (cy - r) or y > (cy + r):
            x = [0, w, w, 0][d]
            
            if not outside:
                ty2 = y
                outside = True
        else:
            if outside:
                ty1 = y
                outside = False
            
            if x < cx:
                dx = (cx - (r**2 - (y - cy)**2)**0.5) 
                x = [0, dx, dx, 0][d]
            else:
                dx = (cx + (r**2 - (y - cy)**2)**0.5)
                x = [dx, w, w, dx][d]
        
        path.append("%.3f,%.3f" % (x, y))
        
        if d % 2 == 1:
            y += 4
        
        d = (d + 1) % 4
    
    return path, ty1, ty2


# Define some paths for the SVG drawing.
paths = []

w = h = 200.0
cx = w/2
cy = h/2
r1 = w/2 - 12
r2 = w/2 - 10
r3 = w/2 - 5

# Sky
#path, y1, y2 = make_sky(0, h, r3)
#path = "M " + path[0] + " " + " ".join(map(lambda c: "L " + c, path))
#paths.append(path_template % {"path": path, "colour": "#5bd2b5"})

#path, y1, y2 = make_sky(y1, y2, r3)
#path = "M " + path[0] + " " + " ".join(map(lambda c: "L " + c, path))
#paths.append(path_template % {"path": path, "colour": "#5bd2b5"})


# Halo
path = []

a = 0
da = math.pi / 50.0

while a <= 100:

    path.append("%.3f,%.3f" % p(r3, a*da))
    a += 1
    path.append("%.3f,%.3f" % p(r2, a*da))
    a += 1

path = "M " + path[0] + " " + " ".join(map(lambda c: "L " + c, path))
paths.append(path_template % {"path": path, "colour": "#ffbbbb"})


# Sun
path = []

a = math.pi * 0.75
l = r1 - 2
d = 0

while l >= -r1 + 2:

    angle = math.pi/4 + math.asin(l/r1)
    if d == 0:
        path.append("%.3f,%.3f" % p(r1, angle))
        path.append("%.3f,%.3f" % p(r1, a + (a - angle)))
    else:
        path.append("%.3f,%.3f" % p(r1, a + (a - angle)))
        path.append("%.3f,%.3f" % p(r1, angle))
    
    d = 1 - d
    l -= 4

path = "M " + path[0] + " " + " ".join(map(lambda c: "L " + c, path))
paths.append(path_template % {"path": path, "colour": "#ff6600"})

f = open("sample.svg", "w")
f.write(template % {"width": w, "height": h, "paths": "\n".join(paths)})
f.close()
