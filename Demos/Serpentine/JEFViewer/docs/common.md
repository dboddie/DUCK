# Common

This module provides common classes used by other modules in the JEF Viewer
application.

    Copyright (C) 2016 David Boddie <david@boddie.org.uk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

## Imports

We import various standard Java classes used for data structures and
input/output handling.

```python
from java.io import DataInputStream, InputStream, IOException
from java.lang import Object, String
from java.nio import ByteBuffer, ByteOrder
```

## LittleStream

This class is used to provide a convenient way to read little-endian values
from an `InputStream` object.

```python
class LittleStream(Object):

    __fields__ = {"position": int}
    
    @args(void, [InputStream])
    def __init__(self, input):
    
        Object.__init__(self)
        
        self.stream = DataInputStream(input)
        self.position = 0
    
    @args(ByteBuffer, [int])
    def _readBytes(self, n):
    
        buf = ByteBuffer.allocate(n)
        buf.order(ByteOrder.LITTLE_ENDIAN)
        read = 0
        while read < n:
            r = self.stream.read(buf.array(), read, n - read)
            if r == -1:
                raise IOException()
            read += r
        
        self.position += n
        return buf
    
    @args(String, [int])
    def readASCII(self, n):
    
        return String(self.readBytes(n), "ASCII")
    
    @args([byte], [int])
    def readBytes(self, n):
    
        buf = self._readBytes(n)
        return buf.array()
    
    @args(int, [])
    def readInt(self):
    
        buf = self._readBytes(4)
        return buf.getInt()
    
    @args(short, [])
    def readShort(self):
    
        buf = self._readBytes(2)
        return buf.getShort()
    
    @args(byte, [])
    def readByte(self):
    
        self.position += 1
        return self.stream.readByte()
    
    @args(void, [int])
    def skipBytes(self, number):
    
        self.stream.skipBytes(number)
        self.position += number
```

## Rectangle

This class represents a bounding rectangle that describes the extent of a
sequence of stitches in a particular thread colour.

```python
class Rectangle(Object):

    __fields__ = {"x1": int, "y1": int, "x2": int, "y2": int}
    
    @args(void, [int, int, int, int])
    def __init__(self, x1, y1, x2, y2):
    
        Object.__init__(self)
        self.x1 = x1
        self.y1 = y1
        self.x2 = x2
        self.y2 = y2
```

## Stitch

This class describes an individual stitch in a sequence of stitches that make
up part of a pattern. Each stitch contains a command that is either `"move"` or
`"stitch"`. The `x` and `y` fields describe absolute coordinates.

```python
class Stitch(Object):

    __fields__ = {"command": String, "x": int, "y": int}
    
    @args(void, [String, int, int])
    def __init__(self, command, x, y):
    
        Object.__init__(self)
        self.command = command
        self.x = x
        self.y = y
```



## Files

* [Resources/icon.svg](../Resources/icon.svg)
* [Resources/sample.svg](../Resources/sample.svg)
