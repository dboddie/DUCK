# JEF Colours

This module provides information about colour definitions to the other
components in the application.

    Copyright (C) 2016 David Boddie <david@boddie.org.uk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

## Imports

We import some classes that are needed to define methods or used to create
objects. The JSON classes are used to unpack some resource data that the build
script supplies in JSON format.

```python
from java.lang import Object, String
from java.util import Map
from android.content.res import Resources
from android.graphics import Color
from org.json import JSONArray, JSONObject, JSONTokener
```

Again, we import the `R` class in order to access resources in the
application package.

```python
from app_resources import R
```

## ColourInfo

The following class is used by other components to access information about
colour definitions. It is instantiated with a `Resources` object that allows
the instance to access the application's resources.

```python
class ColourInfo(Object):

    @args(void, [Resources])
    def __init__(self, resources):
    
        Object.__init__(self)
```

The colours are defined using two arrays in the resources: one
containing colour names and the other containing definitions. We read
these into lists then zip them together to create a list of pairs.

```python
        names = resources.getStringArray(R.array.colour_names)
        colours = resources.getIntArray(R.array.colour_defs)
        
        # Zip the names and colours together then put the lists in a dictionary
        # with keys starting from 1.
        name_colours = zip(names, colours)
```

We put the list of name-colour pairs into a dictionary with keys
starting from 1. The keys are Janome colour codes and the values are
the name-colour pairs.

```python
        self.colours = dict(range(1, len(name_colours) + 1), name_colours)
```

We read the definitions of thread names and codes from a
JSON-encoded string in the resources with a well-defined structure.

```python
        obj = JSONTokener(resources.getString(R.string.thread_defs)).nextValue()
        json_array = CAST(obj, JSONArray)
        
        self.thread_codes = {}
        i = 0
        
        while i < len(json_array):
        
            json_obj = json_array.getJSONObject(i)
            it = json_obj.keys()
            codes = {}
            
            while it.hasNext():
                key = it.next()
                codes[key] = json_obj.getInt(key)
            
            self.thread_codes[i + 1] = codes
            i += 1
```

We define two methods for retrieving the colour and name for a given
colour code.

```python
    @args(int, [int])
    def getColour(self, c):
        # Retrieve the colour value from the Pair and augment it with full alpha.
        return self.colours[c].second() | Color.argb(255, 0, 0, 0)
    
    @args(String, [int])
    def getName(self, c):
        return self.colours[c].first()
```

We also define a method for retrieving the thread information for a
given colour code, though we do not actually use it elsewhere in the
application.

```python
    @args(Map(String, int), [int])
    def getThreadCodes(self, c):
        return self.thread_codes[c]
```



## Files

* [Resources/icon.svg](../Resources/icon.svg)
* [Resources/sample.svg](../Resources/sample.svg)
