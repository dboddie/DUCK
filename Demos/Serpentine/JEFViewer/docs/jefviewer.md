# JEF Viewer

This application can be used to preview Janome JEF embroidery files stored in
the external Download directory of a device.

    Copyright (C) 2016 David Boddie <david@boddie.org.uk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

## Imports

We import classes from the standard Java and Android libraries that we need in
this module.

```python
from java.io import File
from java.lang import Math
from android.app import Activity
from android.os import Environment
from android.widget import GridView
```

We import a class from the `app_resources` module bundled with the
application. This is created by the build script and is used to access the
data resources used by the application.

```python
from app_resources import R
```

We also import classes provided by the [pattern](pattern.html) module in the
application.

```python
from pattern import Pattern, PatternInfo, PatternViewAdapter
```

## JEFViewerActivity

The main class in the application is derived from the standard `Activity`
class. Since most of the functionality is provided by other classes, this class
only provides a minimal `__init__` method.

```python
class JEFViewerActivity(Activity):

    def __init__(self):
    
        Activity.__init__(self)
```

The following method initialises the application and sets up the user
interface.

```python
    def onCreate(self, bundle):
    
        Activity.onCreate(self, bundle)
        
        #window = self.getWindow()
        #window.requestFeature(Window.FEATURE_ACTION_BAR)
        
        resources = self.getResources()
        config = resources.getConfiguration()
        
        #bar = self.getActionBar()
```

Since the application will be looking for files in the Download
directory in the external storage of a device, we obtain the path to
that directory and create an adapter to supply a `GridView` with
thumbnail images of the JEF files inside it.

```python
        envDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)
        subDir = File(envDir, "JEF")
        
        self.adapter = PatternViewAdapter(subDir, resources)
        
        self.layout = GridView(self)
        self.layout.setAdapter(self.adapter)
        self.layout.setHorizontalSpacing(8)
        self.layout.setVerticalSpacing(8)
        
        columns = Math.max(1, config.screenWidthDp / self.adapter.size)
        self.layout.setNumColumns(columns)
        
        self.setContentView(self.layout)
```

This method is called if the application is informed of a change in
configuration of the device. We use the opportunity to adapt the `GridView`
to the screen size and orientation.

```python
    def onConfigurationChanged(self, config):
    
        Activity.onConfigurationChanged(self, config)
        
        columns = Math.max(1, config.screenWidthDp / self.adapter.size)
        self.layout.setNumColumns(columns)
```



## Files

* [Resources/icon.svg](../Resources/icon.svg)
* [Resources/sample.svg](../Resources/sample.svg)
* [pattern.py](../pattern.py)
