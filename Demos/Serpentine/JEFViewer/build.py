#!/usr/bin/env python

"""
Copyright (C) 2016 David Boddie <david@boddie.org.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import json, os, sys

from DUCK.Tools import buildhelper

# See docs/guide/topics/resources/runtime-changes.html
# android.content.pm.ActivityInfo
CONFIG_ORIENTATION = 0x0080
CONFIG_SCREEN_SIZE = 0x0400

thread_colours = [
    ['Black', '#000000'],
    ['White', '#F0F0F0'],
    ['Sunflower', '#FFCC00'],
    ['Dark Brown', '#481A05'],
    ['Green Dust', '#C4E39D'],
    ['Green', '#237336'],
    ['Navy Blue', '#071650'],
    ['Emerald Green', '#4CB58F'],
    ['Pink', '#F669A0'],
    ['Vermilion', '#FF4720'],
    ['Cinnamon', '#E2A188'],
    ['Dark Gray', '#595B61'],
    ['Gold', '#E4C35D'],
    ['Black', '#000000'],
    ['Pale Violet', '#AC9CC7'],
    ['Ivory White', '#FFFFDC'],
    ['Pale Pink', '#F999B7'],
    ['Salmon Pink', '#FFBBBB'],
    ['Olive Drab', '#A39166'],
    ['Wine Red', '#970533'],
    ['Pale Violet', '#AC9CC7'],
    ['Sky', '#65C2C8'],
    ['Silver Gray', '#E5E5E5'],
    ['Gray', '#889B9B'],
    ['Pale Aqua', '#98D6BD'],
    ['Blue', '#0B2F84'],
    ['Powder Blue', '#98F3FE'],
    ['Solar Blue', '#165FA7'],
    ['Cardinal Red', '#FF0927'],
    ['Navy Blue', '#071650'],
    ['Vermilion', '#FF4720'],
    ['Orange', '#FF6600'],
    ['Vermilion', '#FF4720'],
    ['Umber', '#B59474'],
    ['Pale Yellow', '#FDF5B5'],
    ['Blond', '#F5DB8B'],
    ['Sunflower', '#FFCC00'],
    ['Pale Pink', '#F999B7'],
    ['Purple', '#AB5A96'],
    ['Beige Gray', '#D0BAB0'],
    ['Peony Purple', '#C3007E'],
    ['Red', '#FF0000'],
    ['Green', '#237336'],
    ['Moss Green', '#608541'],
    ['Moss Green', '#608541'],
    ['Green', '#237336'],
    ['Aquamarine', '#5BD2B5'],
    ['Old Gold', '#C79732'],
    ['Peacock Green', '#04917B'],
    ['Dark Sepia', '#5C2625'],
    ['Ivory White', '#FFFFDC'],
    ['Burnt Orange', '#FF5A27'],
    ['Blond', '#F5DB8B'],
    ['Brown', '#9C6445'],
    ['Sienna', '#B45A30'],
    ['Violet Blue', '#6231BD'],
    ['Meadow', '#0C8918'],
    ['Bright Blue', '#70A9E2'],
    ['Slate Blue', '#1D5478'],
    ['Baby Blue', '#B2E1E3'],
    ['Yellow Green', '#7FC21C'],
    ['Dark Green', '#06480D'],
    ['Wine', '#843154'],
    ['Crimson', '#FD33A3'],
    ['Salmon', '#F09C96'],
    ['Mustard', '#F7F297'],
    ['Bright Green', '#00B552'],
    ['Ocean Blue', '#0257B5'],
    ['Toast', '#E6965A'],
    ['Beige', '#D7BDA4'],
    ['Honey Dew', '#FF9D00'],
    ['Tangerine', '#FFBA5E'],
    ['Canary Yellow', '#FCF121'],
    ['Hazel', '#E6651E'],
    ['Royal Purple', '#540571'],
    ['Yellow Ocher', '#CC9900'],
    ['Orchid Pink', '#FFBDE3'],
    ['Bamboo', '#E3BE81']
    ]

thread_codes = [
    {'Isacord Polyester': 20, 'Janome': 2, 'Madeira Rayon': 1000,
        'Robison-Anton Polyester': 5596, 'Sulky Rayon': 1005},
    {'Isacord Polyester': 15, 'Janome': 1, 'Madeira Rayon': 1001,
        'Sulky Rayon': 1001},
    {'Janome': 239, 'Madeira Rayon': 1024, 'Sulky Rayon': 1024},
    {'Janome': 205, 'Madeira Rayon': 1278},
    {'Janome': 264, 'Madeira Rayon': 1393},
    {'Isacord Polyester': 5513, 'Janome': 226,
        'Madeira Rayon': 1170, 'Sulky Rayon': 1051},
    {'Janome': 232, 'Sulky Rayon': 1289},
    {'Isacord Polyester': 2830, 'Janome': 250,
        'Madeira Rayon': 1032, 'Sulky Rayon': 1032},
    {'Janome': 201},
    {'Janome': 202, 'Madeira Rayon': 1037, 'Sulky Rayon': 1037},
    {'Janome': 236, 'Madeira Rayon': 1126},
    {'Janome': 252, 'Madeira Rayon': 1242, 'Sulky Rayon': 1042},
    {'Janome': 3, 'Madeira Rayon': 1025},
    {'Janome': 2, 'Madeira Rayon': 1043, 'Sulky Rayon': 1005},
    {'Janome': 209, 'Madeira Rayon': 1330,
         'Robison-Anton Polyester': 5586},
    {'Isacord Polyester': 761, 'Janome': 253,
         'Madeira Rayon': 1084, 'Sulky Rayon': 1127},
    {'Janome': 211},
    {'Janome': 233, 'Madeira Rayon': 1020, 'Sulky Rayon': 1259},
    {'Janome': 268, 'Madeira Rayon': 1105},
    {'Janome': 215, 'Madeira Rayon': 1181},
    {'Isacord Polyester': 3652, 'Janome': 209,
         'Madeira Rayon': 1232, 'Sulky Rayon': 1193},
    {'Janome': 217, 'Robison-Anton Rayon': 2321},
    {'Janome': 220, 'Madeira Rayon': 1001},
    {'Janome': 221, 'Madeira Rayon': 1360},
    {'Janome': 227},
    {'Janome': 207, 'Madeira Rayon': 1028, 'Sulky Rayon': 1143},
    {'Janome': 229},
    {'Janome': 263, 'Madeira Rayon': 1375, 'Sulky Rayon': 1201},
    {'Janome': 244, 'Robison-Anton Rayon': 2529},
    {'Janome': 232},
    {'Janome': 202, 'Madeira Rayon': 1315},
    {'Janome': 203, 'Robison-Anton Polyester': 5769},
    {'Janome': 202, 'Madeira Rayon': 1037},
    {'Janome': 237, 'Madeira Rayon': 1272},
    {'Janome': 210, 'Madeira Rayon': 1105},
    {'Janome': 238},
    {'Janome': 239, 'Madeira Rayon': 1068},
    {'Janome': 211, 'Madeira Rayon': 1232},
    {'Isacord Polyester': 2504, 'Janome': 208,
         'Madeira Rayon': 1188, 'Robison-Anton Polyester': 5800,
         'Sulky Rayon': 1192},
    {'Janome': 223, 'Madeira Rayon': 1383},
    {'Isacord Polyester': 2910, 'Janome': 241,
         'Madeira Rayon': 1334, 'Sulky Rayon': 1255},
    {'Janome': 225, 'Madeira Rayon': 1147},
    {'Janome': 226, 'Madeira Rayon': 1086},
    {'Janome': 246, 'Madeira Rayon': 1106},
    {'Isacord Polyester': 6133, 'Janome': 246,
         'Madeira Rayon': 1194, 'Sulky Rayon': 1156},
    {'Isacord Polyester': 5944, 'Janome': 226,
         'Madeira Rayon': 1370, 'Sulky Rayon': 1174},
    {'Janome': 249},
    {'Janome': 272},
    {'Janome': 251, 'Madeira Rayon': 1185},
    {'Janome': 260, 'Madeira Rayon': 1396},
    {'Janome': 253},
    {'Isacord Polyester': 1312, 'Janome': 235,
         'Madeira Rayon': 1221, 'Sulky Rayon': 1181},
    {'Isacord Polyester': 832, 'Janome': 238,
         'Madeira Rayon': 1255, 'Sulky Rayon': 1055},
    {'Janome': 214, 'Madeira Rayon': 1136},
    {'Janome': 258, 'Madeira Rayon': 1192},
    {'Janome': 261, 'Madeira Rayon': 1385},
    {'Janome': 269},
    {'Janome': 230, 'Madeira Rayon': 1000},
    {'Janome': 231, 'Madeira Rayon': 1360},
    {'Isacord Polyester': 3900, 'Janome': 228,
         'Madeira Rayon': 1134, 'Robison-Anton Polyester': 5684,
         'Sulky Rayon': 1042},
    {'Janome': 218, 'Madeira Rayon': 1029, 'Sulky Rayon': 1134},
    {'Janome': 248, 'Madeira Rayon': 1049, 'Sulky Rayon': 1279},
    {'Isacord Polyester': 2520, 'Janome': 267,
         'Madeira Rayon': 1354, 'Sulky Rayon': 1511},
    {'Janome': 265, 'Madeira Rayon': 1309},
    {'Janome': 256, 'Madeira Rayon': 1181},
    {'Janome': 270, 'Madeira Rayon': 1156,
         'Robison-Anton Polyester': 5802, 'Robison-Anton Rayon': 2572,
         'Sulky Rayon': 1179},
    {'Janome': 206, 'Madeira Rayon': 1249,
         'Robison-Anton Polyester': 5508},
    {'Janome': 222, 'Madeira Rayon': 1270},
    {'Isacord Polyester': 546, 'Janome': 255,
         'Madeira Rayon': 1065, 'Sulky Rayon': 1227},
    {'Janome': 213, 'Madeira Rayon': 1025},
    {'Janome': 273},
    {'Isacord Polyester': 811, 'Janome': 274,
         'Madeira Rayon': 1155, 'Sulky Rayon': 1238},
    {'Janome': 275, 'Madeira Rayon': 1248},
    {'Janome': 254, 'Madeira Rayon': 1146,
         'Robison-Anton Polyester': 5536},
    {'Janome': 243, 'Madeira Rayon': 1250},
    {'Janome': 271, 'Madeira Rayon': 1335},
    {'Janome': 240, 'Madeira Rayon': 1138},
    {'Janome': 224, 'Madeira Rayon': 1055}
    ]

app_name = "JEF Viewer"
activity_dict = {
    "android:configChanges": CONFIG_ORIENTATION | CONFIG_SCREEN_SIZE
    }
package_name = "com.example.jefviewer"
res_files = {
    "drawable": {"ic_launcher": "Resources/icon.svg"},
    "raw": {"sample": "Resources/sample.jef"},
    "values": {
        "colour_names": map(lambda p: p[0], thread_colours),
        "colour_defs": map(lambda p: p[1], thread_colours)
        },
    "string": {
        "thread_defs": json.JSONEncoder().encode(thread_codes)
        }
    }
code_file = "jefviewer.py"
include_paths = []
layout = None
features = []
permissions = ["android.permission.READ_EXTERNAL_STORAGE"]

# Documentation
docs_dir = os.getenv("DOCS_DIR")

if __name__ == "__main__":

    args = sys.argv[:]
    
    result = buildhelper.main(__file__, app_name, package_name, res_files,
        layout, code_file, include_paths, features, permissions, args,
        description = activity_dict, docs_dir = docs_dir,
        include_sources = False)
    
    sys.exit(result)
